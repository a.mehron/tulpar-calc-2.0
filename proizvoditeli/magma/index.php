<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Магма Керамик");
?>



<div class="container p-0 brand-page">
    <div class="banner" style="background-image:url('<?=SITE_TEMPLATE_PATH?>/assets/img/brands/banners/магма.jpg')">
        <h1 class="title">Магма Керамик</h1>
    </div>

    <div class="content-wrapper">
        <div class="navigation">
            <ul class="article-nav-list" id="brandpage-nav">
                <li class="article-nav-item">
                    <a href="#id-1">
                        <span class="article-nav__num">1</span>
                        <span class="article-nav__text">Видео о заводе</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-2">
                        <span class="article-nav__num">2</span>
                        <span class="article-nav__text">Информация о заводе</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-3">
                        <span class="article-nav__num">3</span>
                        <span class="article-nav__text">Видеообзор продукции завода</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-4">
                        <span class="article-nav__num">4</span>
                        <span class="article-nav__text">Каталог продукции завода</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-5">
                        <span class="article-nav__num">5</span>
                        <span class="article-nav__text">Подбор продукции завода</span>
                    </a>
                </li>

            </ul>
        </div>

        <div class="content">
            <h2>Видео о заводе</h2>
			
            <div class="info" id="id-2">
                <h1>«Магма Керамик» — производитель кирпича, отвечающего мировым качественным стандартам</h1>
				<p>Кирпичный завод «Магма Керамик» — современное производственное предприятие, изготавливающее ежегодно до 42 миллионов штук условного кирпича. Завод располагается в Республике Мордовия (село Дубёнки) и выпускает продукцию под торговой маркой «Магма». Кирпич производится методом пластического формования при помощи европейского оборудования — автоматизированной технологической линии KELLER.</p>
				<h2>Выпускаемая продукция</h2>
				<p>ТМ «Магма» выпускает клинкерный кирпич, керамический кирпич, клинкерную брусчатку, строительные смеси. Продукция соответствует ГОСТ 32311-2012, ГОСТ 530-2012, ТУ 5741-003-76380703-2015 и другим нормативным требованиям, предъявляемым к данному виду изделий.</p>
				
				<h2>Преимущества кирпича ТМ «Магма»</h2>
				<p><b>Высокая прочность изделий. </b>Продукция «Магма» соответствует марке прочности М150.</p>
				<p><b>Долговечность. </b>Кирпич устойчив к воздействию природных факторов: хорошо переносит снег, дождь, жаркую погоду, выдерживает неоднократные замерзания и оттаивания. Морозостойкость кирпича «Магма» — F100. </p>
				<p><b>Отличное качество. </b>Для производства кирпича используется высококачественное сырье и ультрасовременное оборудование. На всех стадиях производства осуществляется строгий контроль качества.</p>
				<p><b>Безупречный внешний вид. </b>Кирпич «Магма» отличается своей эстетической привлекательностью и разнообразием оттенков.</p>
				<h2>Кирпич завода «Магма Керамик» в Казани</h2>
				<p>У нас вы можете заказать лицевой кирпич формата 1НФ или 1.4НФ, изготовленный с применением технологии флеш-обжига. Обжиг в печи позволяет выпускать изделия без применения химикатов, благодаря чему произведённый кирпич является экологически чистым продуктом. </p>
				<p>В нашем каталоге вы можете подобрать кирпич подходящего цвета — красный, шоколад, ваниль или белый топаз, а также выбрать поверхность — гладкую или антик. Оплатить заказ можно очень удобно — банковским переводом в режиме онлайн. </p>
				<p>Если вам требуется более подробная информация о характеристиках изделий, помощь в подборе кирпича и расчёте необходимого количества, обращайтесь к нашим специалистам. Звоните по телефону: 8 (843) 231-83-99 или приходите по адресу: г. Казань, ул. Даурская, д. 12а, офис 20.4.   </p>
				
				</div>
            <div class="brand-catalog" id="id-4">
            <h2>Каталог продукции завода</h2>
            <?
            $APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"brand-products", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "PROP",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "CODE",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_COLOR",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "19",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => array(
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "716",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "brand-products",
		"ENLARGE_PROP" => "-",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_COLOR",
		)
	),
	false
);?>

            <a href="/catalog/saranskiy/" class="see-all-products">Посмотреть все товары</a>
            </div>

            <div class="brand-element" id="id-5">
                <h2>Подбор продукции завода</h2>
            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.element", 
	"catalog-element2", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_DETAIL_TO_SLIDER" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
		),
		"ADD_TO_BASKET_ACTION_PRIMARY" => array(
			0 => "BUY",
		),
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BRAND_USE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_SECTION_ID_VARIABLE" => "N",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_PICTURE_MODE" => array(
			0 => "POPUP",
		),
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "9012",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"IMAGE_RESOLUTION" => "16by9",
		"LABEL_PROP" => array(
		),
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"MAIN_BLOCK_OFFERS_PROPERTY_CODE" => array(
		),
		"MAIN_BLOCK_PROPERTY_CODE" => array(
		),
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_COMMENTS_TAB" => "Комментарии",
		"MESS_DESCRIPTION_TAB" => "Описание",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MESS_PRICE_RANGES_TITLE" => "Цены",
		"MESS_PROPERTIES_TAB" => "Характеристики",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_GAZOBLOCK_HEIGHT_SKU",
			1 => "ATT_SNOWKEEPER_LENGTH_SKU",
			2 => "ATT_SIZE_BLOCKS_SKU",
			3 => "ATT_GAZOBLOCK_WIDTH_SKU",
			4 => "ATT_GLOSS_SKU",
			5 => "ATT_MIX_WEIGHT_SKU",
			6 => "ATT_BRICKS_WEIGHT_SKU",
			7 => "ATT_COATING_KIND_SKU",
			8 => "ATT_WATERABSORBANCE_BRICKS_SKU",
			9 => "ATT_WATERABSORBANCE_MIXES_SKU",
			10 => "MORE_PHOTO",
			11 => "ATT_SALT_FOG_PROTECTION_SKU",
			12 => "ATT_BRICK_CATEGORY",
			13 => "ATT_COATING_CLASS_SKU",
			14 => "ATT_STRENGTH_CLASS_BLOCKS_SKU",
			15 => "ATT_GROUP_PACK_QTY_DOBORNIYE_SKU",
			16 => "ATT_TRUCK_QTY_BRICKS_SKU",
			17 => "ATT_BLOCK_PODDON_VOLUME",
			18 => "ATT_BRICKS_PODDIN_QTY_SKU",
			19 => "ATT_BLOCK_QTY_PER_M3",
			20 => "ATT_COROSION_RESISTANCE_SKU",
			21 => "ATT_BRICK_HEATCOND_COEF_SKU",
			22 => "ATT_MAX_TEMP_SKU",
			23 => "ATT_DENSITY_MARK_BLOCKS_SKU",
			24 => "ATT_COLDRESISTANCE_BRICK_SKU",
			25 => "ATT_VOLUME_SKU",
			26 => "ATT_VOLUME_ML_STROIT_HIMIYA_SKU",
			27 => "ATT_COLOR_TONE",
			28 => "ATT_BRICK_SURFACE",
			29 => "ATT_BEND_STRENGTH_SKU",
			30 => "ATT_UDAR_STRENGTH_SKU",
			31 => "ATT_PRODUCT_SIZE_MM_DOBORNIYE_SKU",
			32 => "ATT_INDIVIDUAL_PACK_SIZE_DOBORNIYE_SKU",
			33 => "ATT_RASTVOR_CONSUM_SKU",
			34 => "ATT_RASHOD_RASTVORA_M3_SKU",
			35 => "ATT_BRICKS_CONSUMPTION_SKU",
			36 => "ATT_WARRANTY_YEARS_SKU",
			37 => "ATT_UV_RESISTANCE_SKU",
			38 => "ATT_PENCIL_HARDNESS_SKU",
			39 => "ATT_THICKNESS",
			40 => "ATT_MECHANIC_RESISTANCE",
			41 => "ATT_BRICK_FORMAT",
			42 => "ATT_COLOR_BRICK",
			43 => "ATT_COLOR_KROV_VENT_SPRAV_SKU",
			44 => "ATT_BRICK_COLOR_SKU_SPRAV",
			45 => "ATT_COLOR_MIXES_SKU",
			46 => "ATT_COLOR_FASTNESS",
			47 => "ATT_BRICK_STRENGTH_SKU",
			48 => "ATT_COATING",
			49 => "ATT_COLOR",
			50 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_SIZE_BLOCKS_SKU",
			1 => "ATT_GAZOBLOCK_WIDTH_SKU",
			2 => "ATT_GAZOBLOCK_HEIGHT_SKU",
			3 => "ATT_BRICK_FORMAT",
			4 => "ATT_COLOR_BRICK",
			5 => "ATT_BRICK_SURFACE",
		),
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
		"PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "ATT_MAKER",
			1 => "ATT_SNOWKEEPER_LENGTH",
			2 => "ATT_SIZE_BRICK",
			3 => "ATT_KIRPICH_TYPE",
			4 => "ATT_GLOSS",
			5 => "ATT_WEIGHT_MIXES",
			6 => "ATT_WEIGHT_KG",
			7 => "ATT_COATING_KIND",
			8 => "ATT_WATERABSORBANCE_BRICKS",
			9 => "ATT_WATERABSORBANCE_MIXES",
			10 => "ATT_PROFIL_HEIGHT_MM",
			11 => "ATT_HEIGHT_KROVLYA_VENTILATION",
			12 => "ATT_DIAMETR_KROV_VENTILATION",
			13 => "ATT_SHEET_LENGTH_MM",
			14 => "ATT_RULON_LENGTH_WIDTH",
			15 => "ATT_LIFESPAN_MIXES",
			16 => "ATT_SALT_FOG_PROTECTION",
			17 => "ATT_COATING_CLASS",
			18 => "ATT_STRENGTH_CLASS_BLOCKS",
			19 => "ATT_GROUP_PACK_QTY_DOBORNIYE",
			20 => "ATT_TRUCK_QTY_BRICKS",
			21 => "ATT_BLOCK_PODDON_VOLUME",
			22 => "ATT_PODDON_QTY",
			23 => "ATT_BLOCK_QTY_PER_M3",
			24 => "ATT_COROSION_RESISTANCE",
			25 => "ATT_WATERPROOF_CF",
			26 => "ATT_VAPOURPROOF_CF",
			27 => "ATT_HEAT_TRANSFER_COEF",
			28 => "ATT_BRICK_HEATCOND_COEF",
			29 => "ATT_MAX_TEMP",
			30 => "ATT_DENSITY_MARK_BLOCKS",
			31 => "ATT_MIN_KRATNOST_KREPEZH_SHT",
			32 => "ATT_COLDRESIST_MIXES",
			33 => "ATT_USAGE_MIXES",
			34 => "ATT_VOLUME",
			35 => "ATT_VOLUME_ML_STROIT_HIMIYA",
			36 => "ATT_SURFACE_DENSITY_SETKA",
			37 => "ATT_POROSITY_BRICK",
			38 => "ATT_STRENGHT_LIMIT_BRICK",
			39 => "ATT_COMPRESSIVE_STRENGTH_MIXES",
			40 => "ATT_BEND_STRENGTH",
			41 => "ATT_UDAR_STRENGTH",
			42 => "ATT_COHESIVE_STRENGTH_MIXES",
			43 => "ATT_CELL_SIZE_SETKA",
			44 => "ATT_PRODUCT_SIZE_MM_DOBORNIYE",
			45 => "ATT_INDIVIDUAL_PACK_SIZE_DOBORNIYE",
			46 => "ATT_RAZRYV_NAGRUZKA_SETKA",
			47 => "ATT_WATER_CONSUMPTION_MIXES",
			48 => "ATT_RASTVOR_CONSUM",
			49 => "ATT_RASHOD_RASTVORA_M3",
			50 => "ATT_MIX_USAGE_M2",
			51 => "ATT_BRICKS_CONSUMPTION",
			52 => "ATT_WARRANTY_YEARS",
			53 => "ATT_STORAGE_TIME_MIXES",
			54 => "ATT_SHIPPING_COST",
			55 => "ATT_UV_RESISTANCE",
			56 => "ATT_PENCIL_HARDNESS",
			57 => "ATT_USAGE_TEMP_MIXES",
			58 => "ATT_KROVLYA_TYPE_VENTILATION",
			59 => "ATT_COATING_TYPE",
			60 => "ATT_THICKNESS_MM",
			61 => "ATT_UDLIN_PRI_RAZRYVE_SETKA",
			62 => "ATT_MECHANIC_RESISTANCE",
			63 => "ATT_BRICK_COLOR",
			64 => "ATT_COLOR_MIXES",
			65 => "ATT_COLOR_KROV_VENT_SPRAV",
			66 => "ATT_COLOR_MIXES_SPRAV",
			67 => "ATT_COLOR_STOYKOST",
			68 => "ATT_RULON_PRICE_SETKA",
			69 => "ATT_WAVE_INCR_MM",
			70 => "ATT_SHEET_WIDTH",
			71 => "ATT_PROFIL_WIDTH",
			72 => "ATT_WIDTH_SETKA",
			73 => "ATT_BRICKS_FORMAT",
			74 => "ATT_COLOR_TONE_BRICK",
			75 => "ATT_SURFACE_BRICK",
			76 => "ATT_STRENGTH_MARK",
			77 => "ATT_COLDRESISTANCE_BRICK",
			78 => "ATT_FIRE_RESISTANCE_BRICK",
			79 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SET_VIEWED_IN_COMPONENT" => "N",
		"SHOW_404" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"SLIDER_INTERVAL" => "5000",
		"SLIDER_PROGRESS" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_COMMENTS" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_GIFTS_DETAIL" => "N",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_RATIO_IN_RANGES" => "N",
		"USE_VOTE_RATING" => "N",
		"COMPONENT_TEMPLATE" => "catalog-element2"
	),
	false
);?>
            </div>

        </div>

    </div>
</div>
<?$APPLICATION->SetPageProperty('title', 'Магма Керамик - Официальный сайт Дилера');?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
