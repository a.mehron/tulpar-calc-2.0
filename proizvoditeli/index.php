<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Производители");
?><div class="container">
	<div class="pt-3 pb-3">
		<div class="section brands">
			<div class="container p-sm-0">
				<div class="row title-and-all justify-content-between align-items-center">
					<div class="section-title">
						Производители<p style="font-size: 1rem;font-weight: 400;line-height: 1.5;margin-top: 1rem;">Мы являемся Официальными Дилерами и прямыми партнерами следующих производителей</p>
					</div>

				</div>
				<div class="row brand-logos">
					<div class="col-xl-2 col-lg-3 col-4 p-0 border">
 <a href="/proizvoditeli/alekseevskaya-keramika/"><img src="/local/templates/tulpar_store/assets/img/brands/alekseevsk.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border">
 <a href="/proizvoditeli/promix/"><img src="/local/templates/tulpar_store/assets/img/brands/promix.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border">
 <a href="/proizvoditeli/perel/"><img src="/local/templates/tulpar_store/assets/img/brands/perel.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
 <a href="/proizvoditeli/altair/"><img src="/local/templates/tulpar_store/assets/img/brands/altair.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
 <a href="/proizvoditeli/kirpichnyy-zavod-amstron-porikam/"><img src="/local/templates/tulpar_store/assets/img/brands/Amstron.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
 <a href="/proizvoditeli/arskiy-kirpichnyy-zavod-aspk/"><img src="/local/templates/tulpar_store/assets/img/brands/aspk.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
 <a href="/proizvoditeli/bikton/"><img src="/local/templates/tulpar_store/assets/img/brands/BikTon.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
 <a href="/proizvoditeli/vkz/"><img src="/local/templates/tulpar_store/assets/img/brands/bkz.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0">
 <a href="/proizvoditeli/fakro/"><img src="/local/templates/tulpar_store/assets/img/brands/Fakro.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/kazanskiy-kombinat-stroitelnykh-materialov/"><img src="/local/templates/tulpar_store/assets/img/brands/kazan.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/kc-kepamik/"><img src="/local/templates/tulpar_store/assets/img/brands/kc_keramik.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/shelangovskiy-kirpichnyy-zavod/"><img src="/local/templates/tulpar_store/assets/img/brands/keramika.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/kirpichnyy-zavod-ketra/"><img src="/local/templates/tulpar_store/assets/img/brands/ketra.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/klyuker"><img src="/local/templates/tulpar_store/assets/img/brands/kluker.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0">
 <a href="/proizvoditeli/koshchakovskiy-kirpichnyy-zavod/"><img src="/local/templates/tulpar_store/assets/img/brands/koshak.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/metall-profil/"><img src="/local/templates/tulpar_store/assets/img/brands/metall.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/profi-and-hobby/"><img src="/local/templates/tulpar_store/assets/img/brands/Profi.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/wienerberger/"><img src="/local/templates/tulpar_store/assets/img/brands/Wiener.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/osnovit/"><img src="/local/templates/tulpar_store/assets/img/brands/osnovit.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/starkhaus/"><img src="/local/templates/tulpar_store/assets/img/brands/starkhous.svg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-right-0">
 <a href="/proizvoditeli/chaykovskiy-kirpichnyy-zavod/"><img src="/local/templates/tulpar_store/assets/img/brands/chaika.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0">
 <a href="/proizvoditeli/izkm/"><img src="/local/templates/tulpar_store/assets/img/brands/logo_izkm.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0">
 <a href="/proizvoditeli/kamgeszyab/"><img src="/local/templates/tulpar_store/assets/img/brands/logo_zyab.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/mamadyshskiy/"><img src="/local/templates/tulpar_store/assets/img/brands/logo_makeram.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/bogdanovichskiy/"><img src="/local/templates/tulpar_store/assets/img/brands/bogdanovich.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/bund-bau/"><img src="/local/templates/tulpar_store/assets/img/brands/bund_bau2.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/cheboksarskaya-keramika/"><img src="/local/templates/tulpar_store/assets/img/brands/cheboksar.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/chelninskiy/"><img src="/local/templates/tulpar_store/assets/img/brands/chelninskiy2.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/kazanskiy-zavod-silikatnykh-stenovykh-materialov/"><img src="/local/templates/tulpar_store/assets/img/brands/kazanskiy-zavod-silikatnykh2.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/kirpich-kholding/"><img src="/local/templates/tulpar_store/assets/img/brands/kirpich_holding.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/kottedzh/"><img src="/local/templates/tulpar_store/assets/img/brands/kottedj.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/magma/"><img src="/local/templates/tulpar_store/assets/img/brands/magma.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/makhina-tst/"><img src="/local/templates/tulpar_store/assets/img/brands/mahina.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/steklonit/"><img src="/local/templates/tulpar_store/assets/img/brands/steklonit.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/stroitelnye-innovatsii"><img src="/local/templates/tulpar_store/assets/img/brands/stroitelnie_inovatsii2.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/teplon"><img src="/local/templates/tulpar_store/assets/img/brands/Teplon.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/uniblock"><img src="/local/templates/tulpar_store/assets/img/brands/uniblock.jpg" alt=""></a>
					</div>
					 <!--<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
                      <a href="/proizvoditeli/ventek/"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ventek.jpg" alt=""></a>
                  </div>-->
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/vilpe/"><img src="/local/templates/tulpar_store/assets/img/brands/vilpe.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/grand-line"><img src="/local/templates/tulpar_store/assets/img/brands/grandLine.jpeg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/belebeevskiy"><img src="/local/templates/tulpar_store/assets/img/brands/Белебеевский.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/tekhnonikol"><img src="/local/templates/tulpar_store/assets/img/brands/Технониколь.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
<a href="/proizvoditeli/kulonstroy/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/кулонстрой.jpeg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/кулонстрой.jpeg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/raff"><img src="/local/templates/tulpar_store/assets/img/brands/РАФФ.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/kerma"><img src="/local/templates/tulpar_store/assets/img/brands/керма.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/gras"><img src="/local/templates/tulpar_store/assets/img/brands/ГРАС.jpeg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/kerakam"><img src="/local/templates/tulpar_store/assets/img/brands/керакам.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/zyab-izhevsk"><img src="/local/templates/tulpar_store/assets/img/brands/лого зяб ижевск.png.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/mzksm"><img src="/local/templates/tulpar_store/assets/img/brands/logo мстера.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/build-stone"><img src="/local/templates/tulpar_store/assets/img/brands/build_stroy_logo.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/zsk"><img src="/local/templates/tulpar_store/assets/img/brands/ЗСК_Челны.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/durtuli"><img src="/local/templates/tulpar_store/assets/img/brands/durtuli.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/inkeram"><img src="/local/templates/tulpar_store/assets/img/brands/inkeram.jpg" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/meakir"><img src="/local/templates/tulpar_store/assets/img/brands/meakir.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/revdinski"><img src="/local/templates/tulpar_store/assets/img/brands/revdinski.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/yadrinsi"><img src="/local/templates/tulpar_store/assets/img/brands/yadrinsi.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/morgaushski"><img src="/local/templates/tulpar_store/assets/img/brands/morgaushski.png" alt=""></a>
					</div>
					<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/art-alyans"><img src="/local/templates/tulpar_store/assets/img/brands/logo-art-alyans.png" alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>