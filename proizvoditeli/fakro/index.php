<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Fakro");
?>



<div class="container p-0 brand-page">
    <div class="banner" style="background-image:url('<?=SITE_TEMPLATE_PATH?>/assets/img/brands/banners/факро.jpg')">
        <h1 class="title">Fakro</h1>
    </div>

    <div class="content-wrapper">
        <div class="navigation">
            <ul class="article-nav-list" id="brandpage-nav">
                <li class="article-nav-item">
                    <a href="#id-1">
                        <span class="article-nav__num">1</span>
                        <span class="article-nav__text">Видео о заводе</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-2">
                        <span class="article-nav__num">2</span>
                        <span class="article-nav__text">Информация о заводе</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-3">
                        <span class="article-nav__num">3</span>
                        <span class="article-nav__text">Видеообзор продукции завода</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-4">
                        <span class="article-nav__num">4</span>
                        <span class="article-nav__text">Каталог продукции завода</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-5">
                        <span class="article-nav__num">5</span>
                        <span class="article-nav__text">Подбор продукции завода</span>
                    </a>
                </li>

            </ul>
        </div>

        <div class="content">
            <h2>Видео о заводе</h2>
         

            <div class="info" id="id-2">
                <p><b>Fakro</b></p>
                <p>Компания Fakro уже давно завоевала популярность среди потребителей. Эта компания по праву считается одной из лучших, занимает лидирующие позиции по продажам. </p>
				<p>Основное направление деятельности компании — производство окон для крыши и мансард, чердачных лестниц, гидроизоляционных материалов и различных оконных аксессуаров. </p>
				<p>В производстве используется современное оборудование и качественные материалы, что позволяет выпускать только продукцию высокого качества, соответствующую всем нормам и требованиям. В производстве компания задействует современные подходы и инновации, поэтому рынок получает действительно уникальную продукцию. В точности вся продукция соответствует и всем нормам, требованиям. Есть множество уникальных разработок от этой компании, все они известны строительным компаниям, поэтому продукция и пользуется большим спросом. </p>
				<p>Для выбора нужной лестницы измерьте высоту комнаты. Есть варианты, которые подойдут идеально и предложенный размер будет соответствовать Вашим замерам, но если это не так - не печальтесь, лестницы Fakro можно подогнать под любую длину. Оцените пространство в комнате и представьте тип лестницы, который Вам подойдет. Это может быть складная, раздвижная модель из дерева или металла. Также есть модели с утепленным люком, для домов с холодным чердаком.</p>
				<p>Стоит также отметить, что несмотря на использование качественных материалов и современного оборудования, на уникальность свой продукции, компания имеет возможность сохранить вполне приемлемые цены на свою продукцию, что немаловажно, особенно при осуществлении оптовых закупок. </p>



            <div class="brand-catalog" id="id-4">
            <h2>Каталог продукции завода</h2>
            <?
            $APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"brand-products", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "PROP",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "CODE",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_COLOR",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "19",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => array(
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "858",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "brand-products",
		"ENLARGE_PROP" => "-",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_COLOR",
		)
	),
	false
);?>

            <a href="/catalog/lestnitsy-cherdachnye/" class="see-all-products">Посмотреть все товары</a>
            </div>

            <div class="brand-element" id="id-5">
                <h2>Подбор продукции завода</h2>
            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.element", 
	"catalog-element2", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_DETAIL_TO_SLIDER" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
		),
		"ADD_TO_BASKET_ACTION_PRIMARY" => array(
			0 => "BUY",
		),
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BRAND_USE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_SECTION_ID_VARIABLE" => "N",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_PICTURE_MODE" => array(
			0 => "POPUP",
		),
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "10709",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"IMAGE_RESOLUTION" => "16by9",
		"LABEL_PROP" => array(
		),
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"MAIN_BLOCK_OFFERS_PROPERTY_CODE" => array(
		),
		"MAIN_BLOCK_PROPERTY_CODE" => array(
		),
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_COMMENTS_TAB" => "Комментарии",
		"MESS_DESCRIPTION_TAB" => "Описание",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MESS_PRICE_RANGES_TITLE" => "Цены",
		"MESS_PROPERTIES_TAB" => "Характеристики",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_THICKNESS",
			1 => "ATT_COATING",
			2 => "ATT_COATING_CLASS_SKU",
			3 => "ATT_COLOR",
			4 => "ATT_GAZOBLOCK_HEIGHT_SKU",
			5 => "ATT_SNOWKEEPER_LENGTH_SKU",
			6 => "ATT_SIZE_BLOCKS_SKU",
			7 => "ATT_GAZOBLOCK_WIDTH_SKU",
			8 => "ATT_COATING_KIND_SKU",
			9 => "ATT_WARRANTY_YEARS_SKU",
			10 => "ATT_WOOD_WOODEN_STAIRS_SKU",
			11 => "ATT_DIRECTION_WOOD_STAIRS_SKU",
			12 => "PLAYGROUND_WOODEN_STAIRS_SKU",
			13 => "ATT_RISERS_WOOD_STAIRS_SKU",
			14 => "ATT_SERIES_SKU",
			15 => "ATT_MATERIAL_SKU",
			16 => "ATT_HANDRAIL_WOODEN_STAIRS_SKU",
			17 => "ATT_GLOSS_SKU",
			18 => "ATT_MIX_WEIGHT_SKU",
			19 => "ATT_BRICKS_WEIGHT_SKU",
			20 => "ATT_WATERABSORBANCE_BRICKS_SKU",
			21 => "ATT_WATERABSORBANCE_MIXES_SKU",
			22 => "MORE_PHOTO",
			23 => "ATT_SALT_FOG_PROTECTION_SKU",
			24 => "ATT_BRICK_CATEGORY",
			25 => "ATT_STRENGTH_CLASS_BLOCKS_SKU",
			26 => "ATT_GROUP_PACK_QTY_DOBORNIYE_SKU",
			27 => "ATT_TRUCK_QTY_BRICKS_SKU",
			28 => "ATT_BLOCK_PODDON_VOLUME",
			29 => "ATT_BRICKS_PODDIN_QTY_SKU",
			30 => "ATT_BLOCK_QTY_PER_M3",
			31 => "ATT_COROSION_RESISTANCE_SKU",
			32 => "ATT_BRICK_HEATCOND_COEF_SKU",
			33 => "ATT_MAX_TEMP_SKU",
			34 => "ATT_DENSITY_MARK_BLOCKS_SKU",
			35 => "ATT_COLDRESISTANCE_BRICK_SKU",
			36 => "ATT_VOLUME_SKU",
			37 => "ATT_VOLUME_ML_STROIT_HIMIYA_SKU",
			38 => "ATT_COLOR_TONE",
			39 => "ATT_BRICK_SURFACE",
			40 => "ATT_BEND_STRENGTH_SKU",
			41 => "ATT_UDAR_STRENGTH_SKU",
			42 => "ATT_PRODUCT_SIZE_MM_DOBORNIYE_SKU",
			43 => "ATT_INDIVIDUAL_PACK_SIZE_DOBORNIYE_SKU",
			44 => "ATT_RASTVOR_CONSUM_SKU",
			45 => "ATT_RASHOD_RASTVORA_M3_SKU",
			46 => "ATT_BRICKS_CONSUMPTION_SKU",
			47 => "ATT_UV_RESISTANCE_SKU",
			48 => "ATT_PENCIL_HARDNESS_SKU",
			49 => "ATT_MECHANIC_RESISTANCE",
			50 => "ATT_BRICK_FORMAT",
			51 => "ATT_COLOR_BRICK",
			52 => "ATT_COLOR_KROV_VENT_SPRAV_SKU",
			53 => "ATT_BRICK_COLOR_SKU_SPRAV",
			54 => "ATT_COLOR_MIXES_SKU",
			55 => "ATT_COLOR_FASTNESS",
			56 => "ATT_BRICK_STRENGTH_SKU",
			57 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_SERIES_SKU",
			1 => "ATT_WOOD_WOODEN_STAIRS_SKU",
			2 => "ATT_RISERS_WOOD_STAIRS_SKU",
			3 => "PLAYGROUND_WOODEN_STAIRS_SKU",
			4 => "ATT_DIRECTION_WOOD_STAIRS_SKU",
			5 => "ATT_MATERIAL_SKU",
			6 => "ATT_HANDRAIL_WOODEN_STAIRS_SKU",
		),
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
		"PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "ATT_COATING_CLASS",
			1 => "ATT_COATING_TYPE",
			2 => "ATT_WARRANTY_YEARS",
			3 => "ATT_COATING_KIND",
			4 => "ATT_ATTIC_STAIRCASE_INSULATED",
			5 => "ATT_HEIGHT_KROVLYA_VENTILATION",
			6 => "ATT_SNOWKEEPER_LENGTH",
			7 => "ATT_MATERIAL_ATTIC_STAIRS",
			8 => "ATT_MAKER",
			9 => "ATT_SIZE_BRICK",
			10 => "ATT_WIDTH_SETKA",
			11 => "ATT_D_W_V_STEPS",
			12 => "ATT_MANHOLE_COVER",
			13 => "ATT_OPENING_DIMENSIONS",
			14 => "ATT_COVER_THICKNESS",
			15 => "ATT_KIRPICH_TYPE",
			16 => "ATT_GLOSS",
			17 => "ATT_WEIGHT_MIXES",
			18 => "ATT_WEIGHT_KG",
			19 => "ATT_WATERABSORBANCE_BRICKS",
			20 => "ATT_WATERABSORBANCE_MIXES",
			21 => "ATT_PROFIL_HEIGHT_MM",
			22 => "ATT_DIAMETR_KROV_VENTILATION",
			23 => "ATT_SHEET_LENGTH_MM",
			24 => "ATT_RULON_LENGTH_WIDTH",
			25 => "ATT_LIFESPAN_MIXES",
			26 => "ATT_SALT_FOG_PROTECTION",
			27 => "ATT_STRENGTH_CLASS_BLOCKS",
			28 => "ATT_GROUP_PACK_QTY_DOBORNIYE",
			29 => "ATT_TRUCK_QTY_BRICKS",
			30 => "ATT_BLOCK_PODDON_VOLUME",
			31 => "ATT_PODDON_QTY",
			32 => "ATT_BLOCK_QTY_PER_M3",
			33 => "ATT_COROSION_RESISTANCE",
			34 => "ATT_WATERPROOF_CF",
			35 => "ATT_VAPOURPROOF_CF",
			36 => "ATT_HEAT_TRANSFER_COEF",
			37 => "ATT_BRICK_HEATCOND_COEF",
			38 => "ATT_MAX_TEMP",
			39 => "ATT_DENSITY_MARK_BLOCKS",
			40 => "ATT_MIN_KRATNOST_KREPEZH_SHT",
			41 => "ATT_COLDRESIST_MIXES",
			42 => "ATT_USAGE_MIXES",
			43 => "ATT_VOLUME",
			44 => "ATT_VOLUME_ML_STROIT_HIMIYA",
			45 => "ATT_SURFACE_DENSITY_SETKA",
			46 => "ATT_POROSITY_BRICK",
			47 => "ATT_STRENGHT_LIMIT_BRICK",
			48 => "ATT_COMPRESSIVE_STRENGTH_MIXES",
			49 => "ATT_BEND_STRENGTH",
			50 => "ATT_UDAR_STRENGTH",
			51 => "ATT_COHESIVE_STRENGTH_MIXES",
			52 => "ATT_CELL_SIZE_SETKA",
			53 => "ATT_PRODUCT_SIZE_MM_DOBORNIYE",
			54 => "ATT_INDIVIDUAL_PACK_SIZE_DOBORNIYE",
			55 => "ATT_RAZRYV_NAGRUZKA_SETKA",
			56 => "ATT_WATER_CONSUMPTION_MIXES",
			57 => "ATT_RASTVOR_CONSUM",
			58 => "ATT_RASHOD_RASTVORA_M3",
			59 => "ATT_MIX_USAGE_M2",
			60 => "ATT_BRICKS_CONSUMPTION",
			61 => "ATT_STORAGE_TIME_MIXES",
			62 => "ATT_SHIPPING_COST",
			63 => "ATT_UV_RESISTANCE",
			64 => "ATT_PENCIL_HARDNESS",
			65 => "ATT_USAGE_TEMP_MIXES",
			66 => "ATT_KROVLYA_TYPE_VENTILATION",
			67 => "ATT_THICKNESS_MM",
			68 => "ATT_UDLIN_PRI_RAZRYVE_SETKA",
			69 => "ATT_MECHANIC_RESISTANCE",
			70 => "ATT_BRICK_COLOR",
			71 => "ATT_COLOR_MIXES",
			72 => "ATT_COLOR_KROV_VENT_SPRAV",
			73 => "ATT_COLOR_MIXES_SPRAV",
			74 => "ATT_COLOR_STOYKOST",
			75 => "ATT_RULON_PRICE_SETKA",
			76 => "ATT_WAVE_INCR_MM",
			77 => "ATT_SHEET_WIDTH",
			78 => "ATT_PROFIL_WIDTH",
			79 => "ATT_BRICKS_FORMAT",
			80 => "ATT_COLOR_TONE_BRICK",
			81 => "ATT_SURFACE_BRICK",
			82 => "ATT_STRENGTH_MARK",
			83 => "ATT_COLDRESISTANCE_BRICK",
			84 => "ATT_FIRE_RESISTANCE_BRICK",
			85 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SET_VIEWED_IN_COMPONENT" => "N",
		"SHOW_404" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"SLIDER_INTERVAL" => "5000",
		"SLIDER_PROGRESS" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_COMMENTS" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_GIFTS_DETAIL" => "N",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_RATIO_IN_RANGES" => "N",
		"USE_VOTE_RATING" => "N",
		"COMPONENT_TEMPLATE" => "catalog-element2"
	),
	false
);?>
            </div>

        </div>

    </div>
</div>
<?$APPLICATION->SetPageProperty('title', 'Fakro - Официальный сайт Дилера');?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
