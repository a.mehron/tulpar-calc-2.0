<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("");
?>



<div class="container p-0 brand-page">
    <div class="banner" style="background-image:url('<?=SITE_TEMPLATE_PATH?>/assets/img/brands/banners/Profi_and_Hobby_gl.jpg')">
        <h1 class="title">Profi & Hobby</h1>
    </div>

    <div class="content-wrapper">
        <div class="navigation">
            <ul class="article-nav-list" id="brandpage-nav">
                <li class="article-nav-item">
                    <a href="#id-1">
                        <span class="article-nav__num">1</span>
                        <span class="article-nav__text">Видео о заводе</span>
                    </a>
                </li>

              <!--  <li class="article-nav-item">
                    <a href="#id-2">
                        <span class="article-nav__num">2</span>
                        <span class="article-nav__text">Информация о заводе</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-3">
                        <span class="article-nav__num">3</span>
                        <span class="article-nav__text">Видеообзор продукции завода</span>
                    </a>
                </li>-->

                <li class="article-nav-item">
                    <a href="#id-4">
                        <span class="article-nav__num">4</span>
                        <span class="article-nav__text">Каталог продукции завода</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-5">
                        <span class="article-nav__num">5</span>
                        <span class="article-nav__text">Подбор продукции завода</span>
                    </a>
                </li>

            </ul>
        </div>

        <div class="content">
            <h2>Видео о заводе</h2>
            <a class="fancybox-section" id="id-1" data-fancybox href="https://youtu.be/QGOR9_CKi18">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/banners/Profi_video.jpg" alt="">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/play-yellow.png" alt="" class="play-btn">
            </a>


            <!--<div class="info" id="id-2">
                <p><b>Алексеевский кирпичный завод (Алексеевская керамика)</b></p>
                <p>Более 20 лет Алексеевский кирпичный завод славится своим керамическим кирпичом, и его выпускаемая продукция известна далеко за пределами Татарстана. Технология производства основывается на применении красножгущейся кирпичной глины хорошего качества из карьеров Сахаровского, Алексеевского, Салмановского месторождений.</p>

                <p>
                    <span class="italics">Выпускаемая продукция:</span>
                    <ul>
                        <li>Облицовочный — одинарный и полуторный;</li>
                        <li>Керамический — рядовой;</li>
                        <li>Декоративный ручной формовки — кирпич под старину.</li>
                    </ul>
                </p>

                <p>
                    <span class="italics">Весь кирпич производится в строгом соответствии с ГОСТ:</span>
                    <ul>
                        <li>ГОСТ 530-2007. «Кирпичи и камни керамические лицевые. Технические условия».</li>
                        <li>ГОСТ 530-2012. «Кирпичи и керамические блоки. Технические условия».</li>
                    </ul>
                </p>

                <p>
                <span class="italics">Характеристики кирпича</span>
                    <ul>
                        <li>Марка прочности: М-150</li>
                        <li>Водопоглощение: 14%</li>
                        <li>Морозостойкость: 75 F</li>
                        <li>Вес: 1НФ — 2,3 кг; 1.4НФ — 3,3 кг</li>
                        <li>Коэффициент теплопроводности: 0,284 Вт/(м·°C)</li>
                    </ul>
                </p>

                <p>
                <span class="italics">Выпускаемые цвета</span>
                <table>
                    <tr>
                        <td>Красный</td>
                        <td>Красная глина + песок</td>
                    </tr>

                    <tr>
                        <td>Флеш обжиг</td>
                        <td>Красная глина + песок + флеш обжиг</td>
                    </tr>

                    <tr>
                        <td>Слоновая кость</td>
                        <td>Белая глина + песок</td>
                    </tr>
                </table>
                </p>

                <p>
                <span class="italics">Количество кирпича на поддоне</span>
                <table>
                    <thead class="italics">
                        <th>Формат</th>
                        <th>Цвет и поверхность</th>
                        <th>Количество на поддоне, шт.</th>
                    </thead>

                    <tr>
                        <td rowspan="2">1НФ</td>
                        <td>Слоновая кость все накатки</td>
                        <td>320</td>
                    </tr>

                    <tr>
                        <td>Бавария флеш-обжиг</td>
                        <td>318</td>
                    </tr>

                    <tr>
                        <td>1.4НФ</td>
                        <td>Все кирпичи</td>
                        <td>228</td>
                    </tr>

                    <div><span class="italics">Варианты упаковки:</span> кирпич упакован на поддоне в полипропиленовой ленте и термоусадочной пленке.</div>
                </table>
                </p>

                <p>Завод является одним из наших самых проверенных партнеров, в надежности которого мы уверены на 100%. Кирпич Алексеевского завода завоевал доверие не только частных застройщиков, но и крупных строительных организаций. Он позволяет воплощать в жизнь практически любые творческие замыслы, а возводимые сооружения отличаются качеством, прочностью и долговечностью.</p>

                <p>Благодаря тесному и долгому сотрудничеству с ОАО «Алексеевская Керамика», мы можем предложить своим клиентам довольно выгодные цены на продукцию завода. Обращайтесь, с нами вы легко построите дом вашей мечты!</p>

                <p>Дополнительную информацию можно получить у наших менеджеров по телефону: 8 (843) 216-32-47 или по адресу: г. Казань, ул. Даурская 12а, офис 20.4.</p>
            </div>


            <h2>Видеообзор продукции завода</h2>
            <a class="fancybox-section" id="id-3" data-fancybox href="https://www.youtube.com/watch?v=5RkaHVdNp2Q">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/banners/alex-keram-litsevoy.jpg" alt="">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/play-yellow.png" alt="" class="play-btn">
            </a>

            <br>

            <a class="fancybox-section" id="id-3" data-fancybox href="https://www.youtube.com/watch?v=aUAe9_GjBS8">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/banners/alex-keram-tsokol.jpg" alt="">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/play-yellow.png" alt="" class="play-btn">
            </a>
-->
            <div class="brand-catalog" id="id-4">
            <h2>Каталог продукции завода</h2>
            <?
            $APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"brand-products", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "PROP",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "CODE",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_COLOR",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "19",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => array(
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "857",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "brand-products",
		"ENLARGE_PROP" => "-",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_COLOR",
		)
	),
	false
);?>

            <a href="/catalog/mezhetazhnye-lestnitsy/" class="see-all-products">Посмотреть все товары</a>
            </div>

            <div class="brand-element" id="id-5">
                <h2>Подбор продукции завода</h2>
            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.element", 
	"catalog-element2", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_DETAIL_TO_SLIDER" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
		),
		"ADD_TO_BASKET_ACTION_PRIMARY" => array(
			0 => "BUY",
		),
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BRAND_USE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_SECTION_ID_VARIABLE" => "N",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_PICTURE_MODE" => array(
			0 => "POPUP",
		),
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "10828",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"IMAGE_RESOLUTION" => "16by9",
		"LABEL_PROP" => array(
		),
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"MAIN_BLOCK_OFFERS_PROPERTY_CODE" => array(
		),
		"MAIN_BLOCK_PROPERTY_CODE" => array(
		),
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_COMMENTS_TAB" => "Комментарии",
		"MESS_DESCRIPTION_TAB" => "Описание",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MESS_PRICE_RANGES_TITLE" => "Цены",
		"MESS_PROPERTIES_TAB" => "Характеристики",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_THICKNESS",
			1 => "ATT_COATING",
			2 => "ATT_COATING_CLASS_SKU",
			3 => "ATT_COLOR",
			4 => "ATT_GAZOBLOCK_HEIGHT_SKU",
			5 => "ATT_SNOWKEEPER_LENGTH_SKU",
			6 => "ATT_SIZE_BLOCKS_SKU",
			7 => "ATT_GAZOBLOCK_WIDTH_SKU",
			8 => "ATT_COATING_KIND_SKU",
			9 => "ATT_WARRANTY_YEARS_SKU",
			10 => "ATT_WOOD_WOODEN_STAIRS_SKU",
			11 => "ATT_DIRECTION_WOOD_STAIRS_SKU",
			12 => "PLAYGROUND_WOODEN_STAIRS_SKU",
			13 => "ATT_RISERS_WOOD_STAIRS_SKU",
			14 => "ATT_GLOSS_SKU",
			15 => "ATT_MIX_WEIGHT_SKU",
			16 => "ATT_BRICKS_WEIGHT_SKU",
			17 => "ATT_WATERABSORBANCE_BRICKS_SKU",
			18 => "ATT_WATERABSORBANCE_MIXES_SKU",
			19 => "MORE_PHOTO",
			20 => "ATT_SALT_FOG_PROTECTION_SKU",
			21 => "ATT_BRICK_CATEGORY",
			22 => "ATT_STRENGTH_CLASS_BLOCKS_SKU",
			23 => "ATT_GROUP_PACK_QTY_DOBORNIYE_SKU",
			24 => "ATT_TRUCK_QTY_BRICKS_SKU",
			25 => "ATT_BLOCK_PODDON_VOLUME",
			26 => "ATT_BRICKS_PODDIN_QTY_SKU",
			27 => "ATT_BLOCK_QTY_PER_M3",
			28 => "ATT_COROSION_RESISTANCE_SKU",
			29 => "ATT_BRICK_HEATCOND_COEF_SKU",
			30 => "ATT_MAX_TEMP_SKU",
			31 => "ATT_DENSITY_MARK_BLOCKS_SKU",
			32 => "ATT_COLDRESISTANCE_BRICK_SKU",
			33 => "ATT_VOLUME_SKU",
			34 => "ATT_VOLUME_ML_STROIT_HIMIYA_SKU",
			35 => "ATT_COLOR_TONE",
			36 => "ATT_BRICK_SURFACE",
			37 => "ATT_BEND_STRENGTH_SKU",
			38 => "ATT_UDAR_STRENGTH_SKU",
			39 => "ATT_PRODUCT_SIZE_MM_DOBORNIYE_SKU",
			40 => "ATT_INDIVIDUAL_PACK_SIZE_DOBORNIYE_SKU",
			41 => "ATT_RASTVOR_CONSUM_SKU",
			42 => "ATT_RASHOD_RASTVORA_M3_SKU",
			43 => "ATT_BRICKS_CONSUMPTION_SKU",
			44 => "ATT_UV_RESISTANCE_SKU",
			45 => "ATT_PENCIL_HARDNESS_SKU",
			46 => "ATT_MECHANIC_RESISTANCE",
			47 => "ATT_BRICK_FORMAT",
			48 => "ATT_COLOR_BRICK",
			49 => "ATT_COLOR_KROV_VENT_SPRAV_SKU",
			50 => "ATT_BRICK_COLOR_SKU_SPRAV",
			51 => "ATT_COLOR_MIXES_SKU",
			52 => "ATT_COLOR_FASTNESS",
			53 => "ATT_BRICK_STRENGTH_SKU",
			54 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_WOOD_WOODEN_STAIRS_SKU",
			1 => "ATT_RISERS_WOOD_STAIRS_SKU",
			2 => "PLAYGROUND_WOODEN_STAIRS_SKU",
			3 => "ATT_DIRECTION_WOOD_STAIRS_SKU",
			4 => "ATT_HANDRAIL_WOODEN_STAIRS_SKU",
		),
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
		"PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "ATT_COATING_CLASS",
			1 => "ATT_COATING_TYPE",
			2 => "ATT_WARRANTY_YEARS",
			3 => "ATT_COATING_KIND",
			4 => "ATT_WOOD_WOODEN_STAIRS",
			5 => "ATT_PRODUCT_VIEW_WOOD_STAIRS",
			6 => "ATT_HEIGHT_KROVLYA_VENTILATION",
			7 => "ATT_SNOWKEEPER_LENGTH",
			8 => "ATT_MATERIAL_ATTIC_STAIRS",
			9 => "ATT_MODEL_ATTIC_STAIRS",
			10 => "PLAYGROUND_WOODEN_STAIRS",
			11 => "ATT_RISERS_WOOD_STAIRS",
			12 => "ATT_MAKER",
			13 => "ATT_SIZE_BRICK",
			14 => "ATT_PRODUCT_CATEGORY_WOOD_STAIRS",
			15 => "ATT_THICKNESS_STEP_WOOD_STAIRS",
			16 => "ATT_MARCH_WIDTH_WOOD_STAIRS",
			17 => "ATT_WIDTH_SETKA",
			18 => "ATT_HEIGHT_WOOD_STAIRS",
			19 => "ATT_HEIGHT_STEPS_WOOD_STAIRS",
			20 => "ATT_NUMBER_BOX_WOOD_STAIRS",
			21 => "ATT_NUMBER_STEPS_WOOD_STAIRS",
			22 => "ATT_MINIMUM_OPENING_WOOD_STAIRS",
			23 => "ATT_DIRECTION_WOOD_STAIRS",
			24 => "ATT_TOTAL_WEIGHT_WOOD_STAIRS",
			25 => "ATT_OVERAL_VOLUME_WOOD_STAIRS",
			26 => "ATT_DIMENSIONS_PLAN_WOOD_STAIRS",
			27 => "ATT_SIZES_LARGEST_PLACE_WOOD_STAIRS",
			28 => "ATT_MANUFACTURER_COUNTRY",
			29 => "ATT_TILT_ANGLE_WOOD_STAIRS",
			30 => "ATT_MAX_LOAD",
			31 => "ATT_D_W_V_STEPS",
			32 => "ATT_MANHOLE_COVER",
			33 => "ATT_KIRPICH_TYPE",
			34 => "ATT_GLOSS",
			35 => "ATT_WEIGHT_MIXES",
			36 => "ATT_WEIGHT_KG",
			37 => "ATT_WATERABSORBANCE_BRICKS",
			38 => "ATT_WATERABSORBANCE_MIXES",
			39 => "ATT_PROFIL_HEIGHT_MM",
			40 => "ATT_DIAMETR_KROV_VENTILATION",
			41 => "ATT_SHEET_LENGTH_MM",
			42 => "ATT_RULON_LENGTH_WIDTH",
			43 => "ATT_LIFESPAN_MIXES",
			44 => "ATT_SALT_FOG_PROTECTION",
			45 => "ATT_STRENGTH_CLASS_BLOCKS",
			46 => "ATT_GROUP_PACK_QTY_DOBORNIYE",
			47 => "ATT_TRUCK_QTY_BRICKS",
			48 => "ATT_BLOCK_PODDON_VOLUME",
			49 => "ATT_PODDON_QTY",
			50 => "ATT_BLOCK_QTY_PER_M3",
			51 => "ATT_COROSION_RESISTANCE",
			52 => "ATT_WATERPROOF_CF",
			53 => "ATT_VAPOURPROOF_CF",
			54 => "ATT_HEAT_TRANSFER_COEF",
			55 => "ATT_BRICK_HEATCOND_COEF",
			56 => "ATT_MAX_TEMP",
			57 => "ATT_DENSITY_MARK_BLOCKS",
			58 => "ATT_MIN_KRATNOST_KREPEZH_SHT",
			59 => "ATT_COLDRESIST_MIXES",
			60 => "ATT_USAGE_MIXES",
			61 => "ATT_VOLUME",
			62 => "ATT_VOLUME_ML_STROIT_HIMIYA",
			63 => "ATT_SURFACE_DENSITY_SETKA",
			64 => "ATT_POROSITY_BRICK",
			65 => "ATT_STRENGHT_LIMIT_BRICK",
			66 => "ATT_COMPRESSIVE_STRENGTH_MIXES",
			67 => "ATT_BEND_STRENGTH",
			68 => "ATT_UDAR_STRENGTH",
			69 => "ATT_COHESIVE_STRENGTH_MIXES",
			70 => "ATT_CELL_SIZE_SETKA",
			71 => "ATT_PRODUCT_SIZE_MM_DOBORNIYE",
			72 => "ATT_INDIVIDUAL_PACK_SIZE_DOBORNIYE",
			73 => "ATT_RAZRYV_NAGRUZKA_SETKA",
			74 => "ATT_WATER_CONSUMPTION_MIXES",
			75 => "ATT_RASTVOR_CONSUM",
			76 => "ATT_RASHOD_RASTVORA_M3",
			77 => "ATT_MIX_USAGE_M2",
			78 => "ATT_BRICKS_CONSUMPTION",
			79 => "ATT_STORAGE_TIME_MIXES",
			80 => "ATT_SHIPPING_COST",
			81 => "ATT_UV_RESISTANCE",
			82 => "ATT_PENCIL_HARDNESS",
			83 => "ATT_USAGE_TEMP_MIXES",
			84 => "ATT_KROVLYA_TYPE_VENTILATION",
			85 => "ATT_THICKNESS_MM",
			86 => "ATT_UDLIN_PRI_RAZRYVE_SETKA",
			87 => "ATT_MECHANIC_RESISTANCE",
			88 => "ATT_BRICK_COLOR",
			89 => "ATT_COLOR_MIXES",
			90 => "ATT_COLOR_KROV_VENT_SPRAV",
			91 => "ATT_COLOR_MIXES_SPRAV",
			92 => "ATT_COLOR_STOYKOST",
			93 => "ATT_RULON_PRICE_SETKA",
			94 => "ATT_WAVE_INCR_MM",
			95 => "ATT_SHEET_WIDTH",
			96 => "ATT_PROFIL_WIDTH",
			97 => "ATT_BRICKS_FORMAT",
			98 => "ATT_COLOR_TONE_BRICK",
			99 => "ATT_SURFACE_BRICK",
			100 => "ATT_STRENGTH_MARK",
			101 => "ATT_COLDRESISTANCE_BRICK",
			102 => "ATT_FIRE_RESISTANCE_BRICK",
			103 => "ATT_CELLING_HEIGHT",
			104 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SET_VIEWED_IN_COMPONENT" => "N",
		"SHOW_404" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"SLIDER_INTERVAL" => "5000",
		"SLIDER_PROGRESS" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_COMMENTS" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_GIFTS_DETAIL" => "N",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_RATIO_IN_RANGES" => "N",
		"USE_VOTE_RATING" => "N",
		"COMPONENT_TEMPLATE" => "catalog-element2"
	),
	false
);?>
            </div>

        </div>

    </div>
</div>
<?$APPLICATION->SetPageProperty('title', 'Profi&Hobby');?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
