<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("ООО «Строительные Инновации» ");
?>



<div class="container p-0 brand-page">
    <div class="banner" style="background-image:url('<?=SITE_TEMPLATE_PATH?>/assets/img/brands/banners/строительные инновации.jpg')">
        <h1 class="title">ООО «Строительные Инновации» </h1>
    </div>

    <div class="content-wrapper">
        <div class="navigation">
            <ul class="article-nav-list" id="brandpage-nav">
                <li class="article-nav-item">
                    <a href="#id-1">
                        <span class="article-nav__num">1</span>
                        <span class="article-nav__text">Видео о заводе</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-2">
                        <span class="article-nav__num">2</span>
                        <span class="article-nav__text">Информация о заводе</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-3">
                        <span class="article-nav__num">3</span>
                        <span class="article-nav__text">Видеообзор продукции завода</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-4">
                        <span class="article-nav__num">4</span>
                        <span class="article-nav__text">Каталог продукции завода</span>
                    </a>
                </li>

                <li class="article-nav-item">
                    <a href="#id-5">
                        <span class="article-nav__num">5</span>
                        <span class="article-nav__text">Подбор продукции завода</span>
                    </a>
                </li>

            </ul>
        </div>

        <div class="content">
            <h2>Видео о заводе</h2>
 
            <div class="info" id="id-2">
                <p><b>ООО «Строительные Инновации» </b></p>
                <p>Если нужна проверенная и надежная компания по производству строительных материалов, ООО «Строительные Инновации» - это именно то, что необходимо. Строительные Инновации занимают лидирующие позиции по производству самых разнообразных стройматериалов, которые востребованы во всей стране. </p>
				<p>Компания начала свою деятельность в 2009 году — сравнительно недавно, но уже заслужила доверие и востребованность среди клиентов. Производственная мощность компании огромна и составляет примерно 6 млн гибкой связи и 35 млн. погонных метров арматуры в год. Также компания занимается производством композитной сетки и стеклопластиковой арматуры.  </p>
				<p>Продукция компании имеет высокое качество и степень надежности, предназначена она прежде всего для обеспечения надежности строящихся зданий и для увеличения сроков эксплуатации. Композитные гибкие связи, в том числе и с анкерными утолщениями — одно из направлений производства. Такие изделия широко востребованы на строительстве и производствах, их качеству уделяется особое внимание.  </p>
				<p>Компания тщательно следит за качеством своей продукции, производство оснащено современным оборудованием, используются инновационные технологии. При всем этом у компании есть возможность сохранить приемлемые цены для своих покупателей, а для постоянных клиентов предоставляются скидки.</p>
				<p>Продукции компании доверяют известные российские строительные компании.  </p>
                

            <div class="brand-catalog" id="id-4">
            <h2>Каталог продукции завода</h2>
            <?
            $APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"brand-products", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "sort",
		"ELEMENT_SORT_FIELD2" => "id",
		"ELEMENT_SORT_ORDER" => "asc",
		"ELEMENT_SORT_ORDER2" => "desc",
		"ENLARGE_PRODUCT" => "PROP",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "CODE",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_COLOR",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "19",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => array(
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "937",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "brand-products",
		"ENLARGE_PROP" => "-",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_COLOR",
		)
	),
	false
);?>

            <a href="/catalog/stroitelnaya-setka-po-proizvoditelyu-stroitelnye-innovatsii/" class="see-all-products">Посмотреть все товары</a>
            </div>

            <div class="brand-element" id="id-5">
                <h2>Подбор продукции завода</h2>
            <?$APPLICATION->IncludeComponent(
	"bitrix:catalog.element", 
	"catalog-element2", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_DETAIL_TO_SLIDER" => "N",
		"ADD_ELEMENT_CHAIN" => "N",
		"ADD_PICT_PROP" => "MORE_PHOTO",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => array(
			0 => "BUY",
		),
		"ADD_TO_BASKET_ACTION_PRIMARY" => array(
			0 => "BUY",
		),
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/basket.php",
		"BRAND_USE" => "N",
		"BROWSER_TITLE" => "-",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_SECTION_ID_VARIABLE" => "N",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"DETAIL_PICTURE_MODE" => array(
			0 => "POPUP",
		),
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PREVIEW_TEXT_MODE" => "E",
		"ELEMENT_CODE" => "",
		"ELEMENT_ID" => "12016",
		"GIFTS_DETAIL_BLOCK_TITLE" => "Выберите один из подарков",
		"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "Подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "Выберите один из товаров, чтобы получить подарок",
		"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
		"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
		"GIFTS_MESS_BTN_BUY" => "Выбрать",
		"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
		"GIFTS_SHOW_IMAGE" => "Y",
		"GIFTS_SHOW_NAME" => "Y",
		"GIFTS_SHOW_OLD_PRICE" => "Y",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"IMAGE_RESOLUTION" => "16by9",
		"LABEL_PROP" => array(
		),
		"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
		"LINK_IBLOCK_ID" => "",
		"LINK_IBLOCK_TYPE" => "",
		"LINK_PROPERTY_SID" => "",
		"MAIN_BLOCK_OFFERS_PROPERTY_CODE" => array(
		),
		"MAIN_BLOCK_PROPERTY_CODE" => array(
		),
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_COMMENTS_TAB" => "Комментарии",
		"MESS_DESCRIPTION_TAB" => "Описание",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"MESS_PRICE_RANGES_TITLE" => "Цены",
		"MESS_PROPERTIES_TAB" => "Характеристики",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "ID",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "0",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_THICKNESS",
			1 => "ATT_COATING",
			2 => "ATT_COATING_CLASS_SKU",
			3 => "ATT_COLOR",
			4 => "ATT_GAZOBLOCK_HEIGHT_SKU",
			5 => "ATT_SNOWKEEPER_LENGTH_SKU",
			6 => "ATT_SIZE_BLOCKS_SKU",
			7 => "ATT_GAZOBLOCK_WIDTH_SKU",
			8 => "ATT_GLOSS_SKU",
			9 => "ATT_MIX_WEIGHT_SKU",
			10 => "ATT_BRICKS_WEIGHT_SKU",
			11 => "ATT_COATING_KIND_SKU",
			12 => "ATT_WATERABSORBANCE_BRICKS_SKU",
			13 => "ATT_WATERABSORBANCE_MIXES_SKU",
			14 => "MORE_PHOTO",
			15 => "ATT_SALT_FOG_PROTECTION_SKU",
			16 => "ATT_BRICK_CATEGORY",
			17 => "ATT_STRENGTH_CLASS_BLOCKS_SKU",
			18 => "ATT_GROUP_PACK_QTY_DOBORNIYE_SKU",
			19 => "ATT_TRUCK_QTY_BRICKS_SKU",
			20 => "ATT_BLOCK_PODDON_VOLUME",
			21 => "ATT_BRICKS_PODDIN_QTY_SKU",
			22 => "ATT_BLOCK_QTY_PER_M3",
			23 => "ATT_COROSION_RESISTANCE_SKU",
			24 => "ATT_BRICK_HEATCOND_COEF_SKU",
			25 => "ATT_MAX_TEMP_SKU",
			26 => "ATT_DENSITY_MARK_BLOCKS_SKU",
			27 => "ATT_COLDRESISTANCE_BRICK_SKU",
			28 => "ATT_VOLUME_SKU",
			29 => "ATT_VOLUME_ML_STROIT_HIMIYA_SKU",
			30 => "ATT_COLOR_TONE",
			31 => "ATT_BRICK_SURFACE",
			32 => "ATT_BEND_STRENGTH_SKU",
			33 => "ATT_UDAR_STRENGTH_SKU",
			34 => "ATT_PRODUCT_SIZE_MM_DOBORNIYE_SKU",
			35 => "ATT_INDIVIDUAL_PACK_SIZE_DOBORNIYE_SKU",
			36 => "ATT_RASTVOR_CONSUM_SKU",
			37 => "ATT_RASHOD_RASTVORA_M3_SKU",
			38 => "ATT_BRICKS_CONSUMPTION_SKU",
			39 => "ATT_WARRANTY_YEARS_SKU",
			40 => "ATT_UV_RESISTANCE_SKU",
			41 => "ATT_PENCIL_HARDNESS_SKU",
			42 => "ATT_MECHANIC_RESISTANCE",
			43 => "ATT_BRICK_FORMAT",
			44 => "ATT_COLOR_BRICK",
			45 => "ATT_COLOR_KROV_VENT_SPRAV_SKU",
			46 => "ATT_BRICK_COLOR_SKU_SPRAV",
			47 => "ATT_COLOR_MIXES_SKU",
			48 => "ATT_COLOR_FASTNESS",
			49 => "ATT_BRICK_STRENGTH_SKU",
			50 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_GAZOBLOCK_WIDTH_SKU",
			1 => "ATT_GAZOBLOCK_HEIGHT_SKU",
			2 => "ATT_SNOWKEEPER_LENGTH_SKU",
			3 => "ATT_BRICK_FORMAT",
			4 => "ATT_COLOR_BRICK",
			5 => "ATT_BRICK_SURFACE",
		),
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRICE_VAT_SHOW_VALUE" => "N",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
		"PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_SUBSCRIPTION" => "N",
		"PROPERTY_CODE" => array(
			0 => "252",
			1 => "ATT_STAIRS_ON_APPLICATION",
			2 => "ATT_FOLD_WIDTH",
			3 => "ATT_FOLD_LENGTH",
			4 => "ATT_FOLD_THICKNESS_METAL_COATING",
			5 => "ATT_COATING_CLASS",
			6 => "ATT_COATING_TYPE",
			7 => "ATT_WARRANTY_YEARS",
			8 => "ATT_COATING_KIND",
			9 => "ATT_COATING_THICKNESS_METALTILE",
			10 => "ATT_WOOD_WOODEN_STAIRS",
			11 => "ATT_ZHB_PLATE_HEIGHT_MM",
			12 => "ATT_HEIGHT_KROVLYA_VENTILATION",
			13 => "ATT_SNOWKEEPER_LENGTH",
			14 => "ATT_MODEL_ATTIC_STAIRS",
			15 => "ATT_MAKER",
			16 => "ATT_SIZE_BRICK",
			17 => "ATT_MARCH_WIDTH_WOOD_STAIRS",
			18 => "ATT_WIDTH_SETKA",
			19 => "ATT_HEIGHT_WOOD_STAIRS",
			20 => "ATT_DIRECTION_WOOD_STAIRS",
			21 => "ATT_OPENING_DIMENSIONS",
			22 => "ATT_COVER_THICKNESS",
			23 => "ATT_KIRPICH_TYPE",
			24 => "ATT_GLOSS",
			25 => "ATT_WEIGHT_MIXES",
			26 => "ATT_ZHB_PLATE_WEIGHT_KG",
			27 => "ATT_WEIGHT_KG",
			28 => "ATT_WATERABSORBANCE_BRICKS",
			29 => "ATT_WATERABSORBANCE_MIXES",
			30 => "ATT_PROFIL_HEIGHT_MM",
			31 => "ATT_DIAMETER_FLEXIBLE_CONNECTION",
			32 => "ATT_DIAMETR_KROV_VENTILATION",
			33 => "ATT_SHEET_LENGTH_MM",
			34 => "ATT_RULON_LENGTH_WIDTH",
			35 => "ATT_ZHB_PLATE_LENGTH_MM",
			36 => "ATT_LIFESPAN_MIXES",
			37 => "ATT_SALT_FOG_PROTECTION",
			38 => "ATT_STRENGTH_CLASS_BLOCKS",
			39 => "ATT_GROUP_PACK_QTY_DOBORNIYE",
			40 => "ATT_TRUCK_QTY_BRICKS",
			41 => "ATT_BLOCK_PODDON_VOLUME",
			42 => "ATT_PODDON_QTY",
			43 => "ATT_BLOCK_QTY_PER_M3",
			44 => "ATT_COROSION_RESISTANCE",
			45 => "ATT_WATERPROOF_CF",
			46 => "ATT_VAPOURPROOF_CF",
			47 => "ATT_HEAT_TRANSFER_COEF",
			48 => "ATT_BRICK_HEATCOND_COEF",
			49 => "ATT_MAX_TEMP",
			50 => "ATT_DENSITY_MARK_BLOCKS",
			51 => "ATT_MATERIAL_VENTBOX",
			52 => "ATT_ZHB_METRAZH",
			53 => "ATT_MIN_KRATNOST_KREPEZH_SHT",
			54 => "ATT_COLDRESIST_MIXES",
			55 => "ATT_ZHB_PLATE_PRESSURE_KGS_M2",
			56 => "ATT_USAGE_MIXES",
			57 => "ATT_ZHB_PLATE_VOLUME_M3",
			58 => "ATT_VOLUME",
			59 => "ATT_VOLUME_ML_STROIT_HIMIYA",
			60 => "ATT_BLOCK_FIRE_RESISTANCE",
			61 => "ATT_BLOCK_RELEASE_MOISTURE",
			62 => "ATT_COLOR_TONE_VODOSTOCHNIE_SYSTEMY",
			63 => "ATT_SURFACE_DENSITY_SETKA",
			64 => "ATT_POROSITY_BRICK",
			65 => "ATT_STRENGHT_LIMIT_BRICK",
			66 => "ATT_BLOCK_COMPRESSIVE_STRENGHT",
			67 => "ATT_COMPRESSIVE_STRENGTH_MIXES",
			68 => "ATT_BEND_STRENGTH",
			69 => "ATT_UDAR_STRENGTH",
			70 => "ATT_COHESIVE_STRENGTH_MIXES",
			71 => "ATT_SIZE_VENTBOX",
			72 => "ATT_CELL_SIZE_SETKA",
			73 => "ATT_PRODUCT_SIZE_MM_DOBORNIYE",
			74 => "ATT_INDIVIDUAL_PACK_SIZE_DOBORNIYE",
			75 => "ATT_ZHB_PLATE_HOLE_SIZE_MM",
			76 => "ATT_RAZRYV_NAGRUZKA_SETKA",
			77 => "ATT_WATER_CONSUMPTION_MIXES",
			78 => "ATT_RASTVOR_CONSUM",
			79 => "ATT_RASHOD_RASTVORA_M3",
			80 => "ATT_MIX_USAGE_M2",
			81 => "ATT_BRICKS_CONSUMPTION",
			82 => "ATT_STORAGE_TIME_MIXES",
			83 => "ATT_SHIPPING_COST",
			84 => "ATT_UV_RESISTANCE",
			85 => "ATT_PENCIL_HARDNESS",
			86 => "ATT_USAGE_TEMP_MIXES",
			87 => "ATT_KROVLYA_TYPE_VENTILATION",
			88 => "ATT_THICKNESS_MM",
			89 => "ATT_MATERIAL_THICKNESS",
			90 => "ATT_UDLIN_PRI_RAZRYVE_SETKA",
			91 => "ATT_BLOCK_DRYING_SHRINKAGE",
			92 => "ATT_MECHANIC_RESISTANCE",
			93 => "ATT_BRICK_COLOR",
			94 => "ATT_COLOR_MIXES",
			95 => "ATT_COLOR_KROV_VENT_SPRAV",
			96 => "ATT_COLOR_MIXES_SPRAV",
			97 => "ATT_COLOR_STOYKOST",
			98 => "ATT_RULON_PRICE_SETKA",
			99 => "ATT_WAVE_INCR_MM",
			100 => "ATT_SHEET_WIDTH",
			101 => "ATT_PROFIL_WIDTH",
			102 => "ATT_BRICKS_FORMAT",
			103 => "ATT_COLOR_TONE_BRICK",
			104 => "ATT_SURFACE_BRICK",
			105 => "ATT_STRENGTH_MARK",
			106 => "ATT_COLDRESISTANCE_BRICK",
			107 => "ATT_FIRE_RESISTANCE_BRICK",
			108 => "",
		),
		"SECTION_CODE" => "",
		"SECTION_ID" => $_REQUEST["SECTION_ID"],
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_CANONICAL_URL" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SET_VIEWED_IN_COMPONENT" => "N",
		"SHOW_404" => "N",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DEACTIVATED" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "N",
		"SLIDER_INTERVAL" => "5000",
		"SLIDER_PROGRESS" => "N",
		"STRICT_SECTION_CHECK" => "N",
		"TEMPLATE_THEME" => "blue",
		"USE_COMMENTS" => "N",
		"USE_ELEMENT_COUNTER" => "Y",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_GIFTS_DETAIL" => "N",
		"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"USE_RATIO_IN_RANGES" => "N",
		"USE_VOTE_RATING" => "N",
		"COMPONENT_TEMPLATE" => "catalog-element2"
	),
	false
);?>
            </div>

        </div>

    </div>
</div>
<?$APPLICATION->SetPageProperty('title', 'ООО «Строительные Инновации»  - Официальный сайт Дилера');?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
