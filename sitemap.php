<?php
					use Bitrix\Main\Loader;
					require_once ($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
					if(!Loader::includeModule("sotbit.regions"))
					{
						return false;
					}
					$domain = new \Sotbit\Regions\Location\Domain();
					$domainCode = $domain->getProp("CODE");
					?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"><url><loc><?=$domainCode?>/sitemap-files.php</loc><lastmod>2020-11-25T09:09:31+03:00</lastmod></url><url><loc><?=$domainCode?>/sitemap-iblock-4.php</loc><lastmod>2020-11-25T09:09:35+03:00</lastmod></url><url><loc><?=$domainCode?>/sitemap-iblock-8.php</loc><lastmod>2020-11-25T09:09:35+03:00</lastmod></url></urlset>