<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Купить кирпич, строительный блок ".$_SESSION["SOTBIT_REGIONS"]["UF_IN_CITY"]." с доставкой по ценам производителя | \"Тулпар Трейд\"");
$sname = getSiteName();
$APPLICATION->SetPageProperty("keywords", "Интернет магазин, строительство, кирпичи, блоки, официальный сайт, г #SOTBIT_REGIONS_NAME#");
$APPLICATION->SetPageProperty("description", "На официальном сайте \"Тулпар Трейд\" найдете все необходимое для строительства: кирпичи, блоки, кровлю, лестницы ...");
$APPLICATION->SetTitle("Купить кирпич, строительный блок ".$_SESSION["SOTBIT_REGIONS"]["UF_IN_CITY"]." с доставкой по ценам производителя | \"Тулпар Трейд\"");
?><?
require_once $_SERVER['DOCUMENT_ROOT']."/local/templates/tulpar_store/includes/lib/Mobile_Detect.php";
$detect = new Mobile_Detect();
$isMobile = $detect->isMobile();
?>

<div class="section great-deals">
    <div class="container p-sm-0">
        <div class="row title-and-all justify-content-between align-items-center">
            <div class="section-title">Последние покупки</div>
            <a href="/catalog/vygodnye-predlozheniya/" class="all-offers border rounded p-2 pl-3 pr-5">
                Все последние покупки
            </a>
        </div>

        <?
        $APPLICATION->IncludeComponent(
	"bitrix:catalog.section", 
	"catalog-section-bestoffers", 
	array(
		"ACTION_VARIABLE" => "action",
		"ADD_PICT_PROP" => "-",
		"ADD_PROPERTIES_TO_BASKET" => "Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"ADD_TO_BASKET_ACTION" => "ADD",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"BACKGROUND_IMAGE" => "-",
		"BASKET_URL" => "/personal/cart/",
		"BROWSER_TITLE" => "-",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COMPATIBLE_MODE" => "Y",
		"CONVERT_CURRENCY" => "N",
		"CUSTOM_FILTER" => "",
		"DETAIL_URL" => "",
		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"DISPLAY_COMPARE" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"ELEMENT_SORT_FIELD" => "shows",
		"ELEMENT_SORT_FIELD2" => "sort",
		"ELEMENT_SORT_ORDER" => "desc",
		"ELEMENT_SORT_ORDER2" => "asc",
		"ENLARGE_PRODUCT" => "PROP",
		"FILTER_NAME" => "arrFilter",
		"HIDE_NOT_AVAILABLE" => "N",
		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SUBSECTIONS" => "Y",
		"LABEL_PROP" => array(
		),
		"LAZY_LOAD" => "N",
		"LINE_ELEMENT_COUNT" => "3",
		"LOAD_ON_SCROLL" => "N",
		"MESSAGE_404" => "",
		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
		"MESS_BTN_BUY" => "Купить",
		"MESS_BTN_DETAIL" => "Подробнее",
		"MESS_BTN_SUBSCRIBE" => "Подписаться",
		"MESS_NOT_AVAILABLE" => "Нет в наличии",
		"META_DESCRIPTION" => "-",
		"META_KEYWORDS" => "-",
		"OFFERS_CART_PROPERTIES" => array(
		),
		"OFFERS_FIELD_CODE" => array(
			0 => "CODE",
			1 => "NAME",
			2 => "",
		),
		"OFFERS_LIMIT" => "5",
		"OFFERS_PROPERTY_CODE" => array(
			0 => "ATT_COLOR",
			1 => "",
		),
		"OFFERS_SORT_FIELD" => "sort",
		"OFFERS_SORT_FIELD2" => "id",
		"OFFERS_SORT_ORDER" => "asc",
		"OFFERS_SORT_ORDER2" => "desc",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Товары",
		"PAGE_ELEMENT_COUNT" => "5",
		"PARTIAL_PRODUCT_PROPERTIES" => "N",
		"PRICE_CODE" => array(
			0 => "BASE",
		),
		"PRICE_VAT_INCLUDE" => "Y",
		"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
		"PRODUCT_DISPLAY_MODE" => "Y",
		"PRODUCT_ID_VARIABLE" => "id",
		"PRODUCT_PROPERTIES" => array(
		),
		"PRODUCT_PROPS_VARIABLE" => "prop",
		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
		"PRODUCT_SUBSCRIPTION" => "Y",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "",
		),
		"PROPERTY_CODE_MOBILE" => array(
		),
		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
		"RCM_TYPE" => "personal",
		"SECTION_CODE" => "",
		"SECTION_ID" => "622",
		"SECTION_ID_VARIABLE" => "SECTION_ID",
		"SECTION_URL" => "",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "",
		),
		"SEF_MODE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SHOW_ALL_WO_SECTION" => "Y",
		"SHOW_CLOSE_POPUP" => "N",
		"SHOW_DISCOUNT_PERCENT" => "N",
		"SHOW_FROM_SECTION" => "N",
		"SHOW_MAX_QUANTITY" => "N",
		"SHOW_OLD_PRICE" => "N",
		"SHOW_PRICE_COUNT" => "1",
		"SHOW_SLIDER" => "Y",
		"SLIDER_INTERVAL" => "3000",
		"SLIDER_PROGRESS" => "N",
		"USE_ENHANCED_ECOMMERCE" => "N",
		"USE_MAIN_ELEMENT_SECTION" => "N",
		"USE_PRICE_COUNT" => "N",
		"USE_PRODUCT_QUANTITY" => "N",
		"COMPONENT_TEMPLATE" => "catalog-section-bestoffers",
		"ENLARGE_PROP" => "-",
		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
		"OFFER_TREE_PROPS" => array(
			0 => "ATT_COLOR",
		),
		"TEMPLATE_THEME" => "blue"
	),
	false
);?>
    </div>
</div>

<div class="container p-0 mb-5">
	<h1 class="text-center mb-4">Интернет магазин строительных материалов Тулпар</h1>
    <div class="about-banner-main banner-main text-center">
         <a data-fancybox href="https://www.youtube.com/watch?v=-nfcl8Y-yDo">
            <?php if (!$isMobile): ?>
                <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/about.jpg"  data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/about.jpg' alt="">
            <?php else: ?>
                <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/about_mob.jpg" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/about_mob.jpg' alt="">
            <?php endif; ?>
        </a>
    </div>

    <div class="categories section">
      <div class="title-and-all">
        <div class="section-title">Каталог продукции</div>
      </div>
      <div class="row m-0">
          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/oblitsovochnyy/">
                <div class="image"><img class='lazy' data-src="/upload/iblock/15f/15f27d0507b8c126d77c5df1bf98c6c7.jpg" data-srcset="/upload/iblock/15f/15f27d0507b8c126d77c5df1bf98c6c7.jpg" alt=""></div>
                <span>Облицовочный кирпич</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/keramicheskiy-kirpich/">
                <div class="image"><img class="maw260 lazy" data-src="/upload/iblock/c7a/c7a55adafac867c1e828d5dfd55fa46b.jpg" data-srcset="/upload/iblock/c7a/c7a55adafac867c1e828d5dfd55fa46b.jpg" alt=""></div>
                <span>Керамический кирпич</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/stroitelnyy-kirpich/">
                <div class="image"><img class="maw180 lazy" data-src="/upload/iblock/d23/d2320fcb08a11df52aea2ec054782cfa.jpg" data-srcset="/upload/iblock/d23/d2320fcb08a11df52aea2ec054782cfa.jpg" alt=""></div>
                <span>Строительный кирпич</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/figurnyy-kirpich/">
                <div class="image"><img class="lazy" data-src="/upload/iblock/34f/34f43a6a75ab2c8687124e6b75a93adc.jpg" data-srcset="/upload/iblock/34f/34f43a6a75ab2c8687124e6b75a93adc.jpg" alt=""></div>
                <span>Фигурный кирпич</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/shamotnyy_kirpich/">
                <div class="image"><img class="lazy" data-src="/upload/iblock/d21/d21b7a5ea79641fb5887306930b3dad5.jpg" data-srcset="/upload/iblock/d21/d21b7a5ea79641fb5887306930b3dad5.jpg" alt=""></div>
                <span>Огнеупорный кирпич</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/krupnoformatnyy-keramicheskiy-blok/">
                <div class="image"><img class="lazy" data-src="/upload/iblock/e03/e035a8ac544f420f26e6d1abf8b1e850.jpg" data-srcset="/upload/iblock/e03/e035a8ac544f420f26e6d1abf8b1e850.jpg" alt=""></div>
                <span>Крупноформатный керамический блок</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/peregorodochnyy-keramicheskiy-blok/">
                <div class="image"><img class="lazy" data-src="/upload/iblock/4dd/4ddbebe843c6558e9e714499543ee44b.jpg" data-srcset="/upload/iblock/4dd/4ddbebe843c6558e9e714499543ee44b.jpg" alt=""></div>
                <span>Перегородочный керамический блок</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/keramicheskiy-kamen/">
                <div class="image vam"><img class="maw180 lazy" data-src="/upload/iblock/45a/45ac552ef3d4ed77ceaff543efbfb0ef.jpg" data-srcset="/upload/iblock/45a/45ac552ef3d4ed77ceaff543efbfb0ef.jpg" alt=""></div>
                <span>Керамический камень</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/krupnoformatnye-gazobetonnye-bloki/">
                <div class="image vam"><img class="lazy" data-src="/upload/iblock/b08/b0807f77fa5bcb720cdd1694433ca3f1.jpg" data-srcset="/upload/iblock/b08/b0807f77fa5bcb720cdd1694433ca3f1.jpg" alt=""></div>
                <span>Крупноформатный газобетонный блок</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/peregorodochnye-gazobetonnye-bloki/">
                <div class="image vam"><img class="lazy" data-src="/upload/iblock/95c/95c18d3dcfbb77ba237eb78407333947.jpg" data-srcset="/upload/iblock/95c/95c18d3dcfbb77ba237eb78407333947.jpg" alt=""></div>
                <span>Перегородочный газобетонный блок</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/sukhie-stroitelnye-smesi/">
                <div class="image vam"><img class="maw180 lazy" data-src="/upload/iblock/e82/e8292d29c899630923377f01f85050ea.jpg" data-srcset="/upload/iblock/e82/e8292d29c899630923377f01f85050ea.jpg" alt=""></div>
                <span>Сухие строительные смеси</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/stroitelnaya-setka/">
                <div class="image vam"><img class="lazy" data-src="/upload/iblock/ce9/ce9dddf663b7894d100b499592cfdd1f.jpeg" data-srcset="/upload/iblock/ce9/ce9dddf663b7894d100b499592cfdd1f.jpeg" alt=""></div>
                <span>Строительные сетки</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/metallocherepitsa/">
                <div class="image vam"><img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/mcherepica.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/mcherepica.jpg" alt=""></div>
                <span>Металлическая черепица</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/profnastil/">
                <div class="image vam"><img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/profnastil.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/profnastil.jpg" alt=""></div>
                <span>Профнастил</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/krovelnaya-ventilyatsiya/">
                <div class="image vam"><img class="lazy" data-src="/upload/iblock/32c/32c13f9d740f4f5d0e44b238acfe34b7.jpg" data-srcset="/upload/iblock/32c/32c13f9d740f4f5d0e44b238acfe34b7.jpg" alt=""></div>
                <span>Доборные элементы</span>
              </a>
          </div>

          <div class="col-lg-3 col-6 p-0 border category">
              <a href="/catalog/elementy-bezopasnosti-krovli/">
                <div class="image vam"><img class="maw140 lazy" data-src="/upload/iblock/cd6/cd6f5aff26a0bda397ca4e73a742061e.jpg" data-srcset="/upload/iblock/cd6/cd6f5aff26a0bda397ca4e73a742061e.jpg" alt=""></div>
                <span>Элементы безопасности кровли</span>
              </a>
          </div>


      </div>
    </div>

    <div id="articles-list-carousel">
        <?$APPLICATION->IncludeComponent(
        "bitrix:news.list",
        "articles-list-index",
        array(
            "ACTIVE_DATE_FORMAT" => "d.m.Y",
            "ADD_SECTIONS_CHAIN" => "Y",
            "AJAX_MODE" => "N",
            "AJAX_OPTION_ADDITIONAL" => "",
            "AJAX_OPTION_HISTORY" => "N",
            "AJAX_OPTION_JUMP" => "N",
            "AJAX_OPTION_STYLE" => "N",
            "CACHE_FILTER" => "N",
            "CACHE_GROUPS" => "Y",
            "CACHE_TIME" => "36000000",
            "CACHE_TYPE" => "A",
            "CHECK_DATES" => "Y",
            "DETAIL_URL" => "/articles/#ELEMENT_CODE#.html",
            "DISPLAY_BOTTOM_PAGER" => "N",
            "DISPLAY_DATE" => "Y",
            "DISPLAY_NAME" => "Y",
            "DISPLAY_PICTURE" => "Y",
            "DISPLAY_PREVIEW_TEXT" => "Y",
            "DISPLAY_TOP_PAGER" => "N",
            "FIELD_CODE" => array(
                0 => "",
                1 => "",
            ),
            "FILTER_NAME" => "arrFilter",
            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
            "IBLOCK_ID" => "8",
            "IBLOCK_TYPE" => "dynamic_content",
            "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
            "INCLUDE_SUBSECTIONS" => "Y",
            "MESSAGE_404" => "",
            "NEWS_COUNT" => "8",
            "PAGER_BASE_LINK_ENABLE" => "N",
            "PAGER_DESC_NUMBERING" => "N",
            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
            "PAGER_SHOW_ALL" => "N",
            "PAGER_SHOW_ALWAYS" => "N",
            "PAGER_TEMPLATE" => ".default",
            "PAGER_TITLE" => "Статьи",
            "PARENT_SECTION" => "",
            "PARENT_SECTION_CODE" => "",
            "PREVIEW_TRUNCATE_LEN" => "150",
            "PROPERTY_CODE" => array(
                0 => "",
                1 => "",
            ),
            "SET_BROWSER_TITLE" => "Y",
            "SET_LAST_MODIFIED" => "N",
            "SET_META_DESCRIPTION" => "Y",
            "SET_META_KEYWORDS" => "Y",
            "SET_STATUS_404" => "N",
            "SET_TITLE" => "N",
            "SHOW_404" => "N",
            "SORT_BY1" => "SORT",
            "SORT_BY2" => "SORT",
            "SORT_ORDER1" => "ASC",
            "SORT_ORDER2" => "ASC",
            "STRICT_SECTION_CHECK" => "N",
            "COMPONENT_TEMPLATE" => ".default"
        ),
        false
    );?>
    </div>
</div>

<div class="container p-0 mb-5">
    <div class="delivery-banner-main banner-main text-center">
        <a data-fancybox href="https://www.youtube.com/watch?v=u-w6BmrZKBs">
            <?php if (!$isMobile): ?>
                <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/delivery.jpg"  data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/delivery.jpg' alt="">
            <?php else: ?>
                <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/delivery_mob.jpg" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/delivery_mob.jpg' alt="">
            <?php endif; ?>
        </a>
    </div>

    <div id="delivery-consult">
        <h3><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/question.svg" alt="">Есть вопросы по доставке?</h2>
        <p>Оставьте, пожалуйста, ваши контактные данные, и наш менеджер <br> свяжется с вами в ближайшее время и ответит на все интересующие вас вопросы</p>
		<form>
			<input type="text" name="delivery_name" required placeholder="Ваше имя*" value="">
			<input type="tel" name="delivery_phone" required placeholder="Ваш телефон*" value="">
			<input type="submit" name="" value="Получить консультацию">
		</form>
		<div class="image">
			<img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/truck.png" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/truck.png' alt="">
		</div>
    </div>
</div>

<div class="container p-0 mb-5">
    <div class="payment-banner-main banner-main text-center">
        <a href="https://www.youtube.com/watch?v=k-EwioaAW4s/" data-fancybox>
            <?php if (!$isMobile): ?>
                <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/payment.jpg" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/payment.jpg' alt="">
            <?php else: ?>
                <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/payment_mob.jpg" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/payment_mob.jpg' alt="">
            <?php endif; ?>
        </a>
    </div>

    <div id="delivery-consult" class="help">
        <h3> <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/help.svg" alt="">Нужна помощь в выборе материала?</h2>
        <p>Оставьте, пожалуйста, ваши контактные данные, и наш менеджер <br> свяжется с вами в ближайшее время и ответит на все интересующие вас вопросы</p>
        <form>
            <input type="text" name="help_name" required placeholder="Ваше имя*" value="">
            <input type="tel" name="help_phone" required placeholder="Ваш телефон*" value="">
            <input type="submit" name="" value="Получить консультацию">
        </form>
        <div class="image">
            <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/guy_3.png" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/guy_3.png' alt="">
        </div>
    </div>
</div>


<div class="container p-0">
    <div class="credit-banner-main banner-main text-center">
        <a href="/credit/">
            <?php if (!$isMobile): ?>
                <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/tinkoff.jpg" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/tinkoff.jpg' alt="">
            <?php else: ?>
                <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/tinkof_mob.jpg" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/tinkof_mob.jpg' alt="">
            <?php endif; ?>
        </a>
    </div>
</div>


<div class="section great-deals">
    <div class="container p-sm-0">
        <div class="row title-and-all justify-content-between align-items-center">
            <div class="section-title">Облицовочный кирпич</div>
            <a href="/catalog/oblitsovochnyy/" class="all-offers border rounded p-2 pl-3 pr-5">
                Все облицовочные кирпичи
            </a>
        </div>

        <?
        $APPLICATION->IncludeComponent(
        	"bitrix:catalog.section",
        	"catalog-section-bestoffers",
        	array(
        		"ACTION_VARIABLE" => "action",
        		"ADD_PICT_PROP" => "-",
        		"ADD_PROPERTIES_TO_BASKET" => "Y",
        		"ADD_SECTIONS_CHAIN" => "N",
        		"ADD_TO_BASKET_ACTION" => "ADD",
        		"AJAX_MODE" => "N",
        		"AJAX_OPTION_ADDITIONAL" => "",
        		"AJAX_OPTION_HISTORY" => "N",
        		"AJAX_OPTION_JUMP" => "N",
        		"AJAX_OPTION_STYLE" => "Y",
        		"BACKGROUND_IMAGE" => "-",
        		"BASKET_URL" => "/personal/cart/",
        		"BROWSER_TITLE" => "-",
        		"CACHE_FILTER" => "N",
        		"CACHE_GROUPS" => "Y",
        		"CACHE_TIME" => "36000000",
        		"CACHE_TYPE" => "A",
        		"COMPATIBLE_MODE" => "Y",
        		"CONVERT_CURRENCY" => "N",
        		"CUSTOM_FILTER" => "",
        		"DETAIL_URL" => "",
        		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
        		"DISPLAY_BOTTOM_PAGER" => "N",
        		"DISPLAY_COMPARE" => "N",
        		"DISPLAY_TOP_PAGER" => "N",
        		"ELEMENT_SORT_FIELD" => "sort",
        		"ELEMENT_SORT_FIELD2" => "id",
        		"ELEMENT_SORT_ORDER" => "asc",
        		"ELEMENT_SORT_ORDER2" => "desc",
        		"ENLARGE_PRODUCT" => "PROP",
        		"FILTER_NAME" => "arrFilter",
        		"HIDE_NOT_AVAILABLE" => "N",
        		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
        		"IBLOCK_ID" => "4",
        		"IBLOCK_TYPE" => "catalog",
        		"INCLUDE_SUBSECTIONS" => "N",
        		"LABEL_PROP" => array(
        		),
        		"LAZY_LOAD" => "N",
        		"LINE_ELEMENT_COUNT" => "3",
        		"LOAD_ON_SCROLL" => "N",
        		"MESSAGE_404" => "",
        		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
        		"MESS_BTN_BUY" => "Купить",
        		"MESS_BTN_DETAIL" => "Подробнее",
        		"MESS_BTN_SUBSCRIBE" => "Подписаться",
        		"MESS_NOT_AVAILABLE" => "Нет в наличии",
        		"META_DESCRIPTION" => "-",
        		"META_KEYWORDS" => "-",
        		"OFFERS_CART_PROPERTIES" => array(
        		),
        		"OFFERS_FIELD_CODE" => array(
        			0 => "CODE",
        			1 => "NAME",
        			2 => "",
        		),
        		"OFFERS_LIMIT" => "5",
        		"OFFERS_PROPERTY_CODE" => array(
        			0 => "ATT_COLOR",
        			1 => "",
        		),
        		"OFFERS_SORT_FIELD" => "sort",
        		"OFFERS_SORT_FIELD2" => "id",
        		"OFFERS_SORT_ORDER" => "asc",
        		"OFFERS_SORT_ORDER2" => "desc",
        		"PAGER_BASE_LINK_ENABLE" => "N",
        		"PAGER_DESC_NUMBERING" => "N",
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        		"PAGER_SHOW_ALL" => "N",
        		"PAGER_SHOW_ALWAYS" => "N",
        		"PAGER_TEMPLATE" => ".default",
        		"PAGER_TITLE" => "Товары",
        		"PAGE_ELEMENT_COUNT" => "5",
        		"PARTIAL_PRODUCT_PROPERTIES" => "N",
        		"PRICE_CODE" => array(
        			0 => "BASE",
        		),
        		"PRICE_VAT_INCLUDE" => "Y",
        		"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
        		"PRODUCT_DISPLAY_MODE" => "Y",
        		"PRODUCT_ID_VARIABLE" => "id",
        		"PRODUCT_PROPERTIES" => array(
        		),
        		"PRODUCT_PROPS_VARIABLE" => "prop",
        		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
        		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
        		"PRODUCT_SUBSCRIPTION" => "Y",
        		"PROPERTY_CODE" => array(
        			0 => "",
        			1 => "",
        		),
        		"PROPERTY_CODE_MOBILE" => array(
        		),
        		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
        		"RCM_TYPE" => "personal",
        		"SECTION_CODE" => "",
        		"SECTION_ID" => "21",
        		"SECTION_ID_VARIABLE" => "SECTION_ID",
        		"SECTION_URL" => "",
        		"SECTION_USER_FIELDS" => array(
        			0 => "",
        			1 => "",
        		),
        		"SEF_MODE" => "N",
        		"SET_BROWSER_TITLE" => "N",
        		"SET_LAST_MODIFIED" => "N",
        		"SET_META_DESCRIPTION" => "N",
        		"SET_META_KEYWORDS" => "N",
        		"SET_STATUS_404" => "N",
        		"SET_TITLE" => "N",
        		"SHOW_404" => "N",
        		"SHOW_ALL_WO_SECTION" => "Y",
        		"SHOW_CLOSE_POPUP" => "N",
        		"SHOW_DISCOUNT_PERCENT" => "N",
        		"SHOW_FROM_SECTION" => "N",
        		"SHOW_MAX_QUANTITY" => "N",
        		"SHOW_OLD_PRICE" => "N",
        		"SHOW_PRICE_COUNT" => "1",
        		"SHOW_SLIDER" => "N",
        		"SLIDER_INTERVAL" => "3000",
        		"SLIDER_PROGRESS" => "N",
        		//"TEMPLATE_THEME" => "blue",
        		"USE_ENHANCED_ECOMMERCE" => "N",
        		"USE_MAIN_ELEMENT_SECTION" => "N",
        		"USE_PRICE_COUNT" => "N",
        		"USE_PRODUCT_QUANTITY" => "N",
        		"COMPONENT_TEMPLATE" => ".default2",
        		"ENLARGE_PROP" => "-",
        		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
        		"OFFER_TREE_PROPS" => array(
        			0 => "ATT_COLOR",
        		)
        	),
        	false
        );?>
    </div>
</div>


<div class="section great-deals">
    <div class="container p-sm-0">
        <div class="row title-and-all justify-content-between align-items-center">
            <div class="section-title">Строительный кирпич</div>
            <a href="/catalog/stroitelnyy-kirpich/" class="all-offers border rounded p-2 pl-3 pr-5">
                Все строительные кирпичи
            </a>
        </div>

        <?
        $APPLICATION->IncludeComponent(
        	"bitrix:catalog.section",
        	"catalog-section-bestoffers",
        	array(
        		"ACTION_VARIABLE" => "action",
        		"ADD_PICT_PROP" => "-",
        		"ADD_PROPERTIES_TO_BASKET" => "Y",
        		"ADD_SECTIONS_CHAIN" => "N",
        		"ADD_TO_BASKET_ACTION" => "ADD",
        		"AJAX_MODE" => "N",
        		"AJAX_OPTION_ADDITIONAL" => "",
        		"AJAX_OPTION_HISTORY" => "N",
        		"AJAX_OPTION_JUMP" => "N",
        		"AJAX_OPTION_STYLE" => "Y",
        		"BACKGROUND_IMAGE" => "-",
        		"BASKET_URL" => "/personal/cart/",
        		"BROWSER_TITLE" => "-",
        		"CACHE_FILTER" => "N",
        		"CACHE_GROUPS" => "Y",
        		"CACHE_TIME" => "36000000",
        		"CACHE_TYPE" => "A",
        		"COMPATIBLE_MODE" => "Y",
        		"CONVERT_CURRENCY" => "N",
        		"CUSTOM_FILTER" => "",
        		"DETAIL_URL" => "",
        		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
        		"DISPLAY_BOTTOM_PAGER" => "N",
        		"DISPLAY_COMPARE" => "N",
        		"DISPLAY_TOP_PAGER" => "N",
        		"ELEMENT_SORT_FIELD" => "sort",
        		"ELEMENT_SORT_FIELD2" => "id",
        		"ELEMENT_SORT_ORDER" => "asc",
        		"ELEMENT_SORT_ORDER2" => "desc",
        		"ENLARGE_PRODUCT" => "PROP",
        		"FILTER_NAME" => "arrFilter",
        		"HIDE_NOT_AVAILABLE" => "N",
        		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
        		"IBLOCK_ID" => "4",
        		"IBLOCK_TYPE" => "catalog",
        		"INCLUDE_SUBSECTIONS" => "N",
        		"LABEL_PROP" => array(
        		),
        		"LAZY_LOAD" => "N",
        		"LINE_ELEMENT_COUNT" => "3",
        		"LOAD_ON_SCROLL" => "N",
        		"MESSAGE_404" => "",
        		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
        		"MESS_BTN_BUY" => "Купить",
        		"MESS_BTN_DETAIL" => "Подробнее",
        		"MESS_BTN_SUBSCRIBE" => "Подписаться",
        		"MESS_NOT_AVAILABLE" => "Нет в наличии",
        		"META_DESCRIPTION" => "-",
        		"META_KEYWORDS" => "-",
        		"OFFERS_CART_PROPERTIES" => array(
        		),
        		"OFFERS_FIELD_CODE" => array(
        			0 => "CODE",
        			1 => "NAME",
        			2 => "",
        		),
        		"OFFERS_LIMIT" => "5",
        		"OFFERS_PROPERTY_CODE" => array(
        			0 => "ATT_COLOR",
        			1 => "",
        		),
        		"OFFERS_SORT_FIELD" => "sort",
        		"OFFERS_SORT_FIELD2" => "id",
        		"OFFERS_SORT_ORDER" => "asc",
        		"OFFERS_SORT_ORDER2" => "desc",
        		"PAGER_BASE_LINK_ENABLE" => "N",
        		"PAGER_DESC_NUMBERING" => "N",
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        		"PAGER_SHOW_ALL" => "N",
        		"PAGER_SHOW_ALWAYS" => "N",
        		"PAGER_TEMPLATE" => ".default",
        		"PAGER_TITLE" => "Товары",
        		"PAGE_ELEMENT_COUNT" => "5",
        		"PARTIAL_PRODUCT_PROPERTIES" => "N",
        		"PRICE_CODE" => array(
        			0 => "BASE",
        		),
        		"PRICE_VAT_INCLUDE" => "Y",
        		"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
        		"PRODUCT_DISPLAY_MODE" => "Y",
        		"PRODUCT_ID_VARIABLE" => "id",
        		"PRODUCT_PROPERTIES" => array(
        		),
        		"PRODUCT_PROPS_VARIABLE" => "prop",
        		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
        		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
        		"PRODUCT_SUBSCRIPTION" => "Y",
        		"PROPERTY_CODE" => array(
        			0 => "",
        			1 => "",
        		),
        		"PROPERTY_CODE_MOBILE" => array(
        		),
        		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
        		"RCM_TYPE" => "personal",
        		"SECTION_CODE" => "",
        		"SECTION_ID" => "146",
        		"SECTION_ID_VARIABLE" => "SECTION_ID",
        		"SECTION_URL" => "",
        		"SECTION_USER_FIELDS" => array(
        			0 => "",
        			1 => "",
        		),
        		"SEF_MODE" => "N",
        		"SET_BROWSER_TITLE" => "N",
        		"SET_LAST_MODIFIED" => "N",
        		"SET_META_DESCRIPTION" => "N",
        		"SET_META_KEYWORDS" => "N",
        		"SET_STATUS_404" => "N",
        		"SET_TITLE" => "N",
        		"SHOW_404" => "N",
        		"SHOW_ALL_WO_SECTION" => "Y",
        		"SHOW_CLOSE_POPUP" => "N",
        		"SHOW_DISCOUNT_PERCENT" => "N",
        		"SHOW_FROM_SECTION" => "N",
        		"SHOW_MAX_QUANTITY" => "N",
        		"SHOW_OLD_PRICE" => "N",
        		"SHOW_PRICE_COUNT" => "1",
        		"SHOW_SLIDER" => "N",
        		"SLIDER_INTERVAL" => "3000",
        		"SLIDER_PROGRESS" => "N",
        		//"TEMPLATE_THEME" => "blue",
        		"USE_ENHANCED_ECOMMERCE" => "N",
        		"USE_MAIN_ELEMENT_SECTION" => "N",
        		"USE_PRICE_COUNT" => "N",
        		"USE_PRODUCT_QUANTITY" => "N",
        		"COMPONENT_TEMPLATE" => ".default2",
        		"ENLARGE_PROP" => "-",
        		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
        		"OFFER_TREE_PROPS" => array(
        			0 => "ATT_COLOR",
        		)
        	),
        	false
        );?>
    </div>
</div>

<div class="section great-deals">
    <div class="container p-sm-0">
        <div class="row title-and-all justify-content-between align-items-center">
            <div class="section-title">Керамические блоки</div>
            <a href="/catalog/keramicheskie-bloki/" class="all-offers border rounded p-2 pl-3 pr-5">
                Все керамические блоки
            </a>
        </div>

        <?
        $APPLICATION->IncludeComponent(
        	"bitrix:catalog.section",
        	"catalog-section-bestoffers",
        	array(
        		"ACTION_VARIABLE" => "action",
        		"ADD_PICT_PROP" => "-",
        		"ADD_PROPERTIES_TO_BASKET" => "Y",
        		"ADD_SECTIONS_CHAIN" => "N",
        		"ADD_TO_BASKET_ACTION" => "ADD",
        		"AJAX_MODE" => "N",
        		"AJAX_OPTION_ADDITIONAL" => "",
        		"AJAX_OPTION_HISTORY" => "N",
        		"AJAX_OPTION_JUMP" => "N",
        		"AJAX_OPTION_STYLE" => "Y",
        		"BACKGROUND_IMAGE" => "-",
        		"BASKET_URL" => "/personal/cart/",
        		"BROWSER_TITLE" => "-",
        		"CACHE_FILTER" => "N",
        		"CACHE_GROUPS" => "Y",
        		"CACHE_TIME" => "36000000",
        		"CACHE_TYPE" => "A",
        		"COMPATIBLE_MODE" => "Y",
        		"CONVERT_CURRENCY" => "N",
        		"CUSTOM_FILTER" => "",
        		"DETAIL_URL" => "",
        		"DISABLE_INIT_JS_IN_COMPONENT" => "N",
        		"DISPLAY_BOTTOM_PAGER" => "N",
        		"DISPLAY_COMPARE" => "N",
        		"DISPLAY_TOP_PAGER" => "N",
        		"ELEMENT_SORT_FIELD" => "sort",
        		"ELEMENT_SORT_FIELD2" => "id",
        		"ELEMENT_SORT_ORDER" => "asc",
        		"ELEMENT_SORT_ORDER2" => "desc",
        		"ENLARGE_PRODUCT" => "PROP",
        		"FILTER_NAME" => "arrFilter",
        		"HIDE_NOT_AVAILABLE" => "N",
        		"HIDE_NOT_AVAILABLE_OFFERS" => "N",
        		"IBLOCK_ID" => "4",
        		"IBLOCK_TYPE" => "catalog",
        		"INCLUDE_SUBSECTIONS" => "N",
        		"LABEL_PROP" => array(
        		),
        		"LAZY_LOAD" => "N",
        		"LINE_ELEMENT_COUNT" => "3",
        		"LOAD_ON_SCROLL" => "N",
        		"MESSAGE_404" => "",
        		"MESS_BTN_ADD_TO_BASKET" => "В корзину",
        		"MESS_BTN_BUY" => "Купить",
        		"MESS_BTN_DETAIL" => "Подробнее",
        		"MESS_BTN_SUBSCRIBE" => "Подписаться",
        		"MESS_NOT_AVAILABLE" => "Нет в наличии",
        		"META_DESCRIPTION" => "-",
        		"META_KEYWORDS" => "-",
        		"OFFERS_CART_PROPERTIES" => array(
        		),
        		"OFFERS_FIELD_CODE" => array(
        			0 => "CODE",
        			1 => "NAME",
        			2 => "",
        		),
        		"OFFERS_LIMIT" => "5",
        		"OFFERS_PROPERTY_CODE" => array(
        			0 => "ATT_COLOR",
        			1 => "",
        		),
        		"OFFERS_SORT_FIELD" => "sort",
        		"OFFERS_SORT_FIELD2" => "id",
        		"OFFERS_SORT_ORDER" => "asc",
        		"OFFERS_SORT_ORDER2" => "desc",
        		"PAGER_BASE_LINK_ENABLE" => "N",
        		"PAGER_DESC_NUMBERING" => "N",
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        		"PAGER_SHOW_ALL" => "N",
        		"PAGER_SHOW_ALWAYS" => "N",
        		"PAGER_TEMPLATE" => ".default",
        		"PAGER_TITLE" => "Товары",
        		"PAGE_ELEMENT_COUNT" => "5",
        		"PARTIAL_PRODUCT_PROPERTIES" => "N",
        		"PRICE_CODE" => array(
        			0 => "BASE",
        		),
        		"PRICE_VAT_INCLUDE" => "Y",
        		"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
        		"PRODUCT_DISPLAY_MODE" => "Y",
        		"PRODUCT_ID_VARIABLE" => "id",
        		"PRODUCT_PROPERTIES" => array(
        		),
        		"PRODUCT_PROPS_VARIABLE" => "prop",
        		"PRODUCT_QUANTITY_VARIABLE" => "quantity",
        		"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
        		"PRODUCT_SUBSCRIPTION" => "Y",
        		"PROPERTY_CODE" => array(
        			0 => "",
        			1 => "",
        		),
        		"PROPERTY_CODE_MOBILE" => array(
        		),
        		"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
        		"RCM_TYPE" => "personal",
        		"SECTION_CODE" => "",
        		"SECTION_ID" => "223",
        		"SECTION_ID_VARIABLE" => "SECTION_ID",
        		"SECTION_URL" => "",
        		"SECTION_USER_FIELDS" => array(
        			0 => "",
        			1 => "",
        		),
        		"SEF_MODE" => "N",
        		"SET_BROWSER_TITLE" => "N",
        		"SET_LAST_MODIFIED" => "N",
        		"SET_META_DESCRIPTION" => "N",
        		"SET_META_KEYWORDS" => "N",
        		"SET_STATUS_404" => "N",
        		"SET_TITLE" => "N",
        		"SHOW_404" => "N",
        		"SHOW_ALL_WO_SECTION" => "Y",
        		"SHOW_CLOSE_POPUP" => "N",
        		"SHOW_DISCOUNT_PERCENT" => "N",
        		"SHOW_FROM_SECTION" => "N",
        		"SHOW_MAX_QUANTITY" => "N",
        		"SHOW_OLD_PRICE" => "N",
        		"SHOW_PRICE_COUNT" => "1",
        		"SHOW_SLIDER" => "N",
        		"SLIDER_INTERVAL" => "3000",
        		"SLIDER_PROGRESS" => "N",
        		//"TEMPLATE_THEME" => "blue",
        		"USE_ENHANCED_ECOMMERCE" => "N",
        		"USE_MAIN_ELEMENT_SECTION" => "N",
        		"USE_PRICE_COUNT" => "N",
        		"USE_PRODUCT_QUANTITY" => "N",
        		"COMPONENT_TEMPLATE" => ".default2",
        		"ENLARGE_PROP" => "-",
        		"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
        		"OFFER_TREE_PROPS" => array(
        			0 => "ATT_COLOR",
        		)
        	),
        	false
        );?>
    </div>
</div>



<div class="section brands">
  <div class="container p-sm-0">
      <div class="row title-and-all justify-content-between align-items-center">
          <div class="section-title">Производители<p style="font-size: 1rem;font-weight: 400;line-height: 1.5;margin-top: 1rem;">Мы являемся Официальными Дилерами и прямыми партнерами следующих производителей</p></div>
      </div>

      <div class="row brand-logos">
          <div class="col-xl-2 col-lg-3 col-4 p-0 border">
              <a href="/proizvoditeli/alekseevskaya-keramika/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/alekseevsk.jpg"  data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/alekseevsk.jpg"alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
              <a href="/proizvoditeli/altair/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/altair.jpg"  data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/altair.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
              <a href="/proizvoditeli/kirpichnyy-zavod-amstron-porikam/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Amstron.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Amstron.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
              <a href="/proizvoditeli/arskiy-kirpichnyy-zavod-aspk/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/aspk.jpg"  data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/aspk.jpg"alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
              <a href="/proizvoditeli/bikton/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/BikTon.jpg"  data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/BikTon.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-left-0">
              <a href="/proizvoditeli/vkz/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/bkz.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/bkz.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0">
              <a href="/proizvoditeli/fakro/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Fakro.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Fakro.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kazanskiy-kombinat-stroitelnykh-materialov/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kazan.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kazan.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kc-kepamik/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kc_keramik.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kc_keramik.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/shelangovskiy-kirpichnyy-zavod/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/keramika.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/keramika.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kirpichnyy-zavod-ketra/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ketra.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ketra.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/klyuker"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kluker.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kluker.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0">
              <a href="/proizvoditeli/koshchakovskiy-kirpichnyy-zavod/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/koshak.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/koshak.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/metall-profil/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/metall.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/metall.jpg" alt=""></a>
          </div>
          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
			  <a href="/proizvoditeli/profi-and-hobby/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Profi.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Profi.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/wienerberger/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Wiener.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Wiener.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/osnovit/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/osnovit.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/osnovit.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/starkhaus"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/starkhous.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/starkhous.svg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-right-0">
              <a href="/proizvoditeli/chaykovskiy-kirpichnyy-zavod/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/chaika.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/chaika.jpg" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0">
              <a href="/proizvoditeli/izkm/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo_izkm.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo_izkm.png" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0">
              <a href="/proizvoditeli/kamgeszyab/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo_zyab.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo_zyab.png" alt=""></a>
          </div>

          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/mamadyshskiy/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo_makeram.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo_makeram.jpg" alt=""></a>
          </div>
          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/promix/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/promix.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/promix.jpg" alt=""></a>
          </div>
          <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/perel/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/perel.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/perel.jpg" alt=""></a>
          </div>
		  
		  
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 				<a href="/proizvoditeli/bogdanovichskiy/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/bogdanovich.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/bogdanovich.jpg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
				<a href="/proizvoditeli/bund-bau/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/bund_bau2.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/bund_bau2.jpg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/cheboksarskaya-keramika/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/cheboksar.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/cheboksar.jpg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/chelninskiy/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/chelninskiy2.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/chelninskiy2.jpg" alt=""></a>
			</div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kazanskiy-zavod-silikatnykh-stenovykh-materialov/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kazanskiy-zavod-silikatnykh2.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kazanskiy-zavod-silikatnykh2.jpg" alt=""></a>
			</div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kirpich-kholding/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kirpich_holding.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kirpich_holding.jpg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kottedzh/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kottedj.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/kottedj.jpg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/magma/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/magma.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/magma.png" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/makhina-tst/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/mahina.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/mahina.png" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
			  <a href="/proizvoditeli/steklonit/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/steklonit.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/steklonit.jpg" alt=""></a>
			 </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
			<a href="/proizvoditeli/stroitelnye-innovatsii"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/stroitelnie_inovatsii2.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/stroitelnie_inovatsii2.jpg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
                      <a href="/proizvoditeli/teplon"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Teplon.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Teplon.jpg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/uniblock"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/uniblock.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/uniblock.jpg" alt=""></a>
          </div>
		 <!-- <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/ventek/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ventek.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ventek.jpg" alt=""></a>
          </div>-->
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/vilpe/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/vilpe.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/vilpe.jpg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/grand-line"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/grandLine.jpeg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/grandLine.jpeg" alt=""></a>
          </div>
		  
		  
		 <!-- <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/isover/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Isover.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Isover.jpg" alt=""></a>
          </div>-->
		 <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/belebeevskiy/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Белебеевский.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Белебеевский.png" alt=""></a>
          </div>
		 <!-- <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/lsr/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/лср.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/лср.png" alt=""></a>
          </div>-->
		 <!-- <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kamskiy-zavod-metallokrovlya/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/металлокровля.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/металлокровля.png" alt=""></a>
          </div>-->
		 <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/tekhnonikol/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Технониколь.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/Технониколь.jpg" alt=""></a>
          </div>
         <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kulonstroy/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/кулонстрой.jpeg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/кулонстрой.jpeg" alt=""></a>
          </div>
		  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/raff/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/РАФФ.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/РАФФ.jpg" alt=""></a>
          </div>
			<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kerma/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/керма.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/керма.png" alt=""></a>
          </div>
			<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/gras/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ГРАС.jpeg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ГРАС.jpeg" alt=""></a>
          </div>
			<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/kerakam/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/керакам.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/керакам.png" alt=""></a>
          </div>
			<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/zyab-izhevsk/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/лого зяб ижевск.png.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/лого зяб ижевск.png.jpg" alt=""></a>
          </div>
			<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/mzksm/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo мстера.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo мстера.png" alt=""></a>
          </div>
			<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/build-stone/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/build_stroy_logo.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/build_stroy_logo.png" alt=""></a>
          </div>
			<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/zsk/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ЗСК_Челны.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/ЗСК_Челны.png" alt=""></a>
          </div>

	<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/durtuli"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/durtuli.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/durtuli.jpg" alt=""></a>
	</div>
	<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/inkeram"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/inkeram.jpg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/inkeram.jpg" alt=""></a>
	</div>
	<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/meakir"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/meakir.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/meakir.png" alt=""></a>
	</div>
	<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/revdinski"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/revdinski.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/revdinski.png" alt=""></a>
	</div>
	<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/yadrinsi"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/yadrinsi.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/yadrinsi.png" alt=""></a>
	</div>
	<div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
 <a href="/proizvoditeli/morgaushski"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/morgaushski.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/morgaushski.png" alt=""></a>
	</div>

	  <div class="col-xl-2 col-lg-3 col-4 p-0 border border-top-0 border-left-0">
              <a href="/proizvoditeli/art-alyans/"><img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo-art-alyans.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/brands/logo-art-alyans.png" alt=""></a>
          </div>
      </div>
  </div>
</div>

<? include($_SERVER["DOCUMENT_ROOT"]."/local/templates/tulpar_store/includes/main_catalog_1.php"); ?>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>