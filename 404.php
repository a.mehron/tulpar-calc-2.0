<?
	include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');
	CHTTP::SetStatus("404 Not Found");
	@define("ERROR_404", "Y");
?>
<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); ?>

<div class="container">
    <div class="error-page">
        <div class="image"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/sad.svg" alt=""></div>
        <div class="error-info-h2">
            Ошибка 404
        </div>
        <h1 class="error-info">Cтраница не найдена</h1>
        <p class="errornf-msg">Вы перешли по неправильной ссылке <br> или страница была удалена.</p>
    </div>
</div>

<? include($_SERVER["DOCUMENT_ROOT"]."/local/templates/tulpar_store/includes/main_catalog.php"); ?>


<? include($_SERVER["DOCUMENT_ROOT"]."/local/templates/tulpar_store/footer_404.php"); ?>
