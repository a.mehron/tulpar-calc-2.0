<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("test");
?>

<?
$ID = CCatalogMeasureRatio::add(Array('PRODUCT_ID' => 6731, 'RATIO' => 2));

?>

<?
    $section_id = 20;
    if(CModule::IncludeModule('iblock'))
    {
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_PAGE_URL", "PREVIEW_PICTURE");
        $arFilter = Array("IBLOCK_ID"=>IntVal(4), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID" => 146, "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да");

        $res = CIBlockElement::GetList(
            Array("SORT" => "ASC"),
            $arFilter,
            false,
            Array('nTopCount' => 10),
            $arSelect
        );

        while($ob = $res->GetNextElement()){
            $arItem = $ob->GetFields();
            $arrayResult[] = $arItem['ID'];
        }

  ////////////////

        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "DETAIL_PAGE_URL", "PREVIEW_PICTURE");
        $arFilter = Array("IBLOCK_ID"=>IntVal(10), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");

        $res = CIBlockElement::GetList(
            Array("SORT" => "ASC"),
            $arFilter,
            false,
            false,
            $arSelect
        );

        while($ob = $res->GetNextElement()){
            $arItem = $ob->GetFields();
            $arItem["PROPERTIES"] = $ob->GetProperties();
            $arrayResultBanners[] = $arItem;
        }

        $filterArray = Array();
        $bannerCount = 0;
        foreach ($arrayResult as $productID) {
          if ($bannerCount > 2) {
            break;
          }
          foreach ($arrayResultBanners as $banner) {
            if ($productID == $banner['PROPERTIES']['ATT_PRODUCT_ON_BANNER']['VALUE']) {
              $filterArray[] = $banner['ID'];
              $bannerCount++;
            }
          }
        }

    }
    ?>

<?php $size = sizeof($filterArray); ?>
<?php if ($size > 0): ?>

<div class="container">
  <div class="row">
    <div class="col-xl-9">
      <div class="section-banner d-none d-md-block">
        <?
        $GLOBALS['arrFilter'] = array('ID' => $filterArray);
      	$APPLICATION->IncludeComponent(
        	"bitrix:news.list",
        	"banner-slider-section",
        	array(
        		"ACTIVE_DATE_FORMAT" => "d.m.Y",
        		"ADD_SECTIONS_CHAIN" => "N",
        		"AJAX_MODE" => "N",
        		"AJAX_OPTION_ADDITIONAL" => "",
        		"AJAX_OPTION_HISTORY" => "N",
        		"AJAX_OPTION_JUMP" => "N",
        		"AJAX_OPTION_STYLE" => "Y",
        		"CACHE_FILTER" => "N",
        		"CACHE_GROUPS" => "Y",
        		"CACHE_TIME" => "36000000",
        		"CACHE_TYPE" => "A",
        		"CHECK_DATES" => "Y",
        		"DETAIL_URL" => "",
        		"DISPLAY_BOTTOM_PAGER" => "Y",
        		"DISPLAY_DATE" => "N",
        		"DISPLAY_NAME" => "N",
        		"DISPLAY_PICTURE" => "N",
        		"DISPLAY_PREVIEW_TEXT" => "N",
        		"DISPLAY_TOP_PAGER" => "N",
        		"FIELD_CODE" => array(
        			0 => "DETAIL_PICTURE",
        			1 => "",
        		),
        		"FILTER_NAME" => "arrFilter",
        		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
        		"IBLOCK_ID" => "10",
        		"IBLOCK_TYPE" => "catalog_banners",
        		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        		"INCLUDE_SUBSECTIONS" => "N",
        		"MESSAGE_404" => "",
        		"NEWS_COUNT" => "6",
        		"PAGER_BASE_LINK_ENABLE" => "N",
        		"PAGER_DESC_NUMBERING" => "N",
        		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        		"PAGER_SHOW_ALL" => "N",
        		"PAGER_SHOW_ALWAYS" => "N",
        		"PAGER_TEMPLATE" => ".default",
        		"PAGER_TITLE" => "Новости",
        		"PARENT_SECTION" => "",
        		"PARENT_SECTION_CODE" => "",
        		"PREVIEW_TRUNCATE_LEN" => "",
        		"PROPERTY_CODE" => array(
        			0 => "",
        			1 => "ATT_PRODUCT_ON_BANNER",
        			2 => "",
        		),
        		"SET_BROWSER_TITLE" => "N",
        		"SET_LAST_MODIFIED" => "N",
        		"SET_META_DESCRIPTION" => "N",
        		"SET_META_KEYWORDS" => "N",
        		"SET_STATUS_404" => "N",
        		"SET_TITLE" => "N",
        		"SHOW_404" => "N",
        		"SORT_BY1" => "SORT",
        		"SORT_BY2" => "ACTIVE_FROM",
        		"SORT_ORDER1" => "ASC",
        		"SORT_ORDER2" => "DESC",
        		"STRICT_SECTION_CHECK" => "N",
        		"COMPONENT_TEMPLATE" => "banner-slider-section",
        		"COMPOSITE_FRAME_MODE" => "A",
        		"COMPOSITE_FRAME_TYPE" => "AUTO"
        	),
        	false
        );
        ?>
    </div>
  </div>
</div>
</div>
<?php endif; ?>



<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
