<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Тест 3");
?>
<?$APPLICATION->AddHeadScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')?>
<?$APPLICATION->AddHeadScript('https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js')?>
<?$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')?>
<?$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js')?>
<?$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js')?>
<?$APPLICATION->AddHeadScript('https://cdn.datatables.net/buttons/1.5.4/js/buttons.html5.min.js')?>



<div class="container">
  <?

  $sectionID = 223;
  if(CModule::IncludeModule("catalog") && CModule::IncludeModule("iblock")){
    global $arrFilterPriceList;
    $arSelect = Array("IBLOCK_ID",
              "ID",
              "NAME",
              "PROPERTY_*",
              "CATALOG_GROUP_1"
            );

    if ($sectionID == 255) {
      $strengthMark = "Марка прочности";
    }
    $arrFilterPriceList = Array("IBLOCK_ID"=>IntVal(4), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID" => $sectionID);
    $res = CIBlockElement::GetList(
        Array("SORT" => "ASC"),
        $arrFilterPriceList,
        false,
        false,
        $arSelect
    );

    while($ob = $res->GetNextElement()){
        $arItem = $ob->GetFields();
        $arItem["PROPERTIES"] = $ob->GetProperties();
        $arrayResultItems[] = $arItem;
    }

    $propertiesToSkip = Array($strengthMark, "Тип", "Картинки галерея", "Является основным товаром", "Основной товар",
                              "Оттенок цвета", "Твердость по карандашу", "Прочность при ударе, Дж.", "Прочность при изгибе, Т.", "Защита соляного тумана, часов", "Стойкость к УФ - излучениям", "Максимальная температура эксплуатации, C", "Блеск, %", "Стоимость доставки", "Цвет смеси", "Предел прочности, Н/мм2", "Прочность на сжатие, МПа", "Прочность сцепления, МПа", "Пористость изделий, %", "Размер", "Огнеупорность, °C", "Ширина профиля, мм", "Коэффициент теплопроводности, Вт/(м·°C)", "Водопоглощение, %");
    $theadValues[] = Array('NAME', "Наименование");
    $tbodyValues = Array();
    $counter = 0;
    foreach ($arrayResultItems as $arrayItem) {
      if ($arrayItem['PROPERTIES']['ATT_IS_MAIN_ITEM']['VALUE'] == "Да") {
        continue;
      }

      foreach ($arrayItem['PROPERTIES'] as $key => $property) {
        if (in_array($property['NAME'], $propertiesToSkip)) {
          continue;
        }


        $keyFound = "false";

        if ($property['VALUE'] != "") {
          $curArray = array($key, $property['NAME']);
          if (!in_array($curArray, $theadValues)) {
            $theadValues[] = Array($key, $property['NAME']);
          }
        }
      }
    }

    foreach ($arrayResultItems as $arrayItem) {
      if ($arrayItem['PROPERTIES']['ATT_IS_MAIN_ITEM']['VALUE'] == "Да") {
        continue;
      }

      $rowValues = Array($arrayItem['NAME']);
      $i = 0;
      foreach ($arrayItem['PROPERTIES'] as $key => $property) {
        $curArray = array($key, $property['NAME']);
        if (in_array($curArray, $theadValues)) {
          $rowValues[] = $property['VALUE'];
        }
      }
      $rowValues[] = $arrayItem['CATALOG_PRICE_1'];

      $tbodyValues[] = $rowValues;

    }
  }
  ?>
  <table class="priceList d-none"  id="priceList">
    <thead>
      <th>№</th>
      <?php foreach ($theadValues as $thead): ?>
          <th class="p-3"><?=$thead[1]?></th>
      <?php endforeach; ?>
      <th>Цена, руб.</th>
    </thead>

    <tbody>
      <?php foreach ($tbodyValues as $key => $itemRow): ?>
        <tr>
          <td><?=++$key?></td>
          <?php foreach ($itemRow as $propItem): ?>
          <td><?=$propItem?></td>
          <?php endforeach; ?>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>

<script>
  $(document).ready(function() {
    $('#priceList').DataTable( {
        dom: 'Bfrtip',
        "searching": false,
        buttons: [
          {
              extend: 'excelHtml5',
              orientation: 'landscape',
              pageSize: 'LEGAL'
          },
            {
                extend: 'pdfHtml5',
                orientation: 'landscape',
                pageSize: 'LEGAL'
            }
        ]
    } );

    $('.dt-buttons').prepend('<span>Скачать прайс-лист: </span>');
  } );
</script>


<div class="container categories">
  <h2>У нас вы можете купить</h2>
  <div class="row">
      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/oblitsovochnyy/">
            <div class="image"><img src="/upload/iblock/15f/15f27d0507b8c126d77c5df1bf98c6c7.jpg" alt=""></div>
            <span>Облицовочный кирпич</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/keramicheskiy-kirpich/">
            <div class="image"><img class="maw260" src="/upload/iblock/c7a/c7a55adafac867c1e828d5dfd55fa46b.jpg" alt=""></div>
            <span>Керамический кирпич</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/stroitelnyy-kirpich/">
            <div class="image"><img class="maw180" src="/upload/iblock/d23/d2320fcb08a11df52aea2ec054782cfa.jpg" alt=""></div>
            <span>Строительный кирпич</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/figurnyy-kirpich/">
            <div class="image"><img class="" src="/upload/iblock/34f/34f43a6a75ab2c8687124e6b75a93adc.jpg" alt=""></div>
            <span>Фигурный кирпич</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/shamotnyy_kirpich/">
            <div class="image"><img class="" src="/upload/iblock/d21/d21b7a5ea79641fb5887306930b3dad5.jpg" alt=""></div>
            <span>Огнеупорный кирпич</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/krupnoformatnyy-keramicheskiy-blok/">
            <div class="image"><img class="" src="/upload/iblock/e03/e035a8ac544f420f26e6d1abf8b1e850.jpg" alt=""></div>
            <span>Крупноформатный керамический блок</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/peregorodochnyy-keramicheskiy-blok/">
            <div class="image"><img class="" src="/upload/iblock/4dd/4ddbebe843c6558e9e714499543ee44b.jpg" alt=""></div>
            <span>Перегородочный керамический блок</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/keramicheskiy-kamen/">
            <div class="image vam"><img class="maw180" src="/upload/iblock/45a/45ac552ef3d4ed77ceaff543efbfb0ef.jpg" alt=""></div>
            <span>Керамический камень</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/krupnoformatnye-gazobetonnye-bloki/">
            <div class="image vam"><img class="" src="/upload/iblock/b08/b0807f77fa5bcb720cdd1694433ca3f1.jpg" alt=""></div>
            <span>Крупноформатный газобетонный блок</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/peregorodochnye-gazobetonnye-bloki/">
            <div class="image vam"><img class="" src="/upload/iblock/95c/95c18d3dcfbb77ba237eb78407333947.jpg" alt=""></div>
            <span>Перегородочный газобетонный блок</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/sukhie-stroitelnye-smesi/">
            <div class="image vam"><img class="maw180" src="/upload/iblock/e82/e8292d29c899630923377f01f85050ea.jpg" alt=""></div>
            <span>Сухие строительные смеси</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/stroitelnaya-setka/">
            <div class="image vam"><img class="" src="/upload/iblock/ce9/ce9dddf663b7894d100b499592cfdd1f.jpeg" alt=""></div>
            <span>Строительные сетки</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/metallocherepitsa/">
            <div class="image vam"><img class="" src="<?=SITE_TEMPLATE_PATH?>/assets/img/mcherepica.jpg" alt=""></div>
            <span>Металлическая черепица</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/profnastil/">
            <div class="image vam"><img class="" src="<?=SITE_TEMPLATE_PATH?>/assets/img/profnastil.jpg" alt=""></div>
            <span>Профнастил</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/krovelnaya-ventilyatsiya/">
            <div class="image vam"><img class="" src="/upload/iblock/32c/32c13f9d740f4f5d0e44b238acfe34b7.jpg" alt=""></div>
            <span>Доборные элементы</span>
          </a>
      </div>

      <div class="col-lg-3 col-6 p-0 border category">
          <a href="/catalog/elementy-bezopasnosti-krovli/">
            <div class="image vam"><img class="maw140" src="/upload/iblock/cd6/cd6f5aff26a0bda397ca4e73a742061e.jpg" alt=""></div>
            <span>Элементы безопасности кровли</span>
          </a>
      </div>


  </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
