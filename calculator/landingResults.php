<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Онлайн калькулятор");
?>

<?if($_GET['test'] == "Y"):?>
    <div class="container">
        <img alt="разработка.jpg" src="/upload/medialibrary/71f/71f877afde00352479df9625af04511c.jpg" style="max-width:100%" title="разработка.jpg" align="middle">
    </div>
<?else:?>
    <div class="calc-page">
        <div class="gray-wrapper">
            <div class="container">


                <?
                if (true) {
                    if (CModule::IncludeModule('iblock')) {
                        $productsImageArr = [];

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_BRICKS_FORMAT", "PROPERTY_ATT_SURFACE_BRICK", "PROPERTY_ATT_COLOR_TONE_BRICK", "PROPERTY_ATT_BRICK_COLOR", "PROPERTY_ATT_KIRPICH_TYPE", "PROPERTY_ATT_PODDON_QTY", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(58)); //облицовочный

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 999),
                            $arSelect
                        );

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            $arResult["BRICKS"]["FASADNIY"][] = [
                                "ID" => $arItem["ID"],
                                "NAME" => $arItem["NAME"],
                                "SIZE" => $arItem["PROPERTY_ATT_BRICKS_FORMAT_VALUE"],
                                "MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"],
                                "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"],
                                "SURFACE" => $arItem["PROPERTY_ATT_SURFACE_BRICK_VALUE"],
                                "COLOR_TONE" => $arItem["PROPERTY_ATT_COLOR_TONE_BRICK_VALUE"],
                                "COLOR" => $arItem["PROPERTY_ATT_BRICK_COLOR_VALUE"],
                                "IMAGE" => $arItem["PREVIEW_PICTURE"],
                                "TYPE" => $arItem["PROPERTY_ATT_KIRPICH_TYPE_VALUE"],
                                "PODDON_QNTY" => $arItem["PROPERTY_ATT_PODDON_QTY_VALUE"],
                                "PRICE" => $arItem['CATALOG_PRICE_1']
                            ];
                            $productsImageArr[] = $arItem["PREVIEW_PICTURE"];
                            //$arResult[] = $arItem;
                        }

                        /*
                         *  Список перегородочного кирпича
                         */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_BRICKS_FORMAT", "PROPERTY_ATT_STRENGTH_MARK", "PROPERTY_ATT_KIRPICH_TYPE", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(504)); //перегородочный

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 20),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                $arResult["BRICKS"]["PEREGORODKI"][] = [
                                    "ID" => $arItem["ID"],
                                    "NAME" => $arItem["NAME"],
                                    "SIZE" => $arItem["PROPERTY_ATT_BRICKS_FORMAT_VALUE"],
                                    "STRENGTH_MARK" => $arItem["PROPERTY_ATT_STRENGTH_MARK_VALUE"],
                                    "MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"],
                                    "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"],
                                    "IMAGE" => $arItem["PREVIEW_PICTURE"],
                                    "TYPE" => $arItem["PROPERTY_ATT_KIRPICH_TYPE_VALUE"]
                                ];
                                $productsImageArr[] = $arItem["PREVIEW_PICTURE"];

                                $tempIdArr[] = $arItem["ID"];
                            }

                            //$arResult[] = $arItem;
                        }

                        /*END*/


                        /*
                         *  Список цокольного кирпича
                         */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_BRICKS_FORMAT", "PROPERTY_ATT_STRENGTH_MARK", "PROPERTY_ATT_KIRPICH_TYPE", "PROPERTY_ATT_PODDON_QTY", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(505)); //цокольный

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 200),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];

                        //echo "<pre>";

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                //print_r($arItem);
                                $arResult["BRICKS"]["TSOKOLNIY"][] = [
                                    "ID" => $arItem["ID"],
                                    "NAME" => $arItem["NAME"],
                                    "SIZE" => $arItem["PROPERTY_ATT_BRICKS_FORMAT_VALUE"],
                                    "STRENGTH_MARK" => $arItem["PROPERTY_ATT_STRENGTH_MARK_VALUE"],
                                    "MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"],
                                    "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"],
                                    "PODDON_QNTY" => $arItem['PROPERTY_ATT_PODDON_QTY_VALUE'],
                                    "IMAGE" => $arItem["PREVIEW_PICTURE"],
                                    "TYPE" => $arItem["PROPERTY_ATT_KIRPICH_TYPE_VALUE"],
                                    "PRICE" => $arItem['CATALOG_PRICE_1']
                                ];
                                $productsImageArr[] = $arItem["PREVIEW_PICTURE"];

                                $tempIdArr[] = $arItem["ID"];
                            }

                            //$arResult[] = $arItem;
                        }

//                echo "</pre>";

                        /*END*/

                        /*
                         *  Список рядового кирпича
                         */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_BRICKS_FORMAT", "PROPERTY_ATT_STRENGTH_MARK", "PROPERTY_ATT_KIRPICH_TYPE", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(504)); //рядовой

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 200),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                $arResult["BRICKS"]["RYADOVOY"][] = [
                                    "ID" => $arItem["ID"],
                                    "NAME" => $arItem["NAME"],
                                    "SIZE" => $arItem["PROPERTY_ATT_BRICKS_FORMAT_VALUE"],
                                    "STRENGTH_MARK" => $arItem["PROPERTY_ATT_STRENGTH_MARK_VALUE"],
                                    "MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"],
                                    "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"],
                                    "IMAGE" => $arItem["PREVIEW_PICTURE"],
                                    "TYPE" => $arItem["PROPERTY_ATT_KIRPICH_TYPE_VALUE"]
                                ];
                                $productsImageArr[] = $arItem["PREVIEW_PICTURE"];

                                $tempIdArr[] = $arItem["ID"];
                            }

                            //$arResult[] = $arItem;
                        }

                        /*END*/


                        /*
                        *  Список шамотного кирпича
                        */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_BRICKS_FORMAT", "PROPERTY_ATT_STRENGTH_MARK", "PROPERTY_ATT_KIRPICH_TYPE", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(216)); //шамотный

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 200),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                $arResult["BRICKS"]["SHAMOTNIY"][] = [
                                    "ID" => $arItem["ID"],
                                    "NAME" => $arItem["NAME"],
                                    "SIZE" => $arItem["PROPERTY_ATT_BRICKS_FORMAT_VALUE"],
                                    "STRENGTH_MARK" => $arItem["PROPERTY_ATT_STRENGTH_MARK_VALUE"],
                                    "MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"],
                                    "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"],
                                    "IMAGE" => $arItem["PREVIEW_PICTURE"],
                                    "TYPE" => $arItem["PROPERTY_ATT_KIRPICH_TYPE_VALUE"]
                                ];
                                $productsImageArr[] = $arItem["PREVIEW_PICTURE"];

                                $tempIdArr[] = $arItem["ID"];
                            }

                            //$arResult[] = $arItem;
                        }

                        /*END*/


                        /*
                         *  Список керамблоков
                         */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_BRICKS_FORMAT", "PREVIEW_PICTURE", "PROPERTY_ATT_PODDON_QTY", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(506)); //керамблок

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 500),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                $arResult["BRICKS"]["KERAMBLOCKS"][] = [
                                    "ID" => $arItem["ID"],
                                    "NAME" => $arItem["NAME"],
                                    "SIZE" => $arItem["PROPERTY_ATT_BRICKS_FORMAT_VALUE"],
                                    "MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"],
                                    "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"],
                                    "IMAGE" => $arItem["PREVIEW_PICTURE"],
                                    "PODDON_QNTY" => $arItem["PROPERTY_ATT_PODDON_QTY_VALUE"],
                                    "PRICE" => $arItem["CATALOG_PRICE_1"]
                                ];
                                $productsImageArr[] = $arItem["PREVIEW_PICTURE"];

                                $tempIdArr[] = $arItem["ID"];
                            }

                            //$arResult[] = $arItem;
                        }

                        /*END*/

                        /*
                        *  Список перегородочных керамблоков
                        */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_BRICKS_FORMAT", "PROPERTY_ATT_PODDON_QNTY", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(510)); //керамблок перегородочный

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 500),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            $arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                $arResult["BRICKS"]["KERAMBLOCKS_PEREGORODKI"][] = [
                                    "ID" => $arItem["ID"],
                                    "NAME" => $arItem["NAME"],
                                    "SIZE" => $arItem["PROPERTY_ATT_BRICKS_FORMAT_VALUE"],
                                    "MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"],
                                    "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"],
                                    "IMAGE" => $arItem["PREVIEW_PICTURE"],
                                    "PODDON_QNTY" => $arItem['PROPERTIES']['ATT_PODDON_QTY']['VALUE'],
                                    "PRICE" => $arItem["CATALOG_PRICE_1"]
                                ];
                                $productsImageArr[] = $arItem["PREVIEW_PICTURE"];

                                $tempIdArr[] = $arItem["ID"];
                            }

                            //$arResult[] = $arItem;
                        }

                        /*END*/


                        /*
                         *  Список газоблоков
                         */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_DENSITY_MARK_BLOCKS", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(520)); //газоблок

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 500),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];
                        $tempIdMakerArr = [];

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                $tempIdArr[] = $arItem["ID"];
                                $tempIdMakerArr[$arItem["ID"]] = ["MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"], "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"]];
                            }

                            //$arResult[] = $arItem;
                        }

                        $res = CCatalogSKU::getOffersList(
                            $tempIdArr,
                            4,
                            $skuFilter = array(),
                            $fields = array("NAME", "PROPERTY_ATT_GAZOBLOCK_WIDTH_SKU", "PROPERTY_ATT_DENSITY_MARK_BLOCKS_SKU", "PROPERTY_ATT_GAZOBLOCK_HEIGHT_SKU", "PREVIEW_PICTURE", "PROPERTY_ATT_BRICKS_PODDIN_QTY_SKU", "CATALOG_GROUP_1"),
                            $propertyFilter = array("ACTIVE" => "Y")
                        );

                        foreach ($res as $mainProdId => $mainProduct) {
                            foreach ($mainProduct as $sku) {
                                $arResult["BRICKS"]["GAZOBLOCKS"][] = [
                                    "ID" => $sku["ID"],
                                    "NAME" => $sku["NAME"],
                                    "DENSITY_MARK" => $sku["PROPERTY_ATT_DENSITY_MARK_BLOCKS_SKU_VALUE"],
                                    "WIDTH" => $sku["PROPERTY_ATT_GAZOBLOCK_WIDTH_SKU_VALUE"],
                                    "HEIGHT" => $sku["PROPERTY_ATT_GAZOBLOCK_HEIGHT_SKU_VALUE"],
                                    "MAKER" => $tempIdMakerArr[$mainProdId]["MAKER"],
                                    "MAKER_VALUE_ID" => $tempIdMakerArr[$mainProdId]["MAKER_VALUE_ID"],
                                    "IMAGE" => $sku["PREVIEW_PICTURE"],
                                    "PODDON_QNTY" => $sku["PROPERTY_ATT_BRICKS_PODDIN_QTY_SKU_VALUE"],
                                    "PRICE" => $sku["CATALOG_PRICE_1"]
                                ];
                                $productsImageArr[] = $sku["PREVIEW_PICTURE"];
                            }
                        }

//                echo "<pre>";
//                print_r($arResult["BRICKS"]["GAZOBLOCKS"]);
//                echo "</pre>";


                        /*END*/


                        /*
                         *  Список перегородочных газоблоков
                         */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_DENSITY_MARK_BLOCKS", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(521)); //газоблок перегородочные

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 500),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];
                        $tempIdMakerArr = [];

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                $tempIdArr[] = $arItem["ID"];
                                $tempIdMakerArr[$arItem["ID"]] = ["MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"], "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"]];
                            }

                            //$arResult[] = $arItem;
                        }

                        $res = CCatalogSKU::getOffersList(
                            $tempIdArr,
                            4,
                            $skuFilter = array(),
                            $fields = array("NAME", "PROPERTY_ATT_GAZOBLOCK_WIDTH_SKU", "PROPERTY_ATT_DENSITY_MARK_BLOCKS_SKU", "PROPERTY_ATT_GAZOBLOCK_HEIGHT_SKU", "PROPERTY_ATT_BRICKS_PODDIN_QTY_SKU", "PREVIEW_PICTURE", "CATALOG_GROUP_1"),
                            $propertyFilter = array("ACTIVE" => "Y")
                        );

                        foreach ($res as $mainProdId => $mainProduct) {
                            foreach ($mainProduct as $sku) {
                                $arResult["BRICKS"]["GAZOBLOCKS_PEREGORODKI"][] = [
                                    "ID" => $sku["ID"],
                                    "NAME" => $sku["NAME"],
                                    "DENSITY_MARK" => $sku["PROPERTY_ATT_DENSITY_MARK_BLOCKS_SKU_VALUE"],
                                    "WIDTH" => $sku["PROPERTY_ATT_GAZOBLOCK_WIDTH_SKU_VALUE"],
                                    "HEIGHT" => $sku["PROPERTY_ATT_GAZOBLOCK_HEIGHT_SKU_VALUE"],
                                    "MAKER" => $tempIdMakerArr[$mainProdId]["MAKER"],
                                    "MAKER_VALUE_ID" => $tempIdMakerArr[$mainProdId]["MAKER_VALUE_ID"],
                                    "IMAGE" => $sku["PREVIEW_PICTURE"],
                                    "PODDON_QNTY" => $sku["PROPERTY_ATT_BRICKS_PODDIN_QTY_SKU_VALUE"],
                                    "PRICE" => $sku["CATALOG_PRICE_1"]
                                ];
                                $productsImageArr[] = $sku["PREVIEW_PICTURE"];
                            }
                        }

//                echo "<pre>";
//                print_r($arResult["BRICKS"]["GAZOBLOCKS"]);
//                echo "</pre>";


                        /*END*/


                        /*
                         *  Список кирпича для выбора "перегородки", материал "кирпич"
                         */

                        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_ATT_MAKER", "PROPERTY_ATT_BRICKS_FORMAT", "PROPERTY_ATT_STRENGTH_MARK", "PROPERTY_ATT_KIRPICH_TYPE", "PROPERTY_ATT_PODDON_QTY", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
                        $arFilter = Array("IBLOCK_ID" => IntVal(4), "ACTIVE_DATE" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE" => "Y", "SECTION_ID" => Array(146, 514)); //строительный

                        $res = CIBlockElement::GetList(
                            Array("SORT" => "ASC"),
                            $arFilter,
                            false,
                            Array("nPageSize" => 200),
                            $arSelect
                        );

                        //Почему-то повторяются элементы выборки по 2 раза.
                        // Создадим временный массив для проверки повторения

                        $tempIdArr = [];

                        while ($ob = $res->GetNextElement()) {
                            $arItem = $ob->GetFields();
                            //$arItem["PROPERTIES"] = $ob->GetProperties();
                            if (!in_array($arItem["ID"], $tempIdArr)) {
                                $arResult["BRICKS"]["STROITELNIY"][] = [
                                    "ID" => $arItem["ID"],
                                    "NAME" => $arItem["NAME"],
                                    "SIZE" => $arItem["PROPERTY_ATT_BRICKS_FORMAT_VALUE"],
                                    "STRENGTH_MARK" => $arItem["PROPERTY_ATT_STRENGTH_MARK_VALUE"],
                                    "MAKER" => $arItem["PROPERTY_ATT_MAKER_VALUE"],
                                    "MAKER_VALUE_ID" => $arItem["PROPERTY_ATT_MAKER_ENUM_ID"],
                                    "IMAGE" => $arItem["PREVIEW_PICTURE"],
                                    "TYPE" => $arItem["PROPERTY_ATT_KIRPICH_TYPE_VALUE"],
                                    "PODDON_QNTY" => $arItem["PROPERTY_ATT_PODDON_QTY_VALUE"],
                                    "PRICE" => $arItem["CATALOG_PRICE_1"]
                                ];
                                $productsImageArr[] = $arItem["PREVIEW_PICTURE"];

                                $tempIdArr[] = $arItem["ID"];
                            }

                            //$arResult[] = $arItem;
                        }

                        /*END*/


                        $arResult["MAKERS_IMAGES"] = [
                            "481" => ["IMG_URL" => "/3d/images/icons/makers/alekseevsk.jpg", "PUBLIC_NAME" => "Алексеевская керамика"],
                            "498" => ["IMG_URL" => "/3d/images/icons/makers/altair.jpg", "PUBLIC_NAME" => "Альтаир (Ижевск)"],
                            "502" => ["IMG_URL" => "/3d/images/icons/makers/koshak.jpg", "PUBLIC_NAME" => "АкБарс Керамик (Кощаково)"],
                            "134" => ["IMG_URL" => "/3d/images/icons/makers/aspk.jpg", "PUBLIC_NAME" => "АСПК (Арск)"],
                            "495" => ["IMG_URL" => "/3d/images/icons/makers/kc_keramik.jpg", "PUBLIC_NAME" => "КС Керамик (Кирово-Чепецк)"],
                            "483" => ["IMG_URL" => "/3d/images/icons/makers/kluker.jpg", "PUBLIC_NAME" => "Клюкер (Ключищи)"],
                            "505" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/bkz.jpg", "PUBLIC_NAME" => "ВКЗ (Воткинск)"],
                            "504" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/chaika.jpg", "PUBLIC_NAME" => "Чайковский (Пермский край)"],
                            "503" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/kazan.jpg", "PUBLIC_NAME" => "Казанский (Казань)"],
                            "507" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/chelny.png", "PUBLIC_NAME" => "АкБарс Керамик (Челны)"],
                            "506" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/logo_makeram.jpg", "PUBLIC_NAME" => "Makeram (Мамадыш)"],
                            "630" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/Wiener.jpg", "PUBLIC_NAME" => "Wienerberger Porotherm"],
                            "800" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/ketra.jpg", "PUBLIC_NAME" => "Кетра"],
                            "801" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/Amstron.jpg", "PUBLIC_NAME" => "Amstron"],
                            "802" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/keramika.jpg", "PUBLIC_NAME" => "Шеланга"],
                            "1189" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/BikTon.jpg", "PUBLIC_NAME" => "Биктон"],
                            "1303" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/logo_zyab.png", "PUBLIC_NAME" => "Завод Ячеистых Бетонов (ЗЯБ)"],
                            "1345" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/logo_izkm.png", "PUBLIC_NAME" => "Ижевский завод керамических материалов"],
                            "1445" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/magma.png", "PUBLIC_NAME" => "Саранский кирпичный завод (МАГМА)"],
                            "1547" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/Uniblock.png", "PUBLIC_NAME" => "Uniblock "],
                            "2196" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/chebker.png", "PUBLIC_NAME" => "Чебоксарская керамика"],
                            "2301" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/teplon.png", "PUBLIC_NAME" => "Теплон (Ульяновск)"],
                            "2302" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/cottage_logo.png", "PUBLIC_NAME" => "Коттедж (Самара)"],
                            "2675" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/керма.png", "PUBLIC_NAME" => "Керма"],
                            "2676" => ["IMG_URL" => "/local/templates/tulpar_store/assets/img/brands/%D0%91%D0%B5%D0%BB%D0%B5%D0%B1%D0%B5%D0%B5%D0%B2%D1%81%D0%BA%D0%B8%D0%B9.png", "PUBLIC_NAME" => "Белебеевский"],
                        ];

                        $arResult["COLOR_TONES"] = [
                            "yellow" => "желтый",
                            "brown" => "коричневый",
                            "red" => "красный",
                            "bavarian" => "бавария",
                            "gray" => "серый",
                            "flash" => "бавария (флэш)",
                            "white" => "белый",
                            "green" => "зеленый",
                            "black" => "черный"

                        ];

                        $arResult["KERAMBLOCKS_FORMAT_NAMES"] = [
                            "630" => ["4.5НФ" => "8", "5.4НФ" => "38", "6.2НФ" => "44", "6.7НФ" => "12", "7.2НФ" => "51", "9НФ" => "20", "10.5НФ" => "25", "10.7НФ" => "38", "12.4НФ" => "44", "14.3НФ" => "51",],
                            "800" => ["4.5НФ" => "8", "5.4НФ" => "38", "5.7НФ" => "10", "6.2НФ" => "44", "7.2НФ" => "51", "6.9НФ" => "12", "9НФ" => "20", "10.7НФ" => "38", "11.2НФ" => "25", "12.4НФ" => "44", "14.3НФ" => "51",],
                            "801" => ["5.5НФ" => "38", "6.9НФ" => "12", "7НФ" => "25", "7.4НФ" => "51", "10.7НФ" => "25, 38", "8.5НФ" => "20", "11.2НФ" => "39.8", "14.3НФ" => "51"],
                        ];

                        $arResult["PODDON_PRICE"] = [
                            "134" => 160,
                            "498" => 200,
                            "507" => 158,
                            "504" => 185,
                            "502" => 158,
                            "1345" => 200
                        ];


                        //собираем массив изображений товаров

                        $res = CFile::GetList(array(), array("@ID" => $productsImageArr));
                        while ($res_arr = $res->GetNext()) {
                            $arResult["PRODUCTS_IMAGES"][$res_arr["ID"]] = "/upload/" . $res_arr["SUBDIR"] . "/" . $res_arr["FILE_NAME"];
                        }


                    }

                    $fp = fopen('resultsLanding.json', 'w');
                    fwrite($fp, json_encode($arResult));
                    fclose($fp);

//            echo "<pre>";
//            print_r($arResult["BRICKS"]["GAZOBLOCKS_PEREGORODKI"]);
//            echo "</pre>";

//
//            echo "<pre>";
//                //print_r($arResult["BRICKS"]["FASADNIY"]);
//                foreach ($arResult["BRICKS"]["FASADNIY"] as $BRICK) {
//                    if (!in_array($BRICK["MAKER_VALUE_ID"], array(481, 498, 502, 134, 495, 483, 505, 504, 503, 507, 506, 630, 800, 801, 802, 1189, 1303, 1547, 2196))) {
//                        print_r($BRICK);
//                        echo "<br>";
//                    }
//                }
//            echo "</pre>";
                }
                ?>


                <h1>Онлайн калькулятор</h1>

                <div class="calc-row">
                    <div class="calc-col__preferences">
                        <div class="calc_floors">
                            <div class="floors_buttons">
                                <button class="floor-btn tsokol-floor__js">Цоколь</button>
                                <button class="floor-btn active first-floor__js regular-floor-btn" data-floorNum="1">1 этаж</button>
                                <button class="floor-btn fronton-floor__js">Фронтоны</button>
                                <button data-toggle="tooltip-calc" data-placement="top" title="Добавить этаж" class="add_floor"></button>
                            </div>

                            <div class="new_calculation">
                                <button class="new_calc new_calc_js">Новый расчет</button>
                            </div>
                        </div>

                        <div class="calc_housePart calc_housePart--basement hidden">
                            <div class="calc_construction">
                                <div class="calc_block calc_block--makers">
                                    <div class="title_row">
                                        <div class="calc__block-title">Выбор кирпича</div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_maker">
                                        <div class="calc_label">Производитель</div>

                                        <div class="calc_makers_row calc_makers_row__makers"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_formats hidden">
                                        <div class="calc_label">Формат кирпича</div>

                                        <div class="calc_option_row calc_option_row--format">
                                            <!-- <div class="calc_option_btn calc_brick_size__js" data-material__size="1НФ">1НФ</div>-->
                                            <!-- <div class="calc_option_btn calc_brick_size__js" data-material__size="1.4НФ">1.4НФ</div>-->
                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_strength_mark hidden">
                                        <div class="calc_label">Марка прочности</div>

                                        <div class="calc_option_row calc_option_row--strength_mark"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_items hidden">
                                        <div class="calc_label">Кирпич</div>
                                        <div class="calc_option_row calc_option_row--items"></div>
                                    </div>
                                </div>

                                <div class="calc_option_sub_block calc_option_sub_block_kladka_parameters hidden">
                                    <div class="title_row">
                                        <div class="calc__block-title">Параметры кладки</div>
                                    </div>

                                    <div class="kladka__types basement__kladka-types">

                                        <div class="kladka__type--item" data-kladkaTypeNumber="1">
                                            <div class="title_row">
                                                <div class="calc__block-title calc__block-title--small">Кладка тип №<span class="kladka_type_number__js">1</span></div>
                                            </div>
                                            <div class="dropdown-input-row">
                                                <div class="input-col input-col__wall-width-js">
                                                    <div class="calc_label">Толщина стен</div>
                                                    <select class="calc_select wall_width_js" name="" id="">
                                                        <option value="0.5">0.5 кирпича (12 см)</option>
                                                        <option value="1">1 кирпич (25 см)</option>
                                                        <option value="1.5">1.5 кирпича (38 см)</option>
                                                        <option value="2">2 кирпича (51 см)</option>
                                                        <option value="2.5">2.5 кирпича (64 см)</option>
                                                        <option value="3">3 кирпича (77 см)</option>
                                                    </select>
                                                </div>

                                                <div class="input-col input-col__basement-perimeter">
                                                    <div class="calc_label">Периметр (м)</div>
                                                    <input type="text" value="1" data-type="floatingNumber">
                                                </div>

                                                <!--                                            <div class="input-col input-col__kladka_height-js" style="display: none;">-->
                                                <!--                                                <div class="calc_label">Высота кладки</div>-->
                                                <!--                                                <select class="calc_select kladka__height__js" name="kladka_height" id=""></select>-->
                                                <!--                                            </div>-->

                                                <div class="input-col input-col__kladka_height-js">
                                                    <div class="calc_label">Высота кладки</div>
                                                    <div class="input-box">
                                                        <span class="input-handler input-handler__minus"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/negative-sign.svg" alt=""></span>
                                                        <input type="text" value="1 ряд (7.50 см)" data-value="1" readonly="readonly">
                                                        <span class="input-handler input-handler__plus"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/plus-sign.svg" alt=""></span>
                                                    </div>
                                                </div>

                                                <div class="input-col input-col__basement-setka">
                                                    <div class="calc_label">Укладывать сетку каждые</div>
                                                    <select class="calc_select setka__rows_js" name="" id="">
                                                        <option value="1">1 ряд</option>
                                                        <option value="2">2 ряда</option>
                                                        <option value="3">3 ряда</option>
                                                        <option value="4">4 ряда</option>
                                                        <option value="5">5 ряда</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <button class="calcAddBtn" data-calcAddWhat="basement-kladka-type">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/add_icon.png" alt="">
                                        <span>Добавить</span>
                                    </button>

                                </div>
                            </div>
                        </div>

                        <div class="calc_housePart calc_housePart--floor calc_housePart--floor__js" data-floorNum="1" data-floorNumberText="floor-1">
                            <div class="calc_perimeter calc_block">
                                <div class="title_row">
                                    <div class="calc__block-title">Периметр этажа №<span class="floorNumber__js">1</span></div>
                                    <div class="calc__block-clear">Очистить</div>
                                </div>

                                <div class="input-group">
                                    <div class="input-col input-col-first">
                                        <label class="calc_label" for="">Периметр этажа (м)</label>
                                        <input type="text" data-input="floorPerimeter" data-type="floatingNumber">
                                    </div>

                                    <div class="input-col">
                                        <label class="calc_label" for="">Высота этажа (м)</label>
                                        <input type="text" data-input="floorHeight" data-type="floatingNumber">
                                    </div>
                                </div>
                            </div>

                            <div class="calc_construction hidden">
                                <div class="calc_block">
                                    <div class="title_row">
                                        <div class="calc__block-title">Конструкция стены №<span class="construction__number__js">1</span></div>
                                        <div class="calc__block-clear">Очистить</div>
                                    </div>

                                    <div class="calc_label">Выберите конструкцию стены</div>
                                    <div class="constructions_options_row">
                                        <div data-construction_type="FASADNIY" class="construction_type_item construction_type_item__js">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/oblic.png" alt="">
                                            <span class="name">Облицовка</span>
                                        </div>

                                        <div data-construction_type="PEREGORODKI" class="construction_type_item construction_type_item__js">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/ryadovoy.png" alt="">
                                            <span class="name">Перегородки</span>
                                        </div>

                                        <div data-construction_type="WALLS" class="construction_type_item construction_type_item__js">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/nesush.png" alt="">
                                            <span class="name">Несущие стены</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="calc_block calc_block--pereghorodkiLength hidden">
                                    <div class="title_row">
                                        <div class="calc__block-title">Длина перегородок</div>
                                    </div>

                                    <div class="input-group">
                                        <div class="input-col input-col-first">
                                            <label class="calc_label" for="">Длина всех перегородок этажа (м)</label>
                                            <input type="text" value="0" data-input="floorPeregorodkiWidth" data-type="floatingNumber">
                                        </div>
                                    </div>
                                </div>

                                <div class="calc_block calc_block--doors">
                                    <div class="title_row">
                                        <div class="calc__block-title">Двери</div>
                                    </div>

                                    <div class="floor__door-item" data-doorNum="1">
                                        <div class="title_row">
                                            <div class="calc__block-title calc__block-title--small">Дверь №<span class="doorNum">1</span></div>
                                            <div class="calc__block-clear">Удалить</div>
                                        </div>

                                        <div class="door_input_row">
                                            <div class="input-col input-col--door__qty">
                                                <div class="calc_label">Кол-во (шт)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>

                                            <div class="input-col input-col--door__height">
                                                <div class="calc_label">Высота (см)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>

                                            <div class="input-col input-col--door__width">
                                                <div class="calc_label">Ширина (см)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>
                                        </div>
                                    </div>

                                    <button class="calcAddBtn" data-calcAddWhat="regularDoorType">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/add_icon.png" alt="">
                                        <span>Добавить</span>
                                    </button>
                                </div>

                                <div class="calc_block calc_block--windows">
                                    <div class="title_row">
                                        <div class="calc__block-title">Окна</div>
                                    </div>

                                    <div class="floor__window-item" data-windowNum="1">

                                        <div class="title_row">
                                            <div class="calc__block-title calc__block-title--small">Окно №<span class="windowNum">1</span></div>
                                            <div class="calc__block-clear">Удалить</div>
                                        </div>

                                        <div class="window_input_row">
                                            <div class="input-col input-col--window__qty">
                                                <div class="calc_label">Кол-во (шт)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>

                                            <div class="input-col input-col--window__height">
                                                <div class="calc_label">Высота (см)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>

                                            <div class="input-col input-col--window__width">
                                                <div class="calc_label">Ширина (см)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>
                                        </div>

                                    </div>

                                    <button class="calcAddBtn" data-calcAddWhat="regularWindowType">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/add_icon.png" alt="">
                                        <span>Добавить</span>
                                    </button>
                                </div>

                                <div class="calc_block calc_block--makers hidden">
                                    <div class="title_row">
                                        <div class="calc__block-title">Выбор материала</div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block__materialType hidden">
                                        <div class="calc_label">Тип материала</div>

                                        <div class="calc_option_row calc_option_row--materialType">
                                            <div class="calc_option_btn_border calc-materialType__js" data-materialType="kirpich">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/ryadovoy.png" alt="">
                                                <img class="peregorod_img" src="<?=SITE_TEMPLATE_PATH?>/assets/img/ryadovoy.png" alt="">
                                                Кирпич
                                            </div>

                                            <div class="calc_option_btn_border calc-materialType__js" data-materialType="keramblock">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/nesush.png" alt="">
                                                <img class="peregorod_img" src="<?=SITE_TEMPLATE_PATH?>/assets/img/peregorodki_keram.jpg" alt="">
                                                Керамблок
                                            </div>

                                            <div class="calc_option_btn_border calc-materialType__js" data-materialType="gazoblock">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/gazoblock.jpg" alt="">
                                                <img class="peregorod_img" src="<?=SITE_TEMPLATE_PATH?>/assets/img/peregorodki_gazoblock.jpg" alt="">
                                                Газоблок
                                            </div>
                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_maker hidden">
                                        <div class="calc_label">Производитель</div>

                                        <div class="calc_makers_row calc_makers_row__makers"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_formats hidden">
                                        <div class="calc_label">Формат кирпича</div>

                                        <div class="calc_option_row calc_option_row--format">
                                            <!--                                        <div class="calc_option_btn calc_brick_size__js" data-material__size="1НФ">1НФ</div>-->
                                            <!--                                        <div class="calc_option_btn calc_brick_size__js" data-material__size="1.4НФ">1.4НФ</div>-->
                                        </div>
                                    </div>


                                    <div class="calc_option_sub_block calc_option_sub_block_surfaces hidden">
                                        <div class="calc_label">Поверхность кирпича</div>

                                        <div class="calc_option_row calc_option_row--surface">
                                            <div class="calc_option_btn">Бархат</div>
                                            <div class="calc_option_btn">Дикий камень</div>
                                            <div class="calc_option_btn">Галич</div>
                                            <div class="calc_option_btn calc-surface__js" data-material__surface="гладкая">Гладкая</div>
                                            <div class="calc_option_btn">Рустик</div>
                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_density_mark hidden">
                                        <div class="calc_label">Марка плотности</div>

                                        <div class="calc_option_row calc_option_row--density_mark"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_width_gb hidden">
                                        <div class="calc_label">Ширина, мм</div>

                                        <div class="calc_option_row calc_option_row--width_gb"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_strength_mark hidden">
                                        <div class="calc_label">Марка прочности</div>

                                        <div class="calc_option_row calc_option_row--strength_mark"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_colorTones hidden">
                                        <div class="calc_label">Оттенок кирпича</div>

                                        <div class="calc_option_row calc_option_row--tone">
                                            <div class="calc_option_btn_border">
                                                <span class="color color-red"></span>
                                                Красный
                                            </div>
                                            <div class="calc_option_btn_border">
                                                <span class="color color-brown"></span>
                                                Коричневый
                                            </div>
                                            <div class="calc_option_btn_border calc-colorTone__js" data-material__colorTone="yellow">
                                                <span class="color color-yellow"></span>
                                                Желтый
                                            </div>
                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_color hidden">
                                        <div class="calc_label">Цвет кирпича</div>
                                        <div class="calc_option_row calc_option_row--color">
                                            <div class="calc_option_btn_border calc-color__js" data-material__color="слоновая кость">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/soloma.png" alt="">
                                                Солома
                                            </div>
                                            <div class="calc_option_btn_border">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/milkyway.png" alt="">
                                                Млечный путь
                                            </div>
                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_items hidden">
                                        <div class="calc_label">Кирпич</div>
                                        <div class="calc_option_row calc_option_row--items">

                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_kladka_parameters hidden">
                                        <div class="title_row">
                                            <div class="calc__block-title">Параметры кладки</div>
                                        </div>

                                        <div class="kladka__types">
                                            <div class="kladka__type--item" data-kladkaTypeNumber="1">
                                                <div class="dropdown-input-row">
                                                    <div class="input-col input-col__wall-width-js">
                                                        <div class="calc_label">Толщина стен</div>
                                                        <select class="calc_select wall_width_js" name="" id="">
                                                            <option value="0.5">0.5 кирпича (12 см)</option>
                                                            <option value="1">1 кирпич (25 см)</option>
                                                            <option value="1.5">1.5 кирпича (38 см)</option>
                                                            <option value="2">2 кирпича (51 см)</option>
                                                            <option value="2.5">2.5 кирпича (64 см)</option>
                                                            <option value="3">3 кирпича (77 см)</option>
                                                        </select>
                                                    </div>

                                                    <div class="input-col input-col__basement-setka">
                                                        <div class="calc_label">Укладывать сетку каждые</div>
                                                        <select class="calc_select setka__rows_js" name="" id="">
                                                            <option value="1">1 ряд</option>
                                                            <option value="2">2 ряда</option>
                                                            <option value="3">3 ряда</option>
                                                            <option value="4">4 ряда</option>
                                                            <option value="5">5 ряда</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="calc-block setka_rastvor">
                                    <div class="title_row">
                                        <div class="calc__block-title">Кладочная сетка</div>
                                    </div>

                                    <div class="dropdown-input-row">
                                        <div class="input-col input-col__setka__rows__js">
                                            <div class="calc_label">Укладывать каждые</div>
                                            <select class="calc_select calc_setka_rows__js" name="" id="">
                                                <option value="1">1 ряд</option>
                                                <option value="2">2 ряда</option>
                                                <option value="3">3 ряда</option>
                                                <option value="4">4 рядов</option>
                                                <option value="5">5 рядов</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="add_new_construction hidden">
                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/plus_grey.svg" alt="">
                                <span>Добавить новую конструкцию</span>
                            </div>
                        </div>

                        <div class="calc_housePart calc_housePart--frontons hidden">
                            <div class="calc_construction">

                                <div class="calc_block calc_block--frontons hidden">
                                    <div class="title_row">
                                        <div class="calc__block-title">Фронтоны</div>
                                    </div>

                                    <div class="fronton-item">
                                        <div class="title_row">
                                            <div class="calc__block-title calc__block-title--small">Фронтон №<span class="frontonNumber">1</span></div>
                                            <div class="calc__block-clear">Удалить</div>
                                        </div>

                                        <div class="calc_label">Тип фронтона</div>

                                        <div class="fronton__types">
                                            <div class="fronton__type fronton__type--triangle" data-frontonType="triangle">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/fronton_triangle.png" alt="">
                                                <span>Треугольные</span>
                                            </div>

                                            <div class="fronton__type fronton__type--trapec" data-frontonType="trapec">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/fronton_trapec.png" alt="">
                                                <span>Трапациевидные</span>
                                            </div>
                                        </div>

                                        <div class="fronton_dimensions_row hidden">
                                            <div class="input-col input-col-quantity">
                                                <div class="calc_label">Кол-во (шт)</div>
                                                <input type="text" data-type="integer">
                                            </div>

                                            <div class="input-col input-col-height">
                                                <div class="calc_label">Высота, Н (м)</div>
                                                <input type="text" data-type="floatingNumber">
                                            </div>

                                            <div class="input-col input-col-baseWidth">
                                                <div class="calc_label">Длина основания, А (м)</div>
                                                <input type="text" data-type="floatingNumber">
                                            </div>

                                            <div class="input-col input-col-rebro hidden">
                                                <div class="calc_label">Длина верхнего ребра, В (м)</div>
                                                <input type="text" data-type="floatingNumber">
                                            </div>
                                        </div>
                                    </div>

                                    <button class="calcAddBtn" data-calcAddWhat="addNewFronton">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/add_icon.png" alt="">
                                        <span>Добавить</span>
                                    </button>
                                </div>

                                <div class="calc_block">
                                    <div class="title_row">
                                        <div class="calc__block-title">Конструкция стены №<span class="construction__number__js">1</span></div>
                                        <div class="calc__block-clear">Очистить</div>
                                    </div>

                                    <div class="calc_label">Выберите конструкцию стены</div>
                                    <div class="constructions_options_row">
                                        <div data-construction_type="FASADNIY" class="construction_type_item construction_type_item__js">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/oblic.png" alt="">
                                            <span class="name">Облицовка</span>
                                        </div>

                                        <div data-construction_type="WALLS" class="construction_type_item construction_type_item__js">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/nesush.png" alt="">
                                            <span class="name">Несущие стены</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="calc_block calc_block--windows">
                                    <div class="title_row">
                                        <div class="calc__block-title">Окна</div>
                                    </div>

                                    <div class="floor__window-item" data-windowNum="1">

                                        <div class="title_row">
                                            <div class="calc__block-title calc__block-title--small">Окно №<span class="windowNum">1</span></div>
                                            <div class="calc__block-clear">Удалить</div>
                                        </div>

                                        <div class="window_input_row">
                                            <div class="input-col input-col--window__qty">
                                                <div class="calc_label">Кол-во (шт)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>

                                            <div class="input-col input-col--window__height">
                                                <div class="calc_label">Высота (см)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>

                                            <div class="input-col input-col--window__width">
                                                <div class="calc_label">Ширина (см)</div>
                                                <input type="text" value="0" data-type="integer">
                                            </div>
                                        </div>

                                    </div>

                                    <button class="calcAddBtn" data-calcAddWhat="regularWindowType">
                                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/add_icon.png" alt="">
                                        <span>Добавить</span>
                                    </button>
                                </div>

                                <div class="calc_block calc_block--makers hidden">
                                    <div class="title_row">
                                        <div class="calc__block-title">Выбор материала</div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block__materialType hidden">
                                        <div class="calc_label">Тип материала</div>

                                        <div class="calc_option_row calc_option_row--materialType">
                                            <div class="calc_option_btn_border calc-materialType__js" data-materialType="kirpich">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/ryadovoy.png" alt="">
                                                <img class="peregorod_img" src="<?=SITE_TEMPLATE_PATH?>/assets/img/ryadovoy.png" alt="">
                                                Кирпич
                                            </div>

                                            <div class="calc_option_btn_border calc-materialType__js" data-materialType="keramblock">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/nesush.png" alt="">
                                                <img class="peregorod_img" src="<?=SITE_TEMPLATE_PATH?>/assets/img/peregorodki_keram.jpg" alt="">
                                                Керамблок
                                            </div>

                                            <div class="calc_option_btn_border calc-materialType__js" data-materialType="gazoblock">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/gazoblock.jpg" alt="">
                                                <img class="peregorod_img" src="<?=SITE_TEMPLATE_PATH?>/assets/img/peregorodki_gazoblock.jpg" alt="">
                                                Газоблок
                                            </div>
                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_maker hidden">
                                        <div class="calc_label">Производитель</div>

                                        <div class="calc_makers_row calc_makers_row__makers"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_formats hidden">
                                        <div class="calc_label">Формат кирпича</div>

                                        <div class="calc_option_row calc_option_row--format"></div>
                                    </div>


                                    <div class="calc_option_sub_block calc_option_sub_block_surfaces hidden">
                                        <div class="calc_label">Поверхность кирпича</div>

                                        <div class="calc_option_row calc_option_row--surface"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_density_mark hidden">
                                        <div class="calc_label">Марка плотности</div>

                                        <div class="calc_option_row calc_option_row--density_mark"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_width_gb hidden">
                                        <div class="calc_label">Ширина, мм</div>

                                        <div class="calc_option_row calc_option_row--width_gb"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_strength_mark hidden">
                                        <div class="calc_label">Марка прочности</div>

                                        <div class="calc_option_row calc_option_row--strength_mark"></div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_colorTones hidden">
                                        <div class="calc_label">Оттенок кирпича</div>

                                        <div class="calc_option_row calc_option_row--tone">
                                            <div class="calc_option_btn_border">
                                                <span class="color color-red"></span>
                                                Красный
                                            </div>
                                            <div class="calc_option_btn_border">
                                                <span class="color color-brown"></span>
                                                Коричневый
                                            </div>
                                            <div class="calc_option_btn_border calc-colorTone__js" data-material__colorTone="yellow">
                                                <span class="color color-yellow"></span>
                                                Желтый
                                            </div>
                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_color hidden">
                                        <div class="calc_label">Цвет кирпича</div>
                                        <div class="calc_option_row calc_option_row--color">
                                            <div class="calc_option_btn_border calc-color__js" data-material__color="слоновая кость">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/soloma.png" alt="">
                                                Солома
                                            </div>
                                            <div class="calc_option_btn_border">
                                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/milkyway.png" alt="">
                                                Млечный путь
                                            </div>
                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_items hidden">
                                        <div class="calc_label">Кирпич</div>
                                        <div class="calc_option_row calc_option_row--items">

                                        </div>
                                    </div>

                                    <div class="calc_option_sub_block calc_option_sub_block_kladka_parameters hidden">
                                        <div class="title_row">
                                            <div class="calc__block-title">Параметры кладки</div>
                                        </div>

                                        <div class="kladka__types">
                                            <div class="kladka__type--item" data-kladkaTypeNumber="1">
                                                <div class="dropdown-input-row">
                                                    <div class="input-col input-col__wall-width-js">
                                                        <div class="calc_label">Толщина стен</div>
                                                        <select class="calc_select wall_width_js" name="" id="">
                                                            <option value="0.5">0.5 кирпича (12 см)</option>
                                                            <option value="1">1 кирпич (25 см)</option>
                                                            <option value="1.5">1.5 кирпича (38 см)</option>
                                                            <option value="2">2 кирпича (51 см)</option>
                                                            <option value="2.5">2.5 кирпича (64 см)</option>
                                                            <option value="3">3 кирпича (77 см)</option>
                                                        </select>
                                                    </div>

                                                    <div class="input-col input-col__basement-setka">
                                                        <div class="calc_label">Укладывать сетку каждые</div>
                                                        <select class="calc_select setka__rows_js" name="" id="">
                                                            <option value="1">1 ряд</option>
                                                            <option value="2">2 ряда</option>
                                                            <option value="3">3 ряда</option>
                                                            <option value="4">4 ряда</option>
                                                            <option value="5">5 ряда</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="add_new_construction hidden">
                                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/plus_grey.svg" alt="">
                                <span>Добавить новую конструкцию</span>
                            </div>
                        </div>
                    </div>

                    <div class="calc-col__results" id="calcResults">
                        <div class="calc-col__results__title">Результат расчета:</div>
                        <div class="gray-line"></div>

                        <div class="results__construction__type hidden" data-resultsConstructionType="fasadniy">
                            <div class="results__construction__title">
                                <div class="text">Облицовочный кирпич</div>
                                <span><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/arrow_up_calc.svg" alt=""></span>
                            </div>

                            <div class="result__lines hidden" data-floorNumber="1">

                                <div class="floorNumber">1 этаж</div>

                                <div class="result__line result__line--maker__js">
                                    <div class="result_name">Производитель</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--format__js">
                                    <div class="result_name">Формат</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--surface__js">
                                    <div class="result_name">Поверхность</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--color__js">
                                    <div class="result_name">Цвет</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--bricksTotal__js">
                                    <div class="result_name">Кол-ва кирпича, шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonDivisibleQnty__js">
                                    <div class="result_name">Кол-во (кратно поддонам), шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonQnty__js">
                                    <div class="result_name">Кол-во на поддоне, шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonAmount__js">
                                    <div class="result_name">Кол-во поддонов, шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonPrice__js">
                                    <div class="result_name">Цена поддона, руб.</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--totalCost__js">
                                    <div class="result_name">Общая стоимость, руб.</div>
                                    <div class="result_value"></div>
                                </div>
                            </div>

                            <!--                        <button class="add2Compare_calc">Добавить в сравнение</button>-->
                        </div>

                        <div class="results__construction__type hidden" data-resultsConstructionType="walls">
                            <div class="results__construction__title">
                                <div class="text">Несущие стены</div>
                                <span><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/arrow_up_calc.svg" alt=""></span>
                            </div>

                            <div class="result__lines hidden" data-floorNumber="1">

                                <div class="floorNumber">1 этаж</div>

                                <div class="result__line result__line--maker__js">
                                    <div class="result_name">Производитель</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--format__js">
                                    <div class="result_name">Формат</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--strengthMark__js">
                                    <div class="result_name">Марка прочности</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--densityMark__js">
                                    <div class="result_name">Марка плотности</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--bricksTotal__js">
                                    <div class="result_name">Кол-ва <span>кирпича, шт</span></div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonDivisibleQnty__js">
                                    <div class="result_name">Кол-во (кратно поддонам), <span>шт</span></div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonQnty__js">
                                    <div class="result_name">Кол-во на поддоне, <span>шт</span></div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonAmount__js">
                                    <div class="result_name">Кол-во поддонов, шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonPrice__js">
                                    <div class="result_name">Цена поддона, руб.</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--totalCost__js">
                                    <div class="result_name">Общая стоимость, руб.</div>
                                    <div class="result_value"></div>
                                </div>
                            </div>
                        </div>

                        <div class="results__construction__type hidden" data-resultsConstructionType="peregorodki">
                            <div class="results__construction__title">
                                <div class="text">Перегородки</div>
                                <span><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/arrow_up_calc.svg" alt=""></span>
                            </div>

                            <div class="result__lines hidden" data-floorNumber="1">

                                <div class="floorNumber">1 этаж</div>

                                <div class="result__line result__line--maker__js">
                                    <div class="result_name">Производитель</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--format__js">
                                    <div class="result_name">Формат</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--strengthMark__js">
                                    <div class="result_name">Марка прочности</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--bricksTotal__js">
                                    <div class="result_name">Кол-ва <span>кирпича, шт</span></div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonDivisibleQnty__js">
                                    <div class="result_name">Кол-во (кратно поддонам), <span>шт</span></div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonQnty__js">
                                    <div class="result_name">Кол-во на поддоне, <span>шт</span></div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonAmount__js">
                                    <div class="result_name">Кол-во поддонов, шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonPrice__js">
                                    <div class="result_name">Цена поддона, руб.</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--totalCost__js">
                                    <div class="result_name">Общая стоимость, руб.</div>
                                    <div class="result_value"></div>
                                </div>
                            </div>
                        </div>

                        <div class="results__construction__type hidden" data-resultsConstructionType="tsokolny">
                            <div class="results__construction__title">
                                <div class="text">Цоколь</div>
                                <span><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/arrow_up_calc.svg" alt=""></span>
                            </div>

                            <div class="result__lines">
                                <div class="result__line result__line--maker__js">
                                    <div class="result_name">Производитель</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--format__js">
                                    <div class="result_name">Формат</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--strengthMark__js">
                                    <div class="result_name">Марка прочности</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--bricksTotal__js">
                                    <div class="result_name">Кол-ва кирпича, шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonDivisibleQnty__js">
                                    <div class="result_name">Кол-во (кратно поддонам), шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonQnty__js">
                                    <div class="result_name">Кол-во на поддоне, шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonAmount__js">
                                    <div class="result_name">Кол-во поддонов, шт</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--poddonPrice__js">
                                    <div class="result_name">Цена поддона, руб.</div>
                                    <div class="result_value"></div>
                                </div>

                                <div class="result__line result__line--totalCost__js">
                                    <div class="result_name">Общая стоимость, руб.</div>
                                    <div class="result_value"></div>
                                </div>
                            </div>
                        </div>

                        <div class="final__price">
                            <span class="final-text">Итого:</span>
                            <div class="final_price_value"><span>0</span> руб.</div>
                        </div>

                        <button id="printResultsBtn">Распечатать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?endif;?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
