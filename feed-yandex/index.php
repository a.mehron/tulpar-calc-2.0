<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Фиды");
?>

<div class="p-3">
    <pre>
    <?
        if(CModule::IncludeModule('iblock'))
        {
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "DETAIL_PAGE_URL", "PREVIEW_PICTURE", "CATALOG_GROUP_1");
            $arFilter = Array("IBLOCK_ID"=>IntVal(4), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID" => Array(18,22, 482, 465, 478, 472, 474, 480, 469, 490));

            $res = CIBlockElement::GetList(
                Array("SORT" => "ASC"),
                $arFilter,
                false,
                false,
                $arSelect
            );

            while($ob = $res->GetNextElement()){
                $arItem = $ob->GetFields();
                $arItem["PROPERTIES"] = $ob->GetProperties();
                $arResult[] = $arItem;
            }

            //print_r($arResult);
            //echo sizeof($arResult);
        }
        ?>
    </pre>

    <table id="feedtable">
        <thead>
            <tr>
                <th>id</th>
                <th>title</th>
                <th>description</th>
                <th>link</th>
                <th>image_link</th>
                <th>availability</th>
                <th>price</th>
                <th>google_product_category</th>
                <th>product_type</th>
                <th>brand</th>
            </tr>
        </thead>
        <tbody>
        <?php
            $fp = fopen('yandex_feed_tulpar.csv', 'w');
            fprintf($fp, chr(0xEF).chr(0xBB).chr(0xBF));
            $feed_header = array('ID','Title', 'URL', 'Image', 'Description', 'Price', 'Currency');

            fputcsv($fp, $feed_header);
            foreach ($arResult as $item) {
              //не включать основные товары
              if ($item['PROPERTIES']['ATT_IS_MAIN_ITEM']['VALUE'] == "Да") {
                continue;
              }

                echo "<tr>";
                echo "<td>".$item['ID']."</td>";
                echo "<td>".$item['NAME']."</td>";
                echo "<td>Купить кровельные материалы с доставкой по ценам от производителя!</td>";
                // IDEA: check catalog.item template to see how we receive the URL of offer

                //get item url
                $res = CIBlockElement::GetByID($item['PROPERTIES']['ATT_RELATED_MAIN_ITEM']['VALUE']);
              	if($ar_res = $res->GetNext())
              		$offer_url = $ar_res['DETAIL_PAGE_URL']."?OFFER_ID=".$ar_res['ID'];

              		if ($offer_url != "") {
              			$page_url = $offer_url;
              		} else {
              			$page_url = $item['DETAIL_PAGE_URL'];
              		}

                echo "<td>http://tulpar-trade.ru".$page_url."</td>";

                //get image src
                $image_url = CFile::GetPath($item['PREVIEW_PICTURE']);
                echo "<td>http://tulpar-trade.ru".$image_url."</td>";

                echo "<td>in_stock</td>"; //availability

                echo "<td>".$item['CATALOG_PRICE_1']." RUB</td>"; //price

                echo "<td>3031</td>"; //google_product_category

                $resSection = CIBlockSection::GetNavChain(false, $item['IBLOCK_SECTION_ID']);

                $product_type = "Главная > Каталог";
                echo "<td>Главная > Каталог";
                  while ($arSection = $resSection->GetNext()) {
                    // echo "<pre>";
                    // print_r($arSection);
                    // echo "<pre>";
                    $product_type = $product_type . " > ".$arSection['NAME'];
                    echo " > ".$arSection['NAME'];
                  }
                echo "</td>";

                echo "<td>".$item["PROPERTIES"]['ATT_MAKER']['VALUE']."</td>";

                echo "</tr>";

                $fields = array(
                                $item['ID'],
                                $item['NAME'],
                                'http://tulpar-trade.ru'.$page_url,
                                'http://tulpar-trade.ru'.$image_url,
                                'Купить кирпич и блоки с доставкой по ценам от производителя!',
                                $item['CATALOG_PRICE_1'],
                                'RUB'
                              );

                fputcsv($fp, $fields);
            }
            fclose($fp);
         ?>
        </tbody>
    </table>

</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
