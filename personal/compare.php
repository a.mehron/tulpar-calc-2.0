<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Торговая компания ТУЛПАР / Сравнение товаров");
?>

<script>
	$(document).ready(function(){
		$.each($(`.prop-item`), function(i) {
		   var h = 0;
		   $.each($(`.prop-item:nth-child(${i})`), function() {
			 h = $(this).height() > h ? $(this).height() : h;
		   });
		   $(`.prop-item:nth-child(${i})`).css("height", h);
		 });

		$(".item_name.p-item").each(function(i) {
			var h = $(`.prop-item:nth-child(${i+1})`).height();
			if (h > $(this).height()) {
				$(this).css("height", h);
			} else {
				$(`.prop-item:nth-child(${i+1})`).css("height", $(this).height());
			}
		});

		$(".prop-item").hover(function(){
			$(this).css("background", "#f7f7f7");
			var order = $(this).index() + 1;
			$(".prop-item:nth-child("+order+")").css("background", "#f7f7f7");
			$(".item_name:nth-child("+(order+1)+")").css("background", "#f7f7f7");
		}, function(){
			$(this).css("background", "#fff");
			var order = $(this).index() + 1;
			$(".prop-item:nth-child("+order+")").css("background", "#fff");
			$(".item_name:nth-child("+(order+1)+")").css("background", "#fff");
		});

		$(".item_name.p-item").hover(function(){
			$(this).css("background", "#f7f7f7");
			var order = $(this).index();
			$(".prop-item:nth-child("+order+")").css("background", "#f7f7f7");
		}, function(){
			$(this).css("background", "#fff");
			var order = $(this).index();
			$(".prop-item:nth-child("+order+")").css("background", "#fff");
		});

	});
</script>

<div class="gray-wrapper compare-page">

<div class="container">
	<div class="other-pages">

		<div class="row">
			<h1 class="page-title">Сравнение товаров</h1>

		</div>

		<?php if (sizeof($_SESSION['cmp_element_ids']) != 0): ?>
			<?
			global $arrFilter;
			$arrFilter = array("=ID" => $_SESSION['cmp_element_ids']);


			$APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				"compare_list",
				array(
					"ACTION_VARIABLE" => "action",
					"ADD_PICT_PROP" => "-",
					"ADD_PROPERTIES_TO_BASKET" => "Y",
					"ADD_SECTIONS_CHAIN" => "N",
					"ADD_TO_BASKET_ACTION" => "ADD",
					"AJAX_MODE" => "N",
					"AJAX_OPTION_ADDITIONAL" => "",
					"AJAX_OPTION_HISTORY" => "N",
					"AJAX_OPTION_JUMP" => "N",
					"AJAX_OPTION_STYLE" => "Y",
					"BACKGROUND_IMAGE" => "-",
					"BASKET_URL" => "/personal/cart/",
					"BROWSER_TITLE" => "-",
					"CACHE_FILTER" => "N",
					"CACHE_GROUPS" => "Y",
					"CACHE_TIME" => "36000000",
					"CACHE_TYPE" => "A",
					"COMPATIBLE_MODE" => "Y",
					"CONVERT_CURRENCY" => "N",
					"CUSTOM_FILTER" => "",
					"DETAIL_URL" => "",
					"DISABLE_INIT_JS_IN_COMPONENT" => "N",
					"DISPLAY_BOTTOM_PAGER" => "Y",
					"DISPLAY_COMPARE" => "N",
					"DISPLAY_TOP_PAGER" => "N",
					"ELEMENT_SORT_FIELD" => "sort",
					"ELEMENT_SORT_FIELD2" => "id",
					"ELEMENT_SORT_ORDER" => "asc",
					"ELEMENT_SORT_ORDER2" => "desc",
					"ENLARGE_PRODUCT" => "PROP",
					"FILTER_NAME" => "arrFilter",
					"HIDE_NOT_AVAILABLE" => "N",
					"HIDE_NOT_AVAILABLE_OFFERS" => "N",
					"IBLOCK_ID" => "4",
					"IBLOCK_TYPE" => "catalog",
					"INCLUDE_SUBSECTIONS" => "Y",
					"LABEL_PROP" => array(
					),
					"LAZY_LOAD" => "N",
					"LINE_ELEMENT_COUNT" => "3",
					"LOAD_ON_SCROLL" => "N",
					"MESSAGE_404" => "",
					"MESS_BTN_ADD_TO_BASKET" => "В корзину",
					"MESS_BTN_BUY" => "Купить",
					"MESS_BTN_DETAIL" => "Подробнее",
					"MESS_BTN_SUBSCRIBE" => "Подписаться",
					"MESS_NOT_AVAILABLE" => "Нет в наличии",
					"META_DESCRIPTION" => "-",
					"META_KEYWORDS" => "-",
					"OFFERS_CART_PROPERTIES" => array(
					),
					"OFFERS_FIELD_CODE" => array(
						0 => "CODE",
						1 => "NAME",
						2 => "",
					),
					"OFFERS_LIMIT" => "5",
					"OFFERS_PROPERTY_CODE" => array(
						0 => "ATT_COLOR",
						1 => "",
					),
					"OFFERS_SORT_FIELD" => "sort",
					"OFFERS_SORT_FIELD2" => "id",
					"OFFERS_SORT_ORDER" => "asc",
					"OFFERS_SORT_ORDER2" => "desc",
					"PAGER_BASE_LINK_ENABLE" => "N",
					"PAGER_DESC_NUMBERING" => "N",
					"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
					"PAGER_SHOW_ALL" => "N",
					"PAGER_SHOW_ALWAYS" => "N",
					"PAGER_TEMPLATE" => ".default",
					"PAGER_TITLE" => "Товары",
					"PAGE_ELEMENT_COUNT" => "19",
					"PARTIAL_PRODUCT_PROPERTIES" => "N",
					"PRICE_CODE" => array(
						0 => "BASE",
					),
					"PRICE_VAT_INCLUDE" => "Y",
					"PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
					"PRODUCT_DISPLAY_MODE" => "Y",
					"PRODUCT_ID_VARIABLE" => "id",
					"PRODUCT_PROPERTIES" => array(
					),
					"PRODUCT_PROPS_VARIABLE" => "prop",
					"PRODUCT_QUANTITY_VARIABLE" => "quantity",
					"PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
					"PRODUCT_SUBSCRIPTION" => "Y",
					"PROPERTY_CODE" => array(
						0 => "",
						1 => "",
					),
					"PROPERTY_CODE_MOBILE" => array(
					),
					"RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
					"RCM_TYPE" => "personal",
					"SECTION_CODE" => "",
					"SECTION_ID" => "",
					"SECTION_ID_VARIABLE" => "SECTION_ID",
					"SECTION_URL" => "",
					"SECTION_USER_FIELDS" => array(
						0 => "",
						1 => "",
					),
					"SEF_MODE" => "N",
					"SET_BROWSER_TITLE" => "N",
					"SET_LAST_MODIFIED" => "N",
					"SET_META_DESCRIPTION" => "Y",
					"SET_META_KEYWORDS" => "Y",
					"SET_STATUS_404" => "N",
					"SET_TITLE" => "Y",
					"SHOW_404" => "N",
					"SHOW_ALL_WO_SECTION" => "Y",
					"SHOW_CLOSE_POPUP" => "N",
					"SHOW_DISCOUNT_PERCENT" => "N",
					"SHOW_FROM_SECTION" => "N",
					"SHOW_MAX_QUANTITY" => "N",
					"SHOW_OLD_PRICE" => "N",
					"SHOW_PRICE_COUNT" => "1",
					"SHOW_SLIDER" => "Y",
					"SLIDER_INTERVAL" => "3000",
					"SLIDER_PROGRESS" => "N",
					"TEMPLATE_THEME" => "blue",
					"USE_ENHANCED_ECOMMERCE" => "N",
					"USE_MAIN_ELEMENT_SECTION" => "N",
					"USE_PRICE_COUNT" => "N",
					"USE_PRODUCT_QUANTITY" => "N",
					"COMPONENT_TEMPLATE" => ".default2",
					"ENLARGE_PROP" => "-",
					"OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
					"OFFER_TREE_PROPS" => array(
						0 => "ATT_COLOR",
					)
				),
				false
			);?>
		<?php else: ?>
			<div class="row">
				<div class="wish_error">
					<p class="text-success">Вы еще не добавили товары в список сравнения. Сделать это вы можете из нашего Каталога товаров.</p>
				</div>
			</div>
		<?php endif; ?>


	</div>
</div>

</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
