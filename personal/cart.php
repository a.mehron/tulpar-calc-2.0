<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
?>
<?php
$cart = json_decode($_COOKIE['cart'], true);

if(!CModule::IncludeModule("iblock"))
return;

if(!CModule::IncludeModule("catalog"))
return;
?>

<div class="gray-wrapper">
    <div class="container pl-0 pr-0">
        <?php if (sizeof($cart) > 0): ?>
        <div class="other-pages cart">
            <div class="product-table-cart">
                <div class="product-table-header">
                    <div class="product-table-col product-table-col_img">Изображение товара</div>
                    <div class="product-table-col product-table-col_name">Наименование товара</div>
                    <div class="product-table-col product-table-col_price">Цена</div>
                    <div class="product-table-col product-table-col_qty">Количество</div>
                    <div class="product-table-col product-table-col_coef"></div>
                    <div class="product-table-col product-table-col_total">Итого</div>
                    <div class="product-table-col product-table-col_remove"></div>
                </div>

                <?php
                    $cart_total = 0;
                    foreach ($cart as $cart_item):

                        $measure = "";

                        $intElementID = $cart_item['ID']; // ID предложения
                        $mxResult = CCatalogSku::GetProductInfo($intElementID);
                        if (is_array($mxResult))
                        {
                            $section_id;
                            $res = CIBlockElement::GetByID($mxResult['ID']);
                            if($ar_res = $res->GetNext()) {
                              $section_id = $ar_res['IBLOCK_SECTION_ID'];
                            }
                        }
                        else
                        {
                            ShowError('Это не торговое предложение');
                        }

                        if ($section_id == 18 || $section_id == 22) {
                            $measure = " м<sup>2</sup>";
                        } elseif ($section_id == 21 || $section_id == 190 || $section_id == 146 || $section_id == 209) {
                            $measure = " поддон <span class='kolvo'>(" .$cart_item['PODDON_QTY']." шт.)</span>";
                        }
                ?>

                    <div class="product-table-row" offer-id="<?=$cart_item['ID']?>">
                        <div class="product-table-col product-table-col_img">
                            <div class="img-wrap">
                                <a href="<?=$cart_item['URL'].'?OFFER_ID='.$cart_item['ID']?>" title="<?=$cart_item['NAME']?>">
                                    <img src="<?=$cart_item['IMG']?>">
                                </a>
                            </div>
                        </div>
                        <div class="product-table-col product-table-col_name">
                            <a href="<?=$cart_item['URL'].'?OFFER_ID='.$cart_item['ID']?>"><?=$cart_item['NAME']?></a>
                        </div>
                        <div class="product-table-col product-table-col_price">
                            <span class="price-num" measure-ratio="<?=$cart_item['PODDON_QTY']?>"><?=$cart_item['PRICE']?></span>
                            <span class="price-ico"> ₽</span>
                        </div>
                        <div class="product-table-col product-table-col_qty">
                            <div class="change-qty">
                                <span class="minus"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/minus_bold_gray.svg" alt=""></span>
                                <input type="text" name="elem_<?=$cart_item['ID']?>_qty" value="1">
                                <span class="plus"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/plus_bold_gray.svg" alt=""></span>
                                <span class="measure"><?=$measure?></span>
                            </div>
                        </div>
                        <div class="product-table-col product-table-col_coef"></div>
                        <div class="product-table-col product-table-col_total">
                            <span class="price-num"><?=$cart_item['PRICE'] * $cart_item['PODDON_QTY']?></span>
                            <span class="price-ico"> ₽</span>
                        </div>
                        <div class="product-table-col product-table-col_remove">
                            <button type="button" class="close" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                <?php
                    $cart_total += $cart_item['PRICE']*$cart_item['PODDON_QTY'];
                    endforeach; ?>
            </div>

            <div class="cart-total">
                <div class="total-sum">
                    <span class="text">Итого:</span>
                    <span class="num"><?=$cart_total?></span>
                    <span class="ico"> ₽</span>
                </div>

                <button data-fancybox data-src="#send-cart" id="cart-order" class="btn btn-default">Оформить заказ</button>
            </div>
        </div>
        <?php else: ?>
         <div class="other-pages">
             <h1 class="page-title">Корзина</h1>
             <p>Ваша корзина пуста. Выберите нужный Вам товар из <a href="/catalog/">каталога Интернет-магазина</a> и нажмите кнопку "В корзину".</p>
         </div>
        <?php endif; ?>
    </div>
</div>


<script>
    function updateTotal(){
        var new_total = 0;
        $(".cart .product-table-row .product-table-col_total .price-num").each(function(){
            new_total += parseFloat($(this).text());
        });
        new_total = new_total.toFixed(2);
        new_total = parseFloat(new_total);
        new_total = new_total.toLocaleString();
        $(".cart-total .total-sum .num").text(new_total);
    };

    $(".product-table-col_qty .plus").click(function(){
        var cur_input_val = $(this).siblings("input").val();
        var measure_ratio = parseInt($(this).closest(".product-table-row").find(".product-table-col_price span.price-num").attr('measure-ratio'));
        $(this).siblings("input").val(parseInt(cur_input_val) + 1);
        var cur_product_total = parseFloat($(this).closest(".product-table-row").find(".product-table-col_total span.price-num").text());
        var price_per_unit = parseFloat($(this).closest(".product-table-row").find(".product-table-col_price span.price-num").text());
        $(this).closest(".product-table-row").find(".product-table-col_total span.price-num").text(Number(cur_product_total + price_per_unit*measure_ratio).toFixed(2));
        $(this).next('.measure').find('.kolvo').text("(" + parseInt(cur_input_val*measure_ratio + measure_ratio) + " шт.)");

        updateTotal();
    });


    $(".product-table-col_qty .minus").click(function(){
        var cur_input_val = $(this).siblings("input").val();
        var measure_ratio = parseInt($(this).closest(".product-table-row").find(".product-table-col_price span.price-num").attr('measure-ratio'));
        if (cur_input_val > 1) {
            $(this).siblings("input").val(parseFloat(cur_input_val) - 1);
            var cur_product_total = parseFloat($(this).closest(".product-table-row").find(".product-table-col_total span.price-num").text());
            var price_per_unit = parseFloat($(this).closest(".product-table-row").find(".product-table-col_price span.price-num").text());
            $(this).closest(".product-table-row").find(".product-table-col_total span.price-num").text(Number(cur_product_total - price_per_unit*measure_ratio).toFixed(2));
            $(this).siblings('.measure').find('.kolvo').text("(" + parseInt(cur_input_val*measure_ratio - measure_ratio) + " шт.)");

            updateTotal();
        }
    });

    $(".product-table-col_qty input").change(function(){
        //console.log($(this).val());
        var upd_qty = parseFloat($(this).val());
        var price_per_unit = parseFloat($(this).closest(".product-table-row").find(".product-table-col_price span.price-num").text());
        $(this).closest(".product-table-row").find(".product-table-col_total span.price-num").text(price_per_unit*upd_qty);

        updateTotal();
    });

    $("#cart-order").click(function(){
        var items = [];
        $(".cart .product-table-row").each(function(){
            var name = $(this).find(".product-table-col_name a").text();
            var price = $(this).find(".product-table-col_price .price-num").text() + " руб";
            var qty = $(this).find(".product-table-col_qty input").val();

            var order_item = name + " \t" + price + " \t " + qty;
            items.push(order_item);
        });
        $("#cart_items_input").val(items);

    });

    $("#cart_form").on("submit", function(e){
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/local/ajax/send_cart.php',
            data: $('form').serialize(),
            success:function() {
                $("h2.modal-title").hide();
                $('#cart_form').hide();
                $('#cart_form .footer-default').hide();
                $('#cart_form .success-msg').show();
            }
        });

    });
</script>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
