<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("title", "Контакты – ООО \"Тулпар\" #SOTBIT_REGIONS_UF_IN_CITY#");
$APPLICATION->SetPageProperty("keywords", "Контакты Тулпар, Тулпар #SOTBIT_REGIONS_NAME#, режим работы, адрес, строительство, интернет-магазин");
$APPLICATION->SetPageProperty("description", "Контакты – Тулпар #SOTBIT_REGIONS_UF_IN_CITY#	. Все для строительства, ремонта и обустройства дома.");
$APPLICATION->SetTitle("Контакты");
$city = getCityData();
?><div class="gray-wrapper">
	<div class="container contacts-container">
		<div class="row">
            <!--
            <?
            $map = $_SESSION["SOTBIT_REGIONS"]["MAP_YANDEX"];
            $map['API_KEY'] = '2e144472-190f-4ae0-81b8-77af6a28644b';

            ?>
            -->
<?if(!empty($map['API_KEY']) && isset($map[0]) && !empty($map[0]['VALUE'])):?>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU&amp;apikey=<?=$map['API_KEY']?>" type="text/javascript"></script>
    <script>
        ymaps.ready(init);

        function init() {
            var myMap = new ymaps.Map("map", {
                    center: [<?=$map[0]['VALUE']?>],
                    zoom: 14
                }, {
                    searchControlProvider: 'yandex#search'
                }),

                myGeoObject = new ymaps.GeoObject({
                    // Описание геометрии.
                    geometry: {
                        type: "Point",
                        coordinates: [<?=$map[0]['VALUE']?>]
                    },
                    // Свойства.
                    properties: {
                        iconContent: 'ООО «Тулпар»',
                    }
                }, {
                    // Иконка метки будет растягиваться под размер ее содержимого.
                    preset: 'islands#redStretchyIcon',
                    //draggable: true
                });

            myMap.geoObjects
                .add(myGeoObject);
        }
    </script>
<?endif?>
            <div class="col-lg-4 p-0 contacts__map" id="map">
                <?if(!(!empty($map['API_KEY']) && isset($map[0]) && !empty($map[0]['VALUE']))):?>
				<iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A75a19be1c9669122b22b8c148ba8b1ccc9fbd93fbb542a89247477ca72d7cfc7&amp;source=constructor" width="100%" height="100%" frameborder="0"></iframe>
				<?endif;?>
			</div>
			<div class="col-lg-4 pt-3 pb-3 pl-4 pr-4 contacts__address">
				<h1>Контакты</h1>
				<div class="tel contact-unit">
 <img src="/local/templates/tulpar_store/assets/img/icons/icomoon/phone_contacts.svg" alt="">
					<div class="contact-unit_fa">
 <small>Телефон</small> <a href="tel:<?=$city['phonelink']?>" id="podmena_cont"><?=$city['phoneblur']?> отдел продаж</a>
                        <div class="show-full-phone"><span>Показать телефон</span></div>
					</div>
				</div>
				<div class="hours contact-unit">
 <img src="/local/templates/tulpar_store/assets/img/icons/icomoon/watch-2.svg" alt="">
					<div class="contact-unit_fa">
 <small>Режим работы</small>
                        <br> <span><b><?=strip_tags($city['time'])?></b></span>
					</div>
				</div>
				<div class="address contact-unit">
                     <img src="/local/templates/tulpar_store/assets/img/icons/icomoon/contacts.svg" alt="">
                      <div class="contact-unit_fa">
                         <small>Адрес</small>
                         <br> <span><b><?=strip_tags($city['address'])?></b></span>
                    </div>
				</div>

                <div class="email contact-unit">
                    <img src="/local/templates/tulpar_store/assets/img/icons/icomoon/envelope.svg" alt="">
                    <div class="contact-unit_fa">
                        <small>E-mail</small>
                        <br> <b><a href="mailto:<?=$city['email']?>"><?=$city['email']?></a></b>
                    </div>
                </div>

				<div class="rekvisits">
 <small>Реквизиты компании</small>
					<?=$city['props']?>
				</div>
			</div>
			<div class="col-lg-4 pt-lg-3 pb-lg-3 pl-lg-5 pr-lg-5 form-send border-left">
				<div class="title">
					 Пишите нам
				</div>
				<form id="contacts_form">
 					<input type="text" placeholder="Ваше имя" required="" name="write_us_name" value="">
					<input type="email" placeholder="Ваша эл. почта" required="" name="write_us_email" value="">
					<input type="tel" placeholder="Ваш телефон" required="" name="write_us_phone" value="">
					<textarea name="write_message" placeholder="Сообщение"></textarea> 
					<input type="submit" name="" value="Перезвоните мне">
				</form>
			</div>
		</div>
	</div>
</div>
 <!-- Modal -->
<div class="modal fade" id="contactsFormSendSuccessMessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
 <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
			</div>
			<div class="modal-body">
				<h3>Ваша заявка успешно отправлена!</h3>
			</div>
			<div class="modal-footer">
 <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>