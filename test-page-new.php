<?

use Bitrix\Main\Mail\Event;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION -> SetPageProperty("keywords_inner", "Тестовая страница");
$APPLICATION -> SetPageProperty("title", "Тестовая страница");
$APPLICATION -> SetPageProperty("keywords", "Тестовая страница");
$APPLICATION -> SetPageProperty("description", "Тестовая страница");
$APPLICATION -> SetTitle("test-page-new");
?>

<?

Event ::sendImmediate(array(
	"EVENT_NAME" => "USER_INFO",
	"LID" => "s1",
	"C_FIELDS" => array(
		"EMAIL" => "sotnikovka@gmail.com",
	),
));
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>