<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetPageProperty("keywords", "Как оплатить, Казань, строительство, оплата, банковская карта, расчет, онлайн-оплата, подтверждение оплаты");
$APPLICATION->SetPageProperty("description", "В данном разделе сайта Вы узнаете о возможных способах оплаты и о возможности вернуть часть потраченной суммы от государства");
$APPLICATION->SetTitle("Оплата");
?>

<div class="gray-wrapper">
    <div class="container p-0">
        <div class="row bg-white justify-content-center p-3">
            <div class="col-lg-8 p-0">
                <a class="fancybox-section" data-fancybox href="https://www.youtube.com/watch?v=k-EwioaAW4s/">
                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/video_payment.jpg" alt="">
                    <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/play-yellow.png" alt="" class="play-btn">
                </a>

                <h2 class="page-section">Оплата</h1>

                    <p>В нашей компании мы внедрили все способы оплаты, кроме биткоинов:
                    <ul class="pay-types">
                        <li><img src="/local/templates/tulpar_store/assets/img/pay-type-1.png" alt="наличными деньгами"><span>наличными <br>деньгами</span></li>
                        <li><img src="/local/templates/tulpar_store/assets/img/pay-type-2.png" style="margin-left: 44px;" alt="пластиковой картой"><span>пластиковой <br>картой</span></li>
                        <li><img src="/local/templates/tulpar_store/assets/img/pay-type-3.png" style="margin-right: 40px;" alt="безналичная форма расчётов с юридическими лицами"><span>безналичная <br>форма расчётов с юридическими лицами</span></li>
                        <li><img src="/local/templates/tulpar_store/assets/img/pay-type-4.png" style="margin-left: 16px;" alt="оплата в Вашем банке на расчётный счёт"><span>оплата в Вашем банке на расчётный счёт</span></li>
                        <li><img src="/local/templates/tulpar_store/assets/img/pay-type-5.png" alt="Сбербанк-онлайн"><span>возможны даже переводы через "Сбербанк-онлайн"</span></li>
                    </ul>
                </p>

                <p>При оплате товара в нашей компании, Вы получаете на руки «Договор поставки» и подтверждающие документы об оплате.</p>

                <p>После доставки товара Вы получаете товарную накладную. Это особенно важно, так как у Вас есть полное право вернуть у государства 13% от затраченных средств на строительство своего жилья. Максимальная сумма возврата составляет 260 000 рублей.</p>

                <p>Мы уверенны в качестве реализуемой продукции и дорожим своим членством в «Ассоциации добросовестных поставщиков», поэтому отгружаем товар только после 100% оплаты.</p>

                <p>За 6 лет существовании нашей компании, мы убедились, что очень редкие водители серьезно относятся к бухгалтерским документам. Поэтому документы, подтверждающие оплату и отгрузку, передаем на руки только в нашем офисе продаж.</p>

            </div>
        </div>
    </div>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>