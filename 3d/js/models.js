var models = [
    {   format: "1nf",
        maker: "alexeevsk",
        surface: "rustik",
        tone: "bavaria",
        color: "bavaria",
        brickImg: "/3d/images/icons/bricks/alexeevsk/1nf/rustik_bavaria.jpg",
        url: "/3d/images/models/1nf/alexeevsk/bavaria.jpg"
    },
    {
        format: "1nf",
        maker: "alexeevsk",
        surface: "rustik",
        tone: "yellow",
        color: "slonovaya_kost",
        brickImg: "/3d/images/icons/bricks/alexeevsk/1nf/rustik_slonovaya_kost.jpg",
        url: "/3d/images/models/1nf/alexeevsk/soloma_rustik.jpg"
    },
    {
        format: "1nf",
        maker: "alexeevsk",
        surface: "smooth",
        tone: "yellow",
        color: "slonovaya_kost",
        brickImg: "images/icons/bricks/alexeevsk/1nf/smooth/soloma.jpg",
        url: "/3d/images/models/1nf/alexeevsk/soloma_gladkiy.jpg"
    },
    {   format: "14nf",
        maker: "alexeevsk",
        surface: "smooth",
        tone: "yellow",
        color: "slonovaya_kost",
        brickImg: "/3d/images/icons/bricks/alexeevsk/14nf/slonovaya_kost_gladkiy.jpg",
        url: "/3d/images/models/14nf/alexeevsk/slonovaya_kost_gladkiy.jpg"
    },
    {
        format: "14nf",
        maker: "alexeevsk",
        surface: "rustik",
        tone: "yellow",
        color: "slonovaya_kost",
        brickImg: "/3d/images/icons/bricks/alexeevsk/14nf/slonovaya_kost_rustik.jpg",
        url: "/3d/images/models/14nf/alexeevsk/slonovaya_kost_rustik.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "barhat",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/altair/1nf/chocholate_barhat.jpg",
        url: "/3d/images/models/1nf/altair/barhat_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "barhat",
        tone: "red",
        color: "peach",
        brickImg: "/3d/images/icons/bricks/altair/1nf/peach_barhat.jpg",
        url: "/3d/images/models/1nf/altair/barhat_peach.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "barhat",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/altair/1nf/red_barhat.jpg",
        url: "/3d/images/models/1nf/altair/barhat_red.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "barhat",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/altair/1nf/soloma_barhat.jpg",
        url: "/3d/images/models/1nf/altair/barhat_soloma.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "brown",
        color: "cappuchino",
        brickImg: "/3d/images/icons/bricks/altair/1nf/capuccino_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_cappuchino.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/altair/1nf/chocholate_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "brown",
        color: "dark chocolate",
        brickImg: "/3d/images/icons/bricks/altair/1nf/dark_chocholate_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_dark_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "brown",
        color: "latte",
        brickImg: "/3d/images/icons/bricks/altair/1nf/latte_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_latte.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "brown",
        color: "mokko",
        brickImg: "/3d/images/icons/bricks/altair/1nf/mokko_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_mokko.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "red",
        color: "carrot",
        brickImg: "/3d/images/icons/bricks/altair/1nf/carrot_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_morkov.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "red",
        color: "peach",
        brickImg: "/3d/images/icons/bricks/altair/1nf/peach_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_persik.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/altair/1nf/red_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_red.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/altair/1nf/soloma_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_soloma.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "red",
        color: "terracot",
        brickImg: "/3d/images/icons/bricks/altair/1nf/terracot_smooth.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_terrakot.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "loft",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/altair/1nf/chocolate_loft.jpg",
        url: "/3d/images/models/1nf/altair/loft_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "loft",
        tone: "red",
        color: "peach",
        brickImg: "/3d/images/icons/bricks/altair/1nf/peach_loft.jpg",
        url: "/3d/images/models/1nf/altair/loft_peach.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "loft",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/altair/1nf/red_loft.jpg",
        url: "/3d/images/models/1nf/altair/loft_red.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "loft",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/altair/1nf/soloma_loft.jpg",
        url: "/3d/images/models/1nf/altair/loft_soloma.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "loft",
        tone: "red",
        color: "terracot",
        brickImg: "/3d/images/icons/bricks/altair/1nf/terracot_loft.jpg",
        url: "/3d/images/models/1nf/altair/loft_terrakot.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "terra",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/altair/1nf/chocolate_terra.jpg",
        url: "/3d/images/models/1nf/altair/terra_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "terra",
        tone: "red",
        color: "peach",
        brickImg: "/3d/images/icons/bricks/altair/1nf/peach_terra.jpg",
        url: "/3d/images/models/1nf/altair/terra_peach.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "terra",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/altair/1nf/terra_red.jpg",
        url: "/3d/images/models/1nf/altair/terra_red.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "bavaria",
        color: "kamelot",
        brickImg: "/3d/images/icons/bricks/altair/1nf/gladkiy_kamelot_mix.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_kamelot_mix.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "bavaria",
        color: "bavaria red",
        brickImg: "/3d/images/icons/bricks/altair/1nf/gladkiy_bavaria_red.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_bavaria_red.jpg"
    },
    {
        format: "1nf",
        maker: "altair",
        surface: "smooth",
        tone: "bavaria",
        color: "bavaria",
        brickImg: "/3d/images/icons/bricks/altair/1nf/gladkiy_bavaria.jpg",
        url: "/3d/images/models/1nf/altair/gladkiy_bavaria.jpg"
    },
    {
        format: "14nf",
        maker: "altair",
        surface: "smooth",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/altair/14nf/chocolate_smooth.jpg",
        url: "/3d/images/models/14nf/altair/gladkiy_chokolad.jpg"
    },
    {
        format: "14nf",
        maker: "altair",
        surface: "smooth",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/altair/14nf/red_smooth.jpg",
        url: "/3d/images/models/14nf/altair/gladkiy_red.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "croc_skin",
        tone: "red",
        color: "abrikos",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/croc_abrikos.jpg",
        url: "/3d/images/models/1nf/aspk/croc_abrikos.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "croc_skin",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/croc_chocolate.jpg",
        url: "/3d/images/models/1nf/aspk/croc_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "croc_skin",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/croc_chocolate.jpg",
        url: "/3d/images/models/1nf/aspk/croc_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "croc_skin",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/croc_red.jpg",
        url: "/3d/images/models/1nf/aspk/croc_red.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "croc_skin",
        tone: "red",
        color: "red_light",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/croc_redlight.jpg",
        url: "/3d/images/models/1nf/aspk/croc_red_light.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "dkamen",
        tone: "red",
        color: "abrikos",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/dkamen_abrikos.jpg",
        url: "/3d/images/models/1nf/aspk/dkamen_abrikos.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "dkamen",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/dkamen_red.jpg",
        url: "/3d/images/models/1nf/aspk/dkamen_red.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "dkamen",
        tone: "red",
        color: "red_light",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/dkamen_red_light.jpg",
        url: "/3d/images/models/1nf/aspk/dkamen_red_light.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "red",
        color: "abrikos",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/smooth_abrikos.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_abrikos.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "bavaria",
        color: "bavaria mix",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/gladkiy_bavaria.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_bavaria.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "bavaria",
        color: "bavaria red",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/gladkiy_bavaria_red.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_bavaria_red.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/gladkiy_chokolad.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "brown",
        color: "gliasse",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/gladkiy_gliasse.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_gliasse.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/gladkiy_red.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_red.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "red",
        color: "dark red",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/gladkiy_red_dark.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_red_dark.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "red",
        color: "red_light",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/gladkiy_red_light.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_red_light.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "smooth",
        tone: "brown",
        color: "terracot",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/gladkiy_terrakot.jpg",
        url: "/3d/images/models/1nf/aspk/gladkiy_terrakot.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "kora_dereva",
        tone: "red",
        color: "abrikos",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/kora_dereva_abrikos.jpg",
        url: "/3d/images/models/1nf/aspk/kora_dereva_abrikos.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "kora_dereva",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/kora_dereva_chokolad.jpg",
        url: "/3d/images/models/1nf/aspk/kora_dereva_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "kora_dereva",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/kora_dereva_red.jpg",
        url: "/3d/images/models/1nf/aspk/kora_dereva_red.jpg"
    },
    {
        format: "1nf",
        maker: "aspk",
        surface: "kora_dereva",
        tone: "red",
        color: "red_light",
        brickImg: "/3d/images/icons/bricks/aspk/1nf/kora_dereva_red_light.jpg",
        url: "/3d/images/models/1nf/aspk/kora_dereva_red_light.jpg"
    },
    {
        format: "14nf",
        maker: "aspk",
        surface: "croc_skin",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/aspk/14nf/croc_red.jpg",
        url: "/3d/images/models/14nf/aspk/croc_red.jpg"
    },
    {
        format: "14nf",
        maker: "aspk",
        surface: "smooth",
        tone: "red",
        color: "abrikos",
        brickImg: "/3d/images/icons/bricks/aspk/14nf/gladkiy_abrikos.jpg",
        url: "/3d/images/models/14nf/aspk/gladkiy_abrikos.jpg"
    },
    {
        format: "14nf",
        maker: "aspk",
        surface: "smooth",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/aspk/14nf/gladkiy_chokolad.jpg",
        url: "/3d/images/models/14nf/aspk/gladkiy_chokolad.jpg"
    },
    {
        format: "14nf",
        maker: "aspk",
        surface: "smooth",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/aspk/14nf/gladkiy_red.jpg",
        url: "/3d/images/models/14nf/aspk/gladkiy_red.jpg"
    },
    {
        format: "14nf",
        maker: "aspk",
        surface: "smooth",
        tone: "red",
        color: "red_light",
        brickImg: "/3d/images/icons/bricks/aspk/14nf/gladkiy_red_light.jpg",
        url: "/3d/images/models/14nf/aspk/gladkiy_red_light.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "bavaria",
        color: "arenberg",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_arenberg.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_arenberg.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "bavaria",
        color: "bavaria",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_bavaria.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_bavaria.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_chokolad.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "brown",
        color: "dark chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_dark_chocolad.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_dark_chocolad.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "bavaria",
        color: "elts",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_elts.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_elts.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "red",
        color: "gliasse",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_gliasse.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_gliasse.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "gray",
        color: "kamelot chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_kamelot_chocolad.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_kamelot_chocolad.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "gray",
        color: "kamelot dark chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_kamelot_dark_chocolad.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_kamelot_dark_chocolad.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "gray",
        color: "kamelot gliasse",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_kamelot_gliasse.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_kamelot_gliasse.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "bavaria",
        color: "kamelot mix",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_kamelot_mix.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_kamelot_mix.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "gray",
        color: "kamelot terracot",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_kamelot_terrakot.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_kamelot_terrakot.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "yellow",
        color: "lotos",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_lotos.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_lotos.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_red.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_red.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "bavaria",
        color: "rochester",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_rochester.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_rochester.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "brown",
        color: "terracot",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/gladkiy_terracot.jpg",
        url: "/3d/images/models/1nf/kc_keramik/gladkiy_terracot.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "handmade",
        tone: "bavaria",
        color: "arenberg",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/handmade_arenberg.jpg",
        url: "/3d/images/models/1nf/kc_keramik/handmade_arenberg.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "handmade",
        tone: "bavaria",
        color: "bavaria",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/handmade_bavaria.jpg",
        url: "/3d/images/models/1nf/kc_keramik/handmade_bavaria.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "handmade",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/handmade_red.jpg",
        url: "/3d/images/models/1nf/kc_keramik/handmade_red.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "bavaria",
        color: "arenberg",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/kora_dereva_arenberg.jpg",
        url: "/3d/images/models/1nf/kc_keramik/kora_dereva_arenberg.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "bavaria",
        color: "bavaria",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/kora_dereva_bavaria.jpg",
        url: "/3d/images/models/1nf/kc_keramik/kora_dereva_bavaria.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/kora_dereva_chocolad.jpg",
        url: "/3d/images/models/1nf/kc_keramik/kora_dereva_chocolad.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "brown",
        color: "dark chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/kora_dereva_dark_chocolad.jpg",
        url: "/3d/images/models/1nf/kc_keramik/kora_dereva_dark_chocolad.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "red",
        color: "gliasse",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/kora_dereva_gliasse.jpg",
        url: "/3d/images/models/1nf/kc_keramik/kora_dereva_gliasse.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "yellow",
        color: "lotos",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/kora_dereva_lotos.jpg",
        url: "/3d/images/models/1nf/kc_keramik/kora_dereva_lotos.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "brown",
        color: "terracot",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/kora_dereva_terracot.jpg",
        url: "/3d/images/models/1nf/kc_keramik/kora_dereva_terracot.jpg"
    },
    {
        format: "1nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/kc_keramic/1nf/kora_red.jpg",
        url: "/3d/images/models/1nf/kc_keramik/kora_red.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "bavaria",
        color: "arenberg",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/gladkiy_arenberg.jpg",
        url: "/3d/images/models/14nf/kc_keramic/gladkiy_arenberg.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "bavaria",
        color: "bavaria mix",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/gladkiy_bavaria_mix.jpg",
        url: "/3d/images/models/14nf/kc_keramic/gladkiy_bavaria_mix.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/gladkiy_chocolad.jpg",
        url: "/3d/images/models/14nf/kc_keramic/gladkiy_chocolad.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "brown",
        color: "dark chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/gladkiy_dark_chocolad.jpg",
        url: "/3d/images/models/14nf/kc_keramic/gladkiy_dark_chocolad.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "red",
        color: "gliasse",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/gladkiy_gliasse.jpg",
        url: "/3d/images/models/14nf/kc_keramic/gladkiy_gliasse.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "yellow",
        color: "lotos",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/gladkiy_lotos.jpg",
        url: "/3d/images/models/14nf/kc_keramic/gladkiy_lotos.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/gladkiy_red.jpg",
        url: "/3d/images/models/14nf/kc_keramic/gladkiy_red.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "smooth",
        tone: "brown",
        color: "terracot",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/gladkiy_terrakot.jpg",
        url: "/3d/images/models/14nf/kc_keramic/gladkiy_terrakot.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "bavaria",
        color: "arenberg",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/kora_dereva_arenberg.jpg",
        url: "/3d/images/models/14nf/kc_keramic/kora_dereva_arenberg.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "brown",
        color: "dark chocolate",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/kora_dereva_dark_chocolad.jpg",
        url: "/3d/images/models/14nf/kc_keramic/kora_dereva_dark_chocolad.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "red",
        color: "gliasse",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/kora_dereva_gliasse.jpg",
        url: "/3d/images/models/14nf/kc_keramic/kora_dereva_gliasse.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "yellow",
        color: "lotos",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/kora_dereva_lotos.jpg",
        url: "/3d/images/models/14nf/kc_keramic/kora_dereva_lotos.jpg"
    },
    {
        format: "14nf",
        maker: "kc-keramik",
        surface: "kora_dereva",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/kc_keramic/14nf/kora_dereva_red.jpg",
        url: "/3d/images/models/14nf/kc_keramic/kora_dereva_red.jpg"
    },
    {
        format: "1nf",
        maker: "kluker",
        surface: "smooth",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/kluker/1nf/gladkiy_soloma.jpg",
        url: "/3d/images/models/1nf/klyuker/gladkiy_soloma.jpg"
    },
    {
        format: "14nf",
        maker: "kluker",
        surface: "smooth",
        tone: "red",
        color: "peach",
        brickImg: "/3d/images/icons/bricks/kluker/14nf/gladkiy_persik.jpg",
        url: "/3d/images/models/14nf/klyuker/gladkiy_persik.jpg"
    },
    {
        format: "14nf",
        maker: "kluker",
        surface: "smooth",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/kluker/14nf/gladkiy_soloma.jpg",
        url: "/3d/images/models/14nf/klyuker/gladkiy_soloma.jpg"
    },
    {
        format: "14nf",
        maker: "kluker",
        surface: "smooth",
        tone: "yellow",
        color: "soloma_c21",
        brickImg: "/3d/images/icons/bricks/kluker/14nf/gladkiy_soloma_c21.jpg",
        url: "/3d/images/models/14nf/klyuker/gladkiy_soloma_c21.jpg"
    },
    {
        format: "14nf",
        maker: "kluker",
        surface: "smooth",
        tone: "yellow",
        color: "soloma_c23",
        brickImg: "/3d/images/icons/bricks/kluker/14nf/gladkiy_soloma_c23.jpg",
        url: "/3d/images/models/14nf/klyuker/gladkiy_soloma_c23.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "barhat",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/barhat_chokolad.jpg",
        url: "/3d/images/models/1nf/koshakovsky/barhat_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "barhat",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/barhat_red.jpg",
        url: "/3d/images/models/1nf/koshakovsky/barhat_red.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "barhat",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/barhat_soloma.jpg",
        url: "/3d/images/models/1nf/koshakovsky/barhat_soloma.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "dkamen",
        tone: "red",
        color: "bordo",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/dkamen_bordo.jpg",
        url: "/3d/images/models/1nf/koshakovsky/dkamen_bordo.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "dkamen",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/dkamen_chokolad.jpg",
        url: "/3d/images/models/1nf/koshakovsky/dkamen_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "dkamen",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/dkamen_soloma.jpg",
        url: "/3d/images/models/1nf/koshakovsky/dkamen_soloma.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "galich",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/galich_chokolad.jpg",
        url: "/3d/images/models/1nf/koshakovsky/galich_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "galich",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/galich_soloma.jpg",
        url: "/3d/images/models/1nf/koshakovsky/galich_soloma.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "smooth",
        tone: "red",
        color: "bordo",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/gladkiy_bordo.jpg",
        url: "/3d/images/models/1nf/koshakovsky/gladkiy_bordo.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "smooth",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/gladkiy_chokolad.jpg",
        url: "/3d/images/models/1nf/koshakovsky/gladkiy_chokolad.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "smooth",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/gladkiy_red.jpg",
        url: "/3d/images/models/1nf/koshakovsky/gladkiy_red.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "smooth",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/gladkiy_soloma.jpg",
        url: "/3d/images/models/1nf/koshakovsky/gladkiy_soloma.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "dkamen",
        tone: "yellow",
        color: "milkyway",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/milkyway.jpg",
        url: "/3d/images/models/1nf/koshakovsky/milkyway.jpg"
    },
    {
        format: "1nf",
        maker: "koshak",
        surface: "rustik",
        tone: "yellow",
        color: "sahara",
        brickImg: "/3d/images/icons/bricks/koshak/1nf/sahara.jpg",
        url: "/3d/images/models/1nf/koshakovsky/sahara.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "barhat",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/barhat_chokolad.jpg",
        url: "/3d/images/models/14nf/koshakovsky/barhat_chokolad.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "barhat",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/barhat_red.jpg",
        url: "/3d/images/models/14nf/koshakovsky/barhat_red.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "barhat",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/barhat_soloma.jpg",
        url: "/3d/images/models/14nf/koshakovsky/barhat_soloma.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "dkamen",
        tone: "red",
        color: "bordo",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/dkamen_bordo.jpg",
        url: "/3d/images/models/14nf/koshakovsky/dkamen_bordo.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "dkamen",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/dkamen_chokolad.jpg",
        url: "/3d/images/models/14nf/koshakovsky/dkamen_chokolad.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "dkamen",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/dkamen_soloma.jpg",
        url: "/3d/images/models/14nf/koshakovsky/dkamen_soloma.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "galich",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/galich_chokolad.jpg",
        url: "/3d/images/models/14nf/koshakovsky/galich_chokolad.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "galich",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/galich_soloma.jpg",
        url: "/3d/images/models/14nf/koshakovsky/galich_soloma.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "smooth",
        tone: "red",
        color: "bordo",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/gladkiy_bordo.jpg",
        url: "/3d/images/models/14nf/koshakovsky/gladkiy_bordo.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "smooth",
        tone: "brown",
        color: "chocolate",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/gladkiy_chokolad.jpg",
        url: "/3d/images/models/14nf/koshakovsky/gladkiy_chokolad.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "smooth",
        tone: "red",
        color: "red",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/gladkiy_red.jpg",
        url: "/3d/images/models/14nf/koshakovsky/gladkiy_red.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "smooth",
        tone: "yellow",
        color: "soloma",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/gladkiy_soloma.jpg",
        url: "/3d/images/models/14nf/koshakovsky/gladkiy_soloma.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "dkamen",
        tone: "yellow",
        color: "milkyway",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/milkyway.jpg",
        url: "/3d/images/models/14nf/koshakovsky/milkyway.jpg"
    },
    {
        format: "14nf",
        maker: "koshak",
        surface: "rustik",
        tone: "yellow",
        color: "sahara",
        brickImg: "/3d/images/icons/bricks/koshak/14nf/sahara.jpg",
        url: "/3d/images/models/14nf/koshakovsky/sahara.jpg"
    },
];