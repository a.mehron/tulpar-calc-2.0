var temp = [];
for (model of models) {
    if (!temp.includes(model.color)) {
        temp.push(model.color);
    }
}

console.log(temp);

var chosenOptions = {
    maker: "",
    makerName: "",
    format: "",
    formatName: "",
    surface: "",
    surfaceName: "",
    tone: "",
    toneName: "",
    color: "",
    colorName: ""
}

var currentStateArr = [];

$('#config-btn').click(function() {
  $('.selections').addClass('open');
  $(this).fadeOut();
  $('.toggle-interface').fadeIn();
  $('#container-3d').addClass("clicked");
});

 $('.toggle-interface').click(function(){
    $('.selections').toggleClass('open');
    $('.selected-3d').toggleClass('hidden');

    $('.show-interface').toggleClass('visible');
    $('.hide-interface').toggleClass('visible');

    $('.house-summary').fadeToggle();
 });

 $('.go-back').click(function() {

    let thisParentElementId = this.parentElement.id;

    switch (thisParentElementId) {
        case "brick-formats":
            $('.selected-3d').fadeOut(300);
            $('#brick-formats').fadeOut();
            $('.selected-option[data-option="maker"]').remove();

            showMakerStep();
            break;
        case "brick-surfaces":
            $('.selected-option[data-option="format"]').fadeOut(300).remove();

            $('#brick-surfaces').fadeOut();
            $('#brick-surfaces .border_blue').remove();

            $('#brick-formats').fadeIn();

            chosenOptions.format = "";
            chosenOptions.formatName = "";

            currentStateArr = models.filter(o => o.maker === chosenOptions.maker);
            break;
        case "brick-tones":
            $('.selected-option[data-option="surface"]').fadeOut(300).remove();

            $('#brick-tones').fadeOut();
            $('#brick-tones .border_blue').remove();

            $('#brick-surfaces').fadeIn();

            chosenOptions.surface = "";
            chosenOptions.surfaceName = "";

            currentStateArr = models.filter(o => (o.maker === chosenOptions.maker
                                                  && o.format === chosenOptions.format));

            break;
        case "brick-colors":
            $('.selected-option[data-option="tone"]').fadeOut(300).remove();

            $('#brick-colors').fadeOut();
            $('#brick-colors .border_blue').remove();

            $('#brick-tones').fadeIn();

            chosenOptions.tone = "";
            chosenOptions.toneName = "";

            currentStateArr = models.filter(o => (o.maker === chosenOptions.maker
                                                  && o.format === chosenOptions.format
                                                  && o.surface === chosenOptions.surface));

            break;
        default:

    }

 });

$('.brick-maker').click(function() {
    $('#brick-maker').fadeOut(300);
    $('#brick-formats').fadeIn(300);
    let maker = $(this).attr('data-maker');
    let makerObj = makers.find(o => o.id === maker);

    chosenOptions.maker = makerObj.id;
    chosenOptions.makerName = makerObj.name;

    currentStateArr = models.filter(o => o.maker === chosenOptions.maker);

    let newSelectedOptionEl = document.createElement('div');
    newSelectedOptionEl.setAttribute('data-option', 'maker');
    newSelectedOptionEl.classList.add('selected-option');
    newSelectedOptionEl.innerHTML = `
                                    <div class="option-name">производитель</div>
                                    <div class="option-value">${makerObj.name}</div>
                                    `;

    $('.selected-3d').append(newSelectedOptionEl);
    $('.selected-3d').fadeIn(300);
});

$('.brick-format').click(function() {
    $('#brick-formats').fadeOut(300);
    $('#brick-surfaces').fadeIn(300);
    let chosenFormat = $(this).attr('data-format');
    let chosenFormatName = $(this).attr('data-format-name');

    chosenOptions.format = chosenFormat;
    chosenOptions.formatName = chosenFormatName;

    currentStateArr = currentStateArr.filter(o => (o.format === chosenFormat));

    generateSurfaces();

    let newSelectedOptionEl = document.createElement('div');
    newSelectedOptionEl.setAttribute('data-option', 'format');
    newSelectedOptionEl.classList.add('selected-option');
    newSelectedOptionEl.innerHTML = `
                                    <div class="option-name">формат</div>
                                    <div class="option-value">${chosenFormatName}</div>
                                    `;

    $('.selected-3d').append(newSelectedOptionEl);
});

$('#brick-surfaces').on('click', '.brick-surface', function(){
    $('#brick-surfaces').fadeOut(300);
    $('#brick-tones').fadeIn(300);
    let chosenSurface = $(this).attr('data-surface');
    let chosenSurfaceName = $(this).attr('data-surface-name');

    chosenOptions.surface = chosenSurface;
    chosenOptions.surfaceName = chosenSurfaceName;

    currentStateArr = currentStateArr.filter(o => (o.surface === chosenSurface));

    generateTones();

    let newSelectedOptionEl = document.createElement('div');
    newSelectedOptionEl.setAttribute('data-option', 'surface');
    newSelectedOptionEl.classList.add('selected-option');
    newSelectedOptionEl.innerHTML = `
                                    <div class="option-name">поверхность</div>
                                    <div class="option-value">${chosenSurfaceName}</div>
                                    `;

    $('.selected-3d').append(newSelectedOptionEl);

    //console.log(availableTones);
});

$('#brick-tones').on('click', '.brick-tone', function(){
    $('#brick-tones').fadeOut(300);
    $('#brick-colors').fadeIn(300);

    let chosenTone = $(this).attr('data-tone');
    let chosenToneName = $(this).attr('data-tone-name');

    chosenOptions.tone = chosenTone;
    chosenOptions.toneName = chosenToneName;

    currentStateArr = currentStateArr.filter(o => (o.tone === chosenTone));

    generateColors();

    let newSelectedOptionEl = document.createElement('div');
    newSelectedOptionEl.setAttribute('data-option', 'tone');
    newSelectedOptionEl.classList.add('selected-option');
    newSelectedOptionEl.innerHTML = `
                                    <div class="option-name">оттенок</div>
                                    <div class="option-value">${chosenToneName}</div>
                                    `;

    $('.selected-3d').append(newSelectedOptionEl);

    //console.log(currentStateArr);
});

$('#brick-colors').on('click', '.brick-color', function(){
    $('.selected-3d').fadeOut(250);

    $('#brick-colors').fadeOut(250);
    $('#final-choices').fadeIn(250);

    let chosenColor = $(this).attr('data-color');
    let chosenColorName = $(this).attr('data-color-name');

    chosenOptions.color = chosenColor;
    chosenOptions.colorName = chosenColorName;

    currentStateArr = currentStateArr.filter(o => (o.color === chosenColor));

    let makerEl = document.createElement('div');
    makerEl.classList.add('choice-block');
    makerEl.innerHTML = `
                        <span class="label">Производитель</span>
                        <div class="border_blue">
                          <div class="btn-stn" data-role="maker" data-maker="${chosenOptions.maker}">
                              <span>${chosenOptions.makerName}</span>
                          </div>
                        </div>
                        `;

    let formatEl = document.createElement('div');
    formatEl.classList.add('choice-block');
    formatEl.innerHTML = `
                        <span class="label">Формат</span>
                        <div class="border_blue">
                          <div class="btn-stn" data-role="format" data-format="${chosenOptions.format}">
                              <span>${chosenOptions.formatName}</span>
                          </div>
                        </div>
                        `;

    let surfaceEl = document.createElement('div');
    surfaceEl.classList.add('choice-block');
    surfaceEl.innerHTML = `
                        <span class="label">Поверхность</span>
                        <div class="border_blue">
                          <div class="btn-stn" data-role="surface" data-surface="${chosenOptions.surface}">
                              <span>${chosenOptions.surfaceName}</span>
                          </div>
                        </div>
                        `;

    let toneEl = document.createElement('div');
    toneEl.classList.add('choice-block');
    toneEl.innerHTML = `
                        <span class="label">Оттенок</span>
                        <div class="border_blue">
                          <div class="btn-stn" data-role="tone" data-tone="${chosenOptions.tone}">
                              <span>${chosenOptions.toneName}</span>
                          </div>
                        </div>
                        `;

    let colorEl = document.createElement('div');
    colorEl.classList.add('choice-block');
    colorEl.innerHTML = `
                        <span class="label">Цвет</span>
                        <div class="border_blue">
                          <div class="btn-stn" data-role="color" data-color="${chosenOptions.color}">
                              <span>${chosenOptions.colorName}</span>
                          </div>
                        </div>
                        `;

    let applyBtnEl = document.createElement('button');
    applyBtnEl.classList.add('applyBtn');
    applyBtnEl.textContent = "Применить";

    $('#final-choices').append(makerEl);
    $('#final-choices').append(formatEl);
    $('#final-choices').append(surfaceEl);
    $('#final-choices').append(toneEl);
    $('#final-choices').append(colorEl);
    $('#final-choices').append(applyBtnEl);

});

$('#final-choices').on('click', '.btn-stn', function(){
    let clickedParam = $(this).attr('data-role');

    $('#final-choices .choice-block').remove();

    switch (clickedParam) {
        case "maker":
            $('#final-choices').fadeOut(300);
            showMakerStep();
            $('.selected-option').remove();
            $('.selection-option-block .border_blue').not('#brick-formats .border_blue').not('#brick-maker .border_blue').remove();
            $('#final-choices button').remove();
            break;
        case "format":
            $('#final-choices').fadeOut(300);

            $('#brick-formats').fadeIn(300);
            $('.selected-3d').fadeIn(300);

            chosenOptions.format = "";
            chosenOptions.formatName = "";

            currentStateArr = models.filter(o => o.maker === chosenOptions.maker);

            $('.selected-option').not('.selected-option[data-option="maker"]').remove();
            $('.selection-option-block .border_blue').not('#brick-formats .border_blue').not('#brick-maker .border_blue').remove();
            $('#final-choices button').remove();
            break;
        case "surface":
            $('#final-choices').fadeOut(300);

            $('#brick-surfaces').fadeIn(300);
            $('.selected-3d').fadeIn(300);

            chosenOptions.surface = "";
            chosenOptions.surfaceName = "";

            currentStateArr = models.filter(o => (o.maker === chosenOptions.maker
                                                  && o.format === chosenOptions.format));



            $('.selected-option').not('.selected-option[data-option="maker"]').not('.selected-option[data-option="format"]').remove();
            $('.selection-option-block .border_blue').not('#brick-formats .border_blue').not('#brick-maker .border_blue').remove();
            $('#final-choices button').remove();

            generateSurfaces();
            break;
        case "tone":
            $('#final-choices').fadeOut(300);

            $('#brick-tones').fadeIn(300);
            $('.selected-3d').fadeIn(300);

            chosenOptions.tone = "";
            chosenOptions.toneName = "";

            currentStateArr = models.filter(o => (o.maker === chosenOptions.maker
                                                  && o.format === chosenOptions.format
                                                  && o.surface === chosenOptions.surface));

            $('.selected-option').not('.selected-option[data-option="maker"]').not('.selected-option[data-option="format"]').not('.selected-option[data-option="surface"]').remove();
            $('.selection-option-block .border_blue').not('#brick-formats .border_blue').not('#brick-maker .border_blue').not('#brick-surfaces .border_blue').remove();
            $('#final-choices button').remove();

            generateTones();
            break;
        case "color":
            $('#final-choices').fadeOut(300);

            $('#brick-colors').fadeIn(300);
            $('.selected-3d').fadeIn(300);

            chosenOptions.color = "";
            chosenOptions.colorName = "";

            currentStateArr = models.filter(o => (o.maker === chosenOptions.maker
                                                  && o.format === chosenOptions.format
                                                  && o.surface === chosenOptions.surface
                                                  && o.tone === chosenOptions.tone));

            $('.selected-option').not('.selected-option[data-option="maker"]').not('.selected-option[data-option="format"]').not('.selected-option[data-option="surface"]').not('.selected-option[data-option="tone"]').remove();

            $('.selection-option-block .border_blue').not('#brick-formats .border_blue').not('#brick-maker .border_blue').not('#brick-surfaces .border_blue').not('#brick-tones .border_blue').remove();

            $('#final-choices button').remove();

            generateColors();
            break;

        default:

    }
});

$('#final-choices').on('click', '.applyBtn', function(){
    let chosenModel = currentStateArr[0];

    $('#current-house').attr('src', chosenModel.url);

    $('.loader').addClass('show');

    $('.selections').fadeOut(300);

    let houseSummaryEl = document.createElement('div');
    houseSummaryEl.classList.add('house-summary');

    let editIcon = document.createElement('img');
    editIcon.classList.add('edit');
    editIcon.setAttribute('src', '/3d/images/icons/edit.svg');

    let makerEl = document.createElement('div');
    makerEl.innerHTML = `
                        <span class="label">Производитель</span>
                        <span><b>${chosenOptions.makerName}</b></span>
                        `;

    let formatEl = document.createElement('div');
    formatEl.innerHTML = `
                        <span class="label">Формат</span>
                        <span><b>${chosenOptions.formatName}</b></span>
                        `;

    let surfaceEl = document.createElement('div');
    surfaceEl.innerHTML = `
                        <span class="label">Поверхность</span>
                        <span><b>${chosenOptions.surfaceName}</b></span>
                        `;

    let toneEl = document.createElement('div');
    toneEl.innerHTML = `
                        <span class="label">Оттенок</span>
                        <span><b>${chosenOptions.toneName}</b></span>
                        `;

    let colorEl = document.createElement('div');
    colorEl.innerHTML = `
                        <span class="label">Цвет</span>
                        <span><b>${chosenOptions.colorName}</b></span>
                        `;

    houseSummaryEl.appendChild(makerEl);
    houseSummaryEl.appendChild(formatEl);
    houseSummaryEl.appendChild(surfaceEl);
    houseSummaryEl.appendChild(toneEl);
    houseSummaryEl.appendChild(colorEl);
    houseSummaryEl.appendChild(editIcon);

    $('#container-3d').append(houseSummaryEl);

    $('#current-house').on('load', function() {
       $('.loader').removeClass('show');
       $('.house-summary').fadeIn(300);
    });


    $('.download-image').fadeIn();
});

$('#container-3d').on('click', '.house-summary', function(){
    $('.selections').fadeIn(300);
    $('.house-summary').remove();
});

function showMakerStep() {
    $('#brick-maker').fadeIn();

    chosenOptions.maker = "";
    chosenOptions.makerName = "";

    currentStateArr = models;
}

function generateSurfaces(){
    let availableSurfaces = [];
    for (obj of currentStateArr) {
        let surfaceObj = surfaces.find(o => o.id === obj.surface);
        if (!availableSurfaces.includes(surfaceObj)) {
            availableSurfaces.push(surfaceObj);
        }
    }
    for (surface of availableSurfaces) {
        if (typeof surface !== 'undefined') {
            let newOptionEl = document.createElement('div');
            newOptionEl.classList.add('border_blue');
            newOptionEl.innerHTML = `
                                    <div class="btn-stn brick-surface" data-surface="${surface.id}" data-surface-name="${surface.name}">
                                      <span>${surface.name}</span>
                                    </div>
                                    `;
            $('#brick-surfaces').append(newOptionEl);
        }
    }
}

function generateTones() {
    let availableTones = [];
    for (obj of currentStateArr) {
        let toneObj = tones.find(o => o.id === obj.tone);
        if (!availableTones.includes(toneObj)) {
            availableTones.push(toneObj);
        }
    }

    for (tone of availableTones) {
        let newOptionEl = document.createElement('div');
        newOptionEl.classList.add('border_blue');
        newOptionEl.innerHTML = `
                                <div class="btn-stn brick-tone" data-tone="${tone.id}" data-tone-name="${tone.name}">
                                  <span class="color ${tone.id}"></span>
                                  <span>${tone.name}</span>
                                </div>
                                `;
        $('#brick-tones').append(newOptionEl);
    }
}

function generateColors() {
    for (obj of currentStateArr) {
        let colorObj = colors.find(o => o.id === obj.color);

        let newOptionEl = document.createElement('div');
        newOptionEl.classList.add('border_blue');
        newOptionEl.setAttribute('data-toggle', 'tooltip');
        newOptionEl.setAttribute('data-html', 'true');
        newOptionEl.setAttribute('data-placement', 'left');
        newOptionEl.setAttribute('title', '<img class="tooltip-thumb" src="' + obj.brickImg + '" alt="">');
        newOptionEl.innerHTML = `
                                  <div class="btn-stn brick-color" data-color="${colorObj.id}" data-color-name="${colorObj.name}">
                                    <img class="thumb" src="${obj.brickImg}" alt="">
                                    <span>${colorObj.name}</span>
                                  </div>
                                `;

        $('#brick-colors').append(newOptionEl);
    }

    function isTouchDevice(){
        return true == ("ontouchstart" in window || window.DocumentTouch && document instanceof DocumentTouch);
    }

    if(isTouchDevice()===false) {
        $('[data-toggle="tooltip"]').tooltip()
    }

}

$('.download-image').click(function() {
    let link = document.createElement('a');
    let href =$('#current-house').attr("src");

    let offerName = chosenOptions.maker + "_" + chosenOptions.format + "_" + chosenOptions.surface + "_" + chosenOptions.color;

    link.href = href;  // use realtive url
    link.download = offerName + '.jpg';
    document.body.appendChild(link);
    link.click();
    link.remove();
});
