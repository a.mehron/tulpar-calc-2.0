var makers = [
    {
        id: "alexeevsk",
        name: "Алексеевская керамика"
    },
    {
        id: "altair",
        name: "Альтаир (Ижевск)"
    },
    {
        id: "aspk",
        name: "АСПК (Арск)",
    },
    {
        id: "kc-keramik",
        name: "КС-Керамик (Кирово-Чепецк)",
    },
    {
        id: "kluker",
        name: "Клюкер (Ключищи)",
    },
    {
        id: "koshak",
        name: "Ак Барс Керамик (Кощаково)",
    }
];


var surfaces = [
    {
        id: "rustik",
        name: "рустик"
    },
    {
        id: "smooth",
        name: "гладкая"
    },
    {
        id: "barhat",
        name: "бархат"
    },
    {
        id: "loft",
        name: "лофт"
    },
    {
        id: "terra",
        name: "терра"
    },
    {
        id: "croc_skin",
        name: "крокодиловая кожа"
    },
    {
        id: "dkamen",
        name: "дикий камень"
    },
    {
        id: "kora_dereva",
        name: "кора дерева"
    },
    {
        id: "handmade",
        name: "ручная формовка"
    },
    {
        id: "galich",
        name: "галич"
    }
];

var tones = [
    {
        id: "bavaria",
        name: "баварская кладка"
    },
    {
        id: "yellow",
        name: "желтый"
    },
    {
        id: "brown",
        name: "коричневый"
    },
    {
        id: "red",
        name: "красный"
    },
    {
        id: "gray",
        name: "серый"
    },
    {
        id: "white",
        name: "белый"
    }
];

var colors = [
    {
        id: "bavaria",
        name: "бавария"
    },
    {
        id: "bavaria mix",
        name: "бавария микс"
    },
    {
        id: "bavaria red",
        name: "бавария красная"
    },
    {
        id: "soloma",
        name: "солома"
    },
    {
        id: "soloma_c21",
        name: "солома c21"
    },
    {
        id: "soloma_c23",
        name: "солома c23"
    },
    {
        id: "slonovaya_kost",
        name: "слоновая кость"
    },
    {
        id: "peach",
        name: "персик"
    },
    {
        id: "chocolate",
        name: "шоколад"
    },
    {
        id: "red",
        name: "красный"
    },
    {
        id: "dark red",
        name: "темно красный"
    },
    {
        id: "red_light",
        name: "светло-красный"
    },
    {
        id: "cappuchino",
        name: "капучино"
    },
    {
        id: "dark chocolate",
        name: "темный шоколад"
    },
    {
        id: "latte",
        name: "латте"
    },
    {
        id: "mokko",
        name: "мокко"
    },
    {
        id: "carrot",
        name: "морковь"
    },
    {
        id: "terracot",
        name: "терракот"
    },
    {
        id: "abrikos",
        name: "абрикос"
    },
    {
        id: "gliasse",
        name: "гляссе"
    },
    {
        id: "kamelot",
        name: "камелот микс"
    },
    {
        id: "arenberg",
        name: "аренберг"
    },
    {
        id: "elts",
        name: "эльц"
    },
    {
        id: "kamelot chocolate",
        name: "камелот шоколад"
    },
    {
        id: "kamelot dark chocolate",
        name: "камелот тёмный шоколад"
    },
    {
        id: "kamelot gliasse",
        name: "камелот гляссе"
    },
    {
        id: "kamelot mix",
        name: "камелот микс"
    },
    {
        id: "kamelot terracot",
        name: "камелот терракот"
    },
    {
        id: "lotos",
        name: "лотос"
    },
    {
        id: "rochester",
        name: "рочестер"
    },
    {
        id: "white",
        name: "белый"
    },
    {
        id: "bordo",
        name: "бордо"
    },
    {
        id: "milkyway",
        name: "млечный путь"
    },
    {
        id: "sahara",
        name: "сахара"
    }
];