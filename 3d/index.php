<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("3D визуализация");

use \Bitrix\Main\Page\Asset;

Asset::getInstance()->addCss('/3d/css/styles.css');

Asset::getInstance()->addJs('/3d/js/models.js');
Asset::getInstance()->addJs('/3d/js/attributes.js');
Asset::getInstance()->addJs('/3d/js/app.js');
?>

<?$APPLICATION->SetDirProperty("class", "three-d");?>

<div class="container pt-4">
    <h1>3D визуализация</h1>
  <div id="container-3d">

      <!-- 3  -->
    <div class="loader loader--style3">
      <div class="white_fade"></div>
      <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
      <path fill="#ff1654 " d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
        <animateTransform attributeType="xml"
          attributeName="transform"
          type="rotate"
          from="0 25 25"
          to="360 25 25"
          dur="0.6s"
          repeatCount="indefinite"/>
        </path>
      </svg>
    </div>

    <img src="/3d/images/1nf_gladkiy_chokolad.jpg" alt="" id="current-house" class="house">
    <div id="config-btn" class="config-btn"><span>Конфигуратор дома</span></div>

    <div class="download-image">
        Скачать <br> модель
    </div>

    <div class="toggle-interface">
        <div class="show-interface option-interface">Показать <br> интерфейс</div>
        <div class="hide-interface option-interface visible">Скрыть <br> интерфейс</div>
    </div>

    <div class="selected-3d">
        <div class="title">Выбранное:</div>
    </div>

    <div class="selections">
      <div id="brick-maker" class="selection-option-block">
        <div class="block-title">Выберите производителя</div>

        <div class="border_blue">
          <div class="btn-stn brick-maker" data-maker="alexeevsk">
              <img src="/3d/images/icons/makers/alekseevsk.jpg" alt="">
              <span>Алексеевская керамика</span>
          </div>
        </div>

        <div class="border_blue">
          <div class="btn-stn brick-maker" data-maker="altair">
              <img src="/3d/images/icons/makers/altair.jpg" alt="">
              <span>Альтаир (Ижевск)</span>
          </div>
        </div>

        <div class="border_blue">
          <div class="btn-stn brick-maker" data-maker="aspk">
              <img src="/3d/images/icons/makers/aspk.jpg" alt="">
              <span>АСПК (Арск)</span>
          </div>
        </div>

        <div class="border_blue">
          <div class="btn-stn brick-maker" data-maker="kc-keramik">
              <img src="/3d/images/icons/makers/kc_keramik.jpg" alt="">
              <span>КС-Керамик (Кирово-Чепецк)</span>
          </div>
        </div>

        <div class="border_blue">
          <div class="btn-stn brick-maker" data-maker="kluker">
              <img src="/3d/images/icons/makers/kluker.jpg" alt="">
              <span>Клюкер (Ключищи)</span>
          </div>
        </div>

        <div class="border_blue">
          <div class="btn-stn brick-maker" data-maker="koshak">
              <img src="/3d/images/icons/makers/koshak.jpg" alt="">
              <span>Ак Барс Керамик (Кощаково)</span>
          </div>
        </div>

    </div> <!-- brick maker -->

    <div id="brick-formats" class="selection-option-block" style="display: none;">
        <div class="go-back"><img src="/3d/images/icons/left-arrow.svg" alt="">Производитель</div>
        <div class="block-title">Выберите формат кирпича</div>

        <div class="border_blue">
          <div class="btn-stn brick-format brick-format__1nf" data-format="1nf" data-format-name="1НФ">
              <img src="/3d/images/icons/brick-1nf.svg" alt="">
              <span>1НФ</span>
          </div>
        </div>

        <div class="border_blue">
          <div class="btn-stn brick-format brick-format__14nf" data-format="14nf" data-format-name="1.4НФ">
            <img src="/3d/images/icons/brick-14nf.svg" alt="">
            <span>1.4НФ</span>
          </div>
        </div>
    </div> <!-- brick format -->


    <div id="brick-surfaces" class="selection-option-block" style="display: none;">
      <div class="go-back"><img src="/3d/images/icons/left-arrow.svg" alt="">Формат</div>
      <div class="block-title">Выберите поверхность кирпича</div>
    </div> <!-- brick format -->


    <div id="brick-tones" class="selection-option-block" style="display: none;">
        <div class="go-back"><img src="/3d/images/icons/left-arrow.svg" alt="">Поверхность</div>
        <div class="block-title">Выберите оттенок кирпича</div>
    </div> <!-- brick tones -->

    <div id="brick-colors" class="selection-option-block" style="display: none;">
        <div class="go-back"><img src="/3d/images/icons/left-arrow.svg" alt="">Оттенок</div>
        <div class="block-title">Выберите цвет кирпича</div>
    </div> <!-- brick tones -->

    <div id="final-choices" class="selection-option-block" style="display: none;">
        <div class="block-title">Выбранные параметры</div>
    </div> <!-- brick tones -->

    </div>

  </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
