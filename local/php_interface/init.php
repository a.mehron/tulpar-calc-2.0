<?php
AddEventHandler("sale", "OnOrderNewSendEmail", "bxModifySaleMails");
function bxModifySaleMails($orderID, &$eventName, &$arFields)
{
	$additional_information = '';
	$arOrder = CSaleOrder::GetByID($orderID);
	$order_props = CSaleOrderPropsValue::GetOrderProps($orderID);  
	
	$arFields["USER_DESCRIPTION"] = $arOrder['USER_DESCRIPTION'];

	while ($arProps = $order_props->Fetch()) {    
		if ($arProps["CODE"] == "PHONE") {
			$arFields["PHONE"]  = htmlspecialchars($arProps["VALUE"]);
		}
	}
}

function getSiteName() {
	if (!empty($_SESSION["SOTBIT_REGIONS"]["NAME"]))
		return $_SESSION["SOTBIT_REGIONS"]["NAME"];

	return CSite::GetByID("s1")->Fetch()['NAME'];
}

function getCityData() {
	$name = getSiteName();

	$email = 'tulpar-trade@mail.ru';
	$props = '<p>
 <b>Наименование: ООО «Тулпар» </b>
					</p>
					<b> </b>
					<p>
						<b>
						ИНН: 1659180090 </b>
					</p>
					<b> </b>
					<p>
						<b>
						КПП: 165901001 </b>
					</p>
					<b> </b>
					<p>
						<b>
						Юридический адрес: 420059, РТ, г. Казань, ул. Даурская, 12А, офис 20.4 </b>
					</p>
					<b> </b>
					<p>
						<b>
						ОГРН: 1171690025242 </b>
					</p>
					<b> </b>
					<p>
						<b>
						Р/С 40702810010000096194 </b>
					</p>
					<b> </b>
					<p>
						<b>
						Банк: АО «ТИНЬКОФФ БАНК» </b>
					</p>
					<b> </b>
					<p>
						<b>
						БИК: 044525974 </b>
					</p>
					<b> </b>
					<p>
						<b>
						К/С: 30101 810 1452 5000 0974</b>
					</p>';
/**/
	//echo '<!-- '; print_r($_SESSION["SOTBIT_REGIONS"]); echo ' -->';

	$name = $_SESSION["SOTBIT_REGIONS"]["NAME"];
	if (is_array($_SESSION["SOTBIT_REGIONS"]["UF_PHONE"]))
	    $phone = $_SESSION["SOTBIT_REGIONS"]["UF_PHONE"][0];
	else
        $phone = $_SESSION["SOTBIT_REGIONS"]["UF_PHONE"];

	if ($name && strlen($phone)>0) {

		if($name=='Казань') $format='/^\+(\d)(\d{3})(\d{3})(\d{2})(\d{2})$/';
		else $format='/^\+(\d)(\d{4})(\d{2})(\d{2})(\d{2})$/';

		if (preg_match($format, $phone, $matches)) {
			$phoneformat = '+' . $matches[1] . '(' . $matches[2] . ') ' . $matches[3] . '-' . $matches[4] . '-' . $matches[5];
			$phoneblur = '+' . $matches[1] . '(' . $matches[2] . ') ' . $matches[3] . '-' . $matches[4] . '-<span class="blur-phone">' . $matches[5] . '</span>';

		} else {
			$phoneblur = $phoneformat = $phone;
		}

		return array(
			'address' => $_SESSION["SOTBIT_REGIONS"]["UF_ADDRESS"],
			'time' => $_SESSION["SOTBIT_REGIONS"]["UF_WORKTIME"],
			'email' => $_SESSION["SOTBIT_REGIONS"]["UF_EMAIL"][0],
			'props' => $_SESSION["SOTBIT_REGIONS"]["UF_PROPS"],
			'phone' => $phoneformat,
			'phoneblur' => $phoneblur,
			'phonelink' => $phone,
			'name' => $name,
			'yandex' => $_SESSION["SOTBIT_REGIONS"]["UF_YANDEX"],
			'google' => $_SESSION["SOTBIT_REGIONS"]["UF_GOOGLE"],
			'google-tag' => $_SESSION["SOTBIT_REGIONS"]["UF_GOOGLE_TAGS"]
		);

	} else {
		$name = 'Казань';
	}
/**/
	switch ($name) {
		case 'Набережные Челны':
			return array(
				'props'=> $props,
				'email' => $email,
				'address'=>'г. Набережные Челны, Казанский пр. 149, <br />офис 27',
				'time'=>'Пн - Пт: 08:00 - 17:00 <br>Сб: 08:00 - 14:00',
				'phone'=>'+7(8552) 20-00-28',
				'phoneblur'=>'+7(8552) 20-00-<span class="blur-phone">28</span>',
				'phonelink'=>'+78552200028',
				'name'=>$name,
				'yandex'=>'57510091',
				'google'=>'UA-158368903-1',
				//'google-tag'=>'GTM-TW22JJK'
				'google-tag'=>'GTM-WP562VB'
			);
			break;

		case 'Ижевск':
			return array(
				'props'=> $props,
				'email' => $email,
				'address'=>'г. Ижевск, ул. Карла Маркса 395, <br />офис 108',
				'time'=>'Пн - Пт: 08:00 - 19:00 <br>Сб: 09:00 - 14:00',
				'phone'=>'+7 (3412) 90-72-87',
				'phoneblur'=>'+7 (3412) 90-72-<span class="blur-phone">87</span>',
				'phonelink'=>'+73412907287',
				'name'=>$name,
				'yandex'=>'61907452',
				'google'=>'UA-130631151-2',
				//'google-tag'=>'GTM-TTKTX2V'
				'google-tag'=>'GTM-WP562VB'
			);
			break;

		case 'Казань':
			return array(
				'props'=> $props,
				'email' => $email,
				'address'=>'г. Казань, ул. Даурская 12а, этаж 3, <br />офис 20.4',
				'time'=>'Пн - Пт: 08:00 - 19:00 <br>Сб: 09:00 - 14:00',
				'phone'=>'+7 (843) 216-32-67',
				'phoneblur'=>'+7 (843) 216-32-<span class="blur-phone">67</span>',
				'phonelink'=>'+78432163267',
				'name'=>$name,
				'yandex'=>'53011426',
				'google'=>'UA-130631151-1',
				'google-tag'=>'GTM-WP562VB'
			);
			break;
	}

}

function correctSmartFilter(&$arResult) {
	if (!empty($_SESSION["SOTBIT_REGIONS"]['UF_HIDEPRODUCERS']) && count($_SESSION["SOTBIT_REGIONS"]['UF_HIDEPRODUCERS'])) {
		foreach ($arResult['ITEMS'] as $id => &$vobj)
			if ($vobj['CODE'] == 'ATT_MAKER') {
				foreach ($vobj['VALUES'] as $vid => &$vd) {
					if (in_array($vd['VALUE'], $_SESSION["SOTBIT_REGIONS"]['UF_HIDEPRODUCERS']))
						unset($vobj['VALUES'][$vid]);
				}

			}
	}
}

function setProducerFilter(&$bbFilter) {
// #FW HIDE PRODUCERS
	if (isset($_SESSION["SOTBIT_REGIONS"]['UF_HIDEPRODUCERS']) && is_array($_SESSION["SOTBIT_REGIONS"]['UF_HIDEPRODUCERS']) && count($_SESSION["SOTBIT_REGIONS"]['UF_HIDEPRODUCERS']))
		$bbFilter['!PROPERTY_ATT_MAKER_VALUE'] = $_SESSION["SOTBIT_REGIONS"]['UF_HIDEPRODUCERS'];
}

function getRegionsFilter() {
	return array(
		"LOGIC" => "OR",
		array(
			'PROPERTY_REGIONS' => $_SESSION['SOTBIT_REGIONS']['ID']
		),
		array(
			'PROPERTY_REGIONS' => false
		)
	);
}

function getSEOSiteName() {
	$name = getSiteName();

	switch ($name) {
		case 'Набережные Челны':
			$name = 'Набережных Челнах';
		break;

		case 'Ижевск':
			$name = 'Ижевске';
		break;

		case 'Казань':
			$name = 'Казани';
			break;
	}

	return ' в '.$name;
}


AddEventHandler("main", "OnEndBufferContent", "deleteKernelCss");
 
function deleteKernelCss(&$content) {
   global $USER, $APPLICATION;
    
   if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
   if($APPLICATION->GetProperty("save_kernel") == "Y") return;
 
   $arPatternsToRemove = Array(
      '/<li nk.+?href=".+?kernel_main\/kernel_main\.css\?\d+"[^>]+>/',
   );
 
   $content = preg_replace($arPatternsToRemove, "", $content);
   $content = preg_replace("/\n{2,}/", "\n\n", $content);
}
 
AddEventHandler("main", "OnEndBufferContent", "includeCssInline");
 
function includeCssInline(&$content) {
   global $USER, $APPLICATION;
    
   if((is_object($USER) && $USER->IsAuthorized()) || strpos($APPLICATION->GetCurDir(), "/bitrix/")!==false) return;
   if($APPLICATION->GetProperty("save_kernel") == "Y") return;
 
   preg_match('/<li nk.+?href="(\/bitrix\/cache\/css\/'.SITE_ID.'\/'.SITE_TEMPLATE_ID.'\/template_[^"]+)"[^>]+>/', $content, $arMatches);
   $sFilePath = "http://".$_SERVER["HTTP_HOST"].$arMatches[1];
    
   $obCache = new CPHPCache;
   $life_time = 0*60;
    
   if($obCache->InitCache($life_time, $sFilePath, "/")) {
      $vars         = $obCache->GetVars();
      $sIncludeCss      = $vars["sIncludeCss"];
      $sIncludeCssClear   = $vars["sIncludeCssClear"];
       
   } else {
      $sIncludeCss      = file_get_contents($sFilePath);
      $sIncludeCssClear   = compressCSS($sIncludeCss);
       
   }
    
   if(false) {
      ?><pre style="font-size:10px;line-height: 8px;">До: <?=strlen($sIncludeCss);?></pre><?
      ?><pre style="font-size:10px;line-height: 8px;">После: <?=strlen($sIncludeCssClear);?></pre><?
   }
    
   $content = str_replace($arMatches[0], "<st yle>$sIncludeCssClear</style>", $content);
    
   if($obCache->StartDataCache()) {
      $obCache->EndDataCache(array(
         "sIncludeCss" => $sIncludeCss,
         "sIncludeCssClear" => $sIncludeCssClear,
      ));
   }
}
 
function compressCSS($css, $arOptions = Array()) {
   $sResult = $css;
    
   $sResult = preg_replace("/\/\*[^*]+\*\//", "", $sResult);      // comments
   $sResult = preg_replace("/\/\**\*\//", "", $sResult);         // comments
   $sResult = preg_replace("/\s*(:|,|;|{|}|\t)\s*/", "$1", $sResult);   // whitespaces
   $sResult = preg_replace("/(\t+|\s{2,})/", " ", $sResult);      // tabs and double whitespace
   $sResult = preg_replace("/(\s|:)([\-]{0,1}0px)\s/", " 0 ", $sResult);   // zeros
   //$sResult = preg_replace("/#(\w){6};/", "#$1$1$1;", $sResult);      // #dddddd => #ddd
 
   return $sResult;
}

?>
