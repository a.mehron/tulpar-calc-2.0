<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label pink"></div>
                      <div class="section-name">По формату</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/shamotnyy_kirpich_shb-5-230-114-65/">ШБ-5 (230*114*65)</a></li>
                          <li><a href="/catalog/shamotnyy_kirpich_shb-8-250-124-65/">ШБ-8 (250*124*65)</a></li>
                      </ul>
                  </div>
            </div>

        </div>
    </div>
 </div>
