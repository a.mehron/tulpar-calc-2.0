<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По бренду</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/po-brendu-mp-proekt/">МП Проект</a></li>
                      <li><a href="/catalog/po-brendu-mp-prestizh/">МП Престиж</a></li>
	                  <li><a href="/catalog/po-brendu-mp-modern/">МП Модерн</a></li>
	                  <li><a href="/catalog/po-brendu-mp-byudzhet/">МП Бюджет</a></li>
	                  <li><a href="/catalog/po-brendu-grandsystem/">Grandsystem</a></li>
                      <li><a href="/catalog/po-brendu-grand-line/">Grand Line</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По типу</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/po-tipu-ugol-zheloba/">Угол желоба</a></li>
                          <li><a href="/catalog/po-tipu-truba-soedinitelnaya/">Труба соединительная</a></li>
                          <li><a href="/catalog/po-tipu-truba-vodostochnaya/">Труба водосточная</a></li>
                          <li><a href="/catalog/po-tipu-troynik-truby/">Тройник трубы</a></li>
                          <li><a href="/catalog/po-tipu-soedinitel-zheloba/">Соединитель желоба</a></li>
                          <li><a href="/catalog/po-tipu-pauk/">Паук</a></li>
                          <li><a href="/catalog/po-tipu-ogranichitel-pereliva/">Ограничитель перелива</a></li>
                          <li><a href="/catalog/po-tipu-koleno-truby/">Колено трубы</a></li>
                          <li><a href="/catalog/po-tipu-zaglushka-zheloba/">Заглушка желоба</a></li>
                          <li><a href="/catalog/po-tipu-zhelob-vodostochnyy/">Желоб водосточный</a></li>
                          <li><a href="/catalog/po-tipu-derzhatel-truby/">Держатель трубы</a></li>
                          <li><a href="/catalog/po-tipu-derzhatel-zheloba/">Держатель желоба</a></li>
                          <li><a href="/catalog/po-tipu-voronka-vypusknaya/">Воронка выпускная</a></li>
                          <li><a href="/catalog/po-tipu-voronka-vodosbornaya/">Воронка водосборная</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По типу сечения</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/po-tipu-secheniya-pryamougolnyy/">Прямоугольный</a></li>
                      <li><a href="/catalog/po-tipu-secheniya-kruglyy/">Круглый</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-danger"></div>
                  <div class="section-name">По цвету</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/po-tsvetu-otsinkovannyy/">Оцинкованный</a></li>
                      <li><a href="/catalog/po-tsvetu-krasnyy/">Красный</a></li>
                      <li><a href="/catalog/po-tsvetu-seryy/">Серый</a></li>
                      <li><a href="/catalog/po-tsvetu-korichnevyy/">Коричневый</a></li>
                      <li><a href="/catalog/po-tsvetu-zelenyy/">Зеленый</a></li>
                      <li><a href="/catalog/po-tsvetu-chernyy/">Черный</a></li>
                      <li><a href="/catalog/po-tsvetu-belyy/">Белый</a></li>

                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>