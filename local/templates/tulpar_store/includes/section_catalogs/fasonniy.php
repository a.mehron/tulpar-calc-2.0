<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По цвету</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/figurnyy_kirpich_krasnyy/">красный</a></li>
                      <li><a href="/catalog/figurnyy_kirpich_korichnevyy/">коричневый</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label yellow"></div>
                  <div class="section-name">По форме</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/figurnyy_kirpich_vognutyy/">выпуклый</a></li>
                      <li><a href="/catalog/figurnyy_kirpich_vypuklyy/">вогнутый</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По пустотности</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/figurnyy_kirpich_pustotelyy/">Пустотелый</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/figurnyy_kirpich_arskiy/">Арский</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По формату</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/figurnyy_kirpich_odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
                          <li><a href="/catalog/figurnyy_kirpich_polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>
                      </ul>
                  </div>
            </div>

        </div>
    </div>
 </div>
