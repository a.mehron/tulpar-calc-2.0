<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По толщине</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/sayding-po-tolshchine-0-45/">0,45</a></li>
					  <li><a href="/catalog/sayding-po-tolshchine-0-5/">0,5</a></li>
                  </ul>
                </div>
            </div>
			
			<div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По покрытию</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/sayding-stalnoy-barkhat/">Стальной бархат</a></li>
						  <li><a href="/catalog/sayding-re/">РЕ</a></li>
						  <li><a href="/catalog/sayding-velur/">Velur</a></li>
						  <li><a href="/catalog/sayding-satin-matt/">Satin Matt</a></li>
						  <li><a href="/catalog/sayding-atlas/">Atlas</a></li>
						  <li><a href="/catalog/sayding-colority-print/">Colority Print</a></li>
						  <li><a href="/catalog/sayding-drap/">Drap</a></li>
						  <li><a href="/catalog/sayding-colority-print-dp/">Colority Print dp</a></li>
						  <li><a href="/catalog/sayding-greencoat-pural-bt/">GreenCoat Pural BT</a></li>
						  <li><a href="/catalog/sayding-greencoat-pural-matt-bt/">GreenCoat Pural Matt BT</a></li>
						  <li><a href="/catalog/sayding-print-rf/">Print РФ</a></li>
						  <li><a href="/catalog/sayding-purlite-matt/">PurLite Matt</a></li>
						  <li><a href="/catalog/sayding-quarzit/">Quarzit</a></li>
						  <li><a href="/catalog/sayding-quarzit-lite/">Quarzit lite</a></li>
						  <li><a href="/catalog/sayding-quarzit-pro-matt/">Quarzit Pro Matt</a></li>
						  <li><a href="/catalog/sayding-safari/">Safari</a></li>
						  <li><a href="/catalog/sayding-satin/">Satin</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По виду</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/sayding-korabelnaya-doska/">Корабельная доска</a></li>
						  <li><a href="/catalog/sayding-vertikal/">Вертикаль</a></li>
						  <li><a href="/catalog/sayding-ekobrus-gofr/">ЭкоБрус/Gofr</a></li>
						  <li><a href="/catalog/sayding-blok-khaus/">Блок-хаус</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/sayding-po-proizvoditelyu-grand-line/">Grand Line</a></li>
                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>