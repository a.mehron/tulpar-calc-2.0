<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По типу</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/keramicheskiy-oblitsovochnyy/">Облицовочный</a></li>
                      <li><a href="/catalog/ryadovoy/">Рядовой</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По пустотности</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/keram_kirpich_pustotelyy/">Пустотелый</a></li>
                          <li><a href="/catalog/keram_kirpich_polnotelyy/">Полнотелый</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По цвету</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/keram_kirpich_krasnyy/">красный</a></li>
                      <li><a href="/catalog/keramicheskiy-oblitcovochniy-bavarskaya-kladka/">баварская кладка</a></li>
                      <li><a href="/catalog/keram_kirpich_zheltyy/">желтый</a></li>
                      <li><a href="/catalog/keram_kirpich_korichnevyy/">коричневый</a></li>
                      <li><a href="/catalog/keram_kirpich_seryy/">серый</a></li>
                      <li><a href="/catalog/keram_kirpich_belyy-1/">белый</a></li>
                      <li><a href="/catalog/keram_kirpich-flesh-obzhig/">флеш-обжиг</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-danger"></div>
                  <div class="section-name">По поверхности</div>

                  <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/keramicheskiy-barkhat/">Бархат</a></li>
						 <li><a href="/catalog/keramicheskiy-galich/">Галич</a></li>
						 <li><a href="/catalog/keramicheskiy-gladkiy/">Гладкий</a></li>
						 <li><a href="/catalog/keramicheskiy-dikiy-kamen/">Дикий камень</a></li>
						 <li><a href="/catalog/keramicheskiy-kora-dereva/">Кора дерева</a></li>
						 <li><a href="/catalog/keramicheskiy-krokodilovaya-kozha/">Крокодиловая кожа</a></li>
						 <li><a href="/catalog/keramicheskiy-loft/">Лофт</a></li>
						 <li><a href="/catalog/keramicheskiy-rvanyy-kamen/">Рваный камень</a></li>
						 <li><a href="/catalog/keramicheskiy-rustik/">Рустик</a></li>
						 <li><a href="/catalog/keramicheskiy-ruchnaya-formovka/">Ручная формовка</a></li>
						 <li><a href="/catalog/keramicheskiy-sakhara/">Сахара</a></li>
						 <li><a href="/catalog/keramicheskiy-rustik-kora-dereva/">Рустик кора дерева </a></li>
						 <li><a href="/catalog/keramicheskiy-antik/">Антик</a></li>
						 <li><a href="/catalog/keramicheskiy-vizantiya/">Византия</a></li>
						 <li><a href="/catalog/keramicheskiy-rust/">Руст</a></li>
						 <li><a href="/catalog/keramicheskiy-skala/">Скала</a></li>
						 <li><a href="/catalog/keramicheskiy-galich-s-peskom/">Галич с песком</a></li>
						 <li><a href="/catalog/keramicheskiy-terra/">Терра</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                       <li><a href="/catalog/keram_kirpich_alekseevskiy/">Алексеевский</a></li>
						 <li><a href="/catalog/keram_kirpich_arskiy/">Арский</a></li>
						 <li><a href="/catalog/keram_kirpich_votkinskiy/">Воткинский</a></li>
						 <li><a href="/catalog/keram_kirpich_izhevskiy/">Ижевский</a></li>
						 <li><a href="/catalog/keram_kirpich_kazanskiy/">Казанский</a></li>
						 <li><a href="/catalog/keram_kirpich_kirovo-chepetskiy/">Кирово-Чепецкий</a></li>
						 <li><a href="/catalog/keram_kirpich_klyuchishchenskiy/">Ключищенский</a></li>
						 <li><a href="/catalog/keram_kirpich_koshchakovskiy/">Кощаковский</a></li>
						 <li><a href="/catalog/keram_kirpich_kurkachinskiy/">Куркачинский</a></li>
						 <li><a href="/catalog/keram_kirpich_mamadyshskiy/">Мамадышский</a></li>
						 <li><a href="/catalog/keram_kirpich_chaykovskiy/">Чайковский</a></li>
						 <li><a href="/catalog/keram_kirpich_shelangovskiy/">Шеланговский</a></li>
						 <li><a href="/catalog/keram_kirpich_cheboksarskiy/">Чебоксарский</a></li>
						 <li><a href="/catalog/keram_kirpich_bakalinskiy-1/">Бакалинский</a></li>
						 <li><a href="/catalog/keram_kirpich_tuymazinskiy-1/">Туймазинский</a></li>
						 <li><a href="/catalog/keram_kirpich_chekmagushskiy-1/">Чекмагушевский</a></li>
						 <li><a href="/catalog/keram_kirpich-saranskiy/">Саранский</a></li>
						 <li><a href="/catalog/keram_kirpich_chelninskiy/">Челнинский</a></li>
						 <li><a href="/catalog/keram_kirpich_izhevskiy-izkm/">Ижевский (ИЗКМ) </a></li>
						 <li><a href="/catalog/keram_kirpich_kerma/">Керма</a></li>
						 <li><a href="/catalog/keram_kirpich_belebeevskiy/">Белебеевский</a></li>
						 <li><a href="/catalog/keram_kirpich_mstera/">MSTERA</a></li>
						 <li><a href="/catalog/keram_kirpich_yadrinskiy/">Ядринский</a></li>
						 <li><a href="/catalog/keram_kirpich_revdinskiy/">Ревдинский</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По формату</div>

                      <ul class="list-unstyled pb-3">
					      <li><a href="/catalog/keram_kirpich_evroformat-0-7-nf/">Евроформат (0,7 НФ)</a></li>
                          <li><a href="/catalog/keram_kirpich_odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
                          <li><a href="/catalog/keram_kirpich_polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>
                          <li><a href="/catalog/keram_kirpich_dvoynoy-2-1-nf/">Двойной (2,1 НФ)</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label pink"></div>
                      <div class="section-name">По марке</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/keram_kirpich_m-100/">М 100</a></li>
                          <li><a href="/catalog/keram_kirpich_m-125/">М 125</a></li>
                          <li><a href="/catalog/keram_kirpich_m-150/">М 150</a></li>
                          <li><a href="/catalog/keram_kirpich_m-175/">М 175</a></li>
                          <li><a href="/catalog/keram_kirpich-m-125-175/">M 125-175</a></li>
                          <li><a href="/catalog/keram_kirpich-m-125-200/">M 125-200</a></li>
                          <li><a href="/catalog/keram_kirpich_m-200/">M 200</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-warning"></div>
                  <div class="section-name">По объёму</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/optom/">Оптом</a></li>
                      <li><a href="/catalog/v-roznitsu/">В розницу</a></li>
                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>