<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label pink"></div>
                      <div class="section-name">По марке</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/stroitelnyy_kirpich_m-100/">М 100</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_m-125/">М 125</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_m-150/">М 150</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_m-150-200/">М 150-200</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_m-175/">М 175</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_m-200/">М 200</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/stroitelnyy_kirpich_alekseevskiy/">Алексеевский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_votkinskiy/">Воткинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_kazanskiy/">Казанский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_ketra/">Кетра</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_klyuchishchenskiy/">Ключищенский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_kurkachinskiy/">Куркачинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_mamadyshskiy/">Мамадышский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_chaykovskiy/">Чайковский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_cheboksarskiy/">Чебоксарский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_bakalinskiy-1/">Бакалинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_tuymazinskiy-1/">Туймазинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_chekmagushskiy-1/">Чекмагушевский</a></li>
						  <li><a href="/catalog/stroitelniy-kirpich-kamastroyindustriya/">Челнинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_shelangovskiy/">Шеланговский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_zsk-chelny/">ЗСК Челны</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_dyurtyulinskiy/">Дюртюлинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_meakir/">Меакир</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_mstera/">MSTERA</a></li>
						  <li><a href="/catalog/stroitelniy-kirpich-revdinskiy/">Ревдинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_yadrinskiy/">Ядринский</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По формату</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/stroitelnyy_kirpich_odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>
                      </ul>
                  </div>
            </div>



            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По пустотности</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/stroitelnyi_kirpich_polnotelyy/">Полнотелый</a></li>
                          <li><a href="/catalog/stroitelnyi-kirpich-s-tremya-tekhnologicheskimi-otverstiyami/">С тремя технологическими отверстиями</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По цвету</div>

                  <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/stroitelnyy_kirpich_belyy/">Белый</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_krasnyy/">Красный</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_zheltyy-1/">Желтый</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_seryy-1/">Серый</a></li>
                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>