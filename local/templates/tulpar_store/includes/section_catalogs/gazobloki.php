<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По марке</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/gazobeton-d-300/">D 300</a></li>
                      <li><a href="/catalog/gazobeton-d-350/">D 350</a></li>
                      <li><a href="/catalog/gazobetonnye_bloki_d-400/">D 400</a></li>
                      <li><a href="/catalog/gazobetonnye_bloki_d-500/">D 500</a></li>
                      <li><a href="/catalog/gazobetonnye_bloki_d-600/">D 600</a></li>
                     <li><a href="/catalog/gazobetonnye_bloki_d-700_/">D 700</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                     <li><a href="/catalog/gazobeton_gras/">ГРАС</a></li>
                     <li><a href="/catalog/gazobetonnye_bloki_bikton_/">Биктон</a></li>
                      <li><a href="/catalog/zyab3/">ЗЯБ</a></li>
                      <li><a href="/catalog/gazobeton_uniblock/">Uniblock</a></li>
					  <li><a href="/catalog/gazobeton_teplon/">Теплон</a></li>
					  <li><a href="/catalog/gazobeton_kottedzh/">Коттедж</a></li>
                      <li><a href="/catalog/gazobeton_zyab-izhevsk/">ЗЯБ (Ижевск)</a></li>
                      <li><a href="/catalog/gazoblok_build-stone/">Build stone</a></li>

                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>