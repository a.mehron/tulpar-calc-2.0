<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/peremychki-raff/">РАФФ</a></li>
					  <li><a href="/catalog/peremychki-kulonstroy/">Кулонстрой</a></li>
	                  <li><a href="/catalog/peremychki-kamgeszyab/">КамгэсЗЯБ</a></li>
                  </ul>
                </div>
            </div>
			
			<div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По ширине</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/peremychki-po-shirine-120/">120мм</a></li>
						  <li><a href="/catalog/peremychki-po-shirine-250/">250мм</a></li>
						  <li><a href="/catalog/peremychki-po-shirine-380/">380мм</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По высоте</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/peremychki-po-vysote-65/">65мм</a></li>
						  <li><a href="/catalog/peremychki-po-vysote-90/">90мм</a></li>
						  <li><a href="/catalog/peremychki-po-vysote-140/">140мм</a></li>
						  <li><a href="/catalog/peremychki-po-vysote-190/">190мм</a></li>
						  <li><a href="/catalog/peremychki-po-vysote-220/">220мм</a></li>
						  <li><a href="/catalog/peremychki-po-vysote-440/">440мм</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По длине</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/peremychki-po-dline-1030/">1030мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-1290/">1290мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-1550/">1550мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-1680/">1680мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-1810/">1810мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-1940/">1940мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-2070/">2070мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-2200/">2200мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-2460/">2460мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-2590/">2590мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-2720/">2720мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-2850/">2850мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-2980/">2980мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-3110/">3110мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-3370/">3370мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-3630/">3630мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-3890/">3890мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-3980/">3980мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-4410/">4410мм</a></li>
					  <li><a href="/catalog/peremychki-po-dline-5960/">5960мм</a></li>
                  </ul>
                </div>
            </div>


        </div>
    </div>
 </div>