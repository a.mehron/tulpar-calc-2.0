<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По виду покрытия</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/metallocherepitsa-poliester/">Полиэстер</a></li>
                      <li><a href="/catalog/metallocherepitsa-normanmp/">NormanMP</a></li>
                      <li><a href="/catalog/metallocherepitsa-purman/">Purman</a></li>
                      <li><a href="/catalog/metallocherepitsa-agneta-2x/">Agneta</a></li>
                      <li><a href="/catalog/metallocherepitsa-cloudy/">Cloudy®</a></li>
                      <li><a href="/catalog/metallocherepitsa-plastizol/">Пластизол</a></li>
                      <li><a href="/catalog/metallocherepitsa-colorcoat-prisma/">Colorcoat Prisma®</a></li>
                      <li><a href="/catalog/metallocherepitsa-colorcoat-prisma-v-plenke/">Colorcoat Prisma® в пленке</a></li>
                      <li><a href="/catalog/metallocherepitsa-puretan/">Puretan</a></li>
                      <li><a href="/catalog/metallocherepitsa-vikingmp/">VikingMP®</a></li>
                      <li><a href="/catalog/metallocherepitsa-vikingmp-e/">VikingMP® Е</a></li>
					  <li><a href="/catalog/metallocherepitsa-print-rf/">Print РФ</a></li>
					  <li><a href="/catalog/metallocherepitsa-re-matt/">РЕ Matt</a></li>
					  <li><a href="/catalog/metallocherepitsa-re/">РЕ</a></li>
					  <li><a href="/catalog/metallocherepitsa-satin/">Satin</a></li>
					  <li><a href="/catalog/metallocherepitsa-drap/">Drap</a></li>
					  <li><a href="/catalog/metallocherepitsa-satin-matt/">Satin Matt</a></li>
					  <li><a href="/catalog/metallocherepitsa-stalnoy-barkhat/">Стальной бархат</a></li>
					  <li><a href="/catalog/metallocherepitsa-purlite-matt/">PurLite Matt</a></li>
					  <li><a href="/catalog/metallocherepitsa-greencoat-pural-bt/">GreenCoat Pural BT</a></li>
					  <li><a href="/catalog/metallocherepitsa-greencoat-pural-matt-bt/">GreenCoat Pural Matt BT</a></li>
					  <li><a href="/catalog/metallocherepitsa-atlas/">Atlas</a></li>
					  <li><a href="/catalog/metallocherepitsa-velur/">Velur</a></li>
					  <li><a href="/catalog/metallocherepitsa-quarzit-lite/">Quarzit lite</a></li>
					  <li><a href="/catalog/metallocherepitsa-quarzit/">Quarzit</a></li>
					  <li><a href="/catalog/metallocherepitsa-quarzit-pro-matt/">Quarzit Pro Matt</a></li>
					  <li><a href="/catalog/metallocherepitsa-safari/">Safari</a></li>
					  <li><a href="/catalog/metallocherepitsa-colority-print/">Colority Print</a></li>
                  </ul>
                </div>
            </div>

<!--            <div class="col-xl-3 col-sm-6">-->
<!--                <div class="catalog-section-list">-->
<!--                  <div class="color-label yellow"></div>-->
<!--                  <div class="section-name">По цвету</div>-->
<!---->
<!--                  <ul class="list-unstyled pb-3">-->
<!--                      <li><a href="/catalog/metallocherepitsa-belyy/">белый</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-zheltyy/">желтый</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-oranzhevyy/">оранжевый</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-krasnyy/">красный</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-siniy/">синий</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-zelenyy/">зеленый</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-korichnevyy/">коричневый</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-seryy/">серый</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-chernyy/">черный</a></li>-->
<!--                      <li><a href="/catalog/metallocherepitsa-fioletovyy/">фиолетовый</a></li>-->
<!--                  </ul>-->
<!--                </div>-->
<!--            </div>-->
			
			            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/metallocherepitsa-po-proizvoditelyu-metall-profil">Металл Профиль</a></li>
                      <li><a href="/catalog/metallocherepitsa-po-proizvoditelyu-grand-line">Grand Line</a></li>
                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>
