<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

			      <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label pink"></div>
                      <div class="section-name">По виду покрытия</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/sofity-re/">РЕ</a></li>
                         <li><a href="/catalog/sofity-satin/">Satin</a></li>
                         <li><a href="/catalog/sofity-drap/">Drap</a></li>
                         <li><a href="/catalog/sofity-satin-matt/">Satin Matt</a></li>
                         <li><a href="/catalog/sofity-stalnoy-barkhat/">Стальной бархат</a></li>
                         <li><a href="/catalog/sofity-purlite-matt/">PurLite Matt</a></li>
                         <li><a href="/catalog/sofity-greencoat-pural-bt/">GreenCoat Pural BT</a></li>
                         <li><a href="/catalog/sofity-greencoat-pural-matt-bt/">GreenCoat Pural Matt BT</a></li>
                         <li><a href="/catalog/sofity-atlas/">Atlas</a></li>
                         <li><a href="/catalog/sofity-velur/">Velur</a></li>
                         <li><a href="/catalog/sofity-quarzit-lite/">Quarzit lite</a></li>
                         <li><a href="/catalog/sofity-quarzit/">Quarzit</a></li>
                         <li><a href="/catalog/sofity-quarzit-pro-matt/">Quarzit Pro Matt</a></li>
                         <li><a href="/catalog/sofity-print-rf/">Print РФ</a></li>
                         <li><a href="/catalog/sofity-safari/">Safari</a></li>
                         <li><a href="/catalog/sofity-colority-print/">Colority Print</a></li>
                         <li><a href="/catalog/sofity-ecosteel-t/">Ecosteel T</a></li>
                         <li><a href="/catalog/sofity-purman/">Purman</a></li>
                         <li><a href="/catalog/sofity-cloudy/">Cloudy</a></li>
                         <li><a href="/catalog/sofity-normanmp/">NormanMP</a></li>
                         <li><a href="/catalog/sofity-ecosteel-matt/">Ecosteel Matt</a></li>
                         <li><a href="/catalog/sofity-ecosteel/">Ecosteel</a></li>
                         <li><a href="/catalog/sofity-plastizol/">Пластизол</a></li>
                         <li><a href="/catalog/sofity-vikingmp-e/">VikingMP E</a></li>
                         <li><a href="/catalog/sofity-prisma/">Prisma</a></li>
                         <li><a href="/catalog/sofity-plastizol-dvukhstoronniy/">Пластизол (двухсторонний)</a></li>
                         <li><a href="/catalog/sofity-poliester-kod-syrya-12/">Полиэстер (код сырья 12)</a></li>
                         <li><a href="/catalog/sofity-poliester/">Полиэстер</a></li>
                         <li><a href="/catalog/sofity_vikingmp/">VikingMP</a></li>
                      </ul>
                  </div>
            </div>	
            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По производителю</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/sofity-metall-profil/">Металл Профиль</a></li>
                         <li><a href="/catalog/sofity-grand-line/">Grand Line</a></li>
                      </ul>
                  </div>
            </div>
			      <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label blue"></div>
                      <div class="section-name">По толщине</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/sofity-0-45/">0.45</a></li>
                         <li><a href="/catalog/sofity-0-5/">0.5</a></li>
                      </ul>
                  </div>
            </div>
			      <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label pink"></div>
                      <div class="section-name">По виду</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/sofity-s-polnoy-perforatsiey/">С полной перфорацией</a></li>
                         <li><a href="/catalog/sofity-s-chastichnoy-perforatsiey/">С частичной перфорацией</a></li>
                         <li><a href="/catalog/sofity-bez-perforatsii/">Без перфорации</a></li>
                      </ul>
                  </div>
            </div>

        </div>
    </div>
 </div>