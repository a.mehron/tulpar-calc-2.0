<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По высоте</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/plity-perekrytiya-po-vysote-160-mm/">160 мм</a></li>
					  <li><a href="/catalog/plity-perekrytiya-po-vysote-220-mm/">220 мм</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По материалу</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/zhelezobetonnye-zhb/">Железобетонные (ЖБ)</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/uniblock-plity-perekrytiya/">Uniblock (Униблок)</a></li>
					  <li><a href="/catalog/kamgeszyab-plity-perekrytiya/">КамгэсЗЯБ</a></li>
	                  <li><a href="/catalog/kulonstroy-plity-perekrytiya/">КулонСтрой</a></li>
					  <li><a href="/catalog/raff-plity-perekrytiya/">РАФФ</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-danger"></div>
                  <div class="section-name">По пустотности</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/plity-perekrytiya-po-pustotnosti-pustotnye/">Пустотные</a></li>
                  </ul>
                </div>
            </div>
			
			 <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label yellow"></div>
                  <div class="section-name">По типу</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/plity-perekrytiya-po-tipu-pb/">ПБ</a></li>
					  <li><a href="/catalog/plity-perekrytiya-po-tipu-pk/">ПК</a></li>
                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>