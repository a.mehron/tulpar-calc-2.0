<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По марке</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/klinkernyy-m-300-400/">М 300-400</a></li>
                      </ul>
                  </div>
            </div>
			            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По формату</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/klinkernyy-odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
                      </ul>
                  </div>
            </div>
			            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По цвету</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/klinkernyy-flesh-obzhig/">флеш-обжиг</a></li>
                         <li><a href="/catalog/klinkernyy-krasnyy/">красный</a></li>
                         <li><a href="/catalog/klinkernyy-korichnevyy/">коричневый</a></li>
                       <!--  <li><a href="/catalog/klinkernyy-belyy/">белый</a></li>-->
                      </ul>
                  </div>
            </div>
			            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По поверхности</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/klinkernyy-antik/">Антик</a></li>
                         <li><a href="/catalog/klinkernyy-gladkiy/">Гладкий</a></li>
                      </ul>
                  </div>
            </div>
            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По производителю</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/klinkernyy_saranskiy/">Саранский</a></li>
                      </ul>
                  </div>
            </div>			

        </div>
    </div>
 </div>