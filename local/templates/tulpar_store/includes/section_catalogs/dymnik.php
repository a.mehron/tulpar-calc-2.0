<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По типу</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/dymniki-dymnik/">Дымник</a></li>
                      <li><a href="/catalog/dymniki-dymnik-yelka/">Дымник ёлка</a></li>
					  <li><a href="/catalog/dymniki-dymnik-s-zhalyuzi/">Дымник с жалюзи</a></li>
					  <li><a href="/catalog/dymniki-dymnik-s-setkoy/">Дымник с сеткой</a></li>
					  <li><a href="/catalog/dymniki-dymnik-dvoynoy-s-zhalyuzi/">Дымник двойной с жалюзи</a></li>
					  <li><a href="/catalog/dymniki-dymnik-dvoynoy-s-setkoy/">Дымник двойной с сеткой</a></li>
					  <li><a href="/catalog/dymniki-dymnik-dvoynoy/">Дымник двойной</a></li>
					  <li><a href="/catalog/dymniki-kolpak-pod-fonar/">Колпак под фонарь</a></li>
					  <li><a href="/catalog/dymniki-kolpak-na-stolb/">Колпак на столб</a></li>
					  <li><a href="/catalog/dymniki-kolpak-dvoynoy/">Колпак двойной</a></li>
					  <li><a href="/catalog/dymniki-kozhukh-na-trubu-skatnyy/">Кожух на трубу скатный</a></li>
					  <li><a href="/catalog/dymniki-kozhukh-na-trubu-pryamoy/">Кожух на трубу прямой</a></li>
                  </ul>
                </div>
            </div>
			
			<div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По покрытию</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/dymniki-stalnoy-barkhat/">Стальной бархат</a></li>
						  <li><a href="/catalog/dymniki-colority-print/">Colority Print</a></li>
						  <li><a href="/catalog/dymniki-greencoat-pural-bt/">GreenCoat Pural BT</a></li>
						  <li><a href="/catalog/dymniki-greencoat-pural-matt-bt/">GreenCoat Pural Matt BT</a></li>
						  <li><a href="/catalog/dymniki-print-rf/">Print РФ</a></li>
						  <li><a href="/catalog/dymniki-purlite-matt/">PurLite Matt</a></li>
						  <li><a href="/catalog/dymniki-quarzit-lite/">Quarzit lite</a></li>
						  <li><a href="/catalog/dymniki-quarzit-pro-matt/">Quarzit Pro Matt</a></li>
						  <li><a href="/catalog/dymniki-satin-matt/">Satin Matt</a></li>
						  <li><a href="/catalog/dymniki-quarzit/">Quarzit</a></li>
                          <li><a href="/catalog/dymniki-drap/">Drap</a></li>
						  <li><a href="/catalog/dymniki-safari/">Safari</a></li>
						  <li><a href="/catalog/dymniki-satin/">Satin</a></li>
						  <li><a href="/catalog/dymniki-atlas/">Atlas</a></li>
						  <li><a href="/catalog/dymniki-velur/">Velur</a></li>
						  <li><a href="/catalog/dymniki-zn/">Zn</a></li>
						  <li><a href="/catalog/dymniki-re/">РЕ</a></li>
                      </ul>
                  </div>
            </div>


            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/dymniki-grand-line/">Grand Line</a></li>
                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>