<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По толщине</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/falts-po-tolshchine-0-45/">0,45</a></li>
					  <li><a href="/catalog/falts-po-tolshchine-0-5/">0,5</a></li>
					  <li><a href="/catalog/falts-po-tolshchine-0-55/">0,55</a></li>
					  <li><a href="/catalog/falts-po-tolshchine-0-7/">0,7</a></li>
                  </ul>
                </div>
            </div>
			
			<div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По покрытию</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/falts-atlas/">Atlas</a></li>
						  <li><a href="/catalog/falts-colority-print/">Colority Print</a></li>
						  <li><a href="/catalog/falts-drap/">Drap</a></li>
						  <li><a href="/catalog/falts-greencoat-pural-bt/">GreenCoat Pural BT</a></li>
						  <li><a href="/catalog/falts-greencoat-pural-matt-bt/">GreenCoat Pural Matt BT</a></li>
						  <li><a href="/catalog/falts-purlite-matt/">PurLite Matt</a></li>
						  <li><a href="/catalog/falts-quarzit/">Quarzit</a></li>
						  <li><a href="/catalog/falts-quarzit-lite/">Quarzit lite</a></li>
						  <li><a href="/catalog/falts-quarzit-pro-matt/">Quarzit Pro Matt</a></li>
						  <li><a href="/catalog/falts-safari/">Safari</a></li>
						  <li><a href="/catalog/falts-satin/">Satin</a></li>
						  <li><a href="/catalog/falts-satin-matt/">Satin Matt</a></li>
						  <li><a href="/catalog/falts-velur/">Velur</a></li>
						  <li><a href="/catalog/falts-zn/">Zn</a></li>
						  <li><a href="/catalog/falts-re/">РЕ</a></li>
						  <li><a href="/catalog/falts-stalnoy-barkhat/">Стальной бархат</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По виду</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/klikfalts-line/">Кликфальц Line</a></li>
						  <li><a href="/catalog/klikfalts-pro-line/">Кликфальц Pro Line</a></li>
						  <li><a href="/catalog/klikfalts-pro/">Кликфальц Pro</a></li>
						  <li><a href="/catalog/klikfalts-pro-gofr/">Кликфальц Pro Gofr</a></li>
						  <li><a href="/catalog/klikfalts-mini/">Кликфальц Mini</a></li>
						  <li><a href="/catalog/dvoynoy-stoyachiy-falts-line/">Двойной стоячий фальц Line</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/po-proizvoditelyu-falts-grand-line/">Grand Line</a></li>
                  </ul>
                </div>
            </div>


        </div>
    </div>
 </div>