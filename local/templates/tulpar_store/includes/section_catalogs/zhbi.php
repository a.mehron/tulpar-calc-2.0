<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/zhbi-po-proizvoditelyu-uniblock-uniblok/">Uniblock (Униблок)</a></li>
					  <li><a href="/catalog/zhbi-po-proizvoditelyu-kamgeszyab/">КамгэсЗЯБ</a></li>
					  <li><a href="/catalog/zhbi-po-proizvoditelyu-kulonstroy/">КулонСтрой</a></li>
					  <li><a href="/catalog/zhbi-po-proizvoditelyu-raff/">РАФФ</a></li>
                  </ul>
                </div>
            </div>
			

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">Тип изделия</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/zhbi-koltsa/">ЖБИ кольца</a></li>
						  <li><a href="/catalog/peremychki/">Перемычки</a></li>
						  <li><a href="/catalog/plity-perekrytiya/">Плиты перекрытия</a></li>
                      </ul>
                  </div>
            </div>

        </div>
    </div>
 </div>