<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По материалу</div> 

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/lestnitsy-po-materialu-derevyannye/">Дерево</a></li> 
                      <li><a href="/catalog/lestnitsy-po-materialu-metallicheskie/">Металл</a></li> 
                  </ul>
                </div>
            </div>

                        <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-danger"></div>
                  <div class="section-name">По типу</div> 

                  <ul class="list-unstyled pb-3">
                                <li><a href="/catalog/mezhetazhnye-lestnitsy/">Межэтажные</a></li> 
                                <li><a href="/catalog/lestnitsy-cherdachnye/">Чердачные</a></li> 
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-danger"></div>
                  <div class="section-name">По форме</div> 

                  <ul class="list-unstyled pb-3">
                                <li><a href="/catalog/po-forme-vintovye/">Винтовая</a></li> 
                                <li><a href="/catalog/po-forme-g-obraznye/">Г-образная</a></li> 
      							<li><a href="/catalog/po-forme-p-obraznye/">П-образная</a></li> 
                                <li><a href="/catalog/po-forme-pryamye/">Прямая</a></li> 
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div> 

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/lestnitsy-po-proizvoditelyu-fakro/">Fakro</a></li> 
                      <li><a href="/catalog/lestnitsy-po-proizvoditelyu-profi-hobby/">Profi&Hobby</a></li> 
                  </ul>
                </div>
            </div>



            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По виду</div> 

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/lestnitsy-po-vidu-gusinyy-shag/">Гусиный шаг</a></li> 
                          <li><a href="/catalog/lestnitsy-po-vidu-klassicheskie/">Классические</a></li> 
                          <li><a href="/catalog/lestnitsy-po-vidu-s-zabezhnymi-stupenyami/">С забежными ступенями</a></li> 
                          <li><a href="/catalog/lestnitsy-po-vidu-s-ploshchadkoy/">С площадкой</a></li> 
                      </ul>
                  </div>
            </div>

        </div>
    </div>
 </div>