<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По цвету</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/krasnyy/">красный</a></li>
                      <li><a href="/catalog/kirpich-bavarskaya-kladka/">баварская кладка</a></li>
                      <li><a href="/catalog/zheltyy-/">желтый</a></li>
                      <li><a href="/catalog/korichnevyy/">коричневый</a></li>
                      <li><a href="/catalog/seryy/">серый</a></li>
                      <li><a href="/catalog/belyy/">белый</a></li>
                      <li><a href="/catalog/kirpich-flesh-obzhig/">флеш-обжиг</a></li>
                      <li><a href="/catalog/chernyy/">черный</a></li>
					  <li><a href="/catalog/zelenyy/">зеленый</a></li>
					  <li><a href="/catalog/oranzhevyy/">оранжевый</a></li>
					  <li><a href="/catalog/rozovyy/">розовый</a></li>
					  <li><a href="/catalog/fioletovyy/">фиолетовый</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-danger"></div>
                  <div class="section-name">По поверхности</div>

                  <ul class="list-unstyled pb-3">
                             <li><a href="/catalog/kirpich-vizantiya/">Византия</a></li>
							<li><a href="/catalog/kirpich-barkhat/">Бархат</a></li>
							<li><a href="/catalog/kirpich-galich/">Галич</a></li>
							<li><a href="/catalog/kirpich-gladkiy/">Гладкий</a></li>
							<li><a href="/catalog/kirpich-dikiy-kamen/">Дикий камень</a></li>
							<li><a href="/catalog/kirpich-kora-dereva/">Кора дерева</a></li>
							<li><a href="/catalog/kirpich-krokodilovaya-kozha/">Крокодиловая кожа</a></li>
							<li><a href="/catalog/kirpich-loft/">Лофт</a></li>
							<li><a href="/catalog/kirpich-rvanyy-kamen/">Рваный камень</a></li>
							<li><a href="/catalog/kirpich-rustik/">Рустик</a></li>
							<li><a href="/catalog/kirpich-ruchnaya-formovka/">Ручная формовка</a></li>
							<li><a href="/catalog/kirpich-sakhara/">Сахара</a></li>
							<li><a href="/catalog/kirpich-terra/">Терра</a></li>
							<li><a href="/catalog/kirpich-antik/">Антик</a></li>
							<li><a href="/catalog/kirpich-rust/">Руст</a></li>
							<li><a href="/catalog/kirpich-rustik-kora-dereva/">Рустик кора дерева</a></li>
							<li><a href="/catalog/kirpich-skala/">Скала</a></li>
							<li><a href="/catalog/kirpich-relefnyy/">Рельефный</a></li>
							<li><a href="/catalog/kirpich-galich-s-peskom/">Галич с песком</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/alekseevskiy/">Алексеевский</a></li>
						  <li><a href="/catalog/arskiy/">Арский</a></li>
						  <li><a href="/catalog/votkinskiy/">Воткинский</a></li>
						  <li><a href="/catalog/izhevskiy/">Ижевский</a></li>
						  <li><a href="/catalog/kirovo-chepetskiy/">Кирово-Чепецкий</a></li>
						  <li><a href="/catalog/klyuchishchenskiy/">Ключищенский</a></li>
						  <li><a href="/catalog/koshchakovskiy/">Кощаковский</a></li>
						  <li><a href="/catalog/kurkachinskiy/">Куркачинский</a></li>
						  <li><a href="/catalog/mamadyshskiy/">Мамадышский</a></li>
						  <li><a href="/catalog/chaykovskiy/">Чайковский</a></li>
						  <li><a href="/catalog/shelangovskiy/">Шеланговский</a></li>
						  <li><a href="/catalog/kazanskiy/">Казанский</a></li>
						  <li><a href="/catalog/cheboksarskiy/">Чебоксарский</a></li>
						  <li><a href="/catalog/bakalinskiy-zavod/">Бакалинский</a></li>
						  <li><a href="/catalog/tuymazinskiy-zavod/">Туймазинский</a></li>
						  <li><a href="/catalog/chekmagushskiy-zavod/">Чекмагушский</a></li>
						  <li><a href="/catalog/saranskiy/">Саранский</a></li>
						  <li><a href="/catalog/chelninskiy/">Челнинский</a></li>
						  <li><a href="/catalog/belebeevskiy/">Белебеевский</a></li>
						  <li><a href="/catalog/kerma/">Керма</a></li>
						  <li><a href="/catalog/ketra/">Кетра</a></li>
						  <li><a href="/catalog/izhevskiy-izkm/">Ижевский (ИЗКМ)</a></li>
						  <li><a href="/catalog/mstera/">MSTERA</a></li>
						  <li><a href="/catalog/zsk-chelny/">ЗСК Челны</a></li>
						  <li><a href="/catalog/dyurtyulinskiy/">Дюртюлинский</a></li>
						  <li><a href="/catalog/inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
						  <li><a href="/catalog/meakir/">Меакир</a></li>
<li><a href="/catalog/revdinskiy/">Ревдинский</a></li>
<li><a href="/catalog/yadrinskiy/">Ядринский</a></li>
						  <li><a href="/catalog/kazanskiy-zavod-silikatnykh-stenovykh-materialov/">КЗССМ</a></li>
                  </ul>
                </div>
            </div>



            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По формату</div>

                      <ul class="list-unstyled pb-3">
					      <li><a href="/catalog/evroformat-0-7-nf/">Евроформат (0,7 НФ)</a></li>
                          <li><a href="/catalog/odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
                          <li><a href="/catalog/polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>
                          <li><a href="/catalog/dvoynoy-2-1-nf/">Двойной (2,1 НФ)</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label pink"></div>
                      <div class="section-name">По марке</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/m-100/">М 100</a></li>
                          <li><a href="/catalog/m-125/">М 125</a></li>
                          <li><a href="/catalog/m-150/">М 150</a></li>
                          <li><a href="/catalog/m-175/">М 175</a></li>
						  <li><a href="/catalog/m-200-po-marke/">М 200</a></li>
						  <li><a href="/catalog/m-250-po-marke/">М 250</a></li>
						  <!--<li><a href="/catalog/m-300-po-marke-1/">М 300</a></li>-->
						  <li><a href="/catalog/m-100-200-po-marke/">М 100-200</a></li>
                          <li><a href="/catalog/m-125-175/">M 125-175</a></li>
                          <li><a href="/catalog/m-125-200/">M 125-200</a></li>
						 <!-- <li><a href="/catalog/m-150-175-1/">M 150-175</a></li>-->
						  <li><a href="/catalog/m-150-200-po-marke/">M 150-200</a></li>
                          <li><a href="/catalog/m-300-400/">М 300-400</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label bg-success"></div>
                      <div class="section-name">По пустотности</div>

                      <ul class="list-unstyled pb-3">
                          <li><a href="/catalog/pustotelyy/">Пустотелый</a></li>
                          <li><a href="/catalog/polnotelyy/">Полнотелый</a></li>
                      </ul>
                  </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-warning"></div>
                  <div class="section-name">По объёму</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/optom/">Оптом</a></li>
                      <li><a href="/catalog/v-roznitsu/">В розницу</a></li>
                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>