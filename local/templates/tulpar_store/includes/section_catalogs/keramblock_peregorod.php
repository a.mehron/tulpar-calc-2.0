<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По формату</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/keramicheskie_bloki_4-5-nf/">4,5 НФ</a></li>
					  <li><a href="/catalog/keramicheskie_bloki_4-6-nf/">4,6 НФ</a></li>
                      <li><a href="/catalog/keramicheskie_bloki_5-7-nf/">5,7 НФ</a></li>
                      <li><a href="/catalog/keramicheskie_bloki_6-9-nf/">6,9 НФ</a></li>
					  <li><a href="/catalog/keramicheskie_bloki_10-7-nf/">10,7 НФ</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label yellow"></div>
                  <div class="section-name">По названию</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/keramicheskie_bloki_blok-8/">Блок 8</a></li>
                      <li><a href="/catalog/keramicheskie_bloki_blok-10/">Блок 10</a></li>
                      <li><a href="/catalog/keramicheskie_bloki_blok-12/">Блок 12</a></li>
					  <li><a href="/catalog/keramicheskie_bloki_blok-25/">Блок 25</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-success"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/peregorodochnyy-amstron-porikam/">Амстрон (Порикам)</a></li>
                      <li><a href="/catalog/peregorodochnyy-vinerberger-poroterm/">Винербергер (Поротерм)</a></li>
                      <li><a href="/catalog/peregorodochnyy-ketra/">Кетра</a></li>
					  <li><a href="/catalog/peregorodochnyy-kerakam/">Керакам</a></li>
                  </ul>
                </div>
            </div>

        </div>
    </div>
 </div>