<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По типу</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/kladochnyy-rastvor/">Кладочный раствор</a></li>
                         <li><a href="/catalog/tsvetnoy-rastvor/">Цветной раствор</a></li>
                         <li><a href="/catalog/teploizolyatsionnyy-rastvor/">Теплоизоляционный раствор</a></li>
                         <li><a href="/catalog/kley-dlya-gazobetona/">Клей для газобетона</a></li>
                      </ul>
                  </div>
            </div>
            <div class="col-xl-3 col-sm-6 border-right">
                  <div class="catalog-section-list">
                      <div class="color-label yellow"></div>
                      <div class="section-name">По производителю</div>

                      <ul class="list-unstyled pb-3">
                         <li><a href="/catalog/suh_stroiy_smes_zavod-yacheistykh-betonov/">Завод Ячеистых Бетонов</a></li>
                         <li><a href="/catalog/wienerberger-porotherm/">Wienerberger Porotherm</a></li>
                         <li><a href="/catalog/starkhaus/">Starkhaus</a></li>
                         <li><a href="/catalog/osnovit/">Основит</a></li>
						 <li><a href="/catalog/perel/">Perel</a></li>
						 <li><a href="/catalog/promix/">Promix</a></li>
                      </ul>
                  </div>
            </div>			

        </div>
    </div>
 </div>