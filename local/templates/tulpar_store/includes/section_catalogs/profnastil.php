<div class="section homepage-catalog catalog-page section-catalog">
    <div class="container">
        <div class="row catalog-section-lists rounded">

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label light-blue"></div>
                  <div class="section-name">По виду покрытия</div>

                  <ul class="list-unstyled pb-3">
                      <li><a href="/catalog/profnastil-poliester/">Полиэстер</a></li>
                      <li><a href="/catalog/profnastil-normanmp/">NormanMP</a></li>
                      <li><a href="/catalog/profnastil-purman/">Purman</a></li>
                      <li><a href="/catalog/profnastil-ecosteel/">Ecosteel</a></li>
                      <li><a href="/catalog/profnastil-agneta-2x/">Agneta</a></li>
                      <li><a href="/catalog/profnastil-cloudy/">Cloudy</a></li>
                      <li><a href="/catalog/profnastil-plastizol/">Пластизол</a></li>
                      <li><a href="/catalog/profnastil-colorcoat-prisma/">Colorcoat Prisma®</a></li>
                      <li><a href="/catalog/profnastil-colorcoat-prisma-v-plenke/">Colorcoat Prisma® в пленке</a></li>
                      <li><a href="/catalog/profnastil-puretan/">Puretan</a></li>
                      <li><a href="/catalog/profnastil-vikingmp/">VikingMP®</a></li>
                      <li><a href="/catalog/profnastil-vikingmp-e/">VikingMP® Е</a></li>
					    <li><a href="/catalog/profnastil-stalnoy-barkhat/">Стальной бархат</a></li>
						<li><a href="/catalog/profnastil-pe-dvs/">PE двс</a></li>
						<li><a href="/catalog/profnastil-pe-dachnyy-1/">PE Дачный(1)</a></li>
						<li><a href="/catalog/profnastil-pe-dachnyy-2/">PE Дачный(2)</a></li>
						<li><a href="/catalog/profnastil-pe-matt/">PE Matt</a></li>
						<li><a href="/catalog/profnastil-pe/">PE</a></li>
						<li><a href="/catalog/profnastil-zn/">Zn</a></li>
						<li><a href="/catalog/profnastil-velur/">Velur</a></li>
						<li><a href="/catalog/profnastil-satin-matt/">Satin Matt</a></li>
						<li><a href="/catalog/profnastil-satin/">Satin</a></li>
						<li><a href="/catalog/profnastil-safari/">Safari</a></li>
						<li><a href="/catalog/profnastil-quarzit-pro-matt/">Quarzit Pro Matt</a></li>
						<li><a href="/catalog/profnastil-quarzit-lite/">Quarzit lite</a></li>
						<li><a href="/catalog/profnastil-quarzit/">Quarzit</a></li>
						<li><a href="/catalog/profnastil-purlite-matt/">PurLite Matt</a></li>
						<li><a href="/catalog/profnastil-print-rf/">Print РФ</a></li>
						<li><a href="/catalog/profnastil-greencoat-pural-matt-bt/">GreenCoat Pural Matt BT</a></li>
						<li><a href="/catalog/profnastil-greencoat-pural-bt/">GreenCoat Pural BT</a></li>
						<li><a href="/catalog/profnastil-drap/">Drap</a></li>
						<li><a href="/catalog/profnastil-colority-print-dp/">Colority Print dp</a></li>
                        <li><a href="/catalog/profnastil-colority-print/">Colority Print</a></li>
						<li><a href="/catalog/profnastil-atlas/">Atlas</a></li>
                  </ul>
                </div>
            </div>

            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label bg-danger"></div>
                  <div class="section-name">По виду</div>

                  <ul class="list-unstyled pb-3">
                            <li><a href="/catalog/profnastil-s-8/">С-8</a></li>
                            <li><a href="/catalog/profnastil-s-8-figurnyy/">С-8 фигурный</a></li>
                            <li><a href="/catalog/profnastil-s-10/">С-10</a></li>
                            <li><a href="/catalog/profnastil-s-10-figurnyy/">С-10 фигурный</a></li>
                            <li><a href="/catalog/profnastil-s-20/">С-20</a></li>
                            <li><a href="/catalog/profnastil-с-21/">С-21</a></li>
                            <li><a href="/catalog/profnastil-s-44/">С-44</a></li>
                            <li><a href="/catalog/profnastil-mp-10/">МП-10</a></li>
                            <li><a href="/catalog/profnastil-mp-18/">МП-18</a></li>
                            <li><a href="/catalog/profnastil-mp-20/">МП-20</a></li>
                            <li><a href="/catalog/profnastil-mp-35/">МП-35</a></li>
                            <li><a href="/catalog/profnastil-n-60/">Н-60</a></li>
                            <li><a href="/catalog/profnastil-n-75/">Н-75</a></li>
                            <li><a href="/catalog/profnastil-n-114/">Н-114</a></li>
                            <li><a href="/catalog/profnastil-ns-35/">НС-35</a></li>
                  </ul>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label pink"></div>
                  <div class="section-name">По производителю</div>

                  <ul class="list-unstyled pb-3">
                            <li><a href="/catalog/profilirovannyy-list-po-proizvoditelyu-metall-profil/">Металл Профиль</a></li>
                            <li><a href="/catalog/profilirovannyy-list-po-proizvoditelyu-grand-line/">Grand Line</a></li>
                  </ul>
                </div>
            </div>
            <div class="col-xl-3 col-sm-6">
                <div class="catalog-section-list">
                  <div class="color-label yellow"></div>
                  <div class="section-name">По применению</div>

                  <ul class="list-unstyled pb-3">
                            <li><a href="/catalog/zabornyy-profnastil/">Заборный</a></li>
                            <li><a href="/catalog/krovelnyy-profnastil/">Кровельный</a></li>
                            <li><a href="/catalog/promyshlennyy-profnastil/">Промышленный</a></li>
                            <li><a href="/catalog/stenovoy-profnastil/">Фасадный (стеновой) </a></li>
                  </ul>
                </div>
            </div>


<!--            <div class="col-xl-3 col-sm-6">-->
<!--                <div class="catalog-section-list">-->
<!--                  <div class="color-label yellow"></div>-->
<!--                  <div class="section-name">По цвету</div>-->
<!---->
<!--                  <ul class="list-unstyled pb-3">-->
<!--                      <li><a href="/catalog/profnastil-belyy/">белый</a></li>-->
<!--                      <li><a href="/catalog/profnastil-zheltyy/">желтый</a></li>-->
<!--                      <li><a href="/catalog/profnastil-oranzhevyy/">оранжевый</a></li>-->
<!--                      <li><a href="/catalog/profnastil-krasnyy/">красный</a></li>-->
<!--                      <li><a href="/catalog/profnastil-siniy/">синий</a></li>-->
<!--                      <li><a href="/catalog/profnastil-zelenyy/">зеленый</a></li>-->
<!--                      <li><a href="/catalog/profnastil-korichnevyy/">коричневый</a></li>-->
<!--                      <li><a href="/catalog/profnastil-seryy/">серый</a></li>-->
<!--                      <li><a href="/catalog/profnastil-chernyy/">черный</a></li>-->
<!--                      <li><a href="/catalog/profnastil-fioletovyy/">фиолетовый</a></li>-->
<!--                  </ul>-->
<!--                </div>-->
<!--            </div>-->

        </div>
    </div>
 </div>
