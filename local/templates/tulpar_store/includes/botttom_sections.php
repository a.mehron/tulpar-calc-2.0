<?
$ar_kirpichi = array(20, 632);
$ar_litsevoy = array(57,170,21);
$ar_keram = array(110);
$ar_klinker = array(704);
$ar_stroitelny = array(146);
$ar_fasonniy = array(170,190);
$ar_shamotniy = array(209, 216, 248);
$ar_keramblocks = array(223, 633, 506);
$ar_keramblocks_peregorod = array(510);
$ar_keram_kamen = array(514);
$ar_gazoblocks = array(255, 259, 256, 258, 257);
$ar_metalcherep = array(18, 288, 600, 603, 598, 601, 599, 602);
$ar_proflist = array(22, 362, 390, 608, 605, 606, 607, 610, 614, 612, 613, 609, 611, 604);
$ar_sukhie_smesi = array(526, 527, 528, 529, 530, 635);
$ar_sofity = array(463, 775, 774, 749);
$ar_vodostok = array(780, 785,783,795,794,793,792,784,782,781);

if (in_array($arResult['ID'], $ar_vodostok)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/vodostochnye-sistemy.php';
}

if (in_array($arResult['ID'], $ar_kirpichi)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/kirpich.php';
}

if (in_array($arResult['ID'], $ar_litsevoy)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/litsevoy.php';
}

if (in_array($arResult['ID'], $ar_keram)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/keramicheskiy.php';
}

if (in_array($arResult['ID'], $ar_klinker)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/klinkernyy.php';
}

if (in_array($arResult['ID'], $ar_stroitelny)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/stroitelniy.php';
}

if (in_array($arResult['ID'], $ar_fasonniy)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/fasonniy.php';
}

if (in_array($arResult['ID'], $ar_shamotniy)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/shamotniy.php';
}

if (in_array($arResult['ID'], $ar_keramblocks_peregorod)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/keramblock_peregorod.php';
}

if (in_array($arResult['ID'], $ar_keramblocks)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/keramblocks.php';
}

if (in_array($arResult['ID'], $ar_keram_kamen)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/keram_kamen.php';
}

if (in_array($arResult['ID'], $ar_gazoblocks)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/gazobloki.php';
}

if (in_array($arResult['ID'], $ar_metalcherep)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/metallocherepica.php';
}

if (in_array($arResult['ID'], $ar_proflist)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/profnastil.php';
}

if (in_array($arResult['ID'], $ar_sukhie_smesi)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/sukhie_smesi.php';
}

if (in_array($arResult['ID'], $ar_sofity)) {
    include $_SERVER['DOCUMENT_ROOT'].'/local/templates/tulpar_store/includes/section_catalogs/sofity.php';
}
