<!doctype html>
<html>
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KCZSW4S');</script>
    <!-- End Google Tag Manager -->
    <?php  $APPLICATION->ShowHead(); ?>
	<?
    use Bitrix\Main\Page\Asset;
    // CSS
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/bootstrap.min.css');
	Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/slick-theme.min.css');
	Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/slick.min.css');
	Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/jquery.fancybox.min.css');
	Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/styles.css');
	Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/responsive.css');
	//Asset::getInstance()->addCss('');
    // JS
    CJSCore::Init(array("jquery3"));
    Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js');
	Asset::getInstance()->addJs('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
	Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js');
	Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js');
	Asset::getInstance()->addJs('https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js');

	Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/app.js');

    //STRING
	Asset::getInstance()->addString('<meta charset="utf-8">');
    Asset::getInstance()->addString("<link rel='shortcut icon' href='/local/favicon.ico' />");
    Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">');
    Asset::getInstance()->addString("<link href='/local/templates/tulpar_store/assets/css/libs/roboto-fonts.css' rel='stylesheet'>");
    ?>
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon.png">
	<title>Страница не найдена</title>

	<!-- Yandex.Metrika counter -->
	<script type="text/javascript" >
	    (function (d, w, c) {
	        (w[c] = w[c] || []).push(function() {
	            try {
	                w.yaCounter51395941 = new Ya.Metrika2({
	                    id:51395941,
	                    clickmap:true,
	                    trackLinks:true,
	                    accurateTrackBounce:true,
	                    webvisor:true
	                });
	            } catch(e) { }
	        });

	        var n = d.getElementsByTagName("script")[0],
	            s = d.createElement("script"),
	            f = function () { n.parentNode.insertBefore(s, n); };
	        s.type = "text/javascript";
	        s.async = true;
	        s.src = "https://mc.yandex.ru/metrika/tag.js";

	        if (w.opera == "[object Opera]") {
	            d.addEventListener("DOMContentLoaded", f, false);
	        } else { f(); }
	    })(document, window, "yandex_metrika_callbacks2");
	</script>
	<noscript><div><img src="https://mc.yandex.ru/watch/51395941" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KCZSW4S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?$APPLICATION->ShowPanel();?>

<?php
    session_start();
	$wish_count = sizeof($_SESSION['element_ids']);
	$cmp_count = sizeof($_SESSION['cmp_element_ids']);
	$cart_count = sizeof(json_decode($_COOKIE['cart'], true));


	if ($wish_count != 0) {
		$bookmarked = "(".$wish_count.")";
	} else {
		$bookmarked = "";
	}

	if ($cmp_count != 0) {
		$compared = "(".$cmp_count.")";
	} else {
		$compared = "";
	}


	if ($cart_count != 0) {
		$in_cart = "(".$cart_count.")";
	} else {
		$in_cart = "";
	}
?>

<header class="header">
  <div class="container p-sm-0">
	  <div class="row site-menu-top">
	  	<div class="column">
			<div class="site-menu">
				<div class="row mr-0 pb-lg-2 pb-xl-0">
					<div class="col-xl-1 col pr-lg-0">
						<div class="city"><span>Казань</span></div>
					</div>

					<div class="col-xl-8 col-lg-11 col-sm-auto pl-0 pr-lg-0 pl-lg-4">
						<nav class="menu-list">
							<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
								"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
									"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
									"DELAY" => "N",	// Откладывать выполнение шаблона меню
									"MAX_LEVEL" => "1",	// Уровень вложенности меню
									"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
										0 => "",
									),
									"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
									"MENU_CACHE_TYPE" => "N",	// Тип кеширования
									"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
									"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
									"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
								),
								false
							);?>
						</nav>
					</div>

					<div class="col-xl-3 cub d-sm-none d-xl-block">
						<div class="contact-us-btn">Связаться с нами</div>
						<ul class="nav-feedback" id="nav-feedback1">
							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#zamer-application" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" alt="">
									<span>Отправить заявку на расчет</span>
								</a>
							</li>

							<li class="nav-item callback-enquiry">
								<a data-fancybox data-src="#callback-enquiry" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" alt="">
									<span>Заказать звонок</span>
								</a>
							</li>

							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#hidden-content" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" alt="">
									<span>Стать поставщиком</span>
								</a>
							</li>

							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#hidden-content" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" alt="">
									<span>Оставить отзыв</span>
								</a>
							</li>
						</ul>
					</div>

				</div>
			</div>
	  	</div>
	  </div>
	  <div class="row site-menu-top mobile d-none">
	  	<div class="column">
			<div class="site-menu">
				<div class="close-btn"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/close.svg" alt=""></div>
				<div class="row mr-0 pb-lg-2 pb-xl-0">
					<div class="col-xl-1 col pr-lg-0">
						<div class="city"><span>Казань</span></div>
					</div>

					<div class="menu-title">Меню</div>

					<div class="col-xl-8 col-lg-11 col-sm-auto pl-0 pr-lg-0 pl-lg-4">
						<nav class="menu-list">
							<?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
								"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
									"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
									"DELAY" => "N",	// Откладывать выполнение шаблона меню
									"MAX_LEVEL" => "1",	// Уровень вложенности меню
									"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
										0 => "",
									),
									"MENU_CACHE_TIME" => "3600",	// Время кеширования (сек.)
									"MENU_CACHE_TYPE" => "N",	// Тип кеширования
									"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
									"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
									"USE_EXT" => "N",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
								),
								false
							);?>
						</nav>

						<nav class="menu-list">
							<ul class="list-unstyled list-inline mb-0">
								<li class="list-inline-item calculator-item">
									<a href="/calculator/">Онлайн калькулятор</a>
								</li>

								<!-- <li class="list-inline-item gallery-item">
									<a href="#">Галерея</a>
								</li> -->

								<li class="list-inline-item visualization-item">
									<a href="/3d/">3D визулизация</a>
								</li>

								<li class="list-inline-item brands-item">
									<a href="/proizvoditeli/">Производители</a>
								</li>
							</ul>
						</nav>
					</div>

					<div class="col-xl-3 cub d-sm-none d-xl-block">
						<div class="contact-us-btn">Связаться с нами</div>
						<ul class="nav-feedback" id="nav-feedback1">
							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#zamer-application" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" alt="">
									<span>Отправить заявку на расчет</span>
								</a>
							</li>

							<li class="nav-item callback-enquiry">
								<a data-fancybox data-src="#callback-enquiry" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" alt="">
									<span>Заказать звонок</span>
								</a>
							</li>

							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#hidden-content" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" alt="">
									<span>Стать поставщиком</span>
								</a>
							</li>

							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#hidden-content" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" alt="">
									<span>Оставить отзыв</span>
								</a>
							</li>
						</ul>
					</div>

				</div>
			</div>
	  	</div>
	  </div>
      <div class="row justify-content-between justify-content-sm-start mobile-top-row">
            <div class="col-xl-1 col-lg-1 col-auto logo pr-sm-2 pt-lg-5 pt-xl-0 d-sm-none d-xl-block">
            	<a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/logo_new.svg" width="87" alt=""></a>
            </div>

            <div class="col-8 d-sm-none pl-0 d-flex justify-content-end align-items-center pt-3 right">
                <div class="call-us-mob rounded mr-4"><a class="d-block h-100" href="tel:+78432163267"></a></div>
                <div class="cart-compare in-header d-flex pt-0">
                    <div class="compare mr-2"><a href="/personal/compare.php"></a></div>
                    <div class="bookmarked mr-2"><a href="/wishlist/"></a></div>
                    <div class="basket"><a href="/personal/cart.php"></a></div>
					<div class="menu-btn"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/burger-men.svg" alt=""></div>
                </div>
            </div>

            <div class="col-xl-11 col-lg-12 pr-0 d-sm-block d-none">


                <div class="header-info d-sm-none d-xl-flex">
                    <div class="info-item address">
                        <div class="info-name">Адрес</div>
                        <div class="info-text">г. Казань, ул. Даурская 12а, <br>этаж 3, офис 20.4</div>
                    </div>

                    <div class="info-item phone">
                        <div class="info-name">Телефон</div>
                        <div class="info-text">8 (843) 216 32 67</div>
                    </div>

                    <div class="info-item working-hours">
                        <div class="info-name">Режим работы</div>
                        <div class="info-text">Пн - Пт: 8:00 - 17:00 <br> Сб: 8:00 - 14:00</div>
                    </div>

                    <div class="info-item fn-btn calculator">
                        <a href="/calculator/">Онлайн <br> калькулятор</a>
                    </div>

                    <!-- <div class="info-item fn-btn gallery">
                        <a href="#">Галерея</a>
                    </div> -->

                    <div class="info-item fn-btn visualization">
                        <a href="/3d/">3D визуализация</a>
                    </div>

                    <div class="info-item fn-btn brands">
                        <a href="/proizvoditeli/">Производители</a>
                    </div>
                </div>

                <div class="header-info d-lg-flex d-xl-none mb-lg-3 mb-xl-0">
                    <div class="info-item logo-img">
                        <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/logo.svg" width="60" alt=""></a>
                    </div>

                    <div class="info-item address">
                        <div class="info-name">Адрес</div>
                        <div class="info-text">г. Казань, ул. Даурская 12а, <br>этаж 3, офис 20.4</div>
                    </div>

                    <div class="info-item phone d-sm-none d-md-block">
                        <div class="info-name">Телефон</div>
                        <div class="info-text">8 (843) 216 32 67</div>
                    </div>

                    <div class="info-item working-hours">
                        <div class="info-name">Режим работы</div>
                        <div class="info-text">Пн - Пт: 8:00 - 17:00 <br> Сб: 8:00 - 14:00</div>
                    </div>

					<div class="cub position-relative">
						<div class="contact-us-btn d-sm-none d-lg-block">Связаться с нами</div>
						<ul class="nav-feedback" id="nav-feedback1">
							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#hidden-content" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" alt="">
									<span>Отправить заявку на расчет</span>
								</a>
							</li>

							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#hidden-content" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" alt="">
									<span>Заказать звонок</span>
								</a>
							</li>

							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#hidden-content" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" alt="">
									<span>Стать поставщиком</span>
								</a>
							</li>

							<li class="nav-item na-raschet">
								<a data-fancybox data-src="#hidden-content" href="javascript:;">
									<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" alt="">
									<span>Оставить отзыв</span>
								</a>
							</li>
						</ul>
					</div>

                    <div class="call-us-mob rounded mr-4 d-sm-block d-lg-none"><a class="d-block h-100" href="tel:+78432163267"></a></div>
                </div>

                <div class="header-info d-lg-flex d-xl-none for-lg">
                    <div class="info-item fn-btn calculator">
                        <a href="/calculator/">Онлайн <br> калькулятор</a>
                    </div>

                    <!-- <div class="info-item fn-btn gallery">
                        <a href="#">Галерея</a>
                    </div> -->

                    <div class="info-item fn-btn visualization">
                        <a href="/3d/">3D визуализация</a>
                    </div>

                    <div class="info-item fn-btn brands">
                        <a href="/proizvoditeli/">Производители</a>
                    </div>

                    <div class="cart-compare in-header d-flex">
                        <div class="compare"><a href="/personal/compare.php"></a></div>
                        <div class="bookmarked"><a href="/wishlist/"></a></div>
                        <div class="basket"><a href="/personal/cart.php"></a></div>
                    </div>
                </div>
            </div>
      </div>
  </div>
 </header>

 <?/* Если мы не находимся на главной */?>
 <? if ($APPLICATION->GetCurPage(false) !== '/'): ?>
 <?    $isHome = "not-home";?>
 <? endif;?>

  <div id="m-nav">
  	<div class="m-nav_close"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/close.svg" alt=""></div>
	<div class="m-nav_back"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/back.svg" alt=""></div>
	<div class="m-nav_container">
		<div class="container__inner">
			<!--noindex-->
				<ul class="m-nav-mobile-list">
					<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"></li>

					<li class="m-nav-mobile-list__item icon_catalog c-sub-screen">
						<a href="/catalog/" class="m-nav-mobile-list__link has-icon has_sublist">Каталог товаров <span></span></a>
						<div class="c-sub-screen__sub">
							<ul class="m-nav-mobile-list">
								<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header"><span>Каталог</span></li>

								<li class="m-nav-mobile-list__item c-sub-screen">
									<a href="/catalog/kirpich/" class="m-nav-mobile-list__link has_sublist">Кирпич <span></span></a>
									<div class="c-sub-screen__sub">
										<ul class="m-nav-mobile-list">
											<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Кирпич</span></li>

                                            <li class="m-nav-mobile-list__item">
                                                <a href="/catalog/kirpich/" class="m-nav-mobile-list__link">Все кирпичи</a>
                                            </li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/oblitsovochnyy/" class="m-nav-mobile-list__link has_sublist">Облицовочный<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Облицовочный кирпич</span></li>
                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/oblitsovochnyy/" class="m-nav-mobile-list__link">Все облицовочные</a>
                                                        </li>
														<li class="m-nav-mobile-list__item group-name">По производителю</li>
														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-koshchakovskiy/" class="m-nav-mobile-list__link">Кощаковский</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-arskiy/" class="m-nav-mobile-list__link">Арский</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-kirovo-chepetskiy/" class="m-nav-mobile-list__link">Кирово-чепецкий</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-izhevskiy/" class="m-nav-mobile-list__link">Ижевский</a>
														</li>

														<li class="m-nav-mobile-list__item group-name">По цвету</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-krasnyy/" class="m-nav-mobile-list__link">красный</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-zheltyy/" class="m-nav-mobile-list__link">желтый</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-korichnevyy/" class="m-nav-mobile-list__link">коричневый</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-seryy/" class="m-nav-mobile-list__link">серый</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/oblitsovochniy-belyy/" class="m-nav-mobile-list__link">белый</a>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/keramicheskiy-kirpich/" class="m-nav-mobile-list__link has_sublist">Керамический<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Керамический кирпич</span></li>

                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/keramicheskiy-kirpich/" class="m-nav-mobile-list__link">Все керамические</a>
                                                        </li>

                                                        <li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/keramicheskiy-oblitsovochnyy/" class="m-nav-mobile-list__link has_sublist">Облицовочный<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Облицовочный кирпич</span></li>

                                                                    <li class="m-nav-mobile-list__item">
                                                                        <a href="/catalog/keramicheskiy-oblitsovochnyy/" class="m-nav-mobile-list__link">Все облицовочные</a>
                                                                    </li>

                                                                    <li class="m-nav-mobile-list__item group-name">По производителю</li>
																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-koshchakovskiy/" class="m-nav-mobile-list__link">Кощаковский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-arskiy/" class="m-nav-mobile-list__link">Арский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-kirovo-chepetskiy/" class="m-nav-mobile-list__link">Кирово-чепецкий</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-izhevskiy/" class="m-nav-mobile-list__link">Ижевский</a>
																	</li>

																	<li class="m-nav-mobile-list__item group-name">По цвету</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-krasnyy/" class="m-nav-mobile-list__link">красный</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-zheltyy/" class="m-nav-mobile-list__link">желтый</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-korichnevyy/" class="m-nav-mobile-list__link">коричневый</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-seryy/" class="m-nav-mobile-list__link">серый</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/keramicheskiy-belyy/" class="m-nav-mobile-list__link">белый</a>
																	</li>
																</ul>
															</div>
														</li>

														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/ryadovoy/" class="m-nav-mobile-list__link has_sublist">Рядовой<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Рядовой кирпич</span></li>

                                                                    <li class="m-nav-mobile-list__item">
                                                                        <a href="/catalog/ryadovoy/" class="m-nav-mobile-list__link">Все рядовые</a>
                                                                    </li>

																	<li class="m-nav-mobile-list__item group-name">По производителю</li>
																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-koshchakovskiy/" class="m-nav-mobile-list__link">Кощаковский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-votkinskiy/" class="m-nav-mobile-list__link">Воткинский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-chaykovskiy/" class="m-nav-mobile-list__link">Чайковский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-shelangovskiy/" class="m-nav-mobile-list__link">Шеланговский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-mamadyshskiy/" class="m-nav-mobile-list__link">Мамадышский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-kazanskiy/" class="m-nav-mobile-list__link">Казанский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-kurkachinskiy/" class="m-nav-mobile-list__link">Куркачинский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-ketra/" class="m-nav-mobile-list__link">Кетра</a>
																	</li>

																	<li class="m-nav-mobile-list__item group-name">По цвету</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ryadovoy-krasnyy/" class="m-nav-mobile-list__link">красный</a>
																	</li>
																</ul>
															</div>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/stroitelnyy-kirpich/" class="m-nav-mobile-list__link has_sublist">Строительный <span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Строительный кирпич</span></li>

                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/stroitelnyy-kirpich/" class="m-nav-mobile-list__link">Все строительные</a>
                                                        </li>

                                                        <li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/stroitelnyy-ryadovoy/" class="m-nav-mobile-list__link has_sublist">Рядовой<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Рядовой кирпич</span></li>

                                                                    <li class="m-nav-mobile-list__item">
                                                                        <a href="/catalog/stroitelnyy-ryadovoy/" class="m-nav-mobile-list__link">Все рядовые</a>
                                                                    </li>

                                                                    <li class="m-nav-mobile-list__item group-name">По производителю</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy_kirpich_koshchakovskiy/" class="m-nav-mobile-list__link">Кощаковский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-votkinskiy/" class="m-nav-mobile-list__link">Воткинский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-chaykovskiy/" class="m-nav-mobile-list__link">Чайковский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-shelangovskiy/" class="m-nav-mobile-list__link">Шеланговский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-mamadyshskiy/" class="m-nav-mobile-list__link">Мамадышский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-kazanskiy/" class="m-nav-mobile-list__link">Казанский</a>
																	</li>


																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-kurkachinskiy/" class="m-nav-mobile-list__link">Куркачинский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-ryadovoy-ketra/" class="m-nav-mobile-list__link">Кетра</a>
																	</li>
																</ul>
															</div>
														</li>
														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/stroitelnyy-tsokolnyy/" class="m-nav-mobile-list__link has_sublist">Цокольный<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Цокольный кирпич</span></li>
																	<li class="m-nav-mobile-list__item group-name">По производителю</li>

                                                                    <li class="m-nav-mobile-list__item">
                                                                        <a href="/catalog/stroitelnyy-tsokolnyy/" class="m-nav-mobile-list__link">Все цокольные</a>
                                                                    </li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-tsokolnyy-koshchakovskiy/" class="m-nav-mobile-list__link">Кощаковский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-tsokolnyy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-tsokolnyy-votkinskiy/" class="m-nav-mobile-list__link">Воткинский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-tsokolnyy-chaykovskiy/" class="m-nav-mobile-list__link">Чайковский</a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/stroitelnyy-tsokolnyy-kazanskiy/" class="m-nav-mobile-list__link">Казанский</a>
																	</li>
																</ul>
															</div>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/figurnyy-kirpich/" class="m-nav-mobile-list__link has_sublist">Фигурный <span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Фигурный кирпич</span></li>
														<li class="m-nav-mobile-list__item group-name">По производителю</li>
														<li class="m-nav-mobile-list__item">
															<a href="/catalog/figurnyy_kirpich_arskiy/" class="m-nav-mobile-list__link">Арский</a>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/shamotnyy_kirpich/" class="m-nav-mobile-list__link has_sublist">Шамотный <span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Шамотный кирпич</span></li>

                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/shamotnyy_kirpich/" class="m-nav-mobile-list__link">Все шамотные</a>
                                                        </li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/shb-5/" class="m-nav-mobile-list__link">ШБ-5</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/shb-8/" class="m-nav-mobile-list__link">ШБ-8</a>
														</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</li>

								<li class="m-nav-mobile-list__item c-sub-screen">
									<a href="/catalog/keramicheskie-bloki/" class="m-nav-mobile-list__link has_sublist">Керамические блоки <span></span></a>
									<div class="c-sub-screen__sub">
										<ul class="m-nav-mobile-list">
											<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Керамические блоки</span></li>

                                            <li class="m-nav-mobile-list__item">
                                                <a href="/catalog/keramicheskie-bloki/" class="m-nav-mobile-list__link">Все керамические блоки</a>
                                            </li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/krupnoformatnyy-keramicheskiy-blok/" class="m-nav-mobile-list__link has_sublist">Крупноформатный керамический блок<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Крупноформатный кер. блок</span></li>

                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/krupnoformatnyy-keramicheskiy-blok/" class="m-nav-mobile-list__link">Все крупноформатные блоки</a>
                                                        </li>

                                                        <li class="m-nav-mobile-list__item">
															<a href="/catalog/krupnoformatnyy-amstron-porikam/" class="m-nav-mobile-list__link">Амстрон (порикам)<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/krupnoformatnyy-ketra/" class="m-nav-mobile-list__link">Кетра<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/krupnoformatnyy-vinerberger-poroterm/" class="m-nav-mobile-list__link">Винербергер (поротерм)<span></span></a>
														</li>

													</ul>
												</div>
											</li>
											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/peregorodochnyy-keramicheskiy-blok/" class="m-nav-mobile-list__link has_sublist">Перегородочный керамический блок<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Перегородочный кер. блок</span></li>

                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/peregorodochnyy-keramicheskiy-blok/" class="m-nav-mobile-list__link">Все перегородочные блоки</a>
                                                        </li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/peregorodochnyy-amstron-porikam/" class="m-nav-mobile-list__link">Амстрон (порикам)<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/peregorodochnyy-ketra/" class="m-nav-mobile-list__link has_sublist">Кетра<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/peregorodochnyy-vinerberger-poroterm/" class="m-nav-mobile-list__link">Винербергер (поротерм)<span></span></a>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/keramicheskiy-kamen/" class="m-nav-mobile-list__link has_sublist">Керамический камень<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Керамический камень</span></li>

                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/keramicheskiy-kamen/" class="m-nav-mobile-list__link">Все керамические камни</a>
                                                        </li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/keramicheskiy-kamen-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/keramicheskiy-kamen-mamadyshskiy/" class="m-nav-mobile-list__link">Мамадышский</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/keramicheskiy-kamen-ketra/" class="m-nav-mobile-list__link">Кетра</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/keramicheskiy-kamen-vinerberger-poroterm/" class="m-nav-mobile-list__link">Винербергер (поротерм)</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/keramicheskiy-kamen-shelangovskiy/" class="m-nav-mobile-list__link">Шеланговский</a>
														</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</li>

								<li class="m-nav-mobile-list__item c-sub-screen">
									<a href="/catalog/gazobetonnye-bloki-m/" class="m-nav-mobile-list__link has_sublist">Газобетонные блоки <span></span></a>
									<div class="c-sub-screen__sub">
										<ul class="m-nav-mobile-list">
											<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Газобетонные блоки</span></li>

                                            <li class="m-nav-mobile-list__item">
                                                <a href="/catalog/gazobetonnye-bloki-m/" class="m-nav-mobile-list__link">Все газобетонные блоки</a>
                                            </li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/krupnoformatnye-gazobetonnye-bloki/" class="m-nav-mobile-list__link has_sublist">Крупноформатные газобетонные блоки<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Крупноформатные блоки</span></li>

                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/krupnoformatnye-gazobetonnye-bloki/" class="m-nav-mobile-list__link">Все крупноформатные блоки</a>
                                                        </li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/krupnoformatnye-bikton/" class="m-nav-mobile-list__link">Биктон</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/krupnoformatnye-kzssm/" class="m-nav-mobile-list__link">КЗССМ</a>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/peregorodochnye-gazobetonnye-bloki/" class="m-nav-mobile-list__link has_sublist">Перегородочные газобетонные блоки<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Перегородочные блоки</span></li>

                                                        <li class="m-nav-mobile-list__item">
                                                            <a href="/catalog/peregorodochnye-gazobetonnye-bloki/" class="m-nav-mobile-list__link">Все перегородочные блоки</a>
                                                        </li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/peregorodochnye-bikton/" class="m-nav-mobile-list__link">Биктон</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/peregorodochnye-kzssm/" class="m-nav-mobile-list__link">КЗССМ</a>
														</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>

								<li class="m-nav-mobile-list__item c-sub-screen">
									<a href="/catalog/sukhie-stroitelnye-smesi/" class="m-nav-mobile-list__link has_sublist">Сухие строительные смеси<span></span></a>
									<div class="c-sub-screen__sub">
										<ul class="m-nav-mobile-list">
											<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Сухие строительные смеси</span></li>

                                            <li class="m-nav-mobile-list__item">
                                                <a href="/catalog/sukhie-stroitelnye-smesi/" class="m-nav-mobile-list__link">Все строительные смеси</a>
                                            </li>

											<li class="m-nav-mobile-list__item">
												<a href="/catalog/kladochnyy-rastvor/" class="m-nav-mobile-list__link">Кладочный раствор</a>
											</li>

											<li class="m-nav-mobile-list__item">
												<a href="/catalog/tsvetnoy-rastvor/" class="m-nav-mobile-list__link">Цветной раствор</a>
											</li>

											<li class="m-nav-mobile-list__item">
												<a href="/catalog/teploizolyatsionnyy-rastvor/" class="m-nav-mobile-list__link">Теплоизоляционный раствор</a>
											</li>
										</ul>
									</div>
								</li>

								<li class="m-nav-mobile-list__item c-sub-screen">
									<a href="/catalog/stroitelnaya-setka/" class="m-nav-mobile-list__link has_sublist">Строительная сетка<span></span></a>
									<div class="c-sub-screen__sub">
										<ul class="m-nav-mobile-list">
											<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Строительная сетка</span></li>

                                            <li class="m-nav-mobile-list__item">
                                                <a href="/catalog/sukhie-stroitelnye-smesi/" class="m-nav-mobile-list__link">Все строительные сетки</a>
                                            </li>

											<li class="m-nav-mobile-list__item">
												<a href="/catalog/bazaltovaya-setka/" class="m-nav-mobile-list__link">Базальтовая сетка</a>
											</li>

											<li class="m-nav-mobile-list__item">
												<a href="/catalog/stekloplastikovaya-setka/" class="m-nav-mobile-list__link">Стеклопластиковая сетка</a>
											</li>
										</ul>
									</div>
								</li>

								<li class="m-nav-mobile-list__item c-sub-screen">
									<a href="/catalog/krovelnye-materialy/" class="m-nav-mobile-list__link has_sublist">Кровля<span></span></a>
									<div class="c-sub-screen__sub">
										<ul class="m-nav-mobile-list">
											<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Кровля</span></li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/metallocherepitsa/" class="m-nav-mobile-list__link has_sublist">Металлочерепица<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Металлочерепица</span></li>
														<li class="m-nav-mobile-list__item">
															<a href="/catalog/metallocherepitsa-monterey/" class="m-nav-mobile-list__link">Монтерей<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/metallocherepitsa-supermonterey/" class="m-nav-mobile-list__link">Супермонтерей<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/metallocherepitsa-maksi/" class="m-nav-mobile-list__link">Макси<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/metallocherepitsa-monterossa/" class="m-nav-mobile-list__link">Монтеросса<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/metallocherepitsa-tromantana/" class="m-nav-mobile-list__link">Трамонтана<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/metallocherepitsa-montekristo/" class="m-nav-mobile-list__link">Монтекристо<span></span></a>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/profnastil/" class="m-nav-mobile-list__link has_sublist">Профнастил<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Профнастил</span></li>
														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-s-8/" class="m-nav-mobile-list__link">С-8<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-mp-10/" class="m-nav-mobile-list__link">МП-10<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-mp-18/" class="m-nav-mobile-list__link">МП-18<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-mp-20/" class="m-nav-mobile-list__link">МП-20<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-%D1%81-21/" class="m-nav-mobile-list__link">С-21<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-ns-35/" class="m-nav-mobile-list__link">НС-35<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-mp-35/" class="m-nav-mobile-list__link">МП-35<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-s-44/" class="m-nav-mobile-list__link">С-44<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-n-60/" class="m-nav-mobile-list__link">Н-60<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-n-75/" class="m-nav-mobile-list__link">Н-75<span></span></a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/profnastil-n-114/" class="m-nav-mobile-list__link">Н-114<span></span></a>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item">
												<a href="/catalog/sofity/" class="m-nav-mobile-list__link">Софиты<span></span></a>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/dobornye-elementy/" class="m-nav-mobile-list__link has_sublist">Доборные элементы<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Доборные элементы</span></li>
														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/germetiziruyushchie-materialy/" class="m-nav-mobile-list__link has_sublist">Герметизирующие материалы<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Герметизирующие материалы</span></li>
																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/soedinitelnaya-lenta/" class="m-nav-mobile-list__link">Соединительная лента<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/silikon/" class="m-nav-mobile-list__link">Силикон<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/germetiziruyushchaya-lenta/" class="m-nav-mobile-list__link">Герметизирующая лента<span></span></a>
																	</li>
																</ul>
															</div>
														</li>

														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/flyugery-i-ukazateli-vetra/" class="m-nav-mobile-list__link has_sublist">Флюгеры и указатели ветра<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Флюгеры</span></li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/flyugery/" class="m-nav-mobile-list__link">Флюгеры<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/ukazateli-vetra/" class="m-nav-mobile-list__link">Указатели ветра<span></span></a>
																	</li>
																</ul>
															</div>
														</li>

														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/stroitelnaya-khimiya/" class="m-nav-mobile-list__link has_sublist">Строительная химия<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Строительная химия</span></li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/remontnaya-emal/" class="m-nav-mobile-list__link">Ремонтная эмаль<span></span></a>
																	</li>
																</ul>
															</div>
														</li>

														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/standartnye-elementy-otdelki/" class="m-nav-mobile-list__link has_sublist">Стандартные элементы отделки<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Стандартные элементы отделки</span></li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/planka/" class="m-nav-mobile-list__link">Планка<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/zaglushka/" class="m-nav-mobile-list__link">Заглушка<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/uglovoy-element/" class="m-nav-mobile-list__link">Угловой элемент<span></span></a>
																	</li>
																</ul>
															</div>
														</li>

														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/uplotniteli/" class="m-nav-mobile-list__link has_sublist">Уплотнители<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Уплотнители</span></li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/uplotnitel/" class="m-nav-mobile-list__link">Уплотнитель<span></span></a>
																	</li>
																</ul>
															</div>
														</li>

														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/krepezhnye-izdeliya/" class="m-nav-mobile-list__link has_sublist">Крепежные изделия<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Крепежные изделия</span></li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/samorezy/" class="m-nav-mobile-list__link">Саморезы<span></span></a>
																	</li>
																</ul>
															</div>
														</li>

														<li class="m-nav-mobile-list__item c-sub-screen">
															<a href="/catalog/krovelnaya-ventilyatsiya/" class="m-nav-mobile-list__link has_sublist">Кровельная вентиляция<span></span></a>
															<div class="c-sub-screen__sub">
																<ul class="m-nav-mobile-list">
																	<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Кровельная вентиляция</span></li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/antennyy-vykhod/" class="m-nav-mobile-list__link">Антенный выход<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/manzheta-krovelnaya/" class="m-nav-mobile-list__link">Манжета кровельная<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/vykhod-ventilyatsii/" class="m-nav-mobile-list__link">Выход вентиляции<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/vykhod-vytyazhki/" class="m-nav-mobile-list__link">Выход вытяжки<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/vykhod-universalnyy/" class="m-nav-mobile-list__link">Выход универсальный<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/gofrirovannaya-truba/" class="m-nav-mobile-list__link">Гофрированная труба<span></span></a>
																	</li>

																	<li class="m-nav-mobile-list__item">
																		<a href="/catalog/truba-izolirovannaya/" class="m-nav-mobile-list__link">Труба изолированная<span></span></a>
																	</li>
																</ul>
															</div>
														</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</li>

								<li class="m-nav-mobile-list__item c-sub-screen">
									<a href="/catalog/elementy-bezopasnosti-krovli/" class="m-nav-mobile-list__link has_sublist">Элементы безопасности кровли<span></span></a>
									<div class="c-sub-screen__sub">
										<ul class="m-nav-mobile-list">
											<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Элементы безопасности кровли</span></li>
											<li class="m-nav-mobile-list__item">
												<a href="/catalog/snegozaderzhateli/" class="m-nav-mobile-list__link">Снегозадержатели</a>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/krovelno-stenovaya-lestnitsa/" class="m-nav-mobile-list__link has_sublist">Кровельно-стеновая лестница<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Кровельно-стеновая лестница</span></li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/krovelnaya-lestnitsa/" class="m-nav-mobile-list__link">Кровельная лестница</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/kronshteyn/" class="m-nav-mobile-list__link">Кронштейн</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/poruchen/" class="m-nav-mobile-list__link">Поручень</a>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item">
												<a href="/catalog/krovelnoe-ograzhdenie/" class="m-nav-mobile-list__link">Кровельное ограждение</a>
											</li>

											<li class="m-nav-mobile-list__item">
												<a href="/catalog/perekhodnoy-mostik/" class="m-nav-mobile-list__link">Переходной мостик</a>
											</li>
										</ul>
									</div>
								</li>

								<!-- <li class="m-nav-mobile-list__item c-sub-screen">
									<a href="/catalog/lestnitsy/" class="m-nav-mobile-list__link has_sublist">Лестницы<span></span></a>
									<div class="c-sub-screen__sub">
										<ul class="m-nav-mobile-list">
											<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Лестницы</span></li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/mezhetazhnye/" class="m-nav-mobile-list__link has_sublist">Межэтажные лестницы<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Межэтажные лестницы</span></li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/lestnitsy-mezhetazhnye-g-obraznye/" class="m-nav-mobile-list__link">Г-образные</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/lestnitsy-mezhetazhnye-p-obraznye/" class="m-nav-mobile-list__link">П-образные</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/lestnitsy-mezhetazhnye-vintovye/" class="m-nav-mobile-list__link">Винтовые</a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/lestnitsy-mezhetazhnye-pryamye/" class="m-nav-mobile-list__link">Прямые</a>
														</li>
													</ul>
												</div>
											</li>

											<li class="m-nav-mobile-list__item c-sub-screen">
												<a href="/catalog/cherdachnye/" class="m-nav-mobile-list__link has_sublist">Чердачные лестницы<span></span></a>
												<div class="c-sub-screen__sub">
													<ul class="m-nav-mobile-list">
														<li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Чердачные лестницы</span></li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/cherdachnye-metallicheskie/" class="m-nav-mobile-list__link">Металлические </a>
														</li>

														<li class="m-nav-mobile-list__item">
															<a href="/catalog/cherdachnye/" class="m-nav-mobile-list__link">Деревянные</a>
														</li>
													</ul>
												</div>
											</li>
										</ul>
									</div>
								</li> -->
							</ul>
						</div>
					</li>

					<li class="m-nav-mobile-list__item icon_delivery">
						<a href="/dostavka/" class="m-nav-mobile-list__link has-icon">Доставка</a>
					</li>

                    <li class="m-nav-mobile-list__item icon_payment">
                        <a href="/oplata/" class="m-nav-mobile-list__link has-icon">Оплата</a>
                    </li>

					<li class="m-nav-mobile-list__item icon_about">
						<a href="/about/" class="m-nav-mobile-list__link has-icon">О компании</a>
					</li>

					<li class="m-nav-mobile-list__item icon_blog">
						<a href="/articles/" class="m-nav-mobile-list__link has-icon">Блог</a>
					</li>

					<li class="m-nav-mobile-list__item icon_contact">
						<a href="/contacts/" class="m-nav-mobile-list__link has-icon">Контакты</a>
					</li>

					<li class="m-nav-mobile-list__item icon_calculator">
						<a href="/calculator/" class="m-nav-mobile-list__link has-icon">Онлайн калькулятор</a>
					</li>

					<li class="m-nav-mobile-list__item icon_visualization">
						<a href="/3d/" class="m-nav-mobile-list__link has-icon">3D визуализация</a>
					</li>

					<li class="m-nav-mobile-list__item phone-num">
						<a href="tel:+78432163267" class="m-nav-mobile-list__link has-icon">8 (843) 216-32-67</a>
					</li>
				</ul>
			<!--/noindex-->
		</div>
	</div>
  </div>

  <div class="search-and-cart d-sm-none border-top border-bottom mb-0 <?=$isHome?>">
		<div class="search">
            <?$APPLICATION->IncludeComponent(
                  "bitrix:search.title",
                  "top-search",
                  array(
                      "CATEGORY_0" => array(
                          0 => "iblock_catalog",
                      ),
                      "CATEGORY_0_TITLE" => "Товары",
                      "CATEGORY_0_iblock_catalog" => array(
                          0 => "4",
                      ),
                      "CHECK_DATES" => "Y",
                      "CONTAINER_ID" => "title-search-mob",
                      "INPUT_ID" => "title-search-input-mob",
                      "NUM_CATEGORIES" => "1",
                      "ORDER" => "rank",
                      "PAGE" => "#SITE_DIR#search/index.php",
                      "SHOW_INPUT" => "Y",
                      "SHOW_OTHERS" => "N",
                      "TOP_COUNT" => "5",
                      "USE_LANGUAGE_GUESS" => "Y",
                      "COMPONENT_TEMPLATE" => "top-search"
                  ),
                  false
              );?>
		</div>
	</div>



	<div class="catalog-and-banner">
	<div class="container p-sm-0 ">
	  <div class="row">
		  <div class="col-xl-3 col-lg-4 col-md-5 side-catalog  pl-0 ">
			  <div class="side-catalog-wrapper not-home">
				  <div class="catalog-name rounded-top not-home">Каталог товаров</div>
				  <ul class="catalog-list list-unstyled m-0 d-none d-lg-block not-home">
					  <li class="kirpichi">
						  <a href="/catalog/kirpich/">Кирпич</a>
						  <div class="catalog-submenu">
							  <div class="submenu-section">Кирпич</div>
							  <ul class="list-unstyled">
                                  <li><a href="/catalog/kirpich/">Все кирпичи</a></li>
								  <li>
									  <a href="/catalog/oblitsovochnyy/">Облицовочный</a>
									  <div class="catalog-submenu">
										  <div class="submenu-section">По производителю</div>
										  <ul class="list-unstyled">
											  <li><a href="/catalog/oblitsovochniy-koshchakovskiy/">Кощаковский</a></li>
											  <li><a href="/catalog/oblitsovochniy-arskiy/">Арский</a></li>
											  <li><a href="/catalog/oblitsovochniy-kirovo-chepetskiy/">Кирово-Чепецкий</a></li>
											  <li><a href="/catalog/oblitsovochniy-alekseevskiy/">Алексеевский</a></li>
											  <li><a href="/catalog/oblitsovochniy-klyuchishchenskiy/">Ключищенский</a></li>
											  <li><a href="/catalog/oblitsovochniy-izhevskiy/">Ижевский</a></li>
										  </ul>

										  <div class="submenu-separator mb-3"></div>
										  <div class="submenu-section">По цвету</div>
										  <ul class="list-unstyled">
											  <li><a href="/catalog/oblitsovochniy-krasnyy/">красный</a></li>
											  <li><a href="/catalog/oblitsovochniy-zheltyy/">желтый</a></li>
											  <li><a href="/catalog/oblitsovochniy-korichnevyy/">коричневый</a></li>
											  <li><a href="/catalog/oblitsovochniy-seryy/">серый</a></li>
											  <li><a href="/catalog/oblitsovochniy-belyy/">белый</a></li>
										  </ul>
									  </div>
								  </li>
								  <li>
									  <a href="/catalog/keramicheskiy-kirpich/">Керамический</a>
									  <div class="catalog-submenu">
									  	<div class="submenu-section">Керамический</div>
										<ul class="list-unstyled">
											<li>
												<a href="/catalog/keramicheskiy-oblitsovochnyy/">Облицовочный</a>
												<div class="catalog-submenu">
													<div class="submenu-section">По производителю</div>
													<ul class="list-unstyled">
														<li><a href="/catalog/keramicheskiy-koshchakovskiy/">Кощаковский</a></li>
														<li><a href="/catalog/keramicheskiy-arskiy/">Арский</a></li>
														<li><a href="/catalog/keramicheskiy-kirovo-chepetskiy/">Кирово-Чепецкий</a></li>
														<li><a href="/catalog/keramicheskiy-alekseevskiy/">Алексеевский</a></li>
														<li><a href="/catalog/keramicheskiy-klyuchishchenskiy/">Ключищенский</a></li>
														<li><a href="/catalog/keramicheskiy-izhevskiy/">Ижевский</a></li>
													</ul>

													<div class="submenu-separator mb-3"></div>
													<div class="submenu-section">По цвету</div>
													<ul class="list-unstyled">
														<li><a href="/catalog/keramicheskiy-krasnyy/">красный</a></li>
														<li><a href="/catalog/keramicheskiy-zheltyy/">желтый</a></li>
														<li><a href="/catalog/keramicheskiy-korichnevyy/">коричневый</a></li>
														<li><a href="/catalog/keramicheskiy-seryy/">серый</a></li>
														<li><a href="/catalog/keramicheskiy-belyy/">белый</a></li>
													</ul>
												</div>
											</li>
											<li>
												<a href="/catalog/ryadovoy/">Рядовой</a>
												<div class="catalog-submenu">
													<div class="submenu-section">По производителю</div>
													<ul class="list-unstyled">
														<li><a href="/catalog/ryadovoy-alekseevskiy/">Алексеевский</a></li>
														<li><a href="/catalog/ryadovoy-klyuchishchenskiy/">Ключищенский</a></li>
														<li><a href="/catalog/ryadovoy-votkinskiy/">Воткинский</a></li>
														<li><a href="/catalog/ryadovoy-chaykovskiy/">Чайковский</a></li>
														<li><a href="/catalog/ryadovoy-koshchakovskiy/">Кощаковский</a></li>
														<li><a href="/catalog/ryadovoy-shelangovskiy/">Шеланговский</a></li>
														<li><a href="/catalog/ryadovoy-mamadyshskiy/">Мамадышский</a></li>
														<li><a href="/catalog/ryadovoy-kazanskiy/">Казанский</a></li>
														<li><a href="/catalog/ryadovoy-kurkachinskiy/">Куркачинский</a></li>
														<li><a href="/catalog/ryadovoy-ketra/">Кетра</a></li>
													</ul>

													<div class="submenu-separator mb-3"></div>
													<div class="submenu-section">По цвету</div>
													<ul class="list-unstyled">
														<li><a href="/catalog/ryadovoy-krasnyy/">красный</a></li>
													</ul>
												</div>
											</li>
										</ul>
									  </div>
								  </li>
								  <li>
									  <a href="/catalog/stroitelnyy-kirpich/">Строительный</a>
									  <div class="catalog-submenu">
									  	<div class="submenu-section">Строительный</div>
										<ul class="list-unstyled">
											<li>
												<a href="/catalog/stroitelnyy-ryadovoy/">Рядовой</a>
												<div class="catalog-submenu">
													<div class="submenu-section">По производителю</div>
													<ul class="list-unstyled">
														<li><a href="/catalog/stroitelnyy-ryadovoy-alekseevskiy/">Алексеевский</a></li>
														<li><a href="/catalog/stroitelnyy-ryadovoy-klyuchishchenskiy/">Ключищенский</a></li>
														<li><a href="/catalog/ryadovoy-ketra/">Воткинский</a></li>
														<li><a href="/catalog/stroitelnyy-ryadovoy-chaykovskiy/">Чайковский</a></li>
														<li><a href="/catalog/stroitelnyy_kirpich_koshchakovskiy/">Кощаковский</a></li>
														<li><a href="/catalog/stroitelnyy-ryadovoy-shelangovskiy/">Шеланговский</a></li>
														<li><a href="/catalog/stroitelnyy-ryadovoy-mamadyshskiy/">Мамадышский</a></li>
														<li><a href="/catalog/stroitelnyy-ryadovoy-kazanskiy/">Казанский</a></li>
														<li><a href="/catalog/stroitelnyy-ryadovoy-kurkachinskiy/">Куркачинский</a></li>
														<li><a href="/catalog/stroitelnyy-ryadovoy-ketra/">Кетра</a></li>
													</ul>
												</div>
											</li>
											<li>
												<a href="/catalog/stroitelnyy-tsokolnyy/">Цокольный</a>
												<div class="catalog-submenu">
													<div class="submenu-section">По производителю</div>
													<ul class="list-unstyled">
														<li><a href="/catalog/stroitelnyy-tsokolnyy-koshchakovskiy/">Кощаковский</a></li>
														<li><a href="/catalog/stroitelnyy-tsokolnyy-alekseevskiy/">Алексеевский</a></li>
														<li><a href="/catalog/stroitelnyy-tsokolnyy-votkinskiy/">Воткинский</a></li>
														<li><a href="/catalog/stroitelnyy-tsokolnyy-chaykovskiy/">Чайковский</a></li>
														<li><a href="/catalog/stroitelnyy-tsokolnyy-kazanskiy/">Казанский</a></li>
													</ul>
												</div>
											</li>
										</ul>
									  </div>
								  </li>
								  <li>
									  <a href="/catalog/figurnyy-kirpich/">Фигурный</a>
									  <div class="catalog-submenu">
										  <div class="submenu-section">По производителю</div>
										  <ul class="list-unstyled">
											  <li><a href="/catalog/figurnyy_kirpich_arskiy/">Арский</a></li>
										  </ul>
									  </div>
								  </li>
								  <li>
									  <a href="/catalog/shamotnyy_kirpich/">Шамотный</a>
									  <div class="catalog-submenu">
										  <div class="submenu-section">Шамотный</div>
										  <ul class="list-unstyled">
											  <li><a href="/catalog/shb-5/">ШБ-5</a></li>
											  <li><a href="/catalog/shb-8/">ШБ-8</a></li>
										  </ul>
									  </div>
								  </li>
							  </ul>
						  </div>
					  </li>

					  <li class="keram_blocki">
						  <a href="/catalog/keramicheskie-bloki/">Керамические блоки</a>
						  <div class="catalog-submenu">
							  <div class="submenu-section">Керамические блоки</div>
							  <ul class="list-unstyled">
							  	<li>
									<a href="/catalog/krupnoformatnyy-keramicheskiy-blok/">Крупноформатный керамический блок</a>
									<div class="catalog-submenu">
										<div class="submenu-section">По производителю</div>
										<ul class="list-unstyled">
											<li>
												<a href="/catalog/krupnoformatnyy-amstron-porikam/">Амстрон (Порикам)</a>
											</li>
											<li>
												<a href="/catalog/krupnoformatnyy-ketra/">Кетра</a>
											</li>
											<li>
												<a href="/catalog/krupnoformatnyy-vinerberger-poroterm/">Винербергер (Поротерм)</a>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<a href="/catalog/peregorodochnyy-keramicheskiy-blok/">Перегородочный керамический блок</a>
									<div class="catalog-submenu">
										<div class="submenu-section">По производителю</div>
										<ul class="list-unstyled">
											<li>
												<a href="/catalog/peregorodochnyy-amstron-porikam/">Амстрон (Порикам)</a>
											</li>
											<li>
												<a href="/catalog/peregorodochnyy-ketra/">Кетра</a>
											</li>
											<li>
												<a href="/catalog/peregorodochnyy-vinerberger-poroterm/">Винербергер (Поротерм)</a>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<a href="/catalog/keramicheskiy-kamen/">Керамический камень</a>
									<div class="catalog-submenu">
										<div class="submenu-section">По производителю</div>
										<ul class="list-unstyled">
											<li><a href="/catalog/keramicheskiy-kamen-klyuchishchenskiy/">Ключищенский</a></li>
											<li><a href="/catalog/keramicheskiy-kamen-mamadyshskiy/">Мамадышский</a></li>
											<li><a href="/catalog/keramicheskiy-kamen-ketra/">Кетра</a></li>
											<li><a href="/catalog/keramicheskiy-kamen-vinerberger-poroterm/">Винербергер (Поротерм)</a></li>
											<li><a href="/catalog/keramicheskiy-kamen-shelangovskiy/">Шеланговский</a></li>
										</ul>
									</div>
								</li>
							  </ul>
						  </div>
					  </li>

					  <li class="gazobloki">
					  	<a href="/catalog/gazobetonnye-bloki-m/">Газобетонные блоки</a>
						<div class="catalog-submenu">
							<div class="submenu-section">Газобетонные блоки</div>
							<ul class="list-unstyled">
								<li>
									<a href="/catalog/krupnoformatnye-gazobetonnye-bloki/">Крупноформатные газобетонные блоки</a>
									<div class="catalog-submenu">
										<div class="submenu-section">Крупноформатные блоки</div>
										<ul class="list-unstyled">
                                            <li><a href="/catalog/krupnoformatnye-bikton/">Биктон</a></li>
											<li><a href="/catalog/krupnoformatnye-kzssm/">КЗССМ</a></li>
										</ul>
									</div>
								</li>
								<li>
									<a href="/catalog/peregorodochnye-gazobetonnye-bloki/">Перегородочные газобетонные блоки</a>
									<div class="catalog-submenu">
										<div class="submenu-section">Перегородочные блоки</div>
										<ul class="list-unstyled">
											<li><a href="/catalog/peregorodochnye-bikton/">Биктон</a></li>
											<li><a href="/catalog/peregorodochnye-kzssm/">КЗССМ</a></li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					  </li>

					  <li class="suh_stroy_smesi">
						  <a href="/catalog/sukhie-stroitelnye-smesi/">Сухие строительные смеси</a>
						  <div class="catalog-submenu">
						  	<div class="submenu-section">Сухие строительные смеси</div>
							<ul class="list-unstyled">
								<li>
									<a href="/catalog/kladochnyy-rastvor/">Кладочный раствор </a>
								</li>

								<li>
									<a href="/catalog/tsvetnoy-rastvor/">Цветной раствор </a>
								</li>

								<li>
									<a href="/catalog/teploizolyatsionnyy-rastvor/">Теплоизоляционный раствор </a>
								</li>

								<li>
									<a href="/catalog/kley-dlya-gazobetona/">Клей для газобетона</a>
								</li>
							</ul>
						  </div>
					  </li>

					  <li class="stroy_setka">
						  <a href="/catalog/stroitelnaya-setka/">Строительная сетка</a>
						  <div class="catalog-submenu">
						  	<div class="submenu-section">Строительная сетка</div>
							<ul class="list-unstyled">
								<li>
									<a href="/catalog/bazaltovaya-setka/">Базальтовая сетка</a>
								</li>

								<li>
									<a href="/catalog/stekloplastikovaya-setka/">Стеклопластиковая сетка</a>
								</li>
							</ul>
						  </div>
					  </li>

					  <li class="krovlya">
						  <a href="/catalog/krovelnye-materialy/">Кровля</a>
						  <div class="catalog-submenu">
						  <div class="submenu-section">Кровля</div>
						  <ul class="list-unstyled">
							  <li>
								  <a href="/catalog/metallocherepitsa/">Металлочерепица</a>
								  <div class="catalog-submenu">
									  <div class="submenu-section">Металлочерепица</div>
									  <ul class="list-unstyled">
										  <li>
											  <a href="/catalog/metallocherepitsa-monterey/">Монтерей</a>
										  </li>
										  <li>
											  <a href="/catalog/metallocherepitsa-supermonterey/">Супермонтерей</a>
										  </li>

										  <li>
											  <a href="/catalog/metallocherepitsa-maksi/">Макси</a>
										  </li>

										  <li>
											  <a href="/catalog/metallocherepitsa-tromantana/">Тромантана</a>
										  </li>

										  <li>
											  <a href="/catalog/metallocherepitsa-montekristo/">Монтекристо</a>
										  </li>
									  </ul>
								  </div>
							  </li>

							  <li>
								  <a href="/catalog/profnastil/">Профнастил</a>
								  <div class="catalog-submenu">
									  <div class="submenu-section">Профнастил</div>
									  <ul class="list-unstyled">
										  <li>
											  <a href="/catalog/profnastil-s-8/">С-8</a>
										  </li>
										  <li>
											  <a href="/catalog/profnastil-mp-10/">МП-10</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-mp-18/">МП-18</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-mp-20/">МП-20</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-с-21/">С-21</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-ns-35/">НС-35</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-mp-35/">МП-35</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-s-44/">С-44</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-n-60/">Н-60</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-n-75/">Н-75</a>
										  </li>

										  <li>
											  <a href="/catalog/profnastil-n-114/">Н-114</a>
										  </li>
									  </ul>
								  </div>
							  </li>

							  <li>
								  <a href="/catalog/sofity/">Софиты</a>
							  </li>
							  <li>
								  <a href="/catalog/dobornye-elementy/">Доборные элементы</a>
								  <div class="catalog-submenu">
									  <div class="submenu-section">Доборные элементы</div>
									  <ul class="list-unstyled">
										<li>
											<a href="/catalog/germetiziruyushchie-materialy/">Герметизирующие материалы</a>
											<div class="catalog-submenu">
												<div class="submenu-section"></div>
												<ul class="list-unstyled">
													<li>
														<a href="/catalog/soedinitelnaya-lenta/">Соединительная лента</a>
													</li>

													<li>
														<a href="/catalog/silikon/">Силикон</a>
													</li>

													<li>
														<a href="/catalog/germetiziruyushchaya-lenta/">Герметизирующая лента</a>
													</li>
												</ul>
											</div>
										</li>

										<li>
											<a href="/catalog/flyugery-i-ukazateli-vetra/">Флюгеры и указатели ветра</a>
											<div class="catalog-submenu">
												<div class="submenu-section">Флюгеры и указатели ветра</div>
												<ul class="list-unstyled">
													<li>
														<a href="/catalog/flyugery/">Флюгеры</a>
													</li>

													<li>
														<a href="/catalog/ukazateli-vetra/">Указатели ветра</a>
													</li>
												</ul>
											</div>
										</li>

										<li>
											<a href="/catalog/stroitelnaya-khimiya/">Строительная химия</a>
											<div class="catalog-submenu">
												<div class="submenu-section">Строительная химия</div>
												<ul class="list-unstyled">
													<li>
														<a href="/catalog/remontnaya-emal/">Ремонтная эмаль</a>
													</li>
												</ul>
											</div>
										</li>

										<li>
											<a href="/catalog/standartnye-elementy-otdelki/">Стандартные элементы отделки</a>
											<div class="catalog-submenu">
												<div class="submenu-section">Стандартные элементы отделки</div>
												<ul class="list-unstyled">
													<li>
														<a href="/catalog/planka/">Планка</a>
													</li>

													<li>
														<a href="/catalog/zaglushka/">Заглушка</a>
													</li>

													<li>
														<a href="/catalog/uglovoy-element/">Угловой элемент</a>
													</li>
												</ul>
											</div>
										</li>

										<li>
											<a href="/catalog/uplotniteli/">Уплотнители</a>
											<div class="catalog-submenu">
												<div class="submenu-section">Уплотнители</div>
												<ul class="list-unstyled">
													<li>
														<a href="/catalog/uplotnitel/">Уплотнитель</a>
													</li>
												</ul>
											</div>
										</li>

										<li>
											<a href="/catalog/krepezhnye-izdeliya/">Крепежные изделия</a>
											<div class="catalog-submenu">
												<div class="submenu-section">Крепежные изделия</div>
												<ul class="list-unstyled">
													<li>
														<a href="/catalog/samorezy/">Саморезы</a>
													</li>
												</ul>
											</div>
										</li>

										<li>
											<a href="/catalog/krovelnaya-ventilyatsiya/">Кровельная вентиляция</a>
											<div class="catalog-submenu">
												<div class="submenu-section">Кровельная вентиляция</div>
												<ul class="list-unstyled">
													<li>
														<a href="/catalog/antennyy-vykhod/">Антенный выход</a>
													</li>

													<li>
														<a href="/catalog/manzheta-krovelnaya/">Манжета кровельная</a>
													</li>

													<li>
														<a href="/catalog/vykhod-ventilyatsii/">Выход вентиляции</a>
													</li>

													<li>
														<a href="/catalog/vykhod-vytyazhki/">Выход вытяжки</a>
													</li>

													<li>
														<a href="/catalog/vykhod-universalnyy/">Выход универсальный</a>
													</li>

													<li>
														<a href="/catalog/gofrirovannaya-truba/">Гофрированная труба</a>
													</li>

													<li>
														<a href="/catalog/truba-izolirovannaya/">Труба изолированная</a>
													</li>
												</ul>
											</div>
										</li>
									  </ul>
								  </div>
							  </li>
						  </ul>
						</div>
					  </li>

					  <!-- <li class="vodostok_system">
						  <a href="#">Водосточные системы</a>
					  </li> -->

					  <li class="elem_bezop_krovli">
						  <a href="/catalog/elementy-bezopasnosti-krovli/">Элементы безопасности кровли</a>
						  <div class="catalog-submenu">
							  <div class="submenu-section">Элементы безопасности кровли</div>
							  <ul class="list-unstyled">
								  <li>
									  <a href="/catalog/snegozaderzhateli/">Снегозадержатели</a>
								  </li>

								  <li>
									  <a href="/catalog/krovelno-stenovaya-lestnitsa/">Кровельно-стеновая лестница</a>
									  <div class="catalog-submenu">
										  <div class="submenu-section">Кровельно-стеновая лестница</div>
										  <ul class="list-unstyled">
											  <li>
											  	<a href="/catalog/krovelnaya-lestnitsa/">Кровельная лестница</a>
											  </li>

											  <li>
											  	<a href="/catalog/kronshteyn/">Кронштейн</a>
											  </li>

											  <li>
											  	<a href="/catalog/poruchen/">Поручень</a>
											  </li>
										  </ul>
									  </div>
								  </li>

								  <li>
									  <a href="/catalog/krovelnoe-ograzhdenie/">Кровельное ограждение</a>
								  </li>

								  <li>
									  <a href="/catalog/perekhodnoy-mostik/">Переходной мостик</a>
								  </li>
							  </ul>
						  </div>
					  </li>

					  <!-- <li class="lestnitsy">
						  <a href="/catalog/lestnitsy/">Лестницы</a>
						  <div class="catalog-submenu">
							  	<div class="submenu-section">Лестницы</div>
								<ul class="list-unstyled">
									<li>
										<a href="/catalog/mezhetazhnye/">Межэтажные лестницы</a>
										<div class="catalog-submenu">
											<div class="submenu-section">Межэтажные лестницы</div>
											<ul class="list-unstyled">
												<li>
													<a href="/catalog/lestnitsy-mezhetazhnye-g-obraznye/">Г-образные</a>
													<a href="/catalog/lestnitsy-mezhetazhnye-p-obraznye/">П-образные</a>
													<a href="/catalog/lestnitsy-mezhetazhnye-vintovye/">Винтовые</a>
													<a href="/catalog/lestnitsy-mezhetazhnye-pryamye/">Прямые</a>
												</li>
											</ul>
										</div>
									</li>

									<li>
										<a href="/catalog/cherdachnye/">Чердачные лестницы</a>
										<div class="catalog-submenu">
											<div class="submenu-section">Чердачные лестницы</div>
											<ul class="list-unstyled">
												<li>
													<a href="/catalog/cherdachnye-metallicheskie/">Металлические </a>
													<a href="/catalog/cherdachnye-derevyannye/">Деревянные</a>
												</li>
											</ul>
										</div>
									</li>
								</ul>
						  </div>
					  </li> -->
				  </ul>
			  </div>
		  </div>

		  <div class="col-xl-9 col-lg-8 p-0 col-md-7 d-none d-md-block d-lg-none">
			  <div class="search-and-cart d-none d-sm-flex">
				  <div class="search">
					  <!-- <form class="search-form">
						  <input type="text" name="" value="" class="rounded" placeholder="Введите что Вы хотите найти">
						  <input type="submit" name="" value="">
					  </form> -->
                      <?$APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            "top-search",
                            array(
                                "CATEGORY_0" => array(
                                    0 => "iblock_catalog",
                                ),
                                "CATEGORY_0_TITLE" => "Товары",
                                "CATEGORY_0_iblock_catalog" => array(
                                    0 => "4",
                                ),
                                "CHECK_DATES" => "Y",
                                "CONTAINER_ID" => "title-search-md",
                                "INPUT_ID" => "title-search-input-md",
                                "NUM_CATEGORIES" => "1",
                                "ORDER" => "rank",
                                "PAGE" => "#SITE_DIR#search/index.php",
                                "SHOW_INPUT" => "Y",
                                "SHOW_OTHERS" => "N",
                                "TOP_COUNT" => "5",
                                "USE_LANGUAGE_GUESS" => "Y",
                                "COMPONENT_TEMPLATE" => "top-search"
                            ),
                            false
                        );?>
				  </div>
			  </div>
		  </div>

		  <div class="col-xl-9 col-lg-8 p-0">
			  <div class="search-and-cart d-none d-lg-flex">
				  <div class="search">
					  <?$APPLICATION->IncludeComponent(
							"bitrix:search.title",
							"top-search",
							array(
								"CATEGORY_0" => array(
									0 => "iblock_catalog",
								),
								"CATEGORY_0_TITLE" => "Товары",
								"CATEGORY_0_iblock_catalog" => array(
									0 => "4",
								),
								"CHECK_DATES" => "Y",
								"CONTAINER_ID" => "title-search",
								"INPUT_ID" => "title-search-input",
								"NUM_CATEGORIES" => "1",
								"ORDER" => "rank",
								"PAGE" => "#SITE_DIR#search/index.php",
								"SHOW_INPUT" => "Y",
								"SHOW_OTHERS" => "N",
								"TOP_COUNT" => "5",
								"USE_LANGUAGE_GUESS" => "Y",
								"COMPONENT_TEMPLATE" => "top-search"
							),
							false
						);?>
				  </div>
				  <div class="cart-compare d-none d-xl-flex">
					  <div class="compare"><a href="/personal/compare.php">Сравнение <span id="compare_count"><?=$compared?></span></a></div>
					  <div class="bookmarked"><a href="/wishlist/">Закладки <span id="wishcount"><?=$bookmarked?></span></a></div>
					  <div class="basket"><a href="/personal/cart.php"><span id="cart_count"><?=$in_cart?></span></a></div>
				  </div>
			  </div>
		  </div>
	  </div>

	  <div class="row">
	  	<div class="col p-0">
			<?/* Если мы находимся на главной */?>
			<? if ($APPLICATION->GetCurPage(false) === '/'): ?>

				<?$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"banner-slider",
	array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "N",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array(
			0 => "DETAIL_PICTURE",
			1 => "",
		),
		"FILTER_NAME" => "",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "5",
		"IBLOCK_TYPE" => "dynamic_content",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"INCLUDE_SUBSECTIONS" => "N",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "8",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => ".default",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array(
			0 => "",
			1 => "ATT_PRODUCT_ON_BANNER",
			2 => "",
		),
		"SET_BROWSER_TITLE" => "N",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "N",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N",
		"COMPONENT_TEMPLATE" => "banner-slider",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
			<? endif; ?>
	  	</div>
	  </div>

	</div>
</div>

<?/* Если мы не находимся на главной */?>
<? if ($APPLICATION->GetCurPage(false) !== '/'): ?>

<? endif;?>
