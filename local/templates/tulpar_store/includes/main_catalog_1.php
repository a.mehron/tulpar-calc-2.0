<div class="section new-homepage-catalog">
	<div class="container">
	  <div class="row title-and-all justify-content-between align-items-center mb-4">
	      <div class="section-title text-white">Каталог</div>
	  </div>

	  <div class="row">
		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-success"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/kirpich/">Кирпич</a>

					  <ul class="list-unstyled">
						  <li class="section-title-sm">По пустотности</li>
						  <li><a href="/catalog/polnotelyy/">Полнотелый</a></li>
						  <li class="mb-3"><a href="/catalog/pustotelyy/">Пустотелый</a></li>

						  <li class="section-title-sm">По марке прочности</li>
						  <li><a href="/catalog/m-100/">M 100</a></li>
                          <li><a href="/catalog/m-100-200-po-marke/">M 100-200</a></li>
                          <li><a href="/catalog/m-125/">M 125</a></li>
						  <li><a href="/catalog/m-125-175/">M 125-175</a></li>
						  <li><a href="/catalog/m-125-200/">M 125-200</a></li>
                          <li><a href="/catalog/m-150/">М 150</a></li>
						  <li><a href="/catalog/m-150-200-po-marke/">M 150-200</a></li>
						  <li><a href="/catalog/m-175/">М 175</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_m-200/">M 200</a></li>
                          <li><a href="/catalog/m-250-po-marke/">М 250</a></li>
						  <li class="mb-3"><a href="/catalog/m-300-400/">M 300-400</a></li>

						  <li class="section-title-sm">По размеру</li>
						  <li><a href="/catalog/evroformat-0-7-nf/">Евроформат (0,7 НФ)</a></li>
						  <li><a href="/catalog/odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
						  <li><a href="/catalog/polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>
						  <li class="mb-3"><a href="/catalog/dvoynoy-2-1-nf/">Двойной (2,1 НФ)</a></li>

						  <li class="section-title-sm">По цвету</li>
						  <li><a href="/catalog/kirpich-bavarskaya-kladka/">Баварская кладка</a></li>
						  <li><a href="/catalog/belyy/">Белый</a></li>
						  <li><a href="/catalog/zheltyy-/">Желтый</a></li>
						  <li><a href="/catalog/korichnevyy/">Коричневый</a></li>
						  <li><a href="/catalog/krasnyy/">Красный</a></li>
						  <li><a href="/catalog/zelenyy/">Зеленый</a></li>
				          <li><a href="/catalog/oranzhevyy/">Оранжевый</a></li>
						  <li><a href="/catalog/chernyy/">Черный</a></li>
						  <li><a href="/catalog/kirpich-flesh-obzhig/">Флеш-обжиг</a></li>
						  <li><a href="/catalog/rozovyy/">Розовый</a></li>
						  <li><a href="/catalog/fioletovyy/">Фиолетовый</a></li>
						  <li class="mb-3"><a href="/catalog/seryy/">Серый</a></li>

							<li class="section-title-sm">По поверхности</li>
							<li><a href="/catalog/kirpich-vizantiya/">Византия</a></li>
							<li><a href="/catalog/kirpich-barkhat/">Бархат</a></li>
							<li><a href="/catalog/kirpich-galich/">Галич</a></li>
							<li><a href="/catalog/kirpich-gladkiy/">Гладкий</a></li>
							<li><a href="/catalog/kirpich-dikiy-kamen/">Дикий камень</a></li>
							<li><a href="/catalog/kirpich-kora-dereva/">Кора дерева</a></li>
							<li><a href="/catalog/kirpich-krokodilovaya-kozha/">Крокодиловая кожа</a></li>
							<li><a href="/catalog/kirpich-loft/">Лофт</a></li>
							<li><a href="/catalog/kirpich-rvanyy-kamen/">Рваный камень</a></li>
							<li><a href="/catalog/kirpich-rustik/">Рустик</a></li>
							<li><a href="/catalog/kirpich-ruchnaya-formovka/">Ручная формовка</a></li>
							<li><a href="/catalog/kirpich-sakhara/">Сахара</a></li>
							<li><a href="/catalog/kirpich-terra/">Терра</a></li>
							<li><a href="/catalog/kirpich-antik/">Антик</a></li>
							<li><a href="/catalog/kirpich-rust/">Руст</a></li>
							<li><a href="/catalog/kirpich-rustik-kora-dereva/">Рустик кора дерева</a></li>
							<li><a href="/catalog/kirpich-skala/">Скала</a></li>
							<li><a href="/catalog/kirpich-relefnyy/">Рельефный</a></li>
							<li class="mb-3"><a href="/catalog/kirpich-galich-s-peskom/">Галич с песком</a></li>
							
				          <li class="section-title-sm">По материалу</li>
						  <li><a href="/catalog/klinkernyy/">Клинкерный</a></li>
                          <li><a href="/catalog/silikatnyy/">Силикатный</a></li>
						   <li class="mb-3"><a href="/catalog/keramicheskiy-kirpich/">Керамический</a></li>

						  <li class="section-title-sm">По производителю</li>
						  <li><a href="/catalog/alekseevskiy/">Алексеевский</a></li>
						  <li><a href="/catalog/arskiy/">Арский</a></li>
						  <li><a href="/catalog/votkinskiy/">Воткинский</a></li>
						  <li><a href="/catalog/izhevskiy/">Ижевский</a></li>
						  <li><a href="/catalog/kirovo-chepetskiy/">Кирово-Чепецкий</a></li>
						  <li><a href="/catalog/klyuchishchenskiy/">Ключищенский</a></li>
						  <li><a href="/catalog/koshchakovskiy/">Кощаковский</a></li>
						  <li><a href="/catalog/kurkachinskiy/">Куркачинский</a></li>
						  <li><a href="/catalog/mamadyshskiy/">Мамадышский</a></li>
						  <li><a href="/catalog/chaykovskiy/">Чайковский</a></li>
						  <li><a href="/catalog/shelangovskiy/">Шеланговский</a></li>
						  <li><a href="/catalog/kazanskiy/">Казанский</a></li>
						  <li><a href="/catalog/cheboksarskiy/">Чебоксарский</a></li>
						  <li><a href="/catalog/bakalinskiy-zavod/">Бакалинский</a></li>
						  <li><a href="/catalog/tuymazinskiy-zavod/">Туймазинский</a></li>
						  <li><a href="/catalog/chekmagushskiy-zavod/">Чекмагушевский</a></li>
						  <li><a href="/catalog/saranskiy/">Саранский</a></li>
						  <li><a href="/catalog/chelninskiy/">Челнинский</a></li>
						  <li><a href="/catalog/izhevskiy-izkm/">Ижевский (ИЗКМ)</a></li>
						  <li><a href="/catalog/belebeevskiy/">Белебеевский</a></li>
						  <li><a href="/catalog/ketra/">Кетра</a></li>
						  <li><a href="/catalog/mstera/">MSTERA</a></li>
						  <li><a href="/catalog/zsk-chelny/">ЗСК Челны</a></li>
						  <li><a href="/catalog/dyurtyulinskiy/">Дюртюлинский</a></li>
						  <li><a href="/catalog/inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
						  <li><a href="/catalog/meakir/">Меакир</a></li>
						  <li><a href="/catalog/yadrinskiy/">Ядринский</a></li>
						  <li><a href="/catalog/revdinskiy/">Ревдинский</a></li>
					  </ul>
				  </div>
			  </div>
		  </div>

		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-orange"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/keramicheskiy-kirpich/">Керамический кирпич</a>

					  <ul class="list-unstyled">
						 <li class="section-title-sm">По виду</li>
							 <li><a href="/catalog/keramicheskiy-oblitsovochnyy/">Облицовочный</a></li>
							 <li class="mb-3"><a href="/catalog/ryadovoy/">Рядовой</a></li>

						 <li class="section-title-sm">По пустотности</li>
						 <li><a href="/catalog/keram_kirpich_polnotelyy/">Полнотелый</a></li>
						 <li class="mb-3"><a href="/catalog/keram_kirpich_pustotelyy/">Пустотелый</a></li>

						 <li class="section-title-sm">По марке прочности</li>
						 <li><a href="/catalog/keram_kirpich_m-100/">М 100</a></li>
						 <li><a href="/catalog/keram_kirpich_m-125/">М 125</a></li>
						 <li><a href="/catalog/keram_kirpich_m-150/">М 150</a></li>
						 <li><a href="/catalog/keram_kirpich_m-175/">М 175</a></li>
						 <li class="mb-3"><a href="/catalog/keram_kirpich_m-200/">М 200</a></li>

						 <li class="section-title-sm">По размеру</li>
						 <li><a href="/catalog/keram_kirpich_evroformat-0-7-nf/">Евроформат (0,7 НФ)</a></li>
						 <li><a href="/catalog/keram_kirpich_odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
						 <li><a href="/catalog/keram_kirpich_polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>
						 <li class="mb-3"><a href="/catalog/keram_kirpich_dvoynoy-2-1-nf/">Двойной (2,1 НФ)</a></li>

						 <li class="section-title-sm">По цвету</li>
						 <li><a href="/catalog/keram_kirpich_zheltyy/">Желтый</a></li>
						 <li><a href="/catalog/keram_kirpich_korichnevyy/">Коричневый</a></li>
						 <li><a href="/catalog/keram_kirpich_krasnyy/">Красный</a></li>
						 <li class="mb-3"><a href="/catalog/keram_kirpich_seryy/">Серый</a></li>

						 <li class="section-title-sm">По поверхности</li>
						 <li><a href="/catalog/keramicheskiy-barkhat/">Бархат</a></li>
						 <li><a href="/catalog/keramicheskiy-galich/">Галич</a></li>
						 <li><a href="/catalog/keramicheskiy-gladkiy/">Гладкий</a></li>
						 <li><a href="/catalog/keramicheskiy-dikiy-kamen/">Дикий камень</a></li>
						 <li><a href="/catalog/keramicheskiy-kora-dereva/">Кора дерева</a></li>
						 <li><a href="/catalog/keramicheskiy-krokodilovaya-kozha/">Крокодиловая кожа</a></li>
						 <li><a href="/catalog/keramicheskiy-loft/">Лофт</a></li>
						 <li><a href="/catalog/keramicheskiy-rvanyy-kamen/">Рваный камень</a></li>
						 <li><a href="/catalog/keramicheskiy-rustik/">Рустик</a></li>
						 <li><a href="/catalog/keramicheskiy-ruchnaya-formovka/">Ручная формовка</a></li>
						 <li><a href="/catalog/keramicheskiy-sakhara/">Сахара</a></li>
						 <li><a href="/catalog/keramicheskiy-rustik-kora-dereva/">Рустик кора дерева </a></li>
						 <li><a href="/catalog/keramicheskiy-antik/">Антик</a></li>
						 <li><a href="/catalog/keramicheskiy-vizantiya/">Византия</a></li>
						 <li><a href="/catalog/keramicheskiy-rust/">Руст</a></li>
						 <li><a href="/catalog/keramicheskiy-skala/">Скала</a></li>
						 <li><a href="/catalog/keramicheskiy-galich-s-peskom/">Галич с песком</a></li>
						 <li class="mb-3"><a href="/catalog/keramicheskiy-terra/">Терра</a></li>

						 <li class="section-title-sm">По производителю</li>
						 <li><a href="/catalog/keram_kirpich_alekseevskiy/">Алексеевский</a></li>
						 <li><a href="/catalog/keram_kirpich_arskiy/">Арский</a></li>
						 <li><a href="/catalog/keram_kirpich_votkinskiy/">Воткинский</a></li>
						 <li><a href="/catalog/keram_kirpich_izhevskiy/">Ижевский</a></li>
						 <li><a href="/catalog/keram_kirpich_kazanskiy/">Казанский</a></li>
						 <li><a href="/catalog/keram_kirpich_kirovo-chepetskiy/">Кирово-Чепецкий</a></li>
						 <li><a href="/catalog/keram_kirpich_klyuchishchenskiy/">Ключищенский</a></li>
						 <li><a href="/catalog/keram_kirpich_koshchakovskiy/">Кощаковский</a></li>
						 <li><a href="/catalog/keram_kirpich_kurkachinskiy/">Куркачинский</a></li>
						 <li><a href="/catalog/keram_kirpich_mamadyshskiy/">Мамадышский</a></li>
						 <li><a href="/catalog/keram_kirpich_chaykovskiy/">Чайковский</a></li>
						 <li><a href="/catalog/keram_kirpich_shelangovskiy/">Шеланговский</a></li>
						 <li><a href="/catalog/cheboksarskiy/">Чебоксарский</a></li>
						 <li><a href="/catalog/keram_kirpich_bakalinskiy-1/">Бакалинский</a></li>
						 <li><a href="/catalog/keram_kirpich_tuymazinskiy-1/">Туймазинский</a></li>
						 <li><a href="/catalog/keram_kirpich_chekmagushskiy-1/">Чекмагушевский</a></li>
						 <li><a href="/catalog/keram_kirpich-saranskiy/">Саранский</a></li>
						 <li><a href="/catalog/keram_kirpich_belebeevskiy/">Белебеевский</a></li>
						 <li><a href="/catalog/keram_kirpich_kerma/">Керма</a></li>
						 <li><a href="/catalog/keram_kirpich_ketra/">Кетра</a></li>
						 <li><a href="/catalog/keram_kirpich_chelninskiy/">Челнинский</a></li>
						 <li><a href="/catalog/keram_kirpich_izhevskiy-izkm/">Ижевский (ИЗКМ) </a></li>
						 <li><a href="/catalog/keram_kirpich_mstera/">MSTERA</a></li>
						 <li><a href="/catalog/keram_kirpich_dyurtyulinskiy/">Дюртюлинский</a></li>
						  <li><a href="/catalog/keram_kirpich_inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
						  <li><a href="/catalog/keram_kirpich_meakir/">Меакир</a></li>
						  <li><a href="/catalog/keram_kirpich_yadrinskiy/">Ядринский</a></li>
						  <li><a href="/catalog/keram_kirpich_revdinskiy/">Ревдинский</a></li>
					  </ul>
				  </div>
			  </div>
		  </div>

		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-purple"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/oblitsovochnyy/">Облицовочный кирпич</a>

					  <ul class="list-unstyled border-bottom pb-3">
						  <li class="section-title-sm">По размеру</li>
						  <li><a href="/catalog/oblitsovochniy-evroformat-0-7-nf/">Евроформат (0,7 НФ)</a></li>
						  <li><a href="/catalog/oblitsovochniy_odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
						  <li class="mb-3"><a href="/catalog/oblitsovochniy-polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>
						  

						  <li class="section-title-sm">По производителю</li>
						  <li><a href="/catalog/oblitsovochniy-alekseevskiy/">Алексеевский</a></li>
						  <li><a href="/catalog/oblitsovochniy-arskiy/">Арский</a></li>
						  <li><a href="/catalog/oblitsovochniy-izhevskiy/">Ижевский</a></li>
						  <li><a href="/catalog/oblitsovochniy-izhevskiy-izkm/">Ижевский (ИЗКМ)</a></li>
						  <li><a href="/catalog/oblitsovochniy-kirovo-chepetskiy/">Кирово-Чепецкий</a></li>
						  <li><a href="/catalog/oblitsovochniy-klyuchishchenskiy/">Ключищенский</a></li>
						  <li><a href="/catalog/oblitsovochniy-chelninskiy/">Челнинский</a></li>
						  <li><a href="/catalog/oblitsovochniy-belebeevskiy/">Белебеевский</a></li>
						  <li><a href="/catalog/oblitsovochniy-kerma/">Керма</a></li>
						  <li><a href="/catalog/oblitsovochniy-saranskiy/">Саранский</a></li>
						  <li><a href="/catalog/oblitsovochniy-zsk-chelny/">ЗСК Челны</a></li>
							<li><a href="/catalog/oblitsovochniy-meakir/">Меакир</a></li>
							<li><a href="/catalog/oblitsovochniy-revdinskiy/">Ревдинский</a></li>
						  <!--<li><a href="/catalog/oblitsovochniy-mstera/">MSTERA</a></li>-->
						  <li class="mb-3"><a href="/catalog/oblitsovochniy-koshchakovskiy/">Кощаковский</a></li>


						  <li class="section-title-sm">По цвету</li>
						  <li><a href="/catalog/oblitsovochniy-zheltyy/">Желтый</a></li>
						  <li><a href="/catalog/oblitsovochniy-korichnevyy/">Коричневый</a></li>
						  <li><a href="/catalog/oblitsovochniy-krasnyy/">Красный</a></li>
						  <li><a href="/catalog/oblitsovochniy-rozovyy/">Розовый</a></li>
						  <li><a href="/catalog/oblitsovochniy-fioletovyy/">Фиолетовый</a></li>
						  <li><a href="/catalog/oblitsovochniy-oranzhevyy/">Оранжевый</a></li>
						  <li class="mb-3"><a href="/catalog/oblitsovochniy-seryy/">Серый</a></li>

							<li class="section-title-sm">По поверхности</li>
							<li><a href="/catalog/oblictovochniy-bakhat/">Бархат</a></li>
							<li><a href="/catalog/oblictovochniy-galich/">Галич</a></li>
							<li><a href="/catalog/oblictovochniy-gladkiy/">Гладкий</a></li>
							<li><a href="/catalog/oblictovochniy-dikiy-kamen/">Дикий камень</a></li>
							<li><a href="/catalog/oblictovochniy-kora-dereva/">Кора дерева</a></li>
							<li><a href="/catalog/oblictovochniy-krokodilovaya-kozha/">Крокодиловая кожа</a></li>
							<li><a href="/catalog/oblictovochniy-loft/">Лофт</a></li>
							<li><a href="/catalog/oblictovochniy-rvanyy-kamen/">Рваный камень</a></li>
							<li><a href="/catalog/oblictovochniy-rustik/">Рустик</a></li>
							<li><a href="/catalog/oblictovochniy-ruchnaya-formovka/">Ручная формовка</a></li>
							<li><a href="/catalog/oblictovochniy-sakhara/">Сахара</a></li>
							<li><a href="/catalog/oblitsovochnyy-rustik-kora-dereva/">Рустик кора дерева </a></li>
							<li><a href="/catalog/oblitsovochniy-antik/">Антик</a></li>
							<li><a href="/catalog/oblitsovochnyy-vizantiya/">Византия</a></li>
							<li><a href="/catalog/oblitsovochnyy-rust/">Руст</a></li>
							<li><a href="/catalog/oblitsovochnyy-skala/">Скала</a></li>
							<li><a href="/catalog/oblictovochniy-relefnyy/">Рельефный</a></li>
							<li><a href="/catalog/oblitsovochnyy-galich-s-peskom/">Галич с песком</a></li>
							<li class="mb-3"><a href="/catalog/oblictovochniy-terra/">Терра</a></li>
					  </ul>

					  <a class="catalog-title" href="/catalog/fasonnyy/">Фасонный кирпич</a>

					  <ul class="list-unstyled mb-0">
						  <li class="section-title-sm">По размеру</li>
						  <li><a href="/catalog/fasonnyy_odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
						  <li class="mb-3"><a href="/catalog/fasonnyy_polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>

						  <li class="section-title-sm">По форме</li>
						  <li><a href="/catalog/fasonnyy_vognutyy/">Вогнутый</a></li>
						  <li class="mb-3"><a href="/catalog/fasonnyy_vypuklyy/">Выпуклый</a></li>

						  <li class="section-title-sm">По производителю</li>
						  <li class="mb-3"><a href="/catalog/fasonnyy_arskiy/">Арский</a></li>

						  <li class="section-title-sm">По цвету</li>
						  <li><a href="/catalog/fasonnyy_korichnevyy/">Коричневый</a></li>
						  <li><a href="/catalog/fasonnyy_krasnyy/">Красный</a></li>
					  </ul>
				  </div>
			  </div>
		  </div>

		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-yellow"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/oblitsovochnyy/">Строительный кирпич</a>

					  <ul class="list-unstyled border-bottom pb-3">
						  <li class="section-title-sm">По марке прочности</li>
						  <li><a href="/catalog/stroitelnyy_kirpich_m-100/">М 100</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_m-100-200/">М 100-200</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_m-125/">М 125</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_m-150/">М 150</a></li>
                          <li><a href="/catalog/stroitelnyy_kirpich_m-150-200/">М 150-200</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_m-175/">М 175</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_m-200/">М 200</a></li>
                          <li class="mb-3"><a href="/catalog/stroitelnyy_kirpich_m-250/">М 250</a></li>

						  <li class="section-title-sm">По размеру</li>
						  <li><a href="/catalog/stroitelnyy_kirpich_odinarnyy-1-nf/">Одинарный (1 НФ)</a></li>
						  <li class="mb-3"><a href="/catalog/stroitelnyy_kirpich_polutornyy-1-4-nf/">Полуторный (1,4 НФ)</a></li>

						  <li class="section-title-sm">По производителю</li>
						  <li><a href="/catalog/stroitelnyy_kirpich_alekseevskiy/">Алексеевский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_votkinskiy/">Воткинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_kazanskiy/">Казанский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_ketra/">Кетра</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_klyuchishchenskiy/">Ключищенский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_kurkachinskiy/">Куркачинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_mamadyshskiy/">Мамадышский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_chaykovskiy/">Чайковский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_cheboksarskiy/">Чебоксарский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_bakalinskiy-1/">Бакалинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_tuymazinskiy-1/">Туймазинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_chekmagushskiy-1/">Чекмагушевский</a></li>
						  <li><a href="/catalog/stroitelniy-kirpich-kamastroyindustriya/">Челнинский</a></li>
						 	<li><a href="/catalog/stroitelnyy_kirpich_dyurtyulinskiy/">Дюртюлинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_meakir/">Меакир</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_mstera/">MSTERA</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_zsk-chelny/">ЗСК Челны</a></li>
						  <li><a href="/catalog/stroitelniy-kirpich-revdinskiy/">Ревдинский</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_yadrinskiy/">Ядринский</a></li>
						  <li class="mb-3"><a href="/catalog/stroitelnyy_kirpich_shelangovskiy/">Шеланговский</a></li>


						  <li class="section-title-sm">По цвету</li>
						  <li><a href="/catalog/stroitelnyy_kirpich_belyy/">Белый</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_krasnyy/">Красный</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_zheltyy-1/">Желтый</a></li>
						  <li><a href="/catalog/stroitelnyy_kirpich_seryy-1/">Серый</a></li>
					  </ul>

					  <a class="catalog-title" href="/catalog/ogneupornyy-kirpich/">Огнеупорный кирпич</a>

					  <ul class="list-unstyled mb-0">
						  <li class="section-title-sm">По размеру</li>
						  <li><a href="/catalog/shamotnyy_kirpich_shb-5-230-114-65/">ШБ-5 (230*114*65)</a></li>
						  <li><a href="/catalog/shamotnyy_kirpich_shb-8-250-124-65/">ШБ-8 (250*124*65)</a></li>
					  </ul>
				  </div>
			  </div>
		  </div>
	  </div>
<div class="row">
		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-pink"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/keramicheskie-bloki/">КЕРАМИЧЕСКИЕ БЛОКИ</a>

					  <ul class="list-unstyled pb-3">
						 <li class="section-title-sm">По формату</li>
						 <li><a href="/catalog/keramicheskie_bloki_2-1-nf/">2,1 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_3-6-nf/">3,6 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_4-5-nf/">4,5 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_4-6-nf/">4,6 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_5-4nf/">5,4 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_5-5nf/">5,5 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_5-7-nf/">5,7 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_6-2nf/">6,2 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_6-7nf/">6,7 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_6-9-nf/">6,9 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_7nf/">7 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_7-2nf/">7,2 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_7-3-nf/">7,3 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_7-4nf/">7,4 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_8-5-nf/">8,5 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_8-6-nf/">8,6 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_9-nf/">9 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_10-5nf/">10,5 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_10-7-nf/">10,7 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_11-09-nf/">11,09 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_11-2-nf/">11,2 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_12-4nf/">12,4 НФ</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_12-8-nf/">12,8 НФ</a></li>
						 <li class="mb-3"><a href="/catalog/keramicheskie_bloki_14-3-nf/">14,3 НФ</a></li>

						 <li class="section-title-sm">По названию</li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-2/">Блок 2</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-8/">Блок 8</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-10/">Блок 10</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-12/">Блок 12</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-20/">Блок 20</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-25/">Блок 25</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-30/">Блок 30</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-38/">Блок 38</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-39-8/">Блок 39,8</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_blok-44/">Блок 44</a></li>
						 <li class="mb-3"><a href="/catalog/keramicheskie_bloki_blok-51/">Блок 51</a></li>

						 <li class="section-title-sm">По производителю</li>
						 <li><a href="/catalog/keramicheskie_bloki_amstron-porikam/">Амстрон (Порикам)</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_vinerberger-poroterm/">Винербергер (Поротерм)</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_ketra/">Кетра</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_klyuchishchenskiy/">Ключищенский</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_mamadyshskiy/">Мамадышский</a></li>
						 <li><a href="/catalog/bloki-kamastroyindustriya/">Челнинский</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_shelangovskiy/">Шеланговский</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_arskiy/">Арский</a></li>
						 <li><a href="/catalog/keramicheskie_bloki_kerakam/">Керакам</a></li>
						 <li><a href="/catalog/keramicheskiy-kamen-mstera/">MSTERA</a></li>

						  <li><a href="/catalog/keramicheskie_bloki_inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
						  <li><a href="/catalog/keramicheskie_bloki_meakir/">Меакир</a></li>
						  <li><a href="/catalog/bloki-revdinskiy/">Ревдинский</a></li>
						  <li><a href="/catalog/keramicheskie_bloki_yadrinskiy/">Ядринский</a></li>
					 </ul>
				  </div>
			  </div>
		  </div>

		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-greenlight"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/gazobetonnye-bloki-m/">Газобетонные блоки</a>

					  <ul class="list-unstyled pb-3">
						<li class="section-title-sm">По марке плотности</li>
						<li><a href="/catalog/gazobeton-d-300/">D 300</a></li>
						<li><a href="/catalog/gazobeton-d-350/">D 350</a></li>
						<li><a href="/catalog/gazobeton_d-400/">D 400</a></li>
						<li><a href="/catalog/gazobeton_d-500/">D 500</a></li>
						<li><a href="/catalog/gazobeton_d-600/">D 600</a></li>
						<li class="mb-3"><a href="/catalog/gazoblok_d-700_/">D 700</a></li>

						<li class="section-title-sm">По производителю</li>
						<li><a href="/catalog/gazobeton_gras/">ГРАС</a></li>
						<li><a href="/catalog/zyab3/">ЗЯБ</a></li>
                        <li><a href="/catalog/gazobeton_uniblock/">Uniblock</a></li>
						<li><a href="/catalog/gazoblok_teplon/">Теплон</a></li>
						<li><a href="/catalog/gazoblok_kottedzh/">Коттедж</a></li>
						<li><a href="/catalog/gazoblok_bikton_/">Биктон</a></li>
						<li><a href="/catalog/gazobeton_zyab-izhevsk/">ЗЯБ (Ижевск)</a></li>
						<li><a href="/catalog/gazoblok_build-stone/">Build stone</a></li>
					</ul>
				  </div>
			  </div>
		  </div>

		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-blue"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/profnastil/">Профнастил</a>

					  <ul class="list-unstyled mb-0">
						  <li class="section-title-sm">По покрытию</li>
                      <li><a href="/catalog/profnastil-normanmp/">С покрытием NormanMP</a></li>
                      <li><a href="/catalog/profnastil-purman/">С покрытием Purman</a></li>
                      <li><a href="/catalog/profnastil-ecosteel/">С покрытием Ecosteel</a></li>
                      <li><a href="/catalog/profnastil-agneta-2x/">С покрытием Agneta</a></li>
                      <li><a href="/catalog/profnastil-cloudy/">С покрытием Cloudy</a></li>
                      <li><a href="/catalog/profnastil-plastizol/">С покрытием Пластизол</a></li>
                      <li><a href="/catalog/profnastil-colorcoat-prisma/">С покрытием Colorcoat Prisma®</a></li>
                      <li><a href="/catalog/profnastil-puretan/">С покрытием Puretan</a></li>
                      <li><a href="/catalog/profnastil-vikingmp/">С покрытием VikingMP®</a></li>
                      <li><a href="/catalog/profnastil-vikingmp-e/">С покрытием VikingMP® Е</a></li>
					    <li><a href="/catalog/profnastil-stalnoy-barkhat/">С покрытием Стальной бархат</a></li>
						<li><a href="/catalog/profnastil-pe-dvs/">С покрытием PE двс</a></li>
						<li><a href="/catalog/profnastil-pe-dachnyy-1/">С покрытием PE Дачный(1)</a></li>
						<li><a href="/catalog/profnastil-pe-dachnyy-2/">С покрытием PE Дачный(2)</a></li>
						<li><a href="/catalog/profnastil-pe-matt/">С покрытием PE Matt</a></li>
						<li><a href="/catalog/profnastil-pe/">С покрытием PE</a></li>
						<li><a href="/catalog/profnastil-zn/">С покрытием Zn</a></li>
						<li><a href="/catalog/profnastil-velur/">С покрытием Velur</a></li>
						<li><a href="/catalog/profnastil-satin-matt/">С покрытием Satin Matt</a></li>
						<li><a href="/catalog/profnastil-satin/">С покрытием Satin</a></li>
						<li><a href="/catalog/profnastil-safari/">С покрытием Safari</a></li>
						<li><a href="/catalog/profnastil-quarzit-pro-matt/">С покрытием Quarzit Pro Matt</a></li>
						<li><a href="/catalog/profnastil-quarzit-lite/">С покрытием Quarzit lite</a></li>
						<li><a href="/catalog/profnastil-quarzit/">С покрытием Quarzit</a></li>
						<li><a href="/catalog/profnastil-purlite-matt/">С покрытием PurLite Matt</a></li>
						<li><a href="/catalog/profnastil-print-rf/">С покрытием Print РФ</a></li>
						<li><a href="/catalog/profnastil-greencoat-pural-matt-bt/">С покрытием GreenCoat Pural Matt BT</a></li>
						<li><a href="/catalog/profnastil-greencoat-pural-bt/">С покрытием GreenCoat Pural BT</a></li>
						<li><a href="/catalog/profnastil-drap/">С покрытием Drap</a></li>
						<li><a href="/catalog/profnastil-colority-print-dp/">С покрытием Colority Print dp</a></li>
                        <li><a href="/catalog/profnastil-colority-print/">С покрытием Colority Print</a></li>
						<li><a href="/catalog/profnastil-atlas/">С покрытием Atlas</a></li>
						  <li class="mb-3"><a href="/catalog/profnastil-poliester/">С покрытием Полиэстер</a></li>

						  <li class="section-title-sm">По применению</li>
                            <li><a href="/catalog/zabornyy-profnastil/">Заборный</a></li>
                            <li><a href="/catalog/krovelnyy-profnastil/">Кровельный</a></li>
                            <li><a href="/catalog/promyshlennyy-profnastil/">Промышленный</a></li>
						  <li class="mb-3"><a href="/catalog/stenovoy-profnastil/">Фасадный (стеновой)</a></li>
						  
						  <li class="section-title-sm">По производителю
						 <li><a href="/catalog/profilirovannyy-list-po-proizvoditelyu-grand-line/">Grand Line</a></li>
						  <li class="mb-3"><a href="/catalog/profilirovannyy-list-po-proizvoditelyu-metall-profil/">Металл Профиль</a></li>
<!--						  <li class="section-title-sm">По цвету</li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-belyy/">Белый</a></li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-zheltyy/">Желтый</a></li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-zelenyy/">Зеленый</a></li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-korichnevyy/">Коричневый</a></li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-krasnyy/">Красный</a></li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-oranzhevyy/">Оранжевый</a></li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-seryy/">Серый</a></li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-fioletovyy/">Фиолетовый</a></li>-->
<!--						  <li><a href="/catalog/profilirovannyy-list-chernyy/">Черный</a></li>-->
					  </ul>
				  </div>
			  </div>
		  </div>

		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-purplelight"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/metallocherepitsa/">Металлочерепица</a>

					  <ul class="list-unstyled border-bottom pb-3">
						  <li class="section-title-sm">По покрытию</li>
						  <li><a href="/catalog/metallocherepitsa-agneta-2x/">С покрытием Agneta</a></li>
						  <li><a href="/catalog/metallocherepitsa-cloudy/">С покрытием Cloudy</a></li>
						  <li><a href="/catalog/metallocherepitsa-colorcoat-prisma/">С покрытием Colorcoat Prisma</a></li>
						  <li><a href="/catalog/metallocherepitsa-normanmp/">С покрытием NormanMP</a></li>
						  <li><a href="/catalog/metallocherepitsa-puretan/">С покрытием Puretan</a></li>
						  <li><a href="/catalog/metallocherepitsa-purman/">С покрытием Purman</a></li>
						  <li><a href="/catalog/metallocherepitsa-vikingmp/">С покрытием VikingMP</a></li>
						  <li><a href="/catalog/metallocherepitsa-vikingmp-e/">С покрытием VikingMP Е</a></li>
						  <li><a href="/catalog/metallocherepitsa-plastizol/">С покрытием Пластизол</a></li>
						  <li><a href="/catalog/metallocherepitsa-print-rf/">С покрытием Print РФ</a></li>
						  <li><a href="/catalog/metallocherepitsa-re-matt/">С покрытием РЕ Matt</a></li>
						  <li><a href="/catalog/metallocherepitsa-re/">С покрытием РЕ</a></li>
						  <li><a href="/catalog/metallocherepitsa-satin/">С покрытием Satin</a></li>
						  <li><a href="/catalog/metallocherepitsa-drap/">С покрытием Drap</a></li>
						  <li><a href="/catalog/metallocherepitsa-satin-matt/">С покрытием Satin Matt</a></li>
						  <li><a href="/catalog/metallocherepitsa-stalnoy-barkhat/">С покрытием Стальной бархат</a></li>
						  <li><a href="/catalog/metallocherepitsa-purlite-matt/">С покрытием PurLite Matt</a></li>
						  <li><a href="/catalog/metallocherepitsa-greencoat-pural-bt/">С покрытием GreenCoat Pural BT</a></li>
						  <li><a href="/catalog/metallocherepitsa-greencoat-pural-matt-bt/">С покрытием GreenCoat Pural Matt BT</a></li>
						  <li><a href="/catalog/metallocherepitsa-atlas/">С покрытием Atlas</a></li>
						  <li><a href="/catalog/metallocherepitsa-velur/">С покрытием Velur</a></li>
						  <li><a href="/catalog/metallocherepitsa-quarzit-lite/">С покрытием Quarzit lite</a></li>
						  <li><a href="/catalog/metallocherepitsa-quarzit/">С покрытием Quarzit</a></li>
						  <li><a href="/catalog/metallocherepitsa-quarzit-pro-matt/">С покрытием Quarzit Pro Matt</a></li>
						  <li><a href="/catalog/metallocherepitsa-safari/">С покрытием Safari</a></li>
						  <li><a href="/catalog/metallocherepitsa-colority-print/">С покрытием Colority Print</a></li>
						  <li class="mb-3"><a href="/catalog/metallocherepitsa-poliester/">С покрытием Полиэстер</a></li>
						  
<!--						  <li class="section-title-sm">По цвету</li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-belyy/">Белый</a></li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-zheltyy/">Желтый</a></li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-zelenyy/">Зеленый</a></li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-korichnevyy/">Коричневый</a></li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-krasnyy/">Красный</a></li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-oranzhevyy/">Оранжевый</a></li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-seryy/">Серый</a></li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-siniy/">Синий</a></li>-->
<!--						  <li><a href="/catalog/metallocherepitsa-fioletovyy/">Фиолетовый</a></li>-->
<!--						  <li class="mb-3"><a href="/catalog/metallocherepitsa-chernyy/">Черный</a></li>-->
						  
						  <li class="section-title-sm">По производителю</li>
						  <li><a href="/catalog/metallocherepitsa-metall-profil/">Металл Профиль</a></li>
						  <li><a href="/catalog/metallocherepitsa-grand-line/">Grand Line</a></li>
					  </ul>
				  </div>
			  </div>
		  </div>
		  
		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-blue"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/blagoustroystva/">Благоустройства</a>

					  <ul class="list-unstyled mb-0">
                      <li><a href="/catalog/plitka-bruschatka/">Брусчатка</a></li>
					  <li><a href="/catalog/plitka-trotuarnye-plity/">Тротуарная плитка</a></li>
					  <li><a href="/catalog/plitka-bordyury/">Бордюры (поребрики)</a></li>
                      <li class="mb-3"><a href="/catalog/plitka-poverkhnostnye-sistemy-vodootvody/">Дренажные решетки (водоотводы)</a></li>
					  </ul>
				  </div>
			  </div>
		  </div>
		  
		  
		  		  <div class="col-xs-12 col-sm-6 col-lg-3 catalog-col">
			  <div class="catalog-card">
				  <div class="catalog-colorlabel bg-yellow"></div>
				  <div class="catalog-body">
					  <a class="catalog-title" href="/catalog/oblitsovochnye-materialy/">Облицовочные материалы</a>

					  <ul class="list-unstyled mb-0">
                      <li><a href="/catalog/plitka-oblitsovochnyy-kamen/">Декоративный камень</a></li>
					  <li><a href="/catalog/plitka-dekorativnyy-kirpich/">Декоративный кирпич</a></li>
					  <li><a href="/catalog/obl_mat_metalicheskiy-sayding/">Металлический сайдинг</a></li>
                      <li class="mb-3"><a href="/catalog/sayding/">Виниловый сайдинг</a></li>
					  </ul>
				  </div>
			  </div>
		  </div>
		  
		  
	  </div>
	</div>
</div>