<footer>
    <div class="container p-sm-0">
        <div class="row top pb-3">
            <div class="col-xl-1 logo pr-2 pl-0 d-none d-xl-block">
                <img class="logo" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/logo_new.svg" width="87" alt="">
            </div>

            <div class="col-xl-6 offset-xl-2 col-lg-9">
                <ul class="footer-menu pl-0 pl-md-0">
                    <li><a href="/about/">О компании</a></li>
                    <li><a href="/articles/">Полезные статьи</a></li>

                    <li><a href="/credit/">В кредит</a></li>
                    <li><a href="/contacts/">Контакты</a></li>

                    <li><a href="/oplata/">Оплата</a></li>
                    <li><a href="/dostavka/">Доставка</a></li>

                    <li><a href="/proizvoditeli/">Производители</a></li>
                </ul>
            </div>

            <div class="col-xl-3 col-lg-3 pr-sm-0 footer-address text-white">
                <div class="small-text">Адрес</div>
                <div class="large">г. Казань, ул. Даурская 12а, <br>этаж 3, офис 20.4</div>
                <div class="small-text">Телефон</div>
                <div class="large" id="podmena">8 (843) 216 32 67</div>
                <div class="small-text">Режим работы</div>
                <div class="large">Пн - Пт: 8:00 - 17:00 Сб: 8:00 - 14:00</div>
            </div>
        </div>

        <div class="row copyright pt-4 d-block d-sm-flex">
            <div class="col-12 p-0 mb-3">Цены на сайте не являются публичной офертой.</div>
            <div class="col-12 p-0">© ООО Тулпар, 2012-2018. <br>
                Все материалы данного сайта являются объектами авторского права (в том числе дизайн). Запрещается копирование, распространение (в том числе путем копирования на другие сайты и ресурсы в Интернете) или любое иное использование информации и объектов без предварительного письменного согласия правообладателя. Любое нарушение авторских и смежных прав преследуется по закону (ст. 146 УК РФ, предусматривающая наказания в виде 2-х лет лишения свободы).
            </div>
        </div>
    </div>
</footer>


<div class="nav-shadow"></div>

<a href="#" class="scrollup">Scroll</a>

<div style="display: none;" class="fancybox-cub" id="credit-application">
	<h2 class="modal-title">Заявка на кредит</h2>
    <form id="credit-form">
        <div class="form-group row">
          <label for="appl_name_credit" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name_credit" name="appl_name_credit" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_phone_credit" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone_credit" name="appl_phone_credit" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <div class="form-group row">
        <label for="appl_comment_credit" class="col-sm-3 col-form-label">Комментарий</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" required id="appl_comment_credit" name="appl_comment_credit" placeholder="Здесь вы можете описать свои комментарии к заявке">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <h2 class="sent-title">Ваша заявка отправлена</h2>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="credit-form" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>


<div style="display: none;" class="fancybox-cub" id="zamer-application">
	<h2 class="modal-title">Заявка на расчет</h2>
    <form id="calculation">
        <input type="hidden" name="topic" value="Заявка на расчет">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name" name="appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_email" class="col-sm-3 col-form-label">Эл. почта<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="email" class="form-control" required id="appl_email" name="appl_email" placeholder="Email">
        </div>
      </div>
      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone" name="appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <div class="form-group row">
        <label for="appl_comment" class="col-sm-3 col-form-label">Комментарий</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" required id="appl_comment" name="appl_comment" placeholder="Здесь вы можете описать свои комментарии к заявке">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <h2 class="sent-title">Ваша заявка отправлена</h2>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="calculation" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div style="display: none;" class="fancybox-cub" id="oneclick-buy-modal">
    <div class="callback_show_on_open">
        <h2>Купить в один клик</h2>
        <div class="product_oneclick"></div>

        <div class="attention">
            Наш специалист свяжется с Вами в течение 15 минут
        </div>
    </div>

    <form id="oneclick-buy-form">
        <input type="hidden" name="topic" value="Купить в один клик">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name" name="appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone" name="appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <h2 class="sent-title">Ваша заявка отправлена</h2>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="oneclick-buy-form" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div style="display: none;" class="fancybox-cub" id="callback-enquiry">
    <div class="callback_show_on_open">
        <h2>Заказать обратный звонок</h2>

        <div class="attention">
            Наш специалист свяжется с Вами в течение 30 минут
        </div>
    </div>

    <form id="callback-form">
        <input type="hidden" name="topic" value="Обратный звонок">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name" name="appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone" name="appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <h2 class="sent-title">Ваша заявка отправлена</h2>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="callback-form" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div style="display: none;" class="fancybox-cub" id="feedback-application">
	<h2 class="modal-title">Написать отзыв</h2>
    <form id="feedback-form">
        <input type="hidden" name="topic" value="Заявка на расчет">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name" name="appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_email" class="col-sm-3 col-form-label">Эл. почта<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="email" class="form-control" required id="appl_email" name="appl_email" placeholder="Email">
        </div>
      </div>
      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone" name="appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <div class="form-group row">
        <label for="appl_comment" class="col-sm-3 col-form-label">Комментарий</label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_comment" name="appl_comment" placeholder="Здесь вы можете описать свои комментарии к заявке">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <h2 class="sent-title">Ваша заявка отправлена</h2>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="calculation" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div style="display: none;" class="fancybox-cub" id="send-cart">
	<h2 class="modal-title">Оформление заказа</h2>
    <form id="cart_form">
        <input type="hidden" name="cart_topic" value="Оформление заказа">
        <input type="hidden" id="cart_items_input" name="cart_items" value="">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="cart_appl_name" name="cart_appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>

      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="cart_appl_phone" name="cart_appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <div class="form-group row">
        <label for="appl_comment" class="col-sm-3 col-form-label">Комментарий</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" required id="cart_appl_comment" name="cart_appl_comment" placeholder="Здесь вы можете описать свои комментарии к заявке">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <h2 class="sent-title">Ваша заказ отправлен</h2>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="cart_form" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

</body>
</html>