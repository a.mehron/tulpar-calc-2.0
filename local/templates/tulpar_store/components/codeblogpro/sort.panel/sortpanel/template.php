<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Localization\Loc;

if (!empty($arResult['SORT']['PROPERTIES'])) { ?>
    <span class="message"><?= Loc::getMessage('CODEBLOGPRO_SORT_PANEL_COMPONENT_TEMPALTE_SORT_BY_VALUE') ?>:</span><br>
    <?
    // echo "<pre>";
    // print_r($arResult['SORT']['PROPERTIES']);
    // echo "</pre>";

    $sort_option = "Сначала популярные";
    if ($_GET['sort'] == "catalog_PRICE_1" && $_GET['order'] == "desc") {
        $sort_option = "Сначала подороже";
    } elseif ($_GET['sort'] == "catalog_PRICE_1" && $_GET['order'] == "asc") {
        $sort_option = "Сначала подешевле";
    }elseif ($_GET['sort'] == "sort" && $_GET['order'] == "desc") {
        $sort_option = "Сначала популярные";
    }elseif ($_GET['sort'] == "sort" && $_GET['order'] == "asc") {
        $sort_option = "Сначала популярные";
    }
    ?>
    <div class="d-inline-block">
        <span class="result-sort-current">
            <span class="result-sort-text"><?=$sort_option?></span>
            <span class="caret"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/angle-down.svg" alt=""></span>
        </span>
        <ul class="result-sort-list">
        <? foreach ($arResult['SORT']['PROPERTIES'] as $property) { ?>
            <?
            // echo "<pre>";
            //     print_r($property);
            // echo "</pre>";
            ?>
            <?
            $prop_name_asc = "";
            $prop_name_desc = "";

            if ($property['CODE'] == "sort") {
                $prop_name_asc = "Сначала популярные";
                $prop_name_desc = "Сначала популярные";
            }
            elseif ($property['CODE'] == "catalog_PRICE_1") {
                $prop_name_asc = "Сначала подешевле";
                $prop_name_desc = "Сначала подороже ";
            }

            $temp = explode("&", $property['URL']);
            $new_string = "";
            $new_string_asc = "";
            for ($i=0; $i < sizeof($temp); $i++) {
                if ($i == 0) {
                    $new_string = $temp[$i]."&order=desc";
                    $new_string_asc = $temp[$i]."&order=asc";
                } else {
                    $new_string = $new_string."&".$temp[$i];
                    $new_string_asc = $new_string_asc."&".$temp[$i];
                }
            }
            ?>
            
            <li><a href="<?=$new_string_asc?>"><?= $prop_name_asc ?></a></li>
            <?php if ($property['CODE'] != "sort"): ?>
                <li><a href="<?=$new_string?>"><?= $prop_name_desc ?></a></li>
            <?php endif; ?>

            <?
        } ?>
        </ul>
    </div>
<?} ?>
