<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

/**
 * @global CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arParams
 * @var array $arResult
 * @var array $arCurSection
 */

if (in_array($arCurSection['ID'], array(634, 256, 258, 260, 274, 261))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/gazobetonnye-bloki-m/"/>',true);
} elseif (in_array($arCurSection['ID'], array(281, 268))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/gazoblok/"/>',true);
} elseif (in_array($arCurSection['ID'], array(872, 873, 874))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/derevyannye-lestnitsy/"/>',true);
} elseif (in_array($arCurSection['ID'], array(225, 633, 226, 224))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/keramicheskie-bloki/"/>',true);
} elseif (in_array($arCurSection['ID'], array(113,116,655))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/keramicheskiy-kirpich/"/>',true);
} elseif (in_array($arCurSection['ID'], array(117,499,500))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/keramicheskiy-oblitsovochnyy/"/>',true);
} elseif (in_array($arCurSection['ID'], array(111, 112, 114))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/keramicheskiy-kirpich/"/>',true);
} elseif (in_array($arCurSection['ID'], array(36, 23, 31, 42, 54, 26, 654))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/kirpich/"/>',true);
} elseif (in_array($arCurSection['ID'], array(656,63,59,70))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/oblitsovochnyy/"/>',true);
} elseif (in_array($arCurSection['ID'], array(217, 218))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/ogneupornyy-kirpich/"/>',true);
} elseif (in_array($arCurSection['ID'], array(150, 556))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/stroitelnyy-kirpich/"/>',true);
} elseif (in_array($arCurSection['ID'], array(210, 211))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/shamotnyy_kirpich/"/>',true);
} elseif (in_array($arCurSection['ID'], array(728))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/klinkernyy/"/>',true);
} elseif (in_array($arCurSection['ID'], array(1001))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/sayding/"/>',true);
} elseif (in_array($arCurSection['ID'], array(837, 920, 836, 846, 841, 838, 839))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/lestnitsy/"/>',true);
} elseif (in_array($arCurSection['ID'], array(890,875,876,877))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/mezhetazhnye-lestnitsy/"/>',true);
} elseif (in_array($arCurSection['ID'], array(1034,289,979))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/metallocherepitsa/"/>',true);
} elseif (in_array($arCurSection['ID'], array(362))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/profnastil/"/>',true);
} elseif (in_array($arCurSection['ID'], array(635,699,1048))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/stroitelnaya-setka/"/>',true);
} elseif (in_array($arCurSection['ID'], array(479))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/uplotniteli/"/>',true);
} elseif (in_array($arCurSection['ID'], array(867, 861, 862))) {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '/catalog/lestnitsy-cherdachnye/"/>',true);
} else {
    $APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . parse_url ( $_SERVER[ 'REQUEST_URI' ] )[ 'path' ] . '"/>',true);
	
}


if (in_array($arCurSection['ID'], array(963,792,795,794,793,486,174,171,172,175,193,1089,1092,1087,1090,1091,1081,1079,1073,1080,1072,1075,1074,1082,1088,1086,1076,1093,1085, 1078, 1083,1077,996,1062,1053,1059,1050,1063,1054,1057,1061,1058,1070,363,1071,1051,630,1060,1066,1098,1118,1104,1107,1103,335,1113,1112,1101,1108,1114,1100,1096,1109,1105,1106,1115,1097,1102,1099,1116,1095,337,1110,1094,1111,752,750,760,753,757,761,773,759,755,758,751,754,995,1035,1038,1033,1039,1029,1030,1028,1031,1026,1037,1040,1036,1027,1041,1025,1032,1012,1011,1022,1016,1013,312,1010,1018,1023,1021,1009,1015,1024,1042,1020,1014,1019,1017,639,756,1084,1052,1067,1065,1064))) {
    $APPLICATION->AddHeadString('<meta name="robots" content="noindex, nofollow">',true);
}


if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
	$basketAction = isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '';
}
else
{
	$basketAction = isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '';
}?>


<div class="catalog-container">
	<div class="container p-sm-0">
<!--        <pre>-->
<!--            --><?//echo $arCurSection['ID'];?>
<!--        </pre>-->

<!--        <pre>-->
<!--            --><?//
//            echo $arCurSection['ID'];
//            if (getUserIpAddr() === "94.180.229.184") {
//
//                define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
//                $current = file_get_contents(LOG_FILENAME);
//                $current .= ($arCurSection['ID'].",");
//                file_put_contents(LOG_FILENAME, $current);
//            }
//            ?>
<!--        </pre>-->

	<div class="row">
		<div class="filter-sort-btns d-md-none d-flex justify-content-between w-100 mb-sm-4">
			<button type="button" class="btn btn-info">Фильтр</button>
			<button type="button" class="btn btn-light border sort-btn">Сортировать по:</button>
		</div>


<?
if ($isFilter || $isSidebar): ?>
	<div class="col-xl-3 col-lg-4 col-md-4 filter-container pl-0">
		<? if ($isFilter): ?>
			<div class="filter bg-white p-2 d-none d-md-block">
				<?
				$APPLICATION->IncludeComponent(
					"bitrix:catalog.smart.filter",
					"bootstrap_v40",
					array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"SECTION_ID" => $arCurSection['ID'],
						"FILTER_NAME" => $arParams["FILTER_NAME"],
						"PRICE_CODE" => $arParams["~PRICE_CODE"],
						"CACHE_TYPE" => $arParams["CACHE_TYPE"],
						"CACHE_TIME" => $arParams["CACHE_TIME"],
						"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
						"SAVE_IN_SESSION" => "N",
						"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
						"XML_EXPORT" => "N",
						"SECTION_TITLE" => "NAME",
						"SECTION_DESCRIPTION" => "DESCRIPTION",
						'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
						"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
						'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
						'CURRENCY_ID' => $arParams['CURRENCY_ID'],
						"SEF_MODE" => $arParams["SEF_MODE"],
						"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
						"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
						"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
						"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
					),
					$component,
					array('HIDE_ICONS' => 'Y')
				);
				?>
			</div>
		<? endif ?>
	</div>
<?endif?>
<div class="col-xl-9 col-md-8 p-0">
	<?php
		$nav = CIBlockSection::GetNavChain(4, $arCurSection['ID']);

		if($ar_res = $nav->GetNext())
		  $parent_section_id = $ar_res['ID'];
	?>

	<?php

	// if ($parent_section_id == 20){
	// 	$iblock_id = 10;
	// } elseif ($parent_section_id == 223) {
	// 	$iblock_id = 11;
	// } elseif ($parent_section_id == 526) {
	// 	$iblock_id = 12;
	// } elseif ($parent_section_id == 255) {
	// 	$iblock_id = 13;
	// } elseif ($parent_section_id == 531) {
	// 	$iblock_id = 14;
	// }

	?>

	<?php if ($iblock_id != ""): ?>
		<!-- <div class="section-banner d-none d-md-block">
			<?
		// 	$APPLICATION->IncludeComponent(
		// 	"bitrix:news.list",
		// 	"banner-slider-section",
		// 	array(
		// 		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		// 		"ADD_SECTIONS_CHAIN" => "N",
		// 		"AJAX_MODE" => "N",
		// 		"AJAX_OPTION_ADDITIONAL" => "",
		// 		"AJAX_OPTION_HISTORY" => "N",
		// 		"AJAX_OPTION_JUMP" => "N",
		// 		"AJAX_OPTION_STYLE" => "Y",
		// 		"CACHE_FILTER" => "N",
		// 		"CACHE_GROUPS" => "Y",
		// 		"CACHE_TIME" => "36000000",
		// 		"CACHE_TYPE" => "A",
		// 		"CHECK_DATES" => "Y",
		// 		"DETAIL_URL" => "",
		// 		"DISPLAY_BOTTOM_PAGER" => "Y",
		// 		"DISPLAY_DATE" => "N",
		// 		"DISPLAY_NAME" => "N",
		// 		"DISPLAY_PICTURE" => "N",
		// 		"DISPLAY_PREVIEW_TEXT" => "N",
		// 		"DISPLAY_TOP_PAGER" => "N",
		// 		"FIELD_CODE" => array(
		// 			0 => "",
		// 			1 => "DETAIL_PICTURE",
		// 			2 => "",
		// 		),
		// 		"FILTER_NAME" => "",
		// 		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		// 		"IBLOCK_ID" => "$iblock_id",
		// 		"IBLOCK_TYPE" => "catalog_banners",
		// 		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		// 		"INCLUDE_SUBSECTIONS" => "N",
		// 		"MESSAGE_404" => "",
		// 		"NEWS_COUNT" => "6",
		// 		"PAGER_BASE_LINK_ENABLE" => "N",
		// 		"PAGER_DESC_NUMBERING" => "N",
		// 		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		// 		"PAGER_SHOW_ALL" => "N",
		// 		"PAGER_SHOW_ALWAYS" => "N",
		// 		"PAGER_TEMPLATE" => ".default",
		// 		"PAGER_TITLE" => "Новости",
		// 		"PARENT_SECTION" => "",
		// 		"PARENT_SECTION_CODE" => "",
		// 		"PREVIEW_TRUNCATE_LEN" => "",
		// 		"PROPERTY_CODE" => array(
		// 			0 => "",
		// 			1 => "ATT_PRODUCT_ON_BANNER",
		// 			2 => "",
		// 		),
		// 		"SET_BROWSER_TITLE" => "N",
		// 		"SET_LAST_MODIFIED" => "N",
		// 		"SET_META_DESCRIPTION" => "N",
		// 		"SET_META_KEYWORDS" => "N",
		// 		"SET_STATUS_404" => "N",
		// 		"SET_TITLE" => "N",
		// 		"SHOW_404" => "N",
		// 		"SORT_BY1" => "ACTIVE_FROM",
		// 		"SORT_BY2" => "SORT",
		// 		"SORT_ORDER1" => "DESC",
		// 		"SORT_ORDER2" => "ASC",
		// 		"STRICT_SECTION_CHECK" => "N",
		// 		"COMPONENT_TEMPLATE" => "",
		// 		"COMPOSITE_FRAME_MODE" => "A",
		// 		"COMPOSITE_FRAME_TYPE" => "AUTO"
		// 	),
		// 	false
		// );
		?>
		</div> -->
	<?php endif; ?>
			<?
			$APPLICATION->IncludeComponent(
				"bitrix:catalog.section.list",
				"",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
					"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
					"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
					"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
					"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : '')
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);

			if ($arParams["USE_COMPARE"]=="Y")
			{
				$APPLICATION->IncludeComponent(
					"bitrix:catalog.compare.list",
					"",
					array(
						"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
						"IBLOCK_ID" => $arParams["IBLOCK_ID"],
						"NAME" => $arParams["COMPARE_NAME"],
						"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
						"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
						"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action"),
						"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
						'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
						'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
					),
					$component,
					array("HIDE_ICONS" => "Y")
				);
			}

			$intSectionID = $APPLICATION->IncludeComponent(
				"bitrix:catalog.section",
				"catalog-section3",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"ELEMENT_SORT_FIELD" => $_GET["sort"],
					"ELEMENT_SORT_ORDER" => $_GET["order"],
					"ELEMENT_SORT_FIELD2" => "",
					"ELEMENT_SORT_ORDER2" => "",
					"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
					"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
					"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
					"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
					"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
					"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
					"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
					"BASKET_URL" => $arParams["BASKET_URL"],
					"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
					"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
					"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
					"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
					"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_FILTER" => $arParams["CACHE_FILTER"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SET_TITLE" => $arParams["SET_TITLE"],
					"MESSAGE_404" => $arParams["~MESSAGE_404"],
					"SET_STATUS_404" => $arParams["SET_STATUS_404"],
					"SHOW_404" => $arParams["SHOW_404"],
					"FILE_404" => $arParams["FILE_404"],
					"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
					"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
					"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
					"PRICE_CODE" => $arParams["~PRICE_CODE"],
					"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
					"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

					"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
					"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
					"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
					"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
					"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

					"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
					"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
					"PAGER_TITLE" => $arParams["PAGER_TITLE"],
					"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
					"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
					"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
					"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
					"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
					"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
					"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
					"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
					"LAZY_LOAD" => $arParams["LAZY_LOAD"],
					"MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
					"LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

					"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
					"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
					"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
					"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
					"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
					"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
					"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
					"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

					"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
					"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
					"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
					"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
					"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
					'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
					'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

					'LABEL_PROP' => $arParams['LABEL_PROP'],
					'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
					'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
					'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
					'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
					'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
					'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
					'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
					'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
					'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
					'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
					'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

					'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
					'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
					'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
					'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
					'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
					'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
					'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
					'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
					'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
					'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
					'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
					'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
					'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
					'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
					'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
					'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
					'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

					'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
					'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
					'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

					'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
					"ADD_SECTIONS_CHAIN" => "N",
					'ADD_TO_BASKET_ACTION' => $basketAction,
					'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
					'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
					'COMPARE_NAME' => $arParams['COMPARE_NAME'],
					'USE_COMPARE_LIST' => 'Y',
					'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
					'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
					'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
				),
				$component
			);
			?>
		<?
		$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;

		if (ModuleManager::isModuleInstalled("sale"))
		{
			if (!empty($arRecomData))
			{
				if (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N')
				{
					?>
					<div class="col-xs-12" data-entity="parent-container">
						<div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
							<?=GetMessage('CATALOG_PERSONAL_RECOM')?>
						</div>
						<?
						$APPLICATION->IncludeComponent(
							"bitrix:catalog.section",
							"",
							array(
								"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
								"IBLOCK_ID" => $arParams["IBLOCK_ID"],
								"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
								"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
								"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
								"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
								"PROPERTY_CODE" => $arParams["LIST_PROPERTY_CODE"],
								"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
								"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
								"BASKET_URL" => $arParams["BASKET_URL"],
								"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
								"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
								"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
								"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
								"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
								"CACHE_TYPE" => $arParams["CACHE_TYPE"],
								"CACHE_TIME" => $arParams["CACHE_TIME"],
								"CACHE_FILTER" => $arParams["CACHE_FILTER"],
								"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
								"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
								"PAGE_ELEMENT_COUNT" => 0,
								"PRICE_CODE" => $arParams["~PRICE_CODE"],
								"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
								"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

								"SET_BROWSER_TITLE" => "N",
								"SET_META_KEYWORDS" => "N",
								"SET_META_DESCRIPTION" => "N",
								"SET_LAST_MODIFIED" => "N",
								"ADD_SECTIONS_CHAIN" => "N",

								"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
								"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
								"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
								"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
								"PRODUCT_PROPERTIES" => $arParams["PRODUCT_PROPERTIES"],

								"OFFERS_CART_PROPERTIES" => $arParams["OFFERS_CART_PROPERTIES"],
								"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
								"OFFERS_PROPERTY_CODE" => $arParams["LIST_OFFERS_PROPERTY_CODE"],
								"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
								"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
								"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
								"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
								"OFFERS_LIMIT" => $arParams["LIST_OFFERS_LIMIT"],

								"SECTION_ID" => $intSectionID,
								"SECTION_CODE" => "",
								"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
								"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
								"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
								'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
								'CURRENCY_ID' => $arParams['CURRENCY_ID'],
								'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
								'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

								'LABEL_PROP' => $arParams['LABEL_PROP'],
								'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
								'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
								'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
								'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
								'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
								'PRODUCT_ROW_VARIANTS' => "[{'VARIANT':'3','BIG_DATA':true}]",
								'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
								'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
								'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
								'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
								'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

								"DISPLAY_TOP_PAGER" => 'N',
								"DISPLAY_BOTTOM_PAGER" => 'N',
								"HIDE_SECTION_DESCRIPTION" => "Y",

								"RCM_TYPE" => isset($arParams['BIG_DATA_RCM_TYPE']) ? $arParams['BIG_DATA_RCM_TYPE'] : '',
								"SHOW_FROM_SECTION" => 'Y',

								'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
								'OFFER_TREE_PROPS' => $arParams['OFFER_TREE_PROPS'],
								'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
								'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
								'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
								'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
								'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
								'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
								'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
								'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
								'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
								'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
								'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
								'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
								'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
								'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
								'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

								'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
								'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
								'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

								'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
								'ADD_TO_BASKET_ACTION' => $basketAction,
								'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
								'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
								'COMPARE_NAME' => $arParams['COMPARE_NAME'],
								'USE_COMPARE_LIST' => 'Y',
								'BACKGROUND_IMAGE' => '',
								'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
							),
							$component
						);
						?>
					</div>
					<?
				}
			}
		}
		?>

</div>

		</div>
	</div>
</div>
