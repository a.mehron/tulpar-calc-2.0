<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$APPLICATION->SetTitle("ds");

?><? $h1 = $arResult['SECTION']['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']; ?>

<?//$APPLICATION->AddHeadScript('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')?>
<?//$APPLICATION->AddHeadScript('https://cdn.datatables.net/buttons/1.5.4/js/dataTables.buttons.min.js')?>
<?$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')?>
<?$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js')?>
<?$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js')?>
<?//$APPLICATION->AddHeadScript('https://cdn.datatables.net/buttons/1.5.4/js/buttons.html5.min.js')?>


<?

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
    'LIST' => array(
        'CONT' => 'bx_sitemap',
        'TITLE' => 'bx_sitemap_title',
        'LIST' => 'bx_sitemap_ul',
    ),
    'LINE' => array(
        'CONT' => 'bx_catalog_line',
        'TITLE' => 'bx_catalog_line_category_title',
        'LIST' => 'bx_catalog_line_ul',
        'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
    ),
    'TEXT' => array(
        'CONT' => 'bx_catalog_text',
        'TITLE' => 'bx_catalog_text_category_title',
        'LIST' => 'bx_catalog_text_ul'
    ),
    'TILE' => array(
        'CONT' => 'bx_catalog_tile',
        'TITLE' => 'bx_catalog_tile_category_title',
        'LIST' => 'bx_catalog_tile_ul',
        'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
    )
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));


$header_one = isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
    ? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
    : $arResult['SECTION']['NAME'];
?>


<?
//$_SESSION['header_one'] = $header_one;
if ($arResult['META_DESCRIPTION']) {
    $APPLICATION->SetPageProperty('description', $arResult['META_DESCRIPTION']);
} else {
    $APPLICATION->SetPageProperty('description', $header_one.' купить недорого в Казани - магазин “Тулпар” ★ Цены от 7,10 руб ★ Доставка на объект ★ В наличии большой ассортимент.');
}

// $APPLICATION->SetPageProperty('title', "$h1 купить в Казани - цены от 7,29 руб в Тулпар.");

?>
<?php $showSections = "hide_sections" ?>


<div class="catalog-page <?=$showSections?>">
    <div class="container p-sm-0">

        <?php
        $page = $APPLICATION->GetCurPage();
        if ($page == "/catalog/") { ?>
            <div class="row">
                <h1 class="catalog-page-title">Каталог товаров</h1>
            </div>
        <?	}
        ?>


        <?
        $section_id = $arResult['SECTION']['ID'];
        if(CModule::IncludeModule('iblock'))
        {
            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "DETAIL_PAGE_URL", "PREVIEW_PICTURE");
            $arFilter = Array("IBLOCK_ID"=>IntVal(4), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID" => $section_id, "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да");

            $res = CIBlockElement::GetList(
                Array("SORT" => "ASC"),
                $arFilter,
                false,
                Array('nTopCount' => 10),
                $arSelect
            );

            while($ob = $res->GetNextElement()){
                $arItem = $ob->GetFields();
                $arrayResult[] = $arItem['ID'];
            }

            ////////////////

            $arSelect = Array("ID", "IBLOCK_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_*", "DETAIL_PAGE_URL", "PREVIEW_PICTURE");
            $arFilter = Array("IBLOCK_ID"=>IntVal(10), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y");

            $res = CIBlockElement::GetList(
                Array("SORT" => "ASC"),
                $arFilter,
                false,
                false,
                $arSelect
            );

            while($ob = $res->GetNextElement()){
                $arItem = $ob->GetFields();
                $arItem["PROPERTIES"] = $ob->GetProperties();
                $arrayResultBanners[] = $arItem;
            }

            $filterArray = Array();
            $bannerCount = 0;
            foreach ($arrayResult as $productID) {
                if ($bannerCount > 2) {
                    break;
                }
                foreach ($arrayResultBanners as $banner) {
                    if ($productID == $banner['PROPERTIES']['ATT_PRODUCT_ON_BANNER']['VALUE']) {
                        $filterArray[] = $banner['ID'];
                        $bannerCount++;
                    }
                }
            }

        }
        ?>

        <?php $size = sizeof($filterArray); ?>
        <?php if ($size > 0): ?>
            <div class="section-banner d-none d-md-block">
                <?
                $GLOBALS['arrFilter'] = array('ID' => $filterArray);
                // #FW REGIONS
                $GLOBALS['arrFilter'][]=getRegionsFilter();

                $APPLICATION->IncludeComponent(
                    "bitrix:news.list",
                    "banner-slider-section",
                    array(
                        "ACTIVE_DATE_FORMAT" => "d.m.Y",
                        "ADD_SECTIONS_CHAIN" => "N",
                        "AJAX_MODE" => "N",
                        "AJAX_OPTION_ADDITIONAL" => "",
                        "AJAX_OPTION_HISTORY" => "N",
                        "AJAX_OPTION_JUMP" => "N",
                        "AJAX_OPTION_STYLE" => "Y",
                        "CACHE_FILTER" => "N",
                        "CACHE_GROUPS" => "Y",
                        "CACHE_TIME" => "36000000",
                        "CACHE_TYPE" => "A",
                        "CHECK_DATES" => "Y",
                        "DETAIL_URL" => "",
                        "DISPLAY_BOTTOM_PAGER" => "Y",
                        "DISPLAY_DATE" => "N",
                        "DISPLAY_NAME" => "N",
                        "DISPLAY_PICTURE" => "N",
                        "DISPLAY_PREVIEW_TEXT" => "N",
                        "DISPLAY_TOP_PAGER" => "N",
                        "FIELD_CODE" => array(
                            0 => "DETAIL_PICTURE",
                            1 => "",
                        ),
                        "FILTER_NAME" => "arrFilter",
                        "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                        "IBLOCK_ID" => "10",
                        "IBLOCK_TYPE" => "catalog_banners",
                        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                        "INCLUDE_SUBSECTIONS" => "N",
                        "MESSAGE_404" => "",
                        "NEWS_COUNT" => "6",
                        "PAGER_BASE_LINK_ENABLE" => "N",
                        "PAGER_DESC_NUMBERING" => "N",
                        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                        "PAGER_SHOW_ALL" => "N",
                        "PAGER_SHOW_ALWAYS" => "N",
                        "PAGER_TEMPLATE" => ".default",
                        "PAGER_TITLE" => "Новости",
                        "PARENT_SECTION" => "",
                        "PARENT_SECTION_CODE" => "",
                        "PREVIEW_TRUNCATE_LEN" => "",
                        "PROPERTY_CODE" => array(
                            0 => "",
                            1 => "ATT_PRODUCT_ON_BANNER",
                            2 => "",
                        ),
                        "SET_BROWSER_TITLE" => "N",
                        "SET_LAST_MODIFIED" => "N",
                        "SET_META_DESCRIPTION" => "N",
                        "SET_META_KEYWORDS" => "N",
                        "SET_STATUS_404" => "N",
                        "SET_TITLE" => "N",
                        "SHOW_404" => "N",
                        "SORT_BY1" => "SORT",
                        "SORT_BY2" => "ACTIVE_FROM",
                        "SORT_ORDER1" => "ASC",
                        "SORT_ORDER2" => "DESC",
                        "STRICT_SECTION_CHECK" => "N",
                        "COMPONENT_TEMPLATE" => "banner-slider-section",
                        "COMPOSITE_FRAME_MODE" => "A",
                        "COMPOSITE_FRAME_TYPE" => "AUTO"
                    ),
                    false
                );
                ?>
            </div>
        <?php endif; ?>



        <?
        if(CModule::IncludeModule("catalog") && CModule::IncludeModule("iblock")){
        global $arrFilter;
        $arSelect = Array("IBLOCK_ID",
            "ID",
            "NAME",
            "PROPERTY_ATT_MAKER",
            "PROPERTY_ATT_YOUTUBE_ID",
            "PROPERTY_ATT_VIDEO_COVER",
            "PROPERTY_ATT_ZAVOD_YOUTUBE_ID",
            "PROPERTY_ATT_ZAVOD_VIDEO_COVER"
        );
        $arFilter = Array(
            "IBLOCK_ID"=>IntVal(4),
            "SECTION_ID" => $arResult['SECTION']['ID'],
            "ACTIVE_DATE"=>"Y",
            "ACTIVE"=>"Y",
            "PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да",
            "!PROPERTY_ATT_YOUTUBE_ID" => false
        );

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect); ?>
        <?php if ($res->SelectedRowsCount() > 0): ?>

        <div class="section-videos">
            <!--					<button class="show-video">-->
            <!--						Посмотреть видео про --><?//=$header_one?>
            <!--						<img src="--><?//=SITE_TEMPLATE_PATH?><!--/assets/img/icons/play-btn.svg" alt="">-->
            <!--					</button>-->
            <div class="videos-container">

                <?
                $productReviews = array();
                $factoryReviews = array();
                while($ob = $res->GetNextElement())
                {

                    $arFields = $ob->GetFields();


                    $productReviewVideo = array("IMAGE" => $arFields['PROPERTY_ATT_VIDEO_COVER_VALUE'], "VIDEO_URL" => $arFields['PROPERTY_ATT_YOUTUBE_ID_VALUE']);
                    $factoryReviewVideo = array("IMAGE" => $arFields['PROPERTY_ATT_ZAVOD_VIDEO_COVER_VALUE'], "VIDEO_URL" => $arFields['PROPERTY_ATT_ZAVOD_YOUTUBE_ID_VALUE']);

                    $img_productReviewVideo = CFile::ResizeImageGet($arFields['PROPERTY_ATT_VIDEO_COVER_VALUE'], array('width'=>190, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);
                    $img_factoryReviewVideo = CFile::ResizeImageGet($arFields['PROPERTY_ATT_ZAVOD_VIDEO_COVER_VALUE'], array('width'=>190, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL, true);

                    array_push($productReviews, $productReviewVideo);
                    array_push($factoryReviews, $factoryReviewVideo);


                    if (!in_array($arFields['PROPERTY_ATT_YOUTUBE_ID_VALUE'], $productReviews)) {
                        echo '<div class="video-box">
										<a data-fancybox href="https://www.youtube.com/watch?v='.$arFields['PROPERTY_ATT_YOUTUBE_ID_VALUE'].'">
                                            <img src="'.$img_productReviewVideo['src'].'" alt="">										</a>
										<span class="h3">'.$arFields['NAME'].'</span>
									  </div>';
                    }


                    if (($arFields['PROPERTY_ATT_ZAVOD_YOUTUBE_ID_VALUE'] != "") && (!in_array($arFields['PROPERTY_ATT_ZAVOD_YOUTUBE_ID_VALUE'], $factory_videos))) {
                        echo '<div class="video-box">
										<a data-fancybox href="https://www.youtube.com/watch?v='.$arFields['PROPERTY_ATT_ZAVOD_YOUTUBE_ID_VALUE'].'">
                                            <img src="'.$img_productReviewVideo['src'].'" alt="">										</a>
										<span class="h3">'.$arFields['PROPERTY_ATT_MAKER_VALUE'].'</span>
									  </div>';
                    }

                    $factory_videos[] = $arFields['PROPERTY_ATT_ZAVOD_YOUTUBE_ID_VALUE'];

                }
                echo "</div></div>";
                endif;
                }
                ?>

                <!--PRICELIST START  -->
                <?

                $sectionID = $arResult['SECTION']['ID'];
                if(CModule::IncludeModule("catalog") && CModule::IncludeModule("iblock")){
                    global $arrFilterPriceList;
                    $arSelect = Array("IBLOCK_ID",
                        "ID",
                        "NAME",
                        "PROPERTY_*",
                        "CATALOG_GROUP_1"
                    );

                    if ($sectionID == 255) {
                        $strengthMark = "Марка прочности";
                    }
                    $arrFilterPriceList = Array("IBLOCK_ID"=>IntVal(4), "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", "SECTION_ID" => $sectionID);
                    $res = CIBlockElement::GetList(
                        Array("SORT" => "ASC"),
                        $arrFilterPriceList,
                        false,
                        false,
                        $arSelect
                    );

                    while($ob = $res->GetNextElement()){
                        $arItem = $ob->GetFields();
                        $arItem["PROPERTIES"] = $ob->GetProperties();
                        $arrayResultItems[] = $arItem;
                    }

                    $propertiesToSkip = Array($strengthMark, "Тип", "Картинки галерея", "Является основным товаром", "Основной товар",
                        "Оттенок цвета", "Твердость по карандашу", "Прочность при ударе, Дж.", "Прочность при изгибе, Т.", "Защита соляного тумана, часов", "Стойкость к УФ - излучениям", "Максимальная температура эксплуатации, C", "Блеск, %", "Стоимость доставки", "Цвет смеси", "Предел прочности, Н/мм2", "Прочность на сжатие, МПа", "Прочность сцепления, МПа", "Пористость изделий, %", "Размер", "Огнеупорность, °C", "Ширина профиля, мм", "Коэффициент теплопроводности, Вт/(м·°C)", "Водопоглощение, %");
                    $theadValues[] = Array('NAME', "Наименование");
                    $tbodyValues = Array();
                    $counter = 0;
                    foreach ($arrayResultItems as $arrayItem) {
                        if ($arrayItem['PROPERTIES']['ATT_IS_MAIN_ITEM']['VALUE'] == "Да") {
                            continue;
                        }

                        foreach ($arrayItem['PROPERTIES'] as $key => $property) {
                            if (in_array($property['NAME'], $propertiesToSkip)) {
                                continue;
                            }


                            $keyFound = "false";

                            if ($property['VALUE'] != "") {
                                $curArray = array($key, $property['NAME']);
                                if (!in_array($curArray, $theadValues)) {
                                    $theadValues[] = Array($key, $property['NAME']);
                                }
                            }
                        }
                    }

                    foreach ($arrayResultItems as $arrayItem) {
                        if ($arrayItem['PROPERTIES']['ATT_IS_MAIN_ITEM']['VALUE'] == "Да") {
                            continue;
                        }

                        $rowValues = Array($arrayItem['NAME']);
                        $i = 0;
                        foreach ($arrayItem['PROPERTIES'] as $key => $property) {
                            $curArray = array($key, $property['NAME']);
                            if (in_array($curArray, $theadValues)) {
                                $rowValues[] = $property['VALUE'];
                            }
                        }
                        $rowValues[] = $arrayItem['CATALOG_PRICE_1'];

                        $tbodyValues[] = $rowValues;

                    }
                }
                ?>
                <table class="priceList d-none"  id="priceList">
                    <thead>
                    <th>№</th>
                    <?php foreach ($theadValues as $thead): ?>
                        <th class="p-3"><?=$thead[1]?></th>
                    <?php endforeach; ?>
                    <th>Цена, руб.</th>
                    </thead>

                    <tbody>
                    <?php foreach ($tbodyValues as $key => $itemRow): ?>
                        <tr>
                            <td><?=++$key?></td>
                            <?php foreach ($itemRow as $propItem): ?>
                                <td><?=$propItem?></td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <!--price list end 2 -->



                <div class="<? echo $arCurView['CONT']; ?>"><?
                    if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID'])
                    {
                        $this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
                        $this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                        ?><h1
                        class="<? echo $arCurView['TITLE']; ?>"
                        id="<? echo $this->GetEditAreaId($arResult['SECTION']['ID']); ?>"
                        ><a href="<? echo $arResult['SECTION']['SECTION_PAGE_URL']; ?>"><?
                            echo (
                            isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
                                ? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
                                : $arResult['SECTION']['NAME']
                            );
                            ?>
							<?=$_SESSION["SOTBIT_REGIONS"]["UF_IN_CITY"]?>
                        </a></h1>

                        <div class="sort-container">
                            <div class="sort-panel">
                                <?$APPLICATION->IncludeComponent(
	"codeblogpro:sort.panel", 
	"sortpanel", 
	array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"FIELDS_CODE" => array(
			0 => "sort",
		),
		"IBLOCK_ID" => "4",
		"IBLOCK_TYPE" => "catalog",
		"INCLUDE_SORT_TO_SESSION" => "Y",
		"ORDER_NAME" => "ORDER",
		"PRICE_CODE" => array(
			0 => "1",
		),
		"PROPERTY_CODE" => array(
		),
		"SORT_NAME" => "SORT",
		"SORT_ORDER" => array(
			0 => "asc",
			1 => "desc",
		),
		"COMPONENT_TEMPLATE" => "sortpanel",
		"COMPOSITE_FRAME_MODE" => "A",
		"COMPOSITE_FRAME_TYPE" => "AUTO"
	),
	false
);?>
                            </div>

                            <div class="sort-view">
                                <div class="view-mode">
                                    <span>Вид:</span>
                                    <div class="mode tile clearfix">
                                        <div class="block"></div>
                                        <div class="block"></div>
                                        <div class="block"></div>
                                        <div class="block"></div>
                                    </div>

                                    <div class="mode active list">
                                        <div class="block"></div>
                                        <div class="block"></div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <?
                    }
                    if (0 < $arResult["SECTIONS_COUNT"])
                    {
                        ?>
                        <ul class="<? echo $arCurView['LIST']; ?> catalog-page-tile clearfix">
                            <?
                            switch ($arParams['VIEW_MODE'])
                            {
                            case 'LINE':
                                foreach ($arResult['SECTIONS'] as &$arSection)
                                {
                                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                                    if (false === $arSection['PICTURE'])
                                        $arSection['PICTURE'] = array(
                                            'SRC' => $arCurView['EMPTY_IMG'],
                                            'ALT' => (
                                            '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                                ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                                : $arSection["NAME"]
                                            ),
                                            'TITLE' => (
                                            '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                                ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                                : $arSection["NAME"]
                                            )
                                        );
                                    ?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                                    <a
                                            href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
                                            class="bx_catalog_line_img"
                                            style="background-image: url('<? echo $arSection['PICTURE']['SRC']; ?>');"
                                            title="<? echo $arSection['PICTURE']['TITLE']; ?>"
                                    ></a>
                                    <h2 class="bx_catalog_line_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
                                        if ($arParams["COUNT_ELEMENTS"])
                                        {
                                            ?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
                                        }
                                        ?></h2><?
                                    if ('' != $arSection['DESCRIPTION'])
                                    {
                                        ?><p class="bx_catalog_line_description"><? echo $arSection['DESCRIPTION']; ?></p><?
                                    }
                                    ?><div style="clear: both;"></div>
                                    </li><?
                                }
                                unset($arSection);
                                break;
                            case 'TEXT':
                                foreach ($arResult['SECTIONS'] as &$arSection)
                                {
                                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                                    ?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"><h2 class="bx_catalog_text_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
                                        if ($arParams["COUNT_ELEMENTS"])
                                        {
                                            ?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
                                        }
                                        ?></h2></li><?
                                }
                                unset($arSection);
                                break;
                            case 'TILE':
                                foreach ($arResult['SECTIONS'] as &$arSection)
                                {
                                    $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                                    $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                                    $is_for_display = "false";
                                    $temp = "";
                                    $rsResult = CIBlockSection::GetList(array("SORT"=>"ASC"), array("IBLOCK_ID"=>4, "ID"=>$arSection['ID']), false, array("UF_IS_FOR_DISPLAY"));
                                    while($ar_res = $rsResult->GetNext())
                                    {
                                        if ($ar_res['UF_IS_FOR_DISPLAY'] == "" || $ar_res['UF_IS_FOR_DISPLAY'] == 1) {
                                            $is_for_display = "true";
                                        }
                                        $temp = $ar_res['UF_IS_FOR_DISPLAY'];
                                    }

                                    if ($is_for_display == "false") {
                                        continue;
                                    }

                                    if (false === $arSection['PICTURE'])
                                        $arSection['PICTURE'] = array(
                                            'SRC' => $arCurView['EMPTY_IMG'],
                                            'ALT' => (
                                            '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                                ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
                                                : $arSection["NAME"]
                                            ),
                                            'TITLE' => (
                                            '' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                                ? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
                                                : $arSection["NAME"]
                                            )
                                        );
                                    ?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
                                    <?php // echo "1-".$is_for_display; ?>
                                    <?php //echo "2-".$temp; ?>
                                <a
                                        href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
                                        class="bx_catalog_tile_img"
                                        style="background-image:url('<? echo $arSection['PICTURE']['SRC']; ?>');"
                                        title="<? echo $arSection['PICTURE']['TITLE']; ?>"
                                > </a><?
                                    if ('Y' != $arParams['HIDE_SECTION_NAME'])
                                    {
                                        ?><h2 class="bx_catalog_tile_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
                                        if ($arParams["COUNT_ELEMENTS"])
                                        {
                                            ?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
                                        }
                                        ?></h2><?
                                    }
                                    ?></li><?
                                }
                                unset($arSection);
                                break;
                            case 'LIST':
                            $intCurrentDepth = 1;
                            $boolFirst = true;
                            foreach ($arResult['SECTIONS'] as &$arSection)
                            {
                            $this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
                            $this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

                            if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL'])
                            {
                                if (0 < $intCurrentDepth)
                                    echo "\n",str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']),'<ul>';
                            }
                            elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL'])
                            {
                                if (!$boolFirst)
                                    echo '</li>';
                            }
                            else
                            {
                                while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL'])
                                {
                                    echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
                                    $intCurrentDepth--;
                                }
                                echo str_repeat("\t", $intCurrentDepth-1),'</li>';
                            }

                            echo (!$boolFirst ? "\n" : ''),str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);
                            ?><li id="<?=$this->GetEditAreaId($arSection['ID']);?>"><h2 class="bx_sitemap_li_title"><a href="<? echo $arSection["SECTION_PAGE_URL"]; ?>"><? echo $arSection["NAME"];?><?
                                        if ($arParams["COUNT_ELEMENTS"])
                                        {
                                            ?> <span>(<? echo $arSection["ELEMENT_CNT"]; ?>)</span><?
                                        }
                                        ?></a></h2><?

                                $intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
                                $boolFirst = false;
                                }
                                unset($arSection);
                                while ($intCurrentDepth > 1)
                                {
                                    echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
                                    $intCurrentDepth--;
                                }
                                if ($intCurrentDepth > 0)
                                {
                                    echo '</li>',"\n";
                                }
                                break;
                                }
                                ?>
                        </ul>
                        <?
                        echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
                    }
                    ?>
                </div>
            </div>
        </div>


        <script>
            // $(document).ready(function() {
            //     $('#priceList').DataTable( {
            //         dom: 'Bfrtip',
            //         "searching": false,
            //         buttons: [
            //             {
            //                 extend: 'excelHtml5',
            //                 orientation: 'landscape',
            //                 pageSize: 'LEGAL',
            //                 filename: 'Прайс-лист <?=$arResult['SECTION']['NAME']?>'
            //             },
            //             {
            //                 extend: 'pdfHtml5',
            //                 orientation: 'landscape',
            //                 pageSize: 'LEGAL',
            //                 customize: function(doc) {
            //                     doc.styles.tableBodyEven.alignment = 'center';
            //                     doc.styles.tableBodyOdd.alignment = 'center';
            //                 },
            //                 filename: 'Прайс-лист <?=$arResult['SECTION']['NAME']?>'
            //             }
            //         ]
            //     } );

            //     $('.dt-buttons').prepend('<span>Скачать прайс-лист: </span>');
            // } );
        </script>