<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

include 'simpleimage.php';


use \Bitrix\Main\Localization\Loc;
//$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */


// 'OFFERS' => $arResult['JS_OFFERS'],
// 'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
// 'TREE_PROPS' => $skuProps,



//echo '<pre>';
//    print_r($arCurSection);
//echo '</pre>';

use \Bitrix\Main\Page\Asset;


Asset::getInstance()->addCss('/3d/css/styles.css');

Asset::getInstance()->addJs('/3d/js/models.js');
Asset::getInstance()->addJs('/3d/js/attributes.js');
Asset::getInstance()->addJs('/3d/js/app.js');

$base_page = explode("?", $arParams['CURRENT_BASE_PAGE'])[0];

$APPLICATION->AddHeadString('<link rel="canonical" href="https://' . $_SERVER[ 'SERVER_NAME' ] . '' . $base_page . '"/>',true);

require_once $_SERVER['DOCUMENT_ROOT']."/local/templates/tulpar_store/includes/lib/Mobile_Detect.php";
$detect = new Mobile_Detect();
$isMobile = $detect->isMobile();

$poddon_qty_array = array();

foreach ($arResult['JS_OFFERS'] as $offer) {
    foreach ($offer['DISPLAY_PROPERTIES'] as $property) {
        if ($property['CODE'] == "ATT_BRICKS_PODDIN_QTY_SKU") {
            array_push($poddon_qty_array, $property['VALUE']);
        }
    }
}


$youtubeImage = CFile::GetPath($arResult['PROPERTIES']['ATT_VIDEO_COVER']['VALUE']);
$youtubeFactoryImage = CFile::GetPath($arResult['PROPERTIES']['ATT_ZAVOD_VIDEO_COVER']['VALUE']);

$video_count = 0;

if ($arResult['PROPERTIES']['ATT_YOUTUBE_ID']['VALUE']) {
    $video_count++;
}

if ($arResult['PROPERTIES']['ATT_ZAVOD_YOUTUBE_ID']['VALUE']) {
    $video_count++;
}

if ($arResult['PROPERTIES']['ATT_YOUTUBE_ID']['VALUE']) {

    $new_element = array(
        'ID' => '06092015',
        'SRC' => $youtubeImage,
        'WIDTH' => '1000',
        'HEIGHT' => '1000'
    );

    $zavod_new_element = array(
        'ID' => '05052013',
        'SRC' => $youtubeFactoryImage,
        'WIDTH' => '1000',
        'HEIGHT' => '1000'
    );

    foreach ($arResult['JS_OFFERS'] as $key => $offer) {
        $cur_el_size = $arResult['JS_OFFERS'][$key]['SLIDER_COUNT'];
        $arResult['JS_OFFERS'][$key]['SLIDER_COUNT'] = $cur_el_size + 1;
        $arResult['JS_OFFERS'][$key]['SLIDER'][$cur_el_size] = $new_element;

        if ($arResult['PROPERTIES']['ATT_ZAVOD_YOUTUBE_ID']['VALUE']) {
            $arResult['JS_OFFERS'][$key]['SLIDER'][$cur_el_size + 1] = $zavod_new_element;
            $arResult['JS_OFFERS'][$key]['SLIDER_COUNT']++;
        }
    }

    foreach ($arResult['OFFERS'] as $key => $offer) {
        $cur_el_size = sizeof($arResult['OFFERS'][$key]['MORE_PHOTO']);
        $arResult['OFFERS'][$key]['MORE_PHOTO'][$cur_el_size] = $new_element;

        if ($arResult['PROPERTIES']['ATT_ZAVOD_YOUTUBE_ID']['VALUE']) {
            $arResult['OFFERS'][$key]['MORE_PHOTO'][$cur_el_size + 1] = $zavod_new_element;
        }
    }
}

$this->setFrameMode(true); ?>
<?$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
    $templateLibrary[] = 'currency';
    $currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
    'TEMPLATE_LIBRARY' => $templateLibrary,
    'CURRENCIES' => $currencyList,
    'ITEM' => array(
        'ID' => $arResult['ID'],
        'IBLOCK_ID' => $arResult['IBLOCK_ID'],
        'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
        'JS_OFFERS' => $arResult['JS_OFFERS']
    )
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
    'ID' => $mainId,
    'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
    'STICKER_ID' => $mainId.'_sticker',
    'BIG_SLIDER_ID' => $mainId.'_big_slider',
    'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
    'SLIDER_CONT_ID' => $mainId.'_slider_cont',
    'OLD_PRICE_ID' => $mainId.'_old_price',
    'PRICE_ID' => $mainId.'_price',
    'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
    'PRICE_TOTAL' => $mainId.'_price_total',
    'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
    'QUANTITY_ID' => $mainId.'_quantity',
    'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
    'QUANTITY_UP_ID' => $mainId.'_quant_up',
    'QUANTITY_MEASURE' => $mainId.'_quant_measure',
    'QUANTITY_LIMIT' => $mainId.'_quant_limit',
    'BUY_LINK' => $mainId.'_buy_link',
    'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
    'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
    'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
    'COMPARE_LINK' => $mainId.'_compare_link',
    'TREE_ID' => $mainId.'_skudiv',
    'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
    'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
    'OFFER_GROUP' => $mainId.'_set_group_',
    'BASKET_PROP_DIV' => $mainId.'_basket_prop',
    'SUBSCRIBE_LINK' => $mainId.'_subscribe',
    'TABS_ID' => $mainId.'_tabs',
    'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
    'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
    'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
    : $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
    : $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
    ? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
    : $arResult['NAME'];

if ($_GET['OFFER_ID']) {
    $fullName = $arResult['SIMPLIFIED_OFFERS'][$_GET['OFFER_ID']];
    $offerName = str_replace($name, '', $fullName);

    if ($_GET['OFFER_ID'] == 7095) {
        $offerName .= " ШБ 5 ";
    } elseif ($_GET['OFFER_ID'] == 7096) {
        $offerName .= " ШБ 8 ";
    }
}

//echo "<pre>";
//print_r($arResult['SIMPLIFIED_OFFERS']);
//echo "</pre>";

$APPLICATION->SetPageProperty('title', $name." ".$offerName.' купить в Казани - Тулпар.');
$APPLICATION->SetPageProperty('description', $name." ".$offerName.' в интернет-магазине Тулпар! Характеристики, описание, различные варианты оплаты, доставка на объект');


$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
    $actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
        ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
        : reset($arResult['OFFERS']);
    $showSliderControls = false;

    foreach ($arResult['OFFERS'] as $offer)
    {
        if ($offer['MORE_PHOTO_COUNT'] > 1)
        {
            $showSliderControls = true;
            break;
        }
    }
}
else
{
    $actualItem = $arResult;
    $showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];

// echo "<pre style='display: none;'>";
// print_r($actualItem);
// echo "</pre>";


$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
    'left' => 'product-item-label-left',
    'center' => 'product-item-label-center',
    'right' => 'product-item-label-right',
    'bottom' => 'product-item-label-bottom',
    'middle' => 'product-item-label-middle',
    'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
    foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
    {
        $discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
    }
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
    foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
    {
        $labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
    }
}
?>

<?
//проверка если элемент является плитой перекрытия
if (in_array($arResult['ID'], [11808,20494, 20495,20496])):

    ?>

    <?
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_ATT_MAKER",
        "PREVIEW_PICTURE", "catalog_GROUP_1", "PROPERTY_ATT_ZHB_PLATE_WIDTH_MM",
        "PROPERTY_ATT_ZHB_PLATE_LENGTH_MM", "PROPERTY_ATT_ZHB_PLATE_HEIGHT_MM",
        "PROPERTY_ATT_ZHB_PLATE_WEIGHT_KG",
        "PROPERTY_ATT_ZHB_PLATE_VOLUME_M3",
        "PROPERTY_ATT_ZHB_PLATE_HOLE_SIZE_MM",
        "PROPERTY_ATT_ZHB_PLATE_PRESSURE_KGS_M2",
        "PROPERTY_ATT_ZHB_PLATE_QTY_IN_TRUCK",
        "PROPERTY_ATT_MAIN_PLITY",
        "PROPERTY_MORE_PHOTO",
    );
    $arFilter = Array("IBLOCK_ID"=> 4, "SECTION_ID" => 906, "ACTIVE_DATE"=>"Y", "INCLUDE_SUBSECTIONS" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "PROPERTY_ATT_MAIN_PLITY" => $arResult['ID'], "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
    $variationsArray = array();
    $imagesArr = array();
    $imagesIdOnlyArr = array();
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();


        if ($arFields["PREVIEW_PICTURE"]) {
            $imagesArr[$arFields["ID"]]["IMAGE_ID"][] = $arFields["PREVIEW_PICTURE"];
            $imagesIdOnlyArr[] = $arFields["PREVIEW_PICTURE"];
        }

        if ($arFields["PROPERTY_MORE_PHOTO_VALUE"]) {
            $imagesArr[$arFields["ID"]]["IMAGE_ID"][] = $arFields["PROPERTY_MORE_PHOTO_VALUE"];
            $imagesIdOnlyArr[] = $arFields["PROPERTY_MORE_PHOTO_VALUE"];
        }

        $variationsArray[] = array("ID" => $arFields["ID"],
            "MAKER" => $arFields["PROPERTY_ATT_MAKER_VALUE"],
            "MAIN_PLITY" => $arFields["PROPERTY_ATT_MAIN_PLITY_VALUE"],
            "NAME" => $arFields["NAME"]." ".$arFields["PROPERTY_ATT_ZHB_PLATE_HEIGHT_MM_VALUE"]." ".$arFields["PROPERTY_ATT_ZHB_PLATE_WIDTH_MM_VALUE"]." ".$arFields["PROPERTY_ATT_ZHB_PLATE_LENGTH_MM_VALUE"],
            "WIDTH" => $arFields["PROPERTY_ATT_ZHB_PLATE_WIDTH_MM_VALUE"],
            "LENGTH" =>  $arFields["PROPERTY_ATT_ZHB_PLATE_LENGTH_MM_VALUE"],
            "HEIGHT" =>  $arFields["PROPERTY_ATT_ZHB_PLATE_HEIGHT_MM_VALUE"],
            "WEIGHT" => $arFields["PROPERTY_ATT_ZHB_PLATE_WEIGHT_KG_VALUE"],
            "VOLUME" => $arFields["PROPERTY_ATT_ZHB_PLATE_VOLUME_M3_VALUE"],
            "HOLE_SIZE" => $arFields["PROPERTY_ATT_ZHB_PLATE_HOLE_SIZE_MM_VALUE"],
            "PRESSURE" => $arFields["PROPERTY_ATT_ZHB_PLATE_PRESSURE_KGS_M2_VALUE"],
            "IN_TRUCK_QNTY" => $arFields["PROPERTY_ATT_ZHB_PLATE_QTY_IN_TRUCK_VALUE"],
            "PRICE" => $arFields["CATALOG_PRICE_1"]
        );

    }

    $APPLICATION->SetPageProperty('title', $name." 220 ".$_GET['width']." ".$_GET['length'].' купить '.$_SESSION["SOTBIT_REGIONS"]["UF_IN_CITY"].' - цены от 7,10 руб в Тулпар.');
    $APPLICATION->SetPageProperty('description', $name." 220 ".$_GET['width']." ".$_GET['length'].' в интернет-магазине Тулпар! Характеристики, описание, различные варианты оплаты, доставка на объект');


    $res = CFile::GetList(array("FILE_SIZE"=>"desc"), array("@ID"=> $imagesIdOnlyArr));
    $imagesPathArr = array();
    while($res_arr = $res->GetNext()) {
        $imagesPathArr[$res_arr["ID"]] = $res_arr["SUBDIR"]."/".$res_arr["FILE_NAME"];
    }

    foreach($imagesArr as $key => $image) {
        if ($imagesPathArr[$image["IMAGE_ID"]]) {
            $imagesArr[$key]["IMAGE_PATH"] = "/upload/".$imagesPathArr[$image["IMAGE_ID"]];
        }

        foreach ($image["IMAGE_ID"] as $imageID) {
            if ($imagesPathArr[$imageID]) {
                $imagesArr[$key]["IMAGE_PATH"][$imageID] = "/upload/".$imagesPathArr[$imageID];
            }
        }
    } ?>


    <script>
        let variationsArr = [
            <?foreach ($variationsArray as $variation):

            $elementImages = '[';
            $elementImagesIDs = '[';
            foreach ($imagesArr[$variation["ID"]]["IMAGE_PATH"] as $key => $imageSrc) {
                $elementImages .= '"'.$imageSrc.'",';
                $elementImagesIDs .= '"'.$key.'",';
            }

            $elementImages .= "]";
            $elementImagesIDs .= "]";
            ?>
            {
                id: "<?=$variation['ID']?>",
                name: "<?=$variation['NAME']?>",
                image: <?=$elementImages?>,
                imageIDs: <?=$elementImagesIDs?>,
                width: "<?=$variation['WIDTH']?>",
                length: "<?=$variation['LENGTH']?>",
                height: "<?=$variation['HEIGHT']?>",
                weight: "<?=$variation['WEIGHT']?>",
                volume: "<?=$variation['VOLUME']?>",
                holeSize: "<?=$variation['HOLE_SIZE']?>",
                pressure: "<?=$variation['PRESSURE']?>",
                inTruck: "<?=$variation['IN_TRUCK_QNTY']?>",
                price: "<?=$variation['PRICE']?>"
            },
            <?endforeach;?>
        ]

        $(document).ready(function () {

            <?if ($_GET["width"] && $_GET["id"]):?>
            $(".product-item-detail-slider-image.active").removeClass("active");
            $(".product-item-detail-slider-image.activated").addClass("active");
            <?endif;?>

            let width;
            let height;
            <?if($arResult['ID'] == 11808):?>
            width = 1200;
            height = 220;

            if ($('#form-width-input').is(':checked')) {
                width = 1500;
            }
            <?elseif($arResult['ID'] == 20494):?>
            height = 220;
            width = $('#width-select-js').val();

            <?elseif($arResult['ID'] == 20495):?>
            width = 1198;
            height = 160;

            if ($('#form-height-input').is(':checked')) {
                width = 220;
            }
            <?elseif($arResult['ID'] == 20496):?>
            width = 990;
            height = 220;

            if ($('#form-width-input').is(':checked')) {
                width = 1190;
            }
            <?endif;?>

            let length = $('.show input[type="range"]').val();

            if (<?=$arResult['ID']?> == 20496) {
                if ($('.range-slider-kamges.show input[type="range"]').hasClass('kamges-990')) {
                    switch (length) {
                        case "100":
                            length = 2380;
                            break;
                        case "200":
                            length = 2980;
                            break;
                        case "300":
                            length = 4780;
                            break;
                        case "400":
                            length = 5080;
                            break;
                        case "500":
                            length = 5680;
                            break;
                        case "600":
                            length = 5980;
                            break;
                        case "700":
                            length = 6280;
                            break;
                    }
                } else if ($('.range-slider-kamges.show input[type="range"]').hasClass('kamges-1190')) {
                    switch (length) {
                        case "100":
                            length = 2380;
                            break;
                        case "200":
                            length = 2680;
                            break;
                        case "300":
                            length = 2980;
                            break;
                        case "400":
                            length = 3580;
                            break;
                        case "500":
                            length = 4180;
                            break;
                        case "600":
                            length = 4480;
                            break;
                        case "700":
                            length = 4780;
                            break;
                        case "800":
                            length = 5080;
                            break;
                        case "900":
                            length = 5380;
                            break;
                        case "1000":
                            length = 5680;
                            break;
                        case "1100":
                            length = 5980;
                            break;
                        case "1200":
                            length = 6280;
                            break;
                        case "1300":
                            length = 7180;
                            break;
                    }
                }
            }

            for (let i = 0; i < variationsArr.length; i++) {
                if (variationsArr[i].width == width && variationsArr[i].length == length && variationsArr[i].height == height) {
                    $("h1.element-name").text("<?echo $name;?>"+ " " + variationsArr[i].height+" " +" <?echo $_GET['width']." ".$_GET['length'];?>");
                }
            }

            $('#form-width-input').on('change', function () {
                <?if ($arResult['ID'] == 20496):?>
                $('.range-slider-kamges').toggleClass('show');
                <?endif;?>

                zhbiShowProps(<?=intval($arResult["PROPERTIES"]["ATT_SHIPPING_COST"]["VALUE"])?>, <?=$arResult['ID']?>);
            });

            $('#form-height-input').on('change', function () {
                zhbiShowProps(<?=intval($arResult["PROPERTIES"]["ATT_SHIPPING_COST"]["VALUE"])?>, <?=$arResult['ID']?>);
            });

            $('#width-select-js').on('change', function () {
                zhbiShowProps(<?=intval($arResult["PROPERTIES"]["ATT_SHIPPING_COST"]["VALUE"])?>, <?=$arResult['ID']?>);
            });

            $('#metrazh-select-js').on('change', function () {
                $('.range-slider').hide();
                $('.range-slider').removeClass('show');

                $('[data-metrazh="'+ $(this).val() +'"]').show();
                $('[data-metrazh="'+ $(this).val() +'"]').addClass('show');

                zhbiShowProps(<?=intval($arResult["PROPERTIES"]["ATT_SHIPPING_COST"]["VALUE"])?>, <?=$arResult['ID']?>);
            });

            $('input[type="range"]').on('change', function () {
                zhbiShowProps(<?=intval($arResult["PROPERTIES"]["ATT_SHIPPING_COST"]["VALUE"])?>, <?=$arResult['ID']?>);
            });

            $("body").on('click', ".product-item-detail-slider-right", function () {
                let activeControlSlide = $('.product-item-detail-slider-controls-image.active');
                let next = $(activeControlSlide).next();

                let imageId = $(next).attr("data-value");

                let activeMainPic = $('.product-item-detail-slider-image.active');
                let nextMainPic = $('.product-item-detail-slider-image[data-id='+ imageId +']');

                if ($(next).is(':visible')) {
                    $(activeControlSlide).removeClass("active");
                    $(next).addClass("active");

                    $(activeMainPic).removeClass("active");
                    $(nextMainPic).addClass("active");
                }

            });

            $("body").on('click', ".product-item-detail-slider-left", function () {
                let activeControlSlide = $('.product-item-detail-slider-controls-image.active');
                let prev = $(activeControlSlide).prev();

                let imageId = $(prev).attr("data-value");

                let activeMainPic = $('.product-item-detail-slider-image.active');
                let prevMainPic = $('.product-item-detail-slider-image[data-id='+ imageId +']');

                if ($(prev).is(':visible')) {
                    $(activeControlSlide).removeClass("active");
                    $(prev).addClass("active");

                    $(activeMainPic).removeClass("active");
                    $(prevMainPic).addClass("active");
                }

            });


        });
    </script>

<?endif;

//    echo "<pre>";
//        print_r($offerName);
//    echo "</pre>";

// echo "<pre>";
// print_r($arResult['SKU_PROPS']);
// echo "</pre>";

// echo "<pre>";
// print_r($arResult['OFFERS'][0]['PROPERTIES']);
// echo "</pre>";

?>

<?
$this->AddEditAction($arResult['ID'], $arResult['EDIT_LINK'], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_EDIT"));
$this->AddDeleteAction($arResult['ID'], $arResult['DELETE_LINK'], CIBlock::GetArrayByID($arResult["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
?>


    <div class="bx-catalog-element bx-<?=$arParams['TEMPLATE_THEME']?>" id="<?=$itemIds['ID']?>"
         itemscope itemtype="http://schema.org/Product">

        <div class="container-fluid p-sm-0">
            <div class="row itemimage-and-price">
                <div class="col-lg-6 pl-0">
                    <div class="product-item-detail-slider-container" id="<?=$itemIds['BIG_SLIDER_ID']?>">
                        <span class="product-item-detail-slider-download"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/download.svg" alt=""></span>
                        <span class="product-item-detail-slider-close" data-entity="close-popup"></span>
                        <div class="product-item-detail-slider-block
						<?=($arParams['IMAGE_RESOLUTION'] === '1by1' ? 'product-item-detail-slider-block-square' : '')?>"
                             data-entity="images-slider-block">

                            <span class="product-item-detail-slider-left" <?if(!in_array($arResult['ID'], [11808, 20494, 20495, 20496])) echo 'data-entity="slider-control-left" style="display: none;"';?>></span>
                            <span class="product-item-detail-slider-right" <?if(!in_array($arResult['ID'], [11808, 20494, 20495, 20496])) echo 'data-entity="slider-control-right" style="display: none;"';?>></span>
                            <div class="product-item-label-text <?=$labelPositionClass?>" id="<?=$itemIds['STICKER_ID']?>"
                                <?=(!$arResult['LABEL'] ? 'style="display: none;"' : '' )?>>
                                <?
                                if ($arResult['LABEL'] && !empty($arResult['LABEL_ARRAY_VALUE']))
                                {
                                    foreach ($arResult['LABEL_ARRAY_VALUE'] as $code => $value)
                                    {
                                        ?>
                                        <div<?=(!isset($arParams['LABEL_PROP_MOBILE'][$code]) ? ' class="hidden-xs"' : '')?>>
                                            <span title="<?=$value?>"><?=$value?></span>
                                        </div>
                                        <?
                                    }
                                }
                                ?>
                            </div>
                            <?
                            if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
                            {
                                if ($haveOffers)
                                {
                                    ?>
                                    <div class="product-item-label-ring <?=$discountPositionClass?>" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>"
                                         style="display: none;">
                                    </div>
                                    <?
                                }
                                else
                                {
                                    if ($price['DISCOUNT'] > 0)
                                    {
                                        ?>
                                        <div class="product-item-label-ring <?=$discountPositionClass?>" id="<?=$itemIds['DISCOUNT_PERCENT_ID']?>"
                                             title="<?=-$price['PERCENT']?>%">
                                            <span><?=-$price['PERCENT']?>%</span>
                                        </div>
                                        <?
                                    }
                                }
                            }
                            ?>

                            <?
                            /* global $USER;
                            if ($USER->IsAdmin()):
                            ?>
                            <?echo '<pre>'; print_r($arResult['OFFERS']); echo '</pre>';?>

                            <?endif; */?>
                            <div class="product-item-detail-slider-images-container" data-entity="images-container">
                                <?
                                if (!empty($actualItem['MORE_PHOTO']))
                                {
                                    $counter = 0;

                                    foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                                    {
                                        if (!$_GET["width"] && !$_GET["id"] && !$_GET["length"]) {
                                            ?>

                                            <div class="product-item-detail-slider-image<?= ($key == 0 ? ' active' : '') ?>"
                                                 data-entity="image" data-id="<?= $photo['ID'] ?>">
                                                <img for-youtube="no" class='lazy' data-src="<?= $photo['SRC'] ?>"
                                                     data-srcset='<?= $photo['SRC'] ?>' alt="<?= $alt ?> - Фото №<?php echo $key+1; ?>"
                                                     title="<?= $title ?> - Фото №<?php echo $key+1; ?>"<?= ($key == 0 ? ' itemprop="image"' : '') ?>>
                                            </div>
                                            <?
                                        } elseif($_GET["width"] && $_GET["id"] && $_GET["length"]) {
                                            ?>

                                            <?
                                            $active = false;

                                            if ($photo["ELEMENT_ID"] == $_GET["id"] && $counter === 0) {
                                                $active = true;
                                                $counter++;
                                            }
                                            ?>

                                            <div class="product-item-detail-slider-image<?if($active) echo " activated";?>"
                                                 data-entity="image" data-id="<?= $photo['ID'] ?>">
                                                <img for-youtube="no" class='lazy' data-src="<?= $photo['SRC'] ?>"
                                                     data-srcset='<?= $photo['SRC'] ?>' alt="<?= $alt ?> - Фото №<?php echo $key+1; ?>"
                                                     title="<?= $title ?> - Фото №<?php echo $key+1; ?>"<?= ($key == 0 ? ' itemprop="image"' : '') ?>>
                                            </div>
                                            <?
                                        }
                                    }
                                }


                                if ($arParams['SLIDER_PROGRESS'] === 'Y')
                                {
                                    ?>
                                    <div class="product-item-detail-slider-progress-bar" data-entity="slider-progress-bar" style="width: 0;"></div>
                                    <?
                                }
                                ?>
                            </div>
                        </div>
                        <?
                        if ($showSliderControls)
                        {
                            if ($haveOffers)
                            {
                                foreach ($arResult['OFFERS'] as $keyOffer => $offer)
                                {
                                    if (!isset($offer['MORE_PHOTO_COUNT']) || $offer['MORE_PHOTO_COUNT'] <= 0)
                                        continue;

                                    $strVisible = $arResult['OFFERS_SELECTED'] == $keyOffer ? '' : 'none';
                                    ?>
                                    <noindex>
                                        <div class="product-item-detail-slider-controls-block" id="<?=$itemIds['SLIDER_CONT_OF_ID'].$offer['ID']?>" style="display: <?=$strVisible?>;">
                                            <?

                                            foreach ($offer['MORE_PHOTO'] as $keyPhoto => $photo)
                                            {

                                                ?>
                                                <div class="product-item-detail-slider-controls-image<?=($keyPhoto == 0 ? ' active' : '')?>"
                                                     data-entity="slider-control" data-value="<?=$offer['ID'].'_'.$photo['ID']?>">
                                                    <?php if ($keyOffer == 0): ?>
                                                        <img class="lazy" data-src="<?=$photo['SRC']?>" data-srcset="<?=$photo['SRC']?>">
                                                    <?php else: ?>
                                                        <img class="lazy" src="<?=SITE_TEMPLATE_PATH?>/assets/img/blurred_brick.jpg" data-src="<?=$photo['SRC']?>" data-srcset="<?=$photo['SRC']?> 1x, <?=$photo['SRC']?> 2x">
                                                    <?php endif; ?>
                                                </div>
                                                <?
                                            }
                                            ?>
                                        </div>
                                    </noindex>
                                    <?
                                }
                            }
                            else
                            {
                                ?>
                                <div class="product-item-detail-slider-controls-block" id="<?=$itemIds['SLIDER_CONT_ID']?>">
                                    <?
                                    if (!empty($actualItem['MORE_PHOTO']))
                                    {
                                        $counter = 0;
                                        foreach ($actualItem['MORE_PHOTO'] as $key => $photo)
                                        {
                                            $hide = false;
                                            if(!$_GET["id"]) {
                                                if($photo["ELEMENT_ID"] != $arResult["ID"])
                                                    $hide = true;
                                            } else {
                                                if ($photo["ELEMENT_ID"] != $_GET["id"]) {
                                                    $hide = true;
                                                }
                                            }

                                            $active = false;

                                            if ($_GET["width"] && $_GET["id"]) {
                                                if (!$hide && $counter === 0) {
                                                    $active = true;
                                                    $counter++;
                                                }
                                            }

                                            ?>
                                            <div class="product-item-detail-slider-controls-image<?if($active) echo " active";?>"
                                                 data-entity="slider-control" data-element-id="<?=$photo["ELEMENT_ID"]?>" <?if($hide) echo 'style="display:none;"';?> data-value="<?=$photo['ID']?>">
                                                <img class="lazy" data-src="<?=$photo['SRC']?>" data-srcset='<?=$photo['SRC']?>'>
                                            </div>
                                            <?
                                        }
                                    }
                                    ?>

                                </div>
                                <?
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="col-lg-6 pr-0">
                    <h1 class="element-name"><?=$name." ".$offerName;?></h1>
                    <div class="element-meta">
                        <div class="maker d-inline-block">Производитель: <span><?=$arResult['PROPERTIES']['ATT_MAKER']['VALUE']?></span></div>
                        <div class="maker artikul d-inline-block ml-4">Код товара: <span>100<?=$actualItem['ID']?></span></div>
                        <div class="instock d-inline-block ml-4">В наличии</div>
                    </div>

                    <?php if ( $arResult['PROPERTIES']['TAGS']['VALUE'] ): ?>
                    <div class="products-tags no-absolute" style="margin: 5px 0 10px 0;">
                        <?php foreach ( $arResult['PROPERTIES']['TAGS']['VALUE'] as $arTagKey => $arTag ): ?>
                        <span class="products-tag color-<?=$arResult['PROPERTIES']['TAGS']['VALUE_XML_ID'][$arTagKey]?>"><?php echo $arTag; ?></span>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>

                    <div class="row element-info-box">
                        <div class="col-12">

                            <?if(in_array($arResult['ID'], [11808,20494, 20495, 20496])):?>

                                <?if($arResult['ID'] == 11808):?>

                                    <div class="form-group__input-title">Высота, мм</div>
                                    <p class="form-group__value">220</p>

                                    <div class="form-group" id="plate-width">
                                        <div class="form-group__input-title">Ширина, мм</div>
                                        <div class="checkbox-pivot">
                                            <label for="form-width-input" class="checkbox--label min_value">1200</label>
                                            <input type="checkbox" class="width-checkbox checkbox-width-input" name="width" id="form-width-input" <?if ($_GET["width"] == "1500") echo "checked";?>>

                                            <label for="form-width-input" class="checkbox--btn"></label>
                                            <label for="form-width-input" class="checkbox--label max_value">1500</label>
                                        </div>
                                    </div>

                                <?elseif ($arResult['ID'] == 20494):?>

                                    <div class="form-group__input-title">Высота, мм</div>
                                    <p class="form-group__value">220</p>

                                    <div class="form-group" id="plate-width">
                                        <div class="form-group__input-title">Ширина, мм</div>
                                        <div class="select-pivot">
                                            <select name="width-select" id="width-select-js">
                                                <option <?if ($_GET["width"] == "990") echo "selected";?> value="990">990</option>
                                                <option <?if ($_GET["width"] == "1190") echo "selected";?> value="1190">1190</option>
                                                <option <?if ($_GET["width"] == "1490") echo "selected";?> value="1490">1490</option>
                                            </select>
                                        </div>
                                    </div>

                                <?elseif ($arResult['ID'] == 20495):?>
                                    <div class="form-group" id="plate-height">
                                        <div class="form-group__input-title">Высота, мм</div>
                                        <div class="checkbox-pivot">
                                            <label for="form-height-input" class="checkbox--label min_value">160</label>
                                            <input type="checkbox" class="width-checkbox checkbox-width-input" name="width" id="form-height-input" <?if ($_GET["height"] == "220") echo "checked";?>>

                                            <label for="form-height-input" class="checkbox--btn"></label>
                                            <label for="form-height-input" class="checkbox--label max_value">220</label>
                                        </div>
                                    </div>

                                    <div class="form-group__input-title">Ширина, мм</div>
                                    <p class="form-group__value">1198</p>

                                <?elseif ($arResult['ID'] == 20496):?>
                                    <div class="form-group__input-title">Высота, мм</div>
                                    <p class="form-group__value">220</p>

                                    <div class="form-group" id="plate-width">
                                        <div class="form-group__input-title">Ширина, мм</div>
                                        <div class="checkbox-pivot">
                                            <label for="form-width-input" class="checkbox--label min_value">990</label>
                                            <input type="checkbox" class="width-checkbox checkbox-width-input" name="width" id="form-width-input" <?if ($_GET["width"] == "1190") echo "checked";?>>

                                            <label for="form-width-input" class="checkbox--btn"></label>
                                            <label for="form-width-input" class="checkbox--label max_value">1190</label>
                                        </div>
                                    </div>
                                <?endif;?>

                                <?
                                if ($_GET["length"]) {
                                    switch (true){
                                        case ($_GET["length"] >= 1500 && $_GET["length"] <= 2000):
                                            $metrazh = 1;
                                            break;
                                        case ($_GET["length"] >= 2400 && $_GET["length"] <= 3000):
                                            $metrazh = 2;
                                            break;

                                        case ($_GET["length"] >= 3000 && $_GET["length"] <= 4000):
                                            $metrazh = 3;
                                            break;

                                        case ($_GET["length"] >= 4000 && $_GET["length"] <= 5000):
                                            $metrazh = 4;
                                            break;

                                        case ($_GET["length"] >= 5000 && $_GET["length"] <= 6000):
                                            $metrazh = 5;
                                            break;

                                        case ($_GET["length"] >= 6000 && $_GET["length"] <= 7000):
                                            $metrazh = 6;
                                            break;

                                        case ($_GET["length"] >= 7000 && $_GET["length"] <= 8000):
                                            $metrazh = 7;
                                            break;

                                        case ($_GET["length"] >= 8000 && $_GET["length"] <= 9000):
                                            $metrazh = 8;
                                            break;

                                        default:
                                            $metrazh = null;
                                            break;
                                    }
                                }

                                ?>

                                <?if (in_array($arResult['ID'], [11808,20494, 20495])):?>
                                    <div class="form-group" id="metrazh">
                                        <div class="form-group__input-title">Метраж, м</div>
                                        <div class="select-pivot">
                                            <select name="metrazh-select" id="metrazh-select-js">
                                                <?if($arResult['ID'] == 11808):?>
                                                    <option <?if ($metrazh == 2) echo "selected";?> value="2">2.4 - 3.0</option>
                                                <?elseif ($arResult['ID'] == 20494):?>
                                                    <option value="2">2.2 - 3.0</option>
                                                <?elseif ($arResult['ID'] == 20495):?>
                                                    <option value="1">1.5 - 2.0</option>
                                                <?endif;?>

                                                <?if($arResult['ID'] == 11808):?>

                                                    <?for($i = 3; $i <= 8; $i++):?>
                                                        <option <?if ($metrazh == $i) echo "selected";?> value="<?=$i?>"><?=$i?>.0 - <?=$i+1?>.0</option>
                                                    <?endfor;?>
                                                <?elseif ($arResult['ID'] == 20494):?>

                                                    <?for($i = 2; $i <= 7; $i++):?>
                                                        <option <?if ($metrazh == $i) echo "selected";?> value="<?=$i?>"><?=$i?>.0 - <?=$i+1?>.0</option>
                                                    <?endfor;?>

                                                <?elseif ($arResult['ID'] == 20495):?>

                                                    <?for($i = 2; $i <= 7; $i++):?>
                                                        <option <?if ($metrazh == $i) echo "selected";?> value="<?=$i?>"><?=$i?>.0 - <?=$i+1?>.0</option>
                                                    <?endfor;?>
                                                <?endif;?>
                                            </select>
                                        </div>
                                    </div>
                                <?endif;?>

                                <div class="form-group" id="plate-width">
                                    <div class="form-group__input-title">Длина, м</div>
                                    <?if($arResult['ID'] == 11808):?>
                                        <div data-metrazh="2" class="range-slider <?if ($metrazh == 2) echo "show";?>">
                                            <div class="range-top">
                                                <span>2.4</span>
                                                <span>2.5</span>
                                                <span>2.6</span>
                                                <span>2.7</span>
                                                <span>2.8</span>
                                                <span>2.9</span>
                                                <span>3.0</span>
                                            </div>

                                            <input type="range" min="2400" max="3000" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo "2400"?>" data-orientation="horizontal">
                                        </div>

                                        <?for ($i = 3; $i <= 8; $i++): ?>
                                            <div data-metrazh="<?=$i?>" class="range-slider <?if ($metrazh == $i) echo "show";?>">
                                                <div class="range-top">
                                                    <?for ($j = 0; $j < 10; $j++):?>
                                                        <span><?=$i?>.<?=$j?></span>
                                                    <?endfor;?>
                                                    <span><?=$i+1?>.0</span>
                                                </div>

                                                <input type="range" min="<?=$i?>000" max="<?=$i+1?>000" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo $i."000"?>" data-orientation="horizontal">
                                            </div>
                                        <?endfor;?>

                                    <?elseif($arResult['ID'] == 20494):?>
                                        <div data-metrazh="2" class="range-slider <?if ($metrazh == 2) echo "show";?>">
                                            <div class="range-top">
                                                <span>2.2</span>
                                                <span>2.3</span>
                                                <span>2.4</span>
                                                <span>2.5</span>
                                                <span>2.6</span>
                                                <span>2.7</span>
                                                <span>2.8</span>
                                                <span>2.9</span>
                                                <span>3.0</span>
                                            </div>

                                            <input type="range" min="2180" max="2980" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo "2180"?>" data-orientation="horizontal">
                                        </div>

                                        <?for ($i = 3; $i <= 6; $i++): ?>
                                            <div data-metrazh="<?=$i?>" class="range-slider <?if ($metrazh == $i) echo "show";?>">
                                                <div class="range-top">
                                                    <?for ($j = 0; $j < 10; $j++):?>
                                                        <span><?=$i?>.<?=$j?></span>
                                                    <?endfor;?>
                                                    <span><?=$i+1?>.0</span>
                                                </div>

                                                <input type="range" min="<?=($i*1000)-20?>" max="<?=($i+1)*1000-20?>" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo $i*1000-20?>" data-orientation="horizontal">
                                            </div>
                                        <?endfor;?>

                                        <div data-metrazh="7" class="range-slider <?if ($metrazh == 7) echo "show";?>">
                                            <div class="range-top">
                                                <span>7.0</span>
                                                <span>7.1</span>
                                                <span>7.2</span>
                                            </div>

                                            <input type="range" min="6980" max="7180" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo "6980"?>" data-orientation="horizontal">
                                        </div>

                                    <?elseif ($arResult['ID'] == 20495):?>
                                        <div data-metrazh="1" class="range-slider <?if ($metrazh == 1) echo "show";?>">
                                            <div class="range-top">
                                                <span>1.5</span>
                                                <span>1.6</span>
                                                <span>1.7</span>
                                                <span>1.8</span>
                                                <span>1.9</span>
                                                <span>2.0</span>
                                            </div>

                                            <input type="range" min="1500" max="2000" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo "1500"?>" data-orientation="horizontal">
                                        </div>

                                        <?for ($i = 2; $i <= 6; $i++): ?>
                                            <div data-metrazh="<?=$i?>" class="range-slider <?if ($metrazh == $i) echo "show";?>">
                                                <div class="range-top">
                                                    <?for ($j = 0; $j < 10; $j++):?>
                                                        <span><?=$i?>.<?=$j?></span>
                                                    <?endfor;?>
                                                    <span><?=$i+1?>.0</span>
                                                </div>

                                                <input type="range" min="<?=$i?>000" max="<?=$i+1?>000" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo $i."000"?>" data-orientation="horizontal">
                                            </div>
                                        <?endfor;?>
                                        <div data-metrazh="7" class="range-slider <?if ($metrazh == 7) echo "show";?>">
                                            <div class="range-top">
                                                <?for ($j = 0; $j < 9; $j++):?>
                                                    <span><?=$i?>.<?=$j?></span>
                                                <?endfor;?>
                                            </div>

                                            <input type="range" min="7000" max="7800" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo "7000"?>" data-orientation="horizontal">
                                        </div>
                                    <?elseif ($arResult['ID'] == 20496):?>
                                        <div class="range-slider range-slider-kamges <?if ($_GET['width'] == "990") echo "show"?>">
                                            <div class="range-top">
                                                <span>2.38</span>
                                                <span>2.98</span>
                                                <span>4.78</span>
                                                <span>5.08</span>
                                                <span>5.68</span>
                                                <span>5.98</span>
                                                <span>6.28</span>
                                            </div>

                                            <input type="range" class="kamges-990" min="100" max="700" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo "2380"?>" data-orientation="horizontal">
                                        </div>

                                        <div class="range-slider range-slider-kamges kamges-1190 <?if ($_GET['width'] == "1190") echo "show"?>">
                                            <div class="range-top">
                                                <span>2.38</span>
                                                <span>2.68</span>
                                                <span>2.98</span>
                                                <span>3.58</span>
                                                <span>4.18</span>
                                                <span>4.48</span>
                                                <span>4.78</span>
                                                <span>5.08</span>
                                                <span>5.38</span>
                                                <span>5.68</span>
                                                <span>5.98</span>
                                                <span>6.28</span>
                                                <span>7.18</span>
                                            </div>

                                            <input type="range" class="kamges-1190" min="100" max="1300" step="100" value="<?if ($_GET["length"]) echo $_GET["length"]; else echo "2380"?>" data-orientation="horizontal">
                                        </div>
                                    <?endif;?>

                                </div>

                            <?endif;?>

                            <div class="product-item-detail-info-section">
                                <?
                                foreach ($arParams['PRODUCT_INFO_BLOCK_ORDER'] as $blockName)
                                {
                                    switch ($blockName)
                                    {
                                        case 'sku':
                                            if ($haveOffers && !empty($arResult['OFFERS_PROP']))
                                            {
                                                ?>
                                                <div id="<?=$itemIds['TREE_ID']?>">
                                                    <?
                                                    foreach ($arResult['SKU_PROPS'] as $skuProperty)
                                                    {
                                                        if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
                                                            continue;

                                                        $propertyId = $skuProperty['ID'];
                                                        $skuProps[] = array(
                                                            'ID' => $propertyId,
                                                            'SHOW_MODE' => $skuProperty['SHOW_MODE'],
                                                            'VALUES' => $skuProperty['VALUES'],
                                                            'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
                                                        );
                                                        ?>

                                                        <?php
                                                        if (($arResult['IBLOCK_SECTION_ID'] != 505 && $arResult['IBLOCK_SECTION_ID'] != 504) && ($skuProperty['CODE'] == "ATT_BRICK_STRENGTH_SKU")) {
                                                            $hide = "hide";
                                                        }
                                                        ?>

                                                        <div class="product-item-detail-info-container <?=$hide?>" data-entity="sku-line-block">
                                                            <div class="product-item-detail-info-container-title"><?=htmlspecialcharsEx($skuProperty['NAME'])?></div>
                                                            <div class="product-item-scu-container">
                                                                <div class="product-item-scu-block">
                                                                    <div class="product-item-scu-list">
                                                                        <ul class="product-item-scu-item-list">
                                                                            <?
                                                                            // echo "<pre>";
                                                                            // print_r($skuProperty['VALUES']);
                                                                            // echo "</pre>";
                                                                            foreach ($skuProperty['VALUES'] as &$value)
                                                                            {
                                                                                $value['NAME'] = htmlspecialcharsbx($value['NAME']);

                                                                                if ($skuProperty['SHOW_MODE'] === 'PICT')
                                                                                {
                                                                                    ?>
                                                                                    <li class="product-item-scu-item-color-container" title="<?=$value['NAME']?>"
                                                                                        data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
                                                                                        data-onevalue="<?=$value['ID']?>"
                                                                                        data-toggle="tooltip"
                                                                                        data-placement="top">
                                                                                        <div class="product-item-scu-item-color-block">
                                                                                            <div class="product-item-scu-item-color" title="<?=$value['NAME']?>"
                                                                                                 style="background-image: url('<?=$value['PICT']['SRC']?>');">
                                                                                            </div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <?
                                                                                }
                                                                                else
                                                                                {
                                                                                    ?>
                                                                                    <li class="product-item-scu-item-text-container" title="<?=$value['NAME']?>"
                                                                                        data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
                                                                                        data-onevalue="<?=$value['ID']?>">
                                                                                        <div class="product-item-scu-item-text-block">
                                                                                            <div class="product-item-scu-item-text"><?=$value['NAME']?></div>
                                                                                        </div>
                                                                                    </li>
                                                                                    <?
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </ul>
                                                                        <div style="clear: both;"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <?
                                                    }
                                                    ?>
                                                </div>
                                                <?
                                            }

                                            break;

                                        case 'props':
                                            if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
                                            {
                                                ?>
                                                <div class="product-item-detail-info-container">
                                                    <?
                                                    if (!empty($arResult['DISPLAY_PROPERTIES']))
                                                    {
                                                        ?>
                                                        <dl class="product-item-detail-properties">
                                                            <?
                                                            foreach ($arResult['DISPLAY_PROPERTIES'] as $property)
                                                            {
                                                                if (isset($arParams['MAIN_BLOCK_PROPERTY_CODE'][$property['CODE']]))
                                                                {
                                                                    ?>
                                                                    <dt><?=$property['NAME']?></dt>
                                                                    <dd><?=(is_array($property['DISPLAY_VALUE'])
                                                                            ? implode(' / ', $property['DISPLAY_VALUE'])
                                                                            : $property['DISPLAY_VALUE'])?>
                                                                    </dd>
                                                                    <?
                                                                }
                                                            }
                                                            unset($property);
                                                            ?>
                                                        </dl>
                                                        <?
                                                    }

                                                    if ($arResult['SHOW_OFFERS_PROPS'])
                                                    {
                                                        ?>
                                                        <dl class="product-item-detail-properties" id="<?=$itemIds['DISPLAY_MAIN_PROP_DIV']?>"></dl>
                                                        <?
                                                    }
                                                    ?>
                                                </div>
                                                <?
                                            }

                                            break;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="product-item-detail-pay-block">
                                <?
                                foreach ($arParams['PRODUCT_PAY_BLOCK_ORDER'] as $blockName)
                                {
                                    switch ($blockName)
                                    {
                                        case 'rating':
                                            if ($arParams['USE_VOTE_RATING'] === 'Y')
                                            {
                                                ?>
                                                <div class="product-item-detail-info-container">
                                                    <?
                                                    $APPLICATION->IncludeComponent(
                                                        'bitrix:iblock.vote',
                                                        'stars',
                                                        array(
                                                            'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                                            'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                                                            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                                            'ELEMENT_ID' => $arResult['ID'],
                                                            'ELEMENT_CODE' => '',
                                                            'MAX_VOTE' => '5',
                                                            'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
                                                            'SET_STATUS_404' => 'N',
                                                            'DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
                                                            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                                            'CACHE_TIME' => $arParams['CACHE_TIME']
                                                        ),
                                                        $component,
                                                        array('HIDE_ICONS' => 'Y')
                                                    );
                                                    ?>
                                                </div>
                                                <?
                                            }

                                            break;

                                        case 'price':
                                            ?>
                                            <div class="product-item-detail-info-container buy-block">
                                                <?
                                                if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                                                {
                                                    ?>
                                                    <div class="product-item-detail-price-old" style="display: <?=($showDiscount ? '' : 'none')?>;">
                                                        Старая цена: <span id="<?=$itemIds['OLD_PRICE_ID']?>"><?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?><span>
                                                    </div>
                                                    <?
                                                }
                                                ?>
                                                <span class="price text-center text-sm-left">Цена:</span>
                                                <div data-element-id="<?=$arResult['ID']?>" class="product-item-detail-price-current " data-price-base="<?=$price['RATIO_BASE_PRICE']?>" id="<?=$itemIds['PRICE_ID']?>">
                                                    <?if($_GET["width"] && $_GET["length"] && in_array($arResult['ID'], [11808,20494, 20495, 20496])) {
                                                        foreach ($variationsArray as $variation) {
                                                            if ($variation["WIDTH"] == $_GET["width"] && $variation["LENGTH"] == $_GET["length"]  && $variation["HEIGHT"] == $_GET["height"]) {
                                                                echo number_format($variation["PRICE"], 0, "", " ")." руб.";
                                                            }
                                                        }
                                                    } else {
                                                        echo $price['PRINT_PRICE'];
                                                    }?>
                                                </div>

                                                <?
                                                if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                                                {
                                                    ?>
                                                    <div class="item_economy_price" id="<?=$itemIds['DISCOUNT_PRICE_ID']?>"
                                                         style="display: <?=($showDiscount ? '' : 'none')?>;">
                                                        <?
                                                        if ($showDiscount)
                                                        {
                                                            echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' => $price['PRINT_RATIO_DISCOUNT']));
                                                        }
                                                        ?>
                                                    </div>
                                                    <?
                                                }
                                                ?>
                                            </div>

                                            <?php if ($arResult['PROPERTIES']['ATT_SHIPPING_COST']['VALUE'] != ""): ?>
                                                <div id="priceWithShipping">
                                                    <?if (in_array($arResult['ID'], [11808, 20495, 20496])):

                                                        if ($_GET["width"] && $_GET["length"]) {
                                                            // echo "bingo";
                                                            foreach ($variationsArray as $variation) {
                                                                if ($variation["WIDTH"] == $_GET["width"] && $variation["LENGTH"] == $_GET["length"]) {
                                                                    $price = intval($variation["PRICE"]);
                                                                    $inTruck = intval($variation["IN_TRUCK_QNTY"]);
                                                                    $shippingCost = intval($arResult["PROPERTIES"]["ATT_SHIPPING_COST"]["VALUE"]);

                                                                    $priceWithShipping = $price + ($shippingCost / $inTruck);

                                                                }
                                                            }
                                                        } else {
                                                            $price = intval($arResult["PRICES"]["BASE"]["VALUE"]);
                                                            $inTruck = intval($arResult["PROPERTIES"]["ATT_ZHB_PLATE_QTY_IN_TRUCK"]["VALUE"]);
                                                            $shippingCost = intval($arResult["PROPERTIES"]["ATT_SHIPPING_COST"]["VALUE"]);

                                                            $priceWithShipping = $price + ($shippingCost / $inTruck);
                                                        }
                                                        ?>

                                                        <div class="product-item-detail-info-container buy-block">
                                                            <span class="price text-center text-sm-left hhh">Цена с доставкой*:</span>
                                                            <div class="product-item-detail-price-current">
                                                                <?=number_format($priceWithShipping, 0, "", " ");?>
                                                                руб.
                                                            </div>
                                                        </div>
                                                    <?endif;?>
                                                </div>
                                            <?php endif; ?>

                                            <div id="priceFromStorage"></div>
                                            <?
                                            break;

                                        case 'priceRanges':
                                            if ($arParams['USE_PRICE_COUNT'])
                                            {
                                                $showRanges = !$haveOffers && count($actualItem['ITEM_QUANTITY_RANGES']) > 1;
                                                $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
                                                ?>
                                                <div class="product-item-detail-info-container"
                                                    <?=$showRanges ? '' : 'style="display: none;"'?>
                                                     data-entity="price-ranges-block">
                                                    <div class="product-item-detail-info-container-title">
                                                        <?=$arParams['MESS_PRICE_RANGES_TITLE']?>
                                                        <span data-entity="price-ranges-ratio-header">
														(<?=(Loc::getMessage(
                                                                'CT_BCE_CATALOG_RATIO_PRICE',
                                                                array('#RATIO#' => ($useRatio ? $measureRatio : '1').' '.$actualItem['ITEM_MEASURE']['TITLE'])
                                                            ))?>)
													</span>
                                                    </div>
                                                    <dl class="product-item-detail-properties" data-entity="price-ranges-body">
                                                        <?
                                                        if ($showRanges)
                                                        {
                                                            foreach ($actualItem['ITEM_QUANTITY_RANGES'] as $range)
                                                            {
                                                                if ($range['HASH'] !== 'ZERO-INF')
                                                                {
                                                                    $itemPrice = false;

                                                                    foreach ($arResult['ITEM_PRICES'] as $itemPrice)
                                                                    {
                                                                        if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
                                                                        {
                                                                            break;
                                                                        }
                                                                    }

                                                                    if ($itemPrice)
                                                                    {
                                                                        ?>
                                                                        <dt>
                                                                            <?
                                                                            echo Loc::getMessage(
                                                                                    'CT_BCE_CATALOG_RANGE_FROM',
                                                                                    array('#FROM#' => $range['SORT_FROM'].' '.$actualItem['ITEM_MEASURE']['TITLE'])
                                                                                ).' ';

                                                                            if (is_infinite($range['SORT_TO']))
                                                                            {
                                                                                echo Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                                                                            }
                                                                            else
                                                                            {
                                                                                echo Loc::getMessage(
                                                                                    'CT_BCE_CATALOG_RANGE_TO',
                                                                                    array('#TO#' => $range['SORT_TO'].' '.$actualItem['ITEM_MEASURE']['TITLE'])
                                                                                );
                                                                            }
                                                                            ?>
                                                                        </dt>
                                                                        <dd><?=($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE'])?></dd>
                                                                        <?
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                    </dl>
                                                </div>
                                                <?
                                                unset($showRanges, $useRatio, $itemPrice, $range);
                                            }

                                            break;

                                        case 'quantityLimit':
                                            if ($arParams['SHOW_MAX_QUANTITY'] !== 'N')
                                            {
                                                if ($haveOffers)
                                                {
                                                    ?>
                                                    <div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT']?>" style="display: none;">
                                                        <div class="product-item-detail-info-container-title">
                                                            <?=$arParams['MESS_SHOW_MAX_QUANTITY']?>:
                                                            <span class="product-item-quantity" data-entity="quantity-limit-value"></span>
                                                        </div>
                                                    </div>
                                                    <?
                                                }
                                                else
                                                {
                                                    if (
                                                        $measureRatio
                                                        && (float)$actualItem['CATALOG_QUANTITY'] > 0
                                                        && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y'
                                                        && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N'
                                                    )
                                                    {
                                                        ?>
                                                        <div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT']?>">
                                                            <div class="product-item-detail-info-container-title">
                                                                <?=$arParams['MESS_SHOW_MAX_QUANTITY']?>:
                                                                <span class="product-item-quantity" data-entity="quantity-limit-value">
																<?
                                                                if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
                                                                {
                                                                    if ((float)$actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
                                                                    {
                                                                        echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
                                                                    }
                                                                    else
                                                                    {
                                                                        echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    echo $actualItem['CATALOG_QUANTITY'].' '.$actualItem['ITEM_MEASURE']['TITLE'];
                                                                }
                                                                ?>
															</span>
                                                            </div>
                                                        </div>
                                                        <?
                                                    }
                                                }
                                            }

                                            break;

                                        case 'quantity':
                                            if ($arParams['USE_PRODUCT_QUANTITY'])
                                            {
                                                ?>
                                                <div class="qty-buy d-flex">
                                                <div class="product-item-detail-info-container" style="<?=(!$actualItem['CAN_BUY'] ? 'display: none;' : '')?>"
                                                     data-entity="quantity-block">
                                                    <div class="product-item-detail-info-container-title"><?=Loc::getMessage('CATALOG_QUANTITY')?></div>
                                                    <div class="product-item-amount">
                                                        <div class="product-item-amount-field-container">
                                                            <div class="plus-minus-wrapper">
                                                                <span class="product-item-amount-field-btn-minus no-select" id="<?=$itemIds['QUANTITY_DOWN_ID']?>"></span>
                                                                <input class="product-item-amount-field" id="<?=$itemIds['QUANTITY_ID']?>" type="number"
                                                                       value="<?=$price['MIN_QUANTITY']?>">
                                                                <span class="product-item-amount-field-btn-plus no-select" id="<?=$itemIds['QUANTITY_UP_ID']?>"></span>
                                                            </div>

                                                            <span class="product-item-amount-description-container">
															<span id="<?=$itemIds['QUANTITY_MEASURE']?>">
																<?=$actualItem['ITEM_MEASURE']['TITLE']?>
															</span>
															<span id="<?=$itemIds['PRICE_TOTAL']?>"></span>
														</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?
                                            }

                                            break;

                                        case 'buttons':
                                            ?>
                                            <div data-entity="main-button-container">
                                                <div id="<?=$itemIds['BASKET_ACTIONS_ID']?>" style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;">
                                                    <?
                                                    if ($showAddBtn)
                                                    {
                                                        ?>
                                                        <div class="product-item-detail-info-container">
                                                            <a class="btn <?=$showButtonClassName?> product-item-detail-buy-button" id="<?=$itemIds['ADD_BASKET_LINK']?>"
                                                               href="javascript:void(0);">
                                                                <span><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></span>
                                                            </a>
                                                        </div>
                                                        <?
                                                    }

                                                    if ($showBuyBtn)
                                                    {
                                                        ?>
                                                        <div class="product-item-detail-info-container">
                                                            <!-- <a class="btn <?=$buyButtonClassName?> product-item-detail-buy-button hhh" id="<?=$itemIds['BUY_LINK']?>"
															href="javascript:void(0);">
															<span><?=$arParams['MESS_BTN_BUY']?></span>
														</a> -->

                                                            <a class="btn <?=$buyButtonClassName?> product-item-detail-buy-button" data-role="add2cart" id="add2cart"
                                                               href="javascript:void(0);">
                                                                <span><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></span>
                                                            </a>
                                                        </div>
                                                        <?
                                                    }
                                                    ?>
                                                </div>
                                                <?
                                                if ($showSubscribe)
                                                {
                                                    ?>
                                                    <div class="product-item-detail-info-container">
                                                        <?
                                                        $APPLICATION->IncludeComponent(
                                                            'bitrix:catalog.product.subscribe',
                                                            '',
                                                            array(
                                                                'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                                                'PRODUCT_ID' => $arResult['ID'],
                                                                'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
                                                                'BUTTON_CLASS' => 'btn btn-default product-item-detail-buy-button',
                                                                'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
                                                                'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
                                                            ),
                                                            $component,
                                                            array('HIDE_ICONS' => 'Y')
                                                        );
                                                        ?>
                                                    </div>
                                                    <?
                                                }
                                                ?>
                                                <div class="product-item-detail-info-container">
                                                    <a class="btn btn-link product-item-detail-buy-button" id="<?=$itemIds['NOT_AVAILABLE_MESS']?>"
                                                       href="javascript:void(0)"
                                                       rel="nofollow" style="display: <?=(!$actualItem['CAN_BUY'] ? '' : 'none')?>;">
                                                        <?=$arParams['MESS_NOT_AVAILABLE']?>
                                                    </a>
                                                </div>
                                            </div>
                                            <button data-fancybox data-src="#oneclick-buy-modal" type="button" class="btn btn-oneclick ml-2 ml-sm-0">Купить в 1 клик</button>
                                            </div>
                                            <?
                                            break;
                                    }
                                }

                                if ($arParams['DISPLAY_COMPARE'])
                                {
                                    ?>
                                    <div class="product-item-detail-compare-container">
                                        <div class="product-item-detail-compare">
                                            <div class="checkbox">
                                                <label id="<?=$itemIds['COMPARE_LINK']?>">
                                                    <input type="checkbox" data-entity="compare-checkbox">
                                                    <span data-entity="compare-title"><?=$arParams['MESS_BTN_COMPARE']?></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <?
                                }
                                ?>

                                <div class="payment-options">
                                    <div class="title">Доступные способы оплаты</div>
                                    <div class="options-row d-flex">
                                        <div class="bank">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bank.svg" alt="">
                                            Банковский перевод
                                        </div>

                                        <div class="beznal ml-4 mr-4">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/card.svg" alt="">
                                            Безналичный расчет
                                        </div>

                                        <div class="cash">
                                            <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bank.svg" alt="">
                                            Наличные
                                        </div>
                                    </div>

                                    <p class="disclaimer">*Цена с доставкой до Казани и при загрузке 20 тонн</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <?
                    if ($haveOffers)
                    {
                        if ($arResult['OFFER_GROUP'])
                        {
                            foreach ($arResult['OFFER_GROUP_VALUES'] as $offerId)
                            {
                                ?>
                                <span id="<?=$itemIds['OFFER_GROUP'].$offerId?>" style="display: none;">
								<?
                                $APPLICATION->IncludeComponent(
                                    'bitrix:catalog.set.constructor',
                                    '.default',
                                    array(
                                        'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                        'IBLOCK_ID' => $arResult['OFFERS_IBLOCK'],
                                        'ELEMENT_ID' => $offerId,
                                        'PRICE_CODE' => $arParams['PRICE_CODE'],
                                        'BASKET_URL' => $arParams['BASKET_URL'],
                                        'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
                                        'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                        'CACHE_TIME' => $arParams['CACHE_TIME'],
                                        'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                                        'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME'],
                                        'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                        'CURRENCY_ID' => $arParams['CURRENCY_ID']
                                    ),
                                    $component,
                                    array('HIDE_ICONS' => 'Y')
                                );
                                ?>
							</span>
                                <?
                            }
                        }
                    }
                    else
                    {
                        if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
                        {
                            $APPLICATION->IncludeComponent(
                                'bitrix:catalog.set.constructor',
                                '.default',
                                array(
                                    'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],
                                    'ELEMENT_ID' => $arResult['ID'],
                                    'PRICE_CODE' => $arParams['PRICE_CODE'],
                                    'BASKET_URL' => $arParams['BASKET_URL'],
                                    'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                    'CACHE_TIME' => $arParams['CACHE_TIME'],
                                    'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                                    'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME'],
                                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                    'CURRENCY_ID' => $arParams['CURRENCY_ID']
                                ),
                                $component,
                                array('HIDE_ICONS' => 'Y')
                            );
                        }
                    }
                    ?>
                </div>
            </div>

            <div class="element-tabs border-top">
                <button class="btn btn-secondary" type="button" id="dropdownMenuButton">
                    <b>Характеристики</b> <span class="caret"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/arrow-down.svg" alt=""></span>
                </button>
                <ul class="nav nav-tabs" id="<?=$itemIds['TABS_ID']?>" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-entity="tab" data-value="properties" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Характеристики</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-entity="tab" data-value="description" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Описание</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="shipping-tab" data-toggle="tab" href="#shipping" role="tab" aria-controls="shipping" aria-selected="false">Доставка</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="payment-tab" data-toggle="tab" href="#payment" role="tab" aria-controls="payment" aria-selected="false">Оплата</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="visual-tab" data-toggle="tab" href="#visual" role="tab" aria-controls="visual" aria-selected="false">3D визуализация</a>
                    </li>
                </ul>

                <div class="tab-content" id="<?=$itemIds['TAB_CONTAINERS_ID']?>">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="title">Основные характеристики</div>
                                <?php
                                if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
                                {
                                    ?>
                                    <div class="product-item-detail-tab-content" data-entity="tab-container" data-value="properties">
                                        <?
                                        if (!empty($arResult['DISPLAY_PROPERTIES']) && !$_GET["width"] && !$_GET["length"])
                                        {
                                            ?>
                                            <dl class="product-item-detail-properties">
                                                <?
                                                foreach ($arResult['DISPLAY_PROPERTIES'] as $property)
                                                {

                                                    ?>
                                                    <dt><?=$property['NAME']?></dt>
                                                    <dd><?=(
                                                        is_array($property['DISPLAY_VALUE'])
                                                            ? implode(' / ', $property['DISPLAY_VALUE'])
                                                            : $property['DISPLAY_VALUE']
                                                        )?>
                                                    </dd>
                                                    <?
                                                }
                                                unset($property);
                                                ?>
                                            </dl>
                                            <?
                                        }

                                        if($_GET["width"] && $_GET["length"]) {
                                            ?>
                                            <dl class="product-item-detail-properties">
                                                <?
                                                foreach ($variationsArray as $variation) {
                                                    if ($variation["WIDTH"] == $_GET["width"] && $variation["LENGTH"] == $_GET["length"] && $variation["HEIGHT"] == $_GET["height"]) { ?>
                                                        <dt style="background: rgb(255, 255, 255);">Производитель</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation['MAKER']?></dd>
                                                        <dt style="background: rgb(255, 255, 255);">Вес, кг</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation["WEIGHT"]?></dd>
                                                        <dt style="background: rgb(255, 255, 255);">Объем, м3</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation["VOLUME"]?></dd>
                                                        <dt style="background: rgb(255, 255, 255);">Длина, мм</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation["LENGTH"]?></dd>
                                                        <dt style="background: rgb(255, 255, 255);">Ширина, мм</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation["WIDTH"]?></dd>
                                                        <dt style="background: rgb(255, 255, 255);">Высота, мм</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation["HEIGHT"]?></dd>
                                                        <dt style="background: rgb(255, 255, 255);">Размеры отверстия, мм</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation["HOLE_SIZE"]?></dd>
                                                        <dt style="background: rgb(255, 255, 255);">Нагрузка, кгс/м2</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation["PRESSURE"]?></dd>
                                                        <dt style="background: rgb(255, 255, 255);">Кол-во на машине 20 т, шт</dt>
                                                        <dd style="background: rgb(255, 255, 255);"><?=$variation["IN_TRUCK_QNTY"]?></dd>
                                                    <?}
                                                } ?>
                                            </dl>
                                        <?}

                                        if ($arResult['SHOW_OFFERS_PROPS'])
                                        {
                                            ?>
                                            <dl class="product-item-detail-properties offer-props" id="<?=$itemIds['DISPLAY_PROP_DIV']?>"></dl>
                                            <?
                                        }

                                        ?>
                                    </div>
                                    <?
                                }
                                ?>
                            </div>
                        </div>

                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <?php
                        if ($showDescription)
                        {
                            ?>
                            <div class="product-item-detail-tab-content active" data-entity="tab-container" data-value="description"
                                 itemprop="description">
                                <?
                                if (
                                    $arResult['PREVIEW_TEXT'] != ''
                                    && (
                                        $arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'S'
                                        || ($arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'E' && $arResult['DETAIL_TEXT'] == '')
                                    )
                                )
                                {
                                    echo $arResult['PREVIEW_TEXT_TYPE'] === 'html' ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>';
                                }

                                if ($arResult['DETAIL_TEXT'] != '')
                                {
                                    echo $arResult['DETAIL_TEXT_TYPE'] === 'html' ? $arResult['DETAIL_TEXT'] : '<p>'.$arResult['DETAIL_TEXT'].'</p>';
                                }
                                ?>
                            </div>
                            <?
                        }
                        ?>
                    </div>
                    <div class="tab-pane fade" id="shipping" role="tabpanel" aria-labelledby="shipping-tab">
                        <div class="row bg-white justify-content-center p-3">
                            <div class="col-lg-8 p-0">
                                <a class="fancybox-section" data-fancybox href="https://www.youtube.com/watch?v=u-w6BmrZKBs">
                                    <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/video_delivery.jpg" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/video_delivery.jpg' alt="">
                                    <img data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/play-yellow.png" alt="" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/play-yellow.png' class="play-btn lazy">
                                </a>

                                <h2 class="page-section">Доставка</h2>

                                <p>Наша компания на протяжении более 6 лет помогает клиентам с организацией доставки.</p>
                                <p>За это время мы глубоко вникли в тонкости логистики и хорошо понимаем, какие трудности ожидают клиента при организации доставки. Начиная от небрежного отношения к товару  и заканчивая воровством груза. В начале своей деятельности мы не раз сталкивались с такими проблемами.</p>
                                <p>Сейчас же у нас широкий список водителей, с которыми мы работаем уже на протяжении нескольких лет. Они заинтересованы в дальнейшем сотрудничестве с нами, хорошо знают требования наших клиентов, добросовестно выполняют свою работу и всегда готовы предоставить нам партнерскую скидку.</p>

                                <p>Организация доставки очень важный момент при покупке строительных материалов, не зная особенностей можно столкнуться со следующими проблемами:
                                <ul class="info-page-list">
                                    <li>Очень часто при доставке выбираются машины, которые не могут проехать на участок клиента. </li>
                                    <li>Очень часто заказывают машину с манипулятором, которая заехать на участок может, но для разгрузки ей надо встать параллельно прицепу, а для этого места нет.</li>
                                    <li>Бывают настолько безответственные водители, что при разгрузке поднимают сразу по 2 поддона. При такой разгрузке товар, конечно же, теряет свой первоначальный вид.</li>
                                    <li>Очень часто для разгрузки заказывают более дорогой кран, когда в этом нет никакой необходимости и обычная машина с манипулятором может справиться с разгрузкой. В некоторых же случаях кран наоборот не заказывают, когда потребность в нем присутствует.</li>
                                </ul>
                                </p>

                                <p>На самом деле список проблем очень большой и всегда это бьёт по карману покупателя.</p>
                                <p>В организации доставки для нас нет коммерческой выгоды, но если Вы купите товар у нас, а доставку организуете сами и при этом столкнетесь с трудностями, опыт показывает, что впечатление от покупки будет испорчено. А для нас довольство клиента покупкой является одним из самых главных приоритетов.</p>

                                <p>Поэтому мы всегда рады помочь Вам с организацией логистики. Мы с удовольствием подберем для Вас самый оптимальный вариант доставки и разгрузки.</p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                        <div class="row bg-white justify-content-center p-3">
                            <div class="col-lg-8 p-0">
                                <a class="fancybox-section" data-fancybox href="https://www.youtube.com/watch?v=k-EwioaAW4s">
                                    <img class='lazy' data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/video_payment.jpg" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/video_payment.jpg' alt="">
                                    <img data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/play-yellow.png" alt="" data-srcset='<?=SITE_TEMPLATE_PATH?>/assets/img/play-yellow.png' class="play-btn lazy">
                                </a>

                                <h2 class="page-section">Оплата</h2>

                                <p>В нашей компании мы внедрили все способы оплаты, кроме биткоинов:
                                <ul class="info-page-list">
                                    <li>наличными деньгами</li>
                                    <li>пластиковой картой через терминал</li>
                                    <li>безналичная форма расчётов с юридическими лицами</li>
                                    <li>оплата в Вашем банке на расчётный счёт</li>
                                    <li>при острой необходимости - возможны даже переводы через "Сбербанк-онлайн"</li>
                                </ul>
                                </p>

                                <p>При оплате товара в нашей компании, Вы получаете на руки «Договор поставки» и подтверждающие документы об оплате.</p>

                                <p>После доставки товара Вы получаете товарную накладную. Это особенно важно, так как у Вас есть полное право вернуть у государства 13% от затраченных средств на строительство своего жилья. Максимальная сумма возврата составляет 260 000 рублей.</p>

                                <p>Мы уверенны в качестве реализуемой продукции и дорожим своим членством в «Ассоциации добросовестных поставщиков», поэтому отгружаем товар только после 100% оплаты.</p>

                                <p>За 6 лет существовании нашей компании, мы убедились, что очень редкие водители серьезно относятся к бухгалтерским документам. Поэтому документы, подтверждающие оплату и отгрузку, передаем на руки только в нашем офисе продаж.</p>

                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade" id="visual" role="tabpanel" aria-labelledby="visual-tab">
                        <div class="row bg-white justify-content-center p-3">
                            <div id="container-3d">

                                <!-- 3  -->
                                <div class="loader loader--style3">
                                    <div class="white_fade"></div>
                                    <svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
                            <path fill="#ff1654 " d="M43.935,25.145c0-10.318-8.364-18.683-18.683-18.683c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615c8.072,0,14.615,6.543,14.615,14.615H43.935z">
                                <animateTransform attributeType="xml"
                                                  attributeName="transform"
                                                  type="rotate"
                                                  from="0 25 25"
                                                  to="360 25 25"
                                                  dur="0.6s"
                                                  repeatCount="indefinite"/>
                            </path>
                            </svg>
                                </div>

                                <img src="/3d/images/1nf_gladkiy_chokolad.jpg" alt="" id="current-house" class="house">
                                <div id="config-btn" class="config-btn"><span>Конфигуратор дома</span></div>

                                <div class="download-image">
                                    Скачать <br> модель
                                </div>

                                <div class="toggle-interface">
                                    <div class="show-interface option-interface">Показать <br> интерфейс</div>
                                    <div class="hide-interface option-interface visible">Скрыть <br> интерфейс</div>
                                </div>

                                <div class="selected-3d">
                                    <div class="title">Выбранное:</div>
                                </div>

                                <div class="selections">
                                    <div id="brick-maker" class="selection-option-block">
                                        <div class="block-title">Выберите производителя</div>

                                        <div class="border_blue">
                                            <div class="btn-stn brick-maker" data-maker="alexeevsk">
                                                <img src="/3d/images/icons/makers/alekseevsk.jpg" alt="">
                                                <span>Алексеевская керамика</span>
                                            </div>
                                        </div>

                                        <div class="border_blue">
                                            <div class="btn-stn brick-maker" data-maker="altair">
                                                <img src="/3d/images/icons/makers/altair.jpg" alt="">
                                                <span>Альтаир (Ижевск)</span>
                                            </div>
                                        </div>

                                        <div class="border_blue">
                                            <div class="btn-stn brick-maker" data-maker="aspk">
                                                <img src="/3d/images/icons/makers/aspk.jpg" alt="">
                                                <span>АСПК (Арск)</span>
                                            </div>
                                        </div>

                                        <div class="border_blue">
                                            <div class="btn-stn brick-maker" data-maker="kc-keramik">
                                                <img src="/3d/images/icons/makers/kc_keramik.jpg" alt="">
                                                <span>КС-Керамик (Кирово-Чепецк)</span>
                                            </div>
                                        </div>

                                        <div class="border_blue">
                                            <div class="btn-stn brick-maker" data-maker="kluker">
                                                <img src="/3d/images/icons/makers/kluker.jpg" alt="">
                                                <span>Клюкер (Ключищи)</span>
                                            </div>
                                        </div>

                                        <div class="border_blue">
                                            <div class="btn-stn brick-maker" data-maker="koshak">
                                                <img src="/3d/images/icons/makers/koshak.jpg" alt="">
                                                <span>Ак Барс Керамик (Кощаково)</span>
                                            </div>
                                        </div>

                                    </div> <!-- brick maker -->

                                    <div id="brick-formats" class="selection-option-block" style="display: none;">
                                        <div class="go-back"><img src="/3d/images/icons/left-arrow.svg" alt="">Производитель</div>
                                        <div class="block-title">Выберите формат кирпича</div>

                                        <div class="border_blue">
                                            <div class="btn-stn brick-format brick-format__1nf" data-format="1nf" data-format-name="1НФ">
                                                <img src="/3d/images/icons/brick-1nf.svg" alt="">
                                                <span>1НФ</span>
                                            </div>
                                        </div>

                                        <div class="border_blue">
                                            <div class="btn-stn brick-format brick-format__14nf" data-format="14nf" data-format-name="1.4НФ">
                                                <img src="/3d/images/icons/brick-14nf.svg" alt="">
                                                <span>1.4НФ</span>
                                            </div>
                                        </div>
                                    </div> <!-- brick format -->


                                    <div id="brick-surfaces" class="selection-option-block" style="display: none;">
                                        <div class="go-back"><img src="/3d/images/icons/left-arrow.svg" alt="">Формат</div>
                                        <div class="block-title">Выберите поверхность кирпича</div>
                                    </div> <!-- brick format -->


                                    <div id="brick-tones" class="selection-option-block" style="display: none;">
                                        <div class="go-back"><img src="/3d/images/icons/left-arrow.svg" alt="">Поверхность</div>
                                        <div class="block-title">Выберите оттенок кирпича</div>
                                    </div> <!-- brick tones -->

                                    <div id="brick-colors" class="selection-option-block" style="display: none;">
                                        <div class="go-back"><img src="/3d/images/icons/left-arrow.svg" alt="">Оттенок</div>
                                        <div class="block-title">Выберите цвет кирпича</div>
                                    </div> <!-- brick tones -->

                                    <div id="final-choices" class="selection-option-block" style="display: none;">
                                        <div class="block-title">Выбранные параметры</div>
                                    </div> <!-- brick tones -->

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>


            <?php
            $nav = CIBlockSection::GetNavChain(4, $arResult['IBLOCK_SECTION_ID']);

            if($ar_res = $nav->GetNext())
                $parent_section_id = $ar_res['ID'];
            ?>

            <?php if (!$isMobile): ?>
                <?php if ($parent_section_id == 20 || $parent_section_id == 255 || $parent_section_id == 223): ?>

                    <div class="row coming-together">
                        <div class="col-12">
                            <div class="catalog-block-header" data-entity="header">Сопутствующие товары</div>
                            <?
                            $APPLICATION->IncludeComponent(
                                "bitrix:catalog.section",
                                "catalog-section-bestoffers",
                                array(
                                    "ACTION_VARIABLE" => "action",
                                    "ADD_PICT_PROP" => "-",
                                    "ADD_PROPERTIES_TO_BASKET" => "Y",
                                    "ADD_SECTIONS_CHAIN" => "N",
                                    "ADD_TO_BASKET_ACTION" => "ADD",
                                    "AJAX_MODE" => "N",
                                    "AJAX_OPTION_ADDITIONAL" => "",
                                    "AJAX_OPTION_HISTORY" => "N",
                                    "AJAX_OPTION_JUMP" => "N",
                                    "AJAX_OPTION_STYLE" => "Y",
                                    "BACKGROUND_IMAGE" => "-",
                                    "BASKET_URL" => "/personal/cart/",
                                    "BROWSER_TITLE" => "-",
                                    "CACHE_FILTER" => "N",
                                    "CACHE_GROUPS" => "Y",
                                    "CACHE_TIME" => "36000000",
                                    "CACHE_TYPE" => "A",
                                    "COMPATIBLE_MODE" => "Y",
                                    "CONVERT_CURRENCY" => "N",
                                    "CUSTOM_FILTER" => "",
                                    "DETAIL_URL" => "",
                                    "DISABLE_INIT_JS_IN_COMPONENT" => "N",
                                    "DISPLAY_BOTTOM_PAGER" => "N",
                                    "DISPLAY_COMPARE" => "N",
                                    "DISPLAY_TOP_PAGER" => "N",
                                    "ELEMENT_SORT_FIELD" => "sort",
                                    "ELEMENT_SORT_FIELD2" => "id",
                                    "ELEMENT_SORT_ORDER" => "asc",
                                    "ELEMENT_SORT_ORDER2" => "desc",
                                    "ENLARGE_PRODUCT" => "PROP",
                                    "FILTER_NAME" => "arrFilter",
                                    "HIDE_NOT_AVAILABLE" => "N",
                                    "HIDE_NOT_AVAILABLE_OFFERS" => "N",
                                    "IBLOCK_ID" => "4",
                                    "IBLOCK_TYPE" => "catalog",
                                    "INCLUDE_SUBSECTIONS" => "Y",
                                    "LABEL_PROP" => array(
                                    ),
                                    "LAZY_LOAD" => "N",
                                    "LINE_ELEMENT_COUNT" => "3",
                                    "LOAD_ON_SCROLL" => "N",
                                    "MESSAGE_404" => "",
                                    "MESS_BTN_ADD_TO_BASKET" => "В корзину",
                                    "MESS_BTN_BUY" => "Купить",
                                    "MESS_BTN_DETAIL" => "Подробнее",
                                    "MESS_BTN_SUBSCRIBE" => "Подписаться",
                                    "MESS_NOT_AVAILABLE" => "Нет в наличии",
                                    "META_DESCRIPTION" => "-",
                                    "META_KEYWORDS" => "-",
                                    "OFFERS_CART_PROPERTIES" => array(
                                    ),
                                    "OFFERS_FIELD_CODE" => array(
                                        0 => "CODE",
                                        1 => "NAME",
                                        2 => "",
                                    ),
                                    "OFFERS_LIMIT" => "5",
                                    "OFFERS_PROPERTY_CODE" => array(
                                        0 => "ATT_COLOR",
                                        1 => "",
                                    ),
                                    "OFFERS_SORT_FIELD" => "sort",
                                    "OFFERS_SORT_FIELD2" => "id",
                                    "OFFERS_SORT_ORDER" => "asc",
                                    "OFFERS_SORT_ORDER2" => "desc",
                                    "PAGER_BASE_LINK_ENABLE" => "N",
                                    "PAGER_DESC_NUMBERING" => "N",
                                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                    "PAGER_SHOW_ALL" => "N",
                                    "PAGER_SHOW_ALWAYS" => "N",
                                    "PAGER_TEMPLATE" => ".default",
                                    "PAGER_TITLE" => "Товары",
                                    "PAGE_ELEMENT_COUNT" => "5",
                                    "PARTIAL_PRODUCT_PROPERTIES" => "N",
                                    "PRICE_CODE" => array(
                                        0 => "BASE",
                                    ),
                                    "PRICE_VAT_INCLUDE" => "Y",
                                    "PRODUCT_BLOCKS_ORDER" => "sku,price,props,quantityLimit,quantity,buttons",
                                    "PRODUCT_DISPLAY_MODE" => "Y",
                                    "PRODUCT_ID_VARIABLE" => "id",
                                    "PRODUCT_PROPERTIES" => array(
                                    ),
                                    "PRODUCT_PROPS_VARIABLE" => "prop",
                                    "PRODUCT_QUANTITY_VARIABLE" => "quantity",
                                    "PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'0','BIG_DATA':false}]",
                                    "PRODUCT_SUBSCRIPTION" => "Y",
                                    "PROPERTY_CODE" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "PROPERTY_CODE_MOBILE" => array(
                                    ),
                                    "RCM_PROD_ID" => $_REQUEST["PRODUCT_ID"],
                                    "RCM_TYPE" => "personal",
                                    "SECTION_CODE" => "",
                                    "SECTION_ID" => "638",
                                    "SECTION_ID_VARIABLE" => "SECTION_ID",
                                    "SECTION_URL" => "",
                                    "SECTION_USER_FIELDS" => array(
                                        0 => "",
                                        1 => "",
                                    ),
                                    "SEF_MODE" => "N",
                                    "SET_BROWSER_TITLE" => "N",
                                    "SET_LAST_MODIFIED" => "N",
                                    "SET_META_DESCRIPTION" => "N",
                                    "SET_META_KEYWORDS" => "N",
                                    "SET_STATUS_404" => "N",
                                    "SET_TITLE" => "N",
                                    "SHOW_404" => "N",
                                    "SHOW_ALL_WO_SECTION" => "Y",
                                    "SHOW_CLOSE_POPUP" => "N",
                                    "SHOW_DISCOUNT_PERCENT" => "N",
                                    "SHOW_FROM_SECTION" => "N",
                                    "SHOW_MAX_QUANTITY" => "N",
                                    "SHOW_OLD_PRICE" => "N",
                                    "SHOW_PRICE_COUNT" => "1",
                                    "SHOW_SLIDER" => "N",
                                    "SLIDER_INTERVAL" => "3000",
                                    "SLIDER_PROGRESS" => "N",
                                    "TEMPLATE_THEME" => "blue",
                                    "USE_ENHANCED_ECOMMERCE" => "N",
                                    "USE_MAIN_ELEMENT_SECTION" => "N",
                                    "USE_PRICE_COUNT" => "N",
                                    "USE_PRODUCT_QUANTITY" => "N",
                                    "COMPONENT_TEMPLATE" => ".default2",
                                    "ENLARGE_PROP" => "-",
                                    "OFFER_ADD_PICT_PROP" => "MORE_PHOTO",
                                    "OFFER_TREE_PROPS" => array(
                                        0 => "ATT_COLOR",
                                    )
                                ),
                                false
                            );?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <div class="row">
                <div class="col-xs-12">
                    <?
                    if ($arResult['CATALOG'] && $actualItem['CAN_BUY'] && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
                    {
                        $APPLICATION->IncludeComponent(
                            'bitrix:sale.prediction.product.detail',
                            '.default',
                            array(
                                'BUTTON_ID' => $showBuyBtn ? $itemIds['BUY_LINK'] : $itemIds['ADD_BASKET_LINK'],
                                'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                'POTENTIAL_PRODUCT_TO_BUY' => array(
                                    'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
                                    'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
                                    'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
                                    'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
                                    'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,

                                    'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][0]['ID']) ? $arResult['OFFERS'][0]['ID'] : null,
                                    'SECTION' => array(
                                        'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
                                        'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
                                        'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
                                        'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
                                    ),
                                )
                            ),
                            $component,
                            array('HIDE_ICONS' => 'Y')
                        );
                    }

                    if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
                    {
                        ?>
                        <div data-entity="parent-container">
                            <?
                            if (!isset($arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE']) || $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'] !== 'Y')
                            {
                                ?>
                                <div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
                                    <?=($arParams['GIFTS_DETAIL_BLOCK_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_GIFT_BLOCK_TITLE_DEFAULT'))?>
                                </div>
                                <?
                            }

                            CBitrixComponent::includeComponentClass('bitrix:sale.products.gift');
                            $APPLICATION->IncludeComponent(
                                'bitrix:sale.products.gift',
                                '.default',
                                array(
                                    'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                    'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                                    'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],

                                    'PRODUCT_ROW_VARIANTS' => "",
                                    'PAGE_ELEMENT_COUNT' => 0,
                                    'DEFERRED_PRODUCT_ROW_VARIANTS' => \Bitrix\Main\Web\Json::encode(
                                        SaleProductsGiftComponent::predictRowVariants(
                                            $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
                                            $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT']
                                        )
                                    ),
                                    'DEFERRED_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],

                                    'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
                                    'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
                                    'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
                                    'PRODUCT_DISPLAY_MODE' => 'Y',
                                    'PRODUCT_BLOCKS_ORDER' => $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'],
                                    'SHOW_SLIDER' => $arParams['GIFTS_SHOW_SLIDER'],
                                    'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
                                    'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',

                                    'TEXT_LABEL_GIFT' => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],

                                    'LABEL_PROP_'.$arParams['IBLOCK_ID'] => array(),
                                    'LABEL_PROP_MOBILE_'.$arParams['IBLOCK_ID'] => array(),
                                    'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],

                                    'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
                                    'MESS_BTN_BUY' => $arParams['~GIFTS_MESS_BTN_BUY'],
                                    'MESS_BTN_ADD_TO_BASKET' => $arParams['~GIFTS_MESS_BTN_BUY'],
                                    'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
                                    'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],

                                    'SHOW_PRODUCTS_'.$arParams['IBLOCK_ID'] => 'Y',
                                    'PROPERTY_CODE_'.$arParams['IBLOCK_ID'] => $arParams['LIST_PROPERTY_CODE'],
                                    'PROPERTY_CODE_MOBILE'.$arParams['IBLOCK_ID'] => $arParams['LIST_PROPERTY_CODE_MOBILE'],
                                    'PROPERTY_CODE_'.$arResult['OFFERS_IBLOCK'] => $arParams['OFFER_TREE_PROPS'],
                                    'OFFER_TREE_PROPS_'.$arResult['OFFERS_IBLOCK'] => $arParams['OFFER_TREE_PROPS'],
                                    'CART_PROPERTIES_'.$arResult['OFFERS_IBLOCK'] => $arParams['OFFERS_CART_PROPERTIES'],
                                    'ADDITIONAL_PICT_PROP_'.$arParams['IBLOCK_ID'] => (isset($arParams['ADD_PICT_PROP']) ? $arParams['ADD_PICT_PROP'] : ''),
                                    'ADDITIONAL_PICT_PROP_'.$arResult['OFFERS_IBLOCK'] => (isset($arParams['OFFER_ADD_PICT_PROP']) ? $arParams['OFFER_ADD_PICT_PROP'] : ''),

                                    'HIDE_NOT_AVAILABLE' => 'Y',
                                    'HIDE_NOT_AVAILABLE_OFFERS' => 'Y',
                                    'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
                                    'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
                                    'PRICE_CODE' => $arParams['PRICE_CODE'],
                                    'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
                                    'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
                                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                    'BASKET_URL' => $arParams['BASKET_URL'],
                                    'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
                                    'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
                                    'PARTIAL_PRODUCT_PROPERTIES' => $arParams['PARTIAL_PRODUCT_PROPERTIES'],
                                    'USE_PRODUCT_QUANTITY' => 'N',
                                    'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
                                    'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                                    'POTENTIAL_PRODUCT_TO_BUY' => array(
                                        'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
                                        'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
                                        'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
                                        'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
                                        'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,

                                        'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'])
                                            ? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID']
                                            : null,
                                        'SECTION' => array(
                                            'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
                                            'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
                                            'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
                                            'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
                                        ),
                                    ),

                                    'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
                                    'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
                                    'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
                                ),
                                $component,
                                array('HIDE_ICONS' => 'Y')
                            );
                            ?>
                        </div>
                        <?
                    }

                    if ($arResult['CATALOG'] && $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
                    {
                        ?>
                        <div data-entity="parent-container">
                            <?
                            if (!isset($arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE']) || $arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE'] !== 'Y')
                            {
                                ?>
                                <div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
                                    <?=($arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_GIFTS_MAIN_BLOCK_TITLE_DEFAULT'))?>
                                </div>
                                <?
                            }

                            $APPLICATION->IncludeComponent(
                                'bitrix:sale.gift.main.products',
                                '.default',
                                array(
                                    'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
                                    'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
                                    'LINE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
                                    'HIDE_BLOCK_TITLE' => 'Y',
                                    'BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],

                                    'OFFERS_FIELD_CODE' => $arParams['OFFERS_FIELD_CODE'],
                                    'OFFERS_PROPERTY_CODE' => $arParams['OFFERS_PROPERTY_CODE'],

                                    'AJAX_MODE' => $arParams['AJAX_MODE'],
                                    'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
                                    'IBLOCK_ID' => $arParams['IBLOCK_ID'],

                                    'ELEMENT_SORT_FIELD' => 'ID',
                                    'ELEMENT_SORT_ORDER' => 'DESC',
                                    //'ELEMENT_SORT_FIELD2' => $arParams['ELEMENT_SORT_FIELD2'],
                                    //'ELEMENT_SORT_ORDER2' => $arParams['ELEMENT_SORT_ORDER2'],
                                    'FILTER_NAME' => 'searchFilter',
                                    'SECTION_URL' => $arParams['SECTION_URL'],
                                    'DETAIL_URL' => $arParams['DETAIL_URL'],
                                    'BASKET_URL' => $arParams['BASKET_URL'],
                                    'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
                                    'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
                                    'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],

                                    'CACHE_TYPE' => $arParams['CACHE_TYPE'],
                                    'CACHE_TIME' => $arParams['CACHE_TIME'],

                                    'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
                                    'SET_TITLE' => $arParams['SET_TITLE'],
                                    'PROPERTY_CODE' => $arParams['PROPERTY_CODE'],
                                    'PRICE_CODE' => $arParams['PRICE_CODE'],
                                    'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
                                    'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],

                                    'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
                                    'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
                                    'CURRENCY_ID' => $arParams['CURRENCY_ID'],
                                    'HIDE_NOT_AVAILABLE' => 'Y',
                                    'HIDE_NOT_AVAILABLE_OFFERS' => 'Y',
                                    'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
                                    'PRODUCT_BLOCKS_ORDER' => $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'],

                                    'SHOW_SLIDER' => $arParams['GIFTS_SHOW_SLIDER'],
                                    'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
                                    'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',

                                    'ADD_PICT_PROP' => (isset($arParams['ADD_PICT_PROP']) ? $arParams['ADD_PICT_PROP'] : ''),
                                    'LABEL_PROP' => (isset($arParams['LABEL_PROP']) ? $arParams['LABEL_PROP'] : ''),
                                    'LABEL_PROP_MOBILE' => (isset($arParams['LABEL_PROP_MOBILE']) ? $arParams['LABEL_PROP_MOBILE'] : ''),
                                    'LABEL_PROP_POSITION' => (isset($arParams['LABEL_PROP_POSITION']) ? $arParams['LABEL_PROP_POSITION'] : ''),
                                    'OFFER_ADD_PICT_PROP' => (isset($arParams['OFFER_ADD_PICT_PROP']) ? $arParams['OFFER_ADD_PICT_PROP'] : ''),
                                    'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : ''),
                                    'SHOW_DISCOUNT_PERCENT' => (isset($arParams['SHOW_DISCOUNT_PERCENT']) ? $arParams['SHOW_DISCOUNT_PERCENT'] : ''),
                                    'DISCOUNT_PERCENT_POSITION' => (isset($arParams['DISCOUNT_PERCENT_POSITION']) ? $arParams['DISCOUNT_PERCENT_POSITION'] : ''),
                                    'SHOW_OLD_PRICE' => (isset($arParams['SHOW_OLD_PRICE']) ? $arParams['SHOW_OLD_PRICE'] : ''),
                                    'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
                                    'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
                                    'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
                                    'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
                                    'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
                                    'SHOW_CLOSE_POPUP' => (isset($arParams['SHOW_CLOSE_POPUP']) ? $arParams['SHOW_CLOSE_POPUP'] : ''),
                                    'DISPLAY_COMPARE' => (isset($arParams['DISPLAY_COMPARE']) ? $arParams['DISPLAY_COMPARE'] : ''),
                                    'COMPARE_PATH' => (isset($arParams['COMPARE_PATH']) ? $arParams['COMPARE_PATH'] : ''),
                                )
                                + array(
                                    'OFFER_ID' => empty($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'])
                                        ? $arResult['ID']
                                        : $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'],
                                    'SECTION_ID' => $arResult['SECTION']['ID'],
                                    'ELEMENT_ID' => $arResult['ID'],

                                    'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
                                    'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
                                    'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
                                ),
                                $component,
                                array('HIDE_ICONS' => 'Y')
                            );
                            ?>
                        </div>
                        <?
                    }
                    ?>
                </div>
            </div>
        </div>
        <!--Small Card-->
        <div class="product-item-detail-short-card-fixed hidden-xs" id="<?=$itemIds['SMALL_CARD_PANEL_ID']?>">
            <div class="product-item-detail-short-card-content-container">
                <table style="border: 0;">
                    <tr>
                        <td rowspan="2" class="product-item-detail-short-card-image">
                            <img src="" style="height: 65px;" data-entity="panel-picture">
                        </td>
                        <td class="product-item-detail-short-title-container" data-entity="panel-title">
                            <span class="product-item-detail-short-title-text"><?=$name?></span>
                        </td>
                        <td rowspan="2" class="product-item-detail-short-card-price">
                            <?
                            if ($arParams['SHOW_OLD_PRICE'] === 'Y')
                            {
                                ?>
                                <div class="product-item-detail-price-old" style="display: <?=($showDiscount ? '' : 'none')?>;"
                                     data-entity="panel-old-price">
                                    <?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?>
                                </div>
                                <?
                            }
                            ?>
                            <div class="product-item-detail-price-current" data-entity="panel-price">
                                <?=$price['PRINT_RATIO_PRICE']?>
                            </div>
                        </td>
                        <?
                        if ($showAddBtn)
                        {
                            ?>
                            <td rowspan="2" class="product-item-detail-short-card-btn"
                                style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;"
                                data-entity="panel-add-button">
                                <a class="btn <?=$showButtonClassName?> product-item-detail-buy-button"
                                   id="<?=$itemIds['ADD_BASKET_LINK']?>"
                                   href="javascript:void(0);">
                                    <span><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></span>
                                </a>
                            </td>
                            <?
                        }

                        if ($showBuyBtn)
                        {
                            ?>
                            <td rowspan="2" class="product-item-detail-short-card-btn"
                                style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;">
                                <button data-role="add2cart" class="btn <?=$buyButtonClassName?> product-item-detail-buy-button">
                                    <span><?=$arParams['MESS_BTN_BUY']?></span>
                                </button>
                            </td>
                            <?
                        }
                        ?>
                        <td rowspan="2" class="product-item-detail-short-card-btn"
                            style="display: <?=(!$actualItem['CAN_BUY'] ? '' : 'none')?>;"
                            data-entity="panel-not-available-button">
                            <a class="btn btn-link product-item-detail-buy-button" href="javascript:void(0)"
                               rel="nofollow">
                                <?=$arParams['MESS_NOT_AVAILABLE']?>
                            </a>
                        </td>
                    </tr>
                    <?
                    if ($haveOffers)
                    {
                        ?>
                        <tr>
                            <td>
                                <div class="product-item-selected-scu-container" data-entity="panel-sku-container">
                                    <?
                                    $i = 0;

                                    foreach ($arResult['SKU_PROPS'] as $skuProperty)
                                    {
                                        if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
                                        {
                                            continue;
                                        }

                                        $propertyId = $skuProperty['ID'];

                                        foreach ($skuProperty['VALUES'] as $value)
                                        {
                                            $value['NAME'] = htmlspecialcharsbx($value['NAME']);
                                            if ($skuProperty['SHOW_MODE'] === 'PICT')
                                            {
                                                ?>
                                                <div class="product-item-selected-scu product-item-selected-scu-color selected"
                                                     title="<?=$value['NAME']?>"
                                                     style="background-image: url('<?=$value['PICT']['SRC']?>'); display: none;"
                                                     data-sku-line="<?=$i?>"
                                                     data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
                                                     data-onevalue="<?=$value['ID']?>">
                                                </div>
                                                <?
                                            }
                                            else
                                            {
                                                ?>
                                                <div class="product-item-selected-scu product-item-selected-scu-text selected"
                                                     title="<?=$value['NAME']?>"
                                                     style="display: none;"
                                                     data-sku-line="<?=$i?>"
                                                     data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
                                                     data-onevalue="<?=$value['ID']?>">
                                                    <?=$value['NAME']?>
                                                </div>
                                                <?
                                            }
                                        }

                                        $i++;
                                    }
                                    ?>
                                </div>
                            </td>
                        </tr>
                        <?
                    }
                    ?>
                </table>
            </div>
        </div>
        <!--Top tabs-->
        <?php if (!$isMobile): ?>
            <div class="product-item-detail-tabs-container-fixed hidden-xs d-none" id="<?=$itemIds['TABS_PANEL_ID']?>">
                <ul class="product-item-detail-tabs-list">
                    <?
                    if ($showDescription)
                    {
                        ?>
                        <li class="product-item-detail-tab active" data-entity="tab" data-value="description">
                            <a href="javascript:void(0);" class="product-item-detail-tab-link">
                                <span><?=$arParams['MESS_DESCRIPTION_TAB']?></span>
                            </a>
                        </li>
                        <?
                    }

                    if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS'])
                    {
                        ?>
                        <li class="product-item-detail-tab" data-entity="tab" data-value="properties">
                            <a href="javascript:void(0);" class="product-item-detail-tab-link">
                                <span><?=$arParams['MESS_PROPERTIES_TAB']?></span>
                            </a>
                        </li>
                        <?
                    }

                    if ($arParams['USE_COMMENTS'] === 'Y')
                    {
                        ?>
                        <li class="product-item-detail-tab" data-entity="tab" data-value="comments">
                            <a href="javascript:void(0);" class="product-item-detail-tab-link">
                                <span><?=$arParams['MESS_COMMENTS_TAB']?></span>
                            </a>
                        </li>
                        <?
                    }
                    ?>
                </ul>
            </div>
        <?php endif; ?>

        <!--        <pre>-->
        <!--            --><?//print_r($arResult["JS_OFFERS"]);?>
        <!--        </pre>-->

        <meta itemprop="name" content="<?=$name?>" />
        <meta itemprop="description" content="<?=$name?> купить в Казани - цены от 7,10 руб в Тулпар." />
        <meta itemprop="category" content="<?=$arResult['CATEGORY_PATH']?>" />
        <?
        if ($haveOffers)
        {
            foreach ($arResult['JS_OFFERS'] as $offer)
            {
                $currentOffersList = array();

                if (!empty($offer['TREE']) && is_array($offer['TREE']))
                {
                    foreach ($offer['TREE'] as $propName => $skuId)
                    {
                        $propId = (int)substr($propName, 5);

                        foreach ($skuProps as $prop)
                        {
                            if ($prop['ID'] == $propId)
                            {
                                foreach ($prop['VALUES'] as $propId => $propValue)
                                {
                                    if ($propId == $skuId)
                                    {
                                        $currentOffersList[] = $propValue['NAME'];
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

                $offerPrice = $offer['ITEM_PRICES'][$offer['ITEM_PRICE_SELECTED']];
                ?>
                <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="sku" content="<?=htmlspecialcharsbx(implode('/', $currentOffersList))?>" />
				<meta itemprop="price" content="<?=$offerPrice['BASE_PRICE']?>" />
				<meta itemprop="priceCurrency" content="<?=$offerPrice['CURRENCY']?>" />
				<link itemprop="availability" href="http://schema.org/<?=($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
			</span>
                <?
            }

            unset($offerPrice, $currentOffersList);
        }
        else
        {
            ?>
            <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="price" content="<?=$price['RATIO_PRICE']?>" />
			<meta itemprop="priceCurrency" content="<?=$price['CURRENCY']?>" />
			<link itemprop="availability" href="http://schema.org/<?=($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
		</span>
            <?
        }
        ?>
    </div>
<?
if ($haveOffers)
{
    $offerIds = array();
    $offerCodes = array();

    $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

//    echo "<pre>";
//    print_r($arResult['OFFERS']);
//    echo "</pre>";

//    echo "<pre>";
//    print_r($arResult['SIMPLIFIED_OFFERS']);
//    echo "</pre>";


//        echo "<pre>mehron";
//        print_r($arResult['HL']);
//    echo "</pre>";

//    echo "<pre>";
//        print_r($arResult['JS_OFFERS']);
//    echo "</pre>";

//    echo "<pre>";
//        print_r($arResult['JS_OFFERS_LIGHT']);
//    echo "</pre>";
//
//     echo "<pre>";
//     print_r($arResult['SKU_PROPS']);
//     echo "</pre>";

    foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
    {
        $offerIds[] = (int)$jsOffer['ID'];
        $offerCodes[] = $jsOffer['CODE'];

        $fullOffer = $arResult['OFFERS'][$ind];
        $measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

        $strAllProps = '';
        $strMainProps = '';
        $strPriceRangesRatio = '';
        $strPriceRanges = '';

        if ($arResult['SHOW_OFFERS_PROPS'])
        {
            if (!empty($jsOffer['DISPLAY_PROPERTIES']))
            {
                foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
                {
                    $current = '<dt>'.$property['NAME'].'</dt><dd>'.(
                        is_array($property['VALUE'])
                            ? implode(' / ', $property['VALUE'])
                            : $property['VALUE']
                        ).'</dd>';
                    $strAllProps .= $current;

                    if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
                    {
                        $strMainProps .= $current;
                    }
                }

                unset($current);
            }
        }

        if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
        {
            $strPriceRangesRatio = '('.Loc::getMessage(
                    'CT_BCE_CATALOG_RATIO_PRICE',
                    array('#RATIO#' => ($useRatio
                            ? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
                            : '1'
                        ).' '.$measureName)
                ).')';

            foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
            {
                if ($range['HASH'] !== 'ZERO-INF')
                {
                    $itemPrice = false;

                    foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
                    {
                        if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
                        {
                            break;
                        }
                    }

                    if ($itemPrice)
                    {
                        $strPriceRanges .= '<dt>'.Loc::getMessage(
                                'CT_BCE_CATALOG_RANGE_FROM',
                                array('#FROM#' => $range['SORT_FROM'].' '.$measureName)
                            ).' ';

                        if (is_infinite($range['SORT_TO']))
                        {
                            $strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
                        }
                        else
                        {
                            $strPriceRanges .= Loc::getMessage(
                                'CT_BCE_CATALOG_RANGE_TO',
                                array('#TO#' => $range['SORT_TO'].' '.$measureName)
                            );
                        }

                        $strPriceRanges .= '</dt><dd>'.($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']).'</dd>';
                    }
                }
            }

            unset($range, $itemPrice);
        }

        $jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
        $jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
        $jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
        $jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;

        //$jsOffer['BASE_PRODUCT_NAME'] = $name;
        //$jsOffer['OFFER_NAME_FORMATTED'] = $name." ".strtoupper(str_replace($name, '', $jsOffer['NAME']));

        switch ($arResult['PRODUCT_TYPE_CODE']){
            case 1:
                $keyFormat = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_102'];
                $keyColor = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_103'];
                $keySurface = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_104'];
                $offerFormat = $arResult['SKU_PROPS']['ATT_BRICK_FORMAT']['VALUES'][$keyFormat]['NAME'];
                $offerColor = $arResult['SKU_PROPS']['ATT_COLOR_BRICK']['VALUES'][$keyColor]['NAME'];
                $offerSurface = $arResult['SKU_PROPS']['ATT_BRICK_SURFACE']['VALUES'][$keySurface]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".strtoupper("$offerFormat $offerColor $offerSurface");
                break;
            case 2:
                $keyWidth = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_172'];
                $keyHeight = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_173'];
                $keyLength = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_200'];
                $keyDensity = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_155'];

                $offerWidth = $arResult['SKU_PROPS']['ATT_GAZOBLOCK_WIDTH_SKU']['VALUES'][$keyWidth]['NAME'];
                $offerHeight = $arResult['SKU_PROPS']['ATT_GAZOBLOCK_HEIGHT_SKU']['VALUES'][$keyHeight]['NAME'];
                $offerLength = $arResult['SKU_PROPS']['ATT_SNOWKEEPER_LENGTH_SKU']['VALUES'][$keyLength]['NAME'];
                $offerDensity = $arResult['SKU_PROPS']['ATT_DENSITY_MARK_BLOCKS_SKU']['VALUES'][$keyDensity]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".strtoupper($offerWidth).'x'.strtoupper($offerHeight).'x'.strtoupper($offerLength).'мм '.$offerDensity;
                break;
            case 3:
                $keySeries = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_260'];
                $keyWood = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_293'];
                $keyDirection = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_296'];

                $offerSeries = $arResult['SKU_PROPS']['ATT_SERIES_SKU']['VALUES'][$keySeries]['NAME'];
                $offerWood = $arResult['SKU_PROPS']['ATT_WOOD_WOODEN_STAIRS_SKU']['VALUES'][$keyWood]['NAME'];
                $offerDirection = $arResult['SKU_PROPS']['ATT_DIRECTION_WOOD_STAIRS_SKU']['VALUES'][$keyDirection]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".$offerSeries." ".strtoupper($offerWood)." ".strtoupper($offerDirection);
                break;
            case 4:
                $keySeries = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_260'];
                $keyMaterial =$arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_259'];

                $offerSeries = $arResult['SKU_PROPS']['ATT_SERIES_SKU']['VALUES'][$keySeries]['NAME'];
                $offerMaterial = $arResult['SKU_PROPS']['ATT_MATERIAL_SKU']['VALUES'][$keyMaterial]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".$offerSeries." ".strtoupper($offerMaterial);
                break;
            case 5:
                $keyThickness = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_61'];
                $keyCoating = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_62'];
                $keyColor = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_59'];

                $offerThickness = $arResult['SKU_PROPS']['ATT_THICKNESS']['VALUES'][$keyThickness]['NAME'];
                $offerCoating = $arResult['SKU_PROPS']['ATT_COATING']['VALUES'][$keyCoating]['NAME'];
                $offerColor = $arResult['SKU_PROPS']['ATT_COLOR']['VALUES'][$keyColor]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".$offerThickness." ".$offerCoating." ".$offerColor;
                break;
            case 7:
                $keyColor = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_59'];

                $offerColor = $arResult['SKU_PROPS']['ATT_COLOR']['VALUES'][$keyColor]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".$offerColor;
                break;
            case 9:
                $keyFormat = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_102'];

                $offerFormat = $arResult['SKU_PROPS']['ATT_BRICK_FORMAT']['VALUES'][$keyFormat]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".strtoupper("$offerFormat");
                break;
            case 10:
                $keyColor = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_240'];

                $offerColor = $arResult['SKU_PROPS']['ATT_COLOR_SMESI']['VALUES'][$keyColor]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".strtoupper($offerColor)." ".strtoupper($arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['SEASON']['VALUE']);

                break;
            case 11:
                $keyLength = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_200'];
                $keyName = $arResult['JS_OFFERS_LIGHT'][$jsOffer['ID']]['TREE']['PROP_318'];

                $offerLength = $arResult['SKU_PROPS']['ATT_SNOWKEEPER_LENGTH_SKU']['VALUES'][$keyLength]['NAME'];
                $offerName = $arResult['SKU_PROPS']['ATT_NAME_FLEXIBLE_SKU']['VALUES'][$keyName]['NAME'];

                $jsOffer['OFFER_NAME_FORMATTED'] = $name." ".$offerLength." ".$offerName;

                break;

        }
    }

    $templateData['OFFER_IDS'] = $offerIds;
    $templateData['OFFER_CODES'] = $offerCodes;
    unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

    $jsParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => true,
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
            'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
            'OFFER_GROUP' => $arResult['OFFER_GROUP'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
            'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
            'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
            'USE_STICKERS' => true,
            'USE_SUBSCRIBE' => $showSubscribe,
            'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
            'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
            'ALT' => $alt,
            'TITLE' => $title,
            'MAGNIFIER_ZOOM_PERCENT' => 200,
            'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
            'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
            'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
                ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
                : null
        ),
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'VISUAL' => $itemIds,
        'DEFAULT_PICTURE' => array(
            'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
            'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
        ),
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'ACTIVE' => $arResult['ACTIVE'],
            'NAME' => $arResult['~NAME'],
            'CATEGORY' => $arResult['CATEGORY_PATH']
        ),
        'BASKET' => array(
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'BASKET_URL' => $arParams['BASKET_URL'],
            'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        ),
        'OFFERS' => $arResult['JS_OFFERS'],
        'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
        'TREE_PROPS' => $skuProps,
        'YOUTUBE_ID' => $arResult['PROPERTIES']['ATT_YOUTUBE_ID']['VALUE'],
        'ZAVOD_YOUTUBE_ID' => $arResult['PROPERTIES']['ATT_ZAVOD_YOUTUBE_ID']['VALUE'],
        'VIDEO_COUNT' => $video_count,
        'SHIPPING_COST' => $arResult['PROPERTIES']['ATT_SHIPPING_COST']['VALUE']
    );
}
else
{
    $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
    if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
    {
        ?>
        <div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
            <?
            if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
            {
                foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
                {
                    ?>
                    <input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
                    <?
                    unset($arResult['PRODUCT_PROPERTIES'][$propId]);
                }
            }

            $emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
            if (!$emptyProductProperties)
            {
                ?>
                <table>
                    <?
                    foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
                    {
                        ?>
                        <tr>
                            <td><?=$arResult['PROPERTIES'][$propId]['NAME']?></td>
                            <td>
                                <?
                                if (
                                    $arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
                                    && $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
                                )
                                {
                                    foreach ($propInfo['VALUES'] as $valueId => $value)
                                    {
                                        ?>
                                        <label>
                                            <input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]"
                                                   value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"checked"' : '')?>>
                                            <?=$value?>
                                        </label>
                                        <br>
                                        <?
                                    }
                                }
                                else
                                {
                                    ?>
                                    <select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]">
                                        <?
                                        foreach ($propInfo['VALUES'] as $valueId => $value)
                                        {
                                            ?>
                                            <option value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"selected"' : '')?>>
                                                <?=$value?>
                                            </option>
                                            <?
                                        }
                                        ?>
                                    </select>
                                    <?
                                }
                                ?>
                            </td>
                        </tr>
                        <?
                    }
                    ?>
                </table>
                <?
            }
            ?>
        </div>
        <?
    }

    $jsParams = array(
        'CONFIG' => array(
            'USE_CATALOG' => $arResult['CATALOG'],
            'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
            'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
            'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
            'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
            'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
            'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
            'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
            'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
            'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
            'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
            'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
            'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
            'USE_STICKERS' => true,
            'USE_SUBSCRIBE' => $showSubscribe,
            'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
            'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
            'ALT' => $alt,
            'TITLE' => $title,
            'MAGNIFIER_ZOOM_PERCENT' => 200,
            'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
            'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
            'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
                ? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
                : null
        ),
        'VISUAL' => $itemIds,
        'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
        'PRODUCT' => array(
            'ID' => $arResult['ID'],
            'ACTIVE' => $arResult['ACTIVE'],
            'PICT' => reset($arResult['MORE_PHOTO']),
            'NAME' => $arResult['~NAME'],
            'SUBSCRIPTION' => true,
            'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
            'ITEM_PRICES' => $arResult['ITEM_PRICES'],
            'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
            'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
            'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
            'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
            'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
            'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
            'SLIDER' => $arResult['MORE_PHOTO'],
            'CAN_BUY' => $arResult['CAN_BUY'],
            'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
            'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
            'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
            'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
            'CATEGORY' => $arResult['CATEGORY_PATH']
        ),
        'BASKET' => array(
            'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
            'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
            'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
            'EMPTY_PROPS' => $emptyProductProperties,
            'BASKET_URL' => $arParams['BASKET_URL'],
            'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
            'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
        )
    );
    unset($emptyProductProperties);
}

if ($arParams['DISPLAY_COMPARE'])
{
    $jsParams['COMPARE'] = array(
        'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
        'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
        'COMPARE_PATH' => $arParams['COMPARE_PATH']
    );
}
?>

<?php
// echo "<pre>";
//
// print_r($arResult['JS_OFFERS'][0]);
//
// echo "</pre>";

?>

    <script>

        ////////////////////////////////

        let js_offers = {};
        let js_offers_params ={};

        let poddon_qty;
        <?php


        for ($i=0; $i < sizeof($arResult['JS_OFFERS']); $i++) { ?>
        var valueToPush = {};
        <? foreach ($arResult['JS_OFFERS'][$i]['TREE'] as $key => $value) { ?>
        valueToPush["<?=$key?>"] = "<?=$value?>";
        <?	} ?>

        poddon_qty = 1;

        var valueToPush2 = {};

        valueToPush2["ID"] = "<?=$arResult['JS_OFFERS'][$i]["ID"]?>";
        valueToPush2["NAME"] = "<?=$arResult['JS_OFFERS'][$i]["NAME"]?>";
        valueToPush2["IMG"] = "<?=$arResult['JS_OFFERS'][$i]["PREVIEW_PICTURE"]["SRC"]?>";
        valueToPush2['PRICE'] = "<?=$arResult['JS_OFFERS'][$i]["ITEM_PRICES"][0]["BASE_PRICE"]?>";

        //if (<?//=$poddon_qty_array[$i]?>// != "") {
        //    poddon_qty = <?//=$poddon_qty_array[$i]?>//;
        //}
        //valueToPush2['PODDON_QTY'] = poddon_qty;

        js_offers["row_"+"<?=$i?>"] = valueToPush;
        js_offers_params["row_"+"<?=$i?>"] = valueToPush2;
        <?}?>

//console.log(js_offers_params);


        $('[data-role=add2cart]').click(function(){
            let toCartList = {};

            for (var i = 0; i < $(".product-item-detail-info-section .product-item-scu-item-list .selected").length; i++) {
                var key = $(".product-item-detail-info-section .product-item-scu-item-list .selected:eq("+i+")").attr("data-treevalue");
                key = "PROP_" + key.substr(0, key.indexOf('_'));
                var value = $(".product-item-detail-info-section .product-item-scu-item-list .selected:eq("+i+")").attr("data-onevalue");
                toCartList[key] = value;
            }
            //console.log(toCartList);
            var json_curSetting = JSON.stringify(toCartList);
            //console.log(json_curSetting);
            //console.log(js_offers);

            //console.log(js_offers);

            if (!$.isEmptyObject(js_offers)) {
                for (var offer in js_offers) {
                    var json_offer = JSON.stringify(js_offers[offer]);
                    //console.log(json_offer);
                    if (json_curSetting == json_offer) {
                        //console.log(offer + " yay");

                        js_offers_params[offer]["URL"] = "<?=$arResult["DETAIL_PAGE_URL"]?>";
                        var js_offers_params_json = JSON.stringify(js_offers_params[offer]);
                        console.log(js_offers_params[offer]);



                        $.ajax({
                            type: 'post',
                            url: '/local/ajax/add2cart.php',
                            data: {offer_param: js_offers_params_json},
                            success:function(html) {
                                //$("#cart_count").html("("+html+")");
                            }
                        });

                        $.ajax({
                            type: 'post',
                            url: '/local/ajax/add2cart_new.php',
                            data: {offer_param: js_offers_params_json},
                            success:function(html) {
                                console.log(js_offers_params_json);
                                console.log('html '+ html);
                                $("#cart_count").html("("+html+")");

                                $('.addedToCartSuccess').addClass('show');
                                setTimeout(function () {
                                    $('.addedToCartSuccess').removeClass('show');
                                }, 1500);
                            }
                        });
                    }
                }
            } else {
                let temp = {ID: "<?=$arResult['ID']?>"};
                $.ajax({
                    type: 'post',
                    url: '/local/ajax/add2cart_new.php',
                    dataType: "html",
                    data: {offer_param: JSON.stringify(temp)},
                    success:function(html) {
                        console.log('html2 '+ html);
                        $("#cart_count").html("("+html+")");

                        $('.addedToCartSuccess').addClass('show');
                        setTimeout(function () {
                            $('.addedToCartSuccess').removeClass('show');
                        }, 1500);
                    }
                });
            }

        });


        $(".product-item-detail-slider-download").click(function(){

            let toCartList = {};
            let offer_name = "tulpar";

            for (var i = 0; i < $(".product-item-detail-info-section .product-item-scu-item-list .selected").length; i++) {
                var key = $(".product-item-detail-info-section .product-item-scu-item-list .selected:eq("+i+")").attr("data-treevalue");
                key = "PROP_" + key.substr(0, key.indexOf('_'));
                var value = $(".product-item-detail-info-section .product-item-scu-item-list .selected:eq("+i+")").attr("data-onevalue");
                toCartList[key] = value;
            }
            var json_curSetting = JSON.stringify(toCartList);

            for (var offer in js_offers) {
                var json_offer = JSON.stringify(js_offers[offer]);
                if (json_curSetting == json_offer) {
                    offer_name = js_offers_params[offer]["NAME"];
                }
            }

            var link = document.createElement('a');
            var href = $(this).closest(".product-item-detail-slider-container").find(".product-item-detail-slider-image.active img").attr("src");
            link.href = href;  // use realtive url
            link.download = offer_name + '.jpg';
            document.body.appendChild(link);
            link.click();
            link.remove();
        });


        $(".product-item-detail-slider-controls-image.my_custom_control").click(function(){
            $(".product-item-detail-slider-images-container div[data-entity='image']").removeClass("active");
            $(".product-item-detail-slider-images-container div[data-entity='youtube_image']").addClass("active");
            $(this).toggleClass('active');
            $(".product-item-detail-slider-controls-image[data-entity='slider-control']").removeClass('active');
        });

        $(".product-item-detail-slider-controls-image[data-entity='slider-control']").click(function(){
            $(".product-item-detail-slider-images-container div[data-entity='youtube_image']").removeClass("active");
            $(".product-item-detail-slider-controls-image.my_custom_control").removeClass("active");
        });

    </script>
    <script>
        BX.message({
            ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
            TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
            TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
            BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
            BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
            BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
            BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
            BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
            TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
            COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
            COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
            COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
            BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
            PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
            PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
            RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
            RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
            SITE_ID: '<?=$component->getSiteId()?>'
        });

        var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);

        var product_tmr = {
            productid: "<?=$arResult['ID']?>",
            pagetype: 'product',
            list: '1'
        };
    </script>
<?
unset($actualItem, $itemIds, $jsParams);