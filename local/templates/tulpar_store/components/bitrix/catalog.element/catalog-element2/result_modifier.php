<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */
use Bitrix\Main\Loader;

Loader::includeModule("highloadblock");

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$hlbl = 3; // Указываем ID нашего highloadblock блока к которому будет делать запросы.
$hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();

$entity = HL\HighloadBlockTable::compileEntity($hlblock);
$entity_data_class = $entity->getDataClass();

$rsData = $entity_data_class::getList(array(
    "select" => array("*"),
    "order" => array("ID" => "ASC"),
    "filter" => array()  // Задаем параметры фильтра выборки
));

$hl = [];
while($arData = $rsData->Fetch()){
    $hl[$arData['UF_XML_ID']] = $arData;
}

$arResult['HL_COLORS'] = $hl;

if(in_array($arResult['ID'], [11808, 20494, 20495, 20496])):?>

    <?
    $arSelect = Array("ID", "IBLOCK_ID", "NAME",
        "PREVIEW_PICTURE", "catalog_GROUP_1", "PROPERTY_ATT_ZHB_PLATE_WIDTH_MM",
        "PROPERTY_ATT_ZHB_PLATE_LENGTH_MM", "PROPERTY_ATT_ZHB_PLATE_HEIGHT_MM",
        "PROPERTY_ATT_ZHB_PLATE_WEIGHT_KG",
        "PROPERTY_ATT_ZHB_PLATE_VOLUME_M3",
        "PROPERTY_ATT_ZHB_PLATE_HOLE_SIZE_MM",
        "PROPERTY_ATT_ZHB_PLATE_PRESSURE_KGS_M2",
        "PROPERTY_ATT_ZHB_PLATE_QTY_IN_TRUCK",
        "PROPERTY_MORE_PHOTO",
    );
    $arFilter = Array("IBLOCK_ID"=> 4, "SECTION_ID" => 906, "ACTIVE_DATE"=>"Y", "INCLUDE_SUBSECTIONS" => "Y", "!PROPERTY_ATT_IS_MAIN_ITEM_VALUE" => "Да", "ACTIVE"=>"Y");
    $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>1000), $arSelect);
    $variationsArray = array();
    $imagesArr = array();
    $imagesIdOnlyArr = array();
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();

        if ($arFields["PREVIEW_PICTURE"]) {
            $imagesArr[$arFields["ID"]]["IMAGE_ID"][] = $arFields["PREVIEW_PICTURE"];
            $imagesIdOnlyArr[] = $arFields["PREVIEW_PICTURE"];
        }

        if ($arFields["PROPERTY_MORE_PHOTO_VALUE"]) {
            $imagesArr[$arFields["ID"]]["IMAGE_ID"][] = $arFields["PROPERTY_MORE_PHOTO_VALUE"];
            $imagesIdOnlyArr[] = $arFields["PROPERTY_MORE_PHOTO_VALUE"];
        }

        $variationsArray[] = array("ID" => $arFields["ID"],
            "NAME" => $arFields["NAME"],
            "WIDTH" => $arFields["PROPERTY_ATT_ZHB_PLATE_WIDTH_MM_VALUE"],
            "LENGTH" =>  $arFields["PROPERTY_ATT_ZHB_PLATE_LENGTH_MM_VALUE"],
            "HEIGHT" =>  $arFields["PROPERTY_ATT_ZHB_PLATE_HEIGHT_MM_VALUE"],
            "WEIGHT" => $arFields["PROPERTY_ATT_ZHB_PLATE_WEIGHT_KG_VALUE"],
            "VOLUME" => $arFields["PROPERTY_ATT_ZHB_PLATE_VOLUME_M3_VALUE"],
            "HOLE_SIZE" => $arFields["PROPERTY_ATT_ZHB_PLATE_HOLE_SIZE_MM_VALUE"],
            "PRESSURE" => $arFields["PROPERTY_ATT_ZHB_PLATE_PRESSURE_KGS_M2_VALUE"],
            "IN_TRUCK_QNTY" => $arFields["PROPERTY_ATT_ZHB_PLATE_QTY_IN_TRUCK_VALUE"],
            "PRICE" => $arFields["CATALOG_PRICE_1"]
        );

    }



    $res = CFile::GetList(array("FILE_SIZE"=>"desc"), array("@ID"=> $imagesIdOnlyArr));
    $imagesPathArr = array();
    while($res_arr = $res->GetNext()) {
        $imagesPathArr[$res_arr["ID"]] = $res_arr["SUBDIR"]."/".$res_arr["FILE_NAME"];
    }

    foreach($imagesArr as $key => $image) {
        if ($imagesPathArr[$image["IMAGE_ID"]]) {
            $imagesArr[$key]["IMAGE_PATH"] = "/upload/".$imagesPathArr[$image["IMAGE_ID"]];
        }

        foreach ($image["IMAGE_ID"] as $imageID) {
            if ($imagesPathArr[$imageID]) {
                $imagesArr[$key]["IMAGE_PATH"][$imageID] = "/upload/".$imagesPathArr[$imageID];
            }
        }
    }

    foreach ($arResult["MORE_PHOTO"] as $key => $photo) {
        $arResult["MORE_PHOTO"][$key]["ELEMENT_ID"] = $arResult["ID"];
    }

    if(!empty($imagesArr))
    {
        foreach ($imagesArr as $key => $images)
        {
            foreach ($images["IMAGE_PATH"] as $id => $image)
            {
                $arResult["MORE_PHOTO"][] = ["ID" => $id, "SRC" => $image, "WIDTH" => 1000, "HEIGHT" => 1000, "ELEMENT_ID" => $key];
            }
        }
    }

    //$arResult["MORE_PHOTO_COUNT"] = count($arResult["MORE_PHOTO"]);

//    echo "<pre>";
//    print_r($arResult["MORE_PHOTO_COUNT"]);
//    echo "</pre>";

endif;

switch ($arResult['PROPERTIES']['ATT_PRODUCT_CLASSIFICATION']['VALUE_ENUM_ID']):
    case 2737:
        $PRODUCT_TYPE_CODE = 1; //Кирпич
        break;
    case 2738:
        $PRODUCT_TYPE_CODE = 2; //Газоблок
        break;
    case 2739:
        $PRODUCT_TYPE_CODE = 3; //Лестницы
        break;
    case 2740:
        $PRODUCT_TYPE_CODE = 4; //Чердачые лестницы
        break;
    case 2741:
        $PRODUCT_TYPE_CODE = 5; // Металлочерепица и профнастил
        break;
    case 2742:
        $PRODUCT_TYPE_CODE = 6; //Плиты перекрытия
        break;
    case 2743:
        $PRODUCT_TYPE_CODE = 7; // другие
        break;
    case 2744:
        $PRODUCT_TYPE_CODE = 8; //без характеристик
        break;
    case 2745:
        $PRODUCT_TYPE_CODE = 9; //керамблоки
        break;
    case 2746:
        $PRODUCT_TYPE_CODE = 10; //смеси
        break;
    case 2747:
        $PRODUCT_TYPE_CODE = 11; //сетки
        break;
    default:
        $PRODUCT_TYPE_CODE = -1; //все остальное
        break;
endswitch;

$arResult['PRODUCT_TYPE_CODE'] = $PRODUCT_TYPE_CODE;

$simplifiedOffers = [];

foreach ($arResult['OFFERS'] as $offer) {
    switch ($PRODUCT_TYPE_CODE){
        case 1:
            $simplifiedOffers[$offer['ID']] = strtoupper($offer['PROPERTIES']['ATT_BRICK_FORMAT']['VALUE']." ".$offer['PROPERTIES']['ATT_COLOR_BRICK']['VALUE']." ".$offer['PROPERTIES']['ATT_BRICK_SURFACE']['VALUE']);
            break;
        case 2:
            $simplifiedOffers[$offer['ID']] = $offer['PROPERTIES']['ATT_GAZOBLOCK_WIDTH_SKU']['VALUE']."x".$offer['PROPERTIES']['ATT_GAZOBLOCK_HEIGHT_SKU']['VALUE']."x".$offer['PROPERTIES']['ATT_SNOWKEEPER_LENGTH_SKU']['VALUE']."мм ".$offer['PROPERTIES']['ATT_DENSITY_MARK_BLOCKS_SKU']['VALUE'];
            break;
        case 3:
            $simplifiedOffers[$offer['ID']] = $offer['PROPERTIES']['ATT_SERIES_SKU']['VALUE']." ".strtoupper($offer['PROPERTIES']['ATT_WOOD_WOODEN_STAIRS_SKU']['VALUE'])." ".strtoupper($offer['PROPERTIES']['ATT_DIRECTION_WOOD_STAIRS_SKU']['VALUE']);
            break;
        case 4:
            $simplifiedOffers[$offer['ID']] = $offer['PROPERTIES']['ATT_SERIES_SKU']['VALUE']." ".strtoupper($offer['PROPERTIES']['ATT_MATERIAL_SKU']['VALUE']);
            break;
        case 5:
            $simplifiedOffers[$offer['ID']] = $offer['PROPERTIES']['ATT_THICKNESS']['VALUE']." ".$offer['PROPERTIES']['ATT_COATING']['VALUE']." ".$arResult['HL_COLORS'][$offer['PROPERTIES']['ATT_COLOR']['VALUE']]['UF_NAME'];
            break;
        case 7:
            $simplifiedOffers[$offer['ID']] = $arResult['HL_COLORS'][$offer['PROPERTIES']['ATT_COLOR']['VALUE']]['UF_NAME'];
            break;
        case 9:
            $simplifiedOffers[$offer['ID']] = $offer['PROPERTIES']['ATT_BRICK_FORMAT']['VALUE'];
            break;
        case 10:
            $simplifiedOffers[$offer['ID']] = strtoupper($offer['PROPERTIES']['ATT_COLOR_MIXES_SKU']['VALUE'])." ".strtoupper($offer['PROPERTIES']['ATT_SEASON_OPERATION_MIXED_SKU']['VALUE']);
            break;
        case 11:
            $simplifiedOffers[$offer['ID']] = $offer['PROPERTIES']['ATT_SNOWKEEPER_LENGTH_SKU']['VALUE']." ".$offer['PROPERTIES']['ATT_NAME_FLEXIBLE_SKU']['VALUE'];
            break;
    }

}

$arResult['SIMPLIFIED_OFFERS'] = $simplifiedOffers;


$jsOffersLight = [];
foreach ($arResult['JS_OFFERS'] as $i => $jsOffer) {
    if ($PRODUCT_TYPE_CODE === 10) {
        $jsOffersLight[$jsOffer['ID']] = ['NAME' => $jsOffer['NAME'], 'TREE' => $jsOffer['TREE'], 'SEASON' => $jsOffer['DISPLAY_PROPERTIES'][2]];
    } else {
        $jsOffersLight[$jsOffer['ID']] = ['NAME' => $jsOffer['NAME'], 'TREE' => $jsOffer['TREE']];
    }

}

$arResult['JS_OFFERS_LIGHT'] = $jsOffersLight;

if ( $arResult['ITEM_PRICES'][0]['PERCENT'] == 0 )
{
    $arResult['ITEM_PRICES'][0]['PERCENT'] = 5;
    $arResult['ITEM_PRICES'][0]['PRINT_RATIO_BASE_PRICE'] = round ( ( (float) $arResult['ITEM_PRICES'][0]['PRICE'] / 100) * 105, 2) . ' руб.';
    $arResult['ITEM_PRICES'][0]['PRINT_RATIO_DISCOUNT'] = round ( ( (float) $arResult['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2) . ' руб.';
    $arResult['ITEM_PRICES'][0]['RATIO_DISCOUNT'] = round ( ( (float) $arResult['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2);
    $arResult['ITEM_PRICES'][0]['DISCOUNT'] = round ( ( (float) $arResult['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2); 
}
   

foreach ( $arResult['OFFERS'] as $tKey => $tVal ) {
    if ( $arResult['JS_OFFERS'][$tKey]['ITEM_PRICES'][0]['PERCENT'] == 0 )
    {
        $arResult['OFFERS'][$tKey]['ITEM_PRICES'][0]['PERCENT'] = 5;
        $arResult['OFFERS'][$tKey]['ITEM_PRICES'][0]['PRINT_RATIO_BASE_PRICE'] = round ( ( (float) $tVal['ITEM_PRICES'][0]['PRICE'] / 100) * 105, 2) . ' руб.';
        $arResult['OFFERS'][$tKey]['ITEM_PRICES'][0]['PRINT_RATIO_DISCOUNT'] = round ( ( (float) $tVal['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2) . ' руб.';
        $arResult['OFFERS'][$tKey]['ITEM_PRICES'][0]['RATIO_DISCOUNT'] = round ( ( (float) $tVal['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2);
        $arResult['OFFERS'][$tKey]['ITEM_PRICES'][0]['DISCOUNT'] = round ( ( (float) $tVal['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2);        
    }

}

foreach ( $arResult['JS_OFFERS'] as $tKey => $tVal )
{
    if ( $arResult['JS_OFFERS'][$tKey]['ITEM_PRICES'][0]['PERCENT'] == 0 )
    {
        $arResult['JS_OFFERS'][$tKey]['ITEM_PRICES'][0]['PERCENT'] = 5;
        $arResult['JS_OFFERS'][$tKey]['ITEM_PRICES'][0]['RATIO_BASE_PRICE'] = round ( ( (float) $tVal['ITEM_PRICES'][0]['PRICE'] / 100) * 105, 2);
        $arResult['JS_OFFERS'][$tKey]['ITEM_PRICES'][0]['PRINT_DISCOUNT'] = round ( ( (float) $tVal['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2) . ' руб.';
        $arResult['JS_OFFERS'][$tKey]['ITEM_PRICES'][0]['RATIO_DISCOUNT'] = round ( ( (float) $tVal['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2);
        $arResult['JS_OFFERS'][$tKey]['ITEM_PRICES'][0]['DISCOUNT'] = round ( ( (float) $tVal['ITEM_PRICES'][0]['PRICE'] / 100) * 5, 2);        
    }

}