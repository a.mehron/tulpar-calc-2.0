<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$this->setFrameMode(false);
?><?='<?xml version="1.0" encoding="'.SITE_CHARSET.'"?>'?>
<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" xmlns:turbo="http://turbo.yandex.ru" version="2.0">
	<channel>
        <title>Статьи</title>
        <link><?="https://".$arResult["SERVER_NAME"]?></link>
        <description><?=strlen($arResult["SECTION"]["DESCRIPTION"])>0?$arResult["SECTION"]["DESCRIPTION"]:$arResult["DESCRIPTION"]?></description>
        <yandex:analytics type="Yandex" id="53011426"/>

	<?foreach($arResult["ITEMS"] as $arItem):?>
	<?$arItem['ELEMENT']['DETAIL_TEXT'] = preg_replace('/(<img.+?>)/iu','<figure>$1</figure>', $arItem['ELEMENT']['DETAIL_TEXT']);?>
	<?$arItem['ELEMENT']['DETAIL_TEXT'] = preg_replace('~<a\b[^>]*+>|</a\b[^>]*+>~','', $arItem['ELEMENT']['DETAIL_TEXT']);?> 
        <item turbo="true">
			<title><?=$arItem["title"]?></title>
            <link><?=$arItem["link"]?></link>
            <author><?=$arResult["SERVER_NAME"]?></author>
            <category><?=$arItem["category"]?></category>
            <pubDate><?=$arItem["pubDate"]?></pubDate>
            <turbo:content>
                <![CDATA[
					<?=$arItem['ELEMENT']['DETAIL_TEXT']?>
				]]>
            </turbo:content>
        </item>

	<?endforeach?>

    </channel>
</rss>
<?die();?>