<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<pre>
	<?//print_r($arResult['PROPERTIES']);?>
	<?//CFile::GetPath($arResult['PROPERTIES']['ATT_BLOG_FILE_ONE']['VALUE']);?>
</pre>

<?php if ($arResult['PROPERTIES']['ATT_BLOG_IS_CUSTOM']['VALUE'] != "Да"): ?>

	<div class="article-detail">
		<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
			<img
				class="detail_picture"
				border="0"
				src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>"
				width="<?=$arResult["DETAIL_PICTURE"]["WIDTH"]?>"
				height="<?=$arResult["DETAIL_PICTURE"]["HEIGHT"]?>"
				alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>"
				title="<?=$arResult["DETAIL_PICTURE"]["TITLE"]?>"
				/>
		<?endif?>
		<?if($arParams["DISPLAY_DATE"]!="N" && $arResult["DISPLAY_ACTIVE_FROM"]):?>
			<span class="news-date-time"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
		<?endif;?>
		<?if($arParams["DISPLAY_PREVIEW_TEXT"]!="N" && $arResult["FIELDS"]["PREVIEW_TEXT"]):?>
			<p><?=$arResult["FIELDS"]["PREVIEW_TEXT"];unset($arResult["FIELDS"]["PREVIEW_TEXT"]);?></p>
		<?endif;?>
		<?if($arResult["NAV_RESULT"]):?>
			<?if($arParams["DISPLAY_TOP_PAGER"]):?><?=$arResult["NAV_STRING"]?><br /><?endif;?>
			<?echo $arResult["NAV_TEXT"];?>
			<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?><br /><?=$arResult["NAV_STRING"]?><?endif;?>
		<?elseif(strlen($arResult["DETAIL_TEXT"])>0):?>
			<?echo $arResult["DETAIL_TEXT"];?>
		<?else:?>
			<?echo $arResult["PREVIEW_TEXT"];?>
		<?endif?>


		<?foreach($arResult["FIELDS"] as $code=>$value):
			if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
			{
				?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
				if (!empty($value) && is_array($value))
				{
					?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
				}
			}
			else
			{
				?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
			}
			?>
		<?endforeach;
		if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
		{
			?>
			<div class="news-detail-share">
				<noindex>
				<?
				$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
						"HANDLERS" => $arParams["SHARE_HANDLERS"],
						"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
						"PAGE_TITLE" => $arResult["~NAME"],
						"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
						"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
						"HIDE" => $arParams["SHARE_HIDE"],
					),
					$component,
					array("HIDE_ICONS" => "Y")
				);
				?>
				</noindex>
			</div>
			<?
		}
		?>
	</div>
<?php else: ?>

	<?$filesToAttach = [];?>

	<?php foreach ($arResult['PROPERTIES']['ATT_BLOG_FILES']['VALUE'] as $fileID): ?>
	<?php $filesToAttach[] = substr(CFile::GetPath($fileID), 1); ?>
	<?php endforeach; ?>

	<?$fileNames = [];?>
	<?php foreach ($arResult['PROPERTIES']['ATT_BLOG_FILE_NAMES']['VALUE'] as $fileName): ?>
	<?php $fileNames[] =  $fileName;?>
	<?php endforeach; ?>

	<div class="custom-article">
		<h1><?=$arResult['NAME']?></h1>

		<div class="row">
			<div class="col-md-6 left-block">
				<?=$arResult['PROPERTIES']['ATT_BLOG_LEFT_BLOCK']['~VALUE']['TEXT']?>
			</div>
			<div class="col-md-6 right-block">
				<div class="image">
					<img src="<?=CFile::GetPath($arResult['PROPERTIES']['ATT_BLOG_RIGHT_BLOCK_IMAGE']['VALUE'])?>" alt="">
				</div>

				<div class="form-wrapper text-center">
					<button data-fancybox data-src="#instruction-enquiry" class="btn btn-warning"><?=$arResult['PROPERTIES']['ATT_BLOG_FORM_NAME']['VALUE']?></button>

					<div style="display: none;" class="fancybox-cub" id="instruction-enquiry">
					    <div class="callback_show_on_open">
					        <h2><?=$arResult['PROPERTIES']['ATT_BLOG_FORM_NAME']['VALUE']?></h2>

					        <div class="attention">
					            На вашу почту будет отправлен pdf файл
					        </div>
					    </div>

					    <form id="instruction-form" data-post-id="<?=$arResult['ID']?>">
									<input type="hidden" name="modal-post-title" value="<?=$arResult['NAME']?>">
									<input type="hidden" name="modal-file-location" value='<?=json_encode($filesToAttach);?>'>
									<input type="hidden" name="modal-file-names" value='<?=json_encode($fileNames);?>'>
									<input type="hidden" name="modal-email-body" value="<?=$arResult['PROPERTIES']['ATT_BLOG_EMAIL_BODY_TEXT']['VALUE']?>">
					        <div class="form-group row">
					          <label for="instruction_appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
					          <div class="col-sm-9">
					            <input type="text" class="form-control" required id="instruction_appl_name" name="instruction_appl_name" placeholder="Иванов Иван Иванович">
					          </div>
					        </div>
					      <div class="form-group row">
					        <label for="instruction_appl_email" class="col-sm-3 col-form-label">E-mail<span class="red">*</span></label>
					        <div class="col-sm-9">
					          <input type="email" class="form-control" required id="instruction_appl_email" name="instruction_appl_email" placeholder="example@mail.com">
					        </div>
					      </div>

					      <label class="custom-control custom-control--flex custom-checkbox">
					            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
					            <span class="custom-control__indicator"></span>
					            <span class="custom-control__description">Я даю свое <a href="/agreement" target="_blank">согласие на обработку</a> моих персональных данных.</span>
					      </label>
					    </form>

					    <div class="success-msg">
					        <h2 class="sent-title">Ваша заявка отправлена</h2>
					    </div>

					    <div class="modal-footer">
					        <div class="col-12 footer-default">
					            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
					            <button type="submit" form="instruction-form" class="btn btn-block">ОТПРАВИТЬ</button>
					        </div>
					        <div class=" col-12 d-none">
					            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</div>

		<div class="main-text">
			<?=$arResult["DETAIL_TEXT"]?>
		</div>

		<div class="open-form">
			<h3><img src="/local/templates/tulpar_store/assets/img/icons/download.svg" alt=""><?=$arResult['PROPERTIES']['ATT_BLOG_FORM_NAME']['VALUE']?></h3>
			<p><?=$arResult['PROPERTIES']['ATT_BLOG_OPEN_FORM_DESC']['VALUE']?></p>
			<form id="instruction-open-form" data-post-id="<?=$arResult['ID']?>">
				<input type="hidden" name="modal-post-title" value="<?=$arResult['NAME']?>">
				<input type="hidden" name="modal-file-location" value='<?=json_encode($filesToAttach);?>'>
				<input type="hidden" name="modal-file-names" value='<?=json_encode($fileNames);?>'>
				<input type="hidden" name="modal-email-body" value="<?=$arResult['PROPERTIES']['ATT_BLOG_EMAIL_BODY_TEXT']['VALUE']?>">

				<input type="text" name="instruction_appl_name" required="" placeholder="Ваше имя*" value="">
				<input type="email" name="instruction_appl_email" required="" placeholder="Ваш e-mail*" value="">
				<input type="submit" class="btn btn-warning" name="" value="<?=$arResult['PROPERTIES']['ATT_BLOG_FORM_NAME']['VALUE']?>">
		</form>
		</div>
	</div>
<?php endif; ?>

<?php // TODO: 1. Email body from admin - DONE
			//			 2. Files from admin - DONE
			//			 3. Multiple files - DONE
			//       4. Set up bottom form - DONE
 ?>
