<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div class="row">
	<div class="col p-0">
		<div class="news-detail bg-white">
			<?if($arParams["DISPLAY_PICTURE"]!="N" && is_array($arResult["DETAIL_PICTURE"])):?>
				<div class="news-image rounded border" style="background:url('<?=$arResult['DETAIL_PICTURE']['SRC']?>') no-repeat center / cover;">
					<div class="row justify-content-center news-title m-0 w-100">
						<div class="col-6 text-white">
							<span class="news-date-time text-white fw-200"><?=$arResult["DISPLAY_ACTIVE_FROM"]?></span>
							<h3><?=$arResult["NAME"]?></h3>
						</div>
					</div>
				</div>
			<?endif?>

			<div class="row justify-content-center m-0 news-body pt-4 pb-4">
				<div class="col-6">
					<?if(strlen($arResult["DETAIL_TEXT"])>0):?>
						<?echo $arResult["DETAIL_TEXT"];?>
					<?else:?>
						<?echo $arResult["PREVIEW_TEXT"];?>
					<?endif?>
					
				</div>

			</div>



			<?foreach($arResult["FIELDS"] as $code=>$value):
				if ('PREVIEW_PICTURE' == $code || 'DETAIL_PICTURE' == $code)
				{
					?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?
					if (!empty($value) && is_array($value))
					{
						?><img border="0" src="<?=$value["SRC"]?>" width="<?=$value["WIDTH"]?>" height="<?=$value["HEIGHT"]?>"><?
					}
				}
				else
				{
					?><?=GetMessage("IBLOCK_FIELD_".$code)?>:&nbsp;<?=$value;?><?
				}
				?>
			<?endforeach;
			foreach($arResult["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>

				<?=$arProperty["NAME"]?>:&nbsp;
				<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
					<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
				<?else:?>
					<?=$arProperty["DISPLAY_VALUE"];?>
				<?endif?>
				<br />
			<?endforeach;
			if(array_key_exists("USE_SHARE", $arParams) && $arParams["USE_SHARE"] == "Y")
			{
				?>
				<div class="news-detail-share">
					<noindex>
					<?
					$APPLICATION->IncludeComponent("bitrix:main.share", "", array(
							"HANDLERS" => $arParams["SHARE_HANDLERS"],
							"PAGE_URL" => $arResult["~DETAIL_PAGE_URL"],
							"PAGE_TITLE" => $arResult["~NAME"],
							"SHORTEN_URL_LOGIN" => $arParams["SHARE_SHORTEN_URL_LOGIN"],
							"SHORTEN_URL_KEY" => $arParams["SHARE_SHORTEN_URL_KEY"],
							"HIDE" => $arParams["SHARE_HIDE"],
						),
						$component,
						array("HIDE_ICONS" => "Y")
					);
					?>
					</noindex>
				</div>
				<?
			}
			?>
		</div>
	</div>
</div>
