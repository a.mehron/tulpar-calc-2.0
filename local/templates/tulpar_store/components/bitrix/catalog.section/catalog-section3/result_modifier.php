<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$plityMainUrl = [
    11808 =>  "plity-perekrytiya-uniblock.html",
    20495 => "plity-perekrytiya-kulonstroy.html",
    20496 => "plity-perekrytiya-kamgeszyab.html",
    20494 => "plity-perekrytiya-raff.html",
];

if ($arResult["ID"] == 906) {
    foreach ($arResult["ITEMS"] as $key => $item) {
        $url = $plityMainUrl[$item['PROPERTIES']['ATT_MAIN_PLITY']['VALUE']];
        $arResult["ITEMS"][$key]["DETAIL_PAGE_URL_NEW"] = "/catalog/plity-perekrytiya/$url?height=" . $item["PROPERTIES"]["ATT_ZHB_PLATE_HEIGHT_MM"]["VALUE"] . "&width=" . $item["PROPERTIES"]["ATT_ZHB_PLATE_WIDTH_MM"]["VALUE"] . "&length=" . $item["PROPERTIES"]["ATT_ZHB_PLATE_LENGTH_MM"]["VALUE"]."&id=".$item["ID"];
    }

}

if (in_array($arResult['ID'], array(276,277,278,279,625,285,697,286,544,269,270,283,627,284,707,538,272,626,118,271,698,714,221,219,222,168,183,220,159,177,200,167,176,567,997,574,575,158,185,187,507,577,784,582,178,585,796,509,622,372,584,581,800,508,368,783,781,799,578,583,301,288,300,1008,601,291,638,1004,292,714,297,401,620,780,596,293,621,579,1003,398,298,296,512,597,402,629,592,397,179,997,396,785,392,513,1006,507,369,594,1007,321,797,590,395,557,390,393,782,294,798,400,394,295,345,580,414,593,416,622,588,1005,299,399,591,417,574))) {
    $arResult['IPROPERTY_VALUES']['SECTION_META_TITLE'] = $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']." купить в Казани - Тулпар";
}


//echo "<pre>";
//print_r($arResult);
//echo "</pre>";