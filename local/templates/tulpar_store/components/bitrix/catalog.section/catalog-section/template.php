<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>

<?if($arParams["DISPLAY_TOP_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>


	<div class="row small-item-cards m-0">
		<?foreach($arResult["ITEMS"] as $cell=>$arElement):?>
		<?
		$this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
		$this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
		?>

		<div id="<?=$this->GetEditAreaId($arElement['ID']);?>" class="col small-item-card border">
			<div class="label-compare">
				<div>
					<!-- <span class="label text-white label-sale pl-2 pr-2">Распродажа</span> -->
				</div>
				<span class="bmr-cmp">
					<a class="bookmark" href="#"></a>
					<a class="compare" href="#"></a>
				</span>
			</div>

			<div class="image">
				<a href="<?=$arElement["DETAIL_PAGE_URL"]?>" title="<?=$arElement["NAME"]?>">
					<img src="<?=$arElement["PREVIEW_PICTURE"]["SRC"]?>" alt="<?=$arElement["NAME"]?>"/>
				</a>
			</div>

			<div class="brand-name"><?=$arElement['PROPERTIES']['ATT_MAKER']['VALUE']?></div>
			<div class="item-name"><?=$arElement["NAME"]?></div>
			<div class="artikul">Арт: <span><?=$arElement['PROPERTIES']['ATT_ARTIKUL']['VALUE']?></span></div>

			<div class="price"><?=$arElement["PRICES"]['BASE']['VALUE']?> р.</div>
			<a class="readmore-item rounded pt-2 pb-2" href="<?=$arElement["DETAIL_PAGE_URL"]?>" title="<?=$arElement["NAME"]?>">Подробнее</a>

		</div>
		<?endforeach;?>
	</div>

<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
	<?=$arResult["NAV_STRING"]?>
<?endif;?>
