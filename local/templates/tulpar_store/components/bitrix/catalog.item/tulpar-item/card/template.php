<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<?php foreach ($item['OFFERS'] as $offer): ?>
	<div class="product-item">
		<a class="product-item-image-wrapper" href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$imgTitle?>"
			data-entity="image-wrapper">
			<span class="product-item-image-slider-slide-container slide" id="<?=$itemIds['PICT_SLIDER']?>"
				<?=($showSlider ? '' : 'style="display: none;"')?>
				data-slider-interval="<?=$arParams['SLIDER_INTERVAL']?>" data-slider-wrap="true">
				<?
				if ($showSlider)
				{
					foreach ($morePhoto as $key => $photo)
					{
						?>
						<span class="product-item-image-slide item <?=($key == 0 ? 'active' : '')?>"
							style="background-image: url('<?=$photo['SRC']?>');">
						</span>
						<?
					}
				}
				?>
			</span>
			<span class="product-item-image-original" id="<?=$itemIds['PICT']?>"
				style="background-image: url('<?=$item['PREVIEW_PICTURE']['SRC']?>'); <?=($showSlider ? 'display: none;' : '')?>">
			</span>
			<?
			if ($item['SECOND_PICT'])
			{
				$bgImage = !empty($item['PREVIEW_PICTURE_SECOND']) ? $item['PREVIEW_PICTURE_SECOND']['SRC'] : $item['PREVIEW_PICTURE']['SRC'];
				?>
				<span class="product-item-image-alternative" id="<?=$itemIds['SECOND_PICT']?>"
					style="background-image: url('<?=$bgImage?>'); <?=($showSlider ? 'display: none;' : '')?>">
				</span>
				<?
			}

			if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
			{
				?>
				<div class="product-item-label-ring <?=$discountPositionClass?>" id="<?=$itemIds['DSC_PERC']?>"
					<?=($price['PERCENT'] > 0 ? '' : 'style="display: none;"')?>>
					<span><?=-$price['PERCENT']?>%</span>
				</div>
				<?
			}

			if ($item['LABEL'])
			{
				?>
				<div class="product-item-label-text <?=$labelPositionClass?>" id="<?=$itemIds['STICKER_ID']?>">
					<?
					if (!empty($item['LABEL_ARRAY_VALUE']))
					{
						foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value)
						{
							?>
							<div<?=(!isset($item['LABEL_PROP_MOBILE'][$code]) ? ' class="hidden-xs"' : '')?>>
								<span title="<?=$value?>"><?=$value?></span>
							</div>
							<?
						}
					}
					?>
				</div>
				<?
			}
			?>
			<div class="product-item-image-slider-control-container" id="<?=$itemIds['PICT_SLIDER']?>_indicator"
				<?=($showSlider ? '' : 'style="display: none;"')?>>
				<?
				if ($showSlider)
				{
					foreach ($morePhoto as $key => $photo)
					{
						?>
						<div class="product-item-image-slider-control<?=($key == 0 ? ' active' : '')?>" data-go-to="<?=$key?>"></div>
						<?
					}
				}
				?>
			</div>
			<?
			if ($arParams['SLIDER_PROGRESS'] === 'Y')
			{
				?>
				<div class="product-item-image-slider-progress-bar-container">
					<div class="product-item-image-slider-progress-bar" id="<?=$itemIds['PICT_SLIDER']?>_progress_bar" style="width: 0;"></div>
				</div>
				<?
			}
			?>
		</a>

		<div class="product-item-title">
			<a href="<?=$item['DETAIL_PAGE_URL']?>" title="<?=$productTitle?>"><? echo $productTitle.$offer['ID']?></a>
		</div>

		<div class="price">
			<?=$offer['PRICES']['BASE']['VALUE']?>
		</div>

		<?php
		if (!empty($arParams['PRODUCT_BLOCKS_ORDER']))
		{
			foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName)
			{
				switch ($blockName)
				{
					case 'sku':
						if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP']))
						{
							?>
							<div id="<?=$itemIds['PROP_DIV']?>">
								<?
								foreach ($arParams['SKU_PROPS'] as $skuProperty)
								{
									$propertyId = $skuProperty['ID'];
									$skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
									if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
										continue;
									?>
									<div class="product-item-info-container product-item-hidden" data-entity="sku-block">
										<div class="product-item-scu-container" data-entity="sku-line-block">
											<?=$skuProperty['NAME']?>
											<div class="product-item-scu-block">
												<div class="product-item-scu-list">
													<ul class="product-item-scu-item-list">
														<?
														foreach ($skuProperty['VALUES'] as $value)
														{
															if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
																continue;

															$value['NAME'] = htmlspecialcharsbx($value['NAME']);

															if ($skuProperty['SHOW_MODE'] === 'PICT')
															{
																?>
																<li class="product-item-scu-item-color-container" title="<?=$value['NAME']?>"
																	data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
																	<div class="product-item-scu-item-color-block">
																		<div class="product-item-scu-item-color" title="<?=$value['NAME']?>"
																			style="background-image: url('<?=$value['PICT']['SRC']?>');">
																		</div>
																	</div>
																</li>
																<?
															}
															else
															{
																?>
																<li class="product-item-scu-item-text-container" title="<?=$value['NAME']?>"
																	data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
																	<div class="product-item-scu-item-text-block">
																		<div class="product-item-scu-item-text"><?=$value['NAME']?></div>
																	</div>
																</li>
																<?
															}
														}
														?>
													</ul>
													<div style="clear: both;"></div>
												</div>
											</div>
										</div>
									</div>
									<?
								}
								?>
							</div>
							<?
							foreach ($arParams['SKU_PROPS'] as $skuProperty)
							{
								if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
									continue;

								$skuProps[] = array(
									'ID' => $skuProperty['ID'],
									'SHOW_MODE' => $skuProperty['SHOW_MODE'],
									'VALUES' => $skuProperty['VALUES'],
									'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
								);
							}

							unset($skuProperty, $value);

							if ($item['OFFERS_PROPS_DISPLAY'])
							{
								foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer)
								{
									$strProps = '';

									if (!empty($jsOffer['DISPLAY_PROPERTIES']))
									{
										foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty)
										{
											$strProps .= '<dt>'.$displayProperty['NAME'].'</dt><dd>'
												.(is_array($displayProperty['VALUE'])
													? implode(' / ', $displayProperty['VALUE'])
													: $displayProperty['VALUE'])
												.'</dd>';
										}
									}

									$item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
								}
								unset($jsOffer, $strProps);
							}
						}

						break;
				}
			}
		}
		 ?>
	</div>
<?php endforeach; ?>
