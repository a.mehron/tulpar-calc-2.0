<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<div class="product-item">
	<div class="label-compare">
		<div>
			<!-- <span class="label text-white label-sale pl-2 pr-2">Распродажа</span> -->
		</div>
		<!-- <span class="bmr-cmp">
			<a class="bookmark" element-id="<?//=$item['ID']?>" href="#"></a>
			<a class="compare" href="#"></a>
		</span> -->
		<a href="#" class="remove-compare" element-id="<?=$item['ID']?>"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/close.svg" alt=""></a>
	</div>
	<?
	if(!CModule::IncludeModule("iblock"))

	return;
	$offer_url = "";
	$res = CIBlockElement::GetByID($item['PROPERTIES']['ATT_RELATED_MAIN_ITEM']['VALUE']);
	if($ar_res = $res->GetNext())
		$offer_url = $ar_res['DETAIL_PAGE_URL']."?OFFER_ID=".$ar_res['ID'];

		if ($offer_url != "") {
			$page_url = $offer_url;
		} else {
			$page_url = $item['DETAIL_PAGE_URL'];
		}

	?>
<div class="product-image-color">
	<a class="product-item-image-wrapper" href="<?=$page_url?>" title="<?=$imgTitle?>"
		data-entity="image-wrapper">
		<span class="product-item-image-slider-slide-container slide" id="<?=$itemIds['PICT_SLIDER']?>"
			<?=($showSlider ? '' : 'style="display: none;"')?>
			data-slider-interval="<?=$arParams['SLIDER_INTERVAL']?>" data-slider-wrap="true">
			<?
			if ($showSlider)
			{
				foreach ($morePhoto as $key => $photo)
				{
					?>
					<span class="product-item-image-slide item <?=($key == 0 ? 'active' : '')?>"
						style="background-image: url('<?=$photo['SRC']?>');">
					</span>
					<?
				}
			}
			?>
		</span>
		<span class="product-item-image-original" id="<?=$itemIds['PICT']?>"
			style="background-image: url('<?=$item['PREVIEW_PICTURE']['SRC']?>'); <?=($showSlider ? 'display: none;' : '')?>">
		</span>


	</a>
</div>

<div class="list-middle-block">
	<div class="item-name">
		<a href="<?=$page_url?>" title="<?=$productTitle?>"><?=$productTitle?></a>
	</div>
	<div class="price"><?=$price['PRINT_RATIO_BASE_PRICE']?></div>

	<div class="props">
		<div class="prop-item"><?=$item['PROPERTIES']['ATT_MAKER']['VALUE']?></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_THICKNESS_MM']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_SHEET_WIDTH']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_SHEET_LENGTH_MM']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_PROFIL_HEIGHT_MM']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_WAVE_INCR_MM']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_COLOR_STOYKOST']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_COROSION_RESISTANCE']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_COATING_CLASS']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_COATING_TYPE']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_COATING_KIND']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_WATERPROOF_CF']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_VAPOURPROOF_CF']['VALUE']?></span></div>
		<div class="prop-item triple_line"><span><?=$item['PROPERTIES']['ATT_MECHANIC_RESISTANCE']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_GLOSS']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_MAX_TEMP']['VALUE']?></span></div>
		<div class="prop-item"><span><?=$item['PROPERTIES']['ATT_WARRANTY_YEARS']['VALUE']?></span></div>
	</div>
</div>
</div>
