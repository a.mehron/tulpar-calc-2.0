<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<div class="product-item">
	<div class="label-compare">
		<div>
			<!-- <span class="label text-white label-sale pl-2 pr-2">Распродажа</span> -->
		</div>
		<span class="bmr-cmp">
			<a class="bookmark" state="" element-id="<?=$item['ID']?>" href="#"></a>
			<a class="compare" state="" element-id="<?=$item['ID']?>" href="#"></a>
		</span>
	</div>
	<?
	if(!CModule::IncludeModule("iblock"))

	return;
	$offer_url = "";
	$res = CIBlockElement::GetByID($item['PROPERTIES']['ATT_RELATED_MAIN_ITEM']['VALUE']);
	if($ar_res = $res->GetNext()) {
		$offer_url = $ar_res['DETAIL_PAGE_URL']."?OFFER_ID=".$ar_res['ID'];
	}

	if ($offer_url != "") {
		$page_url = $offer_url;
	} else {
		$page_url = $item['DETAIL_PAGE_URL'];
	}

	if ($item["IBLOCK_SECTION_ID"] == 906): $page_url = $item["DETAIL_PAGE_URL_NEW"]; endif;


    ?>
<div class="product-image-color">
	<?php if ( $item['PROPERTIES']['TAGS']['VALUE'] ): ?>
	<div class="products-tags">
		<?php foreach ( $item['PROPERTIES']['TAGS']['VALUE'] as $arTagKey => $arTag ): ?>
		<span class="products-tag color-<?=$item['PROPERTIES']['TAGS']['VALUE_XML_ID'][$arTagKey]?>"><?php echo $arTag; ?></span>
		<?php endforeach; ?>
	</div>
	<?php endif; ?>
	<a class="product-item-image-wrapper" href="<?=$page_url?>" title="<?=$imgTitle?>"
		data-entity="image-wrapper">
		<span class="product-item-image-slider-slide-container slide" id="<?=$itemIds['PICT_SLIDER']?>"
			<?=($showSlider ? '' : 'style="display: none;"')?>
			data-slider-interval="<?=$arParams['SLIDER_INTERVAL']?>" data-slider-wrap="true">
			<?
			if ($showSlider)
			{
				foreach ($morePhoto as $key => $photo)
				{
					?>
					<span class="product-item-image-slide item <?=($key == 0 ? 'active' : '')?>"
						style="background-image: url('<?=$photo['SRC']?>');">
					</span>
					<?
				}
			}
			?>
		</span>
		<?$img_preview = CFile::ResizeImageGet($item['PREVIEW_PICTURE']["ID"], array('width'=>195, 'height'=>175), BX_RESIZE_IMAGE_PROPORTIONAL, true);?>
		<span class="product-item-image-original" id="<?=$itemIds['PICT']?>"
			style="background-image: url('<?=$img_preview['src']?>'); <?=($showSlider ? 'display: none;' : '')?>">
		</span>
		<?
		if ($item['SECOND_PICT'])
		{
			$bgImage = !empty($item['PREVIEW_PICTURE_SECOND']) ? $item['PREVIEW_PICTURE_SECOND']['SRC'] : $item['PREVIEW_PICTURE']['SRC'];
			?>
			<span class="product-item-image-alternative" id="<?=$itemIds['SECOND_PICT']?>"
				 style="background-image: url('<?=$img_preview['src']?>'); <?=($showSlider ? 'display: none;' : '')?>">
			</span>
			<?
		}

		if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y')
		{
			?>
			<div class="product-item-label-ring <?=$discountPositionClass?>" id="<?=$itemIds['DSC_PERC']?>"
				<?=($price['PERCENT'] > 0 ? '' : 'style="display: none;"')?>>
				<span><?=-$price['PERCENT']?>%</span>
			</div>
			<?
		}

		if ($item['LABEL'])
		{
			?>
			<div class="product-item-label-text <?=$labelPositionClass?>" id="<?=$itemIds['STICKER_ID']?>">
				<?
				if (!empty($item['LABEL_ARRAY_VALUE']))
				{
					foreach ($item['LABEL_ARRAY_VALUE'] as $code => $value)
					{
						?>
						<div<?=(!isset($item['LABEL_PROP_MOBILE'][$code]) ? ' class="hidden-xs"' : '')?>>
							<span title="<?=$value?>"><?=$value?></span>
						</div>
						<?
					}
				}
				?>
			</div>
			<?
		}
		?>
		<div class="product-item-image-slider-control-container" id="<?=$itemIds['PICT_SLIDER']?>_indicator"
			<?=($showSlider ? '' : 'style="display: none;"')?>>
			<?
			if ($showSlider)
			{
				foreach ($morePhoto as $key => $photo)
				{
					?>
					<div class="product-item-image-slider-control<?=($key == 0 ? ' active' : '')?>" data-go-to="<?=$key?>"></div>
					<?
				}
			}
			?>
		</div>
		<?
		if ($arParams['SLIDER_PROGRESS'] === 'Y')
		{
			?>
			<div class="product-item-image-slider-progress-bar-container">
				<div class="product-item-image-slider-progress-bar" id="<?=$itemIds['PICT_SLIDER']?>_progress_bar" style="width: 0;"></div>
			</div>
			<?
		}
		?>
	</a>
</div>

<div class="list-middle-block">
	<div class="list-top-line">
		<div class="maker">Производитель: <span><?=$item['PROPERTIES']['ATT_MAKER']['VALUE']?></span></div>
		<div class="artikul">
			Код товара: <span>100<?=$item['ID']?></span>
		</div>
		<div class="label-compare">
			<div>
				<!-- <span class="label text-white label-sale pl-2 pr-2">Распродажа</span> -->
			</div>
			<span class="bmr-cmp">
				<a class="bookmark" href="#"></a>
				<a class="compare" href="#"></a>
			</span>
		</div>
	</div>

	<div class="item-name list-item-name">
		<a href="<?=$page_url?>" title="<?=$productTitle?>"><?=$productTitle?></a>
	</div>

	<!-- <div class="few-colors">
		<div class="sku-block-title">
			Цвет
		</div>
		<div class="color-items-wrapper">
		<div class="color-items">
			<?/*
			foreach ($arParams['SKU_PROPS'] as $skuProperty)
			{
				$propertyId = $skuProperty['ID'];
				$skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
				if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
					continue;
				?>
						<?
						$counter = 0;
						foreach ($skuProperty['VALUES'] as $value)
						{
							if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
								continue;

							    if ($counter++ > 4) {
							        $hide_when_card = "hide-when-card";
							    } ?>
									<div class="color <?=$hide_when_card?>" title="<?=$value['NAME']?>">
										<img src="<?=$value['PICT']['SRC']?>" alt="">
									</div>
								<?
						}
						?>
				<?
			} */
			?>
			<div class="color multicolor">
				<img src="<?=SITE_TEMPLATE_PATH?>/assets/img/multi.png" alt="">
			</div>
		</div>
		</div>
	</div> -->

	<div id="<?=$itemIds['PROP_DIV']?>" class="all-colors">
		<?
		foreach ($arParams['SKU_PROPS'] as $skuProperty)
		{
			$propertyId = $skuProperty['ID'];
			$skuProperty['NAME'] = htmlspecialcharsbx($skuProperty['NAME']);
			if (!isset($item['SKU_TREE_VALUES'][$propertyId]))
				continue;
			?>
			<div class="product-item-info-container product-item-hidden" data-entity="sku-block">
				<div class="product-item-scu-container" data-entity="sku-line-block">
					<div class="sku-block-title">
						<?=$skuProperty['NAME']?>
					</div>

					<div class="product-item-scu-block">
						<div class="product-item-scu-list">
							<ul class="product-item-scu-item-list">
								<?
								$color_count = 0;
								foreach ($skuProperty['VALUES'] as $value)
								{
									if (!isset($item['SKU_TREE_VALUES'][$propertyId][$value['ID']]))
										continue;

									$value['NAME'] = htmlspecialcharsbx($value['NAME']);

									if ($skuProperty['SHOW_MODE'] === 'PICT')
									{
										if ($color_count > 4) {
											$visibility = "hidden";
										}
										?>
										<li class="product-item-scu-item-color-container <?=$visibility?>" title="<?=$value['NAME']?>"
											data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
											<div class="product-item-scu-item-color-block">
												<div class="product-item-scu-item-color" title="<?=$value['NAME']?>"
													style="background-image: url('<?=$value['PICT']['SRC']?>');">
												</div>
											</div>
										</li>
										<?
									}
									else
									{
										?>
										<li class="product-item-scu-item-text-container" title="<?=$value['NAME']?>"
											data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" data-onevalue="<?=$value['ID']?>">
											<div class="product-item-scu-item-text-block">
												<div class="product-item-scu-item-text"><?=$value['NAME']?></div>
											</div>
										</li>
										<?
									}
									$color_count++;
								}
								?>
								<?php if ($color_count > 4): ?>
									<li class="product-item-scu-item-color-container multi">
										<div class="product-item-scu-item-color-block">
											<div class="product-item-scu-item-color"
												style="background-image: url('<?=SITE_TEMPLATE_PATH?>/assets/img/multi.png');">
											</div>
										</div>
									</li>
								<?php endif; ?>

							</ul>
							<div style="clear: both;"></div>
						</div>
					</div>
				</div>
			</div>
			<?
		}
		?>
	</div>


	<div class="brand-name"><?=$item['PROPERTIES']['ATT_MAKER']['VALUE']?></div>

	<div class="item-name">
		<a href="<?=$page_url?>" title="<?=$productTitle?>"><?=$productTitle?></a>
	</div>

	<div class="artikul">
		Код товара: <span>100<?=$item['ID']?></span>
	</div>

	<div class="props">
		<div class="title">Характеристики</div>
		<?php /* if ($item['PROPERTIES']['ATT_THICKNESS_MM']['VALUE'] != ""): ?>
			<div class="prop-item">Толщина: <span><?=$item['PROPERTIES']['ATT_THICKNESS_MM']['VALUE']?></span></div>
		<?php endif; */?>

        <?
        if (count($item['DISPLAY_PROPERTIES']) > 0) {
            foreach ($item['DISPLAY_PROPERTIES'] as $property): ?>
                <div class="prop-item"><?=$property['NAME']?>: <span><?=$property['VALUE']?></span></div>
            <?
            endforeach;
        }
        ?>

		<?php if ($item['PROPERTIES']['ATT_COLOR_STOYKOST']['VALUE'] != ""): ?>
			<div class="prop-item">Цветостойкость: <span><?=$item['PROPERTIES']['ATT_COLOR_STOYKOST']['VALUE']?></span></div>
		<?php endif; ?>

		<?php if ($item['PROPERTIES']['ATT_COROSION_RESISTANCE']['VALUE'] != ""): ?>
			<div class="prop-item">Коррозиестойкость: <span><?=$item['PROPERTIES']['ATT_COROSION_RESISTANCE']['VALUE']?></span></div>
		<?php endif; ?>

		<?php /* if ($item['PROPERTIES']['ATT_MECHANIC_RESISTANCE']['VALUE'] != ""): ?>
			<div class="prop-item">Уст. к мех. повреждениям: <span><?=$item['PROPERTIES']['ATT_MECHANIC_RESISTANCE']['VALUE']?></span></div>
		<?php endif; */ ?>

		<?php if ($item['PROPERTIES']['ATT_COATING_CLASS']['VALUE'] != ""): ?>
			<div class="prop-item">Класс покрытия: <span><?=$item['PROPERTIES']['ATT_COATING_CLASS']['VALUE']?></span></div>
		<?php endif; ?>

		<?php if ($item['PROPERTIES']['ATT_COLDRESISTANCE_BRICK']['VALUE'] != ""): ?>
			<div class="prop-item">Морозостойкость, F: <span><?=$item['PROPERTIES']['ATT_COLDRESISTANCE_BRICK']['VALUE']?></span></div>
		<?php endif; ?>

		<?php if ($item['PROPERTIES']['ATT_STRENGTH_MARK']['VALUE'] != ""): ?>
			<div class="prop-item">Марка прочности: <span><?=$item['PROPERTIES']['ATT_STRENGTH_MARK']['VALUE']?></span></div>
		<?php endif; ?>

		<?php if ($item['PROPERTIES']['ATT_DENSITY_MARK_BLOCKS']['VALUE'] != ""): ?>
			<div class="prop-item">Марка плотности: <span><?=$item['PROPERTIES']['ATT_DENSITY_MARK_BLOCKS']['VALUE']?></span></div>
		<?php endif; ?>

		<?php if ($item['PROPERTIES']['ATT_FIRE_RESISTANCE_BRICK']['VALUE'] != ""): ?>
			<div class="prop-item">Огнеупорность, °C: <span><?=$item['PROPERTIES']['ATT_FIRE_RESISTANCE_BRICK']['VALUE']?></span></div>
		<?php endif; ?>

		<?php if ($item['PROPERTIES']['ATT_BRICKS_CONSUMPTION']['VALUE'] != ""): ?>
			<div class="prop-item">Расход, шт/м2: <span><?=$item['PROPERTIES']['ATT_BRICKS_CONSUMPTION']['VALUE']?></span></div>
		<?php endif; ?>

		<?php if ($item['PROPERTIES']['ATT_PODDON_QTY']['VALUE'] != ""): ?>
			<div class="prop-item">Кол-во на поддоне, шт: <span><?=$item['PROPERTIES']['ATT_PODDON_QTY']['VALUE']?></span></div>
		<?php endif; ?>
	</div>

	<?php $mainItemID = $item['PROPERTIES']['ATT_RELATED_MAIN_ITEM']['VALUE'];

	if(CModule::IncludeModule("catalog") && CModule::IncludeModule("iblock")){
		$mxResult = CCatalogSku::GetProductInfo($mainItemID);

		global $arrFilter;
		$arSelect = Array(
						  "IBLOCK_ID",
						  "ID",
						  "PROPERTY_ATT_YOUTUBE_ID",
						  "PROPERTY_ATT_VIDEO_COVER",
						  "PROPERTY_ATT_ZAVOD_YOUTUBE_ID",
						  "PROPERTY_ATT_ZAVOD_VIDEO_COVER"
					  );
		$arFilter = Array(
						"IBLOCK_ID"=>IntVal(4),
						"ID" => $mxResult,
						"ACTIVE_DATE"=>"Y",
						"ACTIVE"=>"Y"
						);

		$res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);

		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();

			if ($arFields['PROPERTY_ATT_YOUTUBE_ID_VALUE'] != "") {
				echo '<div class="video-btns">
						<a data-fancybox href="https://www.youtube.com/watch?v='.$arFields['PROPERTY_ATT_YOUTUBE_ID_VALUE'].'" class="video-btn">
						Видео о продукции
						<img src="'.SITE_TEMPLATE_PATH.'/assets/img/icons/player-play.svg" alt="" />
						<img class="white" src="'.SITE_TEMPLATE_PATH.'/assets/img/icons/player-play-white.svg" alt="" />
						</a>';

				if ($arFields['PROPERTY_ATT_ZAVOD_YOUTUBE_ID_VALUE'] != "") {
					echo '<a data-fancybox href="https://www.youtube.com/watch?v='.$arFields['PROPERTY_ATT_ZAVOD_YOUTUBE_ID_VALUE'].'" class="video-btn">
							Видео о производителе
							<img src="'.SITE_TEMPLATE_PATH.'/assets/img/icons/player-play.svg" alt="" />
							<img class="white" src="'.SITE_TEMPLATE_PATH.'/assets/img/icons/player-play-white.svg" alt="" />
							</a>
						</div>';
				} else {
					echo '</div>';
				}
			}



		}
	}
	?>
</div>

<div class="list-price-block card-view-rm" data-item="<?=$item['ID']?>" data-price-base="<?=$price['RATIO_BASE_PRICE']?>">
	<?
	if (!empty($arParams['PRODUCT_BLOCKS_ORDER']))
	{
		foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName)
		{
			switch ($blockName)
			{
				case 'price': ?>
					<div class="product-item-info-container product-item-price-container" data-entity="price-block">
						<?
						if ($arParams['SHOW_OLD_PRICE'] === 'Y')
						{
							?>
							<span class="product-item-price-old" id="<?=$itemIds['PRICE_OLD']?>"
								<?=($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '')?>>
								<?=$price['PRINT_RATIO_BASE_PRICE']?>
							</span>&nbsp;
							<?
						}
						?>
						<?php if (!empty($price)): ?>
						<span class="price-old-catalog"><?=number_format(($price['RATIO_PRICE'] / 100) * 105, 2, '.', ' ')?> руб.</span>
						<?php endif; ?>
						<span class="product-item-price-current" id="<?=$itemIds['PRICE']?>">
							<?
							if (!empty($price))
							{
								if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers)
								{
									echo Loc::getMessage(
										'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
										array(
											'#PRICE#' => $price['PRINT_RATIO_PRICE'],
											'#VALUE#' => $measureRatio,
											'#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
										)
									);
								}
								else
								{
									echo $price['PRINT_RATIO_PRICE'];
								}
							}
							?>
						</span>
					</div>
					<?
					break;

				case 'sku':
					if ($arParams['PRODUCT_DISPLAY_MODE'] === 'Y' && $haveOffers && !empty($item['OFFERS_PROP']))
					{
						?>

						<?
						foreach ($arParams['SKU_PROPS'] as $skuProperty)
						{
							if (!isset($item['OFFERS_PROP'][$skuProperty['CODE']]))
								continue;

							$skuProps[] = array(
								'ID' => $skuProperty['ID'],
								'SHOW_MODE' => $skuProperty['SHOW_MODE'],
								'VALUES' => $skuProperty['VALUES'],
								'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
							);
						}

						unset($skuProperty, $value);

						if ($item['OFFERS_PROPS_DISPLAY'])
						{
							foreach ($item['JS_OFFERS'] as $keyOffer => $jsOffer)
							{
								$strProps = '';

								if (!empty($jsOffer['DISPLAY_PROPERTIES']))
								{
									foreach ($jsOffer['DISPLAY_PROPERTIES'] as $displayProperty)
									{
										$strProps .= '<dt>'.$displayProperty['NAME'].'</dt><dd>'
											.(is_array($displayProperty['VALUE'])
												? implode(' / ', $displayProperty['VALUE'])
												: $displayProperty['VALUE'])
											.'</dd>';
									}
								}

								$item['JS_OFFERS'][$keyOffer]['DISPLAY_PROPERTIES'] = $strProps;
							}
							unset($jsOffer, $strProps);
						}
					}

					break;
			}
		}
	}
	?>

	<?
	// $res = CIBlockSection::GetByID($item['IBLOCK_SECTION_ID']);
	// if($ar_res = $res->GetNext())
	//   echo $ar_res['IBLOCK_SECTION_ID'];
	?>
	<?
	$i=0;
	$main_section_id = 0;
	$resSection = CIBlockSection::GetNavChain(false, $item['IBLOCK_SECTION_ID']);
	while ($arSection = $resSection->GetNext()) {
	$array_sections = $arSection;
		$main_section_id = $arSection['IBLOCK_SECTION_ID'];
		if ($i==1) {
			break;
		}
		$i++;
	}?>

		<?php if ($item['PROPERTIES']['ATT_PODDON_QTY']['VALUE'] != ""): ?>
			<div class="min-buy-amount">Минимальное кол-во для покупки - 1 поддон</div>
		<?php endif; ?>
		<a class="readmore-item rounded pt-2 pb-2" href="<?=$page_url?>">Подробнее</a>


	<?
	if (
		$arParams['DISPLAY_COMPARE']
		&& (!$haveOffers || $arParams['PRODUCT_DISPLAY_MODE'] === 'Y')
	)
	{
		?>
		<div class="product-item-compare-container">
			<div class="product-item-compare">
				<div class="checkbox">
					<label id="<?=$itemIds['COMPARE_LINK']?>">
						<input type="checkbox" data-entity="compare-checkbox">
						<span data-entity="compare-title"><?=$arParams['MESS_BTN_COMPARE']?></span>
					</label>
				</div>
			</div>
		</div>
		<?
	}
	?>
	</div>
</div>
