<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if(!CModule::IncludeModule("catalog"))

return;

if(!CModule::IncludeModule("iblock"))

return;

use \Bitrix\Conversion\Internals\MobileDetect;
$detect = new MobileDetect;
?>
<div class="banner-slider">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

	$ar_res = CPrice::GetBasePrice($arItem['PROPERTIES']['ATT_PRODUCT_ON_BANNER']['VALUE']);

	$res = CIBlockElement::GetByID($arItem['PROPERTIES']['ATT_PRODUCT_ON_BANNER']['VALUE']);
	if($arr_res = $res->GetNext())
		$prod_section_id = $arr_res['IBLOCK_SECTION_ID'];
	?>

	<?
	$i=0;
	$main_section_id = 0;
	$resSection = CIBlockSection::GetNavChain(false, $prod_section_id);
	$array_sections = array();
	while ($arSection = $resSection->GetNext()) {
		array_push($array_sections, $arSection);
		$main_section_id = $arSection['IBLOCK_SECTION_ID'];
		if ($i==1) {
			break;
		}
		$i++;
	}

	if (sizeof($array_sections) <= 1) {
		$main_section_id = $prod_section_id;
	}

	if ($main_section_id != 16 && $main_section_id != 255) {
		$price_per_unit = " РУБ./ШТ.";
	} elseif ($main_section_id == 255) {
		$price_per_unit = " РУБ./М<sup>3</sup>";
	}

	?>

	<div class="slide">
		<?php if ($arItem['PROPERTIES']['ATT_PRODUCT_LINK_BANNER']['VALUE'] != ""): ?>
			<a href="<?=$arItem['PROPERTIES']['ATT_PRODUCT_LINK_BANNER']['VALUE']?>">
		<?php endif; ?>
		<picture>
			<?php $img = CFile::ResizeImageGet( $arItem['PREVIEW_PICTURE']['ID'], array('width' => 480) ); ?>
			<? /*<source media="(max-width: 480px)" data-srcset="<?=$arItem['DETAIL_PICTURE']['SRC']?>" srcset="<?=$arItem['DETAIL_PICTURE']['SRC']?>">*/?>
			<?php if ( $detect->isMobile() ): ?>
				<img data-lazy="<?=$img['src']?>" alt="">
			<?php else: ?>
			<img data-lazy="<?=$arItem['DETAIL_PICTURE']['SRC']?>" alt="">
			<?php endif; ?>
		</picture>
		<?php if ($arItem['PROPERTIES']['ATT_BANNER_EXPIRY_DATE']['VALUE'] != ""): ?>
			<span class="banner_expiry_date"><?=$arItem['PROPERTIES']['ATT_BANNER_EXPIRY_DATE']['VALUE']?></span>
		<?php endif; ?>
		<?php if ($ar_res['PRICE'] != ""): ?>
			<span class="banner_prod_price"><?=$ar_res['PRICE']?><?=$price_per_unit?></span>
		<?php endif; ?>
		<?php if ($arItem['PROPERTIES']['ATT_PRODUCT_LINK_BANNER']['VALUE'] != ""): ?>
		</a>
		<?php endif; ?>
	</div>

<?endforeach;?>
</div>
