$(document).ready(function(){
    AOS.init();

    $('.input-phone').mask("+7 (999) 999-9999");

    $('.makers__items').slick({
      dots: true,
      slidesPerRow: 6,
      rows: 4,
      responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesPerRow: 5,
          rows: 4,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesPerRow: 4,
          rows: 4,
        }
      },
      {
        breakpoint: 767,
        settings: {
          slidesPerRow: 1,
          rows: 4,
        }
      }
    ]
  });

    $('.landing-form').on("submit", function (e) {
      e.preventDefault();

      $(this).find('input[name="topicLanding[]"]').val($(this).attr('data-topic'));

      $.ajax({
        type: 'post',
        url: '/local/templates/tulpar_store/assets/landing/dist/ajax/landing.php',
        dataType: 'html',
        data: $('form').serialize(),
        success:function() {
          $.fancybox.open('<div class="message"><h4>Спасибо!</h4><p>Ваша заявка отправлена.</p></div>');
          $('.landing-form').trigger('reset');
        }
      });
      
    });

});