$(document).ready(function(){
    AOS.init();

    $('.input-phone').mask("+7 (999) 999-9999");

    $('.makers__items').slick({
        dots: true,
        slidesPerRow: 6,
        rows: 4,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    slidesPerRow: 5,
                    rows: 4,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesPerRow: 4,
                    rows: 4,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesPerRow: 1,
                    rows: 4,
                }
            }
        ]
    });
});