function numberWithSpaces(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
}

function zhbiShowProps(shippingCost, productId) {
    let width;
    let maker;
    if (productId == 11808) {
        width = 1200;
        height = 220;
        maker = 'Uniblock';

        if ($('#form-width-input').is(':checked')) {
            width = 1500;
        }
    } else if (productId == 20494) {
        width = $('#width-select-js').val();
        height = 220;
        maker = 'РАФФ';
    } else if (productId == 20495) {
        width = 1198;
        height = 160;
        maker = 'КулонСтрой';

        if ($('#form-height-input').is(':checked')) {
            height = 220;
        }
    } else if (productId == 20496) {
        width = 990;
        height = 220;
        maker = 'КамгэсЗЯБ';

        if ($('#form-width-input').is(':checked')) {
            width = 1190;
        }
    }

    let length = $('.show input[type="range"]').val();

    if (productId == 20496) {
        if ($('.range-slider-kamges.show input[type="range"]').hasClass('kamges-990')) {
            switch (length) {
                case "100":
                    length = 2380;
                    break;
                case "200":
                    length = 2980;
                    break;
                case "300":
                    length = 4780;
                    break;
                case "400":
                    length = 5080;
                    break;
                case "500":
                    length = 5680;
                    break;
                case "600":
                    length = 5980;
                    break;
                case "700":
                    length = 6280;
                    break;
            }
        } else if ($('.range-slider-kamges.show input[type="range"]').hasClass('kamges-1190')) {
            switch (length) {
                case "100":
                    length = 2380;
                    break;
                case "200":
                    length = 2680;
                    break;
                case "300":
                    length = 2980;
                    break;
                case "400":
                    length = 3580;
                    break;
                case "500":
                    length = 4180;
                    break;
                case "600":
                    length = 4480;
                    break;
                case "700":
                    length = 4780;
                    break;
                case "800":
                    length = 5080;
                    break;
                case "900":
                    length = 5380;
                    break;
                case "1000":
                    length = 5680;
                    break;
                case "1100":
                    length = 5980;
                    break;
                case "1200":
                    length = 6280;
                    break;
                case "1300":
                    length = 7180;
                    break;
            }
        }
    }

    let currentProps;

    for (let i = 0; i < variationsArr.length; i++) {
        if (variationsArr[i].width == width && variationsArr[i].length == length && variationsArr[i].height == height) {

            $("h1.element-name").text(variationsArr[i].name);
            document.title = variationsArr[i].name;
            document.querySelector('meta[name="description"]').setAttribute("content", variationsArr[i].name+" в интернет-магазине Тулпар! Характеристики, описание, различные варианты оплаты, доставка на объект");


            let price = variationsArr[i].price.substr(0, variationsArr[i].price.length - 3);
            $(".product-item-detail-price-current").text(numberWithSpaces(price) + " руб.");

            let priceWithShipping = parseInt(parseInt(price) + (parseInt(shippingCost) / parseInt(variationsArr[i].inTruck)));
            $("#priceWithShipping .product-item-detail-price-current").text(numberWithSpaces(priceWithShipping) + " руб.");


            currentProps = `
                        <dt style="background: rgb(255, 255, 255);">Производитель</dt>
                        <dd style="background: rgb(255, 255, 255);">${maker}</dd>
                        <dt style="background: rgb(255, 255, 255);">Вес, кг</dt>
                        <dd style="background: rgb(255, 255, 255);">${variationsArr[i].weight}</dd>
                        <dt style="background: rgb(255, 255, 255);">Объем, м3</dt>
                        <dd style="background: rgb(255, 255, 255);">${variationsArr[i].volume}</dd>
                        <dt style="background: rgb(255, 255, 255);">Длина, мм</dt>
                        <dd style="background: rgb(255, 255, 255);">${variationsArr[i].length}</dd>
                        <dt style="background: rgb(255, 255, 255);">Ширина, мм</dt>
                        <dd style="background: rgb(255, 255, 255);">${variationsArr[i].width}</dd>
                        <dt style="background: rgb(255, 255, 255);">Высота, мм</dt>
                        <dd style="background: rgb(255, 255, 255);">${variationsArr[i].height}</dd>
                        <dt style="background: rgb(255, 255, 255);">Размеры отверстия, мм</dt>
                        <dd style="background: rgb(255, 255, 255);">${variationsArr[i].holeSize}</dd>
                        <dt style="background: rgb(255, 255, 255);">Нагрузка, кгс/м2</dt>
                        <dd style="background: rgb(255, 255, 255);">${variationsArr[i].pressure}</dd>
                        <dt style="background: rgb(255, 255, 255);">Кол-во на машине 20 т, шт</dt>
                        <dd style="background: rgb(255, 255, 255);">${variationsArr[i].inTruck}</dd>
            `;

            $('.product-item-detail-slider-controls-image').hide();
            $('.product-item-detail-slider-controls-image[data-element-id="'+ variationsArr[i].id +'"]').show();
            $('.product-item-detail-slider-controls-image').removeClass("active");
            $('.product-item-detail-slider-controls-image[data-element-id="'+ variationsArr[i].id +'"]:first').addClass("active");

            let currentActiveThumbnail = $('.product-item-detail-slider-controls-image[data-element-id="'+ variationsArr[i].id +'"]:first').attr("data-value");

            $(".product-item-detail-slider-image.active").removeClass("active");
            $(".product-item-detail-slider-image[data-id="+currentActiveThumbnail+"]").addClass("active");

        }

    }



    $('.product-item-detail-tab-content dl.product-item-detail-properties').html(currentProps);
}


$(document).ready(function() {

    // $(document).on( 'click', '.section-videos a[data-fancybox]', function ()
    // {
        
    // } );
	
	$('select').styler();

    $('input[type="range"]').rangeslider({

        polyfill: false,

        // Default CSS classes
        rangeClass: 'rangeslider',
        disabledClass: 'rangeslider--disabled',
        horizontalClass: 'rangeslider--horizontal',
        verticalClass: 'rangeslider--vertical',
        fillClass: 'rangeslider__fill',
        handleClass: 'rangeslider__handle',

        // Callback function
        onInit: function() {},

        // Callback function
        onSlide: function(position, value) {
            //zhbiShowProps();
        },

        // Callback function
        onSlideEnd: function(position, value) {}
    });

    $(".product-item .color-name").each(function(index) {
        let curColor = $(this).closest(".product-image-color").next(".list-middle-block").find(".color").attr("title");
        $(this).text(curColor);
    });

    $(document).scroll(function () {
        var y = $(this).scrollTop();
        if (y > 144) {
            $('#sticky-header').fadeIn(100);
        } else {
            $('#sticky-header').hide();
        }
    });

    $('#brandpage-nav li a').click(function(ev){
        ev.preventDefault();
        //console.log(this.getAttribute('href'));
        let dest = this.getAttribute('href');
        $('html, body').animate({
           scrollTop: $(dest).offset().top - 80
       }, 500);

    });

    const id1 = document.querySelector('#id-1');
    if (id1 != null) {
        document.addEventListener('scroll', function(){
            if (id1.getBoundingClientRect().top < 100) {
                $('#brandpage-nav li').removeClass('active');
                $('a[href="#id-1"]').closest('li').addClass('active');
            } else {
                $('a[href="#id-1"]').closest('li').removeClass('active');
            }
        });
    }

    const id2 = document.querySelector('#id-2');
    if (id2 != null) {
        document.addEventListener('scroll', function(){
            if (id2.getBoundingClientRect().top < 100) {
                $('#brandpage-nav li').removeClass('active');
                $('a[href="#id-2"]').closest('li').addClass('active');
            } else {
                $('a[href="#id-2"]').closest('li').removeClass('active');
            }
        });
    }

    const id3 = document.querySelector('#id-3');
        if (id3 != null) {
        document.addEventListener('scroll', function(){
            if (id3.getBoundingClientRect().top < 100) {
                $('#brandpage-nav li').removeClass('active');
                $('a[href="#id-3"]').closest('li').addClass('active');
            } else {
                $('a[href="#id-3"]').closest('li').removeClass('active');
            }
        });
    }

    const id4 = document.querySelector('#id-4');
    if (id4 != null) {
        document.addEventListener('scroll', function(){
            if (id4.getBoundingClientRect().top < 100) {
                $('#brandpage-nav li').removeClass('active');
                $('a[href="#id-4"]').closest('li').addClass('active');
            } else {
                $('a[href="#id-4"]').closest('li').removeClass('active');
            }
        });
    }

    const id5 = document.querySelector('#id-5');
    if (id5 != null) {
        document.addEventListener('scroll', function(){
            if (id5.getBoundingClientRect().top < 100) {
                $('#brandpage-nav li').removeClass('active');
                $('a[href="#id-5"]').closest('li').addClass('active');
            } else {
                $('a[href="#id-5"]').closest('li').removeClass('active');
            }
        });
    }


    if ($(window).width() <= 480) {
        
        $(".row.small-item-cards.popular").not('.slick-initialized').slick();
        $(".dropdown-toggle").dropdown("toggle");

        $(".sort-panel").appendTo(".filter-sort-btns");

        $(".nav-tabs .nav-item").click(function(){
            $(".element-tabs .nav-tabs").css({"height":"0"});
            $(".nav-tabs .nav-item").show();
            $(this).hide();
            $("#dropdownMenuButton b").text($(this).text());
        });
    }

    $("#dropdownMenuButton").click(function(){
        $(".element-tabs .nav-tabs").css({"height":"auto"});
    });

    if ($(window).width() <= 480) {
        $(".great-deals .row.small-item-cards").not('.slick-initialized').slick({
            slidesToShow: 1,
            dots:true,
            arrows: false
        });

        $(".catalog-section .row.small-item-cards").removeClass("view-list");
    }


    $(".product-item-detail-tab-content dt").hover(function() {
        $(this).css("background", "#e8e8e8");
        $(this).next("dd").css("background", "#e8e8e8");
    }, function(){
        $(this).css("background", "#fff");
        $(this).next("dd").css("background", "#fff");
    });

    $(".product-item-detail-tab-content dd").hover(function() {
        $(this).css("background", "#e8e8e8");
        $(this).prev("dt").css("background", "#e8e8e8");
    }, function(){
        $(this).css("background", "#fff");
        $(this).prev("dt").css("background", "#fff");
    });

    $(".element-tabs .nav-tabs .nav-link").click(function(){
        $(".element-tabs .nav-tabs .nav-link").removeClass("show");
        $(".element-tabs .nav-tabs .nav-link").removeClass("active");
        $(this).addClass("show");
        $(this).addClass("active");

        $(".tab-content .tab-pane").removeClass("show");
        $(".tab-content .tab-pane").removeClass("active");

        var selected_pane = $(this).attr("id");

        $(".tab-content .tab-pane[aria-labelledby='"+selected_pane+"']").addClass("show");
        $(".tab-content .tab-pane[aria-labelledby='"+selected_pane+"']").addClass("active");
    });

    $(".small-item-card .product-item-scu-item-color-container").click(function(){
        $(".small-item-card .product-item-scu-item-color-container").removeClass("active");
        $(this).addClass("active");
    });


    $(".small-item-card").hover(function(){
        $(this).addClass("hover2");
    }, function() {
        $(this).removeClass("hover2");
    });

    $(".small-item-card .product-item-scu-item-color-container").removeClass("selected");

    
    $('body').on('click',function(event){
       if(!$(event.target).is('.header .contact-us-btn')){
         $(".nav-feedback").removeClass("show");
         $(".header .contact-us-btn").removeClass("pressed");
       }
    });

    $(".side-catalog-wrapper.not-home").hover(function() {
           $(this).find(".catalog-list").addClass("opened");
        }, function(){
            $(this).find(".catalog-list").removeClass("opened");
       }
    );


    $(".side-catalog-wrapper").hover(function() {
          $(".nav-shadow").show();
          $(".side-catalog-wrapper .catalog-name").css("background-color", "#c91343");
        }, function(){
            $(".nav-shadow").hide();
            $(".side-catalog-wrapper .catalog-name").css("background-color", "#ff1654");
       }
    );

    // $(".side-catalog-wrapper .catalog-list li").hover(function() {
    //       $(this).children(".catalog-submenu").delay(1000).show(0);
    //       // var height = $(this).children(".catalog-submenu").outerHeight();
    //       // if ($(this).children(".catalog-submenu").outerHeight() > $(this).closest(".catalog-submenu").outerHeight()) {
    //       //     $(".catalog-list .catalog-submenu").css("height", height);
    //       //     $(".catalog-list").css("height", height);
    //       // }
    //     }, function(){
    //         $(this).children(".catalog-submenu").hide();
    //         // $(".catalog-list").css("height", height);
    //
    //    }
    // );

    // var timer;
    // $(".side-catalog-wrapper .catalog-list li").mouseenter(function() {
    //
    //     var that = this;
    //     timer = setTimeout(function(){
    //         $(that).children(".catalog-submenu").show();
    //     }, 500);
    // }).mouseleave(function(e) {
    //     var targetParent = e.currentTarget.parentElement;
    //
    //     var that = this;
    //     clearTimeout(timer);
    //     setTimeout(function () {
    //         $(that).children(".catalog-submenu").hide();
    //     }, 500);
    // });

    $("#appl_phone").mask("+7 (999) 999-9999");
    
    setTimeout(() => 
    {
        $(".banner-slider").not('.slick-initialized').slick({
            dots: true,
            autoplay: false,
            autoplaySpeed:5000
        });
    }, 500);


    $(".best-promo-items").not('.slick-initialized').slick({
        slidesToShow:3,
        responsive:[
        {
            breakpoint:480,
            settings:{
                arrows:false,
                slidesToShow:1,
                dots: true
            }
        }
        ]
    });

    $(".filter-sort-btns  .btn-info").click(function(){
        $(".filter-container .filter").addClass("mobile-show");
        $(".nav-shadow").show();
        $(".filter-container").css({"position":"absolute", "top":"0"});
    });

    $(".filter-sort-btns .btn-light").click(function(){
        $(".filter-sort-btns .sort-panel").toggle();
    });

    $("#close-filter").click(function(){
        $(".filter-container .filter").removeClass("mobile-show");
        $(".nav-shadow").hide();
        $(".filter-container").removeAttr('style');
    });


    $(".main-page-gallery-slider").not('.slick-initialized').slick({
        infinite:false,
        responsive: [
            {
              breakpoint: 576,
              settings: {
                dots: true,
              }
          }
        ]
    });

    $('[data-fancybox]').fancybox({
    	thumbs : {
    		autoStart : true
    	}
    });

    $(".visualization-3d .readmore-yellow").hover(function(){
        $(this).closest(".slide").css("background-size", "105%");
    }, function(){
        $(this).closest(".slide").css("background-size", "100%");
    });

      $('.useful-article-items').not('.slick-initialized').slick({
          slidesToShow:4,
          responsive: [
              {
                breakpoint: 992,
                settings: {
                  slidesToShow:3
                }
              },
              {
                breakpoint: 576,
                settings: {
                  slidesToShow:2,
                  dots: true,
                  arrows:false
                }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow:1,
                dots: true,
                arrows:false
              }
            }
          ]
      });

      $('.news-items').not('.slick-initialized').slick({
          slidesToShow:4,
          responsive: [
              {
                breakpoint: 992,
                settings: {
                  slidesToShow:3
                }
              },
              {
                breakpoint: 576,
                settings: {
                  slidesToShow:2,
                  dots: true,
                  arrows:false
                }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow:1,
                dots: true,
                arrows:false
              }
            }
          ]
      });


    $(".item-image-thumbnails .image-thumb").click(function() {
        $(this).closest(".item-image-thumbnails").find(".image-thumb.selected").removeClass("selected");
        $(this).addClass("selected");

        let imgtarget = $(this).attr("imgtarget");
        $(".item-image-container .main-image").css("background","url("+imgtarget+") no-repeat center / 100%");
    });

    $(".item-image-container .main-image .next-img").click(function(){
        let imgtarget = $(".item-image-container .item-image-thumbnails .image-thumb.selected").closest(".col-2").next(".col-2").find(".image-thumb").attr("imgtarget");
        $(".item-image-container .item-image-thumbnails .image-thumb.selected").closest(".col-2").next(".col-2").find(".image-thumb").addClass("selected").closest(".col-2").siblings().find(".image-thumb").removeClass("selected");
        $(".item-image-container .main-image").css("background","url("+imgtarget+") no-repeat center / 100%");
    });

    $(".item-image-container .main-image .prev-img").click(function(){
        let imgtarget = $(".item-image-container .item-image-thumbnails .image-thumb.selected").closest(".col-2").prev(".col-2").find(".image-thumb").attr("imgtarget");
        $(".item-image-container .item-image-thumbnails .image-thumb.selected").closest(".col-2").prev(".col-2").find(".image-thumb").addClass("selected").closest(".col-2").siblings().find(".image-thumb").removeClass("selected");
        $(".item-image-container .main-image").css("background","url("+imgtarget+") no-repeat center / 100%");
    });

    $(window).scroll(function () {
       if ($(this).scrollTop() > 100) {
           $('.scrollup').fadeIn();
       } else {
           $('.scrollup').fadeOut();
       }
    });

    $('.scrollup').click(function () {
       $("html, body").animate({
           scrollTop: 0
       }, 600);
       return false;
   });

   $("[type='tel']").mask('+7(999)-999-9999');

   if ($("body").height() < $(window).height()) {
       var difference = $(window).height() - $("body").height();
       var wrapper_height = $(".gray-wrapper").outerHeight();
       var new_height = wrapper_height + difference;
       $(".gray-wrapper").css("height", new_height);
   }

   $('#instruction-form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/send-instruction.php',
            data: $('form').serialize(),
            success:function() {
                $("h2.modal-title").hide();
                $('#instruction-form').hide();
                $('#instruction-enquiry .footer-default').hide();
                $('#instruction-enquiry .success-msg').show();
            }
        });
    });


   $('#zamer-application form').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/send.php',
            data: $('form').serialize(),
            success:function() {
                $("h2.modal-title").hide();
                $('#zamer-application form').hide();
                $('#zamer-application .footer-default').hide();
                $('#zamer-application .success-msg').show();
            }
        });
    });

    $('#credit-application form').on('submit', function(e) {
         e.preventDefault();
         $.ajax({
             type: 'post',
             url: '/send-credit.php',
             data: $('form').serialize(),
             success:function() {
                 $("h2.modal-title").hide();
                 $('#credit-application form').hide();
                 $('#credit-application .footer-default').hide();
                 $('#credit-application .success-msg').show();
             }
         });
     });

     $('#delivery-consult form').on('submit', function(e) {
          e.preventDefault();
          $.ajax({
              type: 'post',
              url: '/local/ajax/delivery_consult_form.php',
              data: $('form').serialize(),
              success:function() {
                  $.fancybox.open('<div class="message"><h4>Спасибо!</h4><p>Ваша заявка отправлена.</p></div>');
                  $('#delivery-consult form').trigger('reset');
              }
          });
      });

      $('#instruction-open-form').on('submit', function(e) {
           e.preventDefault();
           $.ajax({
               type: 'post',
               url: '/send-instruction.php',
               data: $('form').serialize(),
               success:function() {
                   $.fancybox.open('<div class="message"><h4>Спасибо!</h4><p>Ваша заявка отправлена.</p></div>');
                   $('#instruction-open-form').trigger('reset');
               }
           });
       });



    $('#callback-enquiry form').on('submit', function(e) {
         e.preventDefault();
         $.ajax({
             type: 'post',
             url: '/send.php',
             data: $('form').serialize(),
             success:function() {
                 $(".callback_show_on_open").hide();
                 $('#callback-enquiry form').hide();
                 $('#callback-enquiry .footer-default').hide();
                 $('#callback-enquiry .success-msg').show();
             }
         });
     });

     $('#oneclick-buy-modal form').on('submit', function(e) {
          e.preventDefault();
          $.ajax({
              type: 'post',
              url: '/send.php',
              data: $('form').serialize(),
              success:function() {
                  $(".callback_show_on_open").hide();
                  $('#oneclick-buy-modal form').hide();
                  $('#oneclick-buy-modal .footer-default').hide();
                  $('#oneclick-buy-modal .success-msg').show();
              }
          });
      });

     $('#contacts_form').on('submit', function(e) {
          e.preventDefault();
          $.ajax({
              type: 'post',
              url: '/local/ajax/contacts.php',
              data: $('form').serialize(),
              success:function() {
                $("#contactsFormSendSuccessMessage").modal('show');
              }
          });
      });


    $(".product-image-wrapper .main-slider").not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.product-image-wrapper .thumbs-slider'
    });

    $('.product-image-wrapper .thumbs-slider').not('.slick-initialized').slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      asNavFor: '.product-image-wrapper .main-slider',
      dots: true,
      focusOnSelect: true
    });

    // $(".product-item-scu-item-color-container").tooltip();

    $(".sort-view .view-mode .mode").click(function(){
        $('.sort-view .view-mode .mode').removeClass("active");
        $(this).addClass("active");
    });

    $(".sort-view .view-mode .mode.tile").click(function(){
        $(".row.small-item-cards").removeClass("view-list");

        $(".small-item-card.product-item-container").css("height", "auto");
    });

    $(".sort-view .view-mode .mode.list").click(function(){
        $(".row.small-item-cards").addClass("view-list");
    });

    // $(".cart-compare.in-header .menu-btn").click(function(){
    //     $("header .site-menu-top.mobile").removeClass("d-none");
    // });

     $(".header .mobile .site-menu .close-btn").on("tap",function(){
         $("header .site-menu-top.mobile").addClass("d-none");
     });

     $(".c-sub-screen > a").click(function(e){
        e.preventDefault();
     });

     $("#m-nav .m-nav-mobile-list .c-sub-screen > a").click(function(){
        $(this).closest(".c-sub-screen").addClass("is-active");
        $(this).closest(".m-nav-mobile-list").addClass("go-left");
        $("#m-nav .m-nav_back").addClass("is-active");
     });

     $('#m-nav').on('click', '.m-nav_back.is-active', function() {

         var $open_lists = $(".m-nav-mobile-list.go-left");

         $open_lists.each(function() {
             if ($(this).find(".m-nav-mobile-list.go-left").length == 0) {
                 $(this).removeClass("go-left");
             }
         })

         var $open_list_items = $(".m-nav-mobile-list__item.is-active");

         $open_list_items.each(function() {
             if ($(this).find(".m-nav-mobile-list__item.is-active").length == 0) {
                 $(this).removeClass("is-active");
             }
         })

         var $target_list = $('#m-nav').find(".m-nav-mobile-list.go-left").length;
         var $target_list_item = $('#m-nav').find(".m-nav-mobile-list .m-nav-mobile-list__item.is-active").length;

         if ($target_list == 0 && $target_list_item == 0) {
             $(this).removeClass("is-active");
         }

        });

        $(".small-item-card .bookmark").click(function(e){
            e.preventDefault();
            let el_id = $(this).attr("element-id");
            if ($(this).attr("state") != "bookmrkd") {
                $(this).attr("state", "bookmrkd");
                $(this).addClass("picked");

                $.ajax({
                    type: 'post',
                    url: '/local/ajax/add_wish.php',
                    data: {el_id: el_id},
                    success:function(html) {
                        $('#wishcount').html("("+html+")");
                    }
                });
            } else {
                $(this).attr("state", "");
                $(this).removeClass("picked");
                $.ajax({
                    type: 'post',
                    url: '/local/ajax/remove-wish.php',
                    data: {elem_id: el_id},
                    success:function(html) {
                        $('#wishcount').html("("+html+")");
                    }
                });
            }
        });

        $(".small-item-card .remove-wishlist").click(function(e){
            e.preventDefault();
            let elem_id = $(this).attr("element-id");
            let $that = $(this);
            $.ajax({
                type: 'post',
                url: '/local/ajax/remove-wish.php',
                data: {elem_id: elem_id},
                success:function(html) {
                    $('#wishcount').html("("+html+")");
                    $that.closest(".small-item-card").remove();
                }
            });
        });

        $(".small-item-card .compare").click(function(e){
            e.preventDefault();
            let cmp_el_id = $(this).attr("element-id");
            if ($(this).attr("state") != "to_compare") {
                $(this).attr("state", "to_compare");
                $(this).addClass("picked");

                $.ajax({
                    type: 'post',
                    url: '/local/ajax/add_compare.php',
                    data: {cmp_el_id: cmp_el_id},
                    success:function(html, data) {
                        $('#compare_count').html("("+html+")");
                    }
                });
            } else {
                $(this).attr("state", "");
                $(this).removeClass("picked");
                $.ajax({
                    type: 'post',
                    url: '/local/ajax/remove-compare.php',
                    data: {cmp_el_id: cmp_el_id},
                    success:function(html) {
                        $('#compare_count').html("("+html+")");
                    }
                });
            }
        });

        $(".compare_main .catalog-section").not('.slick-initialized').slick({
            slidesToShow:4,
            slidesToScroll:1,
            infinite:false
        });

        $(".small-item-card .remove-compare").on('click', function(e){
            e.preventDefault();
            let cmp_el_id = $(this).attr("element-id");
            let $that = $(this);
            let slideIndex = $that.closest(".slick-slide").index();

            if (slideIndex == 0) {
                slideIndex = 1;
            } else {
                slideIndex = slideIndex + 1;
            }

            $(".compare_main .catalog-section").not('.slick-initialized').slick('slickRemove',slideIndex - 1);
             if (slideIndex !== 0){
               slideIndex--;
             }
            $.ajax({
                type: 'post',
                url: '/local/ajax/remove-compare.php',
                data: {cmp_el_id: cmp_el_id},
                success:function(html) {
                    $('#compare_count').html("("+html+")");

                }
            });
        });

        // $(".product-table-col_remove .close").on('click', function(e){
        //     var offer_id = $(this).closest(".product-table-row").attr("offer-id");
        //     $(this).closest(".product-table-row").remove();
        //
        //     updateTotal();
        //
        //     $.ajax({
        //         type: 'post',
        //         url: '/local/ajax/remove_from_cart.php',
        //         data: {offer_id: offer_id},
        //         success:function(html) {
        //             $('#cart_count').html("("+html+")");
        //         }
        //     });
        // });

        $(".cart-compare.in-header .menu-btn").click(function(){
            $("#m-nav").show();
            $("body").css("overflow-y","hidden");
        });

        $("#m-nav .m-nav_close").click(function(){
            $("#m-nav").hide();
            $("body").css("overflow-y","visible");
        });

        $(".search-page .product-item-container:last-child").hover(function(){
            var height = $(this).children(".product-item").outerHeight() + "px";
            this.style.setProperty( 'height', height, 'important' );
        }, function(){
            $(this).css("height", "auto");
        });

        $(".catalog-section .product-item-container").hover(function(){
            let height = ($(this).children(".product-item").outerHeight()-45) + "px";
            if ($(this).siblings().length == 0) {
                this.style.setProperty( 'height', height, 'important');
            }
        }, function(){
            $(this).css("height", "auto");
        });

        if ($('.all-colors').children().length == 0 ) {
            $('.all-colors').hide();
        }

        $(".result-sort-current .result-sort-text").click(function(){
            $(".result-sort-list").toggle();
        });

        $(".qty-buy .btn-oneclick").click(function(){
            let product_name = $("h1.element-name").text() + " ";
            $(".product-item-scu-item-list .selected > div > div").each(function(){
                product_name += $(this).text() + " ";
            });


            $("#oneclick-buy-modal .product_oneclick").text(product_name);

            $("#oneclick-buy-form input[name='topic']").val("Купить в один клик - " + product_name);
        });


        $(".small-item-card .props .title").click(function(){
            if (!$(this).closest(".small-item-cards").hasClass("view-list")) {
                $(this).siblings(".prop-item").toggle();
                $(this).closest(".small-item-card").css("height", "auto");

                let props_height = $(this).closest('.props').outerHeight();
                $(this).closest(".small-item-card").siblings(".small-item-card").find('.props').css("min-height", props_height - 5);
                $(this).closest(".small-item-card").siblings(".small-item-card").css("height", "auto");
            }
        });

        $(".section-videos .videos-container").not('.slick-initialized').slick({
            slidesToShow:4,
            slidesToScroll:4,
            infinite:false,
            arrows:true,
            responsive:[
            {
                breakpoint:480,
                settings:{
                    slidesToShow:2
                }
            }
            ]
        });

        $(".section-videos .show-video").click(function(){
            $(".section-videos .videos-container").toggleClass("open");
        });


        $("#articles-list-carousel .articles-list").not('.slick-initialized').slick({
            slidesToShow: 4,
            slidesToScroll:4,
            responsive: [
                {
                    breakpoint:480,
                    settings: {
                        slidesToShow:1,
                        slidesToScroll:1,
                        arrows: false,
                        dots:true
                    }
                }
            ]
        });



        ///////////////////////// Hints start

      $('label[for="bbFilter_210_1628178473"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/oblitsovochnyy-kirpich.html" target="_blank">Узнать подробнее </a> про  облицовочный кирпич
            </span>
         </span>
         `);

      // $('label[for="bbFilter_210_1628178473"]').after(`<a href="https://tulpar-trade.ru/articles/oblitsovochnyy-kirpich.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_210_369834175"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/shamotnyy-kirpich.html" target="_blank">Узнать подробнее </a> про огнеупорный кирпич
            </span>
         </span>
         `);

      // $('label[for="bbFilter_210_369834175"]').after(`<a href="https://tulpar-trade.ru/articles/shamotnyy-kirpich.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);


      $('label[for="bbFilter_210_71638839"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/keramicheskiy-ryadovoy-kirpich.html" target="_blank">Узнать подробнее </a> про рядовой кирпич
            </span>
         </span>
         `);

      // $('label[for="bbFilter_210_71638839"]').after(`<a href="https://tulpar-trade.ru/articles/keramicheskiy-ryadovoy-kirpich.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_210_4161100179"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/figurnyy-kirpich.html" target="_blank">Узнать подробнее </a> про фигурный кирпич
            </span>
         </span>
         `);

      // $('label[for="bbFilter_210_4161100179"]').after(`<a href="https://tulpar-trade.ru/articles/figurnyy-kirpich.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_210_2585889428"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/stroitelnyy-tsokolnyy-kirpich.html" target="_blank">Узнать подробнее </a> про цокольный кирпич
            </span>
         </span>
         `);

      // $('label[for="bbFilter_210_2585889428"]').after(`<a href="https://tulpar-trade.ru/articles/stroitelnyy-tsokolnyy-kirpich.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_219485612"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/wienerberger/" target="_blank">Узнать подробнее </a> про кирпичный завод Wienerberger
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_219485612"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/wienerberger/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_2590124191"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/alekseevskaya-keramika/" target="_blank">Узнать подробнее </a> про  Алексеевский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_2590124191"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/alekseevskaya-keramika/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_255313712"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/arskiy-kirpichnyy-zavod-aspk/" target="_blank">Узнать подробнее </a> про Арский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_255313712"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/arskiy-kirpichnyy-zavod-aspk/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_1410600121"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/vkz/" target="_blank">Узнать подробнее </a> про Воткинский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_1410600121"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/vkz/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_4205158778"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/altair/" target="_blank">Узнать подробнее </a> про Ижевский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_4205158778"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/altair/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_3178736012"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/kazanskiy-kombinat-stroitelnykh-materialov/" target="_blank">Узнать подробнее </a> про Казанский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_3178736012"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/kazanskiy-kombinat-stroitelnykh-materialov/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_749103973"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/kirpichnyy-zavod-ketra/" target="_blank">Узнать подробнее </a> про кирпичный завод Кетра
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_749103973"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/kirpichnyy-zavod-ketra/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_2215960007"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/kc-kepamik/" target="_blank">Узнать подробнее </a> про Кирово-Чепецкий кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_2215960007"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/kc-kepamik/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_1953253811"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/klyuker/" target="_blank">Узнать подробнее </a> про Ключищенский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_1953253811"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/klyuker/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_3396376858"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/koshchakovskiy-kirpichnyy-zavod/" target="_blank">Узнать подробнее </a> про Кощаковский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_3396376858"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/koshchakovskiy-kirpichnyy-zavod/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      // $('label[for="bbFilter_44_3441253635"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/mamadyshskiy/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_588463151"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/chaykovskiy-kirpichnyy-zavod/" target="_blank">Узнать подробнее </a> про Чайковский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_588463151"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/chaykovskiy-kirpichnyy-zavod/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_3265792585"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/shelangovskiy-kirpichnyy-zavod/" target="_blank">Узнать подробнее </a> про Шеланговский кирпичный завод
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_3265792585"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/shelangovskiy-kirpichnyy-zavod/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      // $('label[for="bbFilter_44_795364596"]').after(`<a href="https://tulpar-trade.ru/catalog/zyab3/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_81853324"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/bikton/" target="_blank">Узнать подробнее </a> про завод строительных материалов Биктон
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_81853324"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/bikton/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_44_1537301491"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/proizvoditeli/kirpichnyy-zavod-amstron-porikam/" target="_blank">Узнать подробнее </a> про кирпичный завод Амстрон
            </span>
         </span>
         `);

      // $('label[for="bbFilter_44_1537301491"]').after(`<a href="https://tulpar-trade.ru/proizvoditeli/kirpichnyy-zavod-amstron-porikam/" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_176_1921009046"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/keramicheskiy-kamen.html" target="_blank">Узнать подробнее </a> про керамический камень
            </span>
         </span>
         `);

      // $('label[for="bbFilter_176_1921009046"]').after(`<a href="https://tulpar-trade.ru/articles/keramicheskiy-kamen.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_176_2626561210"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/krupnoformatnyy-keramicheskiy-blok.html" target="_blank">Узнать подробнее </a> про крупноформатный керамический блок
            </span>
         </span>
         `);

      // $('label[for="bbFilter_176_2626561210"]').after(`<a href="https://tulpar-trade.ru/articles/krupnoformatnyy-keramicheskiy-blok.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_176_3951629356"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/peregorodochnyy-keramicheskiy-blok.html" target="_blank">Узнать подробнее </a> про перегородочный керамический блок
            </span>
         </span>
         `);

      // $('label[for="bbFilter_176_3951629356"]').after(`<a href="https://tulpar-trade.ru/articles/peregorodochnyy-keramicheskiy-blok.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_177_92763392"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/krupnoformatnyy-gazobetonnyy-blok.html" target="_blank">Узнать подробнее </a> про крупноформатный газобетонный блок
            </span>
         </span>
         `);

      // $('label[for="bbFilter_177_92763392"]').after(`<a href="https://tulpar-trade.ru/articles/krupnoformatnyy-gazobetonnyy-blok.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_177_2615402659"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/peregorodochnyy-gazobetonnyy-blok.html" target="_blank">Узнать подробнее </a> про перегородочный газобетонный блок
            </span>
         </span>
         `);

      // $('label[for="bbFilter_177_2615402659"]').after(`<a href="https://tulpar-trade.ru/articles/peregorodochnyy-gazobetonnyy-blok.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_178_439609967"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/kladochnyy-rastvor.html" target="_blank">Узнать подробнее </a> про кладочный раствор
            </span>
         </span>
         `);

      // $('label[for="bbFilter_178_439609967"]').after(`<a href="https://tulpar-trade.ru/articles/kladochnyy-rastvor.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_178_4253796200"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/kley-dlya-gazobetona.html" target="_blank">Узнать подробнее </a> про клей для газобетона
            </span>
         </span>
         `);

      // $('label[for="bbFilter_178_4253796200"]').after(`<a href="https://tulpar-trade.ru/articles/kley-dlya-gazobetona.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_178_3930816027"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/teploizolyatsionnyy-rastvor.html" target="_blank">Узнать подробнее </a> про теплоизоляционный раствор
            </span>
         </span>
         `);

      // $('label[for="bbFilter_178_3930816027"]').after(`<a href="https://tulpar-trade.ru/articles/teploizolyatsionnyy-rastvor.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_178_2639023757"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/tsvetnoy-kladochnyy-rastvor.html" target="_blank">Узнать подробнее </a> про цветной кладочный раствор
            </span>
         </span>
         `);

      // $('label[for="bbFilter_178_2639023757"]').after(`<a href="https://tulpar-trade.ru/articles/tsvetnoy-kladochnyy-rastvor.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_179_2455088264"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/bazaltovaya-setka.html" target="_blank">Узнать подробнее </a> про  базальтовую сетку
            </span>
         </span>
         `);

      // $('label[for="bbFilter_179_2455088264"]').after(`<a href="https://tulpar-trade.ru/articles/bazaltovaya-setka.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      $('label[for="bbFilter_179_3847396382"]').
        after(`
         <span class="hint-wrapper">
            <i id="item_title_hint_210" class="fa fa-question-circle"></i>
            <span class="my-tooltip">
              <a href="https://tulpar-trade.ru/articles/stekloplastikovaya-setka.html" target="_blank">Узнать подробнее </a> про  стеклопластиковую сетку
            </span>
         </span>
         `);

      // $('label[for="bbFilter_179_3847396382"]').after(`<a href="https://tulpar-trade.ru/articles/stekloplastikovaya-setka.html" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);


      $('label[for=""]').not($(".calc_label")).after(`<a href="" class="filter-link" target="_blank"><i id="item_title_hint_210" class="fa fa-question-circle"></i></a>`);

      ///////////////////////// Hints end

      let hoverTimeout;
      $('.hint-wrapper').hover(function(){
        clearTimeout(hoverTimeout);
        $('.hint-wrapper').removeClass('show');
        $(this).addClass('show');
      }, function() {
        let $self = $(this);
        hoverTimeout = setTimeout(function() {
                $self.removeClass('show');
            }, 100);
      });

      //**********CALCULATOR **********//

        let docLocation = $(location).attr('pathname');
        let constructionTemplateHtml;
        let floorTemplateHtml;
        let floorBtnTemplateHtml;

        let floorCounter = 1;

        let dataFromJSON = {};
        let fasadniySelections = {};
        let fasadniyFrontonsSelections = {};
        let peregorodkiSelections = {};
        let wallsSelections = {};
        let wallsFrontonSelections = {};
        let doorsAndWindowsArea = {doors: 0, windows: 0};
        let frontonsArea = 0;
        let frontonWindowsArea = 0;

        let totalCost = 0;
        let totalBasementCost = 0;
        let totalFasadniyCost = 0;
        let totalFrontonsCost = 0;
        let totalWallsCost = 0;
        let totalPeregorodkiCost = 0;

        let totalFrontonsWallsCost = 0;

        if (docLocation == "/calculator/") {
            $.ajax({
                url: '/calculator/results.json',
                dataType: 'json',
                type: 'get',
                cache: false,
                success: function (data) {
                    dataFromJSON = data;

                }
            });

            constructionTemplateHtml = $('.calc_housePart--floor__js[data-floorNum="1"] .calc_construction').clone();
            constructionFrontonTemplateHtml = $('.calc_housePart--frontons .calc_construction').clone();
            floorTemplateHtml = $('.calc_housePart--floor__js[data-floorNum="1"]').clone();
            floorBtnTemplateHtml = $('.regular-floor-btn.first-floor__js').clone();

            $('[data-toggle="tooltip-calc"]').tooltip();
        }


        /* add floor */

        $('body').on('click', '.add_floor' , function () {

            floorCounter++;

            let newFloorInstance = $(floorTemplateHtml).clone();

            $(newFloorInstance).attr('data-floorNum', floorCounter);
            $(newFloorInstance).attr('data-floorNumberText', "floor-"+floorCounter);
            $(newFloorInstance).find('.calc__block-title span.floorNumber__js').text(floorCounter);
            $('.calc_housePart--floor[data-floorNum="'+(floorCounter-1)+'"]').addClass('hidden');


            $(newFloorInstance).find('.input-col__setka__rows__js').html(`<div class="calc_label">Укладывать каждые</div>
                                                <select class="calc_select calc_setka_rows__js" name="" id="">
                                                    <option value="1">1 ряд</option>
                                                    <option value="2">2 ряда</option>
                                                    <option value="3">3 ряда</option>
                                                    <option value="4">4 ряда</option>
                                                    <option value="5">5 ряда</option>
                                                </select>`);

            $(newFloorInstance).find(' select.setka__rows_js').styler();


            $(newFloorInstance).appendTo('.calc-col__preferences');

            let newFloorBtnInstance = $(floorBtnTemplateHtml).clone();
            $(newFloorBtnInstance).text(floorCounter + " этаж");
            $(newFloorBtnInstance).removeClass('first-floor__js');
            $(newFloorBtnInstance).attr('data-floorNum', floorCounter);

            $('.regular-floor-btn[data-floorNum="'+ (floorCounter - 1) +'"]').removeClass('active');

            $(newFloorBtnInstance).insertBefore($('.fronton-floor__js'));

            console.log(floorCounter);
        })


      ///////////////////////////////////////

        /* FRONTONS START */

        let frontonTemplateHtml = $('.calc_housePart--frontons .fronton-item').clone();

        $('.calc_housePart--frontons .fronton-item:first').find('.calc__block-clear').remove();

        $('body').on('click', '.fronton-floor__js', function () {
            $('.calc_housePart').addClass('hidden');
            $('.calc_housePart--frontons').removeClass('hidden');

            $('.calc_housePart--frontons .calc_block--frontons').removeClass('hidden');

            $('.floor-btn').removeClass('active');
            $(this).addClass('active');
        });

        $('body').on('click', '.calcAddBtn[data-calcAddWhat="addNewFronton"]', function () {

            let newFrontonInstance = $(frontonTemplateHtml).clone();

            let lastFrontonNumber = $('.calc_housePart--frontons .fronton-item').length;

            $(newFrontonInstance).find('.calc__block-title .frontonNumber').text(lastFrontonNumber+1);

            $(newFrontonInstance).insertBefore(this);
        });

        $('body').on('click', '.fronton__type--triangle', function () {
           $(this).closest('.fronton__types').find('.fronton__type').removeClass('selected');
           $(this).addClass('selected');

            $(this).closest('.fronton-item').find('.fronton_dimensions_row .input-col-rebro').addClass('hidden');

            $(this).closest('.fronton-item').find('.fronton_dimensions_row').removeClass('hidden');
        });

        $('body').on('click', '.fronton__type--trapec', function () {
            $(this).closest('.fronton__types').find('.fronton__type').removeClass('selected');
            $(this).addClass('selected');

            $(this).closest('.fronton-item').find('.fronton_dimensions_row .input-col-rebro').removeClass('hidden');

            $(this).closest('.fronton-item').find('.fronton_dimensions_row').removeClass('hidden');
        });

        $('body').on('click', '.calc_housePart--frontons .construction_type_item__js[data-construction_type="FASADNIY"]', function () {
            $(this).closest('.calc_construction').attr('data-constructionType', "FASADNIY");

            $(this).closest('.calc_construction').find('.calc_option_sub_block ').not('.calc_option_sub_block_maker').addClass('hidden');
            $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn_border').removeClass('selected');
            $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn').removeClass('selected');

            $(this).closest('.constructions_options_row').find('.construction_type_item__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            let makers = [];

            let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

            $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
                if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                    if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                        console.log(value);
                    } else {
                        makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                        tempValueMakersID.push(value.MAKER_VALUE_ID);
                    }
                }
            });

            let makersElements = "";
            $(makers).each(function (index, value) {
                makersElements += `<div class="maker calc-maker__js" data-type="FASADNIY" data-maker_id="${value.value_id}">
                                            <div class="img">
                                                <img src="${value.maker_image}" alt="">
                                            </div>
                                            <span>${value.value}</span>
                                         </div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_makers_row__makers').html(makersElements);
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_block--makers').removeClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_maker').removeClass('hidden');

            /*LETS SHOW "ADD CONSTUCTION" BUTTON*/
            if ($(this).closest('.constructions_options_row').find('.construction_type_item__js').length > 1) {
                $('.calc_housePart--frontons').find('.add_new_construction').removeClass('hidden');
            }


            console.log(dataFromJSON.BRICKS['FASADNIY']);

        });

        $('body').on('click', ".calc_housePart--frontons .calc-maker__js[data-type='FASADNIY']", function () {
            let thisMakerID = this.getAttribute('data-maker_id');

            $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            fasadniyFrontonsSelections['makerID'] = thisMakerID;
            fasadniyFrontonsSelections['makerName'] = $(this).find('span').text();

            let brickFormatArr = [];
            $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
                if (!brickFormatArr.includes(value.SIZE) && value.MAKER_VALUE_ID === thisMakerID) {
                    brickFormatArr.push(value.SIZE);
                }
            });

            let formatElements = "";
            $(brickFormatArr).each(function (index, value) {
                formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="FASADNIY" data-material__size="${value}">${value}</div>`;
            });



            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_row--format').html(formatElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_formats').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_strength_mark').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_surfaces').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_colorTones').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_color').addClass('hidden');

            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_formats').removeClass('hidden');

            console.log(fasadniyFrontonsSelections);
        });

        $("body").on("click", ".calc_housePart--frontons .calc_brick_size__js[data-type='FASADNIY']", function () {
            let thisFormat = this.getAttribute('data-material__size');

            $(this).closest('.calc_construction').find('.calc_brick_size__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            fasadniyFrontonsSelections['format'] = thisFormat;

            let surfacesArr = [];

            $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
                if (!surfacesArr.includes(value.SURFACE) &&
                    value.MAKER_VALUE_ID === fasadniyFrontonsSelections.makerID &&
                    value.SIZE === thisFormat) {

                    surfacesArr.push(value.SURFACE);
                }
            });

            let surfacesElements = "";
            $(surfacesArr).each(function (index, value) {
                if (value !== null) {
                    surfacesElements += `<div class="calc_option_btn calc-surface__js" data-type="FASADNIY" data-material__surface="${value}">${value}</div>`;
                }
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_row--surface').html(surfacesElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_surfaces').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_colorTones').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_color').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_surfaces').removeClass('hidden');

            console.log(fasadniyFrontonsSelections);
        });

        $('body').on('click', '.calc_housePart--frontons .calc-surface__js[data-type="FASADNIY"]', function () {
            let thisMaterialSurface = this.getAttribute('data-material__surface');

            $(this).closest('.calc_construction').find('.calc-surface__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            fasadniyFrontonsSelections['surface'] = thisMaterialSurface;

            let colorTonesArr = [];
            $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
                if (!colorTonesArr.includes(value.COLOR_TONE) &&
                    value.MAKER_VALUE_ID === fasadniyFrontonsSelections.makerID &&
                    value.SIZE === fasadniyFrontonsSelections.format &&
                    value.SURFACE === thisMaterialSurface) {

                    colorTonesArr.push(value.COLOR_TONE);
                }
            });

            let colorTonesElements = "";
            $(colorTonesArr).each(function (index, value) {
                if (value !== null) {
                    colorTonesElements += `<div class="calc_option_btn_border calc-colorTone__js" data-type="FASADNIY" data-material__colorTone="${value}">
                                            <span class="color color-${value}"></span>
                                            ${dataFromJSON.COLOR_TONES[value]}
                                         </div>`;
                }

            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_row--tone').html(colorTonesElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_colorTones').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_color').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_colorTones').removeClass('hidden');

            console.log(fasadniyFrontonsSelections);
        });

        $('body').on('click', '.calc_housePart--frontons .calc-colorTone__js[data-type="FASADNIY"]', function () {
            let thisColorTone = this.getAttribute('data-material__colorTone');

            $(this).closest('.calc_construction').find('.calc-colorTone__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            fasadniyFrontonsSelections['colorTone'] = thisColorTone;

            let colorsArr = [];
            $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
                if (!colorsArr.includes(value.COLOR) &&
                    value.MAKER_VALUE_ID === fasadniyFrontonsSelections.makerID &&
                    value.SIZE === fasadniyFrontonsSelections.format &&
                    value.SURFACE === fasadniyFrontonsSelections.surface &&
                    value.COLOR_TONE === thisColorTone) {

                    colorsArr.push({color: value.COLOR, product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
                }
            });

            let colorElements = "";
            $(colorsArr).each(function (index, value) {
                let klinker = "";
                if (value.product_id == 9057 || value.product_id == 9056 || value.product_id == 9055 || value.product_id == 9054 || value.product_id == 9051 || value.product_id == 9052 || value.product_id == 9053) {
                    klinker = "(клинкерный)";
                }

                let category = "";
                if (fasadniyFrontonsSelections.makerID === "502") {
                    let thisColor = value.color;
                    let thisColorPrice = value.itemPrice;
                    let thisItemId = value.product_id;

                    $(colorsArr).each(function (i, v) {
                        if (thisColor === v.color && thisItemId !== v.product_id) {
                            if (parseFloat(thisColorPrice) > parseFloat(v.itemPrice)) {
                                category = "(1 категория)";
                            } else {
                                category = "(2 категория)";
                            }
                        }
                    });
                }

                let img = "";
                if (typeof dataFromJSON.PRODUCTS_IMAGES[value.product_image_id] !== "undefined") {
                    img = `<img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">`;
                }

                colorElements += `<div class="calc_option_btn_border calc-color__js" data-type="FASADNIY" data-itemID="${value.product_id}" data-itemPrice="${value.itemPrice}" data-poddonQnty="${value.poddonQnty}" data-material__color="${value.color}">
                                        ${img}
                                        ${value.color} ${klinker} ${category}
                                    </div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_row--color').html(colorElements);
            $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_color').removeClass('hidden');
        });

        $('body').on('click', '.calc_housePart--frontons .calc-color__js[data-type="FASADNIY"]', function () {
            let thisBrickColor = this.getAttribute('data-material__color');
            let thisItemId = this.getAttribute('data-itemID');
            let selectedItemPoddonQnty = this.getAttribute('data-poddonQnty');
            let selectedItemPrice = this.getAttribute('data-itemPrice');

            fasadniyFrontonsSelections['color'] = thisBrickColor;
            fasadniyFrontonsSelections['itemID'] = thisItemId;
            fasadniyFrontonsSelections['poddonQnty'] = selectedItemPoddonQnty;
            fasadniyFrontonsSelections['itemPrice'] = selectedItemPrice;

            $(this).closest('.calc_construction').find('.calc-color__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            calculateFrontonFasadMaterials();
        });

        $('body').on('click', '.calc_housePart--frontons .add_new_construction', function () {

            let thisFloorConstructionNumber = $(this).closest('.calc_housePart--floor').find('.calc_construction').length;

            let thisFloorUnactiveConstructions = $('.calc_housePart--frontons .calc_construction').last().find('.construction_type_item__js').not('.selected').clone();;

            let thisСonstructionTemplateHtmlClone = $(constructionFrontonTemplateHtml).clone();

            $(thisСonstructionTemplateHtmlClone).find('.calc_block--frontons').remove();

            $(thisСonstructionTemplateHtmlClone).find('.calc__block-title span.construction__number__js').text(thisFloorConstructionNumber + 1);

            $(thisСonstructionTemplateHtmlClone).find('.constructions_options_row').html(thisFloorUnactiveConstructions);

            $(thisСonstructionTemplateHtmlClone).removeClass('hidden');
            $(thisСonstructionTemplateHtmlClone).insertBefore('.calc_housePart--frontons .add_new_construction');

            $(this).addClass('hidden');
            console.log('add new construction');
        });

        $('body').on('change', '.calc_housePart--frontons .window_input_row input', function () {

                if ($(this).val() === "") {
                    $(this).val(0);
                }

                calculateFrontonsWindowsArea();

                let constructionType = $(this).closest('.calc_construction').attr('data-constructionType');

                if(constructionType === "FASADNIY") {
                    calculateFrontonFasadMaterials();
                } else if(constructionType === "WALLS") {
                    calculateFrontonWallsMaterials();
                }

            });

        $('body').on('change', '.fronton_dimensions_row input', function () {

                if ($(this).val() === "") {
                    $(this).val(0);
                }

                let constructionType = $(this).closest('.calc_construction').attr('data-constructionType');

                if(constructionType === "FASADNIY") {
                    calculateFrontonFasadMaterials();
                } else if(constructionType === "WALLS") {
                    calculateFrontonWallsMaterials();
                }

            });

        function calculateFrontonFasadMaterials() {

            let fasadniyResultLinesHtmlTemplate = $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="1"]').clone();

            totalFasadniyCost = 0;
            let totalFrontonsArea = 0;

            if (typeof fasadniyFrontonsSelections.itemID !== "undefined") {

                $('.calc_housePart--frontons .fronton-item').each(function (index, value) {
                    let thisFrontonQnty = parseInt($(this).find('.input-col-quantity input').val());
                    let thisFrontonHeight = parseInt($(this).find('.input-col-height input').val());
                    let thisFrontonBaseWidth = parseInt($(this).find('.input-col-baseWidth input').val());

                    let thisFrontonArea = 0;

                    if ($(this).find('.fronton__type.selected').attr('data-frontonType') === "triangle") {
                        thisFrontonArea = thisFrontonQnty*(thisFrontonBaseWidth * thisFrontonHeight)/2;
                    } else if ($(this).find('.fronton__type.selected').attr('data-frontonType') === "trapec") {
                        let thisFrontonRebro = parseInt($(this).find('.input-col-rebro input').val());
                        thisFrontonArea = thisFrontonQnty*(thisFrontonBaseWidth + thisFrontonRebro)*thisFrontonHeight/2;
                    }

                    thisFrontonArea = thisFrontonArea;
                    totalFrontonsArea += thisFrontonArea;
                });

                totalFrontonsArea = totalFrontonsArea - frontonWindowsArea;

                let consumptionRate = 51; //if format == 1nf
                let selectedFormat = fasadniyFrontonsSelections.format;

                if (selectedFormat === "1.4НФ") {
                    consumptionRate = 39;
                }

                let totalBricksRequired = Math.ceil(totalFrontonsArea * consumptionRate);
                let poddonDivisible = Math.ceil(totalBricksRequired / fasadniyFrontonsSelections.poddonQnty) * fasadniyFrontonsSelections.poddonQnty;

                let poddonPrice = 0;
                if (dataFromJSON['PODDON_PRICE'][fasadniyFrontonsSelections.makerID]) {
                    poddonPrice = dataFromJSON['PODDON_PRICE'][fasadniyFrontonsSelections.makerID];
                }

                let totalFrontonsFasadniyCost = poddonDivisible * fasadniyFrontonsSelections.itemPrice + poddonPrice * Math.ceil(totalBricksRequired / fasadniyFrontonsSelections.poddonQnty) + poddonPrice * Math.ceil(totalBricksRequired / fasadniyFrontonsSelections.poddonQnty);

                if (isNaN(totalFasadniyCost)) {
                    totalFasadniyCost = 0;
                }

                totalFasadniyCost = totalFasadniyCost + totalFrontonsFasadniyCost;

                totalCost = totalBasementCost + totalFasadniyCost + totalPeregorodkiCost + totalWallsCost;

                let newResultsInstance = $(fasadniyResultLinesHtmlTemplate).clone();

                $(newResultsInstance).attr('data-floorNumber', "frontons");
                $(newResultsInstance).find('.floorNumber').text("Фронтоны");

                $(newResultsInstance).appendTo('.results__construction__type[data-resultsConstructionType="fasadniy"]');

                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--maker__js .result_value').text(fasadniyFrontonsSelections.makerName);
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--format__js .result_value').text(fasadniyFrontonsSelections.format);
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--surface__js .result_value').text(fasadniyFrontonsSelections.surface);
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--color__js .result_value').text(fasadniyFrontonsSelections.color);
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--bricksTotal__js .result_value').text(totalBricksRequired);
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--poddonDivisibleQnty__js .result_value').text(poddonDivisible);
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--poddonQnty__js .result_value').text(fasadniyFrontonsSelections.poddonQnty);
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--poddonAmount__js .result_value').text(Math.ceil(totalBricksRequired / fasadniyFrontonsSelections.poddonQnty));
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--poddonPrice__js .result_value').text(poddonPrice);
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"] .result__line--totalCost__js .result_value').text(formatCost(totalFrontonsFasadniyCost));

                $('.calc-col__results .final__price .final_price_value span').text(formatCost(totalCost));

                $('.results__construction__type[data-resultsConstructionType="fasadniy"]').removeClass('hidden');
                $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="frontons"]').removeClass('hidden');

            }

            console.log(totalFrontonsArea);

        }

        function calculateFrontonsWindowsArea() {

            frontonWindowsArea = 0;

            $('.calc_housePart--frontons .floor__window-item').each(function (index, value) {
                let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
                let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
                let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

                frontonWindowsArea += thisWindowQnty * (thisWindowHeightCm/100 * thisWindowWidthCm/100).toFixed(2);
            });
        }

        /* FRONTONS WALLS */

        $('body').on('click', '.calc_housePart--frontons .construction_type_item__js[data-construction_type="WALLS"]', function () {
            $(this).closest('.calc_construction').attr('data-constructionType', "WALLS");

            $(this).closest('.calc_construction').find('.calc_option_sub_block ').not('.calc_option_sub_block__materialType').addClass('hidden');
            $(this).closest('.calc_construction').find('.calc_makers_row__makers .maker').removeClass('selected');

            $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn_border').removeClass('selected');
            $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn').removeClass('selected');

            $(this).closest('.constructions_options_row').find('.construction_type_item__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            if ($('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"]').length > 0) {
                let fasadDoors = $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_block--doors').clone();
                let fasadWindows = $('.calc_housePart--frontons .calc_construction[data-constructionType="FASADNIY"] .calc_block--windows').clone();

                $(this).closest('.calc_construction').find('.calc_block--doors').replaceWith(fasadDoors);
                $(this).closest('.calc_construction').find('.calc_block--windows').replaceWith(fasadWindows);
            }

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_block--makers').removeClass('hidden');
            $(this).closest('.calc_construction').find('.calc_option_sub_block__materialType').removeClass('hidden');

            /*LETS SHOW "ADD CONSTUCTION" BUTTON*/
            if ($(this).closest('.constructions_options_row').find('.construction_type_item__js').length > 1) {
                $('.calc_housePart--frontons').find('.add_new_construction').removeClass('hidden');
            }
        });

        // ryadovoy start //
        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc-materialType__js[data-materialType="kirpich"]', function () {
            $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

            $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['materialType'] = "kirpich";

            let makers = [];
            let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

            $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
                if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                    if(typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                        console.log(value);
                    } else {
                        makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                        tempValueMakersID.push(value.MAKER_VALUE_ID);
                    }
                }
            });

            let makersElements = "";
            $(makers).each(function (index, value) {
                makersElements += `<div class="maker calc-maker__js" data-type="RYADOVOY" data-maker_id="${value.value_id}">
                                            <div class="img">
                                                <img src="${value.maker_image}" alt="">
                                            </div>
                                            <span>${value.value}</span>
                                         </div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_makers_row__makers').html(makersElements);
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_block--makers').removeClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_maker').removeClass('hidden');

        });

        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc-maker__js[data-type="RYADOVOY"]', function () {
            let thisMakerID = this.getAttribute('data-maker_id');
            let thisMakerName = $(this).find('span').text();

            $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['makerID'] = thisMakerID;
            wallsFrontonSelections['makerName'] = thisMakerName;

            let brickFormatArr = [];
            $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
                if (!brickFormatArr.includes(value.SIZE) && value.MAKER_VALUE_ID === thisMakerID) {
                    brickFormatArr.push(value.SIZE);
                }
            });

            let formatElements = "";
            $(brickFormatArr).each(function (index, value) {
                formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="RYADOVOY" data-material__size="${value}">${value}</div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_row--format').html(formatElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_formats').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_strength_mark').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_formats').removeClass('hidden');
        });

        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_brick_size__js[data-type="RYADOVOY"]', function () {
            let thisFormat = this.getAttribute('data-material__size');

            $(this).closest('.calc_construction').find('.calc_brick_size__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['format'] = thisFormat;

            let strenghtMarksArr = [];

            $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
                if (!strenghtMarksArr.includes(value.STRENGTH_MARK) &&
                    value.MAKER_VALUE_ID === wallsFrontonSelections.makerID &&
                    value.SIZE === thisFormat)   {
                    strenghtMarksArr.push(value.STRENGTH_MARK);
                }
            });

            let strengthMarkElements = "";
            $(strenghtMarksArr).each(function (index, value) {
                strengthMarkElements += `<div class="calc_option_btn calc-strength_mark__js" data-type="RYADOVOY" data-material__strength-mark="${value}">${value}</div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_row--strength_mark').html(strengthMarkElements);
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_strength_mark').removeClass('hidden');

        });

        $('body').on('click', ".calc_housePart--frontons .calc_construction[data-constructionType='WALLS'] .calc-strength_mark__js[data-type='RYADOVOY']", function () {
            let thisStrengthMark = this.getAttribute('data-material__strength-mark');

            $(this).closest('.calc_construction').find('.calc-strength_mark__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['strengthMark'] = thisStrengthMark;

            let itemsArr = [];
            let itemsIdArr = [];
            $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
                if (!itemsIdArr.includes(value.ID) &&
                    value.MAKER_VALUE_ID === wallsFrontonSelections.makerID &&
                    value.SIZE === wallsFrontonSelections.format &&
                    value.STRENGTH_MARK === thisStrengthMark) {

                    itemsIdArr.push(value.ID);
                    itemsArr.push({strengthMark: value.STRENGTH_MARK, product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
                }
            });

            let itemsElements = "";
            $(itemsArr).each(function (index, value) {
                let img = "";
                if (typeof dataFromJSON.PRODUCTS_IMAGES[value.product_image_id] !== "undefined") {
                    img = `<img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">`;
                }
                itemsElements += `<div class="calc_option_btn_border calc-item__js" data-type="RYADOVOY" data-poddonQnty="${value.poddonQnty}" data-itemPrice="${value.itemPrice}" data-material__item-id="${value.product_id}">
                                            ${img}
                                            ${value.product_name}
                                        </div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_row--items').html(itemsElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').removeClass('hidden');

        });

        $('body').on('click', ".calc_housePart--frontons .calc_construction[data-constructionType='WALLS'] .calc-item__js[data-type='RYADOVOY']", function () {
            let thisItemId = this.getAttribute('data-material__item-id');

            $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['id'] = thisItemId;
            wallsFrontonSelections['poddonQnty'] = this.getAttribute('data-poddonQnty');
            wallsFrontonSelections['itemPrice'] = this.getAttribute('data-itemPrice');

            $(this).closest('.calc_construction').find('.calc_option_sub_block_kladka_parameters').removeClass('hidden');

            calculateFrontonWallsMaterials();
        });

        $('body').on('change', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] select.wall_width_js', function () {
            calculateFrontonWallsMaterials();
        });

        // ryadovoy end//


        //kermablock start//
        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc-materialType__js[data-materialType="keramblock"]', function () {
            $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

            $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['materialType'] = "keramblock";

            let makers = [];
            let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

            $(dataFromJSON.BRICKS['KERAMBLOCKS']).each(function (index, value) {
                if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                    if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                        console.log(value);
                    } else {
                        makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                        tempValueMakersID.push(value.MAKER_VALUE_ID);
                    }
                }
            });

            let makersElements = "";
            $(makers).each(function (index, value) {
                makersElements += `<div class="maker calc-maker__js" data-type="KERAMBLOCKS" data-maker_id="${value.value_id}">
                                            <div class="img">
                                                <img src="${value.maker_image}" alt="">
                                            </div>
                                            <span>${value.value}</span>
                                         </div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_makers_row__makers').html(makersElements);
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_block--makers').removeClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_maker').removeClass('hidden');

        });

        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc-maker__js[data-type="KERAMBLOCKS"]', function () {
            let thisMakerID = this.getAttribute('data-maker_id');
            let thisMakerName = $(this).find('span').text();

            $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['makerID'] = thisMakerID;
            wallsFrontonSelections['makerName'] = thisMakerName;

            let brickFormatArr = [];
            $(dataFromJSON.BRICKS['KERAMBLOCKS']).each(function (index, value) {
                if (!brickFormatArr.includes(value.SIZE) && value.MAKER_VALUE_ID === thisMakerID) {
                    brickFormatArr.push(value.SIZE);
                }
            });

            let formatElements = "";
            let halfBlockArray = ["5.5НФ", "7НФ", "7.4НФ", "5.4НФ", "6.2НФ", "7.2НФ", "5.4НФ", "6.2НФ", "7.2НФ"];
            $(brickFormatArr).each(function (index, value) {
                let sizeName = dataFromJSON.KERAMBLOCKS_FORMAT_NAMES[thisMakerID][value];
                let halfBlock = "";
                if (halfBlockArray.includes(value)) {
                    halfBlock = " - (1/2)";
                }
                formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="KERAMBLOCKS" data-material__size="${value}">${value} (${sizeName}) ${halfBlock}</div>`;
            });

            // let formatElements = "";
            // $(brickFormatArr).each(function (index, value) {
            //     formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="KERAMBLOCKS" data-material__size="${value}">${value}</div>`;
            // });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_row--format').html(formatElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_formats').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_strength_mark').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_formats').removeClass('hidden');
        });

        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_brick_size__js[data-type="KERAMBLOCKS"]', function () {
            let thisFormat = this.getAttribute('data-material__size');

            $(this).closest('.calc_construction').find('.calc_brick_size__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['format'] = thisFormat;

            let itemsArr = [];
            let itemsIdArr = [];
            $(dataFromJSON.BRICKS['KERAMBLOCKS']).each(function (index, value) {
                if (!itemsIdArr.includes(value.ID) &&
                    value.MAKER_VALUE_ID === wallsFrontonSelections.makerID &&
                    value.SIZE === thisFormat) {

                    itemsIdArr.push(value.ID);
                    itemsArr.push({product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
                }
            });

            let itemsElements = "";
            $(itemsArr).each(function (index, value) {
                let img = "";
                if (typeof dataFromJSON.PRODUCTS_IMAGES[value.product_image_id] !== "undefined") {
                    img = `<img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">`;
                }
                itemsElements += `<div class="calc_option_btn_border calc-item__js" data-type="KERAMBLOCKS" data-poddonQnty="${value.poddonQnty}" data-itemPrice="${value.itemPrice}" data-material__item-id="${value.product_id}">
                                            ${img}
                                            ${value.product_name}
                                        </div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_row--items').html(itemsElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').removeClass('hidden');
        });

        $('body').on('click', ".calc_housePart--frontons .calc_construction[data-constructionType='WALLS'] .calc-item__js[data-type='KERAMBLOCKS']", function () {
            let thisItemId = this.getAttribute('data-material__item-id');

            $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['id'] = thisItemId;
            wallsFrontonSelections['poddonQnty'] = this.getAttribute('data-poddonQnty');
            wallsFrontonSelections['itemPrice'] = this.getAttribute('data-itemPrice');

            console.log(wallsFrontonSelections);
            calculateFrontonWallsMaterials();
        });
        //kermablock end//

        //gazoblock start//
        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc-materialType__js[data-materialType="gazoblock"]', function () {
            $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

            $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['materialType'] = "gazoblock";

            let makers = [];
            let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

            $(dataFromJSON.BRICKS['GAZOBLOCKS']).each(function (index, value) {
                if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                    if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                        console.log(value);
                    } else {
                        makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                        tempValueMakersID.push(value.MAKER_VALUE_ID);
                    }
                }
            });

            let makersElements = "";
            $(makers).each(function (index, value) {
                makersElements += `<div class="maker calc-maker__js" data-type="GAZOBLOCKS" data-maker_id="${value.value_id}">
                                            <div class="img">
                                                <img src="${value.maker_image}" alt="">
                                            </div>
                                            <span>${value.value}</span>
                                         </div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_makers_row__makers').html(makersElements);
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_block--makers').removeClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_maker').removeClass('hidden');
        });

        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc-maker__js[data-type="GAZOBLOCKS"]', function () {
            let thisMakerID = this.getAttribute('data-maker_id');
            let thisMakerName = $(this).find('span').text();

            $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['makerID'] = thisMakerID;
            wallsFrontonSelections['makerName'] = thisMakerName;

            let densityMarkArr = [];
            $(dataFromJSON.BRICKS['GAZOBLOCKS']).each(function (index, value) {
                if (!densityMarkArr.includes(value.DENSITY_MARK) && value.MAKER_VALUE_ID === thisMakerID) {
                    densityMarkArr.push(value.DENSITY_MARK);
                }
            });

            let densityElements = "";
            $(densityMarkArr).each(function (index, value) {
                densityElements += `<div class="calc_option_btn calc_block_density__js" data-type="GAZOBLOCKS" data-densityMark="${value}">${value}</div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_row--density_mark').html(densityElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_density_mark').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_width_gb').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_density_mark').removeClass('hidden');
        });

        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_block_density__js[data-type="GAZOBLOCKS"]', function () {
            let thisDensityMark = this.getAttribute('data-densityMark');

            $(this).closest('.calc_construction').find('.calc_block_density__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['densityMark'] = thisDensityMark;

            let widthArr = [];
            $(dataFromJSON.BRICKS['GAZOBLOCKS']).each(function (index, value) {
                if (!widthArr.includes(value.WIDTH) && value.MAKER_VALUE_ID === wallsFrontonSelections.makerID &&
                    value.DENSITY_MARK === thisDensityMark ) {
                    widthArr.push(value.WIDTH);
                }
            });

            let widthElements = "";
            $(widthArr).each(function (index, value) {
                widthElements += `<div class="calc_option_btn calc_block_width__js" data-type="GAZOBLOCKS" data-width="${value}">${value}</div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_row--width_gb').html(widthElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_width_gb').addClass('hidden');
            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_width_gb').removeClass('hidden');
        });

        $('body').on('click', '.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_block_width__js[data-type="GAZOBLOCKS"]', function () {
            let thisWidth = this.getAttribute('data-width');

            $(this).closest('.calc_construction').find('.calc_block_width__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['width'] = thisWidth;

            let itemsArr = [];
            let itemsIdArr = [];
            $(dataFromJSON.BRICKS['GAZOBLOCKS']).each(function (index, value) {
                if (!itemsIdArr.includes(value.ID) &&
                    value.MAKER_VALUE_ID === wallsFrontonSelections.makerID &&
                    value.DENSITY_MARK === wallsFrontonSelections.densityMark &&
                    value.WIDTH === thisWidth) {

                    itemsIdArr.push(value.ID);
                    itemsArr.push({product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, itemHeight: value.HEIGHT,poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
                }
            });

            let itemsElements = "";
            $(itemsArr).each(function (index, value) {
                let img = "";
                if (typeof dataFromJSON.PRODUCTS_IMAGES[value.product_image_id] !== "undefined") {
                    img = `<img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">`;
                }
                itemsElements += `<div class="calc_option_btn_border calc-item__js" data-type="GAZOBLOCKS" data-poddonQnty="${value.poddonQnty}" data-itemHeight="${value.itemHeight}" data-itemPrice="${value.itemPrice}" data-material__item-id="${value.product_id}">
                                            ${img}
                                            ${value.product_name}
                                        </div>`;
            });

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_row--items').html(itemsElements);

            $('.calc_housePart--frontons .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').removeClass('hidden');
        });

        $('body').on('click', ".calc_housePart--frontons .calc_construction[data-constructionType='WALLS'] .calc-item__js[data-type='GAZOBLOCKS']", function () {
            let thisItemId = this.getAttribute('data-material__item-id');

            $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsFrontonSelections['id'] = thisItemId;
            wallsFrontonSelections['poddonQnty'] = this.getAttribute('data-poddonQnty');
            wallsFrontonSelections['itemPrice'] = this.getAttribute('data-itemPrice');
            wallsFrontonSelections['itemHeight'] = this.getAttribute('data-itemHeight');

            console.log(wallsFrontonSelections);
            calculateFrontonWallsMaterials();
        });
        //gazoblock end//

        function calculateFrontonWallsMaterials() {
            let index = 1;
            let totalWallsArea = 0;

            totalFrontonsWallsCost = 0;

            let totalFrontonsArea = 0;

            let wallsResultLinesHtmlTemplate = $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="1"]').clone();
            $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines').not('.result__lines[data-floorNumber="1"]').remove();

            if (typeof wallsFrontonSelections.id !== "undefined") {
                let thisFloorArea = 0;
                let thisFloorWindowArea = 0;

                $('.calc_housePart--frontons .calc_construction[data-constructiontype="WALLS"] .floor__window-item').each(function (i, value) {
                    let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
                    let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
                    let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

                    thisFloorWindowArea += thisWindowQnty * (thisWindowHeightCm / 100 * thisWindowWidthCm / 100).toFixed(2);
                });

                $('.calc_housePart--frontons .fronton-item').each(function (index, value) {
                    let thisFrontonQnty = parseInt($(this).find('.input-col-quantity input').val());
                    let thisFrontonHeight = parseInt($(this).find('.input-col-height input').val());
                    let thisFrontonBaseWidth = parseInt($(this).find('.input-col-baseWidth input').val());

                    let thisFrontonArea = 0;

                    if ($(this).find('.fronton__type.selected').attr('data-frontonType') === "triangle") {
                        thisFrontonArea = thisFrontonQnty*(thisFrontonBaseWidth * thisFrontonHeight)/2;
                    } else if ($(this).find('.fronton__type.selected').attr('data-frontonType') === "trapec") {
                        let thisFrontonRebro = parseInt($(this).find('.input-col-rebro input').val());
                        thisFrontonArea = thisFrontonQnty*(thisFrontonBaseWidth + thisFrontonRebro)*thisFrontonHeight/2;
                    }

                    //thisFrontonArea = thisFrontonArea/10000;
                    totalFrontonsArea += thisFrontonArea;
                });

                totalFrontonsArea = totalFrontonsArea - thisFloorWindowArea;

                totalWallsArea = totalFrontonsArea;

                let consumptionRate;
                let oneBlockVolume = 0;
                let wallsWidth = 0;
                let wallsVolume = 0;

                if (wallsFrontonSelections.materialType === "kirpich") {
                    consumptionRate = 400; //if format == 1nf
                    let selectedFormat = wallsFrontonSelections.format;

                    wallsWidth = $('.calc_housePart--frontons .calc_construction[data-constructiontype="WALLS"] select.wall_width_js').val();
                    console.log("wallsWidth " + wallsWidth);

                    wallsVolume = ((parseFloat(wallsWidth) / 0.5) * 0.13 - 0.01) * totalWallsArea;
                    wallsVolume = wallsVolume.toFixed(2);

                    console.log("perimeter " + wallsFrontonSelections.perimeter);
                    console.log("height " + wallsFrontonSelections.height);
                    console.log("wallsVolume " + wallsVolume);

                    if (selectedFormat === "1.4НФ") {
                        consumptionRate = 308;
                    } else if (selectedFormat === "2.1НФ") {
                        consumptionRate = 198;
                    }

                } else if(wallsFrontonSelections.materialType === "keramblock") {
                    let selectedFormat = wallsFrontonSelections.format;

                    if (selectedFormat === "11.2НФ" && wallsFrontonSelections.makerID === "800") {
                        consumptionRate = 10.7;
                    } else if ((selectedFormat === "10.7НФ" ||
                        selectedFormat === "12.4НФ" ||
                        selectedFormat === "14.3НФ") && wallsFrontonSelections.makerID === "800") {
                        consumptionRate = 17.3;
                    } else if ((selectedFormat === "10.5НФ" && wallsFrontonSelections.makerID === "630") ||
                        (selectedFormat === "10.7НФ" && wallsFrontonSelections.makerID === "801")) {
                        consumptionRate = 11.5;
                    } else if ((selectedFormat === "10.7НФ" ||
                        selectedFormat === "12.4НФ" ||
                        selectedFormat === "14.3НФ") && wallsFrontonSelections.makerID === "630") {
                        consumptionRate = 17.3;
                    } else if ((selectedFormat === "10.7НФ" ||
                        selectedFormat === "12.4НФ" ||
                        selectedFormat === "14.3НФ") && wallsFrontonSelections.makerID === "630") {
                        consumptionRate = 17.3;
                    } else if ((selectedFormat === "10.7НФ" ||
                        selectedFormat === "14.3НФ") && wallsFrontonSelections.makerID === "801") {
                        consumptionRate = 17.4;
                    }
                } else if(wallsFrontonSelections.materialType === "gazoblock") {
                    if (wallsFrontonSelections.makerID == "1189" || wallsFrontonSelections.makerID == "2301" || wallsFrontonSelections.makerID == "2302") {

                        if (wallsFrontonSelections.width == "200" && wallsFrontonSelections.itemHeight == "200") {
                            consumptionRate = 40;
                            oneBlockVolume = 0.025;
                        }
                        else if (wallsFrontonSelections.width == "250" && wallsFrontonSelections.itemHeight == "200") {
                            consumptionRate = 32;
                            oneBlockVolume = 0.03125;
                        }
                        else if (wallsFrontonSelections.width == "300" && wallsFrontonSelections.itemHeight == "200") {
                            consumptionRate = 26.67;
                            oneBlockVolume = 0.0375;
                        }
                        else if (wallsFrontonSelections.width == "400" && wallsFrontonSelections.itemHeight == "200") {
                            consumptionRate = 20;
                            oneBlockVolume = 0.05;
                        }
                        else if (wallsFrontonSelections.width == "500" && wallsFrontonSelections.itemHeight == "200") {
                            consumptionRate = 16;
                            oneBlockVolume = 0.0625;
                        }
                        else if (wallsFrontonSelections.width == "200" && wallsFrontonSelections.itemHeight == "250") {
                            consumptionRate = 32;
                            oneBlockVolume = 0.03125;
                        }
                        else if (wallsFrontonSelections.width == "250" && wallsFrontonSelections.itemHeight == "250") {
                            consumptionRate = 25.6;
                            oneBlockVolume = 0.03906;
                        }
                        else if (wallsFrontonSelections.width == "300" && wallsFrontonSelections.itemHeight == "250") {
                            consumptionRate = 21.34;
                            oneBlockVolume = 0.04687;
                        }
                        else if (wallsFrontonSelections.width == "400" && wallsFrontonSelections.itemHeight == "250") {
                            consumptionRate = 16;
                            oneBlockVolume = 0.0625;
                        }
                        else if (wallsFrontonSelections.width == "500" && wallsFrontonSelections.itemHeight == "250") {
                            consumptionRate = 12.8;
                            oneBlockVolume = 0.078125;
                        }
                    } else {
                        if (wallsFrontonSelections.width == "300" && wallsFrontonSelections.itemHeight == "200") {
                            consumptionRate = 27.78;
                            oneBlockVolume = 0.036;
                        } else if (wallsFrontonSelections.width == "400" && wallsFrontonSelections.itemHeight == "200") {
                            consumptionRate = 20.83;
                            oneBlockVolume = 0.048;
                        } else if (wallsFrontonSelections.width == "300" && wallsFrontonSelections.itemHeight == "250") {
                            consumptionRate = 22.22;
                            oneBlockVolume = 0.045;
                        } else if (wallsFrontonSelections.width == "400" && wallsFrontonSelections.itemHeight == "250") {
                            consumptionRate = 16.67;
                            oneBlockVolume = 0.06;
                        } else if (wallsFrontonSelections.width == "400" && wallsFrontonSelections.itemHeight == "100") {
                            consumptionRate = 41.66;
                            oneBlockVolume = 0.024;
                        } else if (wallsFrontonSelections.width == "100" && wallsFrontonSelections.itemHeight == "400") {
                            consumptionRate = 41.66;
                            oneBlockVolume = 0.024;
                        }

                    }
                }

                let totalBricksRequired = Math.ceil(totalFrontonsArea * consumptionRate);

                if (wallsFrontonSelections.materialType === "gazoblock") {
                    totalBricksRequired = Math.ceil(totalFrontonsArea * (wallsFrontonSelections.width/1000) * consumptionRate);
                } else if (wallsFrontonSelections.materialType === "kirpich") {
                    totalBricksRequired = Math.ceil(wallsVolume * consumptionRate);
                }

                console.log("totalBricksRequired " + totalBricksRequired);

                let poddonDivisible = Math.ceil(totalBricksRequired / wallsFrontonSelections.poddonQnty) * wallsFrontonSelections.poddonQnty;

                console.log("poddonQnty " + wallsFrontonSelections.poddonQnty);
                console.log("poddonDivisible " + poddonDivisible);
                console.log("consumptionRate " + consumptionRate);


                let poddonPrice = 0;
                if (dataFromJSON['PODDON_PRICE'][wallsFrontonSelections.makerID]) {
                    poddonPrice = dataFromJSON['PODDON_PRICE'][wallsFrontonSelections.makerID];
                }

                console.log('itemPrice ' + wallsFrontonSelections.itemPrice);


                let totalThisFloorWallsCost = poddonDivisible * wallsFrontonSelections.itemPrice + poddonPrice * Math.ceil(totalBricksRequired / wallsFrontonSelections.poddonQnty) + poddonPrice * Math.ceil(totalBricksRequired / wallsFrontonSelections.poddonQnty);

                if (wallsFrontonSelections.materialType === "gazoblock") {
                    totalThisFloorWallsCost = poddonDivisible * oneBlockVolume * wallsFrontonSelections.itemPrice + poddonPrice * Math.ceil(totalBricksRequired / wallsFrontonSelections.poddonQnty) + poddonPrice * Math.ceil(totalBricksRequired / wallsFrontonSelections.poddonQnty);

                }

                if (isNaN(totalWallsCost)) {
                    totalWallsCost = 0;
                }

                totalFrontonsWallsCost = totalFrontonsWallsCost + totalThisFloorWallsCost;

                totalCost = totalBasementCost + totalFasadniyCost + totalFrontonsCost + totalPeregorodkiCost + totalWallsCost + totalFrontonsWallsCost;

                let newResultsInstance = $(wallsResultLinesHtmlTemplate).clone();

                $(newResultsInstance).attr('data-floorNumber', "frontons");
                $(newResultsInstance).find('.floorNumber').text("Фронтоны");

                $(newResultsInstance).appendTo('.results__construction__type[data-resultsConstructionType="walls"]');


                $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--maker__js .result_value').text(wallsFrontonSelections.makerName);
                $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--bricksTotal__js .result_value').text(totalBricksRequired);
                $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonDivisibleQnty__js .result_value').text(poddonDivisible);
                $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonQnty__js .result_value').text(wallsFrontonSelections.poddonQnty);
                $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonAmount__js .result_value').text(Math.ceil(totalBricksRequired / wallsFrontonSelections.poddonQnty));
                $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonPrice__js .result_value').text(poddonPrice);
                $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--totalCost__js .result_value').text(formatCost(totalThisFloorWallsCost));

                if (wallsFrontonSelections.materialType === "kirpich") {
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--format__js').css("display", "flex");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--strengthMark__js').css("display", "flex");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--densityMark__js').css("display", "none");

                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--bricksTotal__js .result_name span').text("кирпича, шт");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonDivisibleQnty__js .result_name span').text("шт");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonQnty__js .result_name span').text("шт");



                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--strengthMark__js .result_value').text(wallsFrontonSelections.strengthMark);
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--format__js .result_value').text(wallsFrontonSelections.format);
                }

                if (wallsFrontonSelections.materialType === "keramblock") {
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--format__js').css("display", "flex");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--strengthMark__js').css("display", "none");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--densityMark__js').css("display", "none");

                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--bricksTotal__js .result_name span').text("блока, шт");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonDivisibleQnty__js .result_name span').text("шт");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonQnty__js .result_name span').text("шт");


                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--format__js .result_value').text(wallsFrontonSelections.format);
                }

                if (wallsFrontonSelections.materialType === "gazoblock") {
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--format__js').css("display", "none");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--strengthMark__js').css("display", "none");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--densityMark__js').css("display", "flex");

                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--bricksTotal__js .result_name span').text("блока, м3");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonDivisibleQnty__js .result_name span').text("м3");
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--poddonQnty__js .result_name span').text("м3");


                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"] .result__line--densityMark__js .result_value').text(wallsFrontonSelections.densityMark);
                }

                $('.calc-col__results .final__price .final_price_value span').text(formatCost(totalCost));

                $('.results__construction__type[data-resultsConstructionType="walls"]').removeClass('hidden');
                $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="frontons"]').removeClass('hidden');
                    }

            index++;

        }
        /* FRONTONS END */



        /* BASEMENT */

        let tempData = {};
        let basementSet = {};

        let basementSelectedSettings = {id: null, maker: null, size: null, strengthMark: null, makerName: null, poddonQnty:null};
        basementSet.makers = [];

        let basementKladkaTypeHtml = $('.basement__kladka-types .kladka__type--item[data-kladkaTypeNumber="1"]').clone();

        $('body').on('click', '.tsokol-floor__js', function () {

            $('.calc_housePart').addClass('hidden');
            $('.calc_housePart--basement').removeClass('hidden');

            if (!basementSelectedSettings.id) {

                let constructionType = "TSOKOLNIY";

                let currentConstructionItems;
                let makersElements = "";
                basementSet.makers = [];

                currentConstructionItems = dataFromJSON.BRICKS[constructionType];

                let tempValueIDs = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

                $(currentConstructionItems).each(function (index, value) {
                    if (!(tempValueIDs.includes(value.MAKER_VALUE_ID))) {
                        if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                            console.log(value);
                        } else {
                            basementSet.makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                            tempValueIDs.push(value.MAKER_VALUE_ID);
                        }
                    }
                });

                $(basementSet.makers).each(function (index, value) {
                    makersElements += `<div class="maker calc-maker__js" data-maker_id="${value.value_id}">
                                                <div class="img">
                                                    <img src="${value.maker_image}" alt="">
                                                </div>
                                                <span>${value.value}</span>
                                             </div>`;
                });

                $('.calc_housePart--basement .calc_makers_row__makers').html(makersElements);
                makersElements = "";
            }

            $('.floor-btn').removeClass('active');
            $(this).addClass('active');
        });

        $("body").on("click", ".calc_housePart--basement .calc-maker__js", function () {

            let makerID = this.getAttribute('data-maker_id');
            $('.calc_housePart--basement .calc-maker__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            basementSelectedSettings.maker = makerID;
            basementSelectedSettings.makerName = $(this).find('span').text();

            console.log(dataFromJSON.BRICKS['TSOKOLNIY']);

            let sizesArr = [];
            $(dataFromJSON.BRICKS['TSOKOLNIY']).each(function (index, value) {
                if (!sizesArr.includes(value.SIZE) && value.MAKER_VALUE_ID === makerID && value.SIZE !== null) {
                    sizesArr.push(value.SIZE);
                }
            });

             let sizesElements = "";
             $(sizesArr).each(function (index, value) {
                 sizesElements += `<div class="calc_option_btn calc_brick_size__js" data-material__size="${value}">${value}</div>`;
             });

             $('.calc_housePart--basement .calc_option_row--format').html(sizesElements);

             $('.calc_housePart--basement .calc_option_sub_block_formats').addClass('hidden');
             $('.calc_housePart--basement .calc_option_sub_block_strength_mark').addClass('hidden');
             $('.calc_housePart--basement .calc_option_sub_block_items').addClass('hidden');
             $('.calc_housePart--basement .calc_option_sub_block_kladka_parameters').addClass('hidden');


             $('.calc_housePart--basement .calc_option_sub_block_formats').removeClass('hidden');

            hideBasementCalculaion();
        });

        $("body").on("click", ".calc_housePart--basement .calc_brick_size__js", function () {
            let materialSize = this.getAttribute('data-material__size');
            $('.calc_brick_size__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            basementSelectedSettings.size = materialSize;

            let strengthMarkArr = [];

            $(dataFromJSON.BRICKS['TSOKOLNIY']).each(function (index, value) {
                if (!strengthMarkArr.includes(value.STRENGTH_MARK) &&
                    value.MAKER_VALUE_ID === basementSelectedSettings.maker &&
                    value.SIZE === materialSize && value.STRENGTH_MARK !== null) {

                    strengthMarkArr.push(value.STRENGTH_MARK);
                }
            });

            let strengthMarkElements = "";
            $(strengthMarkArr).each(function (index, value) {
                strengthMarkElements += `<div class="calc_option_btn calc-strength_mark__js" data-material__strength-mark="${value}">${value}</div>`;
            });

            $('.calc_housePart--basement .calc_option_row--strength_mark').html(strengthMarkElements);

            $('.calc_housePart--basement .calc_option_sub_block_items').addClass('hidden');
            $('.calc_housePart--basement .calc_option_sub_block_kladka_parameters').addClass('hidden');
            $('.calc_housePart--basement .calc_option_sub_block_strength_mark').removeClass('hidden');

            hideBasementCalculaion();

        });

        $("body").on("click", ".calc_housePart--basement .calc-strength_mark__js", function () {
            let strengthMark = this.getAttribute('data-material__strength-mark');
            basementSelectedSettings.strengthMark = strengthMark;

            $('.calc-strength_mark__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            let itemsArr = [];
            let itemsIdArr = [];
            $(dataFromJSON.BRICKS['TSOKOLNIY']).each(function (index, value) {
                if (!itemsIdArr.includes(value.ID) &&
                    value.MAKER_VALUE_ID === basementSelectedSettings.maker &&
                    value.SIZE === basementSelectedSettings.size &&
                    value.STRENGTH_MARK === basementSelectedSettings.strengthMark) {

                    itemsIdArr.push(value.ID);
                    itemsArr.push({strengthMark: value.STRENGTH_MARK, product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
                }
            });

            let itemsElements = "";
            $(itemsArr).each(function (index, value) {
                itemsElements += `<div class="calc_option_btn_border calc-item__js" data-itemPrice="${value.itemPrice}" data-poddonQnty="${value.poddonQnty}" data-material__item-id="${value.product_id}">
                                    <img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">
                                    ${value.product_name}
                                </div>`;
            });

            $('.calc_housePart--basement .calc_option_row--items').html(itemsElements);
            $('.calc_housePart--basement .calc_option_sub_block_items').removeClass('hidden');

            $('.calc_housePart--basement .calc_option_sub_block_kladka_parameters').addClass('hidden');

            hideBasementCalculaion();

        });

        let rowNameArr = {"1": "ряд", "2":"ряда", "3": "ряда", "4": "ряда", "5": "рядов"};

        $("body").on("click", ".calc_housePart--basement .calc-item__js", function () {
            let selectedItemId = this.getAttribute('data-material__item-id');
            let selectedItemPoddonQnty = this.getAttribute('data-poddonQnty');
            let selectedItemPrice = this.getAttribute('data-itemPrice');

            basementSelectedSettings.id = selectedItemId;
            basementSelectedSettings.poddonQnty = selectedItemPoddonQnty;
            basementSelectedSettings.itemPrice = selectedItemPrice;

            $('.calc_housePart--basement .calc-item__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');


            if (basementSelectedSettings.size == "1.4НФ") {
                $('.calc_housePart--basement .input-col__kladka_height-js input').val("1 ряд (9.8 см)");
            }

            //
            // let kladkaHeightEl = "";
            // for (let i = 1; i < 6; i++) {
            //     kladkaHeightEl += `<option value="${i}">${i} ${rowNameArr[i]} (${(rowHeight * i).toFixed(2)} см)</option>`;
            // }
            //
            // $('.calc_housePart--basement .kladka__type--item select.kladka__height__js').html(kladkaHeightEl);
            // $('select.kladka__height__js').trigger('refresh');

            //////////


            $('.calc_housePart--basement .calc_option_sub_block_kladka_parameters').removeClass('hidden');

            calculateBasementMaterials();
        });

        $("body").on('click', '.calc_housePart--basement .input-col__kladka_height-js .input-handler', function () {
            let currentValue = $(this).closest('.input-box').find('input').attr('data-value');

            let newValue;
            if ($(this).hasClass("input-handler__plus")) {
                newValue = parseInt(currentValue) + 1;
            } else if ($(this).hasClass("input-handler__minus")) {
                newValue = parseInt(currentValue) - 1;
                if (newValue === 0)
                    newValue = 1;
            }

            $(this).closest('.input-box').find('input').attr('data-value', newValue);

            let rowName;
            if (newValue < 5) {
                rowName = rowNameArr[newValue];
            } else {
                rowName = "рядов";
            }

            let rowHeight = 7.5; //высота кирпича

            if (basementSelectedSettings.size == "1.4НФ") {
                rowHeight = 9.8;
            }

            $(this).closest('.input-box').find('input').val(newValue + " " + rowName + " (" + (rowHeight*newValue).toFixed(2) + " см)");

            calculateBasementMaterials();

        });

        $('body').on('change', '.calc_housePart--basement select.kladka__height__js', function () {
            let setkaMaxRows = this.value;

            let setkaRowsEl = "";
            for (let i = 1; i <= setkaMaxRows; i++) {
                setkaRowsEl += `<option value="${i}">${i} ${rowNameArr[i]}</option>`;
            }

            //$('.calc_housePart--basement .kladka__type--item select.setka__rows_js').html(setkaRowsEl);
            $(this).closest('.kladka__type--item').find('.input-col__basement-setka select.setka__rows_js').html(setkaRowsEl);


            $('.calc_housePart--basement select.setka__rows_js').trigger('refresh');

            calculateBasementMaterials();
        });

        $('body').on('change', '.calc_housePart--basement select.setka__rows_js', function () {
            calculateBasementMaterials();
        });


        $('body').on('change', '.calc_housePart--basement select.wall_width_js', function () {
            calculateBasementMaterials();
        });

        $('body').on('click', '.calc_housePart--basement .calcAddBtn[data-calcAddWhat="basement-kladka-type"]', function () {
            let lastKladkaItemClone = $(basementKladkaTypeHtml).clone();

            let currentKladkaItemNumber = $('.basement__kladka-types .kladka__type--item').last().attr('data-kladkaTypeNumber');
            lastKladkaItemClone.attr('data-kladkaTypeNumber', parseInt(currentKladkaItemNumber) + 1);
            lastKladkaItemClone.find('.calc__block-title span.kladka_type_number__js').text(parseInt(currentKladkaItemNumber) + 1);

            lastKladkaItemClone.find('.input-col__wall-width-js').html();
            lastKladkaItemClone.find('.input-col__kladka_height-js').html();
            lastKladkaItemClone.find('.input-col__basement-setka').html();

            $(lastKladkaItemClone).appendTo('.calc_housePart--basement .calc_option_sub_block_kladka_parameters .kladka__types');

            $(lastKladkaItemClone).find('.input-col__wall-width-js').html(`<div class="calc_label">Толщина стен</div>
                                                <select class="calc_select wall_width_js" name="" id="">
                                                    <option value="0.5">0.5 кирпича (12 см)</option>
                                                    <option value="1">1 кирпич (25 см)</option>
                                                    <option value="1.5">1.5 кирпича (38 см)</option>
                                                    <option value="2">2 кирпича (51 см)</option>
                                                    <option value="2.5">2.5 кирпича (64 см)</option>
                                                    <option value="3">3 кирпича (77 см)</option>
                                                </select>`);


            let rowHeight = 7.5; //высота кирпича

            if (basementSelectedSettings.size == "1.4НФ") {
                rowHeight = 9.8;
            }

            // let kladkaHeightEl = "";
            // for (let i = 1; i < 6; i++) {
            //     kladkaHeightEl += `<option value="${i}">${i} ${rowNameArr[i]} (${(rowHeight * i).toFixed(2)} см)</option>`;
            // }

            // $(lastKladkaItemClone).find('.input-col__kladka_height-js').html(`<div class="calc_label">Высота кладки</div>
            //                                     <select class="calc_select kladka__height__js" name="kladka_height" id="">` + kladkaHeightEl + `</select>`);
            //

            $(lastKladkaItemClone).find('.input-col__basement-setka').html(`<div class="calc_label">Укладывать сетку каждые</div>
                                                <select class="calc_select setka__rows_js" name="" id="">
                                                    <option value="1">1 ряд</option>
                                                    <option value="2">2 ряда</option>
                                                    <option value="3">3 ряда</option>
                                                    <option value="4">4 ряда</option>
                                                    <option value="5">5 ряда</option>
                                                </select>`);


             $('.calc_housePart--basement select.wall_width_js').styler();
             //$('.calc_housePart--basement select.kladka__height__js').styler();
             $('.calc_housePart--basement select.setka__rows_js').styler();

            calculateBasementMaterials();
        });

        $('body').on('change', '.calc_housePart--basement .input-col__basement-perimeter input', function () {
            calculateBasementMaterials();
        });

        function calculateBasementMaterials() {
            totalBasementCost = 0;
            let allBasementKladkaTypes = $('.calc_housePart--basement .kladka__type--item');

            let brickRowHeight = 0.075;
            let consumptionRate = 400;
            if (basementSelectedSettings.size === "1.4НФ") {
                brickRowHeight = 0.098;
                consumptionRate = 308;
            }

            let basementKladkaVolume = 0;

            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--setkaMeter__js').remove();
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--setkaWidth__js').remove();

            allBasementKladkaTypes.each(function (index, value) {
                let widthValue = $(this).find('select.wall_width_js').val();
                let perimeterValue = $(this).find('.input-col__basement-perimeter input').val();
                //let heightValue = $(this).find('.input-col__kladka_height-js select.kladka__height__js').val();
                let heightValue = $(this).find('.input-col__kladka_height-js input').attr('data-value');
                let setkaEachRow = $(this).find('.input-col__basement-setka select.setka__rows_js').val();
                let kladkaWidth = (parseFloat(widthValue) / 0.5) * 0.13 - 0.01;

                basementKladkaVolume += ((parseFloat(widthValue) / 0.5) * 0.13 - 0.01) * (parseInt(heightValue) * brickRowHeight) * parseFloat(perimeterValue);
                basementKladkaVolume = parseFloat(basementKladkaVolume.toFixed(3));

                let setkaRunningMeter = (parseInt(heightValue) / parseInt(setkaEachRow)) * parseFloat(perimeterValue);

                let setkaMeterEl = `<div class="result__line result__line--setkaMeter__js">
                                        <div class="result_name">Сетка №${index+1}, пог. м</div>
                                        <div class="result_value">${setkaRunningMeter.toFixed(2)}</div>
                                    </div>
                                    
                                    <div class="result__line result__line--setkaWidth__js">
                                        <div class="result_name">Ширина сетки №${index+1}, м.</div>
                                        <div class="result_value">${kladkaWidth.toFixed(2)}</div>
                                    </div>`;

                $(setkaMeterEl).insertBefore('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--totalCost__js');
            });


            let bricksRequired = Math.ceil(basementKladkaVolume * consumptionRate);
            let poddonDivisible = Math.ceil(bricksRequired / basementSelectedSettings.poddonQnty) * basementSelectedSettings.poddonQnty;

            let poddonPrice = 0;
            if (dataFromJSON['PODDON_PRICE'][basementSelectedSettings.maker]) {
                 poddonPrice = dataFromJSON['PODDON_PRICE'][basementSelectedSettings.maker];
            }

            totalBasementCost = poddonDivisible * basementSelectedSettings.itemPrice + poddonPrice * Math.ceil(bricksRequired / basementSelectedSettings.poddonQnty) + poddonPrice * Math.ceil(bricksRequired / basementSelectedSettings.poddonQnty);

            totalCost = totalBasementCost + totalFasadniyCost + totalFrontonsCost + totalPeregorodkiCost + totalWallsCost + totalFrontonsWallsCost;


            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--maker__js .result_value').text(basementSelectedSettings.makerName);
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--format__js .result_value').text(basementSelectedSettings.size);
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--strengthMark__js .result_value').text(basementSelectedSettings.size);
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--bricksTotal__js .result_value').text(bricksRequired);
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--poddonDivisibleQnty__js .result_value').text(poddonDivisible);
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--poddonQnty__js .result_value').text(basementSelectedSettings.poddonQnty);
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--poddonAmount__js .result_value').text(Math.ceil(bricksRequired / basementSelectedSettings.poddonQnty));
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--poddonPrice__js .result_value').text(poddonPrice);
            $('.results__construction__type[data-resultsConstructionType="tsokolny"] .result__line--totalCost__js .result_value').text(formatCost(totalBasementCost));

            $('.calc-col__results .final__price .final_price_value span').text(formatCost(totalCost));

            $('.results__construction__type[data-resultsConstructionType="tsokolny"]').removeClass('hidden');

            console.log(basementKladkaVolume);
            console.log(bricksRequired);
        }

        function hideBasementCalculaion() {
            $('.results__construction__type[data-resultsConstructionType="tsokolny"]').addClass('hidden');

            if (totalCost > 0) {
                totalCost = totalCost - totalBasementCost;
            }

            let totalCostCopy = totalCost;
            
            totalCostCopy = totalCostCopy.toFixed(2);

            $('.calc-col__results .final__price .final_price_value span').text(numberWithSpaces(totalCostCopy));
        }

        function formatCost(costNumber) {
            let formattedCost = costNumber.toFixed(2);
            formattedCost = numberWithSpaces(formattedCost);

            return formattedCost;
        }



        /////////////////////////////////////////////////////


    ///// CALCULATOR REGULAR FLOOR ////////////



    $('body').on('click', '.regular-floor-btn', function () {
        let thisFloorNumber = $(this).attr('data-floorNum');
        $('.calc_housePart').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"]').removeClass('hidden');

        $('.floor-btn').removeClass('active');
        $(this).addClass('active');
    });

    ///////////////////////////////////////
    /* FASAD START*/

    $('body').on('click', '.calc_housePart--floor .construction_type_item__js[data-construction_type="FASADNIY"]', function () {
        $(this).closest('.calc_construction').attr('data-constructionType', "FASADNIY");
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block ').not('.calc_option_sub_block_maker').addClass('hidden');
        $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn_border').removeClass('selected');
        $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn').removeClass('selected');

        $(this).closest('.constructions_options_row').find('.construction_type_item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        let makers = [];

        let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

        $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
            if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                    console.log(value);
                } else {
                    makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                    tempValueMakersID.push(value.MAKER_VALUE_ID);
                }
            }
        });

        let makersElements = "";
        $(makers).each(function (index, value) {
            makersElements += `<div class="maker calc-maker__js" data-type="FASADNIY" data-maker_id="${value.value_id}">
                                        <div class="img">
                                            <img src="${value.maker_image}" alt="">
                                        </div>
                                        <span>${value.value}</span>
                                     </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_makers_row__makers').html(makersElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_block--makers').removeClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_maker').removeClass('hidden');

        /*LETS SHOW "ADD CONSTUCTION" BUTTON*/
        if ($(this).closest('.constructions_options_row').find('.construction_type_item__js').length > 1) {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"]').find('.add_new_construction').removeClass('hidden');
        }

        $(this).closest('.calc_construction').find('.calc_block--pereghorodkiLength').addClass('hidden');

        console.log(dataFromJSON.BRICKS['FASADNIY']);

    });

    $('body').on('click', ".calc_housePart--floor .calc-maker__js[data-type='FASADNIY']", function () {
        let thisMakerID = this.getAttribute('data-maker_id');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');


        $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        fasadniySelections['floor-' + thisFloorNumber]['makerID'] = thisMakerID;
        fasadniySelections['floor-' + thisFloorNumber]['makerName'] = $(this).find('span').text();

        let brickFormatArr = [];
        $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
            if (!brickFormatArr.includes(value.SIZE) && value.MAKER_VALUE_ID === thisMakerID) {
                brickFormatArr.push(value.SIZE);
            }
        });

        let formatElements = "";
        $(brickFormatArr).each(function (index, value) {
            formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="FASADNIY" data-material__size="${value}">${value}</div>`;
        });



        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_row--format').html(formatElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_formats').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_strength_mark').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_surfaces').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_colorTones').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_color').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_formats').removeClass('hidden');

        hideFasadniyCalculaion(thisFloorNumber);
    });

    $("body").on("click", ".calc_housePart--floor .calc_brick_size__js[data-type='FASADNIY']", function () {
        let thisFormat = this.getAttribute('data-material__size');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_brick_size__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        fasadniySelections['floor-' + thisFloorNumber]['format'] = thisFormat;

        let surfacesArr = [];

        $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
            if (!surfacesArr.includes(value.SURFACE) &&
                value.MAKER_VALUE_ID === fasadniySelections['floor-' + thisFloorNumber].makerID &&
                value.SIZE === thisFormat) {

                surfacesArr.push(value.SURFACE);
            }
        });

        let surfacesElements = "";
        $(surfacesArr).each(function (index, value) {
            console.log(value);
            if (value !== null) {
                surfacesElements += `<div class="calc_option_btn calc-surface__js" data-type="FASADNIY" data-material__surface="${value}">${value}</div>`;
            }
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_row--surface').html(surfacesElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_surfaces').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_colorTones').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_color').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_surfaces').removeClass('hidden');

        hideFasadniyCalculaion(thisFloorNumber);
    });

    $('body').on('click', '.calc_housePart--floor .calc-surface__js[data-type="FASADNIY"]', function () {
        let thisMaterialSurface = this.getAttribute('data-material__surface');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-surface__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        fasadniySelections['floor-' + thisFloorNumber]['surface'] = thisMaterialSurface;

        let colorTonesArr = [];
        $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
            if (!colorTonesArr.includes(value.COLOR_TONE) &&
                value.MAKER_VALUE_ID === fasadniySelections['floor-' + thisFloorNumber].makerID &&
                value.SIZE === fasadniySelections['floor-' + thisFloorNumber].format &&
                value.SURFACE === thisMaterialSurface) {

                colorTonesArr.push(value.COLOR_TONE);
            }
        });

        let colorTonesElements = "";
        $(colorTonesArr).each(function (index, value) {
            if (value !== null) {
                colorTonesElements += `<div class="calc_option_btn_border calc-colorTone__js" data-type="FASADNIY" data-material__colorTone="${value}">
                                        <span class="color color-${value}"></span>
                                        ${dataFromJSON.COLOR_TONES[value]}
                                     </div>`;
            }

        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_row--tone').html(colorTonesElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_colorTones').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_color').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_colorTones').removeClass('hidden');

        //console.log(colorTonesArr);
        hideFasadniyCalculaion(thisFloorNumber);
    });

    $('body').on('click', '.calc_housePart--floor .calc-colorTone__js[data-type="FASADNIY"]', function () {
        let thisColorTone = this.getAttribute('data-material__colorTone');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-colorTone__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        fasadniySelections['floor-' + thisFloorNumber]['colorTone'] = thisColorTone;

        let colorsArr = [];
        $(dataFromJSON.BRICKS['FASADNIY']).each(function (index, value) {
            if (!colorsArr.includes(value.COLOR) &&
                value.MAKER_VALUE_ID === fasadniySelections['floor-' + thisFloorNumber].makerID &&
                value.SIZE === fasadniySelections['floor-' + thisFloorNumber].format &&
                value.SURFACE === fasadniySelections['floor-' + thisFloorNumber].surface &&
                value.COLOR_TONE === thisColorTone) {

                colorsArr.push({color: value.COLOR, product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
            }
        });

        let colorElements = "";
        $(colorsArr).each(function (index, value) {
            let klinker = "";
            if (value.product_id == 9057 || value.product_id == 9056 || value.product_id == 9055 || value.product_id == 9054 || value.product_id == 9051 || value.product_id == 9052 || value.product_id == 9053) {
                klinker = "(клинкерный)";
            }

            let category = "";
            if (fasadniySelections['floor-' + thisFloorNumber].makerID === "502") {
                let thisColor = value.color;
                let thisColorPrice = value.itemPrice;
                let thisItemId = value.product_id;

                $(colorsArr).each(function (i, v) {
                    if (thisColor === v.color && thisItemId !== v.product_id) {
                        if (parseFloat(thisColorPrice) > parseFloat(v.itemPrice)) {
                            category = "(1 категория)";
                        } else {
                            category = "(2 категория)";
                        }
                    }
                });
            }

            let img = "";
            if (typeof dataFromJSON.PRODUCTS_IMAGES[value.product_image_id] !== "undefined") {
                img = `<img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">`;
            }
            colorElements += `<div class="calc_option_btn_border calc-color__js" data-type="FASADNIY" data-itemID="${value.product_id}" data-itemPrice="${value.itemPrice}" data-poddonQnty="${value.poddonQnty}" data-material__color="${value.color}">
                                    ${img}
                                    ${value.color} ${klinker} ${category}
                                </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_row--color').html(colorElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_option_sub_block_color').removeClass('hidden');

        hideFasadniyCalculaion(thisFloorNumber);
    });

    $('body').on('click', '.calc_housePart--floor .calc-color__js[data-type="FASADNIY"]', function () {
        let thisBrickColor = this.getAttribute('data-material__color');
        let thisItemId = this.getAttribute('data-itemID');
        let selectedItemPoddonQnty = this.getAttribute('data-poddonQnty');
        let selectedItemPrice = this.getAttribute('data-itemPrice');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        fasadniySelections['floor-' + thisFloorNumber]['color'] = thisBrickColor;
        fasadniySelections['floor-' + thisFloorNumber]['itemID'] = thisItemId;
        fasadniySelections['floor-' + thisFloorNumber]['poddonQnty'] = selectedItemPoddonQnty;
        fasadniySelections['floor-' + thisFloorNumber]['itemPrice'] = selectedItemPrice;

        if (thisItemId === "8836" || thisItemId === "8837") {
            fasadniySelections['floor-' + thisFloorNumber]['poddonQnty'] = 434;
        }

        $(this).closest('.calc_construction').find('.calc-color__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        calculateFasadMaterials();
        console.log(fasadniySelections);
    });

    $('body').on('click', '.calc_housePart--floor .calcAddBtn[data-calcAddWhat="regularDoorType"]', function () {
        let doorEl = $(this).closest('.calc_block--doors').find('.floor__door-item[data-doorNum="1"]').clone();

        let currentDoorsNum = $(this).closest('.calc_block--doors').find('.floor__door-item').last().attr('data-doorNum');

        $(doorEl).attr('data-doorNum', parseInt(currentDoorsNum) + 1);
        $(doorEl).find('.calc__block-title span.doorNum').text(parseInt(currentDoorsNum) + 1);

        $(doorEl).find('input').val(0);

        $(doorEl).insertBefore(this);

    });

    $('body').on('click', '.calc_housePart--floor .calcAddBtn[data-calcAddWhat="regularWindowType"]', function () {
        let windowEl = $(this).closest('.calc_block--windows').find('.floor__window-item[data-windowNum="1"]').clone();

        let currentWindowsNum = $(this).closest('.calc_block--windows').find('.floor__window-item').last().attr('data-windowNum');

        console.log(windowEl);

        $(windowEl).attr('data-windowNum', parseInt(currentWindowsNum) + 1);
        $(windowEl).find('.calc__block-title span.windowNum').text(parseInt(currentWindowsNum) + 1);

        $(windowEl).find('input').val(0);

        $(windowEl).insertBefore(this);
    });

    $('body').on('change', '.calc_housePart--floor input[data-input="floorPerimeter"]', function () {

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');
        let floorHeightInput = $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] input[data-input="floorHeight"]').val();

        if (floorHeightInput !== "" && floorHeightInput != "0") {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').removeClass('hidden');
            fasadniySelections['floor-' + thisFloorNumber] = {'perimeter': $(this).val(), 'height': floorHeightInput};
            peregorodkiSelections['floor-' + thisFloorNumber] = {'perimeter': $(this).val(), 'height': floorHeightInput};
            wallsSelections['floor-' + thisFloorNumber] = {'perimeter': $(this).val(), 'height': floorHeightInput};
        } else {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').addClass('hidden');
        }

        if ($(this).val() === "" || $(this).val() == "0") {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').addClass('hidden');
        }

        if ($(this).val() === "") {
            $(this).val(0);
        }

        calculateFasadMaterials();

        console.log(fasadniySelections);
    });

    $('body').on('change', '.calc_housePart--floor input[data-input="floorHeight"]', function () {

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');
        let floorPerimeterInput = $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] input[data-input="floorPerimeter"]').val();

        if (floorPerimeterInput !== "" && floorPerimeterInput != "0") {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').removeClass('hidden');
            fasadniySelections['floor-' + thisFloorNumber] = {'perimeter': floorPerimeterInput, 'height': $(this).val()};
            peregorodkiSelections['floor-' + thisFloorNumber] = {'perimeter': floorPerimeterInput, 'height': $(this).val()};
            wallsSelections['floor-' + thisFloorNumber] = {'perimeter': floorPerimeterInput, 'height': $(this).val()};

        } else {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').addClass('hidden');
        }

        if ($(this).val() === "" || $(this).val() == "0") {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').addClass('hidden');
        }

        if ($(this).val() === "") {
            $(this).val(0);
        }

        calculateFasadMaterials();
    });

    $('body').on('change', '.calc_housePart--floor .door_input_row input', function () {

        if ($(this).val() === "") {
            $(this).val(0);
        }

        let constructionType = $(this).closest('.calc_construction').attr('data-constructionType');
        
        if (constructionType === "PEREGORODKI") {
            calculatePeregorodkiMaterials();
        } else if(constructionType === "FASADNIY") {
            calculateFasadMaterials();
        } else if(constructionType === "WALLS") {
            calculateWallsMaterials();
        }

        calculateDoorsArea();
    });

    $('body').on('change', '.calc_housePart--floor .window_input_row input', function () {

        if ($(this).val() === "") {
            $(this).val(0);
        }

        calculateWindowsArea();

        let constructionType = $(this).closest('.calc_construction').attr('data-constructionType');

        if (constructionType === "PEREGORODKI") {
            calculatePeregorodkiMaterials();
        } else if(constructionType === "FASADNIY") {
            calculateFasadMaterials();
        } else if(constructionType === "WALLS") {
            calculateWallsMaterials();
        }

    });

    $('body').on('click','.floor__door-item .calc__block-clear', function () {
        let thisClosestParent = $(this).closest('.calc_block--doors');

        let remainingDoorsCount = $(this).closest('.calc_block--doors').find('.floor__door-item').length - 1;

        $(this).closest('.floor__door-item').remove();

        if (remainingDoorsCount > 1) {
            let i = 1;
            $(thisClosestParent).find('.floor__door-item').each(function (index, value) {
                $(this).attr('data-doorNum', i);
                $(this).find('.calc__block-title span.doorNum').text(i);
                i++;
            });
        }
        console.log(thisClosestParent.find('.floor__door-item').length);

        calculateDoorsArea();
        calculateFasadMaterials();
    });

    $('body').on('click','.floor__window-item .calc__block-clear', function () {
        let thisClosestParent = $(this).closest('.calc_block--windows');

        let remainingWindowsCount = $(this).closest('.calc_block--windows').find('.floor__window-item').length - 1;

        $(this).closest('.floor__window-item').remove();

        if (remainingWindowsCount > 1) {
            let i = 1;
            $(thisClosestParent).find('.floor__window-item').each(function (index, value) {
                $(this).attr('data-windowNum', i);
                $(this).find('.calc__block-title span.windowNum').text(i);
                i++;
            });
        }

        calculateWindowsArea();
        calculateFasadMaterials();
    });

    $('body').on('change', '.calc_construction[data-constructiontype="FASADNIY"] select.calc_setka_rows__js', function () {
       calculateFasadMaterials();
    });

    $('body').on('click', '.calc_housePart--floor .add_new_construction', function () {

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        let thisFloorConstructionNumber = $(this).closest('.calc_housePart--floor').find('.calc_construction').length;

        let thisFloorUnactiveConstructions = $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').last().find('.construction_type_item__js').not('.selected').clone();;

        let thisСonstructionTemplateHtmlClone = $(constructionTemplateHtml).clone();

        $(thisСonstructionTemplateHtmlClone).find('.calc__block-title span.construction__number__js').text(thisFloorConstructionNumber + 1);

        $(thisСonstructionTemplateHtmlClone).find('.constructions_options_row').html(thisFloorUnactiveConstructions);

        $(thisСonstructionTemplateHtmlClone).find('.input-col__setka__rows__js').html(`<div class="calc_label">Укладывать каждые</div>
                                                                                        <select class="calc_select calc_setka_rows__js" name="" id="">
                                                                                            <option value="1">1 ряд</option>
                                                                                            <option value="2">2 ряда</option>
                                                                                            <option value="3">3 ряда</option>
                                                                                            <option value="4">4 рядов</option>
                                                                                            <option value="5">5 рядов</option>
                                                                                        </select>`);

        $(thisСonstructionTemplateHtmlClone).removeClass('hidden');
        $(thisСonstructionTemplateHtmlClone).insertBefore('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .add_new_construction');

        // console.log($('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').last().find('select.calc_setka_rows__js'));
        //$('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').last().find('select.calc_setka_rows__js').html(optionsEl);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').last().find('select.calc_setka_rows__js').styler();
        // $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction').last().find('select.calc_setka_rows__js').remove();


        $(this).addClass('hidden');
        console.log('add new construction');
    });


    function calculateFasadMaterials() {

        let index = 1;
        let totalWallsArea = 0;
        totalFasadniyCost = 0;

        let fasadniyResultLinesHtmlTemplate = $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="1"]').clone();
        $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines').not('.result__lines[data-floorNumber="1"], .result__lines[data-floorNumber="frontons"]').remove();

        for (let floor in fasadniySelections) {
            if (fasadniySelections.hasOwnProperty(floor)) {

                if ( typeof fasadniySelections[floor].itemID !== "undefined") {
                    totalWallsArea += parseFloat(fasadniySelections[floor].perimeter) * parseFloat(fasadniySelections[floor].height) - (doorsAndWindowsArea.doors + doorsAndWindowsArea.windows) - frontonsArea;

                    let thisFloorArea = 0;
                    let thisFloorDoorArea = 0;
                    let thisFloorWindowArea = 0;

                    $('.calc_housePart--floor[data-floorNumberText="'+floor+'"] .floor__door-item').each(function (i, value) {
                        let thisDoorQnty = parseInt($(this).find('.input-col--door__qty input').val());
                        let thisDoorHeightCm = parseFloat($(this).find('.input-col--door__height input').val());
                        let thisDoorWidthCm = parseFloat($(this).find('.input-col--door__width input').val());

                        thisFloorDoorArea += thisDoorQnty * (thisDoorHeightCm/100 * thisDoorWidthCm/100).toFixed(2);
                    });

                    $('.calc_housePart--floor[data-floorNumberText="'+floor+'"] .floor__window-item').each(function (i, value) {
                        let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
                        let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
                        let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

                        thisFloorWindowArea += thisWindowQnty * (thisWindowHeightCm/100 * thisWindowWidthCm/100).toFixed(2);
                    });

                    thisFloorArea += parseFloat(fasadniySelections[floor].perimeter) * parseFloat(fasadniySelections[floor].height) - thisFloorDoorArea - thisFloorWindowArea;

                    console.log("this floor area " + thisFloorArea);

                    let consumptionRate = 51; //if format == 1nf
                    let selectedFormat = fasadniySelections[floor].format;
                    let selectedItemId = fasadniySelections[floor].itemID;

                    if (selectedFormat === "1.4НФ" || selectedItemId === "6682") {
                        consumptionRate = 39;
                    }

                    let totalBricksRequired = Math.ceil(thisFloorArea * consumptionRate);
                    let poddonDivisible = Math.ceil(totalBricksRequired / fasadniySelections[floor].poddonQnty) * fasadniySelections[floor].poddonQnty;

                    let poddonPrice = 0;
                    if (dataFromJSON['PODDON_PRICE'][fasadniySelections[floor].makerID]) {
                        poddonPrice = dataFromJSON['PODDON_PRICE'][fasadniySelections[floor].makerID];
                    }

                    let totalThisFloorFasadniyCost = poddonDivisible * fasadniySelections[floor].itemPrice + poddonPrice * Math.ceil(totalBricksRequired / fasadniySelections[floor].poddonQnty) + poddonPrice * Math.ceil(totalBricksRequired / fasadniySelections[floor].poddonQnty);

                    if (isNaN(totalFasadniyCost)) {
                        totalFasadniyCost = 0;
                    }

                    totalFasadniyCost = totalFasadniyCost + totalThisFloorFasadniyCost;

                    totalCost = totalBasementCost + totalFasadniyCost + totalFrontonsCost + totalPeregorodkiCost + totalWallsCost + totalFrontonsWallsCost;

                    let floorNumber = floor.replace("floor-", "");

                    if (parseInt(floorNumber )> 1) {
                        let newResultsInstance = $(fasadniyResultLinesHtmlTemplate).clone();

                        $(newResultsInstance).attr('data-floorNumber', floorNumber);
                        $(newResultsInstance).find('.floorNumber').text(floorNumber + " этаж");

                        $(newResultsInstance).appendTo('.results__construction__type[data-resultsConstructionType="fasadniy"]');
                    }




                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--maker__js .result_value').text(fasadniySelections[floor].makerName);
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js .result_value').text(fasadniySelections[floor].format);
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--surface__js .result_value').text(fasadniySelections[floor].surface);
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--color__js .result_value').text(fasadniySelections[floor].color);
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_value').text(totalBricksRequired);
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_value').text(poddonDivisible);
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_value').text(fasadniySelections[floor].poddonQnty);
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonAmount__js .result_value').text(Math.ceil(totalBricksRequired / fasadniySelections[floor].poddonQnty));
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonPrice__js .result_value').text(poddonPrice);
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--totalCost__js .result_value').text(formatCost(totalThisFloorFasadniyCost));

                    $('.calc-col__results .final__price .final_price_value span').text(formatCost(totalCost));

                    $('.results__construction__type[data-resultsConstructionType="fasadniy"]').removeClass('hidden');
                    $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="'+floorNumber+'"]').removeClass('hidden');
                }
            }

            console.log("Total area: " + totalWallsArea + "m2");
            index++;
        }

        $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__line--setkaMeter__js').remove();
        $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__line--setkaWidth__js').remove();

        $('.calc_construction[data-constructiontype="FASADNIY"]').each(function (index, value) {
            let thisFloorNum = $(this).closest('.calc_housePart--floor').attr('data-floornum');

            //let thisSetkaRows = $(this).find('select.calc_setka_rows__js').val();
            let thisSetkaRows = $(this).find('select.calc_setka_rows__js').val();
            //console.dir(this.querySelector('.setka_rastvor .dropdown-input-row .input-col .calc_setka_rows__js select').value);
           // console.dir(this.querySelector('.setka_rastvor .dropdown-input-row .input-col'));


            let thisBrickHeight = 0.075;
            let thisBrickWidth = 0.12;

            if (fasadniySelections["floor-" + thisFloorNum].format === "1.4НФ") {
                thisBrickHeight = 0.098;
            }

            if (fasadniySelections["floor-" + thisFloorNum].format === "0.7НФ") {
                thisBrickWidth = 0.085;
            }

            let thisFloorBricksRowsHeight = Math.ceil((fasadniySelections["floor-" + thisFloorNum].height) / thisBrickHeight);

            let thisSetkaRunningMeter = Math.ceil((thisFloorBricksRowsHeight / thisSetkaRows) * fasadniySelections["floor-" + thisFloorNum].perimeter);

            let thisFloorDoorRunningMeters = 0;
            console.log("thisFloorNum " + thisFloorNum);
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNum+'"] .calc_construction[data-constructiontype="FASADNIY"] .floor__door-item').each(function (i, value) {
                let thisDoorQnty = parseInt($(this).find('.input-col--door__qty input').val());
                let thisDoorHeightCm = parseFloat($(this).find('.input-col--door__height input').val());
                let thisDoorWidthCm = parseFloat($(this).find('.input-col--door__width input').val());

                let thisDoorKladkaRowsNumber = Math.ceil((thisDoorHeightCm/100) / thisBrickHeight);

                thisFloorDoorRunningMeters += parseFloat((thisDoorQnty*(thisDoorKladkaRowsNumber / thisSetkaRows) * thisDoorWidthCm/100).toFixed(2));

                console.log("thisSetkaRows " + thisSetkaRows);

                console.log("thisFloorDoorRunningMeters " + thisFloorDoorRunningMeters);
            });

            let thisFloorWindowsRunningMeters = 0;
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNum+'"] .calc_construction[data-constructiontype="FASADNIY"] .floor__window-item').each(function (i, value) {
                let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
                let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
                let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

                let thisWindowKladkaRowsNumber = Math.ceil((thisWindowHeightCm/100) / thisBrickHeight);

                thisFloorWindowsRunningMeters += parseFloat((thisWindowQnty*(thisWindowKladkaRowsNumber / thisSetkaRows) * thisWindowWidthCm/100).toFixed(2));
            });

            thisSetkaRunningMeter = (thisSetkaRunningMeter - thisFloorDoorRunningMeters - thisFloorWindowsRunningMeters).toFixed(2);

            let setkaResultEl = `<div class="result__line result__line--setkaMeter__js">
                                        <div class="result_name">Сетка, пог. м</div>
                                        <div class="result_value">${thisSetkaRunningMeter}</div>
                                    </div>
                                    
                                    <div class="result__line result__line--setkaWidth__js">
                                        <div class="result_name">Ширина сетки, м.</div>
                                        <div class="result_value">${thisBrickWidth}</div>
                                    </div>`;

            $(setkaResultEl).insertBefore('.results__construction__type[data-resultsconstructiontype="fasadniy"] .result__lines[data-floornumber="'+thisFloorNum+'"] .result__line--totalCost__js')
            console.log("thisSetkaRunningMeter " + thisSetkaRunningMeter);
        });
    }

    function hideFasadniyCalculaion(floorNumber) {

        $('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines[data-floorNumber="'+floorNumber+'"]').addClass('hidden');

        if ($('.results__construction__type[data-resultsConstructionType="fasadniy"] .result__lines').length < 2) {
            $('.results__construction__type[data-resultsConstructionType="fasadniy"]').addClass('hidden');
        }


        if (totalCost > 0) {
            totalFasadniyCost = 0;
            totalCost = totalBasementCost + totalFasadniyCost + totalFrontonsCost + totalPeregorodkiCost + totalWallsCost;
        }

        let totalCostCopy = totalCost;

        totalCostCopy = totalCostCopy.toFixed(2);

        $('.calc-col__results .final__price .final_price_value span').text(numberWithSpaces(totalCostCopy));
    }

    function calculateDoorsArea() {

        doorsAndWindowsArea.doors = 0;

        $('.calc_housePart--floor .floor__door-item').each(function (index, value) {
            let thisDoorQnty = parseInt($(this).find('.input-col--door__qty input').val());
            let thisDoorHeightCm = parseFloat($(this).find('.input-col--door__height input').val());
            let thisDoorWidthCm = parseFloat($(this).find('.input-col--door__width input').val());

            doorsAndWindowsArea.doors += thisDoorQnty * (thisDoorHeightCm/100 * thisDoorWidthCm/100).toFixed(2);
        });
        
        console.log("Doors areas: " + doorsAndWindowsArea.doors + "м2");
    }

    function calculateWindowsArea() {
        doorsAndWindowsArea.windows = 0;

        $('.calc_housePart--floor .floor__window-item').each(function (index, value) {
            let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
            let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
            let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

            doorsAndWindowsArea.windows += thisWindowQnty * (thisWindowHeightCm/100 * thisWindowWidthCm/100).toFixed(2);
        });

        console.log("Windows areas: " + doorsAndWindowsArea.windows + "м2");
    }





        /* FASAD END*/
        ///////////////////////////////////////

    /* PEREGORODKI START */

    $('body').on('click', '.calc_housePart--floor .construction_type_item__js[data-construction_type="PEREGORODKI"]', function () {
        $(this).closest('.calc_construction').attr('data-constructionType', "PEREGORODKI");

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block ').not('.calc_option_sub_block__materialType').addClass('hidden');
        $(this).closest('.calc_construction').find('.calc_makers_row__makers .maker').removeClass('selected');

        $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn_border').removeClass('selected');
        $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn').removeClass('selected');

        $(this).closest('.calc_construction').find('.calc_block--pereghorodkiLength').removeClass('hidden');


        $(this).closest('.constructions_options_row').find('.construction_type_item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_block--makers').removeClass('hidden');
        $(this).closest('.calc_construction').find('.calc_option_sub_block__materialType').removeClass('hidden');

        /*LETS SHOW "ADD CONSTUCTION" BUTTON*/
        if ($(this).closest('.constructions_options_row').find('.construction_type_item__js').length > 1) {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"]').find('.add_new_construction').removeClass('hidden');
        }

        console.log(dataFromJSON.BRICKS['PEREGORODKI']);
    });

    $('body').on('change', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] input[data-input="floorPeregorodkiWidth"]', function () {
        calculatePeregorodkiMaterials();
    });


    //peregorodki kirpich start //

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc-materialType__js[data-materialType="kirpich"]', function () {
        console.log(dataFromJSON);
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

        $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['materialType'] = "kirpich";

        let makers = [];
        let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

        $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
            if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                    console.log(value);
                } else {
                    makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                    tempValueMakersID.push(value.MAKER_VALUE_ID);
                }
            }
        });

        let makersElements = "";
        $(makers).each(function (index, value) {
            makersElements += `<div class="maker calc-maker__js" data-type="STROITELNIY_PEREGORODKI" data-maker_id="${value.value_id}">
                                        <div class="img">
                                            <img src="${value.maker_image}" alt="">
                                        </div>
                                        <span>${value.value}</span>
                                     </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_makers_row__makers').html(makersElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_block--makers').removeClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_maker').removeClass('hidden');

    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc-maker__js[data-type="STROITELNIY_PEREGORODKI"]', function () {
        let thisMakerID = this.getAttribute('data-maker_id');
        let thisMakerName = $(this).find('span').text();


        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['makerID'] = thisMakerID;
        peregorodkiSelections['floor-' + thisFloorNumber]['makerName'] = thisMakerName;

        let brickFormatArr = [];
        $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
            if (!brickFormatArr.includes(value.SIZE) && value.MAKER_VALUE_ID === thisMakerID) {
                brickFormatArr.push(value.SIZE);
            }
        });

        let formatElements = "";
        $(brickFormatArr).each(function (index, value) {
            formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="STROITELNIY_PEREGORODKI" data-material__size="${value}">${value}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_row--format').html(formatElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_formats').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_strength_mark').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_formats').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc_brick_size__js[data-type="STROITELNIY_PEREGORODKI"]', function () {
        let thisFormat = this.getAttribute('data-material__size');
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_brick_size__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['format'] = thisFormat;

        let strenghtMarksArr = [];

        $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
            if (!strenghtMarksArr.includes(value.STRENGTH_MARK) &&
                value.MAKER_VALUE_ID === peregorodkiSelections['floor-' + thisFloorNumber].makerID &&
                value.SIZE === thisFormat)   {
                strenghtMarksArr.push(value.STRENGTH_MARK);
            }
        });

        let strengthMarkElements = "";
        $(strenghtMarksArr).each(function (index, value) {
            strengthMarkElements += `<div class="calc_option_btn calc-strength_mark__js" data-type="STROITELNIY_PEREGORODKI" data-material__strength-mark="${value}">${value}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_row--strength_mark').html(strengthMarkElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_strength_mark').removeClass('hidden');

    });

    $('body').on('click', ".calc_housePart--floor .calc_construction[data-constructionType='PEREGORODKI'] .calc-strength_mark__js[data-type='STROITELNIY_PEREGORODKI']", function () {
        let thisStrengthMark = this.getAttribute('data-material__strength-mark');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-strength_mark__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['strengthMark'] = thisStrengthMark;

        let itemsArr = [];
        let itemsIdArr = [];
        $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
            if (!itemsIdArr.includes(value.ID) &&
                value.MAKER_VALUE_ID === peregorodkiSelections['floor-' + thisFloorNumber].makerID &&
                value.SIZE === peregorodkiSelections['floor-' + thisFloorNumber].format &&
                value.STRENGTH_MARK === thisStrengthMark) {

                itemsIdArr.push(value.ID);
                itemsArr.push({strengthMark: value.STRENGTH_MARK, product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
            }
        });

        let itemsElements = "";
        $(itemsArr).each(function (index, value) {
            itemsElements += `<div class="calc_option_btn_border calc-item__js" data-type="STROITELNIY_PEREGORODKI" data-itemPrice="${value.itemPrice}" data-poddonQnty="${value.poddonQnty}" data-material__item-id="${value.product_id}">
                                        <img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">
                                        ${value.product_name}
                                    </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_row--items').html(itemsElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_items').removeClass('hidden');

    });

    $('body').on('click', ".calc_housePart--floor .calc_construction[data-constructionType='PEREGORODKI'] .calc-item__js[data-type='STROITELNIY_PEREGORODKI']", function () {
        let thisItemId = this.getAttribute('data-material__item-id');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['id'] = thisItemId;
        peregorodkiSelections['floor-' + thisFloorNumber]['poddonQnty'] = this.getAttribute('data-poddonQnty');
        peregorodkiSelections['floor-' + thisFloorNumber]['itemPrice'] = this.getAttribute('data-itemPrice');

        console.log(peregorodkiSelections);

        calculatePeregorodkiMaterials();
    });
    //peregorodki kirpich end //



    //peregorodki keramblock start //
    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc-materialType__js[data-materialType="keramblock"]', function () {
        //console.log(dataFromJSON);
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

        $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['materialType'] = "keramblock";

        let makers = [];
        let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

        $(dataFromJSON.BRICKS['KERAMBLOCKS_PEREGORODKI']).each(function (index, value) {
            if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                    console.log(value);
                } else {
                    makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                    tempValueMakersID.push(value.MAKER_VALUE_ID);
                }
            }
        });

        let makersElements = "";
        $(makers).each(function (index, value) {
            makersElements += `<div class="maker calc-maker__js" data-type="KERAMBLOCKS" data-maker_id="${value.value_id}">
                                        <div class="img">
                                            <img src="${value.maker_image}" alt="">
                                        </div>
                                        <span>${value.value}</span>
                                     </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_makers_row__makers').html(makersElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_block--makers').removeClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_maker').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc-maker__js[data-type="KERAMBLOCKS"]', function () {
        let thisMakerID = this.getAttribute('data-maker_id');
        let thisMakerName = $(this).find('span').text();

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['makerID'] = thisMakerID;
        peregorodkiSelections['floor-' + thisFloorNumber]['makerName'] = thisMakerName;

        let brickFormatArr = [];
        $(dataFromJSON.BRICKS['KERAMBLOCKS_PEREGORODKI']).each(function (index, value) {
            if (!brickFormatArr.includes(value.SIZE) && value.MAKER_VALUE_ID === thisMakerID) {
                brickFormatArr.push(value.SIZE);
            }
        });

        let formatElements = "";
        $(brickFormatArr).each(function (index, value) {
            let sizeName = dataFromJSON.KERAMBLOCKS_FORMAT_NAMES[thisMakerID][value];
            formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="KERAMBLOCKS" data-material__size="${value}">${value} (${sizeName})</div>`;
        });



        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_row--format').html(formatElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_formats').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_strength_mark').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_formats').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc_brick_size__js[data-type="KERAMBLOCKS"]', function () {
        let thisFormat = this.getAttribute('data-material__size');
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_brick_size__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['format'] = thisFormat;

        let itemsArr = [];
        let itemsIdArr = [];
        $(dataFromJSON.BRICKS['KERAMBLOCKS_PEREGORODKI']).each(function (index, value) {
            if (!itemsIdArr.includes(value.ID) &&
                value.MAKER_VALUE_ID === peregorodkiSelections['floor-' + thisFloorNumber].makerID &&
                value.SIZE === thisFormat) {

                itemsIdArr.push(value.ID);
                itemsArr.push({product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
            }
        });

        let itemsElements = "";
        $(itemsArr).each(function (index, value) {
            itemsElements += `<div class="calc_option_btn_border calc-item__js" data-poddonQnty="${value.poddonQnty}" data-itemPrice="${value.itemPrice}" data-type="KERAMBLOCKS" data-material__item-id="${value.product_id}">
                                        <img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">
                                        ${value.product_name}
                                    </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_row--items').html(itemsElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_items').removeClass('hidden');
    });

    $('body').on('click', ".calc_housePart--floor .calc_construction[data-constructionType='PEREGORODKI'] .calc-item__js[data-type='KERAMBLOCKS']", function () {
        let thisItemId = this.getAttribute('data-material__item-id');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['id'] = thisItemId;
        peregorodkiSelections['floor-' + thisFloorNumber]['poddonQnty'] = this.getAttribute('data-poddonQnty');
        peregorodkiSelections['floor-' + thisFloorNumber]['itemPrice'] = this.getAttribute('data-itemPrice');

        console.log(peregorodkiSelections);
        calculatePeregorodkiMaterials();
    });
    //peregorodki keramblock end //


    //peregorodki gazoblock start//
    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc-materialType__js[data-materialType="gazoblock"]', function () {
        //console.log(dataFromJSON);
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

        $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['materialType'] = "gazoblock";

        let makers = [];
        let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

        $(dataFromJSON.BRICKS['GAZOBLOCKS_PEREGORODKI']).each(function (index, value) {
            if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                    console.log(value);
                } else {
                    makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                    tempValueMakersID.push(value.MAKER_VALUE_ID);
                }
            }
        });

        let makersElements = "";
        $(makers).each(function (index, value) {
            makersElements += `<div class="maker calc-maker__js" data-type="GAZOBLOCKS" data-maker_id="${value.value_id}">
                                        <div class="img">
                                            <img src="${value.maker_image}" alt="">
                                        </div>
                                        <span>${value.value}</span>
                                     </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_makers_row__makers').html(makersElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_block--makers').removeClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_maker').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc-maker__js[data-type="GAZOBLOCKS"]', function () {
        let thisMakerID = this.getAttribute('data-maker_id');
        let thisMakerName = $(this).find('span').text();

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['makerID'] = thisMakerID;
        peregorodkiSelections['floor-' + thisFloorNumber]['makerName'] = thisMakerName;

        let densityMarkArr = [];
        $(dataFromJSON.BRICKS['GAZOBLOCKS_PEREGORODKI']).each(function (index, value) {
            if (!densityMarkArr.includes(value.DENSITY_MARK) && value.MAKER_VALUE_ID === thisMakerID) {
                densityMarkArr.push(value.DENSITY_MARK);
            }
        });

        let densityElements = "";
        $(densityMarkArr).each(function (index, value) {
            densityElements += `<div class="calc_option_btn calc_block_density__js" data-type="GAZOBLOCKS" data-densityMark="${value}">${value}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_row--density_mark').html(densityElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_density_mark').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_width_gb').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_density_mark').removeClass('hidden');

        hidePeregorodkiCalculaion(thisFloorNumber);
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc_block_density__js[data-type="GAZOBLOCKS"]', function () {
        let thisDensityMark = this.getAttribute('data-densityMark');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_block_density__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['densityMark'] = thisDensityMark;

        let widthArr = [];
        $(dataFromJSON.BRICKS['GAZOBLOCKS_PEREGORODKI']).each(function (index, value) {
            if (!widthArr.includes(value.WIDTH) && value.MAKER_VALUE_ID === peregorodkiSelections['floor-' + thisFloorNumber].makerID &&
                value.DENSITY_MARK === thisDensityMark ) {
                widthArr.push(value.WIDTH);
            }
        });

        let widthElements = "";
        $(widthArr).each(function (index, value) {
            widthElements += `<div class="calc_option_btn calc_block_width__js" data-type="GAZOBLOCKS" data-width="${value}">${value}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_row--width_gb').html(widthElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_width_gb').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_width_gb').removeClass('hidden');

        hidePeregorodkiCalculaion(thisFloorNumber);
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="PEREGORODKI"] .calc_block_width__js[data-type="GAZOBLOCKS"]', function () {
        let thisWidth = this.getAttribute('data-width');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_block_width__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['width'] = thisWidth;

        let itemsArr = [];
        let itemsIdArr = [];
        $(dataFromJSON.BRICKS['GAZOBLOCKS_PEREGORODKI']).each(function (index, value) {
            if (!itemsIdArr.includes(value.ID) &&
                value.MAKER_VALUE_ID === peregorodkiSelections['floor-' + thisFloorNumber].makerID &&
                value.DENSITY_MARK === peregorodkiSelections['floor-' + thisFloorNumber].densityMark &&
                value.WIDTH === thisWidth) {

                itemsIdArr.push(value.ID);
                itemsArr.push({product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, product_height: value.HEIGHT, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
            }
        });

        let itemsElements = "";
        $(itemsArr).each(function (index, value) {
            itemsElements += `<div class="calc_option_btn_border calc-item__js" data-type="GAZOBLOCKS" data-poddonQnty="${value.poddonQnty}" data-itemHeight="${value.product_height}" data-itemPrice="${value.itemPrice}" data-material__item-id="${value.product_id}">
                                        <img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">
                                        ${value.product_name}
                                    </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_row--items').html(itemsElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="PEREGORODKI"] .calc_option_sub_block_items').removeClass('hidden');

        hidePeregorodkiCalculaion(thisFloorNumber);
    });

    $('body').on('click', ".calc_housePart--floor .calc_construction[data-constructionType='PEREGORODKI'] .calc-item__js[data-type='GAZOBLOCKS']", function () {
        let thisItemId = this.getAttribute('data-material__item-id');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        peregorodkiSelections['floor-' + thisFloorNumber]['id'] = thisItemId;
        peregorodkiSelections['floor-' + thisFloorNumber]['itemHeight'] = this.getAttribute('data-itemHeight');;
        peregorodkiSelections['floor-' + thisFloorNumber]['poddonQnty'] = this.getAttribute('data-poddonQnty');
        peregorodkiSelections['floor-' + thisFloorNumber]['itemPrice'] = this.getAttribute('data-itemPrice');

        calculatePeregorodkiMaterials();

        console.log(peregorodkiSelections);
    });
    //peregorodki gazoblock end//

    $('body').on('change', '.calc_construction[data-constructiontype="PEREGORODKI"] select.calc_setka_rows__js', function () {
        calculatePeregorodkiMaterials();
    });

    function calculatePeregorodkiMaterials() {
        let index = 1;
        let totalWallsArea = 0;
        totalPeregorodkiCost = 0;

        let peregorodkiResultLinesHtmlTemplate = $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="1"]').clone();
        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines').not('.result__lines[data-floorNumber="1"]').remove();

        for (let floor in peregorodkiSelections) {
            if (peregorodkiSelections.hasOwnProperty(floor)) {

                let floorNumber = floor.replace("floor-", "");

                if (typeof peregorodkiSelections[floor].id !== "undefined") {
                    let thisFloorArea = 0;
                    let thisFloorDoorArea = 0;
                    let thisFloorWindowArea = 0;

                    $('.calc_housePart--floor[data-floorNumberText="' + floor + '"] .calc_construction[data-constructiontype="PEREGORODKI"] .floor__door-item').each(function (i, value) {
                        let thisDoorQnty = parseInt($(this).find('.input-col--door__qty input').val());
                        let thisDoorHeightCm = parseFloat($(this).find('.input-col--door__height input').val());
                        let thisDoorWidthCm = parseFloat($(this).find('.input-col--door__width input').val());

                        thisFloorDoorArea += thisDoorQnty * (thisDoorHeightCm / 100 * thisDoorWidthCm / 100).toFixed(2);
                    });

                    $('.calc_housePart--floor[data-floorNumberText="' + floor + '"] .calc_construction[data-constructiontype="PEREGORODKI"] .floor__window-item').each(function (i, value) {
                        let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
                        let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
                        let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

                        thisFloorWindowArea += thisWindowQnty * (thisWindowHeightCm / 100 * thisWindowWidthCm / 100).toFixed(2);
                    });


                    let thisFloorPeregorodkiLength = $('.calc_housePart--floor[data-floorNumberText="' + floor + '"] .calc_construction[data-constructiontype="PEREGORODKI"] input[data-input="floorPeregorodkiWidth"]').val();

                    thisFloorArea += parseFloat(thisFloorPeregorodkiLength) * parseFloat(peregorodkiSelections[floor].height) - thisFloorDoorArea - thisFloorWindowArea;

                    let consumptionRate;
                    let oneBlockVolume = 0;

                    if (peregorodkiSelections[floor].materialType === "kirpich") {
                        consumptionRate = 51; //if format == 1nf
                        let selectedFormat = peregorodkiSelections[floor].format;

                        if (selectedFormat === "1.4НФ") {
                            consumptionRate = 39;
                        } else if (selectedFormat === "2.1НФ") {
                            consumptionRate = 26;
                        }

                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--strengthMark__js').css("display", "flex");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js').css("display", "flex");

                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_name span').text("кирпича, шт");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_name span').text("шт");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_name span').text("шт");



                    } else if(peregorodkiSelections[floor].materialType === "keramblock") {
                        consumptionRate = 8.6;
                        let selectedFormat = peregorodkiSelections[floor].format;
                        if (selectedFormat === "6.9НФ") {
                            consumptionRate = 8.5;
                        } else if (selectedFormat === "9НФ" && peregorodkiSelections[floor].makerID === "800") {
                            consumptionRate = 11.4;
                        } else if (selectedFormat === "9НФ" && peregorodkiSelections[floor].makerID === "630") {
                            consumptionRate = 10.8;
                        }

                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--strengthMark__js').css("display", "none");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js').css("display", "flex");

                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_name span').text("блока, шт");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_name span').text("шт");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_name span').text("шт");


                    } else if(peregorodkiSelections[floor].materialType === "gazoblock") {
                        if (peregorodkiSelections[floor].makerID == "1189" || peregorodkiSelections[floor].makerID == "2302") {
                            if (peregorodkiSelections[floor].width == "100" && peregorodkiSelections[floor].itemHeight == "200") {
                                consumptionRate = 80;
                                oneBlockVolume = 0.0125;
                            }
                            else if (peregorodkiSelections[floor].width == "100" && peregorodkiSelections[floor].itemHeight == "250") {
                                consumptionRate = 64;
                                oneBlockVolume = 0.015625;
                            }
                            else if (peregorodkiSelections[floor].width == "150" && peregorodkiSelections[floor].itemHeight == "200") {
                                consumptionRate = 53.33;
                                oneBlockVolume = 0.01875;
                            }
                            else if (peregorodkiSelections[floor].width == "150" && peregorodkiSelections[floor].itemHeight == "250") {
                                consumptionRate = 42.67;
                                oneBlockVolume = 0.0234375;
                            }
                        } else{
                            if (peregorodkiSelections[floor].width == "100" && peregorodkiSelections[floor].itemHeight == "400") {
                                consumptionRate = 41.67;
                                oneBlockVolume = 0.024;
                            }
                        }

                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--strengthMark__js').css("display", "none");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js').css("display", "none");

                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_name span').text("газоблока, м3");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_name span').text("м3");
                        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_name span').text("м3");


                    }

                    let totalBricksRequired = Math.ceil(thisFloorArea * consumptionRate);

                    if (peregorodkiSelections[floor].materialType === "gazoblock") {
                        totalBricksRequired = Math.ceil(thisFloorArea * (peregorodkiSelections[floor].width/1000) * consumptionRate);
                    }

                    let poddonDivisible = Math.ceil(totalBricksRequired / peregorodkiSelections[floor].poddonQnty) * peregorodkiSelections[floor].poddonQnty;

                    console.log("poddonQnty " + peregorodkiSelections[floor].poddonQnty);
                    console.log("poddonDivisible " + poddonDivisible);


                    let poddonPrice = 0;
                    if (dataFromJSON['PODDON_PRICE'][peregorodkiSelections[floor].makerID]) {
                        poddonPrice = dataFromJSON['PODDON_PRICE'][peregorodkiSelections[floor].makerID];
                    }

                    console.log('itemPrice ' + peregorodkiSelections[floor].itemPrice);


                    let totalThisFloorPeregorodkiCost = poddonDivisible * peregorodkiSelections[floor].itemPrice + poddonPrice * Math.ceil(totalBricksRequired / peregorodkiSelections[floor].poddonQnty) + poddonPrice * Math.ceil(totalBricksRequired / peregorodkiSelections[floor].poddonQnty);

                    if (peregorodkiSelections[floor].materialType === "gazoblock") {
                        totalThisFloorPeregorodkiCost = poddonDivisible * oneBlockVolume * peregorodkiSelections[floor].itemPrice + poddonPrice * Math.ceil(totalBricksRequired / peregorodkiSelections[floor].poddonQnty) + poddonPrice * Math.ceil(totalBricksRequired / peregorodkiSelections[floor].poddonQnty);

                    }

                    if (isNaN(totalPeregorodkiCost)) {
                        totalPeregorodkiCost = 0;
                    }

                    totalPeregorodkiCost = totalPeregorodkiCost + totalThisFloorPeregorodkiCost;

                    totalCost = totalBasementCost + totalFasadniyCost + totalFrontonsCost + totalPeregorodkiCost + totalWallsCost + totalFrontonsWallsCost;



                    if (parseInt(floorNumber )> 1) {
                        let newResultsInstance = $(peregorodkiResultLinesHtmlTemplate).clone();

                        $(newResultsInstance).attr('data-floorNumber', floorNumber);
                        $(newResultsInstance).find('.floorNumber').text(floorNumber + " этаж");

                        $(newResultsInstance).appendTo('.results__construction__type[data-resultsConstructionType="peregorodki"]');
                    }

                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--maker__js .result_value').text(peregorodkiSelections[floor].makerName);
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js .result_value').text(peregorodkiSelections[floor].format);
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--strengthMark__js .result_value').text(peregorodkiSelections[floor].strengthMark);
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_value').text(totalBricksRequired);
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_value').text(poddonDivisible);
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_value').text(peregorodkiSelections[floor].poddonQnty);
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonAmount__js .result_value').text(Math.ceil(totalBricksRequired / peregorodkiSelections[floor].poddonQnty));
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonPrice__js .result_value').text(poddonPrice);
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--totalCost__js .result_value').text(formatCost(totalThisFloorPeregorodkiCost));

                    $('.calc-col__results .final__price .final_price_value span').text(formatCost(totalCost));

                    $('.results__construction__type[data-resultsConstructionType="peregorodki"]').removeClass('hidden');
                    $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="'+floorNumber+'"]').removeClass('hidden');
                }
            }

            index++;
        }

        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__line--setkaMeter__js').remove();
        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__line--setkaWidth__js').remove();

        $('.calc_construction[data-constructiontype="PEREGORODKI"]').each(function (index, value) {
            let thisFloorNum = $(this).closest('.calc_housePart--floor').attr('data-floornum');

            let thisSetkaRows = $(this).find('select.calc_setka_rows__js').val();

            let thisFloorPeregorodkiLength = $('.calc_housePart--floor[data-floorNum="' + thisFloorNum + '"] .calc_construction[data-constructiontype="PEREGORODKI"] input[data-input="floorPeregorodkiWidth"]').val();

            let thisBrickHeight;
            let thisBrickWidth;

            if (peregorodkiSelections["floor-" + thisFloorNum].materialType === "kirpich") {
                thisBrickHeight = 0.075;
                thisBrickWidth = 0.12;

                if (peregorodkiSelections["floor-" + thisFloorNum].format === "1.4НФ") {
                    thisBrickHeight = 0.098;
                } else if (peregorodkiSelections["floor-" + thisFloorNum].format === "2.1НФ") {
                    thisBrickHeight = 0.15;
                }
            } else if (peregorodkiSelections["floor-" + thisFloorNum].materialType === "keramblock") {
                thisBrickHeight = 0.219;
                
                let thisBlockFormat = peregorodkiSelections["floor-" + thisFloorNum].format;
                let thisBlockMakerId = peregorodkiSelections["floor-" + thisFloorNum].makerID;

                thisBrickWidth = parseInt(dataFromJSON.KERAMBLOCKS_FORMAT_NAMES[thisBlockMakerId][thisBlockFormat]) / 100;
            } else if (peregorodkiSelections["floor-" + thisFloorNum].materialType === "gazoblock") {
                thisBrickHeight = peregorodkiSelections["floor-" + thisFloorNum].itemHeight / 1000;
                thisBrickWidth = peregorodkiSelections["floor-" + thisFloorNum].width / 1000;
            }

            let thisFloorBricksRowsHeight = Math.ceil((fasadniySelections["floor-" + thisFloorNum].height) / thisBrickHeight);

            let thisSetkaRunningMeter = Math.ceil((thisFloorBricksRowsHeight / thisSetkaRows) * parseInt(thisFloorPeregorodkiLength));

            let thisFloorDoorRunningMeters = 0;
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNum+'"] .calc_construction[data-constructiontype="PEREGORODKI"] .floor__door-item').each(function (i, value) {
                let thisDoorQnty = parseInt($(this).find('.input-col--door__qty input').val());
                let thisDoorHeightCm = parseFloat($(this).find('.input-col--door__height input').val());
                let thisDoorWidthCm = parseFloat($(this).find('.input-col--door__width input').val());

                let thisDoorKladkaRowsNumber = Math.ceil((thisDoorHeightCm/100) / thisBrickHeight);

                thisFloorDoorRunningMeters += parseFloat((thisDoorQnty*(thisDoorKladkaRowsNumber / thisSetkaRows) * thisDoorWidthCm/100).toFixed(2));

                console.log("thisFloorDoorRunningMeters " + thisFloorDoorRunningMeters);
            });

            let thisFloorWindowsRunningMeters = 0;
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNum+'"] .calc_construction[data-constructiontype="PEREGORODKI"] .floor__window-item').each(function (i, value) {
                let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
                let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
                let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

                let thisWindowKladkaRowsNumber = Math.ceil((thisWindowHeightCm/100) / thisBrickHeight);

                thisFloorWindowsRunningMeters += parseFloat((thisWindowQnty*(thisWindowKladkaRowsNumber / thisSetkaRows) * thisWindowWidthCm/100).toFixed(2));
            });

            thisSetkaRunningMeter = (thisSetkaRunningMeter - thisFloorDoorRunningMeters - thisFloorWindowsRunningMeters).toFixed(2);

            let setkaResultEl = `<div class="result__line result__line--setkaMeter__js">
                                        <div class="result_name">Сетка, пог. м</div>
                                        <div class="result_value">${thisSetkaRunningMeter}</div>
                                    </div>
                                    
                                    <div class="result__line result__line--setkaWidth__js">
                                        <div class="result_name">Ширина сетки, м.</div>
                                        <div class="result_value">${thisBrickWidth}</div>
                                    </div>`;

            $(setkaResultEl).insertBefore('.results__construction__type[data-resultsconstructiontype="peregorodki"] .result__lines[data-floornumber="'+thisFloorNum+'"] .result__line--totalCost__js')
            console.log("thisSetkaRunningMeter " + thisSetkaRunningMeter);
        });

    }
    function hidePeregorodkiCalculaion(floorNumber) {

        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines[data-floorNumber="'+floorNumber+'"]').addClass('hidden');

        if ($('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines').length < 2) {
            $('.results__construction__type[data-resultsConstructionType="peregorodki"]').addClass('hidden');
        }


        if (totalCost > 0) {
            totalPeregorodkiCost = 0;
            totalCost = totalBasementCost + totalFasadniyCost + totalFrontonsCost + totalPeregorodkiCost + totalWallsCost;
        }

        let totalCostCopy = totalCost;

        totalCostCopy = totalCostCopy.toFixed(2);

        $('.calc-col__results .final__price .final_price_value span').text(numberWithSpaces(totalCostCopy));
    }

    /* PEREGORODKI END */


    /* WALLS START */
    $('body').on('click', '.calc_housePart--floor .construction_type_item__js[data-construction_type="WALLS"]', function () {
        $(this).closest('.calc_construction').attr('data-constructionType', "WALLS");

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block ').not('.calc_option_sub_block__materialType').addClass('hidden');
        $(this).closest('.calc_construction').find('.calc_makers_row__makers .maker').removeClass('selected');

        $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn_border').removeClass('selected');
        $(this).closest('.calc_construction').find('.calc_option_sub_block .calc_option_btn').removeClass('selected');

        $(this).closest('.constructions_options_row').find('.construction_type_item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        if ($('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"]').length > 0) {
            let fasadDoors = $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_block--doors').clone();
            let fasadWindows = $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="FASADNIY"] .calc_block--windows').clone();

            $(this).closest('.calc_construction').find('.calc_block--doors').replaceWith(fasadDoors);
            $(this).closest('.calc_construction').find('.calc_block--windows').replaceWith(fasadWindows);
        }

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_block--makers').removeClass('hidden');
        //$('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_option_sub_block__materialType').removeClass('hidden');
        $(this).closest('.calc_construction').find('.calc_option_sub_block__materialType').removeClass('hidden');

        /*LETS SHOW "ADD CONSTUCTION" BUTTON*/
        if ($(this).closest('.constructions_options_row').find('.construction_type_item__js').length > 1) {
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"]').find('.add_new_construction').removeClass('hidden');
        }

        $(this).closest('.calc_construction').find('.calc_block--pereghorodkiLength').addClass('hidden');
    });

    // ryadovoy start //
    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc-materialType__js[data-materialType="kirpich"]', function () {
        console.log(dataFromJSON);
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

        $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['materialType'] = "kirpich";

        let makers = [];
        let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

        $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
            if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                    console.log(value);
                } else {
                    makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                    tempValueMakersID.push(value.MAKER_VALUE_ID);
                }
            }
        });

        let makersElements = "";
        $(makers).each(function (index, value) {
            makersElements += `<div class="maker calc-maker__js" data-type="RYADOVOY" data-maker_id="${value.value_id}">
                                        <div class="img">
                                            <img src="${value.maker_image}" alt="">
                                        </div>
                                        <span>${value.value}</span>
                                     </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_makers_row__makers').html(makersElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_block--makers').removeClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_maker').removeClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc-block.setka_rastvor').addClass('hidden');

    });


    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc-maker__js[data-type="RYADOVOY"]', function () {
        let thisMakerID = this.getAttribute('data-maker_id');
        let thisMakerName = $(this).find('span').text();

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['makerID'] = thisMakerID;
        wallsSelections['floor-' + thisFloorNumber]['makerName'] = thisMakerName;

        let brickFormatArr = [];
        $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
            if (!brickFormatArr.includes(value.SIZE) && value.MAKER_VALUE_ID === thisMakerID) {
                brickFormatArr.push(value.SIZE);
            }
        });

        let formatElements = "";
        $(brickFormatArr).each(function (index, value) {
            formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="RYADOVOY" data-material__size="${value}">${value}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_row--format').html(formatElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_formats').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_strength_mark').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_formats').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc_brick_size__js[data-type="RYADOVOY"]', function () {
        let thisFormat = this.getAttribute('data-material__size');
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_brick_size__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['format'] = thisFormat;

        let strenghtMarksArr = [];

        $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
            if (!strenghtMarksArr.includes(value.STRENGTH_MARK) &&
                value.MAKER_VALUE_ID === wallsSelections['floor-' + thisFloorNumber].makerID &&
                value.SIZE === thisFormat)   {
                strenghtMarksArr.push(value.STRENGTH_MARK);
            }
        });

        let strengthMarkElements = "";
        $(strenghtMarksArr).each(function (index, value) {
            strengthMarkElements += `<div class="calc_option_btn calc-strength_mark__js" data-type="RYADOVOY" data-material__strength-mark="${value}">${value}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_row--strength_mark').html(strengthMarkElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_strength_mark').removeClass('hidden');

    });

    $('body').on('click', ".calc_housePart--floor .calc_construction[data-constructionType='WALLS'] .calc-strength_mark__js[data-type='RYADOVOY']", function () {
            let thisStrengthMark = this.getAttribute('data-material__strength-mark');

            let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

            $(this).closest('.calc_construction').find('.calc-strength_mark__js.selected').not(this).removeClass('selected');
            this.classList.add('selected');

            wallsSelections['floor-' + thisFloorNumber]['strengthMark'] = thisStrengthMark;

            let itemsArr = [];
            let itemsIdArr = [];
            $(dataFromJSON.BRICKS['STROITELNIY']).each(function (index, value) {
                if (!itemsIdArr.includes(value.ID) &&
                    value.MAKER_VALUE_ID === wallsSelections['floor-' + thisFloorNumber].makerID &&
                    value.SIZE === wallsSelections['floor-' + thisFloorNumber].format &&
                    value.STRENGTH_MARK === thisStrengthMark) {

                    itemsIdArr.push(value.ID);
                    itemsArr.push({strengthMark: value.STRENGTH_MARK, product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
                }
            });

            let itemsElements = "";
            $(itemsArr).each(function (index, value) {
                itemsElements += `<div class="calc_option_btn_border calc-item__js" data-type="RYADOVOY" data-poddonQnty="${value.poddonQnty}" data-itemPrice="${value.itemPrice}" data-material__item-id="${value.product_id}">
                                        <img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">
                                        ${value.product_name}
                                    </div>`;
            });

            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_row--items').html(itemsElements);

            $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').removeClass('hidden');

        });

    $('body').on('click', ".calc_housePart--floor .calc_construction[data-constructionType='WALLS'] .calc-item__js[data-type='RYADOVOY']", function () {
        let thisItemId = this.getAttribute('data-material__item-id');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['id'] = thisItemId;
        wallsSelections['floor-' + thisFloorNumber]['poddonQnty'] = this.getAttribute('data-poddonQnty');
        wallsSelections['floor-' + thisFloorNumber]['itemPrice'] = this.getAttribute('data-itemPrice');

        console.log(wallsSelections);

        $(this).closest('.calc_construction').find('.calc_option_sub_block_kladka_parameters').removeClass('hidden');

        calculateWallsMaterials();
    });

    $('body').on('change', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] select.wall_width_js', function () {
        calculateWallsMaterials();
    });

    $('body').on('change', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] select.setka__rows_js', function () {
        calculateWallsMaterials();
    });
    // ryadovoy end//


    //kermablock start//
    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc-materialType__js[data-materialType="keramblock"]', function () {
        //console.log(dataFromJSON);
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

        $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['materialType'] = "keramblock";

        let makers = [];
        let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

        $(dataFromJSON.BRICKS['KERAMBLOCKS']).each(function (index, value) {
            if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                if (typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                    console.log(value);
                } else {
                    makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                    tempValueMakersID.push(value.MAKER_VALUE_ID);
                }
            }
        });

        let makersElements = "";
        $(makers).each(function (index, value) {
            makersElements += `<div class="maker calc-maker__js" data-type="KERAMBLOCKS" data-maker_id="${value.value_id}">
                                        <div class="img">
                                            <img src="${value.maker_image}" alt="">
                                        </div>
                                        <span>${value.value}</span>
                                     </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_makers_row__makers').html(makersElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_block--makers').removeClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_maker').removeClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc-block.setka_rastvor').removeClass('hidden');

    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc-maker__js[data-type="KERAMBLOCKS"]', function () {
        let thisMakerID = this.getAttribute('data-maker_id');
        let thisMakerName = $(this).find('span').text();

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['makerID'] = thisMakerID;
        wallsSelections['floor-' + thisFloorNumber]['makerName'] = thisMakerName;

        let brickFormatArr = [];
        $(dataFromJSON.BRICKS['KERAMBLOCKS']).each(function (index, value) {
            if (!brickFormatArr.includes(value.SIZE) && value.MAKER_VALUE_ID === thisMakerID) {
                brickFormatArr.push(value.SIZE);
            }
        });

        let formatElements = "";
        let halfBlockArray = ["5.5НФ", "7НФ", "7.4НФ", "5.4НФ", "6.2НФ", "7.2НФ", "5.4НФ", "6.2НФ", "7.2НФ"];
        $(brickFormatArr).each(function (index, value) {
            let sizeName = dataFromJSON.KERAMBLOCKS_FORMAT_NAMES[thisMakerID][value];
            let halfBlock = "";
            if (halfBlockArray.includes(value)) {
                halfBlock = " - (1/2)";
            }
            formatElements += `<div class="calc_option_btn calc_brick_size__js" data-type="KERAMBLOCKS" data-material__size="${value}">${value} (${sizeName}) ${halfBlock}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_row--format').html(formatElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_formats').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_strength_mark').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_formats').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc_brick_size__js[data-type="KERAMBLOCKS"]', function () {
        let thisFormat = this.getAttribute('data-material__size');
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_brick_size__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['format'] = thisFormat;

        let itemsArr = [];
        let itemsIdArr = [];
        $(dataFromJSON.BRICKS['KERAMBLOCKS']).each(function (index, value) {
            if (!itemsIdArr.includes(value.ID) &&
                value.MAKER_VALUE_ID === wallsSelections['floor-' + thisFloorNumber].makerID &&
                value.SIZE === thisFormat) {

                itemsIdArr.push(value.ID);
                itemsArr.push({product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
            }
        });

        let itemsElements = "";
        $(itemsArr).each(function (index, value) {
            let img = "";
            if (typeof dataFromJSON.PRODUCTS_IMAGES[value.product_image_id] !== "undefined") {
                img = `<img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">`;
            }

            itemsElements += `<div class="calc_option_btn_border calc-item__js" data-type="KERAMBLOCKS" data-poddonQnty="${value.poddonQnty}" data-itemPrice="${value.itemPrice}" data-material__item-id="${value.product_id}">
                                        ${img}
                                        ${value.product_name}
                                    </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_row--items').html(itemsElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').removeClass('hidden');
    });

    $('body').on('click', ".calc_housePart--floor .calc_construction[data-constructionType='WALLS'] .calc-item__js[data-type='KERAMBLOCKS']", function () {
        let thisItemId = this.getAttribute('data-material__item-id');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['id'] = thisItemId;
        wallsSelections['floor-' + thisFloorNumber]['poddonQnty'] = this.getAttribute('data-poddonQnty');
        wallsSelections['floor-' + thisFloorNumber]['itemPrice'] = this.getAttribute('data-itemPrice');

        console.log(wallsSelections);
        calculateWallsMaterials();
    });
    //kermablock end//


    //gazoblock start//
    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc-materialType__js[data-materialType="gazoblock"]', function () {
        //console.log(dataFromJSON);
        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_option_sub_block').not('.calc_option_sub_block__materialType').addClass('hidden');

        $(this).closest('.calc_construction').find('.calc-materialType__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['materialType'] = "gazoblock";

        let makers = [];
        let tempValueMakersID = []; //создаем пустой массив с id производителей, чтобы затем проверить, есть ли этот производитель внутри этого массива

        $(dataFromJSON.BRICKS['GAZOBLOCKS']).each(function (index, value) {
            if (!(tempValueMakersID.includes(value.MAKER_VALUE_ID))) {
                if(typeof dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID] === 'undefined') {
                    console.log(value);
                } else {
                    makers.push({value_id: value.MAKER_VALUE_ID, value: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].PUBLIC_NAME, maker_image: dataFromJSON.MAKERS_IMAGES[value.MAKER_VALUE_ID].IMG_URL});
                    tempValueMakersID.push(value.MAKER_VALUE_ID);
                }
            }
        });

        let makersElements = "";
        $(makers).each(function (index, value) {
            makersElements += `<div class="maker calc-maker__js" data-type="GAZOBLOCKS" data-maker_id="${value.value_id}">
                                        <div class="img">
                                            <img src="${value.maker_image}" alt="">
                                        </div>
                                        <span>${value.value}</span>
                                     </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_makers_row__makers').html(makersElements);
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_block--makers').removeClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_maker').removeClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc-block.setka_rastvor').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc-maker__js[data-type="GAZOBLOCKS"]', function () {
        let thisMakerID = this.getAttribute('data-maker_id');
        let thisMakerName = $(this).find('span').text();

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-maker__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['makerID'] = thisMakerID;
        wallsSelections['floor-' + thisFloorNumber]['makerName'] = thisMakerName;

        let densityMarkArr = [];
        $(dataFromJSON.BRICKS['GAZOBLOCKS']).each(function (index, value) {
            if (!densityMarkArr.includes(value.DENSITY_MARK) && value.MAKER_VALUE_ID === thisMakerID) {
                densityMarkArr.push(value.DENSITY_MARK);
            }
        });

        let densityElements = "";
        $(densityMarkArr).each(function (index, value) {
            densityElements += `<div class="calc_option_btn calc_block_density__js" data-type="GAZOBLOCKS" data-densityMark="${value}">${value}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_row--density_mark').html(densityElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_density_mark').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_width_gb').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_density_mark').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc_block_density__js[data-type="GAZOBLOCKS"]', function () {
        let thisDensityMark = this.getAttribute('data-densityMark');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_block_density__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['densityMark'] = thisDensityMark;

        let widthArr = [];
        $(dataFromJSON.BRICKS['GAZOBLOCKS']).each(function (index, value) {
            if (!widthArr.includes(value.WIDTH) && value.MAKER_VALUE_ID === wallsSelections['floor-' + thisFloorNumber].makerID &&
                value.DENSITY_MARK === thisDensityMark ) {
                widthArr.push(value.WIDTH);
            }
        });

        let widthElements = "";
        $(widthArr).each(function (index, value) {
            widthElements += `<div class="calc_option_btn calc_block_width__js" data-type="GAZOBLOCKS" data-width="${value}">${value}</div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_row--width_gb').html(widthElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_width_gb').addClass('hidden');
        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').addClass('hidden');

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_width_gb').removeClass('hidden');
    });

    $('body').on('click', '.calc_housePart--floor .calc_construction[data-constructionType="WALLS"] .calc_block_width__js[data-type="GAZOBLOCKS"]', function () {
        let thisWidth = this.getAttribute('data-width');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc_block_width__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['width'] = thisWidth;

        let itemsArr = [];
        let itemsIdArr = [];
        $(dataFromJSON.BRICKS['GAZOBLOCKS']).each(function (index, value) {
            if (!itemsIdArr.includes(value.ID) &&
                value.MAKER_VALUE_ID === wallsSelections['floor-' + thisFloorNumber].makerID &&
                value.DENSITY_MARK === wallsSelections['floor-' + thisFloorNumber].densityMark &&
                value.WIDTH === thisWidth) {

                itemsIdArr.push(value.ID);
                itemsArr.push({product_id: value.ID, product_image_id: value.IMAGE, product_name: value.NAME, itemHeight: value.HEIGHT,poddonQnty: value.PODDON_QNTY, itemPrice: value.PRICE});
            }
        });

        let itemsElements = "";
        $(itemsArr).each(function (index, value) {
            let img = "";
            if (typeof dataFromJSON.PRODUCTS_IMAGES[value.product_image_id] !== "undefined") {
                img = `<img src="${dataFromJSON.PRODUCTS_IMAGES[value.product_image_id]}" alt="${value.product_name}">`;
            }

            itemsElements += `<div class="calc_option_btn_border calc-item__js" data-type="GAZOBLOCKS" data-poddonQnty="${value.poddonQnty}" data-itemHeight="${value.itemHeight}" data-itemPrice="${value.itemPrice}" data-material__item-id="${value.product_id}">
                                        ${img}
                                        ${value.product_name}
                                    </div>`;
        });

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_row--items').html(itemsElements);

        $('.calc_housePart--floor[data-floorNum="'+thisFloorNumber+'"] .calc_construction[data-constructionType="WALLS"] .calc_option_sub_block_items').removeClass('hidden');
    });

    $('body').on('click', ".calc_housePart--floor .calc_construction[data-constructionType='WALLS'] .calc-item__js[data-type='GAZOBLOCKS']", function () {
        let thisItemId = this.getAttribute('data-material__item-id');

        let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        $(this).closest('.calc_construction').find('.calc-item__js.selected').not(this).removeClass('selected');
        this.classList.add('selected');

        wallsSelections['floor-' + thisFloorNumber]['id'] = thisItemId;
        wallsSelections['floor-' + thisFloorNumber]['poddonQnty'] = this.getAttribute('data-poddonQnty');
        wallsSelections['floor-' + thisFloorNumber]['itemPrice'] = this.getAttribute('data-itemPrice');
        wallsSelections['floor-' + thisFloorNumber]['itemHeight'] = this.getAttribute('data-itemHeight');

        console.log(wallsSelections);
        calculateWallsMaterials();
    });
    //gazoblock end//

    function calculateWallsMaterials() {
        let index = 1;
        let totalWallsArea = 0;
        totalWallsCost = 0;

        let wallsResultLinesHtmlTemplate = $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="1"]').clone();
        $('.results__construction__type[data-resultsConstructionType="peregorodki"] .result__lines').not('.result__lines[data-floorNumber="1"]').remove();

        for (let floor in wallsSelections) {
            if (wallsSelections.hasOwnProperty(floor)) {

                if (typeof wallsSelections[floor].id !== "undefined") {
                    let thisFloorArea = 0;
                    let thisFloorDoorArea = 0;
                    let thisFloorWindowArea = 0;

                    $('.calc_housePart--floor[data-floorNumberText="' + floor + '"] .calc_construction[data-constructiontype="WALLS"] .floor__door-item').each(function (i, value) {
                        let thisDoorQnty = parseInt($(this).find('.input-col--door__qty input').val());
                        let thisDoorHeightCm = parseFloat($(this).find('.input-col--door__height input').val());
                        let thisDoorWidthCm = parseFloat($(this).find('.input-col--door__width input').val());

                        thisFloorDoorArea += thisDoorQnty * (thisDoorHeightCm / 100 * thisDoorWidthCm / 100).toFixed(2);
                    });

                    $('.calc_housePart--floor[data-floorNumberText="' + floor + '"] .calc_construction[data-constructiontype="WALLS"] .floor__window-item').each(function (i, value) {
                        let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
                        let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
                        let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

                        thisFloorWindowArea += thisWindowQnty * (thisWindowHeightCm / 100 * thisWindowWidthCm / 100).toFixed(2);
                    });


                    thisFloorArea += parseFloat(wallsSelections[floor].perimeter) * parseFloat(wallsSelections[floor].height) - thisFloorDoorArea - thisFloorWindowArea;

                    let consumptionRate;
                    let oneBlockVolume = 0;
                    let wallsWidth = 0;
                    let wallsVolume = 0;

                    if (wallsSelections[floor].materialType === "kirpich") {
                        consumptionRate = 400; //if format == 1nf
                        let selectedFormat = wallsSelections[floor].format;

                        wallsWidth = $('.calc_housePart--floor[data-floorNumberText="' + floor + '"] .calc_construction[data-constructiontype="WALLS"] select.wall_width_js').val();
                        console.log("wallsWidth " + wallsWidth);

                        wallsVolume = ((parseFloat(wallsWidth) / 0.5) * 0.13 - 0.01) * thisFloorArea;
                        wallsVolume = wallsVolume.toFixed(2);

                        console.log("perimeter " + wallsSelections[floor].perimeter);
                        console.log("height " + wallsSelections[floor].height);
                        console.log("wallsVolume " + wallsVolume);

                        if (selectedFormat === "1.4НФ") {
                            consumptionRate = 308;
                        } else if (selectedFormat === "2.1НФ") {
                            consumptionRate = 198;
                        }

                    } else if(wallsSelections[floor].materialType === "keramblock") {
                        let selectedFormat = wallsSelections[floor].format;

                        if (selectedFormat === "11.2НФ" && wallsSelections[floor].makerID === "800") {
                            consumptionRate = 10.7;
                        } else if ((selectedFormat === "10.7НФ" ||
                                    selectedFormat === "12.4НФ" ||
                                    selectedFormat === "14.3НФ") && wallsSelections[floor].makerID === "800") {
                            consumptionRate = 17.3;
                        } else if ((selectedFormat === "5.4НФ" || selectedFormat === "6.2НФ" || selectedFormat === "7.2НФ") && wallsSelections[floor].makerID === "800") {
                            consumptionRate = 34;
                        } else if ((selectedFormat === "5.5НФ" || selectedFormat === "7.4НФ") && wallsSelections[floor].makerID === "801") {
                            consumptionRate = 34;
                        } else if ((selectedFormat === "5.4НФ" || selectedFormat === "6.2НФ" || selectedFormat === "7.2НФ") && wallsSelections[floor].makerID === "630") {
                            consumptionRate = 34;
                        } else if ((selectedFormat === "10.5НФ" && wallsSelections[floor].makerID === "630") ||
                                   (selectedFormat === "10.7НФ" && wallsSelections[floor].makerID === "801")) {
                            consumptionRate = 11.5;
                        } else if ((selectedFormat === "10.7НФ" ||
                                    selectedFormat === "12.4НФ" ||
                                    selectedFormat === "14.3НФ") && wallsSelections[floor].makerID === "630") {
                            consumptionRate = 17.3;
                        } else if ((selectedFormat === "10.7НФ" ||
                            selectedFormat === "12.4НФ" ||
                            selectedFormat === "14.3НФ") && wallsSelections[floor].makerID === "630") {
                            consumptionRate = 17.3;
                        } else if ((selectedFormat === "10.7НФ" ||
                            selectedFormat === "14.3НФ" ||
                            selectedFormat === "7НФ") && wallsSelections[floor].makerID === "801") {
                            consumptionRate = 17.4;
                        }
                    } else if(wallsSelections[floor].materialType === "gazoblock") {
                        if (wallsSelections[floor].makerID == "1189" || wallsSelections[floor].makerID == "2301" || wallsSelections[floor].makerID == "2302") {

                            if (wallsSelections[floor].width == "200" && wallsSelections[floor].itemHeight == "200") {
                                consumptionRate = 40;
                                oneBlockVolume = 0.025;
                            }
                            else if (wallsSelections[floor].width == "250" && wallsSelections[floor].itemHeight == "200") {
                                consumptionRate = 32;
                                oneBlockVolume = 0.03125;
                            }
                            else if (wallsSelections[floor].width == "300" && wallsSelections[floor].itemHeight == "200") {
                                consumptionRate = 26.67;
                                oneBlockVolume = 0.0375;
                            }
                            else if (wallsSelections[floor].width == "400" && wallsSelections[floor].itemHeight == "200") {
                                consumptionRate = 20;
                                oneBlockVolume = 0.05;
                            }
                            else if (wallsSelections[floor].width == "500" && wallsSelections[floor].itemHeight == "200") {
                                consumptionRate = 16;
                                oneBlockVolume = 0.0625;
                            }
                            else if (wallsSelections[floor].width == "200" && wallsSelections[floor].itemHeight == "250") {
                                consumptionRate = 32;
                                oneBlockVolume = 0.03125;
                            }
                            else if (wallsSelections[floor].width == "250" && wallsSelections[floor].itemHeight == "250") {
                                consumptionRate = 25.6;
                                oneBlockVolume = 0.03906;
                            }
                            else if (wallsSelections[floor].width == "300" && wallsSelections[floor].itemHeight == "250") {
                                consumptionRate = 21.34;
                                oneBlockVolume = 0.04687;
                            }
                            else if (wallsSelections[floor].width == "400" && wallsSelections[floor].itemHeight == "250") {
                                consumptionRate = 16;
                                oneBlockVolume = 0.0625;
                            }
                            else if (wallsSelections[floor].width == "500" && wallsSelections[floor].itemHeight == "250") {
                                consumptionRate = 12.8;
                                oneBlockVolume = 0.078125;
                            }
                        } else {
                            if (wallsSelections[floor].width == "300" && wallsSelections[floor].itemHeight == "200") {
                                consumptionRate = 27.78;
                                oneBlockVolume = 0.036;
                            } else if (wallsSelections[floor].width == "400" && wallsSelections[floor].itemHeight == "200") {
                                consumptionRate = 20.83;
                                oneBlockVolume = 0.048;
                            } else if (wallsSelections[floor].width == "300" && wallsSelections[floor].itemHeight == "250") {
                                consumptionRate = 22.22;
                                oneBlockVolume = 0.045;
                            } else if (wallsSelections[floor].width == "400" && wallsSelections[floor].itemHeight == "250") {
                                consumptionRate = 16.67;
                                oneBlockVolume = 0.06;
                            } else if (wallsSelections[floor].width == "400" && wallsSelections[floor].itemHeight == "100") {
                                consumptionRate = 41.66;
                                oneBlockVolume = 0.024;
                            } else if (wallsSelections[floor].width == "100" && wallsSelections[floor].itemHeight == "400") {
                                consumptionRate = 41.66;
                                oneBlockVolume = 0.024;
                            }

                        }
                    }

                    let totalBricksRequired = Math.ceil(thisFloorArea * consumptionRate);

                    console.log("foo consumptionRate" + consumptionRate);

                    if (wallsSelections[floor].materialType === "gazoblock") {
                        totalBricksRequired = Math.ceil(thisFloorArea * (wallsSelections[floor].width/1000) * consumptionRate);
                    } else if (wallsSelections[floor].materialType === "kirpich") {
                        totalBricksRequired = Math.ceil(wallsVolume * consumptionRate);
                    }

                    let poddonDivisible = Math.ceil(totalBricksRequired / wallsSelections[floor].poddonQnty) * wallsSelections[floor].poddonQnty;

                    console.log("poddonQnty " + wallsSelections[floor].poddonQnty);
                    console.log("poddonDivisible " + poddonDivisible);
                    console.log("consumptionRate " + consumptionRate);


                    let poddonPrice = 0;
                    if (dataFromJSON['PODDON_PRICE'][wallsSelections[floor].makerID]) {
                        poddonPrice = dataFromJSON['PODDON_PRICE'][wallsSelections[floor].makerID];
                    }

                    console.log('itemPrice ' + wallsSelections[floor].itemPrice);


                    let totalThisFloorWallsCost = poddonDivisible * wallsSelections[floor].itemPrice + poddonPrice * Math.ceil(totalBricksRequired / wallsSelections[floor].poddonQnty) + poddonPrice * Math.ceil(totalBricksRequired / wallsSelections[floor].poddonQnty);

                    if (wallsSelections[floor].materialType === "gazoblock") {
                        totalThisFloorWallsCost = poddonDivisible * oneBlockVolume * wallsSelections[floor].itemPrice + poddonPrice * Math.ceil(totalBricksRequired / wallsSelections[floor].poddonQnty) + poddonPrice * Math.ceil(totalBricksRequired / wallsSelections[floor].poddonQnty);

                    }

                    if (isNaN(totalWallsCost)) {
                        totalWallsCost = 0;
                    }

                    totalWallsCost = totalWallsCost + totalThisFloorWallsCost;

                    totalCost = totalBasementCost + totalFasadniyCost + totalFrontonsCost + totalPeregorodkiCost + totalWallsCost + totalFrontonsWallsCost;

                    let floorNumber = floor.replace("floor-", "");

                    if (parseInt(floorNumber )> 1) {
                        let newResultsInstance = $(wallsResultLinesHtmlTemplate).clone();

                        $(newResultsInstance).attr('data-floorNumber', floorNumber);
                        $(newResultsInstance).find('.floorNumber').text(floorNumber + " этаж");

                        $(newResultsInstance).appendTo('.results__construction__type[data-resultsConstructionType="walls"]');
                    }

                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--maker__js .result_value').text(wallsSelections[floor].makerName);
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_value').text(totalBricksRequired);
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_value').text(poddonDivisible);
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_value').text(wallsSelections[floor].poddonQnty);
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonAmount__js .result_value').text(Math.ceil(totalBricksRequired / wallsSelections[floor].poddonQnty));
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonPrice__js .result_value').text(poddonPrice);
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--totalCost__js .result_value').text(formatCost(totalThisFloorWallsCost));

                    if (wallsSelections[floor].materialType === "kirpich") {
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--strengthMark__js').css("display", "flex");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js').css("display", "flex");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--densityMark__js').css("display", "none");

                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_name span').text("кирпича, шт");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_name span').text("шт");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_name span').text("шт");

                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--strengthMark__js .result_value').text(wallsSelections[floor].strengthMark);
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js .result_value').text(wallsSelections[floor].format);
                    }

                    if (wallsSelections[floor].materialType === "keramblock") {
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--strengthMark__js').css("display", "none");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js').css("display", "none");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--densityMark__js').css("display", "none");

                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_name span').text("блока, шт");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_name span').text("шт");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_name span').text("шт");


                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js .result_value').text(wallsSelections[floor].format);
                    }

                    if (wallsSelections[floor].materialType === "gazoblock") {
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--strengthMark__js').css("display", "none");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--format__js').css("display", "none");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--densityMark__js').css("display", "flex");

                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--bricksTotal__js .result_name span').text("блока, м3");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonDivisibleQnty__js .result_name span').text("м3");
                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--poddonQnty__js .result_name span').text("м3");


                        $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="' + floorNumber + '"] .result__line--densityMark__js .result_value').text(wallsSelections[floor].densityMark);
                    }

                    $('.calc-col__results .final__price .final_price_value span').text(formatCost(totalCost));

                    $('.results__construction__type[data-resultsConstructionType="walls"]').removeClass('hidden');
                    $('.results__construction__type[data-resultsConstructionType="walls"] .result__lines[data-floorNumber="'+floorNumber+'"]').removeClass('hidden');
                }
            }

            index++;
        }

        $('.results__construction__type[data-resultsConstructionType="walls"] .result__line--setkaMeter__js').remove();
        $('.results__construction__type[data-resultsConstructionType="walls"] .result__line--setkaWidth__js').remove();

        $('.calc_construction[data-constructiontype="WALLS"]').each(function (index, value) {
            let thisFloorNum = $(this).closest('.calc_housePart--floor').attr('data-floornum');

            let thisSetkaRows;

            let thisBrickHeight;
            let thisBrickWidth;

            if (wallsSelections["floor-" + thisFloorNum].materialType === "kirpich") {
                thisSetkaRows = $(this).find('select.setka__rows_js').val();
                thisBrickHeight = 0.075;

                let widthValue = $(this).find('select.wall_width_js').val();
                thisBrickWidth = ((parseFloat(widthValue) / 0.5) * 0.13 - 0.01).toFixed(2);

                if (wallsSelections["floor-" + thisFloorNum].format === "1.4НФ") {
                    thisBrickHeight = 0.098;
                } else if (wallsSelections["floor-" + thisFloorNum].format === "2.1НФ") {
                    thisBrickHeight = 0.15;
                }
            } else if (wallsSelections["floor-" + thisFloorNum].materialType === "keramblock") {
                thisBrickHeight = 0.219;
                thisSetkaRows = $(this).find('select.calc_setka_rows__js').val();

                let thisBlockFormat = wallsSelections["floor-" + thisFloorNum].format;
                let thisBlockMakerId = wallsSelections["floor-" + thisFloorNum].makerID;

                thisBrickWidth = parseInt(dataFromJSON.KERAMBLOCKS_FORMAT_NAMES[thisBlockMakerId][thisBlockFormat]) / 100;
            } else if (wallsSelections["floor-" + thisFloorNum].materialType === "gazoblock") {
                thisSetkaRows = $(this).find('select.calc_setka_rows__js').val();

                thisBrickHeight = wallsSelections["floor-" + thisFloorNum].itemHeight / 1000;
                thisBrickWidth = wallsSelections["floor-" + thisFloorNum].width / 1000;
            }

            let thisFloorBricksRowsHeight = Math.ceil((fasadniySelections["floor-" + thisFloorNum].height) / thisBrickHeight);

            let thisSetkaRunningMeter = Math.ceil((thisFloorBricksRowsHeight / thisSetkaRows) * parseFloat(fasadniySelections["floor-" + thisFloorNum].perimeter));

            let thisFloorDoorRunningMeters = 0;
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNum+'"] .calc_construction[data-constructiontype="WALLS"] .floor__door-item').each(function (i, value) {
                let thisDoorQnty = parseInt($(this).find('.input-col--door__qty input').val());
                let thisDoorHeightCm = parseFloat($(this).find('.input-col--door__height input').val());
                let thisDoorWidthCm = parseFloat($(this).find('.input-col--door__width input').val());

                let thisDoorKladkaRowsNumber = Math.ceil((thisDoorHeightCm/100) / thisBrickHeight);

                thisFloorDoorRunningMeters += parseFloat((thisDoorQnty*(thisDoorKladkaRowsNumber / thisSetkaRows) * thisDoorWidthCm/100).toFixed(2));

                console.log("thisFloorDoorRunningMeters " + thisFloorDoorRunningMeters);
            });

            let thisFloorWindowsRunningMeters = 0;
            $('.calc_housePart--floor[data-floorNum="'+thisFloorNum+'"] .calc_construction[data-constructiontype="WALLS"] .floor__window-item').each(function (i, value) {
                let thisWindowQnty = parseInt($(this).find('.input-col--window__qty input').val());
                let thisWindowHeightCm = parseFloat($(this).find('.input-col--window__height input').val());
                let thisWindowWidthCm = parseFloat($(this).find('.input-col--window__width input').val());

                let thisWindowKladkaRowsNumber = Math.ceil((thisWindowHeightCm/100) / thisBrickHeight);

                thisFloorWindowsRunningMeters += parseFloat((thisWindowQnty*(thisWindowKladkaRowsNumber / thisSetkaRows) * thisWindowWidthCm/100).toFixed(2));
            });

            thisSetkaRunningMeter = (thisSetkaRunningMeter - thisFloorDoorRunningMeters - thisFloorWindowsRunningMeters).toFixed(2);

            let setkaResultEl = `<div class="result__line result__line--setkaMeter__js">
                                        <div class="result_name">Сетка, пог. м</div>
                                        <div class="result_value">${thisSetkaRunningMeter}</div>
                                    </div>
                                    
                                    <div class="result__line result__line--setkaWidth__js">
                                        <div class="result_name">Ширина сетки, м.</div>
                                        <div class="result_value">${thisBrickWidth}</div>
                                    </div>`;

            $(setkaResultEl).insertBefore('.results__construction__type[data-resultsconstructiontype="walls"] .result__lines[data-floornumber="'+thisFloorNum+'"] .result__line--totalCost__js')
            console.log("thisSetkaRunningMeter " + thisSetkaRunningMeter);
        });
    }


    /* WALLS END */

    $('body').on('click', '.construction_type_item__js',function () {
       // let thisFloorNumber = $(this).closest('.calc_housePart--floor').attr('data-floorNum');

        let thisCalcConstructionWrapper = $(this).closest('.calc_construction');
        $(thisCalcConstructionWrapper).nextAll('.calc_construction').remove();
    });

    let calculatorHtmlTemplate = $('.calc-row').clone();

    $('body').on('click', '.new_calc_js', function () {
        let newCalculatorInstance = $(calculatorHtmlTemplate).clone();

        $('.calc-row').replaceWith(newCalculatorInstance);
    });

    let floorCalcConstructionHtmlTemplate = $('.calc_housePart--floor__js .calc_construction').clone();

    $('body').on('click', '.calc_housePart--floor__js .calc__block-clear', function () {

        let thisAvailableSelections = $(this).closest('.calc_construction').find('.constructions_options_row').clone();
        $(thisAvailableSelections).find('.construction_type_item__js').removeClass('selected');

        let newCalcConstructionBlockInstance = $(floorCalcConstructionHtmlTemplate).clone();

        let thisBlockNumber = $(this).closest('.title_row').find('.construction__number__js').text();
        $(newCalcConstructionBlockInstance).find('.construction__number__js').text(thisBlockNumber);
        $(newCalcConstructionBlockInstance).removeClass('hidden');

        if (parseInt(thisBlockNumber) > 1) {
            $(newCalcConstructionBlockInstance).find('.constructions_options_row').replaceWith(thisAvailableSelections);
        }

        $(this).closest('.calc_housePart--floor__js').find('.add_new_construction').addClass('hidden');

        $(this).closest('.calc_construction').replaceWith(newCalcConstructionBlockInstance);
    })

    $('body').on('click', '#printResultsBtn', function () {

        console.log('print');
        var divContents = $("#calcResults").html();
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html><head><title>DIV Contents</title>');
        printWindow.document.write('<link rel="stylesheet" href="/local/templates/tulpar_store/assets/css/styles.css" />');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();
    });

});


document.addEventListener("DOMContentLoaded", function() {
    setTimeout(() => 
    {
        var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
        var lazyBg = [].slice.call(document.querySelectorAll(".lazy-bg"));

        if ("IntersectionObserver" in window) {
            let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                let lazyImage = entry.target;
                lazyImage.src = lazyImage.dataset.src;
                lazyImage.srcset = lazyImage.dataset.srcset;
                lazyImage.classList.remove("lazy");
                lazyImageObserver.unobserve(lazyImage);
                }
            });
            });

            lazyImages.forEach(function(lazyImage) {
            lazyImageObserver.observe(lazyImage);
            });

            //lazy-bg
            let lazyImageObserverBg = new IntersectionObserver(function(entries, observer) {
                entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                    let lazyBg = entry.target;
                    lazyBg.style.backgroundImage = 'url(' + lazyBg.dataset.src + ')';
                    lazyBg.srcset = lazyBg.dataset.srcset;
                    lazyBg.classList.remove("lazy-bg");
                    lazyImageObserverBg.unobserve(lazyBg);
                }
                });
            });
        
            lazyBg.forEach(function(lazyBg) {
                lazyImageObserverBg.observe(lazyBg);
            });

        } else {
            // Possibly fall back to a more compatible method here
        }
    }, 1000)
 
});

(function($) {
    $.fn.inputFilter = function(inputFilter) {
        return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
            if (inputFilter(this.value)) {
                this.oldValue = this.value;
                this.oldSelectionStart = this.selectionStart;
                this.oldSelectionEnd = this.selectionEnd;
            } else if (this.hasOwnProperty("oldValue")) {
                this.value = this.oldValue;
                this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
            }
        });
    };
}(jQuery));

$(document).ready(function() {
    // Restrict input to digits by using a regular expression filter.
    $('[data-type="integer"]').inputFilter(function(value) {
        return /^\d*$/.test(value);
    });

    $('[data-type="floatingNumber"]').inputFilter(function(value) {
        return /^-?\d*[.,]?\d*$/.test(value);
    });


    $('body').on('click', '.show-full-phone', function () {
       $('.blur-phone').css("filter", "none");
       $('.show-full-phone').hide();
       $('.footer-phone-wrapper').css({"border":"none", "padding": "0"});
       
       $('.sticky-header').css('padding', '4px 0');

    });

    $('body').on('click', '.basket-handler__js[data-basketHandler="plus"]', function () {
        let measureRatio = $(this).closest('.change-qty').attr('data-measure-ratio');
        let productId = $(this).closest('.change-qty').attr('data-product-id');
        let basketRecordId = $(this).closest('.change-qty').attr('data-basket-record-id');
        let thisBtn = $(this);

        updateBasket('add', measureRatio, productId, basketRecordId);
    });

    $('body').on('click', '.basket-handler__js[data-basketHandler="minus"]', function () {
        let measureRatio = $(this).closest('.change-qty').attr('data-measure-ratio');
        let productId = $(this).closest('.change-qty').attr('data-product-id');
        let basketRecordId = $(this).closest('.change-qty').attr('data-basket-record-id');

        updateBasket('decrease', measureRatio, productId, basketRecordId);
    });

    $('body').on('click', '.basket-handler__js[data-basketHandler="remove"]', function () {
        let productId = $(this).closest('.product-table-row').attr('data-product-id');
        let basketRecordId = $(this).closest('.product-table-row').attr('data-basket-record-id');

        updateBasket('remove', null, productId, basketRecordId);
    });


    $('#cart_form_new').on('submit', function(e) {
        e.preventDefault();
        $.ajax({
            type: 'post',
            url: '/local/ajax/create_order.php',
            dataType: 'json',
            data: $('#cart_form_new').serialize(),
            success:function(data) {
                $("h2.modal-title").hide();
                $('#cart_form_new').hide();
                $('#send-cart .footer-default').hide();
                $('#send-cart .success-msg').show();
                console.log(data);
                
            },
            complete:function () {
                $("h2.modal-title").hide();
                $('#cart_form_new').hide();
                $('#send-cart .footer-default').hide();
                $('#send-cart .success-msg').show();
                var _tmr = _tmr || [];
                _tmr.push({
                    type: 'itemView',
                    productid: product_tmr.productid,
                    pagetype: 'purchase',
                    list: product_tmr.list,
                    totalvalue: ( (basketTotal) ? basketTotal : 0 )
                });
                console.log('mytarget', product_tmr, basketTotal);
                setTimeout(function () {
                    location.reload();
                }, 2000);

            }
        });
    });

    $('#add2cart').click(function () {
       $('.addedToCartSuccess').toggleClass('show');
    });
});

function updateBasket(actionType, measureRatio, productId, basketRecordId) {
    let data = {};
    data['actionType'] = actionType;
    data['measureRatio'] = measureRatio;
    data['productId'] = productId;
    data['basketRecordId'] = basketRecordId;

    data = JSON.stringify(data);

    $.ajax({
        type: 'post',
        url: '/basket/index.php',
        dataType: 'html',
        data: {action: "updateBasket", data: data},
        success:function(response) {
            console.log(response);
            $("#basket--js").replaceWith($(response));
        }
    });
}