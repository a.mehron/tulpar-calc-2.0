<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$city = getCityData();

use Bitrix\Sale;
use Bitrix\Sale\Basket;
Bitrix\Main\Loader::includeModule("sale");


$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
?><footer>
    <div class="container p-sm-0">
        <div class="row top pb-3">
            <div class="col-xl-1 logo pr-2 pl-0 d-none d-xl-block">
                <img class="logo" src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/logo_new.svg" width="87" alt="">
            </div>

            <div class="col-xl-6 offset-xl-2 col-lg-9">
                <ul class="footer-menu pl-0 pl-md-0">
                    <li><a href="/about/">О компании</a></li>
                    <li><a href="/articles/">Полезные статьи</a></li>

                    <li><a href="/credit/">В кредит</a></li>
                    <li><a href="/contacts/">Контакты</a></li>

                    <li><a href="/oplata/">Оплата</a></li>
<li><a href="/obmen-i-vozvrat/">Обмен и возврат</a></li>
                    <li><a href="/dostavka/">Доставка</a></li>

                    <li><a href="/proizvoditeli/">Производители</a></li>
                    <li><a href="/agreement/">Политика конфиденциальности</a></li>
					<ul class="footer-social">
                        <li><a href="https://www.facebook.com/tulparooo"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://vk.com/tulpar_trade"><i class="fa fa-vk"></i></a></li>
                        <li><a href="https://ok.ru/group/55773038379124"><i class="fa fa-odnoklassniki"></i></a></li>
					</ul>
					<ul class="footer-social">
                        <li><a href="https://www.instagram.com/tulpar_trade/"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCVORTWcbjF3oZf-A2uTQCIg/featured"><i class="fa fa-youtube-play"></i></a></li>
                        <li><a class="fa" href="http://zen.yandex.ru/id/5ed90b1db87fc303ce4e5610"><i class="fab fa-yandex"></i></a></li>

<?/*
 <li><a class="fa" href="https://zen.yandex.ru/profile/editor/id/5ed90b1db87fc303ce4e5610"><i class="fab fa-yandex"></i></a></li>
<!--<li><a href="https://zen.yandex.ru/profile/editor/id/5ed90b1db87fc303ce4e5610"><img id="dzen" src="<?=SITE_TEMPLATE_PATH?>/assets/img/zen-icon.svg" alt=""></a></li>-->      */?>              
					</ul>
                </ul>
            </div>

            <div class="col-xl-3 col-lg-3 pr-sm-0 footer-address text-white">
                <div class="small-text">Адрес</div>
                <div class="large"><?=$city['address']?></div>
                <div class="small-text">Телефон</div>
                <div class="large" id="podmena"><span class="footer-phone-wrapper"><?=$city['phone']?></span></div>
                <div class="small-text">Режим работы</div>
                <div class="large"><?=$city['time']?></div>
            </div>
        </div>

        <div class="row copyright pt-4 d-block d-sm-flex">
            <div class="col-12 p-0 mb-3">Цены на сайте не являются публичной офертой.</div>
            <div class="col-12 p-0">© ООО Тулпар, 2012-2020. <br>
                Все материалы данного сайта являются объектами авторского права (в том числе дизайн). Запрещается копирование, распространение (в том числе путем копирования на другие сайты и ресурсы в Интернете) или любое иное использование информации и объектов без предварительного письменного согласия правообладателя. Любое нарушение авторских и смежных прав преследуется по закону (ст. 146 УК РФ, предусматривающая наказания в виде 2-х лет лишения свободы).
            </div>
        </div>
    </div>
</footer>


<div class="nav-shadow"></div>

<a href="#" class="scrollup">Scroll</a>

<div style="display: none;" class="fancybox-cub" id="credit-application">
	<span class="modal-title">Заявка на кредит</span>
    <form id="credit-form">
        <div class="form-group row">
          <label for="appl_name_credit" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name_credit" name="appl_name_credit" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_phone_credit" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone_credit" name="appl_phone_credit" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <div class="form-group row">
        <label for="appl_comment_credit" class="col-sm-3 col-form-label">Комментарий</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" required id="appl_comment_credit" name="appl_comment_credit" placeholder="Здесь вы можете описать свои комментарии к заявке">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement/" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <span class="sent-title">Ваша заявка отправлена</span>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="credit-form" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>


<div style="display: none;" class="fancybox-cub" id="zamer-application">
	<span class="modal-title">Заявка на расчет</span>
    <form id="calculation">
        <input type="hidden" name="topic" value="Заявка на расчет">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name" name="appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_email" class="col-sm-3 col-form-label">Эл. почта<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="email" class="form-control" required id="appl_email" name="appl_email" placeholder="Email">
        </div>
      </div>
      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone" name="appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <div class="form-group row">
        <label for="appl_comment" class="col-sm-3 col-form-label">Комментарий</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" required id="appl_comment" name="appl_comment" placeholder="Здесь вы можете описать свои комментарии к заявке">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement/" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <span class="sent-title">Ваша заявка отправлена</span>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="calculation" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div style="display: none;" class="fancybox-cub" id="oneclick-buy-modal">
    <div class="callback_show_on_open">
        <span class="h2">Купить в один клик</span>
        <div class="product_oneclick"></div>

        <div class="attention">
            Наш специалист свяжется с Вами в течение 15 минут
        </div>
    </div>

    <form id="oneclick-buy-form">
        <input type="hidden" name="topic" value="Купить в один клик">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name" name="appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone" name="appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement/" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <span class="h2 sent-title">Ваша заявка отправлена</span>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="oneclick-buy-form" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div style="display: none;" class="fancybox-cub" id="callback-enquiry">
    <div class="callback_show_on_open">
        <span class="h2">Заказать обратный звонок</span>

        <div class="attention">
            Наш специалист свяжется с Вами в течение 10 минут
        </div>
    </div>

    <form id="callback-form">
        <input type="hidden" name="topic" value="Обратный звонок">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name" name="appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone" name="appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement/" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <span class="h2 sent-title">Ваша заявка отправлена</span>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="callback-form" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div style="display: none;" class="fancybox-cub" id="feedback-application">
	<span class="h2 modal-title">Написать отзыв</span>
    <form id="feedback-form">
        <input type="hidden" name="topic" value="Заявка на расчет">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="appl_name" name="appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>
      <div class="form-group row">
        <label for="appl_email" class="col-sm-3 col-form-label">Эл. почта<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="email" class="form-control" required id="appl_email" name="appl_email" placeholder="Email">
        </div>
      </div>
      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_phone" name="appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <div class="form-group row">
        <label for="appl_comment" class="col-sm-3 col-form-label">Комментарий</label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="appl_comment" name="appl_comment" placeholder="Здесь вы можете описать свои комментарии к заявке">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement/" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <span class="h2 sent-title">Ваша заявка отправлена</span>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="calculation" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div style="display: none;" class="fancybox-cub" id="send-cart">
	<span class="h2 modal-title">Оформление заказа</span>
    <form id="cart_form_new">
        <input type="hidden" name="cart_topic" value="Оформление заказа">
        <input type="hidden" id="cart_items_input" name="cart_items" value="">
        <div class="form-group row">
          <label for="appl_name" class="col-sm-3 col-form-label">Ваше имя<span class="red">*</span></label>
          <div class="col-sm-9">
            <input type="text" class="form-control" required id="cart_appl_name" name="cart_appl_name" placeholder="Иванов Иван Иванович">
          </div>
        </div>

      <div class="form-group row">
        <label for="appl_phone" class="col-sm-3 col-form-label">Телефон<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="tel" class="form-control" required id="cart_appl_phone" name="cart_appl_phone" placeholder="+7 (999) 123-4567">
        </div>
      </div>

      <div class="form-group row">
        <label for="cart_appl_email" class="col-sm-3 col-form-label">Email<span class="red">*</span></label>
        <div class="col-sm-9">
          <input type="email" class="form-control" required id="cart_appl_email" name="cart_appl_email">
        </div>
      </div>

      <div class="form-group row">
        <label for="appl_comment" class="col-sm-3 col-form-label">Комментарий</label>
        <div class="col-sm-9">
          <input type="text" class="form-control" required id="cart_appl_comment" name="cart_appl_comment" placeholder="Здесь вы можете описать свои комментарии к заявке">
        </div>
      </div>

      <label class="custom-control custom-control--flex custom-checkbox">
            <input type="checkbox" class="custom-control__input required" name="is_agreement" data-lh_name="-570417102_field_7">
            <span class="custom-control__indicator"></span>
            <span class="custom-control__description">Я даю свое <a href="/agreement/" target="_blank">согласие на обработку</a> моих персональных данных.</span>
        </label>
    </form>

    <div class="success-msg">
        <span class="h2 sent-title">Ваша заказ отправлен</span>
        <p>Наши менеджеры свяжутся с Вами в ближайшее возможное время</p>
    </div>

    <div class="modal-footer">
        <div class="col-12 footer-default">
            <p class="text-left"><small><span class="red">*</span> Поля для обязательного заполнения</small></p>
            <button type="submit" form="cart_form_new" id="cart_form_btn" class="btn btn-block">ОТПРАВИТЬ</button>
        </div>
        <div class=" col-12 d-none">
            <button type="button" class="btn btn-success btn-block" data-dismiss="modal" aria-label="Close">ЗАКРЫТЬ</button>
        </div>
    </div>
</div>

<div class="addedToCartSuccess"><span>Товар успешно добавлен в корзину</span></div>
<? /*
<script>
var modules = {};
modules.list = [
  '/bitrix/js/main/jquery/jquery-3.3.1.min.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
	'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.2/rangeslider.min.js',
	'https://cdnjs.cloudflare.com/ajax/libs/jQueryFormStyler/2.0.2/jquery.formstyler.min.js',
	'/local/templates/tulpar_store/assets/js/app.js',
  '/local/templates/tulpar_store/assets/js/lazyload.min.js',

];

modules.load = ( i = 0 ) =>
{
	var script = document.createElement( 'script' );
  script.src = modules.list[ i ];
  script.defer = 'defer';
	script.onload = () => 
	{
		if ( ++i < modules.list.length ) modules.load( i );
	};
	document.head.appendChild( script );
}

window.addEventListener( 'load', () => modules.load() );
</script>
*/ ?>
<? /*
<!-- Facebook Pixel Code -->
<script data-skip-moving="true">
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2084480491641859');
    fbq('track', 'PageView');
</script>
<noscript data-skip-moving="true">
    <img height="1" width="1"
          src="https://www.facebook.com/tr?id=2084480491641859&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->

 <!-- Yandex.Metrika counter -->
<script type="text/javascript" data-skip-moving="true">
    (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
        m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(<?=$city['yandex']?>, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        ecommerce:"dataLayer"
    });
</script> 
<noscript data-skip-moving="true"><div><img src="https://mc.yandex.ru/watch/<?=$city['yandex']?>" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
*/?>
<script>	
  window.lazyLoadOptions = {	
        elements_selector: ".lazy",	
    };
    $(document).ready(() => 
    {
      $(".contact-us-btn").click(function(){
        $(this).toggleClass("pressed");
        $(this).siblings(".nav-feedback").toggleClass("show");
      });
      $(".result-sort-current .result-sort-text").click(function(){
        $(".result-sort-list").toggle();
      });
    })
</script>
<? /*
<script>
(function(w, d, s, h, id) {
    w.roistatProjectId = id; w.roistatHost = h;
    var p = d.location.protocol == "https:" ? "https://" : "http://";
    var u = /^.roistat_visit=[^;]+(.)?$/.test(d.cookie) ? "/dist/module.js" : "/api/site/1.0/"+id+"/init";
    var js = d.createElement(s); js.charset="UTF-8"; js.async = 1; js.src = p+h+u; var js2 = d.getElementsByTagName(s)[0]; js2.parentNode.insertBefore(js, js2);
})(window, document, 'script', 'cloud.roistat.com', '225fd04ffcbe3c3708f9320cfac9398d');
</script> */?>
<?php $APPLICATION->ShowHeadScripts(); ?>
<!-- calltouch -->
<script type="text/javascript" >
    jQuery(document).on('click', 'input[type="submit"]', function() {
        console.log('ct order event');
        var m = jQuery(this).closest('form');
        var fio = m.find('input[name*="name"]').val();
        var phone = m.find('input[name*="phone"]').val();
        var mail = m.find('input[name*="email"]').val();
        var comment = m.find('textarea[name*="message"]').val();
        var ct_site_id = '38913';
        var sub = 'Заявка "'+m.find('input[type="submit"]').attr('value')+'"'; 
        if (comment === 'undefined'){comment = ''};
        var ct_data = {
            fio: fio,
            phoneNumber: phone,
            subject: sub,
            email: mail,
            comment: comment,
            requestUrl: location.href,
            sessionId: window.call_value
        };
        console.log(ct_data);
        if (!!phone || !!mail){
            jQuery.ajax({
                url: 'https://api-node15.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
                dataType: 'json', type: 'POST', data: ct_data, async: false
            });
        };
    });
    jQuery(document).on('click', 'button[type="submit"][form="credit-form"]', function() {
        console.log('ct order event');
        var m = jQuery('#credit-form');
        var fio = m.find('input[name*="name"]').val();
        var phone = m.find('input[name*="phone"]').val();
        var mail = m.find('input[name*="email"]').val();
        var comment = m.find('input[name*="comment"]').val();
        var ct_site_id = '38913';
        var sub = 'Заявка "Заявка на кредит"'; 
        if (comment === 'undefined'){comment = ''};
        var ct_data = {
            fio: fio,
            phoneNumber: phone,
            subject: sub,
            email: mail,
            comment: comment,
            requestUrl: location.href,
            sessionId: window.call_value
        };
        console.log(ct_data);
        if (!!phone || !!mail){
            jQuery.ajax({
                url: 'https://api-node15.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
                dataType: 'json', type: 'POST', data: ct_data, async: false
            });
        };
    });
    jQuery(document).on('click', 'button[type="submit"][form="oneclick-buy-form"]', function() {
        console.log('ct order event');
        var m = jQuery('#oneclick-buy-form');
        var fio = m.find('input[name*="name"]').val();
        var phone = m.find('input[name*="phone"]').val();
        var ct_site_id = '38913';
        var sub = 'Заявка "Купить в один клик"'; 
        var ct_data = {
            fio: fio,
            phoneNumber: phone,
            subject: sub,
            requestUrl: location.href,
            sessionId: window.call_value
        };
        console.log(ct_data);
        if (!!phone || !!mail){
            jQuery.ajax({
                url: 'https://api-node15.calltouch.ru/calls-service/RestAPI/requests/'+ct_site_id+'/register/',
                dataType: 'json', type: 'POST', data: ct_data, async: false
            });
        };
    });

    var basketTotal = "<?=$basket->getPrice()?>";
    if( typeof product_tmr != 'undefined' ) {
      var _tmr = _tmr || [];
      _tmr.push({
          type: 'itemView',
          productid: product_tmr.productid,
          pagetype: product_tmr.pagetype,
          list: product_tmr.list,
          totalvalue: ( (basketTotal) ? basketTotal : "0" )
      });
      console.log('mytarget', product_tmr, basketTotal);
    }
    
</script>

<!-- /calltouch -->
</body>
</html>