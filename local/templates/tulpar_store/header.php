<?
//global $USER;
$city = getCityData();

//if (preg_match('/^(.*)\/\/+(.*)$/', $_SERVER['REQUEST_URI'], $matches) === 1) {
//    $str = preg_replace('~/{2,}~', '/', $_SERVER['REQUEST_URI']);
//    header('Location: https://izhevsk.tulpar-trade.ru'.$str);
//    header('HTTP/1.1 301 Moved Permanently');
//}

//print_r($_SERVER['REQUEST_URI']);

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Sale;
use Bitrix\Sale\Basket;
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");
?><!doctype html>
<html class="<?$APPLICATION->ShowProperty("class")?>">
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WP562VB');</script>
<!-- End Google Tag Manager -->

 <? /*     <!-- Google Tag Manager -->
   <script data-skip-moving="true">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','<?=$city['google-tag']?>');</script>
    <!-- End Google Tag Manager --> 

Global site tag (gtag.js) - Google Analytics
    <script data-skip-moving="true" async src="https://www.googletagmanager.com/gtag/js?id=<?=$city['google']?>"></script>
    <script data-skip-moving="true">
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '<?=$city['google']?>');
    </script>
--> */ ?>


    
    <?php  $APPLICATION->ShowHead(); ?>
    <?
    use Bitrix\Main\Page\Asset;
    // CSS
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/bootstrap.min.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/slick-theme.min.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/slick.min.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/jquery.fancybox.min.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/styles.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/responsive.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/roboto-fonts.css');
    Asset::getInstance()->addCss('/bitrix/css/main/font-awesome.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/jquery.formstyler.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/jquery.formstyler.theme.min.css');
    Asset::getInstance()->addCss('/local/templates/tulpar_store/assets/css/libs/rangeslider.min.css');



    // JS
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/jquery-3.3.1.min.min.js');
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/popper.min.js');
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/bootstrap.min.js');
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/jquery.fancybox.min.js');
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/jquery.maskedinput.min.js');
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/rangeslider.min.js');
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/slick.min.js');

    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/lazyload.min.js');
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/jquery.formstyler.min.js');

    //STRING
    Asset::getInstance()->addString('<meta charset="utf-8">');
    Asset::getInstance()->addString("<link rel='shortcut icon' href='/local/favicon.ico' />");
    Asset::getInstance()->addString('<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">');
    //  Asset::getInstance()->addString("<link href='/local/templates/tulpar_store/assets/css/libs/roboto-fonts.css' rel='stylesheet'>");
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/libs/b422bfb009.js');
    Asset::getInstance()->addJs('/local/templates/tulpar_store/assets/js/app.js');
?>
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/assets/img/favicon.png">
    <title><?$APPLICATION->ShowTitle()?></title>


</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WP562VB"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<? /* <!-- Google Tag Manager (noscript) -->
    <noscript data-skip-moving="true"><iframe src="https://www.googletagmanager.com/ns.html?id=<?=$city['google-tag']?>"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->*/ ?>

<?$APPLICATION->ShowPanel();?>

<?php
global $USER;
session_start();
$wish_count = sizeof($_SESSION['element_ids']);
$cmp_count = sizeof($_SESSION['cmp_element_ids']);

if ($wish_count != 0) {
    $bookmarked = "(".$wish_count.")";
} else {
    $bookmarked = "";
}

if ($cmp_count != 0) {
    $compared = "(".$cmp_count.")";
} else {
    $compared = "";
}


$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$basketItems = $basket->getBasketItems();
if (sizeof($basketItems) > 0) {
    $in_cart = "(".sizeof($basketItems).")";
}
?>

<?
if ($USER->IsAuthorized()) { ?><?}?>
<div class="sticky-header" id="sticky-header">
    <div class="container">
        <div class="logo">
            <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/logo_new.svg" width="87" alt=""></a>
        </div>

        <div class="phone">
            <a href="tel:<?=$city['phonelink']?>" class="phone_podmena"><?=$city['phone']?></a>
           <?/* <div class="show-full-phone"><span>Показать телефон</span></div>*/?>
        </div>

        <div class="get-consultation"><a data-fancybox data-src="#callback-enquiry" href="javascript:;">Получить бесплатную консультацию</a></div>
    </div>
</div>

<?
// Autoreload
if (!$_SESSION["SOTBIT_REGIONS"]["NAME"] && empty($_SESSION["RELOAD"])) {
    $_SESSION["RELOAD"] = true;
    ?>
    <script>
        location.reload();
    </script>
<?
}
?>

<header class="header" data-region="<?=$_SESSION["SOTBIT_REGIONS"]["NAME"]?>">
    <div class="container p-sm-0">
        <div class="row site-menu-top">
            <div class="column">
                <div class="site-menu">
                    <div class="row mr-0 pb-lg-2 pb-xl-0">
                        <div class="col-xl-1 col-lg-2 col-sm-2 pr-lg-0">
                            <div class="city city--placeholder">
                                <span><?=$city['name']?></span>
                                <!--
                                <div class="dropdown" style="float:left;display:none; ">
                                    <span class="dropbtn"><?=$city['name']?></span>
                                    <?
    //$sites = array(
    //                                  'https://tulpar-trade.ru' => 'Казань',
    //                                  'https://izhevsk.tulpar-trade.ru/' => 'Ижевск',
    //                                  'https://chelny.tulpar-trade.ru/' => 'Набережные Челны'
    //                              );
                                    ?>
                                    <div class="dropdown-content" style="left:0;">
                                    <?foreach ($sites as $url => $stitle): if ($city['name']!=$stitle):?>
                                        <li><a href="<?=$url?>"><?=$stitle?></a></li>
                                    <?endif; endforeach;?>
                                    </div>
                                 </div>
                                 -->
                            </div>
                        </div>

                        <div class="col-xl-8 col-lg-10 col-sm-10 pl-0 pr-lg-0 pl-lg-4">
                            <nav class="menu-list">
                                <?$APPLICATION->IncludeComponent(
    "bitrix:menu", 
    "top_menu", 
    array(
        "ALLOW_MULTI_SELECT" => "N",
        "CHILD_MENU_TYPE" => "left",
        "DELAY" => "N",
        "MAX_LEVEL" => "1",
        "MENU_CACHE_GET_VARS" => array(
        ),
        "MENU_CACHE_TIME" => "3600",
        "MENU_CACHE_TYPE" => "A",
        "MENU_CACHE_USE_GROUPS" => "Y",
        "ROOT_MENU_TYPE" => "top",
        "USE_EXT" => "N",
        "COMPONENT_TEMPLATE" => "top_menu"
    ),
    false
);?>
                            </nav>
                        </div>

                        <div class="col-xl-3 cub d-sm-none d-xl-block">
                            <div class="contact-us-btn">Связаться с нами</div>
                            <ul class="nav-feedback" id="nav-feedback1">
                                <li class="nav-item na-raschet">
                                    <a data-fancybox data-src="#zamer-application" href="javascript:;">
                                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" alt="">
                                        <span>Отправить заявку на расчет</span>
                                    </a>
                                </li>

                                <li class="nav-item callback-enquiry">
                                    <a data-fancybox data-src="#callback-enquiry" href="javascript:;">
                                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" alt="">
                                        <span>Заказать звонок</span>
                                    </a>
                                </li>

                                <li class="nav-item na-raschet">
                                    <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" alt="">
                                        <span>Стать поставщиком</span>
                                    </a>
                                </li>

                                <li class="nav-item na-raschet">
                                    <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" alt="">
                                        <span>Оставить отзыв</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row site-menu-top mobile d-none">
            <div class="column">
                <div class="site-menu">
                    <div class="close-btn"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/close.svg" alt=""></div>
                    <div class="row mr-0 pb-lg-2 pb-xl-0">
                        <div class="col-xl-1 col-lg-2 col-sm-2 pr-lg-0">
                            <div class="city city--placeholder"><span><?=$city['name']?></span></div>
                        </div>

                        <div class="menu-title">Меню</div>

                        <div class="col-xl-8 col-lg-10 col-sm-10 pl-0 pr-lg-0 pl-lg-4">
                            <nav class="menu-list">
                                <?$APPLICATION->IncludeComponent("bitrix:menu", "top_menu", Array(
                                    "ALLOW_MULTI_SELECT" => "N",    // Разрешить несколько активных пунктов одновременно
                                    "CHILD_MENU_TYPE" => "left",    // Тип меню для остальных уровней
                                    "DELAY" => "N", // Откладывать выполнение шаблона меню
                                    "MAX_LEVEL" => "1", // Уровень вложенности меню
                                    "MENU_CACHE_GET_VARS" => array( // Значимые переменные запроса
                                        0 => "",
                                    ),
                                    "MENU_CACHE_TIME" => "3600",    // Время кеширования (сек.)
                                    "MENU_CACHE_TYPE" => "N",   // Тип кеширования
                                    "MENU_CACHE_USE_GROUPS" => "Y", // Учитывать права доступа
                                    "ROOT_MENU_TYPE" => "top",  // Тип меню для первого уровня
                                    "USE_EXT" => "N",   // Подключать файлы с именами вида .тип_меню.menu_ext.php
                                ),
                                    false
                                );?>
                            </nav>

                            <nav class="menu-list">
                                <ul class="list-unstyled list-inline mb-0">
                                    <li class="list-inline-item calculator-item">
                                        <a href="/calculator/">Онлайн калькулятор</a>
                                    </li>

                                    <!-- <li class="list-inline-item gallery-item">
                                        <a href="#">Галерея</a>
                                    </li> -->

                                    <li class="list-inline-item visualization-item">
                                        <a href="/3d/">3D визулизация</a>
                                    </li>

                                    <li class="list-inline-item brands-item">
                                        <a href="/proizvoditeli/">Производители</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <div class="col-xl-3 cub d-sm-none d-xl-block">
                            <div class="contact-us-btn">Связаться с нами</div>
                            <ul class="nav-feedback" id="nav-feedback1">
                                <li class="nav-item na-raschet">
                                    <a data-fancybox data-src="#zamer-application" href="javascript:;">
                                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" alt="">
                                        <span>Отправить заявку на расчет</span>
                                    </a>
                                </li>

                                <li class="nav-item callback-enquiry">
                                    <a data-fancybox data-src="#callback-enquiry" href="javascript:;">
                                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" alt="">
                                        <span>Заказать звонок</span>
                                    </a>
                                </li>

                                <li class="nav-item na-raschet">
                                    <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" alt="">
                                        <span>Стать поставщиком</span>
                                    </a>
                                </li>

                                <li class="nav-item na-raschet">
                                    <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" alt="">
                                        <span>Оставить отзыв</span>
                                    </a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row justify-content-between justify-content-sm-start mobile-top-row">
            <!-- <div class="col-xl-1 col-lg-1 col-auto logo pr-sm-2 pt-lg-5 pt-xl-0 d-sm-none d-xl-block">
            </div> -->
            <a class="logo web" href="/"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/logo_new.svg" width="87" alt=""></a>

            <div class="col-12 d-sm-none pl-0 d-flex justify-content-between align-items-center pt-3 right">
                <a class="logo" href="/"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/logo_new.svg" width="87" alt=""></a>
                <div class="call-us-mob rounded">
                    <a class="d-none h-100" href="tel:<?=$city['phonelink']?>"></a>
                    <a class="d-block text-phone phone_podmena" href="tel:<?=$city['phonelink']?>"><?=$city['phone']?></a>
                        <div class="city city--placeholder">
                            <span><?=$city['name']?></span>
                            <!--
                            <div class="dropdown" style="text-align: center; padding-top: 15px;">
                                    <span class="dropbtn"><?=$city['name']?></span>
                                    <?
                                    $sites = array(
                                        'https://tulpar-trade.ru' => 'Казань',
                                        'https://izhevsk.tulpar-trade.ru/' => 'Ижевск',
                                        'https://chelny.tulpar-trade.ru/' => 'Набережные Челны'
                                    );
                                    ?>
                                    <div class="dropdown-content" style="left:0;">
                                    <?foreach ($sites as $url => $stitle): if ($city['name']!=$stitle):?>
                                        <li><a href="<?=$url?>"><?=$stitle?></a></li>
                                    <?endif; endforeach;?>
                                    </div>
                             </div>
                             -->
                        </div>

                </div>

                <div class="cart-compare in-header d-flex pt-0">
                    <div class="compare mr-2"><a href="/personal/compare.php"></a></div>
                    <div class="bookmarked mr-2"><a href="/wishlist/"></a></div>
                    <div class="basket"><a href="/basket/"></a></div>
                    <div class="menu-btn"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/burger-men.svg" alt=""></div>
                </div>
            </div>

            <div class="col-xl-11 col-lg-12 pr-0 d-sm-block d-none">


                <div class="header-info d-sm-none d-xl-flex">
                    <div class="info-item address">
                        <div class="info-name">Адрес</div>
                        <div class="info-text"><?=$city['address']?></div>
                    </div>

                    <div class="info-item">
                        <div class="info-name">Телефон</div>
                        <div class="info-text phone_podmena">
                            <div id="phone_number"><?=$city['phone']?></div>
                            <?/*<div class="show-full-phone"><span>Показать телефон</span></div>*/?>
							</div>
                    </div>

                    <div class="info-item working-hours">
                        <div class="info-name">Режим работы</div>
                        <div class="info-text"><?=$city['time']?></div>
                    </div>

                    <div class="info-item fn-btn calculator">
                        <a href="/calculator/">Онлайн <br> калькулятор</a>
                    </div>

                    <!-- <div class="info-item fn-btn gallery">
                        <a href="#">Галерея</a>
                    </div> -->

                    <div class="info-item fn-btn visualization">
                        <a href="/3d/">3D визуализация</a>
                    </div>

                    <div class="info-item fn-btn brands">
                        <a href="/proizvoditeli/">Производители</a>
                    </div>
                </div>

                <div class="header-info d-lg-flex d-xl-none mb-lg-3 mb-xl-0">
                    <div class="info-item logo-img">
                        <a href="/"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/logo_new.svg" width="60" alt=""></a>
                    </div>

                    <div class="info-item address">
                        <div class="info-name">Адрес</div>
                        <div class="info-text"><?=$city['address']?></div>
                    </div>

                    <div class="info-item d-sm-none d-md-block">
                        <div class="info-name">Телефон</div>
                        <div class="info-text phone_podmena"><?=$city['phone']?></div>
                    </div>

                    <div class="info-item working-hours">
                        <div class="info-name">Режим работы</div>
                        <div class="info-text"><?=$city['time']?></div>
                    </div>

                    <div class="cub position-relative">
                        <div class="contact-us-btn d-sm-none d-lg-block">Связаться с нами</div>
                        <ul class="nav-feedback" id="nav-feedback1">
                            <li class="nav-item na-raschet">
                                <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                    <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/calc.svg" alt="">
                                    <span>Отправить заявку на расчет</span>
                                </a>
                            </li>

                            <li class="nav-item na-raschet">
                                <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                    <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/call.svg" alt="">
                                    <span>Заказать звонок</span>
                                </a>
                            </li>

                            <li class="nav-item na-raschet">
                                <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                    <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/post.svg" alt="">
                                    <span>Стать поставщиком</span>
                                </a>
                            </li>

                            <li class="nav-item na-raschet">
                                <a data-fancybox data-src="#hidden-content" href="javascript:;">
                                    <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/icomoon/comment.svg" alt="">
                                    <span>Оставить отзыв</span>
                                </a>
                            </li>
                        </ul>
                    </div>

                    <div class="call-us-mob rounded mr-4 d-sm-block d-lg-none"><a class="d-block h-100" href="tel:<?=$city['phonelink']?>"></a></div>
                </div>

                <div class="header-info d-lg-flex d-xl-none for-lg">
                    <div class="info-item fn-btn calculator">
                        <a href="/calculator/">Онлайн <br> калькулятор</a>
                    </div>

                    <!-- <div class="info-item fn-btn gallery">
                        <a href="#">Галерея</a>
                    </div> -->

                    <div class="info-item fn-btn visualization">
                        <a href="/3d/">3D визуализация</a>
                    </div>

                    <div class="info-item fn-btn brands">
                        <a href="/proizvoditeli/">Производители</a>
                    </div>

                    <div class="cart-compare in-header d-flex">
                        <div class="compare"><a href="/personal/compare.php"></a></div>
                        <div class="bookmarked"><a href="/wishlist/"></a></div>
                        <div class="basket"><a href="/basket/"></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div style="display: none" class="region_choose_wrapper">
<? $APPLICATION->IncludeComponent("sotbit:regions.choose", "tulpar_city_choose", Array(

),
    false
);
?>
</div>
<?/* Если мы не находимся на главной */?>
<? if ($APPLICATION->GetCurPage(false) !== '/'): ?>
    <?    $isHome = "not-home";?>
<? endif;?>

<div id="m-nav">
    <div class="m-nav_close"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/close.svg" alt=""></div>
    <div class="m-nav_back"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/back.svg" alt=""></div>
    <div class="m-nav_container">
        <div class="container__inner">
            <!--noindex-->
            <ul class="m-nav-mobile-list">
                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"></li>

                <li class="m-nav-mobile-list__item icon_catalog c-sub-screen">
                    <a href="/catalog/" class="m-nav-mobile-list__link has-icon has_sublist">Каталог товаров <span></span></a>
                    <div class="c-sub-screen__sub">
                        <ul class="m-nav-mobile-list">
                            <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header"><span>Каталог</span></li>

                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/kirpich/" class="m-nav-mobile-list__link has_sublist">Кирпич <span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Кирпич</span></li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/kirpich/" class="m-nav-mobile-list__link">Все кирпичи</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/oblitsovochnyy/" class="m-nav-mobile-list__link has_sublist">Облицовочный<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Облицовочный кирпич</span></li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochnyy/" class="m-nav-mobile-list__link">Все облицовочные</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item group-name">По производителю</li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-koshchakovskiy/" class="m-nav-mobile-list__link">Кощаковский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-arskiy/" class="m-nav-mobile-list__link">Арский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-kirovo-chepetskiy/" class="m-nav-mobile-list__link">Кирово-чепецкий</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-izhevskiy/" class="m-nav-mobile-list__link">Ижевский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-saranskiy/" class="m-nav-mobile-list__link">Саранский</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-zsk-chelny/" class="m-nav-mobile-list__link">ЗСК Челны</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-meakir/" class="m-nav-mobile-list__link">Меакир</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-revdinskiy/" class="m-nav-mobile-list__link">Ревдинский</a>
                                                    </li>

                                                    <!--<li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-mstera/" class="m-nav-mobile-list__link">MSTERA</a>
                                                    </li>-->

                                                    <li class="m-nav-mobile-list__item group-name">По цвету</li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-krasnyy/" class="m-nav-mobile-list__link">красный</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/litcevoy-oblitcovochniy-bavarskaya-kladka/" class="m-nav-mobile-list__link">баварская кладка</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-zheltyy/" class="m-nav-mobile-list__link">желтый</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-korichnevyy/" class="m-nav-mobile-list__link">коричневый</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-seryy/" class="m-nav-mobile-list__link">серый</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/kirpich/" class="m-nav-mobile-list__link">белый</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-flesh-obzhig/" class="m-nav-mobile-list__link">флеш-обжиг</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-oranzhevyy/" class="m-nav-mobile-list__link">оранжевый</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-rozovyy/" class="m-nav-mobile-list__link">розовый</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/oblitsovochniy-fioletovyy/" class="m-nav-mobile-list__link">фиолетовый</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/keramicheskiy-kirpich/" class="m-nav-mobile-list__link has_sublist">Керамический<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Керамический кирпич</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kirpich/" class="m-nav-mobile-list__link">Все керамические</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/keramicheskiy-oblitsovochnyy/" class="m-nav-mobile-list__link has_sublist">Облицовочный<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Облицовочный кирпич</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-oblitsovochnyy/" class="m-nav-mobile-list__link">Все облицовочные</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item group-name">По производителю</li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-koshchakovskiy/" class="m-nav-mobile-list__link">Кощаковский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-arskiy/" class="m-nav-mobile-list__link">Арский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-kirovo-chepetskiy/" class="m-nav-mobile-list__link">Кирово-чепецкий</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-izhevskiy/" class="m-nav-mobile-list__link">Ижевский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-saranskiy/" class="m-nav-mobile-list__link">Саранский</a>
                                                                </li>

                                                                 <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-meakir/" class="m-nav-mobile-list__link">Меакир</a>
                                                                </li>

                                                                 <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-revdinskiy/" class="m-nav-mobile-list__link">Ревдинский</a>
                                                                </li>

                                                                <!--<li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-mstera/" class="m-nav-mobile-list__link">MSTERA</a>
                                                                </li>-->

                                                                <li class="m-nav-mobile-list__item group-name">По цвету</li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-krasnyy/" class="m-nav-mobile-list__link">красный</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-oblitcovochniy-bavarskaya-kladka/" class="m-nav-mobile-list__link">баварская кладка</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-zheltyy/" class="m-nav-mobile-list__link">желтый</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-korichnevyy/" class="m-nav-mobile-list__link">коричневый</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-seryy/" class="m-nav-mobile-list__link">серый</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/kirpich/" class="m-nav-mobile-list__link">белый</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/keramicheskiy-flesh-obzhig/" class="m-nav-mobile-list__link">флеш-обжиг</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/ryadovoy/" class="m-nav-mobile-list__link has_sublist">Рядовой<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Рядовой кирпич</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy/" class="m-nav-mobile-list__link">Все рядовые</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item group-name">По производителю</li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-votkinskiy/" class="m-nav-mobile-list__link">Воткинский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-chaykovskiy/" class="m-nav-mobile-list__link">Чайковский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-shelangovskiy/" class="m-nav-mobile-list__link">Шеланговский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-mamadyshskiy/" class="m-nav-mobile-list__link">Мамадышский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-kazanskiy/" class="m-nav-mobile-list__link">Казанский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-kurkachinskiy/" class="m-nav-mobile-list__link">Куркачинский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-ketra/" class="m-nav-mobile-list__link">Кетра</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-chelninskiy/" class="m-nav-mobile-list__link">Челнинский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/kirpich/" class="m-nav-mobile-list__link">Кощаковский</a>
                                                                </li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-cheboksarskiy/" class="m-nav-mobile-list__link">Чебоксарский</a>
                                                                </li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-arskiy/" class="m-nav-mobile-list__link">Арский</a>
                                                                </li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-mstera/" class="m-nav-mobile-list__link">MSTERA</a>
                                                                </li>

                                                                 <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-meakir/" class="m-nav-mobile-list__link">Меакир</a>
                                                                </li>

                                                                 <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-dyurtyulinskiy/" class="m-nav-mobile-list__link">Дюртюлинский</a>
                                                                </li>

                                                                 <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-revdinskiy/" class="m-nav-mobile-list__link">Ревдинский</a>
                                                                </li>

                                                                 <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-inkeram-tkz/" class="m-nav-mobile-list__link">Тольяттинский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-yadrinskiy/" class="m-nav-mobile-list__link">Ядринский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-morgaushenskiy/" class="m-nav-mobile-list__link">Моргаушенский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item group-name">По цвету</li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ryadovoy-krasnyy/" class="m-nav-mobile-list__link">красный</a>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/silikatnyy/" class="m-nav-mobile-list__link has_sublist">Силикатный <span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Силикатный кирпич</span></li>
                                                    <li class="m-nav-mobile-list__item group-name">По производителю</li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/silikatnyy-po-proizvoditelyu-kzssm/" class="m-nav-mobile-list__link">КЗССМ</a>
                                                        <a href="/catalog/silikatnyy-po-proizvoditelyu-zsk-chelny/" class="m-nav-mobile-list__link">ЗСК Челны</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/stroitelnyy-kirpich/" class="m-nav-mobile-list__link has_sublist">Строительный <span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Строительный кирпич</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/stroitelnyy-kirpich/" class="m-nav-mobile-list__link">Все строительные</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/stroitelnyy-ryadovoy/" class="m-nav-mobile-list__link has_sublist">Рядовой<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Рядовой кирпич</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy/" class="m-nav-mobile-list__link">Все рядовые</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item group-name">По производителю</li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-votkinskiy/" class="m-nav-mobile-list__link">Воткинский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-chaykovskiy/" class="m-nav-mobile-list__link">Чайковский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-shelangovskiy/" class="m-nav-mobile-list__link">Шеланговский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-mamadyshskiy/" class="m-nav-mobile-list__link">Мамадышский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-kazanskiy/" class="m-nav-mobile-list__link">Казанский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-revdinskiy/" class="m-nav-mobile-list__link">Ревдинский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-kurkachinskiy/" class="m-nav-mobile-list__link">Куркачинский</a>
                                                                </li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-cheboksarskiy/" class="m-nav-mobile-list__link">Чебоксарский</a>
                                                                </li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-arskiy/" class="m-nav-mobile-list__link">Арский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-ketra/" class="m-nav-mobile-list__link">Кетра</a>
                                                                </li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-mstera/" class="m-nav-mobile-list__link">MSTERA</a>
                                                                </li>

                                                                  <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-meakir/" class="m-nav-mobile-list__link">Меакиир</a>
                                                                </li>


                                                                  <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-dyurtyulinskiy/" class="m-nav-mobile-list__link">Дюртюлинский</a>
                                                                </li>

                                                                  <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-inkeram-tkz/" class="m-nav-mobile-list__link">Тольяттинский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-yadrinskiy/" class="m-nav-mobile-list__link">Ядринский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-ryadovoy-zsk-chelny/" class="m-nav-mobile-list__link">ЗСК Челны</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/stroitelnyy-tsokolnyy/" class="m-nav-mobile-list__link has_sublist">Цокольный<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Цокольный кирпич</span></li>
                                                                <li class="m-nav-mobile-list__item group-name">По производителю</li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-tsokolnyy/" class="m-nav-mobile-list__link">Все цокольные</a>
                                                                </li>


                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-tsokolnyy-alekseevskiy/" class="m-nav-mobile-list__link">Алексеевский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-tsokolnyy-votkinskiy/" class="m-nav-mobile-list__link">Воткинский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-tsokolnyy-chaykovskiy/" class="m-nav-mobile-list__link">Чайковский</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-tsokolnyy-kazanskiy/" class="m-nav-mobile-list__link">Казанский</a>
                                                                </li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/stroitelnyy-tsokolnyy-cheboksarskiy/" class="m-nav-mobile-list__link">Чебоксарский</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/figurnyy-kirpich/" class="m-nav-mobile-list__link has_sublist">Фигурный <span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Фигурный кирпич</span></li>
                                                    <li class="m-nav-mobile-list__item group-name">По производителю</li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/figurnyy_kirpich_arskiy/" class="m-nav-mobile-list__link">Арский</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/shamotnyy_kirpich/" class="m-nav-mobile-list__link has_sublist">Шамотный <span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Шамотный кирпич</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/shamotnyy_kirpich/" class="m-nav-mobile-list__link">Все шамотные</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/shb-5/" class="m-nav-mobile-list__link">ШБ-5</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/shb-8/" class="m-nav-mobile-list__link">ШБ-8</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/klinkernyy/" class="m-nav-mobile-list__link has_sublist">Клинкерный <span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Клинкерный кирпич</span></li>
                                                    <li class="m-nav-mobile-list__item group-name">По производителю</li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klinkernyy_saranskiy/" class="m-nav-mobile-list__link">Саранский</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item group-name">По цвету</li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klinkernyy-krasnyy/" class="m-nav-mobile-list__link">красный</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klinkernyy-korichnevyy/" class="m-nav-mobile-list__link">коричневый</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klinkernyy-flesh-obzhig/" class="m-nav-mobile-list__link">флеш-обжиг</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/kirpich/" class="m-nav-mobile-list__link">белый</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/dekorativnyy/" class="m-nav-mobile-list__link has_sublist">Декоративный <span></span></a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/keramicheskie-bloki/" class="m-nav-mobile-list__link has_sublist">Керамические блоки <span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Керамические блоки</span></li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/keramicheskie-bloki/" class="m-nav-mobile-list__link">Все керамические блоки</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/krupnoformatnyy-keramicheskiy-blok/" class="m-nav-mobile-list__link has_sublist">Крупноформатный керамический блок<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Крупноформатный кер. блок</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnyy-keramicheskiy-blok/" class="m-nav-mobile-list__link">Все крупноформатные блоки</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnyy-amstron-porikam/" class="m-nav-mobile-list__link">Амстрон (порикам)<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnyy-ketra/" class="m-nav-mobile-list__link">Кетра<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnyy-vinerberger-poroterm/" class="m-nav-mobile-list__link">Винербергер (поротерм)<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnyy-revdinskiy/" class="m-nav-mobile-list__link"><span>Ревдинский</span></a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </li>
                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/peregorodochnyy-keramicheskiy-blok/" class="m-nav-mobile-list__link has_sublist">Перегородочный керамический блок<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Перегородочный кер. блок</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/peregorodochnyy-keramicheskiy-blok/" class="m-nav-mobile-list__link">Все перегородочные блоки</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/peregorodochnyy-amstron-porikam/" class="m-nav-mobile-list__link">Амстрон (порикам)<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/peregorodochnyy-ketra/" class="m-nav-mobile-list__link has_sublist">Кетра<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/peregorodochnyy-vinerberger-poroterm/" class="m-nav-mobile-list__link">Винербергер (поротерм)<span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/keramicheskiy-kamen/" class="m-nav-mobile-list__link has_sublist">Керамический камень 2.1НФ<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Керамический камень 2.1НФ</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen/" class="m-nav-mobile-list__link">Все керамические камни</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-klyuchishchenskiy/" class="m-nav-mobile-list__link">Ключищенский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-mamadyshskiy/" class="m-nav-mobile-list__link">Мамадышский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-ketra/" class="m-nav-mobile-list__link">Кетра</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-vinerberger-poroterm/" class="m-nav-mobile-list__link">Винербергер (поротерм)</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-shelangovskiy/" class="m-nav-mobile-list__link">Шеланговский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/kamastroyindustriya/" class="m-nav-mobile-list__link">Челнинский</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-arskiy/" class="m-nav-mobile-list__link">Арский</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-mstera/" class="m-nav-mobile-list__link">MSTERA</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-meakir/" class="m-nav-mobile-list__link">Меакир</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-inkeram-tkz/" class="m-nav-mobile-list__link">Тольяттинский</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-revdinskiy/" class="m-nav-mobile-list__link">Ревдинский</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/keramicheskiy-kamen-yadrinskiy/" class="m-nav-mobile-list__link">Ядринский</a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/gazobetonnye-bloki-m/" class="m-nav-mobile-list__link has_sublist">Газобетонные блоки <span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Газобетонные блоки</span></li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/gazobetonnye-bloki-m/" class="m-nav-mobile-list__link">Все газобетонные блоки</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/krupnoformatnye-gazobetonnye-bloki/" class="m-nav-mobile-list__link has_sublist">Крупноформатные газобетонные блоки<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Крупноформатные блоки</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnye-gazobetonnye-bloki/" class="m-nav-mobile-list__link">Все крупноформатные блоки</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnye-bikton_/" class="m-nav-mobile-list__link">Биктон</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/zyab2/" class="m-nav-mobile-list__link">ЗЯБ</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnye-uniblock/" class="m-nav-mobile-list__link">Uniblock</a>
                                                    </li>

                                                 <!--   <li class="m-nav-mobile-list__item">-->
                                                 <!--  <a href="/catalog/krupnoformatnye-kzssm/" class="m-nav-mobile-list__link">КЗССМ</a>-->
                                                 <!--    </li>-->
                                                        <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnye-teplon/" class="m-nav-mobile-list__link">Теплон</a>
                                                    </li>
                                                        <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnye-kottedzh/" class="m-nav-mobile-list__link">Коттедж</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnye-gras/" class="m-nav-mobile-list__link">ГРАС</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnye-zyab-izhevsk/" class="m-nav-mobile-list__link">ЗЯБ (Ижевск)</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krupnoformatnye-build-stone/" class="m-nav-mobile-list__link">Build stone</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/peregorodochnye-gazobetonnye-bloki/" class="m-nav-mobile-list__link has_sublist">Перегородочные газобетонные блоки<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Перегородочные блоки</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/peregorodochnye-gazobetonnye-bloki/" class="m-nav-mobile-list__link">Все перегородочные блоки</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                      <a href="/catalog/peregorodochnye-bikton_/" class="m-nav-mobile-list__link">Биктон</a>
                                                        <a href="/catalog/zyab/" class="m-nav-mobile-list__link">ЗЯБ</a>
                                                        <a href="/catalog/peregorodochnye-uniblock/" class="m-nav-mobile-list__link">Uniblock</a>
                                                        <a href="/catalog/peregorodochnye-teplon/" class="m-nav-mobile-list__link">Теплон</a>
                                                        <a href="/catalog/peregorodochnye-kottedzh/" class="m-nav-mobile-list__link">Коттедж</a>
                                                        <a href="/catalog/peregorodochnye-gras/" class="m-nav-mobile-list__link">ГРАС</a>
                                                        <a href="/catalog/peregorodochnye-zyab-izhevsk/" class="m-nav-mobile-list__link">ЗЯБ (Ижевск)</a>
                                                        <a href="/catalog/peregorodochnye-build-stone/" class="m-nav-mobile-list__link">Build stone</a>

                                                </ul>
                                            </div>
                                        </li>
                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/u-obraznye-gazobetonnye-bloki/" class="m-nav-mobile-list__link has_sublist">U-образные газобетонные блоки<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>U-образные блоки</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/u-obraznye-gazobetonnye-bloki/" class="m-nav-mobile-list__link">Все U-образные газобетонные блоки</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/u-obraznye-gazobetonnye-bloki-teplon/" class="m-nav-mobile-list__link">Теплон</a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/u-obraznye-gazobetonnye-bloki-build-stone/" class="m-nav-mobile-list__link">Build stone</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>


		             <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/plitka-bloki-keramzitobetonnye/" class="m-nav-mobile-list__link has_sublist">Керамзитобетонные блоки<span></span></a>
                            </li>

                             <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/oblitsovochnye-materialy/" class="m-nav-mobile-list__link has_sublist">Облицовочные материалы<span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Облицовочные материалы</span></li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/oblitsovochnye-materialy/" class="m-nav-mobile-list__link">Все облицовочные материалы</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/plitka-oblitsovochnyy-kamen/" class="m-nav-mobile-list__link">Декоративный камень</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/plitka-dekorativnyy-kirpich/" class="m-nav-mobile-list__link">Декоративный кирпич</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/obl_mat_metalicheskiy-sayding/" class="m-nav-mobile-list__link">Металлический сайдинг</a>
                                        </li>
										<li class="m-nav-mobile-list__item">
                                            <a href="/catalog/sayding/" class="m-nav-mobile-list__link">Виниловый сайдинг</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
							
							
							
							
							
							
		                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/blagoustroystva/" class="m-nav-mobile-list__link has_sublist">Благоустройства<span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Благоустройства</span></li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/blagoustroystva/" class="m-nav-mobile-list__link">Все благоустройства</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/plitka-bruschatka/" class="m-nav-mobile-list__link">Брусчатка</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/plitka-trotuarnye-plity/" class="m-nav-mobile-list__link">Тротуарная плитка</a>
                                        </li>
										
										<li class="m-nav-mobile-list__item">
                                            <a href="/catalog/plitka-bordyury/" class="m-nav-mobile-list__link">Бордюры (поребрики)</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/plitka-poverkhnostnye-sistemy-vodootvody/" class="m-nav-mobile-list__link">Дренажные решетки (водоотводы)</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>


                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/sukhie-stroitelnye-smesi/" class="m-nav-mobile-list__link has_sublist">Сухие строительные смеси<span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Сухие строительные смеси</span></li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/sukhie-stroitelnye-smesi/" class="m-nav-mobile-list__link">Все строительные смеси</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/kladochnyy-rastvor/" class="m-nav-mobile-list__link">Кладочный раствор</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/tsvetnoy-rastvor/" class="m-nav-mobile-list__link">Цветной раствор</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/teploizolyatsionnyy-rastvor/" class="m-nav-mobile-list__link">Теплоизоляционный раствор</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/stroitelnaya-setka/" class="m-nav-mobile-list__link has_sublist">Строительная сетка<span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Строительная сетка</span></li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/sukhie-stroitelnye-smesi/" class="m-nav-mobile-list__link">Все строительные сетки</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/bazaltovaya-setka/" class="m-nav-mobile-list__link">Базальтовая сетка</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/stekloplastikovaya-setka/" class="m-nav-mobile-list__link">Стеклопластиковая сетка</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/krovelnye-materialy/" class="m-nav-mobile-list__link has_sublist">Кровля<span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Кровля</span></li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/metallocherepitsa/" class="m-nav-mobile-list__link has_sublist">Металлочерепица<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Металлочерепица</span></li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-monterey/" class="m-nav-mobile-list__link">Монтерей<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-supermonterey/" class="m-nav-mobile-list__link">Супермонтерей<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-maksi/" class="m-nav-mobile-list__link">Макси<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-monterossa/" class="m-nav-mobile-list__link">Монтеросса<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-tromantana/" class="m-nav-mobile-list__link">Трамонтана<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-montekristo/" class="m-nav-mobile-list__link">Монтекристо<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-modern/" class="m-nav-mobile-list__link">Modern<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-classic/" class="m-nav-mobile-list__link">Classic<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-kredo/" class="m-nav-mobile-list__link">Kredo<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-kamea/" class="m-nav-mobile-list__link">Kamea<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-kvinta-uno/" class="m-nav-mobile-list__link">Kvinta Uno<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/metallocherepitsa-kvinta-plus/" class="m-nav-mobile-list__link">Kvinta plus<span></span></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/profnastil/" class="m-nav-mobile-list__link has_sublist">Профнастил<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Профнастил</span></li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-s-8/" class="m-nav-mobile-list__link">С-8<span></span></a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-s-8-figurnyy/" class="m-nav-mobile-list__link">С-8 фигурный<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-s-10/" class="m-nav-mobile-list__link">С-10<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-s-10-figurnyy/" class="m-nav-mobile-list__link">С-10 фигурный<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-s-20/" class="m-nav-mobile-list__link">С-20<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-с-21/" class="m-nav-mobile-list__link">С-21<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-s-44/" class="m-nav-mobile-list__link">С-44<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-mp-10/" class="m-nav-mobile-list__link">МП-10<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-mp-18/" class="m-nav-mobile-list__link">МП-18<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-mp-20/" class="m-nav-mobile-list__link">МП-20<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-mp-35/" class="m-nav-mobile-list__link">МП-35<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-n-60/" class="m-nav-mobile-list__link">Н-60<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-n-75/" class="m-nav-mobile-list__link">Н-75<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-n-114/" class="m-nav-mobile-list__link">Н-114<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/profnastil-ns-35/" class="m-nav-mobile-list__link">НС-35<span></span></a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </li>
                                        
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/standartnye-elementy-otdelki/" class="m-nav-mobile-list__link">Стандартные элементы отделки<span></span></a>
                                        </li>
                                        
                                                                             <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/sofity/" class="m-nav-mobile-list__link">Софиты<span></span></a>
                                        <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>По производителю</span></li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/sofity-metall-profil/" class="m-nav-mobile-list__link">Металл Профиль<span></span></a>
                                                        <a href="/catalog/sofity-grand-line/" class="m-nav-mobile-list__link">Grand Line<span></span></a>
                                                    </li>
                                                </ul>
                                         </div>
                                        </li>
                                        
                                        
                                        
                                        
                                         <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/falts/" class="m-nav-mobile-list__link has_sublist">Фальц<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>По виду</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klikfalts-line/" class="m-nav-mobile-list__link">Кликфальц Line<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klikfalts-pro-line/" class="m-nav-mobile-list__link">Кликфальц Pro Line<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klikfalts-pro-gofr/" class="m-nav-mobile-list__link">Кликфальц Pro Gofr<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klikfalts-pro/" class="m-nav-mobile-list__link">Кликфальц Pro<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/klikfalts-mini/" class="m-nav-mobile-list__link">Кликфальц Mini<span></span></a>
                                                    </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/dvoynoy-stoyachiy-falts-line/" class="m-nav-mobile-list__link">Двойной стоячий фальц Line<span></span></a>
                                                    </li>

                                                </ul>
                                            </div>
                                        </li>
                                        
    
                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/sayding/" class="m-nav-mobile-list__link has_sublist">Сайдинг<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>По виду</span></li>
                                                        
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/sayding-korabelnaya-doska/" class="m-nav-mobile-list__link">Корабельная доска<span></span></a>
                                                        </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/sayding-vertikal/" class="m-nav-mobile-list__link">Вертикаль<span></span></a>
                                                        </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/sayding-ekobrus-gofr/" class="m-nav-mobile-list__link">ЭкоБрус/Gofr<span></span></a>
                                                        </li>
                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/sayding-blok-khaus/" class="m-nav-mobile-list__link">Блок-хаус<span></span></a>
                                                        </li>

                                                </ul>
                                            </div>
                                        </li>
                                        
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/krepezhnye-izdeliya/" class="m-nav-mobile-list__link">Крепежные изделия<span></span></a>
                                        </li>
                                        
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/uplotniteli/" class="m-nav-mobile-list__link">Уплотнители<span></span></a>
                                        </li>
                                        
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/krovelnaya-ventilyatsiya/" class="m-nav-mobile-list__link">Кровельная вентиляция<span></span></a>
                                        </li>
                                        
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/flyugery-i-ukazateli-vetra/" class="m-nav-mobile-list__link">Флюгеры и указатели ветра<span></span></a>
                                        </li>
                                        
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/germetiziruyushchie-materialy/" class="m-nav-mobile-list__link">Герметизирующие материалы<span></span></a>
                                        </li>
                                        
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/stroitelnaya-khimiya/" class="m-nav-mobile-list__link">Строительная химия<span></span></a>
                                        </li>
                                        
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/dymniki/" class="m-nav-mobile-list__link">Дымники<span></span></a>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/dobornye-elementy/" class="m-nav-mobile-list__link has_sublist">Доборные элементы<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Доборные элементы</span></li>
                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/germetiziruyushchie-materialy/" class="m-nav-mobile-list__link has_sublist">Герметизирующие материалы<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Герметизирующие материалы</span></li>
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/soedinitelnaya-lenta/" class="m-nav-mobile-list__link">Соединительная лента<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/silikon/" class="m-nav-mobile-list__link">Силикон<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/germetiziruyushchie-materialy/" class="m-nav-mobile-list__link">Герметизирующая лента<span></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/flyugery-i-ukazateli-vetra/" class="m-nav-mobile-list__link has_sublist">Флюгеры и указатели ветра<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Флюгеры</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/flyugery/" class="m-nav-mobile-list__link">Флюгеры<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/ukazateli-vetra/" class="m-nav-mobile-list__link">Указатели ветра<span></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/stroitelnaya-khimiya/" class="m-nav-mobile-list__link has_sublist">Строительная химия<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Строительная химия</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/remontnaya-emal/" class="m-nav-mobile-list__link">Ремонтная эмаль<span></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/standartnye-elementy-otdelki/" class="m-nav-mobile-list__link has_sublist">Стандартные элементы отделки<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Стандартные элементы отделки</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/planka/" class="m-nav-mobile-list__link">Планка<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/zaglushka/" class="m-nav-mobile-list__link">Заглушка<span></span></a>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/uplotniteli/" class="m-nav-mobile-list__link has_sublist">Уплотнители<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Уплотнители</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/uplotnitel/" class="m-nav-mobile-list__link">Уплотнитель<span></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/krepezhnye-izdeliya/" class="m-nav-mobile-list__link has_sublist">Крепежные изделия<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Крепежные изделия</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/samorezy/" class="m-nav-mobile-list__link">Саморезы<span></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/krovelnaya-ventilyatsiya/" class="m-nav-mobile-list__link has_sublist">Кровельная вентиляция<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Кровельная вентиляция</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/antennyy-vykhod/" class="m-nav-mobile-list__link">Антенный выход<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/manzheta-krovelnaya/" class="m-nav-mobile-list__link">Манжета кровельная<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/vykhod-ventilyatsii/" class="m-nav-mobile-list__link">Выход вентиляции<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/vykhod-vytyazhki/" class="m-nav-mobile-list__link">Выход вытяжки<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/vykhod-universalnyy/" class="m-nav-mobile-list__link">Выход универсальный<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/gofrirovannaya-truba/" class="m-nav-mobile-list__link">Гофрированная труба<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/truba-izolirovannaya/" class="m-nav-mobile-list__link">Труба изолированная<span></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    
                                                                                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/dymniki/" class="m-nav-mobile-list__link has_sublist">Дымники<span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Дымники</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-dymnik/" class="m-nav-mobile-list__link">Дымник (флюгарка)<span></span></a>
                                                                </li>
                                                                
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-dymnik-dvoynoy/" class="m-nav-mobile-list__link">Дымник двойной<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-dymnik-s-zhalyuzi/" class="m-nav-mobile-list__link">Дымник с жалюзи<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-dymnik-dvoynoy-s-zhalyuzi/" class="m-nav-mobile-list__link">Дымник двойной с жалюзи<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-dymnik-s-setkoy/" class="m-nav-mobile-list__link">Дымник с сеткой<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-dymnik-dvoynoy-s-setkoy/" class="m-nav-mobile-list__link">Дымник двойной с сеткой<span></span></a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-dymnik-yelka/" class="m-nav-mobile-list__link">Дымник ёлка<span></span></a>
                                                                </li>
                                                                
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-kozhukh-na-trubu-pryamoy/" class="m-nav-mobile-list__link">Кожух на трубу прямой<span></span></a>
                                                                </li>
                                                                
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-kozhukh-na-trubu-skatnyy/" class="m-nav-mobile-list__link">Кожух на трубу скатный<span></span></a>
                                                                </li>
                                                                
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-kolpak-na-stolb/" class="m-nav-mobile-list__link">Колпак на столб<span></span></a>
                                                                </li>
                                                                
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-kolpak-pod-fonar/" class="m-nav-mobile-list__link">Колпак под фонарь<span></span></a>
                                                                </li>
                                                                
                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/dymniki-kolpak-dvoynoy/" class="m-nav-mobile-list__link">Колпак двойной<span></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/elementy-bezopasnosti-krovli/" class="m-nav-mobile-list__link has_sublist">Элементы безопасности кровли<span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Элементы безопасности кровли</span></li>
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/snegozaderzhateli/" class="m-nav-mobile-list__link">Снегозадержатели</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/krovelno-stenovaya-lestnitsa/" class="m-nav-mobile-list__link has_sublist">Кровельно-стеновая лестница<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Кровельно-стеновая лестница</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/krovelnaya-lestnitsa/" class="m-nav-mobile-list__link">Кровельная лестница</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/kronshteyn/" class="m-nav-mobile-list__link">Кронштейн</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/elementy-bezopasnosti-krovli/" class="m-nav-mobile-list__link">Поручень</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/krovelnoe-ograzhdenie/" class="m-nav-mobile-list__link">Кровельное ограждение</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/perekhodnoy-mostik/" class="m-nav-mobile-list__link">Переходной мостик</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/vodostochnye-sistemy/" class="m-nav-mobile-list__link has_sublist">Водосточные системы</span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/kruglogo-secheniya-12590-grandsystem/" class="m-nav-mobile-list__link">Grandsystem - круглого сечения</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/kruglogo-secheniya-mp-prestizh/" class="m-nav-mobile-list__link">МП Престиж - круглого сечения</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/kruglogo-secheniya-mp-proekt-d185150/" class="m-nav-mobile-list__link">МП Проект - круглого сечения</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/pryamougolnogo-secheniya-modern-12076/" class="m-nav-mobile-list__link">МП Модерн - прямоугольного сечения</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/pryamougolnogo-secheniya-mp-byudzhet/" class="m-nav-mobile-list__link">МП Бюджет - прямоугольного сечения</a>
                                        </li>
                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/po-brendu-grand-line/" class="m-nav-mobile-list__link">Grand Line</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            
                          <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/teploizolyatsiya/" class="m-nav-mobile-list__link has_sublist">Теплоизоляция<span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Теплоизоляция</span></li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/mineralnaya-vata/" class="m-nav-mobile-list__link">Минеральная вата</a>
                                        </li>

                                        <li class="m-nav-mobile-list__item">
                                            <a href="/catalog/ekstrudirovannyy-penopolistirol/" class="m-nav-mobile-list__link">Экструдированный пенополистирол</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>

                            <li class="m-nav-mobile-list__item c-sub-screen">
                                <a href="/catalog/lestnitsy/" class="m-nav-mobile-list__link has_sublist">Лестницы <span></span></a>
                                <div class="c-sub-screen__sub">
                                    <ul class="m-nav-mobile-list">
                                        <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Лестницы</span></li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/mezhetazhnye-lestnitsy/" class="m-nav-mobile-list__link has_sublist">Межэтажные<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Межэтажные лестницы</span></li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/mezhetazhnye-lestnitsy-po-forme-pryamye/" class="m-nav-mobile-list__link has_sublist">Прямые <span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Прямые лестницы</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/pryamye-klassic/" class="m-nav-mobile-list__link">Классические</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/pryamye-gusinyy/" class="m-nav-mobile-list__link">Гусиный шаг</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/mezhetazhnye-lestnitsy-po-forme-g-obraznye/" class="m-nav-mobile-list__link has_sublist">Г-образные <span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Г-образные</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/g-obraznye-s-zabezhnymi-stupenyami/" class="m-nav-mobile-list__link">С забежными ступенями</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/g-obraznye-s-ploshchadkoy/" class="m-nav-mobile-list__link">С площадкой</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item c-sub-screen">
                                                        <a href="/catalog/mezhetazhnye-lestnitsy-po-forme-p-obraznye/" class="m-nav-mobile-list__link has_sublist">П-образные <span></span></a>
                                                        <div class="c-sub-screen__sub">
                                                            <ul class="m-nav-mobile-list">
                                                                <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>П-образные</span></li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/p-obraznye-s-zabezhnymi-stupenyami/" class="m-nav-mobile-list__link">С забежными ступенями</a>
                                                                </li>

                                                                <li class="m-nav-mobile-list__item">
                                                                    <a href="/catalog/p-obraznye-s-ploshchadkoy/" class="m-nav-mobile-list__link">С площадкой</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/mezhetazhnye-lestnitsy-po-forme-vintovye/" class="m-nav-mobile-list__link">Винтовые</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>

                                        <li class="m-nav-mobile-list__item c-sub-screen">
                                            <a href="/catalog/lestnitsy-cherdachnye/" class="m-nav-mobile-list__link has_sublist">Чердачные<span></span></a>
                                            <div class="c-sub-screen__sub">
                                                <ul class="m-nav-mobile-list">
                                                    <li class="m-nav-mobile-list__item m-nav-mobile-list__item_header m-nav-mobile-list__item_empty"><span>Чердачные лестницы</span></li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/lestnitsy-cherdachnye-po-materialu-derevyannye/" class="m-nav-mobile-list__link">Деревянные</a>
                                                    </li>

                                                    <li class="m-nav-mobile-list__item">
                                                        <a href="/catalog/lestnitsy-cherdachnye-po-materialu-metallicheskie/" class="m-nav-mobile-list__link">Металлические</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>








                        </ul>
                    </div>
                </li>

                <li class="m-nav-mobile-list__item icon_delivery">
                    <a href="/dostavka/" class="m-nav-mobile-list__link has-icon">Доставка</a>
                </li>

                <li class="m-nav-mobile-list__item icon_payment">
                    <a href="/oplata/" class="m-nav-mobile-list__link has-icon">Оплата</a>
                </li>

                <li class="m-nav-mobile-list__item icon_about">
                    <a href="/about/" class="m-nav-mobile-list__link has-icon">О компании</a>
                </li>

                <li class="m-nav-mobile-list__item icon_brands">
                    <a href="/proizvoditeli/" class="m-nav-mobile-list__link has-icon">Производители</a>
                </li>

                <li class="m-nav-mobile-list__item icon_blog">
                    <a href="/articles/" class="m-nav-mobile-list__link has-icon">Блог</a>
                </li>

                <li class="m-nav-mobile-list__item icon_contact">
                    <a href="/contacts/" class="m-nav-mobile-list__link has-icon">Контакты</a>
                </li>

                <li class="m-nav-mobile-list__item icon_calculator">
                    <a href="/calculator/" class="m-nav-mobile-list__link has-icon">Онлайн калькулятор</a>
                </li>

                <li class="m-nav-mobile-list__item icon_visualization">
                    <a href="/3d/" class="m-nav-mobile-list__link has-icon">3D визуализация</a>
                </li>

                <li class="m-nav-mobile-list__item phone-num">
                    <a href="tel:+<?=$city['phonelink']?>" class="m-nav-mobile-list__link has-icon phone_podmena"><?=$city['phone']?></a>
                </li>
            </ul>
            <!--/noindex-->
        </div>
    </div>
</div>

<div class="search-and-cart d-sm-none border-top border-bottom mb-0 <?=$isHome?>">
    <div class="search">
        <?$APPLICATION->IncludeComponent(
            "bitrix:search.title",
            "top-search",
            array(
                "CATEGORY_0" => array(
                    0 => "iblock_catalog",
                ),
                "CATEGORY_0_TITLE" => "Товары",
                "CATEGORY_0_iblock_catalog" => array(
                    0 => "4",
                ),
                "CHECK_DATES" => "Y",
                "CONTAINER_ID" => "title-search-mob",
                "INPUT_ID" => "title-search-input-mob",
                "NUM_CATEGORIES" => "1",
                "ORDER" => "rank",
                "PAGE" => "#SITE_DIR#search/index.php",
                "SHOW_INPUT" => "Y",
                "SHOW_OTHERS" => "N",
                "TOP_COUNT" => "5",
                "USE_LANGUAGE_GUESS" => "Y",
                "COMPONENT_TEMPLATE" => "top-search"
            ),
            false
        );?>
    </div>
</div>



<div class="catalog-and-banner">
    <div class="container p-sm-0 ">
        <div class="row">
            <div class="col-xl-3 col-lg-4 col-md-5 side-catalog  pl-0 ">
                <div class="side-catalog-wrapper not-home">
                    <div class="catalog-name rounded-top not-home">Каталог товаров</div>
                    <ul class="catalog-list list-unstyled m-0 d-none d-lg-block not-home">
                        <li class="kirpichi">
                            <a href="/catalog/kirpich/">Кирпич</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Кирпич</div>
                                <ul class="list-unstyled">
                                    <li><a href="/catalog/kirpich/">Все кирпичи</a></li>
                                    <li>
                                        <a href="/catalog/oblitsovochnyy/">Облицовочный</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По производителю</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/oblitsovochniy-koshchakovskiy/">Кощаковский</a></li>
                                                <li><a href="/catalog/oblitsovochniy-arskiy/">Арский (АСПК)</a></li>
                                                <li><a href="/catalog/oblitsovochniy-kerma/">Керма</a></li>
                                                <li><a href="/catalog/oblitsovochniy-belebeevskiy/">Белебеевский</a></li>
                                                <li><a href="/catalog/oblitsovochniy-kirovo-chepetskiy/">Кирово-Чепецкий (КС-Керамик)</a></li>
                                                <li><a href="/catalog/oblitsovochniy-alekseevskiy/">Алексеевский</a></li>
                                                <li><a href="/catalog/oblitsovochniy-klyuchishchenskiy/">Ключищенский (Клюкер)</a></li>
                                                <!--<li><a href="/catalog/oblitsovochniy-lsr/">ЛСР</a></li>-->
                                                <li><a href="/catalog/oblitsovochniy-chelninskiy/">Челнинский (Камастройиндустрия)</a></li>
                                                <li><a href="/catalog/oblitsovochniy-izhevskiy/">Ижевский (Альтаир)</a></li>
                                                <li><a href="/catalog/oblitsovochniy-saranskiy/">Саранский (Магма)</a></li>
                                                <li><a href="/catalog/oblitsovochniy-zsk-chelny/">ЗСК Челны</a></li>
                                                <li><a href="/catalog/oblitsovochniy-meakir/">Меакир</a></li>
                                                <li><a href="/catalog/oblitsovochniy-revdinskiy/">Ревдинский</a></li>
                                                <!--<li><a href="/catalog/oblitsovochniy-mstera/">MSTERA</a></li>-->
                                            </ul>

                                            <div class="submenu-separator mb-3"></div>
                                            <div class="submenu-section">По цвету</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/oblitsovochniy-krasnyy/">красный</a></li>
                                                <li><a href="/catalog/litcevoy-oblitcovochniy-bavarskaya-kladka/">баварская кладка</a></li>
                                                <li><a href="/catalog/oblitsovochniy-zheltyy/">желтый</a></li>
                                                <li><a href="/catalog/oblitsovochniy-korichnevyy/">коричневый</a></li>
                                                <li><a href="/catalog/oblitsovochniy-seryy/">серый</a></li>
                                                <li><a href="/catalog/kirpich/">белый</a></li>
                                                <li><a href="/catalog/oblitsovochniy-flesh-obzhig/">флеш-обжиг</a></li>
                                                <li><a href="/catalog/oblitsovochniy-oranzhevyy/">оранжевый</a></li>
                                                <li><a href="/catalog/oblitsovochniy-rozovyy/">розовый</a></li>
                                                <li><a href="/catalog/oblitsovochniy-fioletovyy/">фиолетовый</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/keramicheskiy-kirpich/">Керамический</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Керамический</div>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="/catalog/keramicheskiy-oblitsovochnyy/">Облицовочный</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">По производителю</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/keramicheskiy-koshchakovskiy/">Кощаковский</a></li>
                                                            <li><a href="/catalog/keramicheskiy-arskiy/">Арский (АСПК)</a></li>
                                                            <li><a href="/catalog/oblitsovochniy-kerma/">Керма</a></li>
                                                            <li><a href="/catalog/oblitsovochniy-belebeevskiy/">Белебеевский</a></li>
                                                            <li><a href="/catalog/keramicheskiy-kirovo-chepetskiy/">Кирово-Чепецкий (КС-Керамик)</a></li>
                                                            <li><a href="/catalog/keramicheskiy-alekseevskiy/">Алексеевский</a></li>
                                                            <li><a href="/catalog/keramicheskiy-klyuchishchenskiy/">Ключищенский (Клюкер)</a></li>
                                                            <!--<li><a href="/catalog/oblitsovochniy-lsr/">ЛСР</a></li>-->
                                                            <li><a href="/catalog/oblitsovochniy-chelninskiy/">Челнинский (Камастройиндустрия)</a></li>
                                                            <li><a href="/catalog/keramicheskiy-izhevskiy/">Ижевский (Альтаир)</a></li>
                                                            <li><a href="/catalog/keramicheskiy-saranskiy/">Саранский (Магма)</a></li>
                                                            <li><a href="/catalog/keramicheskiy-meakir/">Меакир</a></li>
                                                            <li><a href="/catalog/keramicheskiy-revdinskiy/">Ревдинский</a></li>
                                                            
                                                            <!--<li><a href="/catalog/keramicheskiy-mstera/">MSTERA</a></li>-->
                                                        </ul>

                                                        <div class="submenu-separator mb-3"></div>
                                                        <div class="submenu-section">По цвету</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/keramicheskiy-krasnyy/">красный</a></li>
                                                            <li><a href="/catalog/keramicheskiy-oblitcovochniy-bavarskaya-kladka/">баварская кладка</a></li>
                                                            <li><a href="/catalog/keramicheskiy-zheltyy/">желтый</a></li>
                                                            <li><a href="/catalog/keramicheskiy-korichnevyy/">коричневый</a></li>
                                                            <li><a href="/catalog/keramicheskiy-seryy/">серый</a></li>
                                                            <li><a href="/catalog/kirpich/">белый</a></li>
                                                            <li><a href="/catalog/keramicheskiy-flesh-obzhig/">флеш-обжиг</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="/catalog/ryadovoy/">Рядовой</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">По производителю</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/ryadovoy-alekseevskiy/">Алексеевский</a></li>
                                                            <li><a href="/catalog/ryadovoy-klyuchishchenskiy/">Ключищенский (Клюкер)</a></li>
                                                            <li><a href="/catalog/ryadovoy-votkinskiy/">Воткинский</a></li>
                                                            <li><a href="/catalog/ryadovoy-chaykovskiy/">Чайковский</a></li>
                                                            <li><a href="/catalog/ryadovoy-shelangovskiy/">Шеланговский</a></li>
                                                            <li><a href="/catalog/ryadovoy-mamadyshskiy/">Мамадышский</a></li>
                                                            <li><a href="/catalog/ryadovoy-kazanskiy/">Казанский</a></li>
                                                            <li><a href="/catalog/ryadovoy-kurkachinskiy/">Куркачинский</a></li>
                                                            <li><a href="/catalog/ryadovoy-ketra/">Кетра</a></li>
                                                            <li><a href="/catalog/ryadovoy-chelninskiy/">Челнинский (Камастройиндустрия)</a></li>
                                                            <li><a href="/catalog/kirpich/">Кощаковский</a></li>
                                                            <li><a href="/catalog/ryadovoy-cheboksarskiy/">Чебоксарский</a></li>
                                                            <li><a href="/catalog/ryadovoy-arskiy/">Арский (АСПК)</a></li>
                                                            <li><a href="/catalog/ryadovoy-mstera/">MSTERA</a></li>
                                                            <li><a href="/catalog/ryadovoy-meakir/">Меакир</a></li>
                                                            <li><a href="/catalog/ryadovoy-revdinskiy/">Ревдинский</a></li>
                                                            <li><a href="/catalog/ryadovoy-dyurtyulinskiy/">Дюртюлинский</a></li>
                                                            <li><a href="/catalog/ryadovoy-yadrinskiy/">Ядринкий</a></li>
                                                            <li><a href="/catalog/ryadovoy-inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
                                                        </ul>
                                                        <div class="submenu-separator mb-3"></div>
                                                        <div class="submenu-section">По цвету</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/ryadovoy-krasnyy/">красный</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a href="/catalog/silikatnyy/">Силикатный</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По производителю</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/silikatnyy-po-proizvoditelyu-kzssm/">КЗССМ</a></li>
                                                <li><a href="/catalog/silikatnyy-po-proizvoditelyu-zsk-chelny/">ЗСК Челны</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/stroitelnyy-kirpich/">Строительный</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Строительный</div>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="/catalog/stroitelnyy-ryadovoy/">Рядовой</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">По производителю</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-alekseevskiy/">Алексеевский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-klyuchishchenskiy/">Ключищенский (Клюкер)</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-votkinskiy/">Воткинский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-chaykovskiy/">Чайковский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-shelangovskiy/">Шеланговский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-mamadyshskiy/">Мамадышский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-kazanskiy/">Казанский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-kurkachinskiy/">Куркачинский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-ketra/">Кетра</a></li>
                                                            <li><a href="/catalog/stroitelnyy_kirpich_cheboksarskiy/">Чебоксарский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-arskiy/">Арский (АСПК)</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-mstera/">MSTERA</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-yadrinskiy/">Ядринский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-zsk-chelny/">ЗСК Челны</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-meakir/">Меакир</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-revdinskiy/">Ревдинский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-dyurtyulinskiy/">Дюртюлинский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-ryadovoy-inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                <li>
                                                    <a href="/catalog/stroitelnyy-tsokolnyy/">Цокольный</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">По производителю</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/stroitelnyy-tsokolnyy-alekseevskiy/">Алексеевский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-tsokolnyy-votkinskiy/">Воткинский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-tsokolnyy-chaykovskiy/">Чайковский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-tsokolnyy-kazanskiy/">Казанский</a></li>
                                                            <li><a href="/catalog/stroitelnyy-tsokolnyy-cheboksarskiy/">Чебоксарский</a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/figurnyy-kirpich/">Фигурный</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По производителю</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/figurnyy_kirpich_arskiy/">Арский (АСПК)</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/shamotnyy_kirpich/">Шамотный</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Шамотный</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/shb-5/">ШБ-5</a></li>
                                                <li><a href="/catalog/shb-8/">ШБ-8</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/klinkernyy/">Клинкерный</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По производителю</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/klinkernyy_saranskiy/">Саранский (Магма)</a></li>
                                            </ul>
                                            <div class="submenu-separator mb-3"></div>
                                            <div class="submenu-section">По цвету</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/klinkernyy-krasnyy/">красный</a></li>
                                                <li><a href="/catalog/klinkernyy-korichnevyy/">коричневый</a></li>
                                                <li><a href="/catalog/klinkernyy-flesh-obzhig/">флеш-обжиг</a></li>
                                                <li><a href="/catalog/kirpich/">белый</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/dekorativnyy/">Декоративный</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="keram_blocki">
                            <a href="/catalog/keramicheskie-bloki/">Керамические блоки</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Керамические блоки</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/krupnoformatnyy-keramicheskiy-blok/">Крупноформатный керамический блок</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По производителю</div>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="/catalog/krupnoformatnyy-amstron-porikam/">Амстрон (Порикам)</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/krupnoformatnyy-ketra/">Кетра</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/krupnoformatnyy-vinerberger-poroterm/">Винербергер (Поротерм)</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/krupnoformatnyy-revdinskiy/">Ревдинский</a>
                                                </li>             
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/peregorodochnyy-keramicheskiy-blok/">Перегородочный керамический блок</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По производителю</div>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="/catalog/peregorodochnyy-amstron-porikam/">Амстрон (Порикам)</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/peregorodochnyy-ketra/">Кетра</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/peregorodochnyy-vinerberger-poroterm/">Винербергер (Поротерм)</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/keramicheskiy-kamen/">Керамический камень 2.1НФ</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По производителю</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/keramicheskiy-kamen-klyuchishchenskiy/">Ключищенский (Клюкер)</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-mamadyshskiy/">Мамадышский</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-ketra/">Кетра</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-vinerberger-poroterm/">Винербергер (Поротерм)</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-shelangovskiy/">Шеланговский</a></li>
                                                <li><a href="/catalog/kamastroyindustriya/">Челнинский (Камастройиндустрия)</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-arskiy/">Арский (АСПК)</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-mstera/">MSTERA</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-meakir/">Меакир</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-revdinskiy/">Ревдинский</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-yadrinskiy/">Ядринский</a></li>
                                                <li><a href="/catalog/keramicheskiy-kamen-inkeram-tkz/">Тольяттинский (Инкерам)</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="gazobloki">
                            <a href="/catalog/gazobetonnye-bloki-m/">Газобетонные блоки</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Газобетонные блоки</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/krupnoformatnye-gazobetonnye-bloki/">Крупноформатные газобетонные блоки</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Крупноформатные блоки</div>
                                            <ul class="list-unstyled">
                                             <li><a href="/catalog/krupnoformatnye-bikton_/">Биктон</a></li>
                                                <li><a href="/catalog/zyab2/">ЗЯБ</a></li>
                                                <li><a href="/catalog/krupnoformatnye-uniblock/">Uniblock</a></li>
                                                <li><a href="/catalog/krupnoformatnye-teplon/">Теплон</a></li>
                                                <li><a href="/catalog/krupnoformatnye-kottedzh/">Коттедж</a></li>
                                                <li><a href="/catalog/krupnoformatnye-gras/">ГРАС</a></li>
                                                <li><a href="/catalog/krupnoformatnye-zyab-izhevsk/">ЗЯБ (Ижевск)</a></li>
                                                <li><a href="/catalog/krupnoformatnye-build-stone/">Build stone</a></li>
                                                <!--<li><a href="/catalog/krupnoformatnye-kzssm/">КЗССМ</a></li>-->
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/peregorodochnye-gazobetonnye-bloki/">Перегородочные газобетонные блоки</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Перегородочные блоки</div>
                                            <ul class="list-unstyled">
                                               <li><a href="/catalog/peregorodochnye-bikton_/">Биктон</a></li>
                                                <li><a href="/catalog/zyab/">ЗЯБ</a></li>
                                                <li><a href="/catalog/peregorodochnye-uniblock/">Uniblock</a></li>
                                                <li><a href="/catalog/peregorodochnye-teplon/">Теплон</a></li>
                                                <li><a href="/catalog/peregorodochnye-kottedzh/">Коттедж</a></li>
                                                <li><a href="/catalog/peregorodochnye-gras/">ГРАС</a></li>
                                                <li><a href="/catalog/peregorodochnye-zyab-izhevsk/">ЗЯБ (Ижевск)</a></li>
                                                <li><a href="/catalog/krupnoformatnye-build-stone/">Build stone</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/u-obraznye-gazobetonnye-bloki/">U-образные газобетонные блоки</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">U-образные блоки</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/u-obraznye-gazobetonnye-bloki-teplon/">Теплон</a></li>
                                                <li><a href="/catalog/u-obraznye-gazobetonnye-bloki-build-stone/">Build stone</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>

		<li class="keramzitobetonnye-bloki">
                            <a href="/catalog/plitka-bloki-keramzitobetonnye/">Керамзитобетонные блоки</a>

                 </li>


		<li class="oblitsovochnye-materialy">
                            <a href="/catalog/oblitsovochnye-materialy/">Облицовочные материалы</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Облицовочные материалы</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/plitka-oblitsovochnyy-kamen/">Декоративный камень </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/plitka-dekorativnyy-kirpich/">Декоративный кирпич </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/obl_mat_metalicheskiy-sayding/">Металлический сайдинг </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/sayding/">Виниловый сайдинг</a>
                                    </li>
                                </ul>
                            </div>
                  </li>

                 <li class="blagoustroystva">
                            <a href="/catalog/blagoustroystva/">Благоустройства</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Благоустройства</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/plitka-bruschatka/">Брусчатка </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/plitka-trotuarnye-plity/">Тротуарная плитка </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/plitka-bordyury/">Бордюры (поребрики) </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/plitka-poverkhnostnye-sistemy-vodootvody/">Дренажные решетки (водоотводы)</a>
                                    </li>
                                </ul>
                            </div>
                   </li>

                        <li class="suh_stroy_smesi">
                            <a href="/catalog/sukhie-stroitelnye-smesi/">Сухие строительные смеси</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Сухие строительные смеси</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/kladochnyy-rastvor/">Кладочный раствор </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/tsvetnoy-rastvor/">Цветной раствор </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/teploizolyatsionnyy-rastvor/">Теплоизоляционный раствор </a>
                                    </li>

                                    <li>
                                        <a href="/catalog/kley-dlya-gazobetona/">Клей для газобетона</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="stroy_setka">
                            <a href="/catalog/stroitelnaya-setka/">Строительная сетка</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Строительная сетка</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/bazaltovaya-setka/">Базальтовая сетка</a>
                                    </li>

                                    <li>
                                        <a href="/catalog/stekloplastikovaya-setka/">Стеклопластиковая сетка</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="krovlya">
                            <a href="/catalog/krovelnye-materialy/">Кровля</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Кровля</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/metallocherepitsa/">Металлочерепица</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Металлочерепица</div>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="/catalog/metallocherepitsa-monterey/">Монтерей</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/metallocherepitsa-supermonterey/">Супермонтерей</a>
                                                </li>

                                                <li>
                                                    <a href="/catalog/metallocherepitsa-maksi/">Макси</a>
                                                </li>

                                                <li>
                                                    <a href="/catalog/metallocherepitsa-tromantana/">Тромантана</a>
                                                </li>

                                                <li>
                                                    <a href="/catalog/metallocherepitsa-montekristo/">Монтекристо</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/metallocherepitsa-modern/">Modern</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/metallocherepitsa-classic/">Classic</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/metallocherepitsa-kredo/">Kredo</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/metallocherepitsa-kamea/">Kamea</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/metallocherepitsa-kvinta-uno/">Kvinta Uno</a>
                                                </li>
                                                <li>
                                                    <a href="/catalog/metallocherepitsa-kvinta-plus/">Kvinta plus</a>
                                                </li>
                                                
                                                
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a href="/catalog/profnastil/">Профнастил</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Профнастил</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/profnastil-s-8/">С-8</a></li>
                                                <li><a href="/catalog/profnastil-s-8-figurnyy/">С-8 фигурный</a></li>
                                                <li><a href="/catalog/profnastil-s-10/">С-10</a></li>
                                                <li><a href="/catalog/profnastil-s-10-figurnyy/">С-10 фигурный</a></li>
                                                <li><a href="/catalog/profnastil-s-20/">С-20</a></li>
                                                <li><a href="/catalog/profnastil-с-21/">С-21</a></li>
                                                <li><a href="/catalog/profnastil-s-44/">С-44</a></li>
                                                <li><a href="/catalog/profnastil-mp-10/">МП-10</a></li>
                                                <li><a href="/catalog/profnastil-mp-18/">МП-18</a></li>
                                                <li><a href="/catalog/profnastil-mp-20/">МП-20</a></li>
                                                <li><a href="/catalog/profnastil-mp-35/">МП-35</a></li>
                                                <li><a href="/catalog/profnastil-n-60/">Н-60</a></li>
                                                <li><a href="/catalog/profnastil-n-75/">Н-75</a></li>
                                                <li><a href="/catalog/profnastil-n-114/">Н-114</a></li>
                                                <li><a href="/catalog/profnastil-ns-35/">НС-35</a></li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a href="/catalog/standartnye-elementy-otdelki/">Стандартные элементы отделки</a>
                                    </li>
                                    
                                    <li>
                                        <a href="/catalog/sofity/">Софиты</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По производителю</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/sofity-metall-profil/">Металл Профиль</a></li>
                                                <li><a href="/catalog/sofity-grand-line/">Grand Line</a></li>
                                            </ul>
                                        </div>
                                    </li>

                                    
                                    
                                   <li>
                                        <a href="/catalog/falts/">Фальц</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По виду</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/klikfalts-line/">Кликфальц Line</a></li>
                                                <li><a href="/catalog/klikfalts-pro-line/">Кликфальц Pro Line</a></li>
                                                <li><a href="/catalog/klikfalts-pro/">Кликфальц Pro</a></li>
                                                <li><a href="/catalog/klikfalts-pro-gofr/">Кликфальц Pro Gofr</a></li>
                                                <li><a href="/catalog/klikfalts-mini/">Кликфальц Mini</a></li>
                                                <li><a href="/catalog/dvoynoy-stoyachiy-falts-line/">Двойной стоячий фальц Line</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    
                                    
                                   <li>
                                        <a href="/catalog/sayding/">Сайдинг</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По виду</div>
                                            <ul class="list-unstyled">
                                                <li><a href="/catalog/sayding-korabelnaya-doska/">Корабельная доска</a></li>
                                                <li><a href="/catalog/sayding-vertikal/">Вертикаль</a></li>
                                                <li><a href="/catalog/sayding-ekobrus-gofr/">ЭкоБрус/Gofr</a></li>
                                                <li><a href="/catalog/sayding-blok-khaus/">Блок-хаус</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                    
                                    <li>
                                        <a href="/catalog/krepezhnye-izdeliya/">Крепежные изделия</a>
                                    </li>
                                    
                                    <li>
                                        <a href="/catalog/uplotniteli/">Уплотнители</a>
                                    </li>
                                    
                                    <li>
                                        <a href="/catalog/krovelnaya-ventilyatsiya/">Кровельная вентиляция</a>
                                    </li>
                                    
                                    <li>
                                        <a href="/catalog/flyugery-i-ukazateli-vetra/">Флюгеры и указатели ветра</a>
                                    </li>
                                    
                                    <li>
                                        <a href="/catalog/germetiziruyushchie-materialy/">Герметизирующие материалы</a>
                                    </li>
                                    
                                    <li>
                                        <a href="/catalog/stroitelnaya-khimiya/">Строительная химия</a>
                                    </li>

                                    <li>
                                        <a href="/catalog/gidroizolyatsiya-i-paroizolyatsiya/">Гидро и пароизоляция</a>
                                    </li>
                                    
                                    <li>
                                        <a href="/catalog/dobornye-elementy/">Доборные элементы</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Доборные элементы</div>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="/catalog/germetiziruyushchie-materialy/">Герметизирующие материалы</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section"></div>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="/catalog/soedinitelnaya-lenta/">Соединительная лента</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/silikon/">Силикон</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/germetiziruyushchie-materialy/">Герметизирующая лента</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <a href="/catalog/flyugery-i-ukazateli-vetra/">Флюгеры и указатели ветра</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">Флюгеры и указатели ветра</div>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="/catalog/flyugery/">Флюгеры</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/ukazateli-vetra/">Указатели ветра</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <a href="/catalog/stroitelnaya-khimiya/">Строительная химия</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">Строительная химия</div>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="/catalog/remontnaya-emal/">Ремонтная эмаль</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <a href="/catalog/standartnye-elementy-otdelki/">Стандартные элементы отделки</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">Стандартные элементы отделки</div>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="/catalog/planka/">Планка</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/zaglushka/">Заглушка</a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <a href="/catalog/uplotniteli/">Уплотнители</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">Уплотнители</div>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="/catalog/uplotnitel/">Уплотнитель</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <a href="/catalog/krepezhnye-izdeliya/">Крепежные изделия</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">Крепежные изделия</div>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="/catalog/samorezy/">Саморезы</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>

                                                <li>
                                                    <a href="/catalog/krovelnaya-ventilyatsiya/">Кровельная вентиляция</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">Кровельная вентиляция</div>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="/catalog/antennyy-vykhod/">Антенный выход</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/manzheta-krovelnaya/">Манжета кровельная</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/vykhod-ventilyatsii/">Выход вентиляции</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/vykhod-vytyazhki/">Выход вытяжки</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/vykhod-universalnyy/">Выход универсальный</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/gofrirovannaya-truba/">Гофрированная труба</a>
                                                            </li>

                                                            <li>
                                                                <a href="/catalog/truba-izolirovannaya/">Труба изолированная</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                
                                                <li>
                                                    <a href="/catalog/dymniki/">Дымники</a>
                                                    <div class="catalog-submenu">
                                                        <div class="submenu-section">Дымники</div>
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <a href="/catalog/dymniki-dymnik/">Дымник (флюгарка)</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-dymnik-s-zhalyuzi/">Дымник с жалюзи</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-dymnik-dvoynoy-s-zhalyuzi/">Дымник двойной с жалюзи</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-dymnik-s-setkoy/">Дымник с сеткой</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-dymnik-dvoynoy-s-setkoy/">Дымник двойной с сеткой</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-dymnik-yelka/">Дымник ёлка</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-dymnik-dvoynoy/">Дымник двойной</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-kozhukh-na-trubu-pryamoy/">Кожух на трубу прямой</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-kozhukh-na-trubu-skatnyy/">Кожух на трубу скатный</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-kolpak-na-stolb/">Колпак на столб</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-kolpak-pod-fonar/">Колпак под фонарь</a>
                                                            </li>
                                                            <li>
                                                                <a href="/catalog/dymniki-kolpak-dvoynoy/">Колпак двойной</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="vodostok_system">
                            <a href="/catalog/vodostochnye-sistemy/">Водосточные системы</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Водосточные системы</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/kruglogo-secheniya-12590-grandsystem/">Grandsystem - круглого сечения</a>
                                    </li>
                                    <li>
                                        <a href="/catalog/kruglogo-secheniya-mp-prestizh/">МП Престиж - круглого сечения</a>
                                    </li>
                                    <li>
                                        <a href="/catalog/kruglogo-secheniya-mp-proekt-d185150/">МП Проект - круглого сечения</a>
                                    </li>
                                    <li>
                                        <a href="/catalog/pryamougolnogo-secheniya-modern-12076/">МП Модерн - прямоугольного сечения</a>
                                    </li>

                                    <li>
                                        <a href="/catalog/pryamougolnogo-secheniya-mp-byudzhet/">МП Бюджет - прямоугольного сечения</a>
                                    </li>
                                    <li>
                                        <a href="/catalog/po-brendu-grand-line/">Grand Line</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="elem_bezop_krovli">
                            <a href="/catalog/elementy-bezopasnosti-krovli/">Элементы безопасности кровли</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Элементы безопасности кровли</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/snegozaderzhateli/">Снегозадержатели</a>
                                    </li>

                                    <li>
                                        <a href="/catalog/krovelno-stenovaya-lestnitsa/">Кровельно-стеновая лестница</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">Кровельно-стеновая лестница</div>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <a href="/catalog/krovelnaya-lestnitsa/">Кровельная лестница</a>
                                                </li>

                                                <li>
                                                    <a href="/catalog/kronshteyn/">Кронштейн</a>
                                                </li>

                                                <li>
                                                    <a href="/catalog/elementy-bezopasnosti-krovli/">Поручень</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>

                                    <li>
                                        <a href="/catalog/krovelnoe-ograzhdenie/">Кровельное ограждение</a>
                                    </li>

                                    <li>
                                        <a href="/catalog/perekhodnoy-mostik/">Переходной мостик</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="stairs">
                            <a href="/catalog/lestnitsy/">Лестницы</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">По типу</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/mezhetazhnye-lestnitsy/">Межэтажные</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По форме</div>
                                          <ul class="list-unstyled">
                                              <li>
                                                  <a href="/catalog/mezhetazhnye-lestnitsy-po-forme-pryamye/">Прямые</a>
                                                  <div class="catalog-submenu">
                                                      <div class="submenu-section">По виду</div>
                                                      <ul class="list-unstyled">
                                                          <li><a href="/catalog/pryamye-klassic/">Классические</a></li>
                                                          <li><a href="/catalog/pryamye-gusinyy/">Гусиный шаг</a></li>
                                                      </ul>
                                                  </div>
                                              </li>
                                              <li>
                                                  <a href="/catalog/mezhetazhnye-lestnitsy-po-forme-g-obraznye/">Г-образные</a>
                                                  <div class="catalog-submenu">
                                                      <div class="submenu-section">По виду</div>
                                                      <ul class="list-unstyled">
                                                          <li><a href="/catalog/g-obraznye-s-zabezhnymi-stupenyami/">С забежными ступенями</a></li>
                                                          <li><a href="/catalog/g-obraznye-s-ploshchadkoy/">С площадкой</a></li>
                                                      </ul>
                                                  </div>
                                              </li>
                                              <li>
                                                  <a href="/catalog/mezhetazhnye-lestnitsy-po-forme-p-obraznye/">П-образные</a>
                                                  <div class="catalog-submenu">
                                                      <div class="submenu-section">По виду</div>
                                                      <ul class="list-unstyled">
                                                          <li><a href="/catalog/p-obraznye-s-zabezhnymi-stupenyami/">С забежными ступенями</a></li>
                                                          <li><a href="/catalog/p-obraznye-s-ploshchadkoy/">С площадкой</a></li>
                                                      </ul>
                                                  </div>
                                              </li>
                                              <li>
                                                  <a href="/catalog/mezhetazhnye-lestnitsy-po-forme-vintovye/">Винтовые</a>
                                              </li>
                                          </ul>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="/catalog/lestnitsy-cherdachnye/">Чердачные</a>
                                        <div class="catalog-submenu">
                                            <div class="submenu-section">По материалу</div>
                                          <ul class="list-unstyled">
                                              <li>
                                                  <a href="/catalog/lestnitsy-cherdachnye-po-materialu-derevyannye/">Деревянные</a>
                                              </li>
                                              <li>
                                                  <a href="/catalog/lestnitsy-cherdachnye-po-materialu-metallicheskie/">Металлические</a>
                                              </li>
                                          </ul>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li class="teploizolyatsiya">
                            <a href="/catalog/teploizolyatsiya/">Теплоизоляция</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Теплоизоляция</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/mineralnaya-vata/">Минеральная вата</a>
                                    </li>

                                    <li>
                                        <a href="/catalog/ekstrudirovannyy-penopolistirol/">Экструдированный пенополистирол</a>
                                    </li>
                                </ul>
                        <div class="submenu-separator mb-3"></div>
                        <div class="submenu-section">По применению</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/krovelnyy-uteplitel/">Кровельный утеплитель</a>
                                    </li>

                                    <li>
                                        <a href="/catalog/stenovoy-uteplitel/">Стеновой утеплитель</a>
                                    </li>
                                </ul>

                            </div>
                        </li>

                        <li class="teploizolyatsiya">
                            <a href="/catalog/gidroizolyatsiya-i-paroizolyatsiya/">Гидро и пароизоляция</a>
                            <div class="catalog-submenu">
                                <div class="submenu-section">Гидро и пароизоляция</div>
                                <ul class="list-unstyled">
                                    <li>
                                        <a href="/catalog/gidroizolyatsiya/">Гидроизоляция</a>
                                    </li>

                                    <li>
                                        <a href="/catalog/paroizolyatsiya/">Пароизоляция</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                            <li class="plity">
                                <a href="/catalog/zhbi/">ЖБИ</a>
                                    <div class="catalog-submenu">
                                    <div class="submenu-section">ЖБИ</div>
                                        <ul class="list-unstyled">
                                            <li>
                                                <a href="/catalog/plity-perekrytiya/">Плиты перекрытия</a>
                                                    <div class="catalog-submenu">
                                                    <div class="submenu-section">По производителю</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/uniblock-plity-perekrytiya/">Uniblock</a></li>
                                                            <li><a href="/catalog/kamgeszyab-plity-perekrytiya/">КамгэсЗЯБ</a></li>
                                                            <li><a href="/catalog/kulonstroy-plity-perekrytiya/">КулонСтрой</a></li>
                                                            <li><a href="/catalog/raff-plity-perekrytiya/">РАФФ</a></li>
                                                        </ul>
                                                    </div>
                                            </li>

                                            <li>
                                                <a href="/catalog/peremychki/">Перемычки</a>
                                                    <div class="catalog-submenu">
                                                    <div class="submenu-section">По производителю</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/peremychki-kamgeszyab/">КамгэсЗЯБ</a></li>
                                                            <li><a href="/catalog/peremychki-kulonstroy/">КулонСтрой</a></li>
                                                            <li><a href="/catalog/peremychki-raff/">РАФФ</a></li>
                                                        </ul>
                                                    </div>
                                            </li>

                                            <li>
                                                <a href="/catalog/zhbi-koltsa/">ЖБИ кольца</a>
                                                    <div class="catalog-submenu">
                                                    <div class="submenu-section">По производителю</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/zhbi-koltsa-kulonstroy/">КулонСтрой</a></li>
                                                        </ul>
                                                        <div class="submenu-separator mb-3"></div>
                                                            <div class="submenu-section">По типу</div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="/catalog/zhbi-koltsa-dobornye/">Кольца доборные</a></li>
                                                            <li><a href="/catalog/zhbi-koltsa-stenovye-dlya-kolodtsev/">Кольца стеновые</a></li>
                                                            <li><a href="/catalog/zhbi-koltsa-plity-dnishcha-kolodtsev/">Днища колодцев</a></li>
                                                            <li><a href="/catalog/zhbi-koltsa-plity-perekrytiya-kolodtsev/">Крышки колодцев</a></li>
                                                        </ul>
                                                    </div>
                                            </li>
                                        </ul>
                                    </div>
                            </li>
                </div>
            </div>

            <div class="col-xl-9 col-lg-8 p-0 col-md-7 d-none d-md-block d-lg-none">
                <div class="search-and-cart d-none d-sm-flex">
                    <div class="search">
                        <!-- <form class="search-form">
                            <input type="text" name="" value="" class="rounded" placeholder="Введите что Вы хотите найти">
                            <input type="submit" name="" value="">
                        </form> -->
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            "top-search",
                            array(
                                "CATEGORY_0" => array(
                                    0 => "iblock_catalog",
                                ),
                                "CATEGORY_0_TITLE" => "Товары",
                                "CATEGORY_0_iblock_catalog" => array(
                                    0 => "4",
                                ),
                                "CHECK_DATES" => "Y",
                                "CONTAINER_ID" => "title-search-md",
                                "INPUT_ID" => "title-search-input-md",
                                "NUM_CATEGORIES" => "1",
                                "ORDER" => "rank",
                                "PAGE" => "#SITE_DIR#search/index.php",
                                "SHOW_INPUT" => "Y",
                                "SHOW_OTHERS" => "N",
                                "TOP_COUNT" => "5",
                                "USE_LANGUAGE_GUESS" => "Y",
                                "COMPONENT_TEMPLATE" => "top-search"
                            ),
                            false
                        );?>
                    </div>
                </div>
            </div>

            <div class="col-xl-9 col-lg-8 p-0">
                <div class="search-and-cart d-none d-lg-flex">
                    <div class="search">
                        <?$APPLICATION->IncludeComponent(
                            "bitrix:search.title",
                            "top-search",
                            array(
                                "CATEGORY_0" => array(
                                    0 => "iblock_catalog",
                                ),
                                "CATEGORY_0_TITLE" => "Товары",
                                "CATEGORY_0_iblock_catalog" => array(
                                    0 => "4",
                                ),
                                "CHECK_DATES" => "Y",
                                "CONTAINER_ID" => "title-search",
                                "INPUT_ID" => "title-search-input",
                                "NUM_CATEGORIES" => "1",
                                "ORDER" => "rank",
                                "PAGE" => "#SITE_DIR#search/index.php",
                                "SHOW_INPUT" => "Y",
                                "SHOW_OTHERS" => "N",
                                "TOP_COUNT" => "5",
                                "USE_LANGUAGE_GUESS" => "Y",
                                "COMPONENT_TEMPLATE" => "top-search"
                            ),
                            false
                        );?>
                    </div>
                    <div class="cart-compare d-none d-xl-flex">
                        <div class="compare"><a href="/personal/compare.php">Сравнение <span id="compare_count"><?=$compared?></span></a></div>
                        <div class="bookmarked"><a href="/wishlist/">Закладки <span id="wishcount"><?=$bookmarked?></span></a></div>
                        <div class="basket"><a href="/basket/"><span id="cart_count"><?=$in_cart?></span></a></div>
                    </div>
                </div>
            </div>
        </div>

        <?if($_GET['test'] == "Y"):?>
        <div class="row horizontal-menu">
            <div class="col">
                <a href="/catalog/kirpich/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/002-brick-1.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/002-brick-1.svg" alt="">
            </span>
                    Облицовочный кирпич
                </a>
            </div>

            <div class="col">
                <a href="/catalog/stroitelnyy-tsokolnyy/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/001-brick.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/001-brick.svg" alt="">
            </span>
                    Цокольный кирпич
                </a>
            </div>

            <div class="col">
                <a href="/catalog/stroitelnyy-ryadovoy/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/kirpich.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/kirpich.svg" alt="">
            </span>
                    Рядовой кирпич
                </a>
            </div>

            <div class="col">
                <a href="/catalog/keramicheskie-bloki/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/keramblock.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/keramblock.svg" alt="">
            </span>
                    Керамические блоки
                </a>
            </div>
            <div class="col">
                <a href="/catalog/gazobetonnye-bloki-m/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/gazoblock.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/gazoblock.svg" alt="">
            </span>
                    Газобетонные блоки
                </a>
            </div>
            <div class="col">
                <a href="/catalog/sukhie-stroitelnye-smesi/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/smesi.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/smesi.svg" alt="">
            </span>
                    Строительные смеси
                </a>
            </div>
            <div class="col">
                <a href="/catalog/stroitelnaya-setka/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/setka.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/setka.svg" alt="">
            </span>
                    Строительная сетка
                </a>
            </div>
            <div class="col short">
                <a href="/catalog/krovelnye-materialy/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/house-roof.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/house-roof.svg" alt="">
            </span>
                    Кровля
                </a>
            </div>
            <div class="col long">
                <a href="/catalog/elementy-bezopasnosti-krovli/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/elementy-bezopasnosty.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/elementy-bezopasnosty.svg" alt="">
            </span>
                    Элементы безопасности <br> кровли
                </a>
            </div>

            <div class="col">
                <a href="/catalog/zhbi/">
            <span class="image">
              <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/zbi.png" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/zbi.png" alt="">
            </span>
                    Железобетонные изделия
                </a>
            </div>
        </div>
        <?else:?>
        <div class="row horizontal-menu">
            <div class="col has-dropdown">
                <a href="/catalog/kirpich/">
                    <span class="image">
                      <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/kirpich_i_kladka_v1.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/kirpich_i_kladka_v1.svg" alt="">
                    </span>
                    <span class="name">Кирпичи и элементы кладки</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/oblitsovochnyy/">Облицовочный</a></li>
                        <li><a href="/catalog/stroitelnyy-tsokolnyy/">Цокольный</a></li>
                        <li><a href="/catalog/stroitelnyy-ryadovoy/">Рядовой</a></li>
                        <li><a href="/catalog/figurnyy-kirpich/">Фигурный</a></li>
                        <li><a href="/catalog/shamotnyy_kirpich/">Огнеупорный</a></li>
                        <li><a href="/catalog/dekorativnyy/">Декоративный</a></li>
                        <li><a href="/catalog/kladochnyy-rastvor/">Кладочный раствор</a></li>
                        <li><a href="/catalog/tsvetnoy-rastvor/">Цветной раствор</a></li>
                        <li><a href="/catalog/bazaltovaya-setka/">Базальтовая сетка</a></li>
                        <li><a href="/catalog/stekloplastikovaya-setka/">Стеклопластиковая сетка</a></li>
                        <li><a href="/catalog/gibkaya-svyaz-st/">Гибкие связи</a></li>
                        <li><a href="/catalog/ventkorobka-dlya-kirpicha/">Вентиляционные короба</a></li>
                        <li><a href="/catalog/stenovoy-uteplitel/">Стеновой утеплитель</a></li>
                    </ul>
                </div>
            </div>

            <div class="col has-dropdown">
                <a href="/catalog/keramicheskie-bloki/">
                    <span class="image">
                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/keramblock_v1.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/keramblock_v1.svg" alt="">
                    </span>
                    <span class="name">Блоки и элементы кладки</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/keramicheskie-bloki/">Керамические блоки</a></li>
                        <li><a href="/catalog/gazobetonnye-bloki-m/">Газобетонные блоки</a></li>
                        <li><a href="/catalog/keramicheskiy-kamen/">Керамический камень 2.1НФ</a></li>
                        <li><a href="/catalog/plitka-bloki-keramzitobetonnye/">Керамзитобетонные блоки</a></li>
                        <li><a href="/catalog/kley-dlya-gazobetona/">Клей для газобетона</a></li>
                        <li><a href="/catalog/bazaltovaya-setka/">Базальтовая сетка</a></li>
                        <li><a href="/catalog/stekloplastikovaya-setka/">Стеклопластиковая сетка</a></li>
                        <li><a href="/catalog/gibkaya-svyaz-st/">Гибкие связи</a></li>
                        <li><a href="/catalog/stenovoy-uteplitel/">Стеновой утеплитель</a></li>
                    </ul>
                </div>
            </div>

            <div class="col has-dropdown">
                <a href="/catalog/lestnitsy/">
                    <span class="image">
                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/stairs_v1.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/stairs_v1.svg" alt="">
                    </span>
                    <span class="name">Лестницы</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/mezhetazhnye-lestnitsy/">Межэтажные</a></li>
                        <li><a href="/catalog/lestnitsy-cherdachnye/">Чердачные</a></li>
                    </ul>
                </div>
            </div>

            <div class="col has-dropdown">
                <a href="/catalog/vodostochnye-sistemy/">
                    <span class="image">
                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/vodostok_v1.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/vodostok_v1.svg" alt="">
                    </span>
                    <span class="name">Водосточная система</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/po-tipu-secheniya-kruglyy/">Круглого сечения</a></li>
                        <li><a href="/catalog/po-tipu-secheniya-pryamougolnyy/">Прямоугольного сечения</a></li>
                    </ul>
                </div>
            </div>

            <div class="col has-dropdown">
                <a href="/catalog/krovelnye-materialy/">
                    <span class="image">
                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/house-roof_v1.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/house-roof_v1.svg" alt="">
                    </span>
                    <span class="name">Кровельные материалы</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/profnastil/">Профнастил</a></li>
                        <li><a href="/catalog/metallocherepitsa/">Металлочерепица</a></li>
                        <li><a href="/catalog/sofity/">Софиты</a></li>
                        <li><a href="/catalog/standartnye-elementy-otdelki/">Стандартные элементы отделки</a></li>
                        <li><a href="/catalog/krepezhnye-izdeliya/">Крепежные изделия</a></li>
                        <li><a href="/catalog/uplotniteli/">Уплотнители</a></li>
                        <li><a href="/catalog/krovelnaya-ventilyatsiya/">Кровельная вентиляция</a></li>
                        <li><a href="/catalog/flyugery-i-ukazateli-vetra/">Флюгеры и указатели ветра</a></li>
                        <li><a href="/catalog/germetiziruyushchie-materialy/">Герметизирующие материалы</a></li>
                        <li><a href="/catalog/stroitelnaya-khimiya/">Строительная химия</a></li>
                        <li><a href="/catalog/krovelnyy-uteplitel/">Кровельный утеплитель</a></li>
                        <li><a href="/catalog/gidroizolyatsiya-i-paroizolyatsiya/">Гидро и пароизоляция</a></li>
                    </ul>
                </div>
            </div>

            <div class="col has-dropdown">
                <a href="/catalog/elementy-bezopasnosti-krovli/">
                    <span class="image">
                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/elementy-bezopasnosty_v1.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/elementy-bezopasnosty_v1.svg" alt="">
                    </span>
                    <span class="name">Элементы безопасности кровли</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/snegozaderzhateli/">Снегозадержатели</a></li>
                        <li><a href="/catalog/krovelnoe-ograzhdenie/">Кровельное ограждение</a></li>
                        <li><a href="/catalog/krovelno-stenovaya-lestnitsa/">Кровельно-стеновая лестница</a></li>
                        <li><a href="/catalog/perekhodnoy-mostik/">Переходной мостик</a></li>
                    </ul>
                </div>
            </div>


            <div class="col has-dropdown">
                <a href="/catalog/zhbi/">
                 <span class="image">
                  <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/zbi_v1.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/zbi_v1.svg" alt="">
                 </span>
                    <span class="name">Железобетонные изделия</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/plity-perekrytiya/">Плиты перекрытия</a></li>
                        <li><a href="/catalog/peremychki/">Перемычки</a></li>
                        <li><a href="/catalog/zhbi-koltsa/">Железобетонные кольца</a></li>
                    </ul>
                </div>
            </div>

           			  <div class="col has-dropdown">
                <a href="/catalog/oblitsovochnye-materialy/">
                    <span class="image">
                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/облицовка.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/облицовка.svg" alt="">
                    </span>
                    <span class="name">Облицовочные материалы</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/plitka-oblitsovochnyy-kamen/">Декоративный камень</a></li>
                        <li><a href="/catalog/plitka-dekorativnyy-kirpich/">Декоративный кирпич</a></li>
                        <li><a href="/catalog/obl_mat_metalicheskiy-sayding/">Металлический сайдинг</a></li>
                        <li><a href="/catalog/sayding/">Виниловый сайдинг</a></li>
                    </ul>
                </div>
            </div>
			
			
		   <div class="col has-dropdown">
                <a href="/catalog/blagoustroystva/">
                    <span class="image">
                        <img class="lazy" data-src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/благоустройства.svg" data-srcset="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/благоустройства.svg" alt="">
                    </span>
                    <span class="name">Благоустройства</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/plitka-bruschatka/">Брусчатка</a></li>
                        <li><a href="/catalog/plitka-trotuarnye-plity/">Тротуарная плитка</a></li>
                        <li><a href="/catalog/plitka-bordyury/">Бордюры (поребрики)</a></li>
                        <li><a href="/catalog/plitka-poverkhnostnye-sistemy-vodootvody/">Дренажные решетки (водоотводы)</a></li>
                    </ul>
                </div>
            </div>


            <!--<div class="col has-dropdown">
                <a href="/catalog/teploizolyatsiya/">
                    <span class="image">
                        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/teploizolyatsiya.svg" alt="">
                    </span>
                    <span class="name">Теплоизоляция</span>
                </a>

                <div class="custom-dropdown-menu">
                    <ul>
                        <li><a href="/catalog/catalog/mineralnaya-vata/">Минеральная вата</a></li>
                        <li><a href="/catalog/catalog/ekstrudirovannyy-penopolistirol/">Экструзионный пенополистирол</a></li>
                    </ul>
                </div>
            </div>-->
        </div>
        <?endif;?>


        <div class="row">
            <div class="col p-0">
                <?/* Если мы находимся на главной */?>
                <? if ($APPLICATION->GetCurPage(false) === '/'): ?>

                    <?
                    $bannerFilter = array();
                    $bannerFilter[] = getRegionsFilter();

                    $APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "banner-slider",
                        array(
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "AJAX_MODE" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "CACHE_FILTER" => "N",
                            "CACHE_GROUPS" => "Y",
                            "CACHE_TIME" => "36000000",
                            "CACHE_TYPE" => "A",
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "DISPLAY_BOTTOM_PAGER" => "Y",
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "N",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "DISPLAY_TOP_PAGER" => "N",
                            "FIELD_CODE" => array(
                                0 => "DETAIL_PICTURE",
                                1 => "",
                            ),
                            //"USE_FILTER" => 'Y',
                            "FILTER_NAME" => "bannerFilter",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "N",
                            "IBLOCK_ID" => "5",
                            "IBLOCK_TYPE" => "dynamic_content",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "INCLUDE_SUBSECTIONS" => "N",
                            "MESSAGE_404" => "",
                            "NEWS_COUNT" => "8",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "PAGER_DESC_NUMBERING" => "N",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => ".default",
                            "PAGER_TITLE" => "Новости",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "PROPERTY_CODE" => array(
                                0 => "",
                                1 => "ATT_PRODUCT_ON_BANNER",
                                2 => "",
                            ),
                            "SET_BROWSER_TITLE" => "N",
                            "SET_LAST_MODIFIED" => "N",
                            "SET_META_DESCRIPTION" => "N",
                            "SET_META_KEYWORDS" => "N",
                            "SET_STATUS_404" => "N",
                            "SET_TITLE" => "N",
                            "SHOW_404" => "N",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER1" => "DESC",
                            "SORT_ORDER2" => "ASC",
                            "STRICT_SECTION_CHECK" => "N",
                            "COMPONENT_TEMPLATE" => "banner-slider",
                            "COMPOSITE_FRAME_MODE" => "A",
                            "COMPOSITE_FRAME_TYPE" => "AUTO"
                        ),
                        false
                    );?>
                <? endif; ?>
            </div>
        </div>

    </div>
</div>

<?/* Если мы не находимся на главной */?>
<? if ($APPLICATION->GetCurPage(false) !== '/'): ?>

<? endif;?>