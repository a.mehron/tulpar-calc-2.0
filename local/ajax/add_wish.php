<?
session_start();

$element_id = (isset($_POST['el_id']) ? $_POST['el_id'] : "empty");

if(!is_array($_SESSION['element_ids']))
{
  $_SESSION['element_ids'] = array();
  array_push($_SESSION['element_ids'], $element_id);
}
else{
    if (!in_array($element_id, $_SESSION['element_ids'])) {
        array_push($_SESSION['element_ids'], $element_id);
    }
}

echo sizeof($_SESSION['element_ids']);

?>
