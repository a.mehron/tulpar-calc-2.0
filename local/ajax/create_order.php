<?
global $user_errors;
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main\Application;
use Bitrix\Sale;
use Bitrix\Sale\Basket;
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");

$request = Application::getInstance()->getContext()->getRequest();

if(!CUser::IsAuthorized()){

    if (empty($request->getPost("cart_appl_phone"))) {
        $user_errors['phone'] = 'Телефон обязателен!';
    }

    if (empty($request->getPost("cart_appl_name"))) {
        $user_errors['phone'] = 'Имя обязательно!';
    }
    $user_ID = null;
}else{
    $user_ID = $USER->GetID();
}

$fullName = $request->getPost("cart_appl_name");
$phone = $request->getPost("cart_appl_phone");
//$email = "tulpar-trade@mail.ru";
$email = $request->getPost("cart_appl_email");
$login = $request->getPost("cart_appl_email");
$new_password = randString(7);

$rsUser = CUser::GetByLogin( $request->getPost("cart_appl_email") );
$arUser = $rsUser->Fetch();
if ($arUser) {
    $user_ID = $arUser['ID'];
}

if(!isset($user_ID)){
    $user = new CUser;
    $arFields = Array(
        "NAME"              => $fullName,
        "LAST_NAME"         => "",
        "EMAIL"             => $email, // обязатель
        "LOGIN"             => $login, // обязатель
        "LID"               => "ru",
        "PERSONAL_PHONE"    => $phone,
        "ACTIVE"            => "Y",
        "GROUP_ID"          => array(2), // группа Все пользователи
        "PASSWORD"          => $new_password, // обязатель
        "CONFIRM_PASSWORD"  => $new_password, // обязатель
    );

    $user_ID = $user->Add($arFields);
    if (intval($user_ID) > 0){
       echo "Пользователь успешно добавлен.";
        // die();
    }
    else{
        $user_errors['new_user'] = $user->LAST_ERROR;
    }

}

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

if(!empty($basket->getQuantityList()) && isset($user_ID)){

    foreach ($basket as $product)
    {
        $product_fields = [
            'PRODUCT_ID' => $product->getProductId(),
            'NAME' => $product->getField('NAME'),
            'PRICE' => $product->getPrice(),
            'CURRENCY' => $product->getField('CURRENCY'),
            'QUANTITY' => $product->getQuantity() ,
            'PRODUCT_XML_ID' => $product->getField('PRODUCT_XML_ID') ,
            'CATALOG_XML_ID' => $product->getField('CATALOG_XML_ID')
        ];
        $item = $basket->createItem("catalog", $product->getProductId());
        // unset($product[$product->getProductId()]);
        $item->setFields($product_fields);
    }
    // //////////////////////////////////////////////////
    $order = Bitrix\Sale\Order::create(SITE_ID, $user_ID);
    $order->setPersonTypeId(1); // Физич/Юридич. лицо
    $order->setBasket($basket);
    // добавляем комментарий
	if (!empty($request->getPost("cart_appl_comment"))) {
		$order->setField("USER_DESCRIPTION", $request->getPost("cart_appl_comment"));
	}
	
    // ////////////////////////////////////////////////// Доставка
    $shipmentCollection = $order->getShipmentCollection();
    $shipment = $shipmentCollection->createItem(
        Bitrix\Sale\Delivery\Services\Manager::getObjectById(1)
    );
    // //////////////////////////////////////////////////
    $shipmentItemCollection = $shipment->getShipmentItemCollection();

    foreach ($basket as $basketItem)
    {
        $item = $shipmentItemCollection->createItem($basketItem);
        $item->setQuantity($basketItem->getQuantity());
    }
    // ////////////////////////////////////////////////// Оплата
    $paymentCollection = $order->getPaymentCollection();
    $payment = $paymentCollection->createItem(
        Bitrix\Sale\PaySystem\Manager::getObjectById(1) // ННаличными при получении
    );
    // //////////////////////////////////////////////////
    $payment->setField("SUM", $order->getPrice());
    $payment->setField("CURRENCY", $order->getCurrency());
    // $order->setField("LOCATION" , 129);

    // ///////////////////////////////// user props
    // Устанавливаем свойства
    $propertyCollection = $order->getPropertyCollection();
    // телефон
    $phoneProp = $propertyCollection->getPhone();
    $phoneProp->setValue($phone);
    // имя
    $nameProp = $propertyCollection->getPayerName();
    $nameProp->setValue(!empty($fullName) ? $fullName : '');
    // email
    $emailProp = $propertyCollection->getUserEmail();
    $emailProp->setValue($email);

    $result = $order->save();
    if (!$result->isSuccess()){
        echo json_encode(['error' => $result->getErrors()]);
    }else{
        echo json_encode(['success' =>  "/basket?ORDER_ID=".$order->getField('ACCOUNT_NUMBER')]);
    }
}elseif(!empty($basket->getQuantityList())){
    echo json_encode(['error' => 'пустая корзина']);
}elseif(isset($user_ID)){
    echo json_encode(['error' => 'не создался пользователь']);
}

echo $user_ID;