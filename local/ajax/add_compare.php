<?
session_start();

$element_id = (isset($_POST['cmp_el_id']) ? $_POST['cmp_el_id'] : "empty");

if(!is_array($_SESSION['cmp_element_ids']))
{
  $_SESSION['cmp_element_ids'] = array();
  array_push($_SESSION['cmp_element_ids'], $element_id);
}
else{
    if (!in_array($element_id, $_SESSION['cmp_element_ids'])) {
        array_push($_SESSION['cmp_element_ids'], $element_id);
    }
}

echo sizeof($_SESSION['cmp_element_ids']);

?>
