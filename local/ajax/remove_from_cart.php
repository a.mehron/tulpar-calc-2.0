<?php
$offer_id = (isset($_POST['offer_id']) ? $_POST['offer_id'] : "empty");
$cart_json = json_decode($_COOKIE['cart'], true);

if (($key = array_search($offer_id, $cart_json)) !== false) {
    unset($cart_json[$key]);
    echo "string";
}

foreach ($cart_json as $key => $value) {
    if ($value['ID'] == $offer_id) {
        unset($cart_json[$key]);
    }
}

setcookie("cart", json_encode($cart_json), time()+18000, "/");

echo sizeof($cart_json);

 ?>
