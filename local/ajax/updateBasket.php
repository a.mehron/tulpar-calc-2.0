<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");


use Bitrix\Sale;
use Bitrix\Sale\Basket;
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");


$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$basketItems = $basket->getBasketItems();

if (isset($_POST['action'])) {
    if ($_POST['action'] === "updateBasket" && isset($_POST['data'])) {
        $data = json_decode($_POST['data'], true);

        updateBasketItemAmount($data['actionType'], $data['productId'], $data['measureRatio'], $data['basketRecordId'], $basket);
    }
}

function updateBasketItemAmount($actionType, $productId, $measureRatio, $basketRecordId, $basketObj) {

    switch ($actionType){
        case "add":
            $item = $basketObj->getItemById($basketRecordId);
            $currentQuantity = $item->getField('QUANTITY');
            $item->setField('QUANTITY', $currentQuantity+$measureRatio);
            break;
    }

    $basketObj->save();

}

print_r($basket->getItemById($data['basketRecordId'])->getField('QUANTITY'));
