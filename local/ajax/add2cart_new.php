<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");


use Bitrix\Sale;
use Bitrix\Sale\Basket;
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");

$element = (isset($_POST['offer_param']) ? $_POST['offer_param'] : "empty");

$element = json_decode($element, true);

$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());

$productId = $element["ID"];
$quantity = 1;

$isNewItem = false;

if ($item = $basket->getExistsItem('catalog', $productId)) {
    $measureData = \Bitrix\Catalog\MeasureRatioTable::getCurrentRatio($productId);
    $item->setField('QUANTITY', $item->getQuantity() + $measureData[$productId]);
}
else {
    $item = $basket->createItem('catalog', $productId);
    $item->setFields(array(
        'QUANTITY' => $quantity,
        'CURRENCY' => Bitrix\Currency\CurrencyManager::getBaseCurrency(),
        'LID' => Bitrix\Main\Context::getCurrent()->getSite(),
        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
    ));

    $isNewItem = true;
}

if ($isNewItem) {
    $measureData = \Bitrix\Catalog\MeasureRatioTable::getCurrentRatio($productId);
    $item->setField('QUANTITY', $measureData[$productId]);
}

$basket->save();


echo sizeof($basketItems = $basket->getBasketItems());

