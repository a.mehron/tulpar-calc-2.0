<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы о нашей компании");
?><div class="gray-wrapper">
	<div class="container p-0">
		<div class="row bg-white justify-content-center p-3">
			<div class="col-lg-8 p-0">
				<h1 class="page-section mb-4">Отзывы о нашей компании</h1>
				<a target="_blank" class="out-reviews" href="https://yandex.ru/maps/org/161044343820">Отзывы на Яндекс.Организации</a><br>
				<a target="_blank" class="out-reviews" href="https://g.co/kgs/QCZHhV">Отзывы на Google Мой бизнес</a><br>
				<a target="_blank" class="out-reviews" href="https://m.2gis.ru/kazan/firm/70000001030442136/tab/reviews?m=49.145685%2C55.76074%2F18">Отзывы на 2ГИС</a><br>
			</div>
		</div>
	</div>
</div><?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>