<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Разделы");
?>
<div class="container">
	<?$APPLICATION->IncludeComponent(
		"bitrix:catalog.section.list",
		"tree1",
		Array(
			"ADD_SECTIONS_CHAIN" => "Y",
			"CACHE_GROUPS" => "Y",
			"CACHE_TIME" => "36000000",
			"CACHE_TYPE" => "A",
			"COMPOSITE_FRAME_MODE" => "A",
			"COMPOSITE_FRAME_TYPE" => "AUTO",
			"COUNT_ELEMENTS" => "Y",
			"IBLOCK_ID" => "4",
			"IBLOCK_TYPE" => "catalog",
			"SECTION_CODE" => "",
			"SECTION_FIELDS" => array("CODE","NAME",""),
			"SECTION_ID" => $_REQUEST["SECTION_ID"],
			"SECTION_URL" => "",
			"SECTION_USER_FIELDS" => array("",""),
			"SHOW_PARENT_NAME" => "Y",
			"TOP_DEPTH" => "5",
			"VIEW_MODE" => "LINE"
		)
	);?>
</div>


<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
