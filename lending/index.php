<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Лендинг");
$APPLICATION->SetAdditionalCSS("https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.css");
$APPLICATION->SetAdditionalCSS("/local/templates/tulpar_store/assets/landing/dist/css/main.css");
$APPLICATION->SetAdditionalCSS("/local/templates/tulpar_store/assets/landing/dist/fonts/fonts.css");
$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/aos/2.3.4/aos.js');
$APPLICATION->AddHeadScript('/local/templates/tulpar_store/assets/landing/dist/js/main.js');
?>

    <div class="landing">
        <div class="container">
            <div class="video-banner video-banner--whyus">
                <a data-fancybox href="https://www.youtube.com/watch?v=77jb9NJV85w">
                    <span class="play-btn animated infinite pulse slower"><img src="/local/templates/tulpar_store/assets/landing/dist/img/play.png" alt=""></span>
                    <div class="video-banner__tulpar-logo" data-aos="fade-left" data-aos-duration="1000" data-aos-delay="200" data-aos-once="true">
                        <span class="tulpar-logo__img"><img src="/local/templates/tulpar_store/assets/landing/dist/img/logo.png" alt=""></span>
                        <div class="tulpar-logo__title">ТУЛПАР</div>
                    </div>

                    <div class="why-tulpar" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="700" data-aos-once="true">
                        <span class="why-tulpar__text">ПОЧЕМУ ВЫБИРАЮТ ТУЛПАР</span>
                    </div>
                </a>
            </div>

            <div class="can-buy">
                <div class="can-buy__title landing-title">
                    У нас  можно приобрести
                </div>

                <div class="can-buy__block" data-aos="fade-up" data-aos-once="true">
                    <div class="item-canbuy">
                        <a href="/catalog/kirpich/">
                            <div class="item-canbuy__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/01.jpg" alt="">
                            </div>
                            <div class="item-canbuy__image--hover image--hover-kirpich">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/01_hover.png" alt="">
                            </div>

                            <div class="item-canbuy__title">
                                Кирпич
                            </div>
                        </a>
                    </div>

                    <div class="item-canbuy">
                        <a href="/catalog/keramicheskie-bloki/">
                            <div class="item-canbuy__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/02.jpg" alt="">
                            </div>
                            <div class="item-canbuy__image--hover image--hover-keramblock">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/02_hover.png" alt="">
                            </div>

                            <div class="item-canbuy__title">
                                Керамические блоки
                            </div>
                        </a>
                    </div>

                    <div class="item-canbuy">
                        <a href="/catalog/gazobetonnye-bloki-m/">
                            <div class="item-canbuy__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/03.jpg" alt="">
                            </div>
                            <div class="item-canbuy__image--hover image--hover-gazoblock">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/03_hover.png" alt="">
                            </div>

                            <div class="item-canbuy__title">
                                Газобетонные блоки
                            </div>
                        </a>
                    </div>

                    <div class="item-canbuy">
                        <a href="/catalog/lestnitsy/">
                            <div class="item-canbuy__image image-stairs">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/04.jpg" alt="">
                            </div>
                            <div class="item-canbuy__image--hover image--hover-stairs">
                            </div>

                            <div class="item-canbuy__title">
                                Лестницы
                            </div>
                        </a>
                    </div>

                    <div class="item-canbuy">
                        <a href="/catalog/krovelnye-materialy/">
                            <div class="item-canbuy__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/05.jpg" alt="">
                            </div>
                            <div class="item-canbuy__image--hover image--hover-roof">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/05_hover.png" alt="">
                            </div>

                            <div class="item-canbuy__title">
                                Кровельные материалы
                            </div>
                        </a>
                    </div>

                    <div class="item-canbuy">
                        <a href="/catalog/plity-perekrytiya/">
                            <div class="item-canbuy__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/06.jpg" alt="">
                            </div>
                            <div class="item-canbuy__image--hover image--hover-plity">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/06_hover.png" alt="">
                            </div>

                            <div class="item-canbuy__title">
                                Плиты перекрытия
                            </div>
                        </a>
                    </div>

                    <div class="item-canbuy">
                        <a href="/catalog/sukhie-stroitelnye-smesi/">
                            <div class="item-canbuy__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/07.jpg" alt="">
                            </div>
                            <div class="item-canbuy__image--hover image--hover-mixes">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/07_hover.png" alt="">
                            </div>

                            <div class="item-canbuy__title">
                                Строительные смеси
                            </div>
                        </a>
                    </div>


                    <div class="item-canbuy">
                        <a href="/catalog/stroitelnaya-setka/">
                            <div class="item-canbuy__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/08.jpg" alt="">
                            </div>
                            <div class="item-canbuy__image--hover image--hover-setka">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/canbuy/08_hover.png" alt="">
                            </div>

                            <div class="item-canbuy__title">
                                Строительная сетка
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="contact-dark landing__contact-form">
            <div class="contact-dark__left"></div>
            <div class="contact-dark__center">
                <div class="container">
                    <div class="contact-dark__body">
                        <div class="contact-dark__form">
                            <h2>Нужна помощь в выборе материала?</h2>

                            <div class="contact-dark__desc">
                                Оставьте, пожалуйста, ваши контактные данные, и наш менеджер <br>
                                свяжется с вами в ближайшее время и ответит на все интересующие вас вопросы
                            </div>

                            <form class="landing-form" data-topic="Помощь в выборе материала">
                                <input type="hidden" name="topicLanding[]">
                                <div class="input-row input-row--full">
                                    <input type="text" class="help-full__input" placeholder="Ваш вопрос" name="yourQuestion[]">
                                </div>

                                <div class="input-row input-row--half">
                                    <input type="text" class="help-half__input" placeholder="Имя" name="yourName[]">
                                    <input type="text" class="help-half__input input-phone" placeholder="Номер телефона" name="yourPhone[]">
                                </div>

                                <div class="input-row input-row--half">
                                    <div class="checkbox-wrapper custom-checkbox">
                                        <label>
                                            <input type="checkbox" checked name="checkbox-personal-data" class="checkbox-personal-data">
                                            <span class="pretty-checkbox"></span>
                                            <span class="personal-text">Даю согласие на обработку персональных данных</span>
                                        </label>
                                    </div>
                                    <div class="help-half__input">
                                        <input type="submit" value="Отправить">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="contact-dark__workers" data-aos="fade-up" data-aos-once="true">
                            <div class="workers__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/workers1.png" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="contact-dark__right">
                <div class="right__image" data-aos="fade-up" data-aos-delay="300" data-aos-once="true">
                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/bricks.png" alt="">
                </div>
            </div>
        </div>

        <div class="about-banner">
            <div class="container">
                <div class="landing-title about-title">О компании</div>
                <div class="video-banner video-banner--about">
                    <a data-fancybox href="https://www.youtube.com/watch?v=-nfcl8Y-yDo">
                        <span class="play-btn"><img src="/local/templates/tulpar_store/assets/landing/dist/img/play.png" alt=""></span>
                        <div class="video-banner__tulpar-logo" data-aos="fade-left" data-aos-duration="1000" data-aos-delay="200" data-aos-once="true">
                            <span class="tulpar-logo__img"><img src="/local/templates/tulpar_store/assets/landing/dist/img/logo.png" alt=""></span>
                            <div class="tulpar-logo__title">ТУЛПАР</div>
                        </div>

                        <div class="why-tulpar" data-aos="fade-right" data-aos-duration="1000" data-aos-delay="700" data-aos-once="true">
                            <span class="why-tulpar__text">О КОМПАНИИ</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <div class="advantages">
            <div class="container">
                <div class="landing-title advantages-title">Наши преимущества</div>
                <div class="advantages-items" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                    <div class="advantage__item">
                        <div class="image-advantage">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/svg/factory.svg" alt="">
                        </div>

                        <div class="text-advantage">
                            Работаем только на прямую с заводами производителями
                        </div>
                    </div>

                    <div class="advantage__item">
                        <div class="image-advantage">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/svg/cartera.svg" alt="">
                        </div>

                        <div class="text-advantage">
                            Гарантия лучшей цены
                        </div>
                    </div>

                    <div class="advantage__item">
                        <div class="image-advantage">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/svg/question.svg" alt="">
                        </div>

                        <div class="text-advantage">
                            Консультация профессионалов по вопросам выбора материала
                        </div>
                    </div>

                    <div class="advantage__item">
                        <div class="image-advantage">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/svg/calculator.svg" alt="">
                        </div>

                        <div class="text-advantage">
                            Расчет необходимого количества материала
                        </div>
                    </div>

                    <div class="advantage__item">
                        <div class="image-advantage">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/svg/delivery-truck.svg" alt="">
                        </div>

                        <div class="text-advantage">
                            Организация доставки и разгрузки
                        </div>
                    </div>

                    <div class="advantage__item">
                        <div class="image-advantage">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/svg/wall.svg" alt="">
                        </div>

                        <div class="text-advantage">
                            Возможность посмотреть образцы товара в шоуруме
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="visualization__landing">
            <div class="container">
                <div class="title-visual landing-title">3D Визуализация <span class="subtext">Переходите по ссылке  на калькулятор и расчетайте стоимости дома</span></div>

                <div class="visual__house" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                    <div class="house__image">
                        <img src="/local/templates/tulpar_store/assets/landing/dist/img/house.jpg" alt="">
                    </div>

                    <div class="visual__link">
                        <a href="/3d/">Рассчитать стоимость дома</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="contact-dark landing__contact-form2" data-aos="fade-up" data-aos-once="true">
            <div class="contact-dark__left"></div>
            <div class="contact-dark__center">
                <div class="container">
                    <div class="contact-dark__body">
                        <div class="contact-dark__form">
                            <h2>Бесплатно! Сделаем точный расчет строительства</h2>

                            <div class="contact-dark__desc">
                                Перед началом строительства, исходя из масштабов строительства,  необходимо <br>
                                определиться с количеством и типом используемого материала, кирпича.
                            </div>

                            <form class="landing-form" data-topic="Сделаем точный расчет">
                                <input type="hidden" name="topicLanding[]">
                                <div class="input-row input-row--full">
                                    <input type="text" class="help-full__input" placeholder="Ваш вопрос" name="yourQuestion[]">
                                </div>

                                <div class="input-row input-row--half">
                                    <input type="text" class="help-half__input" placeholder="Имя" name="yourName[]">
                                    <input type="text" class="help-half__input input-phone"  placeholder="Номер телефона" name="yourPhone[]">
                                </div>

                                <div class="input-row input-row--half">
                                    <div class="checkbox-wrapper custom-checkbox">
                                        <label>
                                            <input type="checkbox" checked name="checkbox-personal-data" class="checkbox-personal-data">
                                            <span class="pretty-checkbox"></span>
                                            <span class="personal-text">Даю согласие на обработку персональных данных</span>
                                        </label>
                                    </div>
                                    <div class="help-half__input">
                                        <input type="submit" value="Отправить">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="contact-dark__workers2">
                            <div class="workers__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/workers2.png" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="contact-dark__right">
                <div class="right__image">
                </div>
            </div>
        </div>

        <div class="makers">
            <div class="container">
                <div class="landing-title makers__title">
                    Производители
                </div>

                <div class="makers__items" data-aos="fade-up" data-aos-duration="1000" data-aos-once="true">
                    <div class="item-maker"><a href="/proizvoditeli/alekseevskaya-keramika/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/01.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/promix/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/02.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/perel/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/03.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/altair/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/04.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kirpichnyy-zavod-amstron-porikam/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/05.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/arskiy-kirpichnyy-zavod-aspk/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/06.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/bikton/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/07.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/vkz/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/08.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/fakro/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/09.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kazanskiy-kombinat-stroitelnykh-materialov/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/10.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kc-kepamik/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/11.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/shelangovskiy-kirpichnyy-zavod/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/12.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kirpichnyy-zavod-ketra/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/13.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/klyuker"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/14.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/koshchakovskiy-kirpichnyy-zavod/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/15.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/metall-profil/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/16.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/profi-and-hobby/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/17.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/wienerberger/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/18.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/osnovit/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/19.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/starkhaus/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/20.svg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/chaykovskiy-kirpichnyy-zavod/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/21.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/izkm/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/22.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kamgeszyab/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/23.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/mamadyshskiy/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/24.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/bogdanovichskiy/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/25.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/bund-bau/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/26.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/cheboksarskaya-keramika/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/27.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/chelninskiy/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/28.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kazanskiy-zavod-silikatnykh-stenovykh-materialov/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/29.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kirpich-kholding/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/30.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kottedzh/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/31.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/magma/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/32.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/makhina-tst/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/33.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/steklonit/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/34.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/stroitelnye-innovatsii"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/35.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/teplon"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/36.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/uniblock"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/37.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/vilpe/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/38.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/grand-line"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/39.jpeg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/belebeevskiy"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/40.png" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/tekhnonikol"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/41.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kulonstroy/"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/42.jpeg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/raff"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/43.jpg" alt=""></a></div>
                    <div class="item-maker"><a href="/proizvoditeli/kerma"><img src="/local/templates/tulpar_store/assets/landing/dist/img/brands/44.png" alt=""></a></div>
                </div>
            </div>
        </div>

        <div class="landing-partners">
            <div class="container">
                <div class="landing-title partners__title">
                    Наши партнеры
                </div>

                <div class="partner__items">
                    <div class="item-partner">
                        <span class="partner-image">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/partners/1.jpg" alt="">
                        </span>

                        <span class="partner-title">
                            Камастройинвест
                        </span>

                    </div>

                    <div class="item-partner">
                        <span class="partner-image">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/partners/2.jpg" alt="">
                        </span>

                        <span class="partner-title">
                            Суварстроит
                        </span>
                    </div>

                    <div class="item-partner">
                        <span class="partner-image">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/partners/3.jpg" alt="">
                        </span>

                        <span class="partner-title">
                            Жилой комплекс <br> "Романтика"
                        </span>
                    </div>

                    <div class="item-partner">
                        <span class="partner-image">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/partners/4.jpg" alt="">
                        </span>

                        <span class="partner-title">
                            Унистрой
                        </span>
                    </div>

                    <div class="item-partner">
                        <span class="partner-image">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/partners/5.jpg" alt="">
                        </span>

                        <span class="partner-title">
                            Жилой комплекс <br> "Южный парк"
                        </span>
                    </div>

                    <div class="item-partner">
                        <span class="partner-image">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/partners/6.jpg" alt="">
                        </span>

                        <span class="partner-title">
                            Царево - Village
                        </span>
                    </div>

                    <div class="item-partner">
                        <span class="partner-image">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/partners/7.jpg" alt="">
                        </span>

                        <span class="partner-title">
                            Vincent
                        </span>
                    </div>

                    <div class="item-partner">
                        <span class="partner-image">
                            <img src="/local/templates/tulpar_store/assets/landing/dist/img/partners/8.jpg" alt="">
                        </span>

                        <span class="partner-title">
                            СитиСтрой
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="contact-dark landing__contact-form3">
            <div class="contact-dark__left"></div>
            <div class="contact-dark__center">
                <div class="container">
                    <div class="contact-dark__body">
                        <div class="contact-dark__form">
                            <h2>Остались вопросы? <br> Мы на них ответим!</h2>

                            <div class="contact-dark__desc">
                                Оставьте, ваши контактные данные, и наш менеджер свяжется с вами в ближайшее время и ответит на все интересующие вас вопросы!
                            </div>

                            <form class="landing-form" data-topic="Остались вопросы?">
                                <input type="hidden" name="topicLanding[]" value="">
                                <div class="input-row input-row--full">
                                    <input type="text" class="help-full__input" placeholder="Ваш вопрос" name="yourQuestion[]">
                                </div>

                                <div class="input-row input-row--half">
                                    <input type="text" class="help-half__input" placeholder="Имя" name="yourName[]">
                                    <input type="text" class="help-half__input input-phone" placeholder="Номер телефона" name="yourPhone[]">
                                </div>

                                <div class="input-row input-row--half">
                                    <div class="checkbox-wrapper custom-checkbox">
                                        <label>
                                            <input type="checkbox" checked name="checkbox-personal-data" class="checkbox-personal-data">
                                            <span class="pretty-checkbox"></span>
                                            <span class="personal-text">Даю согласие на обработку персональных данных</span>
                                        </label>
                                    </div>
                                    <div class="help-half__input">
                                        <input type="submit" value="Отправить">
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="contact-dark__workers3">
                            <div class="workers__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/workers3.png" alt="">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="contact-dark__right">
                <div class="right__image">
                </div>
            </div>
        </div>

        <div class="social_landing" >
            <div class="container">
                <div class="landing-title social__title">Следите за нами соцсетях</div>
                <div class="social__items" >
                    <div class="social__col">
                        <div class="social__blog">
                            <div class="blog__title">Наш блог</div>
                            <div class="blog__text">Полезные статьи от "Тулпар Трейд"</div>
                            <div class="blog__link"><a href="#">Читать блог</a></div>

                            <div class="blog__image">
                                <img src="/local/templates/tulpar_store/assets/landing/dist/img/worker_blog.png" alt="">
                            </div>
                        </div>

                        <div class="social__item">
                            <a href="">
                                <div class="social__image">
                                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/social/fb.png" alt="">
                                    <span>Следите за нашей страницей <b>в Facebook</b></span>
                                </div>

                                <div class="social__blue">
                                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/social/blue.png" alt="">
                                </div>

                                <div class="social__arrow">
                                    <span></span>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="social__col">
                        <div class="social__item">
                            <a href="">
                                <div class="social__image">
                                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/social/vk.png" alt="">
                                    <span>Следите за нашей страницей <b>в Вконтакте</b></span>
                                </div>

                                <div class="social__blue">
                                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/social/blue.png" alt="">
                                </div>

                                <div class="social__arrow">
                                    <span></span>
                                </div>
                            </a>
                        </div>

                        <div class="social__item">
                            <a href="">
                                <div class="social__image">
                                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/social/ig.png" alt="">
                                    <span>Следите за нашей страницей <b>в Инстаграм</b></span>
                                </div>

                                <div class="social__blue">
                                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/social/blue.png" alt="">
                                </div>

                                <div class="social__arrow">
                                    <span></span>
                                </div>
                            </a>
                        </div>

                        <div class="social__item">
                            <a href="">
                                <div class="social__image">
                                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/social/ok.png" alt="">
                                    <span>Следите за нашей страницей <b>в Одноклассниках</b></span>
                                </div>

                                <div class="social__blue">
                                    <img src="/local/templates/tulpar_store/assets/landing/dist/img/social/blue.png" alt="">
                                </div>

                                <div class="social__arrow">
                                    <span></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>