<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("В кредит");
?><div class="credit-banner-main banner-main text-center">
    <div class="banner-web">
        <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/tinkof.jpg" alt="">
        <button id="credit-btn" data-fancybox data-src="#credit-application">Оформить заявку</button>
    </div>
</div>

<div class="container pt-4" id="preimushestva">
    <h3 class="text-center">Преимущества нашего кредита</h3>
    <div class="preimushestva">
        <div class="item  col-md-6 col-sm-6 col-xs-6">
            <span class="icon col-md-2 col-sm-2">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/bezspravok2.png" alt="">
            </span>
            <div class="text col-md-11 col-sm-12">
                <h5>Без залога и поручителей</h5>
                <p>Кредит на покупку в нашем магазине без залога и поручителей. Оформите онлайн заявку и получите одобрение в день обращения</p>
            </div>
        </div>

        <div class="item  col-md-6 col-sm-6 col-xs-6">
            <span class="icon col-md-2 col-sm-2">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/pasport.png" alt="">
            </span>

            <div class="text col-md-11 col-sm-12">
                <h5>По паспорту, без справок</h5>
                <p>Подтверждение дохода не является обязательным условием для получения кредита наличными без залога.</p>
            </div>
        </div>

        <div class="item  col-md-6 col-sm-6 col-xs-6">
            <span class="icon col-md-2 col-sm-2">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/summa.png" alt="">
            </span>
            <div class="text col-md-11 col-sm-12">
                <h5>Кредит до 100 000 ₽</h5>
                <p>Получите кредит лимитом до 100 000 ₽ на покупку в нашем магазине</p>
            </div>
        </div>

        <div class="item  col-md-6 col-sm-6 col-xs-6">
            <span class="icon col-md-2 col-sm-2">
                <img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/summa.png" alt="">
            </span>
            <div class="text col-md-11 col-sm-12">
                <h5>Ставка по кредиту от 12%</h5>
                <p>Для каждого клиента ставка рассчитывается индивидуально</p>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="pt-3 pb-3">

    </div>
</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>