<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");

use Bitrix\Sale;
use Bitrix\Sale\Basket;
Bitrix\Main\Loader::includeModule("sale");
Bitrix\Main\Loader::includeModule("catalog");


$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$basketItems = $basket->getBasketItems();

if (isset($_POST['action'])) {
    $GLOBALS['APPLICATION']->RestartBuffer();

    if ($_POST['action'] === "updateBasket" && isset($_POST['data'])) {
        $data = json_decode($_POST['data'], true);

        updateBasketItemAmount($data['actionType'], $data['productId'], $data['measureRatio'], $data['basketRecordId'], $basket);
    }
}

function updateBasketItemAmount($actionType, $productId, $measureRatio, $basketRecordId, $basketObj) {

    $item = $basketObj->getItemById($basketRecordId);
    if ($item) {
        $currentQuantity = $item->getField('QUANTITY');
    }

    switch ($actionType){
        case "add":
            $item->setField('QUANTITY', $currentQuantity+$measureRatio);
            break;
        case "decrease":
            if ($currentQuantity > 0) {
                $newQuantity = (($currentQuantity - $measureRatio) < $measureRatio) ? 0 : ($currentQuantity - $measureRatio);
                $item->setField('QUANTITY', $newQuantity);
            }
            break;
        case "remove":
            $item->delete();
            break;
    }

    $basketObj->save();

}


$prodIdArr =  [];
foreach ($basket as $basketItem){
    $prodIdArr[] = $basketItem->getField('PRODUCT_ID');
}


///////////


//////////

$arSelect = Array("ID", "NAME", "PREVIEW_PICTURE", "DETAIL_PAGE_URL", "PROPERTY_ATT_BASKET_COEF_MEASURE_UNIT");
$arFilter = Array("IBLOCK_ID" => 4, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y", "ID" => $prodIdArr);
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

$simpleProdIdArr = [];
$allBasketItemsArr = [];
while ($ob = $res->GetNextElement()) {
    $arFields = $ob->GetFields();
    $simpleProdIdArr[] = $arFields["ID"];
    $allBasketItemsArr[$arFields["ID"]] = $arFields;
}

$skuIdArr = array_diff($prodIdArr, $simpleProdIdArr);

$skuMainProductList = CCatalogSKU::getProductList(
    $skuIdArr
);

$skuMainProdIdArr = [];
foreach ($skuMainProductList as $skuMainProd) {
    $skuMainProdIdArr[] = $skuMainProd["ID"];
}

$res = CCatalogSKU::getOffersList(
    $skuMainProdIdArr, // массив ID товаров
    $iblockID = 4, // указываете ID инфоблока только в том случае, когда ВЕСЬ массив товаров из одного инфоблока и он известен
    $skuFilter = array(), // дополнительный фильтр предложений. по умолчанию пуст.
    $fields = array("PREVIEW_PICTURE", "NAME", "SECTION_ID"),  // массив полей предложений. даже если пуст - вернет ID и IBLOCK_ID
    $propertyFilter = array("CODE" => ["ATT_BASKET_COEF_MEASURE_UNIT_SKU"])
);

//echo "<pre>";
//print_r($res);
//echo "</pre>";
//
//foreach ($basket as $basketItem) {
//    echo $basketItem->getField('DETAIL_PAGE_URL') . ' - ' . $basketItem->getQuantity() . '<br />';
//}


foreach ($res as $product) {
    foreach ($product as $id => $sku) {
        if (in_array($id, $skuIdArr)) {
            $allBasketItemsArr[$id] = $sku;
            $allBasketItemsArr[$id]["IS_OFFER"] = "Y";
        }
    }
}

$previewImageIdArr = [];
foreach ($allBasketItemsArr as $item) {
    $previewImageIdArr[] = $item['PREVIEW_PICTURE'];
}

$res = CFile::GetList(
    array(), array("@ID" => $previewImageIdArr)
);


while($res_arr = $res->GetNext()) {
    foreach ($allBasketItemsArr as $key => $basketItem) {
        if ($allBasketItemsArr[$key]['PREVIEW_PICTURE'] == $res_arr['ID']) {
            $allBasketItemsArr[$key]['PREVIEW_PICTURE_URL'] = "/upload/".$res_arr['SUBDIR']."/".$res_arr['FILE_NAME'];
        }
    }
}

$basketItemIdArr = [];
foreach ($basketItems as $basketItem) {
    $id = $basketItem->getField('PRODUCT_ID');
    $price = $basketItem->getPrice();
    $finalPrice = $basketItem->getFinalPrice();
    $quantity = $basketItem->getQuantity();
    $basketRecordId = $basketItem->getId();
    $measureName = $basketItem->getField('MEASURE_NAME');
    if ($allBasketItemsArr[$id]["IS_OFFER"] === "Y") {
        $allBasketItemsArr[$id]["DETAIL_PAGE_URL"] = $basketItem->getField('DETAIL_PAGE_URL')."?OFFER_ID=$id";
    }

    $allBasketItemsArr[$id]["PRICE"] = $price;
    $allBasketItemsArr[$id]["PRICE"] = $price;
    $allBasketItemsArr[$id]["FINAL_PRICE"] = $finalPrice;
    $allBasketItemsArr[$id]["QUANTITY"] = $quantity;
    $allBasketItemsArr[$id]['BASKET_RECORD_ID'] = $basketRecordId;
    $allBasketItemsArr[$id]['MEASURE_NAME'] = $measureName;

    $basketItemIdArr[] = $id;
}

$measureData = \Bitrix\Catalog\MeasureRatioTable::getCurrentRatio($basketItemIdArr);
$products = array();
//echo "<pre>";
//print_r($allBasketItemsArr);
//echo "</pre>";

//
//echo "<pre>";
//    print_r($basketItems);
//echo "<pre>";
//exit;
?><div id="basket--js">

    <div class="gray-wrapper">
        <div class="container pl-0 pr-0">
            <?php if (sizeof($basketItems) > 0): ?>
                <div class="other-pages cart">
                    <div class="product-table-cart">
                        <div class="product-table-header">
                            <div class="product-table-col product-table-col_img">Изображение товара</div>
                            <div class="product-table-col product-table-col_name">Наименование товара</div>
                            <div class="product-table-col product-table-col_price">Цена</div>
                            <div class="product-table-col product-table-col_qty">Количество</div>
                            <div class="product-table-col product-table-col_coef"></div>
                            <div class="product-table-col product-table-col_total">Итого</div>
                            <div class="product-table-col product-table-col_remove"></div>
                        </div>

                        <?php



                        foreach ($allBasketItemsArr as $basketItem):

                            /**
                             * TODO
                            заменить offer-id на data-offer-id и протестировтаь
                             */
                            if ($basketItem['PROPERTIES']['ATT_BASKET_COEF_MEASURE_UNIT_SKU']['VALUE']) {
                                $basketCoefMeasureUnit = $basketItem['PROPERTIES']['ATT_BASKET_COEF_MEASURE_UNIT_SKU']['VALUE'];
                            } elseif ($basketItem['PROPERTY_ATT_BASKET_COEF_MEASURE_UNIT_VALUE']) {
                                $basketCoefMeasureUnit = $basketItem['PROPERTY_ATT_BASKET_COEF_MEASURE_UNIT_VALUE'];
                            } else {
                                $basketCoefMeasureUnit = "";
                            }

                            if ($basketItem['QUANTITY'] > 0):

                                $products[] = (string)$basketItem["ID"];
                            ?>

                            <div class="product-table-row" offer-id="<?=$basketItem["ID"]?>" data-product-id="<?=$basketItem["ID"]?>" data-basket-record-id="<?=$basketItem['BASKET_RECORD_ID']?>">
                                <div class="product-table-col product-table-col_img">
                                    <div class="img-wrap">
                                        <a href="<?=$basketItem["DETAIL_PAGE_URL"]?>" title="<?=$basketItem["NAME"]?>">
                                            <img src="<?=$basketItem['PREVIEW_PICTURE_URL']?>">
                                        </a>
                                    </div>
                                </div>
                                <div class="product-table-col product-table-col_name">
                                    <a href="<?=$basketItem["DETAIL_PAGE_URL"]?>"><?=$basketItem['NAME']?></a>
                                </div>
                                <div class="product-table-col product-table-col_price">
                                    <span class="price-num"><?=number_format($basketItem['PRICE'], 2)?></span>
                                    <span class="price-ico"> ₽</span>
                                </div>
                                <div class="product-table-col product-table-col_qty">
                                    <div class="change-qty" data-product-id="<?=$basketItem['ID']?>" data-basket-record-id="<?=$basketItem['BASKET_RECORD_ID']?>" data-measure-ratio="<?=$measureData[$basketItem['ID']]?>">
                                        <span class="minus basket-handler__js" data-basketHandler="minus"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/minus_bold_gray.svg" alt=""></span>
                                        <input type="text" readonly name="elem_<?=$basketItem['ID']?>_qty" value="<?=$basketItem['QUANTITY']?> <?=$basketItem['MEASURE_NAME']?>">
                                        <span class="plus basket-handler__js" data-basketHandler="plus"><img src="<?=SITE_TEMPLATE_PATH?>/assets/img/icons/plus_bold_gray.svg" alt=""></span>
                                        <span class="measure"><span><?if ($basketCoefMeasureUnit) echo intval(($basketItem['QUANTITY']/$measureData[$basketItem['ID']]));?></span><?=$basketCoefMeasureUnit;?></span>
                                    </div>
                                </div>
                                <div class="product-table-col product-table-col_coef"></div>
                                <div class="product-table-col product-table-col_total">
                                    <span class="price-num"><?=number_format($basketItem['FINAL_PRICE'], 2)?></span>
                                    <span class="price-ico"> ₽</span>
                                </div>
                                <div class="product-table-col product-table-col_remove">
                                    <button type="button" class="close basket-handler__js" data-basketHandler="remove" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                            <?endif;
                            endforeach; ?>

                    </div>

                    <div class="cart-total">
                        <div class="total-sum">
                            <span class="text">Итого:</span>
                            <span class="num"><?=number_format($price = $basket->getPrice(), 2, '.', ',');?></span>
                            <span class="ico"> ₽</span>
                        </div>

                        <button data-fancybox data-src="#send-cart" id="cart-order" class="btn btn-default">Оформить заказ</button>
                    </div>
                </div>
            <?php else: ?>
                <div class="other-pages">
                    <h1 class="page-title">Корзина</h1>
                    <p>Ваша корзина пуста. Выберите нужный Вам товар из <a href="/catalog/">каталога Интернет-магазина</a> и нажмите кнопку "В корзину".</p>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?if(count($products) > 0):?>
<script>
    var product_tmr = {
        productid: <?=json_encode($products);?>,
        pagetype: 'cart',
        list: '1'
    };
</script>
<?endif?>

<?
if (isset($_POST['action'])) {
    die();
}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>